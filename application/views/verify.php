
<!DOCTYPE html><head>
	<meta charset="utf-8" />
	<title>Starclub Inc.</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="http://sccms.starsite.com/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/css/metro.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/css/style.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/css/style_responsive.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="http://sccms.starsite.com/assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="http://sccms.starsite.com/assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="http://sccms.starsite.com/assets/cleditor/jquery.cleditor.css" />
	<link rel="stylesheet" type="text/css" href="http://sccms.starsite.com/assets/bootstrap-datepicker/css/datepicker.css" />
	<link href="http://sccms.starsite.com/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="http://sccms.starsite.com/assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" href="http://sccms.starsite.com/assets/jquery-file-upload/css/jquery.fileupload-ui.css">
    
<noscript>
        <link rel="stylesheet" href="http://sccms.starsite.com/assets/jquery-file-upload/css/jquery.fileupload-ui-noscript.css">
    </noscript>
    <!-- BEGIN JAVASCRIPTS -->
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <script src="http://sccms.starsite.com/assets/js/jquery-1.8.3.min.js"></script>    
    <script src="http://sccms.starsite.com/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>    
    
    <!--[if lt IE 9]>
    <script src="http://sccms.starsite.com/assets/js/excanvas.js"></script>
    <script src="http://sccms.starsite.com/assets/js/respond.js"></script>    
    <![endif]-->    
    <script src="http://sccms.starsite.com/assets/breakpoints/breakpoints.js"></script>        
    <script src="http://sccms.starsite.com/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="http://sccms.starsite.com/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="http://sccms.starsite.com/assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="http://sccms.starsite.com/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://sccms.starsite.com/assets/js/jquery.blockui.js"></script>    
    <script src="http://sccms.starsite.com/assets/js/jquery.cookie.js"></script>
    <script src="http://sccms.starsite.com/assets/js/nicEdit.js"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>    
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="http://sccms.starsite.com/assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>    
    <script src="http://sccms.starsite.com/assets/flot/jquery.flot.js"></script>
    <script src="http://sccms.starsite.com/assets/flot/jquery.flot.resize.js"></script>
    <script src="http://sccms.starsite.com/assets/flot/jquery.flot.pie.js"></script>
    <script src="http://sccms.starsite.com/assets/flot/jquery.flot.stack.js"></script>    
    <script type="text/javascript" src="http://sccms.starsite.com/assets/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/js/jquery.pulsate.min.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="http://sccms.starsite.com/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!--<script type="text/javascript" src="http://sccms.starsite.com/assets/cleditor/jquery.cleditor.js"></script>-->    
    <script src="http://sccms.starsite.com/assets/js/app.js"></script>    
    <script>
        jQuery(document).ready(function() {        
            App.setPage("auth/login");  // set current page
            App.init(); // init the rest of plugins and elements
        });
    </script>

</head>
<input type='hidden' value="http://sccms.starsite.com/index.php/ajax" id="ajax_control_url">
<!-- END HEAD -->
<!-- BEGIN BODY -->
<style >
    .backcolor, .backcolor .backcolor{
        background-color: # !important;
    }
    .btn, .backcolor .btn:hover, .backcolor .btn:focus{
        background-color: # !important;
    }
    span.title, ul.sub li a{
        color : # !important;
    }
    a:hover span.title, span.title:hover,.page-sidebar > ul > li > ul.sub > li > a:hover{
        color: # !important;
    }
</style>
<body class="fixed-top backcolor">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner backcolor">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="http://sccms.starsite.com/index.php">
				<img src="http://sccms.starsite.com/assets/img/logo.png" alt="logo" style="height:28px" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">

	<!-- BEGIN LOGIN -->
<div class="login">
  <div class="content">
  
<h3 class="form-title"><?php echo $text; ?></h3>

	</div>
</div>
<script>
    $("body").addClass('login');
</script>
<!-- END LOGIN -->	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2014 &copy; Starclub Inc. </div>
	<!-- END FOOTER -->
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>
