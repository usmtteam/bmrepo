
  function _login() {
    FB.login(function(response) {
       // handle the response
       if(response.status==='connected') {
        _i();
        
       }
     }, {scope: 'public_profile,email'});

 }
 
function _logout() {
    var user_token = "";
    var current_user = null;
            
              $( ".login_top" ).removeClass( "hide" );
              $( ".login_top" ).addClass( "show" );
              $( ".login_top_out" ).addClass( "hide" );
              $( ".login_top_out" ).removeClass( "show" );

              $( ".login_top_mobile" ).removeClass( "hide" );
              $( ".login_top_mobile" ).addClass( "show" );
              $( ".login_top_out_mobile" ).addClass( "hide" );
              $( ".login_top_out_mobile" ).removeClass( "show" );
              eraseCookie('current_user');
             
          
 }

 function _i(){
     FB.api('/me', function(response) {
        var username = response.first_name + ' ' + response.last_name;
        var first_name = response.first_name;
        var last_name  = response.last_name;
        var email = response.email;
        var facebook_page = response.id;

        $.ajax({
          url:'http://admincms.starsite.com/api?cid=' + l_nChannelId + '&option=register&username=' + username + '&firstname=' + first_name + '&lastname=' + last_name + '&email=' + email + '&facebook_page_id=' + facebook_page + '&facebook=1',
          type: 'GET',
          success: function(data, status, headers, config) {
            if(data.status == true) {
              var user_token = data.token;
              var current_user = data.info['id'];
              var facebook_page = data.info['facebook_page_id'];
              var username = data.info['firstname'] + ' ' + data.info['lastname'];
              localStorage.setItem('current_user',current_user);
              
              //$rootScope.dialogLogin.close();
              createCookie('current_user',current_user,(86400 * 30));
              createCookie('facebook_page',facebook_page,(86400 * 30));
              createCookie('username',username,(86400 * 30));
              
              //set_cookie( "current_user", current_user, 7, "starsite.com" ); 
              //set_cookie( "user_token", user_token, 7, "starsite.com" ); 

              $( ".cd-user-modal" ).removeClass( "is-visible" );
              $( ".login_top" ).addClass( "hide" );
              $( ".login_top" ).removeClass( "show" );
              $( ".login_top_out" ).addClass( "show" );
              $( ".login_top_out" ).removeClass( "hide" );

              $( ".login_top_mobile" ).addClass( "hide" );
              $( ".login_top_mobile" ).removeClass( "show" );
              $( ".login_top_out_mobile" ).addClass( "show" );
              $( ".login_top_out_mobile" ).removeClass( "hide" );


            }
            else {
              var error = data.message;
            }
          }
        });
          
    });

     
 }

 function login_email(){

        var email = $('#text_email').val();
        var password = $('#text_password').val();
        
        if(email == '') {
          var error = 'Please enter your email.';
        }
        else if(password == '') {
          error = 'Please enter your password.';
        }else{

        $.ajax({
          url:'http://admincms.starsite.com/api?cid=' + l_nChannelId + '&option=login&email=' + email + '&password=' + password + '&ud_token=WEB',
          type: 'GET',
          success: function(data, status, headers, config) {
            if(data.status == true) {
              var user_token = data.token;
              var current_user = data.info['id'];
              localStorage.setItem('current_user',current_user);
              
              //$rootScope.dialogLogin.close();
              createCookie('current_user',current_user,(86400 * 30));
              
              
              //set_cookie( "current_user", current_user, 7, "starsite.com" ); 
              //set_cookie( "user_token", user_token, 7, "starsite.com" ); 

              $( ".cd-user-modal" ).removeClass( "is-visible" );
              $( ".login_top" ).addClass( "hide" );
              $( ".login_top" ).removeClass( "show" );
              $( ".login_top_out" ).addClass( "show" );
              $( ".login_top_out" ).removeClass( "hide" );

              $( ".login_top_mobile" ).addClass( "hide" );
              $( ".login_top_mobile" ).removeClass( "show" );
              $( ".login_top_out_mobile" ).addClass( "show" );
              $( ".login_top_out_mobile" ).removeClass( "hide" );


             

            }
            else {
              var error = data.message;
            }
          }
        });
    }

 }


 function signup_email(){

        var username = $('#signup-username');
        var first_name = $('#signup-firstname');
        var last_name = $('#signup-lastname');
        var email = $('#signup-email');
        var password = $('#signup-password');
        
        if(username.val() == '') {
          var error = 'Please enter your username.';
          username.focus();
        }
        else if(first_name.val() == '') {
          error = 'Please enter your first name.';
          first_name.focus();
        }
        else if(last_name.val() == '') {
          error = 'Please enter your last name.';
          last_name.focus();
        }
        else if(email.val() == '') {
          error = 'Please enter your email.';
          email.focus();
        }
        else if(password.val() == '') {
          error = 'Please enter your password.';
          password.focus();
        }else{

        $.ajax({
          url:'http://admincms.starsite.com/api?cid=' + l_nChannelId + '&option=register&username=' + username.val() + '&firstname=' + first_name.val() + '&lastname=' + last_name.val() + '&email=' + email.val() + '&password=' + password.val(),
          type: 'GET',
          success: function(data, status, headers, config) {
            if(data.status == true) {
              var user_token = data.token;
              var current_user = data.info['id'];
              localStorage.setItem('current_user',current_user);
              
              createCookie('current_user',current_user,(86400 * 30));
              
              //set_cookie( "current_user", current_user, 7, "starsite.com" ); 
              //set_cookie( "user_token", user_token, 7, "starsite.com" ); 

              $( ".cd-user-modal" ).removeClass( "is-visible" );
              $( ".login_top" ).addClass( "hide" );
              $( ".login_top" ).removeClass( "show" );
              $( ".login_top_out" ).addClass( "show" );
              $( ".login_top_out" ).removeClass( "hide" );

              $( ".login_top_mobile" ).addClass( "hide" );
              $( ".login_top_mobile" ).removeClass( "show" );
              $( ".login_top_out_mobile" ).addClass( "show" );
              $( ".login_top_out_mobile" ).removeClass( "hide" );
             

            }
            else {
              var error = data.message;
            }
          }
        });
      }

 }



function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

        