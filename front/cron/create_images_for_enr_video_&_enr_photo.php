<?php

set_time_limit(0); // Override Codeigniter 30 second time limit
//var_dump(phpinfo());
$time_start = microtime(true); // Start calculating time
echo "<BR><BR>\$time_start: ".$time_start;
//exit;

///Applications/MAMP/htdocs/starsite/front/config/settings.php
//require(__DIR__.'/../config/settings.php');
//require(__DIR__.'/../config/constants.php');
///Applications/MAMP/htdocs/starsite/cms/application/libraries/Utils.php
require(__DIR__.'/../../cms/application/libraries/Utils.php');

/*
|--------------------------------------------------------------------------
| Custom Define
|--------------------------------------------------------------------------
 */

define('RT_PATH', dirname(dirname(__DIR__)));
define('UPLOAD_DIR', RT_PATH.'/cms/assets/ufile/');
define('UPLOAD_DIR_WATER', RT_PATH.'/cms/assets/ufile/watermark/');
define('WATERMARK_PLAY_BUTTON',RT_PATH.'/cms/assets/img/play.png');
define('CDN_FRONT','http://d3hl6zmfu9utfu.cloudfront.net');

define('AMAZON_AWS_BUCKET_FOR_ASSETS','sc-media-assets');

define('AMAZON_AWS_KEY','AKIAIWNCSRKRK462ZOUA');
define('AMAZON_AWS_SECRET','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');

define('USE_EC2',true);

//echo dirname(__DIR__);
//echo "<br><br>".RT_PATH;
//echo "<br><br>".UPLOAD_DIR;

//exit;

$l_xDevTeamConnection =
    mysqli_connect('mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com','stars_dba','xZUsd2124Ut-#~4W}','starsite_stars');

$sql_photo = 'SELECT "enr_photo", ec.slug, ep.* FROM enr_photo ep
           LEFT JOIN enr_chanel ec ON ec.id = ep.cid
           WHERE ec.slug IS NOT NULL';

$sql_video = 'SELECT "enr_video", ec.slug, ev.* FROM enr_video ev
           LEFT JOIN enr_chanel ec ON ec.id = ev.cid
           WHERE ec.slug IS NOT NULL';

$result_photo = mysqli_query($l_xDevTeamConnection, $sql_photo);
$result_video = mysqli_query($l_xDevTeamConnection, $sql_video);

$result_photo_main = array();
$result_video_main = array();

while ($l_xObj = mysqli_fetch_assoc($result_photo)) {
    $result_photo_main[] = $l_xObj;

//    var_dump($l_xObj);
//    exit;
}

while ($l_xObj = mysqli_fetch_assoc($result_video)) {
    $result_video_main[] = $l_xObj;

//    var_dump($l_xObj);
//    exit;
}

//string 'http://d3hl6zmfu9utfu.cloudfront.net' (length=36)
echo "<BR><BR>CDN_FRONT: ".CDN_FRONT;

//var_dump($l_aResultsMain);

echo "<BR><BR>create_images(\$result_photo_main)";
create_images($result_photo_main); // enr_photo

echo "<BR><BR>create_images(\$result_video_main)";
create_images($result_video_main); // enr_video

// Script End time
$time_end = microtime(true);

// dividing with 60 will give the execution time in minutes other wise seconds
$execution_time = ($time_end - $time_start)/60;

//execution time of the script
echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';

$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//execution time of the script
echo "<BR><BR>\$time: ".$time;

/**
 * Load image.
 *
 * @param $source_file
 * @return null|resource
 */
function loadImage($source_file){

    // Load image
    $ext = exif_imagetype ($source_file);
    $destination_file = NULL;

    if($ext == IMAGETYPE_PNG) $destination_file = imagecreatefrompng ($source_file);
    elseif($ext == IMAGETYPE_JPEG) $destination_file = imagecreatefromjpeg ($source_file);
    elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $destination_file = imagecreatefromwbmp ($source_file);
    elseif($ext == IMAGETYPE_GIF) $destination_file = imagecreatefromgif ($source_file);

    return $destination_file;
}

/**
 * Resize image and push to S3.
 *
 * @param $thumb_array
 * @return bool|mixed|null
 */
function resize_image_push_to_S3($thumb_array){

    echo "<BR><BR>Resize image";

    $filepath_thumb = $thumb_array['filepath'];
    $newwidth = $thumb_array['new_width'];
    $file_a3_path_thumb = $thumb_array['s3_path'];

    echo "<BR>\$filepath_thumb: ".$filepath_thumb;
    if (file_exists($filepath_thumb)) { echo " exists"; }
    echo "<BR>\$newwidth: ".$newwidth;
    echo "<BR>\$file_a3_path_thumb: ".$file_a3_path_thumb;

    if (file_exists($filepath_thumb)){
        // Thumb No Watermark
        list($width,$height)=getimagesize($filepath_thumb);

        $newheight=($height/$width) * $newwidth;

        // Load original image
        $src = loadImage($filepath_thumb);

        // Resize the image to be however pixels wide, and maintain the original aspect ratio.
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // Copy from the original image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // Write the resized image jpeg to disk
        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);

        if ($l_sNewPath ){ echo "<BR> Pushed to S3: ". $l_sNewPath; }
        elseif (!$l_sNewPath ){ echo "<BR><b>Error: </b> Failed to push to S3: ". $file_a3_path_thumb; }

        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        //execution time of the script
        echo "<BR><BR>\$time: ".$time;
        echo "<BR><BR>\$time mins: ".$time/60;

        return $l_sNewPath;

    } else {

        echo "<BR><BR><b>Error: </b>Unresized file ".$filepath_thumb." does not exist";

        return false;
    }
}

/**
 * Watermark Play Button and push to S3.
 *
 * @param $thumb_array
 * @return bool|mixed|null
 */
function watermark_play_button_push_to_s3($thumb_array){

    $watermarked_local_file = $thumb_array['filepath'];
    $width = $thumb_array['new_width'];
    $watermarked_cloud_file = $thumb_array['s3_watermark_path'];

    if (file_exists($watermarked_local_file)){

        // Watermark Play Button onto Thumbnail

        // Get Original Thumbnail width & height
        list($sw, $sh) = getimagesize($watermarked_local_file);

        // Get Watermark Play Button width & height
        list($sww, $swh) = getimagesize(WATERMARK_PLAY_BUTTON);

        // Calculate simulated size
        if (empty($width)){ $width = $sw; }

        $height = round($sh * $width / $sw);

        // Resized watermark size
        $ww = round($width);
        $wh = round($swh * $ww / $sww);

        // Load original image
        $hSource = loadImage($watermarked_local_file);

        // Load watermark image
        $hWatermark = loadImage(WATERMARK_PLAY_BUTTON);

        // Resize the image to be however pixels wide, and maintain the original aspect ratio.
        $hDest = imagecreatetruecolor($width, $height);

        // Copy from the original image into the $tmp image
        imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);

        // Watermark
        imagecopyresampled($hDest, $hWatermark, $width - $ww, ($height - $wh)/2, 0, 0, $ww, $wh, $sww, $swh);

        // Write the resized image jpeg to disk
        imagejpeg($hDest,$watermarked_local_file."_watermark.jpeg");

        $l_sNewAbsoluteFilePath = $watermarked_local_file."_watermark.jpeg";

        echo "<BR><BR>Watermarked file ".$watermarked_local_file;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath, $watermarked_cloud_file);

//            @unlink($watermarked_local_file);

        if ($l_sNewPath ){ echo "<BR> Pushed to S3: ". $l_sNewPath; }
        elseif (!$l_sNewPath ){ echo "<BR><b>Error: </b> Failed to push to S3: ". $l_sNewPath; }

        // Calculate script run time in seconds
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        //execution time of the script
        echo "<BR><BR>\$time: ".$time;
        echo "<BR><BR>\$time mins: ".$time/60;

        return $l_sNewPath;

    } else {

        echo "<BR><BR><b>Error: </b>Unwatermarked file ".$watermarked_local_file." does not exist";

        return false;
    }
}

/**
 * Create images for enr_photo and enr_video.
 *
 * @param $l_aResultsMain
 */
function create_images($l_aResultsMain){

    foreach ($l_aResultsMain as $l_xResultRow){
        $slug = $l_xResultRow['slug'];
        echo "<BR><BR>\$slug: ".$slug;

        if (!empty($l_xResultRow['enr_photo'])){ $destination = $l_xResultRow['destination']; }
        elseif (!empty($l_xResultRow['enr_video'])) { $destination = $l_xResultRow['image_path']; }

        // Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
        if($destination) {

            $slug_photo_path = "/".$slug."/assets/ufile/".$destination;
            echo "<br><br>\$photo_path: ".$slug_photo_path;

            $photo_CDN_path = CDN_FRONT.$slug_photo_path;
            echo "<br><br>\$photo_CDN_path: ".$photo_CDN_path;

            $pos = strrpos($destination, "/"); // position of last forward slash
            echo "<BR><BR>\$pos of last forward slash: ".$pos;

            $photo_dir = substr($destination, 0, $pos + 1); // include last forward slash
            echo "<BR><BR>\$photo_dir: ".$photo_dir;

            if (!is_dir(UPLOAD_DIR.$photo_dir)){
                mkdir(UPLOAD_DIR.$photo_dir, 0777, true);

                echo "<BR><BR>Made \$photo_dir: ".$photo_dir;
            }

            if (!is_dir(UPLOAD_DIR."big_thumb/".$photo_dir)){
                mkdir(UPLOAD_DIR."big_thumb/".$photo_dir, 0777, true);

                echo "<BR><BR>Made UPLOAD_DIR/big_thumb/\$photo_dir: ".UPLOAD_DIR."big_thumb/".$photo_dir;
            }

            if (!is_dir(UPLOAD_DIR."thumb/".$photo_dir)){
                mkdir(UPLOAD_DIR."thumb/".$photo_dir, 0777, true);

                echo "<BR><BR>Made UPLOAD_DIR/thumb/\$photo_dir: ".UPLOAD_DIR."thumb/".$photo_dir;
            }

            if (!is_dir(UPLOAD_DIR."small_thumb/".$photo_dir)){
                mkdir(UPLOAD_DIR."small_thumb/".$photo_dir, 0777, true);

                echo "<BR><BR>Made UPLOAD_DIR/small_thumb/\$photo_dir: ".UPLOAD_DIR."small_thumb/".$photo_dir;
            }

            $thumbnail_filename = substr($destination, $pos + 1, strlen($destination)); // include last forward slash
            echo "<BR><BR>\$thumbnail_filename: ".$thumbnail_filename;

            // exit;

            // Original Thumbnail
            $thumbnail_path = file_get_contents($photo_CDN_path);

            $photo_filepath       = $destination;
            $big_thumb_filepath   = "big_thumb/".$destination;
            $thumb_filepath       = "thumb/".$destination;
            $small_thumb_filepath = "small_thumb/".$destination;

            // Local dir
            $thumbnail_filepath        = UPLOAD_DIR . $photo_filepath;
            $big_thumbnail_filepath    = UPLOAD_DIR . $big_thumb_filepath;
            $medium_thumbnail_filepath = UPLOAD_DIR . $thumb_filepath;
            $small_thumbnail_filepath  = UPLOAD_DIR . $small_thumb_filepath;

            if (!file_put_contents($thumbnail_filepath, $thumbnail_path)) {
                echo "<BR><BR><b>Error: </b>Could not put original thumbnail file to " . $thumbnail_filepath;
            } else {

                echo "<BR><BR>Put original thumbnail file to " . $thumbnail_filepath;

                // Copy original thumbnail to resize and watermark
                if (copy($thumbnail_filepath, $big_thumbnail_filepath)){ echo "<BR><BR>Copied from ".$thumbnail_filepath." to ".$big_thumbnail_filepath; }
                if (copy($thumbnail_filepath, $medium_thumbnail_filepath)){ echo "<BR><BR>Copied from ".$thumbnail_filepath." to ".$medium_thumbnail_filepath; }
                if (copy($thumbnail_filepath, $small_thumbnail_filepath)){ echo "<BR><BR>Copied from ".$thumbnail_filepath." to ".$small_thumbnail_filepath; }

                //exit;

                // Push images to S3
                if($thumbnail_path!="") {

                    // Create and push to S3: Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)

                    $orig_thumb_array =
                        array(
                        "filepath" => $thumbnail_filepath,
                        "new_width" => NULL,
                        "s3_path" => $slug . "/assets/ufile/big_thumb/" . $photo_filepath,
                        "s3_watermark_path" => $slug . "/assets/ufile/watermark/" . $photo_filepath
                    );

                    $big_thumb_array =
                        array(
                        "filepath" => $big_thumbnail_filepath,
                        "new_width" => 640,
                        "s3_path" => $slug . "/assets/ufile/big_thumb/" . $photo_filepath,
                        "s3_watermark_path" => $slug . "/assets/ufile/watermark/big_thumb/" . $photo_filepath
                    );

                    $med_thumb_array =
                        array(
                        "filepath" => $medium_thumbnail_filepath,
                        "new_width" => 320,
                        "s3_path" => $slug . "/assets/ufile/thumb/" . $photo_filepath,
                        "s3_watermark_path" => $slug . "/assets/ufile/watermark/thumb/" . $photo_filepath
                    );

                    $small_thumb_array =
                        array(
                        "filepath" => $small_thumbnail_filepath,
                        "new_width" => 180,
                        "s3_path" => $slug . "/assets/ufile/small_thumb/" . $photo_filepath,
                        "s3_watermark_path" => $slug . "/assets/ufile/watermark/small_thumb/" . $photo_filepath
                    );

                    $l_sNew_640_Path = resize_image_push_to_S3($big_thumb_array);
                    $l_sNew_320_Path = resize_image_push_to_S3($med_thumb_array);
                    $l_sNew_180_Path = resize_image_push_to_S3($small_thumb_array);

                    if (!empty($l_xResultRow['enr_video'])){

                        // Create and push to S3: Original Watermarked Thumbnail, Big Watermarked Thumbnail (640),
                        //      Medium Watermarked Thumbnail (320),
                        //      Small Watermarked Thumbnail (180)

                        $l_sNew_640_Watermark_Path = watermark_play_button_push_to_s3($big_thumb_array);
                        $l_sNew_320_Watermark_Path = watermark_play_button_push_to_s3($med_thumb_array);
                        $l_sNew_180_Watermark_Path = watermark_play_button_push_to_s3($small_thumb_array);

                    }
                }
            }
        }
        echo "<hr>";
//        exit;
    }
}
?>