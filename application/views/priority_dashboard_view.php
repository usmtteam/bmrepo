<!-- BEGIN PAGE -->
<div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!--<div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Questions</h3>
            </div>
            <div class="modal-body">
                <p>Here will be a configuration form</p>
            </div>
        </div>-->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE CONTAINER-->            
        <div class="container-fluid" id="dashboard">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                    <div class="page-title ">
                        Priority Ordering
                        <small></small>
                         
                    </div>
                    <?php // echo $message;?>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                    
                    <div class="btn-group pull-right">
                                    <a id="sample_editable_1_new" href="<?php echo site_url("dashboard/index"); ?>" class="btn">
                                    Return to Channel List Channel 
                                    </a>
                                </div>
                </div>
                
            </div>
            <!-- END PAGE HEADER-->
            
            <ul id="sortable">

            <?php foreach($chanels as $chanel): 
                if ($chanel->is_oflo =='y' && $chanel->priority !== '0'){

                    ?>
                
              <li class="priority_listing" id="<?php echo $chanel->id; ?>"><span class="drag"></span><?php echo $chanel->name."-------> Priority ".$chanel->priority; ?>
            
                <div id="ck-button">
                <label>
                  <input type="checkbox" name="priorityDeactivate" class="priorityDeactivate" value="<?php echo $chanel->id; ?>" /><span class="deactivate">Deactivate</span><span style='display:none' class='channel_priority_num'><?php echo $chanel->priority; ?></span>
               </label>
               </div>
              </li>
       
             

             <?php } endforeach; ?>
            </ul>

             <ul id="sortable2">

            <?php foreach($chanels as $chanel): 
                if ($chanel->is_oflo =='y' && $chanel->priority == '0'){

                    ?>
                
              <li class="priority_listing2"><span class="drag"></span>
            
              <?php echo $chanel->name." "."-------> No Priority Given"; ?>
            
             <div id="ck-button1">
                <label>
                  <input type="checkbox" name="priorityActivate" class="priorityActivate" value="<?php echo $chanel->id; ?>" /><span class="activate">Activate</span>
               </label>
               </div>
           

              </li>
        
             
             <?php } endforeach; ?>
            </ul>
            
        </div>
</div>

<script type="text/javascript">
    
    $(function() {
        $('#sortable').sortable({
            opacity: 0.7,
            handle: 'span',
            update: function(event, ui) {
             
                var list_sortable = $(this).sortable('toArray').toString();
                // $('#priority_list').html(list_sortable);

                // change order in the database using Ajax
                $.ajax({
                    url: 'priority_update',
                    type: 'POST',
                    data: {list_order:list_sortable},
                    // success: function(data) {
                    //     //finished
                    //     console.log(data);
                    // }
                });
            }


        }); // fin sortable

        $('.priorityActivate').click(function(){
            
            var data = $(this).val();

            if (this.checked) {
            
                $.ajax({
                    url: 'priority_activated',
                    type: 'POST',
                    data: {list_order:data},
                    // success: function(data) {
                    //     //finished
                    //     console.log(data);
                    // }
                });

            }else {

                 $.ajax({
                    url: 'priority_deactivated',
                    type: 'POST',
                    data: {list_order:data},
                    // success: function(data) {
                    //     //finished
                    //     console.log(data);
                    // }
                });
            }
        })


        $('.priorityDeactivate').click(function(){
            
            var data = $(this).val();

            var curSpan = $(this).parent().children('.channel_priority_num').get(0);
            var spansText = curSpan.innerText;

            if (this.checked) {
                
                $.ajax({
                    url: 'priority_deactivated',
                    type: 'POST',
                    data: {list_order:data},
                    // success: function(data) {
                    //     //finished
                    //     console.log(data);
                    // }
                });
          
            }
            // else {

            //      $.ajax({
            //         url: 'priority_keep',
            //         type: 'POST',
            //         data: {list_order:data},
            //         data: {original_num:spansText},
            //         success: function(data) {
            //             //finished
            //             console.log(data);
            //         }
            //     });
            // }
        })

});
    

</script>
