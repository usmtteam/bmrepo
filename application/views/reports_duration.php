
<!-- BEGIN PAGE -->
<?php  setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>

<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Report Manager Duration

                    <a href="<?php echo site_url('reports_master/index');?>" id="views_duration" class="views_duration" >Switch To Views</a>


                </div>
         
                    <?php 
                    echo form_open_multipart("reports_master/result_duration", "id ='myform'"); 


                  $months= array(
                    'All',
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July ',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December',
                    'By_Months'
                ); 

                 ?>
                <div class="overAll">
                    <select name="over_all" id="over_all">
                      <?php foreach($months as $month): ?>
                    
                      <option value="<?php echo strtolower($month); ?>" <?php if (strtolower($month)==$month_match) { ?>selected="selected"<?php } ?> >
                        <?php echo $month; ; ?>
                      </option>

                      <?php endforeach; ?> 
                    </select>
                  </div>
               
               <div class="channelAll">
                     
                    <select  name="channel_all" id="channel_all">
        
                      <?php foreach($channel_names as $names): ?>

                      <option value="<?php echo $names['id']; ?>" <?php if ($names['id']== $channel_id) { ?>selected="selected"<?php } ?> >
                        <?php echo $names['name']; ?>
                       
                      </option>

                      <?php endforeach; ?> 

                    </select>
                  </div>
                
                 <div  id="premium">
                    
                    <label><input name="premiumCheckbox" type="checkbox" <?php  if (isset($_POST['premiumCheckbox'])) {echo 'checked="checked"';} ?>/>Premium Only?</label>
                </div>

                        <?php echo form_submit('submit', "Submit",' name="submit", class="btn blue"');

                        echo form_submit('submit', "Download",' name="submit", class="btn blue"');

                         echo form_close();
              
                
                ?>
                 
                       
                </div>
      
             
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class='duration'>Content</th>
                            <th class='duration'>Duration</th>
                            <th class='duration'>Revenue</th>
                            
                        </tr>
                    </thead>
                    <tbody id="reportBody">  
                        
                        <tr>

                            <td>
                            <p><strong>Total</strong></p>
                          
                           
                            </td>
                  
                            <td>
                           <strong><?php echo gmdate("H:i:s", $duration); ?></strong>
                          
                           
                            </td>
                        
                            <td>
                             <strong>

                             <?php echo money_format('%.2n',$revenue) . "\n"; ?> </strong>
                          
                           
                            </td>
                        </tr>

                         <?php 

                                  foreach ($report_by_channel as $report_channel){

                                    $timeStamp_number = date( 'm', $report_channel['time_stamp'] );

                                    $date = date_parse($month_match);
                                    $month_number = $date['month'];

                                    if($month_match == 'all'){
                                      $duration_is = $report_channel['total_duration'];
                                    }else{
                                      if ($month_number == $timeStamp_number){
                                      $duration_is = $report_channel['total_duration'];
                                      }else {
                                      $duration_is = 0;
                                      }
                                    }


                               ?>
                         <?php 
                    echo form_open_multipart("reports_master/result_duration", "id ='myform'");
                    ?>
                                <tr>
                                   
                                   <? if ($duration_is >0 ) {?>

                                    <td>
                                   
                                     <?php 

                                      if($month_match == 'by_months'){

                                        echo strtoupper($report_channel['month']); 

                                      }else{
                                        if (!$report_channel['description']){
                                          echo "<strike>Deleted Content</strike>";
                                        }else {
                                          echo $report_channel['description']; 

                                        } 

                                      }

                                     ?>

                                    </td>
                          
                                    <td>
                                 
                                  <input type="text" id="duration_is" name="duration_is" value="<?php echo gmdate("H:i:s", $duration_is); ?>">
                                   
                                  <input style="display:none"type="text" id="video_id" name="video_id" value="<?=$report_channel['id']; ?>">

                                   <input style="display:none"type="text" id="cid" name="cid" value="<?=$report_channel['cid']; ?>">

                                   <input style="display:none"type="text" id="month_m" name="month_m" value="<?=$month_match; ?>">
                                  <?

                                 echo form_submit('submit', "Update",' name="submit", class="btn blue"');

                                  ?>
                       
                    
                                  
                                   
                                    </td>
                                   

                                    <td>


                                   <?php 

                                   $total_revenue = $duration_is*3/2;

                                   echo money_format('%.2n',$total_revenue) . "\n"; ?>
                                  
                                   
                                    </td>

                                
                                </tr>
                                
                               <?php   echo form_close(); }} ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- <script type="text/javascript">



$('.update').click(function() { 
 
    $.ajax({
        url: 'result_duration',
        type: 'POST'
       
        }
    });  

});
    

</script> -->
