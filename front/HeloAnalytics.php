<?php

    function getService()
    {
      // Creates and returns the Analytics service object.

      // Load the Google API PHP Client Library.
     require_once realpath(dirname(__FILE__).'/include/lib/google-api-php-client/src/Google/autoload.php');

      // Use the developers console and replace the values with your
      // service account email, and relative location of your key file.
      $service_account_email = '1086299963281-3hkvof7kbufpboomjjo3s6lmp494qkb1@developer.gserviceaccount.com';
      $key_file_location = realpath(dirname(__FILE__).'/include/lib/google-api-php-client/Stars-5b7bf05ede77.p12');

      // Create and configure a new client object.
      $client = new Google_Client();
      $client->setApplicationName("Starsite");
      $analytics = new Google_Service_Analytics($client);

      $client->addScope("http://www.googleapis.com/auth/analytics.readonly");
      $client->addScope("http://www.googleapis.com/auth/analytics");

      // Read the generated client_secrets.p12 key.
      $key = file_get_contents($key_file_location);
      $cred = new Google_Auth_AssertionCredentials(
          $service_account_email,
          array(Google_Service_Analytics::ANALYTICS_READONLY),
          $key
      );
      $client->setAssertionCredentials($cred);
      if($client->getAuth()->isAccessTokenExpired()) {
        try {
          $client->getAuth()->refreshTokenWithAssertion($cred);
        } catch (Exception $e) {
          var_dump($e->getMessage());
        }

      }

     if($client->isAccessTokenExpired()) {
          $client->refreshToken('refresh-token');
      }

      //$_SESSION['service_token'] = $client->getAccessToken();


      

      return $analytics;
    }

       function createNewProperty(&$accountId, $url, $name, &$analytics){

        try {
          $property = new Google_Service_Analytics_Webproperty();
          $property->setName($name);
          $property->setWebsiteUrl($url);
          $analytics->management_webproperties->insert($accountId, $property);


        } catch (apiServiceException $e) {
          print 'There was an Analytics API service error '
              . $e->getCode() . ':' . $e->getMessage();
        } catch (apiException $e) {
          print 'There was a general API error '
              . $e->getCode() . ':' . $e->getMessage();
        } 

        return $property;


    }

    function createNewProfile($accountId, $url, $pid, &$analytics){

      $profile = new Google_Service_Analytics_Profile();
      $profile->setName('All Website Data');
      $profile->setWebsiteUrl($url);

      try {
        $analytics->management_profiles->insert($accountId, $pid, $profile);

      } catch (apiServiceException $e) {
        print 'There was an Analytics API service error '
            . $e->getCode() . ':' . $e->getMessage();

      } catch (apiException $e) {
        print 'There was a general API error '
            . $e->getCode() . ':' . $e->getMessage();
      }

      $profileID = $profile->id;

      return $profile;

      }

    function getIDfromlistProperties($accountId, $url, &$analytics){


      try {
      $properties = $analytics->management_webproperties
          ->listManagementWebproperties($accountId);

    } catch (apiServiceException $e) {
      print 'There was an Analytics API service error '
          . $e->getCode() . ':' . $e->getMessage();

    } catch (apiException $e) {
      print 'There was a general API error '
          . $e->getCode() . ':' . $e->getMessage();
    }
    $theurl = $url;
  
    foreach ($properties->getItems() as $property) {

      $checkurl = $property->getWebsiteUrl();

    if($theurl == $checkurl){
     
    $propertyId = $property->getId();
    }

    }


      return $propertyId;

    }

    function getIDfromlistProfiles($accountId, &$propertyID, &$analytics){

      try {
      $profiles = $analytics->management_profiles
          ->listManagementProfiles($accountId, $propertyID);

    } catch (apiServiceException $e) {
      print 'There was an Analytics API service error '
          . $e->getCode() . ':' . $e->getMessage();

    } catch (apiException $e) {
      print 'There was a general API error '
          . $e->getCode() . ':' . $e->getMessage();
    }

    foreach ($profiles->getItems() as $profile) {

    if($propertyID == $profile->getWebPropertyId()){
    $profileId = $profile->getId();
    }
    }

    return $profileId;

    }

    function getnumberOfProperties($accountId, $url, &$analytics){


      try {
      $properties = $analytics->management_webproperties
          ->listManagementWebproperties($accountId);

    } catch (apiServiceException $e) {
      print 'There was an Analytics API service error '
          . $e->getCode() . ':' . $e->getMessage();

    } catch (apiException $e) {
      print 'There was a general API error '
          . $e->getCode() . ':' . $e->getMessage();
    }

    return $properties['totalResults'];  

  }

  function sendEmail($accountId, $remainingGaAccouts){


    $to      = 'bryan23s@gmail.com, vincent@starclubltd.com';
    $subject = 'IMPORTANT - Only '.$remainingGaAccouts.' GA accounts remaining for account '.$accountId;
    $message = "This is just a reminder that you currently have ".$remainingGaAccouts." remaining accounts left for GA account #".$accountId.".  Please remember to add a new account and update id";
    $headers = 'From: admin@starsite.com' . "\r\n" .
        'Reply-To: admin@starsite.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);


  }

    function linkProfileToProperty($accountId, $pid, $profileID, &$analytics){

      try {
      $property = $analytics->management_webproperties->get($accountId, $pid);

      } catch (apiServiceException $e) {
        print 'There was an Analytics API service error '
            . $e->getCode() . ':' . $e->getMessage();

      } catch (apiException $e) {
        print 'There was a general API error '
            . $e->getCode() . ':' . $e->getMessage();
      }


      $property->setDefaultProfileId($profileID);


      try {
        $analytics->management_webproperties->update($accountId, $pid,
            $property);
      } catch (apiServiceException $e) {
        print 'There was an Analytics API service error '
            . $e->getCode() . ':' . $e->getMessage();
      } catch (apiException $e) {
        print 'There was a general API error '
            . $e->getCode() . ':' . $e->getMessage();
      }

      
    }


    function getFirstprofileId(&$analytics) {
      // Get the user's first view (profile) ID.

      // Get the list of accounts for the authorized user.
      $accounts = $analytics->management_accounts->listManagementAccounts();

      if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
            ->listManagementWebproperties($firstAccountId);

        if (count($properties->getItems()) > 0) {
          $items = $properties->getItems();
          $firstPropertyId = $items[0]->getId();

          // Get the list of views (profiles) for the authorized user.
          $profiles = $analytics->management_profiles
              ->listManagementProfiles($firstAccountId, $firstPropertyId);

          if (count($profiles->getItems()) > 0) {
            $items = $profiles->getItems();

            // Return the first view (profile) ID.
            return $items[0]->getId();

          } else {
            throw new Exception('No views (profiles) found for this user.');
          }
        } else {
          throw new Exception('No properties found for this user.');
        }
      } else {
        throw new Exception('No accounts found for this user.');
      }
    }

    function getResults(&$analytics, $profileId) {
      // Calls the Core Reporting API and queries for the number of sessions
      // for the last seven days.
       return $analytics->data_ga->get(
           'ga:' . $profileId,
           '7daysAgo',
           'today',
           'ga:sessions');
    }

    function printResults(&$results) {
      // Parses the response from the Core Reporting API and prints
      // the profile name and total sessions.
      if (count($results->getRows()) > 0) {

        // Get the profile name.
        $profileName = $results->getProfileInfo()->getProfileName();

        // Get the entry for the first entry in the first row.
        $rows = $results->getRows();
        $sessions = $rows[0][0];

        // Print the results.
        print "First view (profile) found: $profileName\n";
        print "Total sessions: $sessions\n";
      } else {
        print "No results found.\n";
      }
    }

    function queryCoreReportingApi($tableId, &$analytics, $start_date, $end_date, $videoId, $fields) {

    $filter = "ga:eventCategory==$videoId;ga:eventAction==$fields";

    $optParams = array(
        'dimensions' => 'ga:eventCategory, ga:eventAction, ga:eventLabel',
        'metrics' => 'ga:sessions',
        'filters' => $filter,
        'max-results' => '5000');

    return $analytics->data_ga->get(
        $tableId,
        $start_date,
        $end_date,
        'ga:events',
        $optParams);
  }

  function calcTotals($data){
     
    $total=0;
    $i = 0;
   
    foreach($data['rows'] as $value){

      $total +=$value[3];
  
      $i++;
    }

    return $total;
  }

  function queryCoreChannelReportingApi($tableId, &$analytics, $start_date, $end_date, $fields) {

    $filter = "ga:eventAction==$fields";

    $optParams = array(
        'dimensions' => 'ga:eventCategory',
        'metrics' => 'ga:totalEvents',
        'filters' => $filter,
        'max-results' => '5000');

    return $analytics->data_ga->get(
        $tableId,
        $start_date,
        $end_date,
        'ga:events',
        $optParams);
  }

  function calcChannelTotals($data){
     
    $total=0;
    $i=0;
    var_dump($data[1]);
    while($data['rows'][$i][1] !=""){

      $total +=$data['rows'][$i][3];
      $i++;
    }

    return $total;
  }





    

    $videoId = "1208";

    $i = 0;
    $tableId = "ga:102648882";
    $analytics = getService();
    $fields = "Video-View";
    $start_date = "2015-06-01";
    $end_date = "2015-07-27";


    $data = queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$fields);
 
    
              
              $video_view = calcTotals($data);

    $video_error = 0;
    $video_finished = 0;
    $video_played = 0;
  
    $ad_completed = 0;
    $ad_error = 0;
    $ad_started = 0;
    $ad_expanded_changed = 0;
    $ad_impression = 0;
    $ad_loaded = 0;
    $count=0;

   
  

    echo "<br><br><br><br>";
    echo "Video ID ".$videoId."<br>";
    echo "Total Video Impressions: ".$video_played." Impressions<br>";
    echo "Total Video Views: ".$video_view." Views<br>";
    echo "Total Video Finished: ".$video_finished." Finished<br>";
    echo "Total Video Errors: ".$video_error." Errors<br>";
  
    echo "<br><br>";

    echo "Ads: <br>";
    echo "Total Ads Completed: ".$ad_completed." Completed<br>";
    echo "Total Ads Started: ".$ad_started." Started<br>";
    echo "Total Ad Errors: ".$ad_error." Errors<br>";
    echo "Total Ads Expanded/Changed: ".$ad_expanded_changed." Expanded/Changed<br>";
    echo "Total Ad Impressions: ".$ad_impression." Impression<br>";
    echo "Total Ads Loaded: ".$ad_loaded." Loaded<br>";


    echo "<br><br><br><br>";
    




//$property = createNewProperty($accountId, $url, $name, $analytics);
//$propertyID = getIDfromlistProperties($accountId, $url, $analytics);
//$profile = createNewProfile($accountId, $url, $propertyID, $analytics);
//$profileID = getIDfromlistProfiles($accountId,$propertyID, $analytics);
//$linked =linkProfileToProperty($accountId, $propertyID, $profileID, $analytics);



?>
