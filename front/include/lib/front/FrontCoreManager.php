<?php


	class FrontCoreManager{
		
		private static $_xInstance = NULL;
		private $_xSqlManager;
 	
		function __construct(){
			$this->init();
		}
		
		function init(){ 
			$this->_xSqlManager = new FrontCoreSql();
		}		
		
		static public function getInstance(){
		    if(self::$_xInstance == NULL) self::$_xInstance = new FrontCoreManager();
	        return self::$_xInstance;
	    } 
		public function getAssetById($p_nId){		
			return $this->_xSqlManager->getAssetById($p_nId);	
		}

	}
	 
?>