enriqueApp.directive('attronoff', function() {
	return {
		link: function($scope, $element, $attrs) {
			$scope.$watch(
                function () { return $element.attr('data-attr-on'); },
                function (newVal) { 
                	var attr = $element.attr('data-attr-name');
                	
                	if(!eval(newVal)) {
                		$element.removeAttr(attr);
                	}
                	else {
                		$element.attr(attr, attr);
                	}
                }
            );
		}
	};
});