<!DOCTYPE html>
<html lang="en">
<head>

    <?

    $l_bIsMobile = false;
    if($l_nDeviceType == DEVICE_TYPE_MOBILE){
        $l_bIsMobile = true;
        $l_aZoneUnits = array('ZONE_1'=>11,'ZONE_2'=>12,'ZONE_3'=>13);
        $l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=33';
    }else{
        $l_aZoneUnits = array('ZONE_1'=>16,'ZONE_2'=>1,'ZONE_3'=>2);
        $l_sAdServerURL = 'https://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=32';

    }

    // $l_sAdServerURL = 'http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=229&LR_SCHEMA=vast2';
    /*$l_sMetaTitle = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
    $l_sMetaTitle = preg_replace('/[\r\n\t]+/',"",$l_sMetaTitle);
    $l_sMetaTitle = preg_replace("/[^!'#a-zA-Z0-9_.-\s]/","",$l_sMetaTitle);
    $l_sMetaDescription = $l_sMetaTitle; */

    $l_sMetaTitle = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
    $l_sMetaDescription = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
 
    ?>
    <meta charset="utf-8" />
    <title><?php if($l_xChannel){ echo $l_xChannel->site_title; } else { echo "Breaking Movement"; }?></title>
    <meta content="width=device-width, initial-scale=-100%, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <? /*<meta name="apple-itunes-app" content="app-id=app-id=1058980361, app-argument=http://oflo.co/"> */ ?>

    
    <link rel="shortcut icon" href="<?= STATIC_FILES_HOST ?>/images/favicon.png" type="image/x-icon"/>

<!--    
    <link href="<?= STATIC_FILES_HOST ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   
    <link href="<?= STATIC_FILES_HOST ?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>

<link href="<?= STATIC_FILES_HOST ?>/css/runtime/head-runtime.css" rel="stylesheet" type="text/css"/>
-->



     <link href="<?= STATIC_FILES_HOST ?>/css/runtime/runtime.css" rel="stylesheet" type="text/css"/> 


    <style type="text/css">
        @font-face {
        font-family: 'starsitefont';
        src:url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.eot?e5v9gc');
        src:url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.eot?#iefixe5v9gc') format('embedded-opentype'),
        url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.woff2?e5v9gc') format('woff2'),
        url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.ttf?e5v9gc') format('truetype'),
        url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.woff?e5v9gc') format('woff'),
        url('<?=STATIC_FILES_HOST ?>/assets/fonts/starsitefont.svg?e5v9gc#starsitefont') format('svg');
        font-weight: normal;
        font-style: normal;
        }

        
        @font-face {
    font-family: 'abelregular';
    src: url('<?=STATIC_FILES_HOST ?>/fonts/abel-regular-webfont.woff2') format('woff2'),
         url('<?=STATIC_FILES_HOST ?>/fonts/abel-regular-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}

        @font-face {
            font-family: 'Dosis';
            src: url('<?=STATIC_FILES_HOST ?>/fonts/dosis-regular-webfont.woff2') format('woff2'),
                 url('<?=STATIC_FILES_HOST ?>/fonts/dosis-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;

        }

        /*@font-face {
            font-family: 'dosislight';
            src: url('<?=STATIC_FILES_HOST ?>/assets/fonts/dosis-light-webfont.woff2') format('woff2'),
                 url('<?=STATIC_FILES_HOST ?>/assets/fonts/dosis-light-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;

        }*/
        

        .ssfont{font-family:'starsitefont';}
        .menu{font-family:'dosisbold';}

        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('<?=STATIC_FILES_HOST ?>/assets/fonts/glyphicons-halflings-regular.eot');
            src: url('<?=STATIC_FILES_HOST ?>/assets/fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),
            url('<?=STATIC_FILES_HOST ?>/fonts/glyphicons-halflings-regular.woff2') format('woff2'),
            url('<?=STATIC_FILES_HOST ?>/fonts/glyphicons-halflings-regular.woff') format('woff'),
            url('<?=STATIC_FILES_HOST ?>/fonts/glyphicons-halflings-regular.ttf') format('truetype'),
            url('<?=STATIC_FILES_HOST ?>/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }
    </style>

    <script async src="<?= STATIC_FILES_HOST ?>/scripts/runtime/head-runtime.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
    <script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/jquery/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-2.0.3.min.js"></script>


    <script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= STATIC_FILES_HOST ?>/assets/scripts/xStarsite.js?v=<?= RELEASE_VERSION ?>"></script>
    <script type="text/javascript" src="<?= STATIC_FILES_HOST ?>/assets/scripts/xUtils.js?v=<?= RELEASE_VERSION ?>"></script>



    <script src="<?= STATIC_FILES_HOST ?>/scripts/runtime/content-runtime.js"></script>


    <link href="<?= STATIC_FILES_HOST ?>/css/runtime/content-runtime.css" rel="stylesheet" type="text/css"/>
    -->


 <!--   <link href="//vjs.zencdn.net/5.4.6/video-js.css" rel="stylesheet">

    <link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/assets/videojs-ima-master-5.3/src/videojs.ima.css" />
    <link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/assets/videojs-resolution-switcher-master-0.4.2/lib/videojs-resolution-switcher.css" />

-->

     

       <link href="<?=STATIC_FILES_HOST?>/css/oflo.css" rel="stylesheet" type="text/css"/> 






    <? if($l_xAssetResult['cid'] != 86){ ?>
        <script async type='text/javascript' src='//scads.starsite.com/www/delivery/spcjs.php?id=1'></script>
    <? } ?>

    <script type="text/javascript">

        var l_nCurrentAssetId = '<?= $l_nAssetId ?>';
        var l_sCurrentAssetType = '<?= $l_sAssetType ?>';
        var l_sCurrentAssetTypeId = '<?= $l_sAssetTypeId ?>';
        var l_nU = '<?= $l_nCurrentUserId ?>';
        var l_bLoggedIn = <?= $l_bUserLoggedIn ? 'true' : 'false'; ?>;
        var l_nChannelId = '<?= $l_xChannel->cid ?>';

    </script>

    <style type="text/css">

        <?php if ($l_nDeviceType == DEVICE_TYPE_MOBILE && preg_match("/android/i", $Device_Make)) {?>

            /* Android preroll fix*/
            #ima-ad-container > div:nth-child(1) { position: inherit !important; }

        <?php } ?>

        <?php if ($l_nDeviceType == DEVICE_TYPE_MOBILE && preg_match("/android/i", $Device_Make) == 0){ ?>

            /* Overlay videojs button on top of iPhone play button */
            .video-js .vjs-tech {
                z-index: 9999;
            }

        <? } ?>

    </style>

    <meta property="og:title" content="<?= substr($l_sMetaTitle, 0, 159) ?>" />
    <meta property="og:description" content="<?= substr($l_sMetaDescription, 0, 159) ?>"/>

    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
    <meta property="og:image" content="<?= $l_sImageUrl ?>" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />


    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Starsitenetwork" />
    <meta name="twitter:title" content="Breaking Muscle" />
    <meta name="twitter:description" content="<?= substr($l_sMetaDescription, 0, 159) ?>" />
    <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
    <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />

    


    <?

    $l_sBackgroundImage = strlen($l_xChannel->backgroundImage) > 2 ? '<img class="channel-banner-height" src="'.CDN_FRONT.'/'.$l_xChannel->url."/images/bg-sn.jpg?v=".RELEASE_VERSION.'" width="100%" />' : '';
   

    ?>
<script async src="https://starclub.ep.livefyre.com/media/c3RhcmNsdWIuZXAubGl2ZWZ5cmUuY29t/javascripts/customprofiles.js"></script>
<script async src="//cdn.livefyre.com/Livefyre.js"></script>

<? if($l_nDeviceType!=DEVICE_TYPE_MOBILE){ ?>
<style>
header.fyre-cp-modal-header {
    background-image: -moz-linear-gradient(90deg, #454545 0%, dimgrey 100%);
    border-bottom: 1px solid #333;
    border-top: 1px solid #606060;
    color: #fff;
    font-size: 18px;
    height: 40px;
    padding: 10px;
}

.fyre-cp-local-login { width:350px; }
#janrainEngageEmbed{ height:176px;}
.janrainSlideControl{ display:none;}
.janrainContent{ height:176px;}
*:focus, * {
    outline: none;
     border-color: inherit;
  -webkit-box-shadow: none;
  box-shadow: none;
}
.ch-name{
    color: #fff;
    position: absolute;
    left: 10px;
    text-shadow: 2px 2px 5px black;
    top: 165px;
    z-index: 9999;
    font-weight:bold;
}
.pos-rel{
    position:relative;
}
</style>
<? }else{ ?>
<style>
.ch-name{
    color: #fff;
    position: absolute;
    left: 10px;
    text-shadow: 2px 2px 5px black;
    top: 35px;
    z-index: 9999;
    font-weight:bold;
    font-size:19px;
}
.pos-rel{
    position:relative;
}

@media (min-width: 479px) {
.ch-name{
    
    top: 160px;
    font-size:40px;
  
}

#_selz-sprite{ display:block; }

    }

</style>
<? } ?>


</head>