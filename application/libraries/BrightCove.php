<?php 
class Brightcove {
        public $token_read = '';
        public $token_write = '';
        public $read_url = 'http://api.brightcove.com/services/library?';
        public $write_url = 'http://api.brightcove.com/services/post';

	public function __construct() {
		$this->token_read = BRIGHTCOVE_READ_API_KEY;
		$this->token_write = BRIGHTCOVE_WRITE_API_KEY;

	}

	public function createVideo($file = NULL, $meta) {
		$request = array();
		$post = array();
		$params = array();
		$video = array();


		foreach($meta as $key => $value) {
			$video[$key] = $value;
		}
		$params['token'] = $this->token_write;
		$params['video'] = $video;
		$params['encode_to'] = 'MP4';
		$params['create_multiple_renditions'] = 'True';
		$post['params'] = $params;
		$post['method'] = 'create_video';

		$request['json'] = json_encode($post);

		if($file) {
			$request['file'] = '@' . $file;
		}

		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);

		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);
		
		// Check request error code and re-call createVideo if request
                // returned a 213 error. A 213 error occurs when you have 
                // exceeded your allowed number of concurrent write requests

		if(isset($json->error))	{
			if($json->error->code == 213) {
				return $this->createVideo($file, $meta);
			} else {
				return FALSE;
			}
		} else {
			return $response;
		}
	}
	
	public function getVideoStatus($video_id) {

		$request = array();
		$post = array();
		$params = array();

		$params['video_id'] = $video_id;
		$params['token'] = $this->token_write;
                $post['method'] = 'get_upload_status';
		$post['params'] = $params;


		$request['json'] = json_encode($post);




//		$url = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $video_id . '&video_fields=itemState&media_delivery=default&token='. $this->token_write;

		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);
		
		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);
		
		if($json->result == 'COMPLETE') return 'ACTIVE';
		
		return '';
	}
	
	public function addOverlayLogo($video_id, $remote_id,$tooltip,$linkurl) {
		$request = array();
		$post = array();
		$params = array();
		
		$params['token'] = $this->token_write;
		$params['logooverlay'] = array(
                    

                   
                        'tooltip' => $tooltip,
                        'linkURL' => $linkurl,
                            'image' => array(
				'type' => 'LOGO_OVERLAY',
                                'resize' => 'false',
                  //              'displayName' => $displayname,
                                'id' => $remote_id
		//		'remoteUrl' => $remote_url
			),
 

			'alignment' => 'BOTTOM_RIGHT'
 

		);
		$params['video_id'] = $video_id;
		$post['params'] = $params;
		$post['method'] = 'add_logo_overlay';
		$request['json'] = json_encode($post);
		
		if($file) {
			$request['file'] = '@' . $file;
		}
		
		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);
		
		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);
		
		// Check request error code and re-call createVideo if request
		// returned a 213 error. A 213 error occurs when you have
		// exceeded your allowed number of concurrent write requests
		if(isset($json->error))	{
				return FALSE;
		} else {
			return $response;
		}
	}
}