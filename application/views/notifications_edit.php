<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Edit Push Notification


                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Edit Push Notification Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
               
                    <?php 
                    echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("message");
                        echo form_error("title");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <div class="row-fluid">
                        <div class="control-group">

                        <form>

             
                        
                          <label for="platform">Platform</label>
                         
                          <select id="platform_id" name="platform_id">
                              <?php
                                foreach($platform_names as $name) { ?>
                                  <option value="<?= $name['id'] ?>"  
                                    <? if ($platform_id == $name['id']) { echo ' selected="selected"'; } ?> 
                                   >

                                    <?= $name['name'] ?>

                                    </option>
                              <?php
                                } ?>
                          </select>
                        
                 
                    
                        <br><br>

                 
                          <label for="recipient">Recipient</label>
                          <select id="recipient" name="recipients">
                               <?php foreach($push_recipients as $recipient): ?>

                            <option value="<?=$recipient['name']?>"
                                <? if ($recipients == $recipient['name']) { echo ' selected="selected"'; } ?> 
                            >

                            <?=$recipient['name']?></option>

                        <?php endforeach; ?>
              
                          </select>

                            <br><br>

                             <div id="followers_div">
                       
                          <label for="followers">Followers of Channel?</label>
                          <select id ="followers" name="followers">
                             
                             <?php foreach($oflo_channels as $follows): ?>

                             <option value="<?=$follows['cid']?>"
                                <? if ($followers == $follows['cid']) { echo ' selected="selected"'; } ?> 
                            >

                            <?=$follows['name']?></option>

                    
                               
                                     
                           <?php endforeach; ?>
                      
              
                          </select>
                        
                      </div>


                        
                        
                    
                         <br><br>
                        <div class="form-control" id="channel_recipients">
                          <label for="channel_recipients">Breaking Movement Channels</label>
                           <input type="checkbox" id="selectallchannel"> Select All<br>
                           <br>
                               <?php foreach($oflo_channels as $channels): ?>


                            <input type="checkbox" name="channel_recipients[]" id="channel_recipients" value="<?=$channels['cid']?>"
                                <? if ($channel_recipients == $channels['cid']) { echo ' checked="checked"'; } ?> >
                            

                             <?=$channels['name']?><br>


                        <?php endforeach; ?>

              
                     
                        
                        </div>
                        
                          <br>

                          <div class="form-control" id="user_recipients">
                          <label for="user_recipients">Breaking Movement Users</label>
                          
                          <input type="checkbox" id="selectalluser"> Select All<br>
                           <br>
                                 <?php foreach($oflo_users as $users): ?>


                            <input type="checkbox" class="checkbox1" name="user_recipients[]" id="user_recipients" value="<?=$users['cid']?>"
                                <? if ($user_recipients == $users['cid']) { echo ' checked="checked"'; } ?> >
                            

                             <?=$users['username']?><br>


                        <?php endforeach; ?>

              
                     
                        
                        </div>
                        
                          <br>
                        <div class=form-control>
                            <label for="pushTitle">Subject</label>
                            <input type="text" name="title" id="title" required value="<?php echo $title; ?>"><br>
                        
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="pushMessage">Message</label>
                            <div class="input-group">
                              <textarea name="message" id="pushMessage" class="form-control" rows="5" required><?php echo $message; ?></textarea>
                            </div>
                        </div>
                      
                    <!--   <br>
                      <div class="control-group">
                               
                                <div class="span4">
                             <label>Date</label>
                   
                                 
                                  <div id="date" class="input-append"> -->
                            <input type="hidden" id="date" name="date" value="<?php 
                                        date_default_timezone_set('America/Los_Angeles');
                                        
                                        echo date("Y-m-d G:i:s"); ?>" readonly />
                                  <!-- <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                        
                        </div>
                            </div>
                                </div> -->

                       <script language="javascript">
                $(document).ready(function() {
                  $('#date').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});

                  });
              </script>
              
        
                        
                  
                            
                            </div>
                        
                        </div>

                   
                        </div>
                    </div>
                    <div class="form-actions">
                     <?php echo form_submit('submit', "Update",' class="btn blue"');?>

                    </div>
                    <?php echo form_close();?>

                        
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>

<script>



  $(document).ready(function () {
      Platforms(); 

      $("#platform_id, #recipient").change(function () {
          Platforms();
      });

  });

  function Platforms() {

        $("#channel_recipients").hide();
        $('#followers_div').hide();

      


      if ($("#platform_id").val() == "3"){

   


        if($("#recipient").val() == "Channel"){
            $("#user_recipients").hide();

          $("#channel_recipients").show();}
     } else{
          $("#channel_recipients").hide();
      }

  }


  $(document).ready(function () {
      Recipients(); 
   
      $("#platform_id, #recipient").change(function () {
          Recipients();
      });

  });

  function Recipients() {

      if ($("#platform_id").val() == "3"){

        if($("#recipient").val() == "All"){

          $("#channel_recipients").show();
        }
     } else{
          $("#channel_recipients").hide();
      }

     
   }


     $(document).ready(function () {
      Followers(); 
   
      $("#platform_id, #recipient").change(function () {
          Followers();
      });

  });

  function Followers() {

      if ($("#platform_id").val() == "2"){

        if($("#recipient").val() == "Followers"){

          $("#followers_div").show();
        }
     } else{
          $("#followers_div").hide();
      }

     
   }

  $(document).ready(function () {
      Breaking MovementUsers(); 
      $("#platform_id, #recipient").change(function () {
          Breaking MovementUsers();
      });

  });

  function Breaking MovementUsers () {
    
      if ($("#platform_id").val() == "3"){

        if($("#recipient").val() == "All"){

          $("#user_recipients").show();
        }
     } else{
          $("#user_recipients").hide();
      }

     
   }
   

      Breaking MovementUsers1(); 
      $("#platform_id, #recipient").change(function () 
     {
    Breaking MovementUsers1();
      });



  function Breaking MovementUsers1 () {


      if ($("#platform_id").val() == "3"){

        if($("#recipient").val() == "User"){
              $("#channel_recipients").hide();

          $("#user_recipients").show();
        }
     } else{
          $("#user_recipients").hide();
      }

     
   }


      $('#selectallchannel').click(function(event) {  
          if(this.checked) { 
              $('.checkbox').each(function() { 
                  this.checked = true;             
              });
          }else{
              $('.checkbox').each(function() { 
                  this.checked = false;                     
              });        
          }
      });

       $('#selectalluser').click(function(event) {  
          if(this.checked) { 
              $('.checkbox1').each(function() { 
                  this.checked = true;             
              });
          }else{
              $('.checkbox1').each(function() { 
                  this.checked = false;                     
              });        
          }
      });
     
</script>
