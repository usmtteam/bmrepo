<?

	ini_set('memory_limit','1280M');
    ini_set('upload_max_filesize','100M');
    ini_set('max_execution_time',100000);
	require(__DIR__.'/../config/settings.php');	

	$l_xReportsConnection = mysqli_connect($l_aDatabase_Reports['host'],$l_aDatabase_Reports['user'],$l_aDatabase_Reports['pass'],$l_aDatabase_Reports['db']);
	$l_xDevTeamConnection = mysqli_connect($l_aDatabase_DevTeam['host'],$l_aDatabase_DevTeam['user'],$l_aDatabase_DevTeam['pass'],$l_aDatabase_DevTeam['db']);
	
	
	$l_sSql = 'SELECT * FROM enr_channel_oauth WHERE social_property_type_id = 1 AND active = "Y"';/// AND channel_id = 55
	
	$l_xResult = mysqli_query($l_xDevTeamConnection,$l_sSql);

	$l_aResultsMain = array();
	while ($l_xObj = mysqli_fetch_array($l_xResult,MYSQLI_ASSOC)) {
		$l_aResultsMain[] = $l_xObj;
	} 

	$l_aReachTotals = array();

	foreach($l_aResultsMain as $l_xResultRow){

		$l_nSiteId = intval($l_xResultRow['channel_id']);

		$l_nPageId = $l_xResultRow['page_id'];
		$l_sPageName = $l_xResultRow['page_name'];
		$l_sPageCategory = $l_xResultRow['page_category'];
		$l_sAccessToken = $l_xResultRow['social_token'];

		if($l_sPageCategory == 'User'){
			continue;
			$l_sFBAPIEndpoint = 'https://graph.facebook.com/v2.3/me/friends/?access_token='.$l_sAccessToken;		
			$l_sData = file_get_contents($l_sFBAPIEndpoint);
			$l_aJSON = json_decode($l_sData); 
			$l_FBData = $l_aJSON->data;
			print_R($l_FBData);
		}

		// Query FB Graph API
		$l_sFBAPIEndpoint = 'https://graph.facebook.com/v2.3/'.$l_nPageId.'/insights/?access_token='.$l_sAccessToken;		
		$l_sData = file_get_contents($l_sFBAPIEndpoint);
		$l_aJSON = json_decode($l_sData); 
		$l_FBData = $l_aJSON->data;
		//Map data to Keys
		$l_aMappedData = array();
		foreach($l_FBData as $l_xRow){
			$l_aMappedData[$l_xRow->name.'::'.$l_xRow->period] = $l_xRow;
		}
		
		// DB NAME    ===  FB Key
		$l_aColumnsToRecord = array();
		$l_aColumnsToRecord['lifetime_total_likes'] = 'page_fans::lifetime';
		$l_aColumnsToRecord['daily_new_likes'] = 'page_fan_adds_unique::day';
		$l_aColumnsToRecord['daily_page_engaged_users'] = 'page_engaged_users::day';
		$l_aColumnsToRecord['daily_total_reach'] = 'page_impressions_unique::day';
		$l_aColumnsToRecord['daily_total_impressions'] = 'page_impressions::day';
		$l_aColumnsToRecord['daily_page_consumptions'] = 'page_consumptions::day';
		$l_aColumnsToRecord['daily_daily_count_of_fans_online'] = 'page_fans_online_per_day::day';

		//$l_aColumnsToRecord['lifetime_total_likes']
		$l_aReachTotals[$l_nSiteId] += $l_aMappedData['page_fans::lifetime']->values[0]->value;
		// print_r($l_aMappedData);
		
		//Starting Date 
		// Assuming we have 3 dates <<< // fix this
		for($i=0; $i<3; $i++){
			$l_xDate = new DateTime($l_aMappedData['page_fans::lifetime']->values[$i]->end_time);
			$l_sNewDate = $l_xDate->format('Y-m-d');
			
			//Check for existing
			$l_sSql = mysqli_query($l_xDevTeamConnection,'SELECT id FROM enr_fbpages_key_metrics WHERE site_id = '.$l_nSiteId.' AND date = "'.$l_sNewDate.'" LIMIT 1');
			$l_xResult = mysqli_fetch_assoc($l_sSql);

			$l_nCountBlock = $i;
			if($l_xResult){
				$l_sSql = 'UPDATE enr_fbpages_key_metrics SET ';
				$l_aUpdates = array();
				foreach($l_aColumnsToRecord as $k=>$v){
					$l_sValue = $l_aMappedData[$v]->values[$l_nCountBlock]->value;
					$l_aUpdates[] = $k.' = "'.$l_sValue.'"';
				}
				$l_sSql .= join(',',$l_aUpdates);
				$l_sSql .= ' WHERE site_id = '.$l_nSiteId.' AND date = "'.$l_sNewDate.'"';
			}else{
				$l_sSql = 'INSERT INTO enr_fbpages_key_metrics (site_id,date,';
				$l_aCols = array_keys($l_aColumnsToRecord);
				$l_sSql .= join(',',$l_aCols).') VALUES ('.$l_nSiteId.',"'.$l_sNewDate.'",';
				$l_aVals = array();
				foreach($l_aColumnsToRecord as $k=>$v){
					$l_sValue = $l_aMappedData[$v]->values[$l_nCountBlock]->value;
					$l_aVals[] = '"'.$l_sValue.'"';
				}
				$l_sSql .= join(',',$l_aVals).')';
			}
			mysqli_query($l_xDevTeamConnection,$l_sSql);
		}		
	}

// print_r($l_aReachTotals);
	foreach($l_aReachTotals as $k => $v){
		if($v < 10)
			continue;
		$l_sSql = 'UPDATE enr_chanel SET total_reach = '.$v.' WHERE id = '.$k;
		mysqli_query($l_xDevTeamConnection,$l_sSql);
		echo $l_sSql;
	}
	


?>