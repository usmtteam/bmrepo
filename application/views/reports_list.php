
<!-- BEGIN PAGE -->
<?php  setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>

<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Report Manager Views 
                    <a href="<?php echo site_url('reports_master/duration');?>" id="views_duration" class="views_duration" >Switch To Duration</a>

                 

                </div>
         
                    <?php 
                    echo form_open_multipart("reports_master/result", "id ='myform'"); 


                  $months= array(
                    'All',
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July ',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December',
                    'By_Months'
                ); 

                 ?>
                <div class="overAll">
                    <select name="over_all" id="over_all">
                      <?php foreach($months as $month): ?>
                    
                      <option value="<?php echo strtolower($month); ?>" <?php if (strtolower($month)==$month_match) { ?>selected="selected"<?php } ?> >
                        <?php echo $month; ; ?>
                      </option>

                      <?php endforeach; ?> 
                    </select>
                  </div>
               
               <div class="channelAll">
                     
                    <select  name="channel_all" id="channel_all">
        
                      <?php foreach($channel_names as $names): ?>

                      <option value="<?php echo $names['id']; ?>" <?php if ($names['id']== $channel_id) { ?>selected="selected"<?php } ?> >
                        <?php echo $names['name']; ?>
                       
                      </option>

                      <?php endforeach; ?> 

                    </select>
                  </div>
                
                 <div  id="premium">
                    
                    <label><input name="premiumCheckbox" type="checkbox" <?php  if (isset($_POST['premiumCheckbox'])) {echo 'checked="checked"';} ?>/>Premium Only?</label>
                </div>

                        <?php echo form_submit('submit', "Submit",' name="submit", class="btn blue"');

                        echo form_submit('submit', "Download",' name="submit", class="btn blue"');

                         echo form_close();
              
                
                ?>
                 
                       
                </div>
      
             
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class='duration'>Content</th>
                        
                            <th class='duration'>Views</th>
                            <th class='duration'>Revenue</th>
                            
                        </tr>
                    </thead>
                    <tbody id="reportBody">  
                        
                        <tr>

                            <td>
                            <p><strong>Total</strong></p>
                          
                           
                            </td>
                  
                            

                            <td>
                           <strong>
                        
                           <?php echo number_format($views); ?></strong>
                          
                           
                            </td>
                           

                            <td>
                             <strong>

                             <?php echo money_format('%.2n',$revenue) . "\n"; ?> </strong>
                          
                           
                            </td>
                        </tr>

                         <?php 

                                  foreach ($report_by_channel as $report_channel){
                               ?>
                        
                                <tr>
                                   

                                    <td>
                                   
                                     <?php 

                                      if($month_match == 'by_months'){

                                        echo strtoupper($report_channel['month']); 

                                      }else{
                                        if (!$report_channel['description']){
                                          echo "<strike>Deleted Content</strike>";
                                        }else {
                                          echo $report_channel['description']; 

                                        } 

                                      }

                                     ?>
                                   
                                    </td>
                          
                                   

                                    <td>
                                   <?php echo number_format($report_channel['total_views']); ?>

                           
                                  
                                   
                                    </td>
                                   

                                    <td>


                                   <?php 

                                   $total_revenue = $report_channel['total_views']/1000*3/2;

                                   echo money_format('%.2n',$total_revenue) . "\n"; ?>
                                  
                                   
                                    </td>

                                
                                </tr>
                                
                               <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
