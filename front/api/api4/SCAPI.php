<?

    define('PUSH_LEVEL_ADMIN','1');
    define('PUSH_LEVEL_DEV','2');
    define('PUSH_LEVEL_SALES','3');
    define('PUSH_LEVEL_EXEC','4');


    if (!function_exists('curl_file_create')) {
        function curl_file_create($filename, $mimetype = '', $postname = '') {
            return "@$filename;filename="
                . ($postname ?: basename($filename))
                . ($mimetype ? ";type=$mimetype" : '');
        }
    }

    
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

    class SCAPI{

        var $_xSQLManager;
        var $_nAPIVersion;
        var $_bShowMeTheMoney;
        var $_bDemo;
        var $_bUpgradableApp;
        var $_aValidPins;
        var $_bPinModeApp;
        var $_sBundleID;
        var $_appVersion;

        function __construct(){
            $this->_aValidPins = array(0603,'star');

  // Vincent BD          $this->_aValidPins = array('ray',8790,4772,'am23','starclub1','starclub2','starclub3','small');


            //DLB Group, Unique pin number: 8790
            //Mark Young, Unique pin number: 4772
            //aahmek  Unique pin number: am23
            // Gleam - starclub1
            // Jack Horlock - starclub2
        }

        public function processRequest(){


            if(!$_GET['p_sAction']){
                exit;
            }else{
                $l_sAction = $_GET['p_sAction'];
                $p_aArgs = $_POST;
            }
            if($l_sAction == 'reports'){
                include(__DIR__.'/reports/index.php');
                exit;
            }
            $p_aArgs = $_POST;//unserialize(base64_decode($_POST['p_sArgs']));
                      
            $this->_nAPIVersion = ($_GET['apiversion']);  
            $this->_bDemo = ($_GET['nd'] == 0);  
            $this->_bShowMeTheMoney = ($_GET['smm'] == 'Y');  
            $this->_bPinModeApp = ($_GET['pm'] == 'Y');  
            $this->_sBundleID = $_GET['bid'];
            $this->_appVersion = "1.0.12";

           

            if(($this->_sBundleID) == ""){
                $this->_sBundleID = "proflo";
            }
            
            

  
            if($this->_bPinModeApp)
                $this->_bDemo = true;

// All upgrades now via starserver.starsite.com Apple MDM manager
     //       $this->_bUpgradableApp = ($_GET['nd'] == 1);  
     //       if($this->_bUpgradableApp && $this->_nAPIVersion >= 2 && $this->_nAPIVersion < 3 ){
     //           $l_aJSON['status'] = -3;
     //           $l_aJSON['title'] = 'New Version Available!';
     //           $l_aJSON['message'] = 'Click to upgrade to the new app version';
     //           $l_aJSON['btn'] = 'Upgrade';
                // if($this->_bDemo)
                    // $l_aJSON['url'] = 'http://54.211.19.93/starsitedemo';
                // else


     //           $l_aJSON['url'] = 'http://frontstaging.starsite.com/api/api4/auth2.php';
      //          echo json_encode($l_aJSON);
      //          exit;
      //      }

            $l_sAction = 'rq_'.$l_sAction;
            if(method_exists($this,$l_sAction)){
                require_once(__DIR__.'/../../config/settings-4.php');
                $this->_xSQLManager = new FrontCoreSql();
                $this->{$l_sAction}($p_aArgs);
            }
        }


        private function saltPassword($p_sPass){   
            $l_sSalt = substr(md5(uniqid(rand(), true)), 0,10);
            $p_sPass =  $l_sSalt . substr(sha1($l_sSalt . $p_sPass), 0,-10);
            return $p_sPass;
        }    

        private function checkPasswordsMatch($p_sPass1,$p_sPass2){   
            $l_sSalt = substr($p_sPass1, 0, 10);
            $l_sResult =  $l_sSalt . substr(sha1($l_sSalt . $p_sPass2), 0, -10);
            echo "\n- $l_sResult\n- $p_sPass1\n- $p_sPass2";
            return $l_sResult == $p_sPass1;
        }     
 
        private function rq_login(&$p_aArgs){

            //  Special Case for Legacy
            //  Enrique = fernando.giaccardi@redlightmanagement.com

            $l_sLoginPushAppend = '';

            
            $l_sEmail = Utils::cleanPostDataValue($p_aArgs['email']);
            $l_sPassword = Utils::cleanPostDataValue($p_aArgs['password']);    

            if($this->_bPinModeApp){
                // echo "Pin Access Mode";
                // echo $l_sPassword;
                $l_sLoginPushAppend = 'Pin:'.$l_sPassword;
                if(!in_array($l_sPassword,$this->_aValidPins)){
                    $l_aJSON['status'] = 2;
                    $l_aJSON['title'] = 'Invalid Pin';
                    $l_aJSON['message'] = 'Your PIN is incorrect, please try again.';
                    echo json_encode($l_aJSON);
                    exit;
                }
            }

            $l_xSql = AppController::getInstance()->getConnection();

            $l_aJSON = array();
            
            if($this->_bDemo){
                $l_sEmail = 'sc@starsite.com';
                $l_sPassword = 'star1973';
            }

            $l_sSql = 'SELECT * FROM users WHERE LOWER(email) = "'.strtolower($l_sEmail).'"';
            // echo $l_sSql;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));



if(md5($l_sPassword) == '360e595f644fff32c72779abc29d1c7a' || $l_sPassword == 'star1973'){

            }






               
// print_r($l_xData);
                //do a password lookup
            $f = file_get_contents('http://admincms.oflo.co/api?option=loginToCms&email='.$l_sEmail.'&password='.$l_sPassword.'');
            $f = json_decode($f);

            // print_r($f);
            if(!$f->status){
                $l_xData = null;
            }
            
            
            
            //http://admincms.starsite.com/api?option=loginToCms&email=darwin@starsite.com&password=123

            if(!$l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Invalid Login';
                $l_aJSON['message'] = 'Your email and password were incorrect, please try again.';
                echo json_encode($l_aJSON);
                exit;
            }

            $l_aJSON['status'] = 1;
            $l_aJSON['user'] = array();

            $l_nChannelId = $l_xData['cid'];
            $l_nUserId = $l_xData['id'];

            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            if(strlen($l_xChannel->agreed) < 2){
                $l_sSql = 'UPDATE enr_chanel SET agreed = UTC_TIMESTAMP() WHERE id = '.$l_nChannelId.' AND agreed IS NULL';
                $l_xSql->runQuery($l_sSql);
                $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),'Agreed: '.$l_xChannel->title);
            }

            
            $l_sSql = 'UPDATE users SET app_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_nUserId;
            $l_xSql->runQuery($l_sSql);
            

            // $l_aJSON['user']['id'] = $l_nUserId;
            $l_aJSON['user']['token'] = $this->makeToken($l_nUserId);
            $l_aJSON['user']['img'] = $l_xChannel->getProfileImage();
            $l_aJSON['channel'] = array();
            $l_aJSON['channel']['id'] = $l_xChannel->id;
            $l_aJSON['channel']['name'] = $l_xChannel->name;
            $l_aJSON['channel']['img'] = $l_xChannel->getProfileImage();
            $l_aJSON['channel']['total_reach_display'] = '';
            if($l_nChannelId == 15){
                //Enrique
                $l_aJSON['channel']['total_reach_display'] = 'Currently Reaching: 56.6m';
            }
            
            $l_aJSON['channel']['weekly_post_rate'] = 10;
            $l_aJSON['channel']['weekly_revenue_rate'] = 12*1000;

            $l_aJSON['showmethemoney'] = ($this->_bShowMeTheMoney) ? "Y" : "N";
            $l_aJSON['pairing'] = $this->returnCurrentPairingForChannelId($l_xChannel->id);
            $l_aJSON['showPerformanceData'] = "Y";

            

            $this->cleanForReturn($l_aJSON);
            echo json_encode($l_aJSON);

            //Utils::closeBufferAndContinueProcess('{"status": "1"}');
  //          $this->sendPush(array(PUSH_LEVEL_ADMIN),'LOGIN: '.$l_xChannel->title.$l_sLoginPushAppend);

        }

        private function returnCurrentPairingForChannelId($p_nChannelId){
            $l_aJSON['pairing'] = array();
            $l_xChannelManager = new ChannelManager();
            
            $sBundleID = $_GET['bid'];
            $sBundleID = $_GET['bid'];

            //if SC = DEMO = 30

            $l_aSocialProperties = unserialize(SOCIAL_SHARING_PROPERTIES);

            if($p_nChannelId == 30){
                    $l_aSocialSharring = array();
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_FACEBOOK_ID] = array('title'=>'Facebook','id' => SOCIAL_SHARING_PROPERTY_FACEBOOK_ID ,'icon' => '9' ,'connected' => 'Y','color' => "#6079B5");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TWITTER_ID] = array('title'=>'Twitter','id' => SOCIAL_SHARING_PROPERTY_TWITTER_ID ,'icon' => '8','connected' => 'Y','color' => "#1DAEEC");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y');
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_GOOGLE_ID] = array('title'=>'Google+','id' => SOCIAL_SHARING_PROPERTY_GOOGLE_ID,'icon' => 'J','connected' => 'Y');
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_PINTEREST_ID] = array('title'=>'Pinterest','id' => SOCIAL_SHARING_PROPERTY_PINTEREST_ID,'icon' => 'H','connected' => 'Y');
                    $l_aSocialProperties = $l_aSocialSharring; 
            }if($this->_nAPIVersion >= 3.4){
                $l_aSocialSharring = array();
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_FACEBOOK_ID] = array('title'=>'Facebook','id' => SOCIAL_SHARING_PROPERTY_FACEBOOK_ID ,'icon' => '9' ,'connected' => 'Y','color' => "#6079B5");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TWITTER_ID] = array('title'=>'Twitter','id' => SOCIAL_SHARING_PROPERTY_TWITTER_ID ,'icon' => '8','connected' => 'Y','color' => "#1DAEEC");


                    if($this->_sBundleID=="oflocreator"){
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT_E);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT_E);
                }else if($this->_sBundleID=="proflo"){
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT_P);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT_P);
                }else{
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT);
                }
                    
                    $l_aSocialProperties = $l_aSocialSharring; 
            }



            else if($this->_nAPIVersion >= 3.1){
                $l_aSocialSharring = array();
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_FACEBOOK_ID] = array('title'=>'Facebook','id' => SOCIAL_SHARING_PROPERTY_FACEBOOK_ID ,'icon' => '9' ,'connected' => 'Y','color' => "#6079B5");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TWITTER_ID] = array('title'=>'Twitter','id' => SOCIAL_SHARING_PROPERTY_TWITTER_ID ,'icon' => '8','connected' => 'Y','color' => "#1DAEEC");


                    if($this->_sBundleID=="oflocreator"){
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT_E);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT_E);
                }else if($this->_sBundleID=="proflo"){
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT_P);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT_P);
                }else{
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT);
                }
                    
                    $l_aSocialProperties = $l_aSocialSharring; 
            }

            $l_aSocialTokenPropertyIds = array();
            $l_aChannelSocialTokens = $l_xChannelManager->getSocialTokensByChannelId($p_nChannelId);
            foreach($l_aChannelSocialTokens as $l_xRow){
                $l_aSocialTokenPropertyIds[$l_xRow['social_property_type_id']][] = array('id' =>$l_xRow['id'],'title' =>$l_xRow['page_name'],'active' =>$l_xRow['active']);
            }
            
            foreach($l_aSocialProperties as $l_xRow){
                if($l_aSocialTokenPropertyIds[$l_xRow['id']]){
                    $l_xRow['connected_pages'] = $l_aSocialTokenPropertyIds[$l_xRow['id']];
                    $l_xRow['connected'] = 'Y';
                }else{
                    $l_xRow['connected'] = 'N';
                }
                $l_aJSON['pairing'][] = $l_xRow;
            }
            
            return $l_aJSON['pairing'];
        }


        public function rq_deletePairedItem(&$paired_id){

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "DELETE FROM enr_channel_oauth WHERE id = '$paired_id'";
     
            $l_xSql->runQuery($l_sSql);

        }

        public function rq_updateAvatar(&$p_aArgs){


            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = intval($p_aArgs['channel_id']);
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_xSql = AppController::getInstance()->getConnection();

            if(!$_FILES['file'])
                $this->showError(0,'File Missing');

            if(!preg_match("/\/(jpe?g|gif|png)/",strtolower($_FILES['file']['type']))){
                $l_aJSON = array();
                $l_aJSON['status'] = 2;
                $l_aJSON['message'] = 'Invalid File Type';
                echo json_encode($l_aJSON);
                exit;
            }
            

 
    
            $l_bSuccess = false;
            $l_sNewAbsoluteFilePath = $this->saveUploadedAvatarFileForUserId($_FILES['file'],$l_nUserId);

            require_once(RESIZE_IMAGE_CLASS);
            $image = new resize($l_sNewAbsoluteFilePath);
            $image->resizeImage(600,600,'auto');
            $image->saveImage($l_sNewAbsoluteFilePath,88);

            $l_sNewRelativeFilePath = str_replace(FILES_PATH,"",$l_sNewAbsoluteFilePath);           
        

            $l_aKeyValue = array();
            $l_aKeyValue['img'] = $l_sNewRelativeFilePath;
    
            $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/photo';
            $l_sS3FilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sNewRelativeFilePath);
            $l_sStoragePath = $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/photo\//","/photo/",$l_sS3FilePath));

            Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);

            $l_sSql = 'UPDATE enr_chanel SET image_path = "'.$l_sStoragePath.'" WHERE id = '.$l_nChannelId;
            $this->_xSQLManager->runQuery($l_sSql);


            $l_sMemcacheKey = Memcaching::makeKey($l_xChannel->slug);
            $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $l_aJSON = array();
            $l_aJSON['status'] = 1;
            $l_aJSON['img'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.$l_sStoragePath;
            // $l_aJSON['img'] = Utils::fixCDNImages($this->returnPhotoType($l_sNewRelativeFilePath,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_MED));
            echo json_encode($l_aJSON);

        }


        
        public function rq_sendToLegacyServer($p_nChannelId,$p_sType,$p_sPath,$p_sDesc){

            $l_sCId = 1;

            switch(intval($p_nChannelId)){
                case 19: $l_sSlug = 'ludacris'; break;
                case 18:
                    $l_sSlug = 'tyrese';
                    break;
                case 17: case 15:
                    $l_sSlug = 'enrique';
                    $l_sCId = 7;
                    break;

                default : return;
            }


            $ch = curl_init();
            $data = array(
                'cid' => $l_sCId, 
                'user_id' => '-1', 
                'description' => $p_sDesc, 
                'token' => 'c493d4babdabc76b248ef5f05b00b70a', 
                'is_publish' => '1'
            );

            if($p_sType == 'video'){
                $l_sType = 'add_video';
                $data['video'] = curl_file_create($p_sPath,'video/*','video.mp4');
            }else{
                $l_sType = 'add_image';
                $l_sType = 'add_photo';
                $data['image'] = curl_file_create($p_sPath,'image/jpg','picture.jpeg');
                // $data['image'] = new CurlFile($p_sPath,'image/jpg','picture.jpeg');
            }
            
            $l_sURL = 'http://cms.'.$l_sSlug.'.starsite.com/index.php/api?option='.$l_sType;
            $ch = curl_init($l_sURL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch); 
            // print_r($output );

                // ENRIQUE
                // #define CID 7
                // #define SERVER              @"http://cms.enrique.starsite.com"
                // TYRESE
                // #define CID 1
                // #define SERVER              @"http://cms.tyrese.starsite.com”
                // #define SERVER              @"http://cms.ludacris.starsite.com”
                // #define SERVER                @"http://cms.baggirlproblems.starsite.com”

        }



        private function saveUploadedAvatarFileForUserId($p_xFile,$p_nId){
                        
            if(!preg_match("/\/(jpe?g|gif|png)/",strtolower($p_xFile['type']))){
                return false;
            }
            
            ini_set('max_execution_time',60); 
            ini_set('memory_limit', '128M');
            $l_sHash = date('Y/m');
            $l_aFileParts = pathinfo($p_xFile['name']);
            $l_sNewPath = FILES_PATH.'users/'.$l_sHash.'/';
           
            if(!is_dir($l_sNewPath)) 
                mkdir($l_sNewPath,0777,true);
            
            $l_nUnique = uniqid();
            $l_sNewRelativeFilePath = $p_nId.'_'.$l_nUnique.'.'.strtolower($l_aFileParts['extension']);
            $l_sNewAbsoluteFilePath = $l_sNewPath.$l_sNewRelativeFilePath;
            
            if(move_uploaded_file($p_xFile['tmp_name'],$l_sNewAbsoluteFilePath)){
                $l_sNewAbsoluteFilePath = Utils::copyAndFixPhotoRotation($l_sNewAbsoluteFilePath);
                // Utils::buildImageSizesAndCrop($l_sNewAbsoluteFilePath,array(FRONT_ASSET_SIZE_TYPE_USER_AVATAR,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_MED,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_LARGE)); 
            }
            
            return $l_sNewAbsoluteFilePath;
        }




        public function rq_dashboardData(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;


            



            //IF 30 == DEMO
            if($l_nMainChannelId == 30){
                $l_aReturnData = array();
                $l_aReturnData['status'] = 1;

                $l_aReturnData['totals']['reach'] = 24891584;
                $l_aReturnData['totals']['impressions'] = 40976096;
                $l_aReturnData['totals']['earnings'] = 13531.5849504;
                $l_aReturnData['totals']['earnings_avg'] = "188,565";
                $l_aReturnData['totals']['earnings_days'] = 9;
                $l_aReturnData['totals']['earnings_avg_symbol'] = "%.1fk";
                $l_aReturnData['totals']['earnings_subtitle'] = "YTD";
                $l_aReturnData['totals']['reach_avg'] = "21.9";
                $l_aReturnData['totals']['reach_days'] = 9;
                $l_aReturnData['totals']['reach_avg_symbol'] = "%.1fm";
                $l_aReturnData['totals']['reach_subtitle'] = "Reach/30d";
                $l_aReturnData['totals']['impressions_avg'] = "41.0";
                $l_aReturnData['totals']['impressions_avg_symbol'] = "%.1fm";
                $l_aReturnData['totals']['impressions_days'] = 9;
                $l_aReturnData['totals']['impressions_subtitle'] = "Impressions/30d";
                $l_aReturnData['totals']['weekly_post_rate'] = 5;

                $l_aReturnData['data'] = array();
                $l_aDummyText = array();
                $l_aDummyText[] = 'Backstage at The Billboard Awards';
                $l_aDummyText[] = 'Music Video Premiere "Beast Mode"';
                $l_aDummyText[] = 'Crazy Boston terrier in baby\'s crib! Must see!!!!';
                $l_aDummyText[] = 'Talking Ludaversal on power 106, w. Big Boi in Los Angeles';

                for($i=0; $i<20; $i++){
                    $l_sImage = 'http://lorempixel.com/400/200/?r='.rand();
                    $l_aDummy = array();
                    $l_aDummy['id'] = $i;
                    $l_aDummy['post_id'] = $i;
                    if($i < count($l_aDummyText)){
                        $l_aDummy['description'] = $l_aDummyText[$i];
                        $l_sImage = 'http://darwin.on.starsite.com/api/assets/img/temp/'.$i.'.jpg';
                    }else{
                        $l_aDummy['description'] = 'Maecenas venenatis sapien sed ex accumsan tincidunt. Phasellus luctus volutpat elit, vel gravida mi bibendum sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum dignissim eros sed elementum. Duis convallis lacus quis sollicitudin tristique. Nullam ultricies turpis sit amet sollicitudin porta. Nullam sollicitudin nec velit eget viverra.';
                        $l_aDummy['description'] = explode(" ",strtolower($l_aDummy['description']));
                        shuffle($l_aDummy['description']);
                        $l_aDummy['description'] = substr(ucfirst(join(" ",$l_aDummy['description'])),0,150);
                    }
                    

                    $l_aDummy['start_date'] = date('Y-m-d',mktime(3,0,1,date("m"),date("d") - $i,date("Y") ));
                    $l_aDummy['photo_start_date'] = date('Y-m-d',mktime(3,0,1,date("m"),date("d") - $i,date("Y") ));
                    $l_aDummy['fb_likes'] = rand(30000,80000);
                    $l_aDummy['fb_comments'] = rand(1000,3000);
                    $l_aDummy['fb_shares'] = rand(1000,5000);
                    $l_aDummy['fb_reach'] = rand(7 * 1000000,10 * 1000000);
                    $l_aDummy['fb_impressions'] = rand(2 * 1000000,10 * 1000000);
                    $l_aDummy['fb_engagement'] = rand(30000,80000);
                    $l_aDummy['video_length'] = rand(50000,95076);
                    $l_aDummy['engagement_score'] = rand(60,88);
                    $l_aDummy['video_view'] = rand(700000,1350760);
                    $l_aDummy['video_impression'] = rand(30000,75076);
                    $l_aDummy['video_impression'] = rand(30000,75076);
                    $l_aDummy['slug'] = 'sc';

                    
                    $l_xTheData = $this->buildStatObject($l_aDummy,$l_nMainChannelId);
                    $l_xTheData['thumbnail'] = $l_sImage;
                    $l_xTheData['file_url'] = 'http://techslides.com/demos/sample-videos/small.mp4';
                    $l_aReturnData['data'][] = $l_xTheData;
                }
                
                
                $this->outputJSON(json_encode($l_aReturnData));
                return;
            }


            // $l_sMemcacheKey = Memcaching::makeKey($l_xChannel->slug);
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            //IF legacy - then use old Databases
            //IF Enrique 15

            //IF legacy - then use old Databases
            //IF Enrique OR Tyrese
            $l_bVideosOnly = false;
            if($l_nChannelId == 15){
                
                $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
                $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
                $DB_OVERRIDE['DB_NAME'] = 'scdevteam';
                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);
                $l_nChannelId = 7;
                 $l_bVideosOnly = true;
            }


            $this->_xSQLManager = new FrontCoreSql();
            $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
                $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
                $DB_OVERRIDE['DB_NAME'] = 'oflo_pro';
                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_nQueryChannelId = $l_nChannelId;
            
            if($l_bVideosOnly){
                 if($l_nQueryChannelId==30){
                  $l_sSql = 'SELECT f.*,v.*,r.*,c.*, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN oflo_reports_monthly r 
                        ON r.channel_id = '.$l_nQueryChannelId.'

                        JOIN enr_content_post_to_social c
                        ON c.content_id = f.content_id

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';
                    }else{

                        $l_sSql = 'SELECT f.*,v.*,r.*,c.*, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN oflo_reports_monthly r 
                        ON r.channel_id = '.$l_nQueryChannelId.'

                        JOIN enr_content_post_to_social c
                        ON c.content_id = f.content_id

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';

                        $l_sSql = 'SELECT f.*,v.*, c.slug,c.total_reach, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN enr_chanel c
                        ON c.id = '.$l_nQueryChannelId.'

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';

                    }

            }else{

                if($l_nQueryChannelId==30){
                 $l_sSql = 'SELECT f.*,v.*, c.slug,c.total_reach, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN enr_chanel c
                        ON c.id = '.$l_nQueryChannelId.'

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';

                    }else{

                        $l_sSql = 'SELECT f.*,v.*,ph.*, c.slug,c.total_reach,c.id, v.image_path, v.id as video_db_id, ph.id as photo_db_id, c.slug,

                        ph.start_date as photo_start_date,

                        CASE WHEN v.id > 0 THEN v.image_path
                        ELSE ph.destination
                        END 
                        as image_path,

                        CASE WHEN v.id > 0 THEN v.destination
                        ELSE ph.destination
                        END 
                        as destination,

                        CASE WHEN v.id > 0 THEN v.description
                        ELSE ph.description
                        END 
                        as description,

                        CASE WHEN v.id > 0 THEN v.fb_reach ELSE ph.fb_reach END as fb_reach,
                        CASE WHEN v.id > 0 THEN v.fb_impressions ELSE ph.fb_impressions END as fabo_impressions,
                        CASE WHEN v.id > 0 THEN v.fb_likes ELSE ph.fb_likes END as fabo_likes,
                        CASE WHEN v.id > 0 THEN v.fb_comments ELSE ph.fb_comments END as fabo_comments,
                        CASE WHEN v.id > 0 THEN v.fb_engagement ELSE ph.fb_engagement END as fabo_engagement,
                        CASE WHEN v.id > 0 THEN v.start_date ELSE ph.start_date END as start_date


                        FROM enr_feed f

                        LEFT JOIN enr_video v 
                        ON v.id = f.content_id

                        LEFT JOIN enr_photo ph 
                        ON ph.id = f.content_id

                        JOIN enr_chanel c
                        ON ( v.cid = c.id OR ph.cid = c.id )

                        WHERE c.id = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';

                    }
            }
            // f.post_type = "video"
                        // AND 
          

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

           
            


             // print_r($l_xData);
             // exit;

            // $l_sSql = 'SELECT count(*) FROM 
            //             enr_feed f
            //             JOIN enr_video v 
            //             ON v.id = f.content_id
            //             JOIN enr_brightcove_video_metrics bc 
            //             ON bc.video_id = v.brightcove_media_id

            //             JOIN ss_social_tracking_details ss 
            //             ON ss.content_id = f.content_id

            //             JOIN enr_fbposts_key_metrics fbm 
            //             ON fbm.post_id = ss.post_id

            //             WHERE f.post_type = "video"
            //             AND cid = '.$l_nQueryChannelId.'
            //             AND posted >= "'.date('Y-m-d',strtotime(date('Y-m-d').' -7 days')).'"
            //             ORDER BY f.time_stamp DESC';
            // $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

             
            // $l_sSql = 'SELECT * FROM 
                        
            //             enr_feed f
            //             JOIN enr_photo v 
            //             ON v.id = f.content_id
                        
            //             JOIN ss_social_tracking_details ss 
            //             ON ss.content_id = f.content_id

            //             JOIN enr_fbposts_key_metrics fbm 
            //             ON fbm.post_id = ss.post_id

            //             WHERE f.post_type = "photo"
            //             ORDER BY f.time_stamp DESC';
            // $l_xPhotoData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
              


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            foreach($l_xData as $l_xRow){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    $l_nWeeklyPostRate++;
                }
                $reach = $l_xRow['total_reach'];
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                //print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['revenue'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

            }

            
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }

            $l_sSql = "SELECT page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE channel_id = $l_nMainChannelId";
            
            $l_xReportData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            foreach($l_xReportData as $l_xRow){
               if($l_xRow['revenue']==0){
                $revenue += $l_xRow['revenue']; 
                $page_views += $l_xRow['page_views_ga']; 
                }else{
                    $revenue += $l_xRow['revenue']; 
                $page_views += $l_xRow['video_views']; 
                }
            }

            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_nTempVal = $revenue;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
            }else if($l_nTempVal > 1000000){
                $l_sSymbol = '%.1fm';
                $l_cTempVal = number_format($l_nTempVal/1000000,1);
            }else if($l_nTempVal > 1000000000){
                $l_sSymbol = '%.1fb';
                $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
            }else{
                $l_cTempVal = $l_nTempVal;
                $l_sSymbol = '%.2f';
            }

            $l_aReturnData['totals']['earnings_avg'] = number_format(round($l_cTempVal,0),0);
            $earned = $l_cTempVal;
            $l_aReturnData['totals']['earnings_days'] = 0;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,134';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Total Rev.';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;
            }
            
            
            $l_nTempVal = $reach;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
                    if($l_nTempVal > 1000000){
                    $l_sSymbol = '%.1fm';
                    $l_cTempVal = number_format($l_nTempVal/1000000,1);
                            if($l_nTempVal > 1000000000){
                        $l_sSymbol = '%.1fb';
                        $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
                    }
                }
            }else{
                $l_cTempVal = $l_nTempVal;
                $l_sSymbol = '%.2f';
            }

            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
           


            $l_aReturnData['totals']['reach_avg'] = $l_cTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_cTempVal;
            $l_aReturnData['totals']['reach'] = $l_cTempVal;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach';

            $l_aReturnData['totals']['impressions'] = $page_views;

            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($page_views);
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
                    if($l_nTempVal > 1000000){
                    $l_sSymbol = '%.1fm';
                    $l_cTempVal = number_format($l_nTempVal/1000000,1);
                            if($l_nTempVal > 1000000000){
                        $l_sSymbol = '%.1fb';
                        $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
                    }
                }
            }else{
                $l_cTempVal = $l_nTempVal;
                $l_sSymbol = '%.2f';
            }

            $l_aReturnData['totals']['impressions'] = $l_cTempVal;
            $l_aReturnData['totals']['impressions_avg'] = ($l_cTempVal);
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Views';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;
            $l_aReturnData['totals']['weekly_revenue_rate'] = $earned/52;



            $l_aReturnData['status'] = 1;

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_curateData(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $index = $_GET['index'];

             $total_count = $_GET['count'];

            $l_nChannelId = 64;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                $l_sSql = "select id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination, image_path, tags, description, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, deep_link_web_lp, brightcove_media_id, image_aspect_ratio
                        from enr_video
                        where 
                             is_publish=1 and 
                            cid='64' and start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
            order by start_date desc";
            }
       
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            
            $num_count = count($l_xData);  


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }

                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }
/*
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }
*/


            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['earnings_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,565';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = '%.2f';
            }
            


            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['reach'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }


            $l_aReturnData['totals']['reach_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach/30d';


            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['impressions'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['impressions_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions/30d';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;

            $l_aReturnData['totals']['earnings'] = 13531.5849504;
            $l_aReturnData['totals']['earnings_avg'] = "188,565";
            $l_aReturnData['totals']['earnings_days'] = 9;

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_ofloDataOld(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            
            $index = $_GET['index'];

             $total_count = $_GET['count'];


             var_dump($this->_sBundleID); exit();

            
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                $l_sSql = "select id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, cid, video_url, video_type, is_publish, destination, image_path, tags, description, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, deep_link_web_lp, deep_link_web_oflo, deep_link_web_oflo_lp, brightcove_media_id, image_aspect_ratio
                        from enr_video
                        where 
                             is_publish=1 and 
                            is_oflo='y' and start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                    order by start_date desc";
            }
       
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            
            $num_count = count($l_xData);  


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }

                $l_xTheData = $this->buildStatObjectNoReportsSlugOD($l_xRow,$l_xRow['cid']);
                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }
/*
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }
*/


            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['earnings_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,565';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = '%.2f';
            }
            


            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['reach'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }


            $l_aReturnData['totals']['reach_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach/30d';


            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['impressions'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['impressions_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions/30d';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;

            $l_aReturnData['totals']['earnings'] = 13531.5849504;
            $l_aReturnData['totals']['earnings_avg'] = "188,565";
            $l_aReturnData['totals']['earnings_days'] = 9;

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }


        public function rq_ofloDataId(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            
            $index = $_GET['index'];

             $total_count = $_GET['count'];

             $id = $_GET['id'];



            
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                $l_sSql = "select id, 'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, cid, video_url, video_type, is_publish, destination, image_path, tags, description, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, deep_link_web_lp, deep_link_web_oflo, deep_link_web_oflo_lp, brightcove_media_id, image_aspect_ratio
                        from enr_video
                        where 
                             is_publish=1 and 
                            is_oflo='y' and start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) 
                            and id=$id
                   order by start_date desc";
            }
       
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            
            $num_count = count($l_xData);  


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }

                $l_xTheData = $this->buildStatObjectNoReportsSlugOD($l_xRow,$l_xRow['cid']);
                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }
/*
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }
*/


            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['earnings_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,565';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = '%.2f';
            }
            


            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['reach'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }


            $l_aReturnData['totals']['reach_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach/30d';


            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['impressions'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['impressions_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions/30d';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;

            $l_aReturnData['totals']['earnings'] = 13531.5849504;
            $l_aReturnData['totals']['earnings_avg'] = "188,565";
            $l_aReturnData['totals']['earnings_days'] = 9;

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_searchCatList($search_term){

            if($search_term!=""){
                $search_term = $_GET['search_term'];
            }

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT distinct * FROM `viral_media_category` WHERE cat_name LIKE '%$search_term%'";
       


            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $i=0;
            foreach($l_xData as $l_xRow){

                $l_aReturnData['data'][$i]['id'] = $l_xRow['id'];
                $l_aReturnData['data'][$i]['cat_name'] = $l_xRow['cat_name'];
                
                $i++;
            }

            $l_aReturnData['status'] = 1;

           /* $j=0;            
            foreach($l_aReturnData as $l_xRow){
               
            echo json_encode($l_xRow);

                $j++;
            
            }       */
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_searchChannelList($id){

            if($id!=""){
                $id = $_GET['id'];
            }

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT distinct * FROM enr_chanel WHERE id='$id' AND is_oflo='y'";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $i=0;
            foreach($l_xData as $l_xRow){

                $l_aReturnData['data'][$i]['id'] = $l_xRow['id'];
                $l_aReturnData['data'][$i]['name'] = $l_xRow['name'];
                $l_aReturnData['data'][$i]['slug'] = $l_xRow['slug'];
                 $l_aReturnData['data'][$i]['image_path'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/thumb/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['small_thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/small_thumb/'.$l_xRow['image_path'];   
                $l_aReturnData['data'][$i]['banner'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/bg-sn.jpg';
                $l_aReturnData['data'][$i]['global_ga_key'] = 'UA-63548102-57';
                 $l_aReturnData['data'][$i]['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/chanel.jpg';   
                $l_aReturnData['data'][$i]['channel_message'] = '';    
              
                $i++;
            }

            $l_aReturnData['status'] = 1;
            /*
            $j=0;            
            foreach($l_aReturnData as $l_xRow){
               
            echo json_encode($l_xRow);

                $j++;
            }
    */
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_searchChannelListTerm($search_term){

            if($search_term!=""){
                $search_term = $_GET['search_term'];
            }

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT distinct * FROM enr_chanel WHERE name LIKE '%$search_term%' AND is_oflo='y'";

            


            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $i=0;
            foreach($l_xData as $l_xRow){

                $l_aReturnData['data'][$i]['id'] = $l_xRow['id'];
                $l_aReturnData['data'][$i]['name'] = $l_xRow['name'];
                $l_aReturnData['data'][$i]['slug'] = $l_xRow['slug'];
                 $l_aReturnData['data'][$i]['image_path'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/thumb/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['small_thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/small_thumb/'.$l_xRow['image_path'];   
                $l_aReturnData['data'][$i]['banner'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/bg-sn.jpg'; 
                $l_aReturnData['data'][$i]['global_ga_key'] = 'UA-63548102-57';
                 $l_aReturnData['data'][$i]['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/chanel.jpg';   
                $l_aReturnData['data'][$i]['channel_message'] = '';    

                
                $i++;
            }

            $l_aReturnData['status'] = 1;
            /*
            $j=0;            
            foreach($l_aReturnData as $l_xRow){
               
            

                $j++;
            }
    */
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_returnCatList(){

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT * FROM `viral_media_category`";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $i=0;
            foreach($l_xData as $l_xRow){

                $l_aReturnData['data'][$i]['id'] = $l_xRow['id'];
                $l_aReturnData['data'][$i]['cat_name'] = $l_xRow['cat_name'];
                
                $i++;
            }

            $j=0;            
            $l_aReturnData['data'][$i]['global_ga_key'] = 'UA-63548102-57';

            $l_aReturnData['status'] = 1;
            /*foreach($l_aReturnData as $l_xRow){
               
            echo json_encode($l_xRow);

                $j++;
            
            }*/       
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_returnCatFeed(){
                $index = $_GET['index'];

             $total_count = $_GET['count'];

            $category = $_GET['cat_term'];
           
          
            
            $l_nChannelId = $channel_id;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
           //$l_lSql = "SELECT content_id, COUNT(content_id) AS like_total FROM enr_like GROUP BY content_id ORDER BY like_total DESC LIMIT 1";
            

                   // $l_sSql = "SELECT * FROM ( ( SELECT concat('video_', ev.id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev LEFT JOIN sc_reports_daily srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND srd.time_stamp >= DATEADD(hh, -24, GETDATE()) ) ) t1 WHERE t1.fan_id=-1";
               
                    $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, IF (ev.feed_date != 0, UNIX_TIMESTAMP(ev.feed_date), UNIX_TIMESTAMP(ev.start_date)) as feed_date, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev LEFT JOIN viral_media_cat_post vmcp ON vmcp.post_id = ev.id
                              LEFT JOIN viral_media_category vmc ON vmc.id = vmcp.cat_id
                              WHERE vmcp.content_type_id = 1  AND  ev.is_oflo = 'y' AND ev.is_publish= 1 
                              AND feed_date <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 HOUR))
                              AND vmc.cat_name LIKE '%$category%'
         
                        ORDER BY feed_date DESC";

//               var_dump($l_sSql);  exit();

        
           //$num_rows = mysql_num_rows($this->_xSQLManager->runQuery($l_sSql));

          
      
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);
           
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){
                

               if($count>=$index){


                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                

                     $l_xTheData = $this->buildStatObjectNoReportsSlug($l_xRow,$l_xRow['cid']);
                



                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                if($actual_count==$total_count){
                    break;
                }

            }else{
                $other_count++;
            }
                
                $count++;
            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_returnMenuList(){

            $l_aReturnData = array();

            $l_aReturnData['data'][0]['id'] = 0;
            $l_aReturnData['data'][0]['menu_name'] = "Latest";
            $l_aReturnData['data'][0]['hex'] = "#d63032";

            $l_aReturnData['data'][1]['id'] = 1;
            $l_aReturnData['data'][1]['menu_name'] = "Trending";
            $l_aReturnData['data'][1]['hex'] = "#ae2562";

            $l_aReturnData['data'][2]['id'] = 2;
            $l_aReturnData['data'][2]['menu_name'] = "Following";
            $l_aReturnData['data'][3]['hex'] = "#ae25a0";

            $l_aReturnData['data'][3]['id'] = 3;
            $l_aReturnData['data'][3]['menu_name'] = "Liked";
            $l_aReturnData['data'][3]['hex'] =  "#3074d6";

            $l_aReturnData['status'] = 1;
               
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_returnMenuCatList(){

            $l_aReturnData = array();

            

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT cat_name, hex_color, cat_image FROM viral_media_category WHERE is_oflo=1 ORDER BY cat_name ASC";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $i=0;

            foreach($l_xData as $l_xRow){

                $l_aReturnData['data']['categories'][$i]['id'] = $i;
                $l_aReturnData['data']['categories'][$i]['category_name'] = $l_xRow['cat_name'] ? $l_xRow['cat_name'] : "" ;
                $l_aReturnData['data']['categories'][$i]['background'] = "";
                $l_aReturnData['data']['categories'][$i]['end_point'] = strtolower($l_xRow['cat_name']);
                $l_aReturnData['data']['categories'][$i]['hex'] = $l_xRow['hex_color'] ? $l_xRow['hex_color'] : "#ae".rand(1000,9999);
                $l_aReturnData['data']['categories'][$i]['icon'] = $l_xRow['cat_image'] ? "http://d3hl6zmfu9utfu.cloudfront.net/admin/assets/ufile/".$l_xRow['cat_image'] : "" ;
                $i++;
            }

        

            // $l_aReturnData['data']['default']['id'] = 0;
            // $l_aReturnData['data']['default']['feed_name'] = "Latest";
            // $l_aReturnData['data']['default']['hex'] = "#d63032";
            // $l_aReturnData['data']['default']['icon'] ="http://d3hl6zmfu9utfu.cloudfront.net/category_images/ofloCategoryLatest@3x.png";

            $l_aReturnData['data']['feeds'][0]['id'] = 0;
            $l_aReturnData['data']['feeds'][0]['category_name'] = "Latest";
            $l_aReturnData['data']['feeds'][0]['hex'] = "#d63032";
            $l_aReturnData['data']['feeds'][0]['icon'] ="http://d3hl6zmfu9utfu.cloudfront.net/category_images/ofloCategoryLatest@3x.png";

            $l_aReturnData['data']['feeds'][1]['id'] = 1;
            $l_aReturnData['data']['feeds'][1]['category_name'] = "Trending";
            $l_aReturnData['data']['feeds'][1]['hex'] = "#ae2562";
            $l_aReturnData['data']['feeds'][1]['icon'] ="http://d3hl6zmfu9utfu.cloudfront.net/category_images/ofloCategoryTrending@3x.png";

            $l_aReturnData['data']['feeds'][2]['id'] = 2;
            $l_aReturnData['data']['feeds'][2]['category_name'] = "Following";
            $l_aReturnData['data']['feeds'][2]['hex'] = "#ae25a0";
            $l_aReturnData['data']['feeds'][2]['icon'] ="http://d3hl6zmfu9utfu.cloudfront.net/category_images/ofloCategoryFollowing@3x.png";

            $l_aReturnData['data']['feeds'][3]['id'] = 3;
            $l_aReturnData['data']['feeds'][3]['category_name'] = "Liked";
            $l_aReturnData['data']['feeds'][3]['hex'] =  "#3074d6";
            $l_aReturnData['data']['feeds'][3]['icon'] ="http://d3hl6zmfu9utfu.cloudfront.net/category_images/ofloCategoryLiked@3x.png";

            $l_aReturnData['data']['default'] = "Latest";
            $l_aReturnData['status'] = 1;
               
            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_defaultData($search_term, $channel_id, $index, $count,$user_id, $p_bBypassMemcache = false){

 $index = $_GET['index'];

             $total_count = $_GET['count'];

            
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;

            date_default_timezone_set('UTC');
         $utc_time = date("Y-m-d h:i:sa"); 

        date_default_timezone_set('America/Los_Angeles');
         $pacific_time = date("Y-m-d h:i:sa"); 
 

             
          //  $l_aCachedContent = Memcaching::getInstance()->getKey($this->l_sMemcacheKey);
           
         //   if(!$l_aCachedContent){
            
            if($l_bVideosOnly){
               //  $this->l_sMemcacheKey = Memcaching::makeKey('oflo-latest-feed');
                $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio
                        from enr_video ev inner join enr_feed ef
                        where 
                             ev.is_publish=1 and ev.id = ef.content_id and 
                            ev.is_oflo='y' and ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                    order by ev.start_date desc";
            
       
           $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


           // Memcaching::getInstance()->store($this->l_sMemcacheKey,$l_xData,30);
            
            }
            //else{
            //   $l_xData = $l_aCachedContent['value'];
            
           // }  
 //var_dump($l_aCachedContent);
            //PRIORITY
           // $l_rSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, srd.priority_number, srd.duration, srd.content_id as content_id, ec.slug as slug FROM enr_video ev INNER JOIN enr_chanel ec ON ec.id = ev.cid INNER JOIN priority srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND ev.is_oflo='y' ORDER BY RAND() LIMIT 1";


           //  $l_yData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));
           //   $l_aReturnData = array();
           //   foreach($l_yData as $l_yRow){
                
                  
           //      $l_yTheData = $this->buildStatObjectNoReportsSlugOD($l_yRow,$l_yRow['cid']);
           //       $l_aReturnData['data'][] = $l_yTheData;

                

                
           //  }
            $num_count = count($l_xData);

           
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                
                    $l_xTheData = $this->buildStatObjectNoReportsSlugOD($l_xRow,$l_xRow['cid']);
                
                
                


                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

               

                $l_aReturnData['data'][] = $l_xTheData;

            

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }
        }

        public function rq_returnChannelList(){

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT * FROM enr_chanel WHERE is_oflo='y' AND priority > 0 ORDER BY priority ASC";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $i=0;
            foreach($l_xData as $l_xRow){

                $l_aReturnData['data'][$i]['id'] = $l_xRow['id'];
                $l_aReturnData['data'][$i]['name'] = $l_xRow['name'];
                $l_aReturnData['data'][$i]['slug'] = $l_xRow['slug'];
                $l_aReturnData['data'][$i]['priority'] = $l_xRow['priority'];
                $l_aReturnData['data'][$i]['image_path'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/thumb/'.$l_xRow['image_path'];
                $l_aReturnData['data'][$i]['small_thumb'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xRow['slug'].'/assets/ufile/small_thumb/'.$l_xRow['image_path'];   
                $l_aReturnData['data'][$i]['banner'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/bg-sn.jpg';
                $l_aReturnData['data'][$i]['global_ga_key'] = 'UA-63548102-57';
                $l_aReturnData['data'][$i]['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/chanel.jpg';   
                $l_aReturnData['data'][$i]['channel_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xRow['slug'].'/images/chanel.jpg';   
                $l_aReturnData['data'][$i]['channel_message'] = '';    

                
                $i++;
            }
            $l_aReturnData['status'] = 1;
           /* $j=0;            
            foreach($l_aReturnData as $l_xRow){
               
            echo json_encode($l_xRow);

                $j++;
            }*/

            $this->outputJSON(json_encode($l_aReturnData));
            return;

        }

        public function rq_addLike($content_id, $user_id, $post_type_id){

            $content_id = $_GET['id'];
            $user_id = $_GET['userid'];
            $post_type_id = $_GET['ptype'];

            if($post_type_id==1){
                $post_type = "video";
            }else{
                $post_type = "photo";
            }

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "INSERT IGNORE INTO enr_like (id,user_id,userid,post_type,post_type_id,content_id,time_stamp)
            VALUES ('','','$user_id','$post_type',$post_type_id,$content_id,UTC_TIMESTAMP())";

            $this->_xSQLManager->runQuery($l_sSql);

            $l_aReturnData = array();

            $l_aReturnData = $this->rq_userLikesnFollowsWithId($user_id,$content_id);

            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }


        public function rq_addReport($content_id, $user_id, $device_id){

            $content_id = $_GET['id'];
            $user_id = $_GET['userid'];
            $block = $_GET['block'];
            $report = $_GET['report'];
            $device_id = $_GET['deviceid'];


            $l_xSql = AppController::getInstance()->getConnection();

            if($block==1){

            $l_sSql = "INSERT IGNORE INTO oflo_block (userid,content_id,time_stamp,device_id)
            VALUES ('$user_id',$content_id,UTC_TIMESTAMP(),'$device_id')";

            $this->_xSQLManager->runQuery($l_sSql);

            $l_pSql = "DELETE FROM enr_like WHERE userid='$user_id' AND content_id=$content_id";

            $this->_xSQLManager->runQuery($l_pSql);

            }else if($block==2){

            $l_rSql = "DELETE FROM oflo_block WHERE (userid='$user_id' OR device_id='$device_id') AND content_id=$content_id";

            $this->_xSQLManager->runQuery($l_rSql);

            }else{}


            if($report==1){

                $report_type = "Spam";
                $l_rSql = "INSERT IGNORE INTO oflo_report (userid,content_id,report_type,time_stamp)
            VALUES ('$user_id',$content_id,$report,UTC_TIMESTAMP())";
            $this->_xSQLManager->runQuery($l_rSql);
            $send_email = 1;
            }else if($report==2){

                $report_type = "Explicit, graphic or sexual content";
                $l_rSql = "INSERT IGNORE INTO oflo_report (userid,content_id,report_type,time_stamp)
            VALUES ('$user_id',$content_id,$report,UTC_TIMESTAMP())";
            $this->_xSQLManager->runQuery($l_rSql);
            $send_email = 1;
            }else if($report==3){

                 $report_type = "Abusive or hamrful";
                $l_rSql = "INSERT IGNORE INTO oflo_report (userid,content_id,report_type,time_stamp)
            VALUES ('$user_id',$content_id,$report,UTC_TIMESTAMP())";
            $this->_xSQLManager->runQuery($l_rSql);
            $send_email = 1;
            }else if($report==4){

                 $report_type = "Video Unavailable";
                $l_rSql = "INSERT IGNORE INTO oflo_report (userid,content_id,report_type,time_stamp)
            VALUES ('$user_id',$content_id,$report,UTC_TIMESTAMP())";
            $this->_xSQLManager->runQuery($l_rSql);
            $send_email = 1;
            }else{

            }

            if($send_email==1){
                 
            $to = 'abuse@oflo.co';
            $subject = 'Content ID#: '.$content_id.' Reported '.$report_type;
            $message = 'User '.$user_id.' Reported '.$report_type.' on post number '.$content_id;
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: StarClub <no-reply@starsite.com>\n\r";

            mail($to, $subject, $message, $headers);

            }else{}


            

            $l_aReturnData = array();

            $l_aReturnData = $this->rq_userLikesnFollowsWithIdB($user_id,$content_id,$device_id);

            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }


        public function rq_removeLike($content_id,$user_id){
            $content_id = $_GET['id'];
            $user_id = $_GET['userid'];

             $l_xSql = AppController::getInstance()->getConnection();

             $l_sSql = "DELETE FROM enr_like WHERE userid='$user_id' AND content_id=$content_id";



            $this->_xSQLManager->runQuery($l_sSql);

            $l_aReturnData = array();
            
            $l_aReturnData = $this->rq_userLikesnFollowsWithId($user_id,$content_id);

            $this->outputJSON(json_encode($l_aReturnData));

            return;

        }

        public function rq_getRecent($slug){

            $slug = $_GET['slug'];

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT id FROM enr_chanel WHERE slug = '$slug'";

            $l_qData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


            $l_aReturnData = array();

           

            foreach($l_qData as $l_xRow){

                    $cid = $l_xRow['id'];
                    $l_nChannelId = $cid;
                    
            }

             $index = $_GET['index'];

             $total_count = $_GET['count'];

            
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;



            $l_rSql = "SELECT id from enr_video WHERE cid = $cid ORDER BY id DESC LIMIT 1";

            $l_yData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));


           

            foreach($l_yData as $l_yRow){

                    $id = $l_yRow['id'];
                   // $l_nChannelId = $cid;
                    
            }


            
            
            if($l_bVideosOnly){
                $l_pSql = "select id, 'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, cid, video_url, video_type, is_publish, destination, image_path, tags, description, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, deep_link_web_lp, deep_link_web_oflo, deep_link_web_oflo_lp, brightcove_media_id, image_aspect_ratio
                        from enr_video
                        where 
                             is_publish=1 and 
                            is_oflo='y' and start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) 
                            and id=$id
            order by start_date desc";
            }
       
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_pSql));
            
            $num_count = count($l_xData);  


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }

                $l_xTheData = $this->buildStatObjectNoReportsSlugOD($l_xRow,$l_xRow['cid']);
                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }
/*
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }
*/


            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['earnings_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,565';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = '%.2f';
            }
            


            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['reach'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }


            $l_aReturnData['totals']['reach_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach/30d';


            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['impressions'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['impressions_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions/30d';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;

            $l_aReturnData['totals']['earnings'] = 13531.5849504;
            $l_aReturnData['totals']['earnings_avg'] = "188,565";
            $l_aReturnData['totals']['earnings_days'] = 9;

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }
           

        }

         public function rq_addFollow($channel_id, $user_id, $post_type_id){

            $channel_id = $_GET['cid'];
            $user_id = $_GET['userid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "INSERT INTO enr_follows (id,userid,channel_id,time_stamp)
            VALUES ('','$user_id',$channel_id,UTC_TIMESTAMP())";

            $this->_xSQLManager->runQuery($l_sSql);

            $l_aReturnData = array();

            $l_aReturnData = $this->rq_userLikesnFollowsWithCid($user_id,$channel_id);

            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }

        public function rq_returnChannelSubscribers($channel_id){

            $channel_id = $_GET['channel_id'];
            $user_id = $_GET['userid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT channel_id FROM enr_follows WHERE channel_id = $channel_id";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


            $l_aReturnData = array();

            $count = 0;
            $video_count = 0;

            foreach($l_xData as $l_xRow){

                    //$l_aReturnData['follows'][$count] = $l_xRow['userid'];
                    $count++;
                }

            $l_rSql = "SELECT id FROM enr_video WHERE cid = $channel_id and is_publish = 1";

            $l_yData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));   

            foreach($l_yData as $l_yRow){

                    //$l_aReturnData['follows'][$count] = $l_xRow['userid'];
                    $video_count++;
                } 

                $l_aReturnData['data']['count'] = $count;
                $l_aReturnData['data']['video_count'] = $video_count;
                $l_aReturnData['status'] = 1;

                $this->outputJSON(json_encode($l_aReturnData));

                return;
        }

        public function rq_removeFollow($channel_id,$user_id){
            $channel_id = $_GET['cid'];
            $user_id = $_GET['userid'];

             $l_xSql = AppController::getInstance()->getConnection();

             $l_sSql = "DELETE FROM enr_follows WHERE userid='$user_id' AND channel_id=$channel_id";



            $this->_xSQLManager->runQuery($l_sSql);

            $l_aReturnData = array();

            $l_aReturnData = $this->rq_userLikesnFollowsWithCid($user_id,$channel_id);

            $this->outputJSON(json_encode($l_aReturnData));

            return;

        }

        public function rq_userLikesnFollows($user_id, $device_id){

            $user_id = $_GET['userid'];
            $device_id = $_GET['deviceid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            if($user_id!=""){

                $l_sSql = "SELECT content_id FROM enr_like WHERE userid='$user_id'";

                $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


                $l_aReturnData = array();

                $i = 0;

                foreach($l_xData as $l_xRow){

                    $l_aReturnData['likes'][$i] = $l_xRow['content_id'];
                    $i++;
                }

                $l_rSql = "SELECT channel_id FROM enr_follows WHERE userid='$user_id'";

                $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));

                $j = 0;

                foreach($l_aData as $l_aRow){

                    $l_aReturnData['follows'][$j] = $l_aRow['channel_id'];
                    $j++;
                }
            }

            $l_qSql = "SELECT content_id FROM oflo_block WHERE userid='$user_id' OR device_id = '$device_id'";

            $l_bData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_qSql));

            $k = 0;

            foreach($l_bData as $l_bRow){

                $l_aReturnData['blocked'][$k] = $l_bRow['content_id'];
                $k++;
            }
           
            $l_aReturnData['status'] = 1;

            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }

        public function rq_userLikesnFollowsWithId($user_id,$content_id){

            $user_id = $_GET['userid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT content_id FROM enr_like WHERE userid='$user_id'";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            

            $l_aReturnData = array();

            $i = 0;

            foreach($l_xData as $l_xRow){

                $l_aReturnData['likes'][$i] = $l_xRow['content_id'];
                $i++;
            }

            $l_rSql = "SELECT channel_id FROM enr_follows WHERE userid='$user_id'";

            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));

            

       

            $j = 0;

            foreach($l_aData as $l_aRow){

                $l_aReturnData['follows'][$j] = $l_aRow['channel_id'];
                $j++;
            }

            $l_qSql = "SELECT * FROM oflo_block WHERE userid='$user_id'";

            $l_bData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_qSql));

            

       

            $k = 0;

            foreach($l_bData as $l_bRow){

                $l_aReturnData['blocked'][$k] = $l_bRow['content_id'];
                $k++;
            }

            $l_aReturnData['status'] = 1;
            $l_aReturnData['id'] = $content_id;
            $l_aReturnData['userid'] = $user_id;
            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }


        public function rq_userLikesnFollowsWithIdB($user_id,$content_id,$device_id){

            $user_id = $_GET['userid'];
            $device_id = $_GET['deviceid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            if($user_id!=""){

                $l_sSql = "SELECT content_id FROM enr_like WHERE userid='$user_id'";

                $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

                

                $l_aReturnData = array();

                $i = 0;

                foreach($l_xData as $l_xRow){

                    $l_aReturnData['likes'][$i] = $l_xRow['content_id'];
                    $i++;
                }

                $l_rSql = "SELECT channel_id FROM enr_follows WHERE userid='$user_id'";

                $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));

                

           

                $j = 0;

                foreach($l_aData as $l_aRow){

                    $l_aReturnData['follows'][$j] = $l_aRow['channel_id'];
                    $j++;
                }
            }

            $l_qSql = "SELECT * FROM oflo_block WHERE userid='$user_id' OR device_id = '$device_id'";

            $l_bData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_qSql));

            

       

            $k = 0;

            foreach($l_bData as $l_bRow){

                $l_aReturnData['blocked'][$k] = $l_bRow['content_id'];
                $k++;
            }

            $l_aReturnData['status'] = 1;
            $l_aReturnData['id'] = $content_id;
            $l_aReturnData['userid'] = $user_id;
            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }

        public function rq_userLikesnFollowsWithCid($user_id,$channel_id){

            $user_id = $_GET['userid'];
           

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "SELECT content_id FROM enr_like WHERE userid='$user_id'";

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            

            $l_aReturnData = array();

            $i = 0;

            foreach($l_xData as $l_xRow){

                $l_aReturnData['likes'][$i] = $l_xRow['content_id'];
                $i++;
            }

            $l_rSql = "SELECT channel_id FROM enr_follows WHERE userid='$user_id'";

            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));

            

       

            $j = 0;

            foreach($l_aData as $l_aRow){

                $l_aReturnData['follows'][$j] = $l_aRow['channel_id'];
                $j++;
            }

            $l_aReturnData['status'] = 1;
            $l_aReturnData['id'] = $channel_id;
            $l_aReturnData['userid'] = $user_id;

            $this->outputJSON(json_encode($l_aReturnData));

            return;
            
        }

        public function getLikes($content_id){

            

             $l_xSql = AppController::getInstance()->getConnection();

             $l_sSql = "SELECT id FROM enr_like WHERE content_id=$content_id";



             $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $like_count=0;
            foreach($l_xData as $l_xRow){
 
                $like_count++;
            }

                return $like_count;
            
        }

        public function getCommentCount($content_id){


             $l_xSql = AppController::getInstance()->getConnection();

             $l_sSql = "SELECT comment_count FROM oflo_comment_count WHERE content_id=$content_id";



             $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $l_aReturnData = array();
            $comment_count=0;
            foreach($l_xData as $l_xRow){
 
                $comment_count = $l_xRow['comment_count'];
            }

                return $comment_count;
            
        }        

        public function rq_feedOptionSearch($option, $search_term, $channel_id, $index, $count){

             $option = $_GET['op'];

             $index = $_GET['index'];

             $total_count = $_GET['count'];

         

             if($option==0){
                $channel_id = $_GET['channel_id'];
             }else if($option==1){

             }else{}

            if($search_term==""){
                $search_term = $_GET['search_term'];
            }
            $l_nChannelId = $channel_id;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                if($option==0){
                    $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.is_publish, ev.video_url as video_url, ev.video_type as video_type, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev WHERE ev.is_publish=1 AND ev.is_oflo='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) AND ev.cid=$channel_id ORDER BY ev.start_date DESC";
                }else if($option==1){
                    $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.is_publish, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw,ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, ec.slug as slug FROM enr_video ev LEFT JOIN enr_chanel ec ON ec.id = ev.cid WHERE ev.is_publish=1 AND ev.is_oflo='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) AND ev.description LIKE '%$search_term%' ORDER BY ev.start_date DESC"; 

                }else{

                }
            }
        
           //$num_rows = mysql_num_rows($this->_xSQLManager->runQuery($l_sSql));

           
           
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);
           
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){


                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                if($option==0){
                    $l_xRow['slug'] = $l_xChannel->slug;
                $l_xTheData = $this->buildStatObjectNoReports($l_xRow,$l_nMainChannelId);
                }else if($option==1){


                     $l_xTheData = $this->buildStatObjectNoReports($l_xRow,$l_xRow['cid']);
                }



                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                if($actual_count==$total_count){
                    break;
                }

            }else{
                $other_count++;
            }
                
                $count++;
            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
              $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

         public function rq_feedFollow($search_term, $channel_id, $index, $count,$user_id){


             $userid = $_GET['userid'];

             $index = $_GET['index'];

             $total_count = $_GET['count'];

         

             if($option==0){
                $channel_id = $_GET['channel_id'];
             }else if($option==1){

             }else{}

            if($search_term==""){
                $search_term = $_GET['search_term'];
            }
            $l_nChannelId = $channel_id;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                if($option==0){
                    $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid,ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev LEFT JOIN enr_follows ef ON ef.channel_id = ev.cid WHERE ev.is_publish=1 AND ev.is_oflo='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) AND ef.userid='$userid' ORDER BY ev.start_date DESC";
               

                }else{

                }
            }
        
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);
           
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){


                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                

                     $l_xTheData = $this->buildStatObjectNoReportsSlug($l_xRow,$l_xRow['cid']);
                



                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                if($actual_count==$total_count){
                    break;
                }

            }else{
                $other_count++;
            }
                
                $count++;
            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
            $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }


        public function rq_feedTrending($search_term, $channel_id, $index, $count,$user_id){


             $index = $_GET['index'];

             $total_count = $_GET['count'];

         

            
            $l_nChannelId = $channel_id;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;

            $number_month = date("m");

            $month_name = strtolower(date("F", strtotime("2001-$number_month-1")));
            
           //$l_lSql = "SELECT content_id, COUNT(content_id) AS like_total FROM enr_like GROUP BY content_id ORDER BY like_total DESC LIMIT 1";
            

                   // $l_sSql = "SELECT * FROM ( ( SELECT concat('video_', ev.id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev LEFT JOIN sc_reports_daily srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND srd.time_stamp >= DATEADD(hh, -24, GETDATE()) ) ) t1 WHERE t1.fan_id=-1";
               
                     // $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, srd.content_id as content_id, COUNT(content_id) AS like_total FROM enr_video ev RIGHT OUTER JOIN enr_like srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND ev.is_oflo='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) GROUP BY content_id ORDER BY like_total DESC";

               
                     $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, srd.content_id, (srd.page_views_ga + srd.video_views) as total_views FROM enr_video ev INNER JOIN oflo_reports_monthly srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND ev.is_oflo='y' AND srd.month='$month_name' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) ORDER BY total_views DESC";            
               
            
           //$num_rows = mysql_num_rows($this->_xSQLManager->runQuery($l_sSql));

           
           
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);
           
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;
            //$total_count = 0;

            foreach($l_xData as $l_xRow){
                

               if($count>=$index){


                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                

                     $l_xTheData = $this->buildStatObjectNoReportsSlug($l_xRow,$l_xRow['cid']);
                



                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                if($actual_count==$total_count){
                    break;
                }

            }else{
                $other_count++;
            }
                
                $count++;
            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }



        public function rq_ofloData($search_term, $channel_id, $index, $count,$user_id, $p_bBypassMemcache = false){

 $index = $_GET['index'];

             $total_count = $_GET['count'];

            
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;

            date_default_timezone_set('UTC');
         $utc_time = date("Y-m-d h:i:sa"); 

        date_default_timezone_set('America/Los_Angeles');
         $pacific_time = date("Y-m-d h:i:sa"); 
 

             
          //  $l_aCachedContent = Memcaching::getInstance()->getKey($this->l_sMemcacheKey);
           
         //   if(!$l_aCachedContent){
            
            if($l_bVideosOnly){
               //  $this->l_sMemcacheKey = Memcaching::makeKey('oflo-latest-feed');
                $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio
                        from enr_video ev inner join enr_feed ef
                        where 
                             ev.is_publish=1 and ev.id = ef.content_id and 
                            ev.is_oflo='y' and ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                    order by ev.start_date desc";
            
       
           $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


           // Memcaching::getInstance()->store($this->l_sMemcacheKey,$l_xData,30);
            
            }
            //else{
            //   $l_xData = $l_aCachedContent['value'];
            
           // }  
 //var_dump($l_aCachedContent);
            //PRIORITY
           // $l_rSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, srd.priority_number, srd.duration, srd.content_id as content_id, ec.slug as slug FROM enr_video ev INNER JOIN enr_chanel ec ON ec.id = ev.cid INNER JOIN priority srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND ev.is_oflo='y' ORDER BY RAND() LIMIT 1";


           //  $l_yData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_rSql));
           //   $l_aReturnData = array();
           //   foreach($l_yData as $l_yRow){
                
                  
           //      $l_yTheData = $this->buildStatObjectNoReportsSlugOD($l_yRow,$l_yRow['cid']);
           //       $l_aReturnData['data'][] = $l_yTheData;

                

                
           //  }
            $num_count = count($l_xData);

           
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){

               if($count>=$index){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                
                    $l_xTheData = $this->buildStatObjectNoReportsSlugOD($l_xRow,$l_xRow['cid']);
                
                
                


                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

               

                $l_aReturnData['data'][] = $l_xTheData;

            

                $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;
                

            }

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;

            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }
        }



        public function rq_feedLiked($search_term, $channel_id, $index, $count, $user_id){


             $index = $_GET['index'];

             $total_count = $_GET['count'];

            $user_id = $_GET['userid'];
           
          
            
            $l_nChannelId = $channel_id;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
           //$l_lSql = "SELECT content_id, COUNT(content_id) AS like_total FROM enr_like GROUP BY content_id ORDER BY like_total DESC LIMIT 1";
            

                   // $l_sSql = "SELECT * FROM ( ( SELECT concat('video_', ev.id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev LEFT JOIN sc_reports_daily srd ON srd.content_id = ev.id WHERE ev.is_publish=1 AND srd.time_stamp >= DATEADD(hh, -24, GETDATE()) ) ) t1 WHERE t1.fan_id=-1";
               
                    $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.deep_link_web_lp, ev.deep_link_web_oflo, ev.deep_link_web_oflo_lp, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio, srd.content_id as content_id, srd.userid FROM enr_video ev RIGHT OUTER JOIN enr_like srd ON srd.content_id = ev.id WHERE srd.userid = '$user_id' AND ev.is_publish=1 AND ev.is_oflo='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) ORDER BY ev.start_date DESC";

//               var_dump($l_sSql);  exit();

        
           //$num_rows = mysql_num_rows($this->_xSQLManager->runQuery($l_sSql));

          
      
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);
           
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            $count = 0;
            $other_count = 0;
            $actual_count = 0;

            foreach($l_xData as $l_xRow){
                

               if($count>=$index){


                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }
                

                     $l_xTheData = $this->buildStatObjectNoReportsSlug($l_xRow,$l_xRow['cid']);
                



                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

                $actual_count++;
                if($actual_count==$total_count){
                    break;
                }

            }else{
                $other_count++;
            }
                
                $count++;
            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining<$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_curateSearch($search_term){

             
                $search_term = $_GET['search_term'];

                $index = $_GET['index'];

             $total_count = $_GET['count'];
            

           //u $l_nChannelId = 64;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                $l_sSql = "SELECT ev.id as id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.cid as cid, ev.video_url as video_url, ev.video_type as video_type, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.brightcove_media_id, ev.image_aspect_ratio as image_aspect_ratio FROM enr_video ev WHERE ev.is_publish=1 AND ev.is_oflo ='y' AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR) AND (ev.description LIKE '%$search_term%' OR ev.tags LIKE '%$search_term%') ORDER BY ev.start_date DESC";
            }
        
           
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            $num_count = count($l_xData);

            
            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;

             $count = 0;
            $other_count = 0;
            $actual_count = 0;
            
            foreach($l_xData as $l_xRow){

                if($count>=$index){

                    if($l_nMainChannelId == 15){
                        $l_nWeeklyPostRate = 5;
                    }else{
                        if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                    }

                    $l_xTheData = $this->buildStatObjectNoReportsSlug($l_xRow,$l_xRow['cid']);
                   

                    $l_sDays = time() - strtotime($l_xRow['start_date']);
                    $l_sDays = floor($l_sDays/(60*60*24));
                    
                    if($l_sDays <= 30){
                        $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                        $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                        $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                        $l_nDaysUnder30++;
                    }

                    $l_aReturnData['data'][] = $l_xTheData;
                    
                    $actual_count++;
                    if($actual_count==$total_count){
                        break;
                    }

                }else{
                    $other_count++;
                }
                
                $count++;

            }
           

            $l_aReturnData['status'] = 1;
            $l_aReturnData['count'] = $actual_count;
            $l_aReturnData['total_count'] = $num_count;
            $remaining = $num_count - ($actual_count + $index);
            if($remaining>$actual_count){
             $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }else{
                $l_aReturnData['remaining'] = $num_count - ($actual_count + $index);
            }

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        private function rq_updatePushDeviceId(&$p_aArgs){

            Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_bAppType = $p_aArgs['type'];
            $l_xUserManager = new UserManager();     

            $l_aLocationData = $p_aArgs['loc'];
            $l_aKeyValue = array();
            $l_aKeyValue['city'] = $l_aLocationData['city'];
            $l_aKeyValue['country'] = $l_aLocationData['country'];
            $l_aKeyValue['country_code'] = $l_aLocationData['countryCode'];
            $l_aKeyValue['isp'] = $l_aLocationData['isp'];
            $l_aKeyValue['lat'] = $l_aLocationData['lat'];
            $l_aKeyValue['lng'] = $l_aLocationData['lon'];
            $l_aKeyValue['ip_address'] = $l_aLocationData['query'];
            $l_aKeyValue['region'] = $l_aLocationData['region'];
            $l_aKeyValue['region_name'] = $l_aLocationData['regionName'];
            $l_aKeyValue['timezone'] = $l_aLocationData['timezone'];
            $l_aKeyValue['zip'] = $l_aLocationData['zip'];
            $l_aKeyValue['device_id'] = $l_sDeviceToken;
            $l_aKeyValue['type'] = $l_bAppType;
            $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           

        }

        private function rq_updatePushDeviceIdOflo(&$p_aArgs){

            Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_nUserId = $p_aArgs['device_id'];
             $l_bAppType = $p_aArgs['type'];
            $l_xUserManager = new UserManager();  
                

            $l_aLocationData = $p_aArgs['loc'];
            $l_aKeyValue = array();
            $l_aKeyValue['city'] = $l_aLocationData['city'];
            $l_aKeyValue['country'] = $l_aLocationData['country'];
            $l_aKeyValue['country_code'] = $l_aLocationData['countryCode'];
            $l_aKeyValue['isp'] = $l_aLocationData['isp'];
            $l_aKeyValue['lat'] = $l_aLocationData['lat'];
            $l_aKeyValue['lng'] = $l_aLocationData['lon'];
            $l_aKeyValue['ip_address'] = $l_aLocationData['query'];
            $l_aKeyValue['region'] = $l_aLocationData['region'];
            $l_aKeyValue['region_name'] = $l_aLocationData['regionName'];
            $l_aKeyValue['timezone'] = $l_aLocationData['timezone'];
            $l_aKeyValue['zip'] = $l_aLocationData['zip'];
            $l_aKeyValue['device_id'] = $l_sDeviceToken;
            $l_aKeyValue['type'] = $l_bAppType;
            $l_xUserManager->updateProfileOflo($l_nUserId,$l_sDeviceToken);           

        }


        private function rq_updatePushLoginOflo(&$p_aArgs){

             Utils::closeBufferAndContinueProcess('{"status": "1"}');
            // $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_nUserId = $p_aArgs['device_id'];
             $l_bAppType = $p_aArgs['type'];
             $l_sLoginId = $p_aArgs['login_id'];
            


            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = "INSERT INTO device_login (device_id,login_id,type) VALUES ('$l_nUserId','$l_sLoginId','$l_bAppType') ON DUPLICATE KEY UPDATE login_id = '$l_sLoginId'";
            $l_xSql->runQuery($l_sSql);

            return;



        }

        private function rq_deletePost(&$p_aArgs){
            
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_nVideoId = $p_aArgs['video_id'];
            $l_nPhotoId = $p_aArgs['photo_id'];
            $l_nItemType = strtolower(trim($p_aArgs['item_type']));

            $l_xSql = AppController::getInstance()->getConnection();
            if($l_nItemType == 'video'){
                $l_sSql = 'DELETE FROM enr_feed WHERE LOWER(post_type) = "'.$l_nItemType.'" AND content_id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
                $l_sSql = 'DELETE FROM enr_video WHERE id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
            }else{
                $l_sSql = 'DELETE FROM enr_feed WHERE LOWER(post_type) = "'.$l_nItemType.'" AND content_id = '.$l_nPhotoId;
                $l_xSql->runQuery($l_sSql);
                $l_sSql = 'DELETE FROM enr_photo WHERE id = '.$l_nPhotoId;
                $l_xSql->runQuery($l_sSql);
            }

            // if($l_nChannelId == 19){


            // }
            

        }


         private function rq_updatePost(&$p_aArgs){

            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_nVideoId = $p_aArgs['video_id'];
            $l_nPhotoId = $p_aArgs['photo_id'];
            $l_nItemType = strtolower(trim($p_aArgs['item_type']));
            $l_sMessage = trim($p_aArgs['msg']);

            $l_xSql = AppController::getInstance()->getConnection();
            if($l_nItemType == 'video'){
                
                $l_sSql = 'UPDATE enr_video SET description = "'.$l_xSql->db_escape_string($l_sMessage).'" WHERE cid = '.$l_nChannelId.' AND id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
            }
        }




        private function rq_post(&$p_aArgs){
            
            // print_R($_GET);
            // print_R($_POST);
            // print_R($p_aArgs);

            // exit();

            $l_aPairingData = json_decode(base64_decode($p_aArgs['pairing_data']));
            $l_sAssetType = strtoupper(Utils::cleanPostDataValue($p_aArgs['type']));
            $l_sDescription = Utils::cleanPostDataValue($p_aArgs['description']);
            $l_nChannelId = $p_aArgs['channel_id'];

//            echo "\$l_aPairingData: ";
//            print_r($l_aPairingData);
            // echo $l_nChannelId;
            // exit;

            if($l_nChannelId == 15){
                //enrique block
                Utils::closeBufferAndContinueProcess('{"status": "1"}');
                exit;
            }
            
            $l_nAssetTypeId = ($l_sAssetType == 'PHOTO') ? CONTENT_TYPE_ID_PHOTO : CONTENT_TYPE_ID_VIDEO;


            $l_xChannelManager = new ChannelManager();
// $l_nChannelId  = 6;
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
             // print_R($l_xChannel);
             // exit;


           $l_bCLOSE_BUFFER = 1;

            if($l_nChannelId == 17){
                // $l_bCLOSE_BUFFER = 0;
            }
            // echo "FAIL";
            //Utils::closeBufferAndContinueProcess('{"status": "1"}');

            
            
            switch($l_sAssetType){

               
                case 'PHOTO':
              
                    if(!$_FILES['data'])
                        return false;

                    if(!preg_match("/(image\/)|(jpe?g|gif|png)/i",strtolower($_FILES['data']['type']))){
                        return false;
                    }
                    if($l_bCLOSE_BUFFER)
                        Utils::closeBufferAndContinueProcess('{"status": "1"}');

                    $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/photo';
                    

                    if($l_sNewRelativeFilePath = Utils::uploadFileAndReturnRelativePathOnSuccess($l_sFolderBase,$l_nChannelId,$_FILES['data'])){

                        $l_sNewAbsoluteFilePath = FILES_PATH.$l_sNewRelativeFilePath;
                        $l_sNewAbsoluteFilePath = Utils::copyAndFixPhotoRotation($l_sNewAbsoluteFilePath);

                        
                        Utils::buildImageSizesAndCrop($l_sNewAbsoluteFilePath,array(THUMBNAIL_USER_AVATAR,THUMBNAIL_PHOTO_THUMBNAIL,THUMBNAIL_PHOTO_SIZED));
                        $l_sS3FilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sNewRelativeFilePath);
                        $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/photo\//","/photo/",$l_sS3FilePath));
                        $l_sS3ImageFilePath = $l_sS3FilePath;
                           //todo - send Thumbnails

                        $l_aParts = pathinfo($l_sNewAbsoluteFilePath);
                        $l_sThumbnailPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'.jpg';
                        $l_sThumbnailPathBig = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_big.jpg';
                        $l_sThumbnailPathMed = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_med.jpg';
                        $l_sThumbnailPathSmall = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_small.jpg';

                        $l_sThumbnailPathAvatar = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_avatar.jpg';
                        $l_sThumbnailPathThumb = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_thumb.jpg';
                        $l_sThumbnailPathSized = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_sized.jpg';

                        require_once(RESIZE_IMAGE_CLASS);

                        $image = new resize($l_sThumbnailPath);
                        $image->resizeImage(1200,1200,'crop');
                        $image->saveImage($l_sThumbnailPath ,88);
                        $image->resizeImage(640,640,'crop');
                        $image->saveImage($l_sThumbnailPathBig ,88);

                        $image->resizeImage(360,360,'crop');
                        $image->saveImage($l_sThumbnailPathMed ,88);

                        $image->resizeImage(180,180,'crop');
                        $image->saveImage($l_sThumbnailPathSmall ,88);

                        list($width,$height)=getimagesize($l_sNewAbsoluteFilePath);
                        $aspect_ratio = $height / $width;

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_photo (cid) VALUES ('.$l_xChannel->id.')';   
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();
                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $l_sStoragePath;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['time_stamp'] = time();
                        $temp_date = date('Y-m-d H:i:s');
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                        $l_aKeyValues['is_publish'] = 1;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.'.$l_xChannel->slug.'://'.$l_xChannel->id.'/photo/'.$l_nInsertId;
                       
               
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'INLINE');
                        $l_sBitlyLandingPageOf = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_PHOTO,'LANDING_PAGE');
                        $l_sBitlyLandingPageOflo = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_PHOTO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
                        $l_aKeyValues['deep_link_web_oflo_lp'] = $l_sBitlyLandingPageOf;
                        $l_aKeyValues['deep_link_web_oflo'] = $l_sBitlyLandingPageOflo;


                        $l_aKeyValues['is_oflo'] = 'y';
                        $l_aKeyValues['image_aspect_ratio'] = $aspect_ratio;

                        
                        $l_aCleanValues = array();
                        foreach($l_aKeyValues as $k => $v){
                            $l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
                        }
                        $l_sSql = 'UPDATE enr_photo SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
                        $l_xSql->runQuery($l_sSql);

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'photo',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }

                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);

                        //TODO
                        //watermark
                        $l_sS3FilePath = str_replace("assets/ufile/photo","assets/ufile/watermark/photo",$l_sS3FilePath);
                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);

                        $l_sS3ImageFilePathBig = str_replace("assets/ufile/photo","assets/ufile/big_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathBig,$l_sS3ImageFilePathBig);

                        $l_sS3ImageFilePathMed = str_replace("assets/ufile/photo","assets/ufile/thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathMed,$l_sS3ImageFilePathMed);

                        $l_sS3ImageFilePathSmall = str_replace("assets/ufile/photo","assets/ufile/small_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathSmall,$l_sS3ImageFilePathSmall);
                        

                        //Wait for photo on Cloudfront
                        sleep(2);
                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        $this->logItemToFeed($l_nInsertId,'photo');

                        unlink($l_sThumbnailPath);
                        unlink($l_sThumbnailPathBig);
                        unlink($l_sThumbnailPathMed);
                        unlink($l_sThumbnailPathSmall);

                        unlink($l_sThumbnailPathAvatar);
                        unlink($l_sThumbnailPathThumb);
                        unlink($l_sThumbnailPathSized);
                    }

                    $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Photo: '.$l_xChannel->title);

                    break;

                
                case 'VIDEO':
               
                    if(!$_FILES['data'])
                        return false;
                    
                    if(!preg_match("/video\/|(mp4|quicktime|mov)/i",strtolower($_FILES['data']['type']))){
                        return false;
                    }


                    define('ZENCODER_API_KEY','b34cc8e8b92ae58bf3f1a151608a0ef6');
                    define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');
                    define('ZENCODER_PROCESS_VIDEO_SCRIPT',LIB_PATH.'zencoder/clProcessVideo.php');
                    
                    


                      if($l_bCLOSE_BUFFER)
                         Utils::closeBufferAndContinueProcess('{"status": "1"}');

                    $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/video';
              
                    if($l_sNewRelativeFilePath = Utils::uploadFileAndReturnRelativePathOnSuccess($l_sFolderBase,$l_nChannelId,$_FILES['data'])){

                        $l_sNewAbsoluteFilePath = FILES_PATH.$l_sNewRelativeFilePath;

                        // list($l_aMetaData,$l_xFileInfo) = Utils::getMediaMetaInfo($l_sNewAbsoluteFilePath);
                        $l_aParts = pathinfo($l_sNewAbsoluteFilePath);
                        $l_sThumbnailPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'.jpg';
                        $l_sThumbnailPathBig = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_big.jpg';
                        $l_sThumbnailPathMed = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_med.jpg';
                        $l_sThumbnailPathSmall = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_small.jpg';
                        $l_sThumbnailPathSquare = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_square.jpg';

                        $l_sThumbnailWatermarkPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermark.jpg';
                        $l_sThumbnailWatermarkPathBig = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermarkbig.jpg';
                        $l_sThumbnailWatermarkPathMed = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermarkmed.jpg';
                        $l_sThumbnailWatermarkPathSmall = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermarksmall.jpg';
                        $l_sThumbnailWatermarkPathSquare = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermarksquare.jpg';

                        $l_sTempVideoPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_temp.mov';
                        $l_sThumbnailPath = str_replace("//","/",$l_sThumbnailPath);
                       // $l_sThumbnailWatermarkPath = str_replace("//","/",$l_sThumbnailWatermarkPath);
                        $l_sTempVideoPath = str_replace("//","/",$l_sTempVideoPath);
                        
                        if(move_uploaded_file($_FILES['thumb']['tmp_name'],$l_sThumbnailPath)){
                            $l_sThumbnailPath = Utils::copyAndFixPhotoRotation($l_sThumbnailPath);
                        }

                        list($widthar,$heightar)=getimagesize($l_sThumbnailPath);
                        $aspect_ratio = $heightar / $widthar;


                        $big_height = $aspect_ratio*1200;
                        $mid_height = $aspect_ratio*640;
                        $small_height = $aspect_ratio*360;
                        $tiny_height = $aspect_ratio*180;

                        require_once(RESIZE_IMAGE_CLASS);

                        $image = new resize($l_sThumbnailPath);
                        $image->resizeImage(1200,$big_height,'crop');
                        $image->saveImage($l_sThumbnailPath ,88);
                        $image->resizeImage(640,$mid_height,'crop');
                        $image->saveImage($l_sThumbnailPathBig ,88);

                        $image->resizeImage(360,$small_height,'crop');
                        $image->saveImage($l_sThumbnailPathMed ,88);

                        $image->resizeImage(180,$tiny_height,'crop');
                        $image->saveImage($l_sThumbnailPathSmall ,88);

                        $image->resizeImage(180,180,'crop');
                        $image->saveImage($l_sThumbnailPathSquare ,88);

                        


                        echo $l_sThumbnailPath;
                        echo "\n;";
                        echo "CREATE WATER";
                        echo "\n;";
                        $l_xImage = imagecreatefromjpeg($l_sThumbnailPath);
                        $l_xWatermark = imagecreatefrompng(__DIR__.'/_watermark.png');
                        $l_nWatermarkWidth = imagesx($l_xWatermark);
                        $l_nWatermarkHeight = imagesy($l_xWatermark);
                        $l_nImageWidth = imagesx($l_xImage);
                        $l_nImageHeight = imagesy($l_xImage);
                        
                        imagecopy($l_xImage, $l_xWatermark, $l_nImageWidth/2 - $l_nWatermarkWidth/2,$l_nImageHeight/2 - $l_nWatermarkHeight/2 , 0, 0, imagesx($l_xWatermark), imagesy($l_xWatermark));
                        imagejpeg($l_xImage,$l_sThumbnailWatermarkPath,88);

                        imagedestroy($l_xImage);

                        $l_sS3VideoFilePath = $l_sNewRelativeFilePath;
                        $l_sS3ImageFilePath = preg_replace("/.+files\//","",$l_sThumbnailPath);
                        $l_sS3ImageFilePath = str_replace("assets/ufile/video","assets/ufile/photo",$l_sS3ImageFilePath);
                        $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/video\//","/video/",$l_sS3VideoFilePath));

    
                        $l_sS3ThumbnailFilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sThumbnailPath);
                        $l_sStorageThumbnailPath = preg_replace("/^\//","",preg_replace("/.+ufile\/video\//","/photo/",$l_sS3ThumbnailFilePath));
 

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_video (cid, is_publish) VALUES ('.$l_xChannel->id.',0)';   
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();

                        // ffprobe
                        $cmd = "/usr/bin/ffprobe -v quiet -of flat -select_streams v:0 -show_entries stream=width,height,duration ".$l_sNewAbsoluteFilePath." | sed -n 's/streams.stream.0.//p'";
                        exec($cmd, $output, $cvh_return_var);

                        $output_implode = implode("&", $output);
                        $output_implode = str_replace('"', '', $output_implode);
                        parse_str($output_implode, $output_parsed);

                        $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$l_sNewAbsoluteFilePath.' '.$l_nInsertId.' '.$output_parsed['height'];
                        echo $l_sCommand;
                        exec($l_sCommand . " > /dev/null &");       


                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $l_sStoragePath;
                        $l_aKeyValues['video_width']    = $output_parsed['width'];
                        $l_aKeyValues['video_height']   = $output_parsed['height'];
                        $l_aKeyValues['video_duration'] = $output_parsed['duration'];
                        $l_aKeyValues['image_path'] = $l_sStorageThumbnailPath;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['time_stamp'] = time();
                        $temp_date = date('Y-m-d H:i:s');
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                       // $l_aKeyValues['is_publish'] = 0;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.oflopro://'.$l_xChannel->id.'/video/'.$l_nInsertId;
                        $l_aKeyValues['image_aspect_ratio'] = $aspect_ratio;
                        $l_aKeyValues['video_url'] = 0;
                        
                        $l_aKeyValues['is_oflo'] = 'y';
                        
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');
                        $l_sBitlyLandingPageOf = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyLandingPageOflo = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
                        $l_aKeyValues['deep_link_web_oflo_lp'] = $l_sBitlyLandingPageOf;
                        $l_aKeyValues['deep_link_web_oflo'] = $l_sBitlyLandingPageOflo;
                        
                        
                        $l_aCleanValues = array();
                        foreach($l_aKeyValues as $k => $v){
                            $l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
                        }
                        $l_sSql = 'UPDATE enr_video SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
                        $l_xSql->runQuery($l_sSql);

                        
                        //send video to be processed
                        // $l_nBCId = $this->sendVideoToBrightcove($l_sNewAbsoluteFilePath,$l_sDescription,$l_xChannel);


                        // //echo "BC ID: $l_nBCId";
                        // $l_sSql = 'UPDATE enr_video SET brightcove_media_id = "'.$l_nBCId.'" WHERE id = '.$l_nInsertId;
                        // $l_xSql->runQuery($l_sSql);

                        
                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3VideoFilePath);

                        //thumbnail
                        Utils::pushFileToS3($l_sThumbnailPath,$l_sS3ImageFilePath);

                        $l_sS3ImageFilePathBig = str_replace("assets/ufile/photo","assets/ufile/big_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathBig,$l_sS3ImageFilePathBig);

                        $l_sS3ImageFilePathMed = str_replace("assets/ufile/photo","assets/ufile/thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathMed,$l_sS3ImageFilePathMed);

                        $l_sS3ImageFilePathSmall = str_replace("assets/ufile/photo","assets/ufile/small_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathSmall,$l_sS3ImageFilePathSmall);

                        $l_sS3ImageFilePathSquare = str_replace("assets/ufile/photo","assets/ufile/small_square_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailPathSquare,$l_sS3ImageFilePathSquare);

/*
                        $filename = $l_sThumbnailWatermarkPath;

                        // Get new sizes
                        list($width, $height) = getimagesize($filename);
                        $newwidth = 1200;
                        $newheight = 640;
                        // Load
                        $thumb = imagecreatetruecolor($newwidth, $newheight);
                        $source = imagecreatefromjpeg($filename);

                        // Resize
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                        // Output
                        imagejpeg($thumb,$l_sThumbnailWatermarkPath,88); */

                        //TODO
                        //watermark
                        $l_sS3ImageWmFilePath = str_replace("assets/ufile/photo","assets/ufile/watermark/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailWatermarkPath,$l_sS3ImageWmFilePath);


                        $imagewm = new resize($l_sThumbnailWatermarkPath);

                        $imagewm->resizeImage(640,341,'crop');
                        $imagewm->saveImage($l_sThumbnailWatermarkPathBig ,88);

                        $imagewm->resizeImage(360,192,'crop');
                        $imagewm->saveImage($l_sThumbnailWatermarkPathMed ,88);

                        $imagewm->resizeImage(180,96,'crop');
                        $imagewm->saveImage($l_sThumbnailWatermarkPathSmall ,88);

                        $l_sS3ImageFilePathBig = str_replace("assets/ufile/photo","assets/ufile/watermark/big_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailWatermarkPathBig,$l_sS3ImageFilePathBig);

                        $l_sS3ImageFilePathMed = str_replace("assets/ufile/photo","assets/ufile/watermark/thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailWatermarkPathMed,$l_sS3ImageFilePathMed);

                        $l_sS3ImageFilePathSmall = str_replace("assets/ufile/photo","assets/ufile/watermark/small_thumb/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailWatermarkPathSmall,$l_sS3ImageFilePathSmall);
                        

                        sleep(30);
                        $this->logItemToFeed($l_nInsertId,'video');
                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'video',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }

                        unlink($l_sNewAbsoluteFilePath);
//                        unlink($l_sTempVideoPath);

                        unlink($l_sThumbnailPath);
                        unlink($l_sThumbnailPathBig);
                        unlink($l_sThumbnailPathMed);
                        unlink($l_sThumbnailPathSmall);
                        unlink($l_sThumbnailPathSquare);

                        unlink($l_sThumbnailWatermarkPath);
                        unlink($l_sThumbnailWatermarkPathBig);
                        unlink($l_sThumbnailWatermarkPathMed);
                        unlink($l_sThumbnailWatermarkPathSmall);

                        $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Video: '.$l_xChannel->title);
                        //return true;
 
                    }        
                    break;
                
                default:
                    return false;
                    break;
            }


        }   



        private function rq_curatePost(&$p_aArgs){

           
    
            $l_aPairingData = json_decode(base64_decode($p_aArgs['pairing_data']));
            $l_sAssetType = strtoupper(Utils::cleanPostDataValue($p_aArgs['type']));
            $l_sDescription = Utils::cleanPostDataValue($p_aArgs['description']);
            $l_nViralId = $p_aArgs['viral_id'];


            $l_nChannelId = $p_aArgs['channel_id'];
                   
            $l_nAssetTypeId = CONTENT_TYPE_ID_VIDEO;
            $l_xChannelManager = new ChannelManager();

            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
         
            $l_bCLOSE_BUFFER = 1;

            if($l_bCLOSE_BUFFER)
                        Utils::closeBufferAndContinueProcess('{"status": "1"}');

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'SELECT * FROM enr_video WHERE id = '.$l_nViralId.'';   
                        $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));



                        foreach($l_aData as $l_xRow){
                            $new_destination = $l_xRow['destination'];
                            $new_image_path = $l_xRow['image_path'];
                        }



                        $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/video';
              
                        $new_path = "".$l_xChannel->slug."/assets/ufile/".$new_destination;
                      $new_photo_path = "".$l_xChannel->slug."/assets/ufile/".$new_image_path;
                      $new_watermark_path = "".$l_xChannel->slug."/assets/ufile/watermark/".$new_image_path;

                      $new_photo_path_small = "".$l_xChannel->slug."/assets/ufile/small_thumb/".$new_image_path;
                      $new_watermark_path_small = "".$l_xChannel->slug."/assets/ufile/watermark/small_thumb/".$new_image_path;

                      $new_photo_path_med = "".$l_xChannel->slug."/assets/ufile/thumb/".$new_image_path;
                      $new_watermark_path_med = "".$l_xChannel->slug."/assets/ufile/watermark/thumb/".$new_image_path;

                      $new_photo_path_big = "".$l_xChannel->slug."/assets/ufile/big_thumb/".$new_image_path;
                      $new_watermark_path_big = "".$l_xChannel->slug."/assets/ufile/watermark/big_thumb/".$new_image_path;


                      $filebroken = explode( '.', $new_destination);
                      $extension = array_pop($filebroken);
                      $fileTypeless = implode('.', $filebroken);
                      $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8");
                      $middle = Utils::GetBetween("file-", ".m3u8", $nf->body);

                      Utils::copyFileOnS3('viral/assets/ufile/'.$new_destination,$new_path);
                      Utils::copyFileOnS3('viral/assets/ufile/'.$new_image_path,$new_photo_path);
                      Utils::copyFileOnS3('viral/assets/ufile/watermark/'.$new_image_path,$new_watermark_path);

                      Utils::copyFileOnS3('viral/assets/ufile/small_thumb/'.$new_image_path,$new_photo_path_small);
                      Utils::copyFileOnS3('viral/assets/ufile/watermark/small_thumb/'.$new_image_path,$new_watermark_path_small);

                      Utils::copyFileOnS3('viral/assets/ufile/thumb/'.$new_image_path,$new_photo_path_med);
                      Utils::copyFileOnS3('viral/assets/ufile/watermark/thumb/'.$new_image_path,$new_watermark_path_med);

                      Utils::copyFileOnS3('viral/assets/ufile/big_thumb/'.$new_image_path,$new_photo_path_big);
                      Utils::copyFileOnS3('viral/assets/ufile/watermark/big_thumb/'.$new_image_path,$new_watermark_path_big);


                      $l_sNewAbsoluteFilePath = FILES_PATH.$new_path;

                      list($widthar,$heightar)=getimagesize($l_sNewAbsoluteFilePath);
                        $aspect_ratio = $heightar / $widthar;
                    
                      $playlist = sizeof($middle[1]);
                      for($j=0;$j<$playlist;$j++){
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8");
                          for($i=1;$i<10;$i++){
                            $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                            if($nf!=false){
                            Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                            }else{
                              break;
                            }
                          }
                      }
                      /*    TO DO -  Fix  bitrates  */
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/master.m3u8");
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-1080.mp4");
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-1080.webm");          
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-720.mp4");         
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-720.webm");     
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-480.mp4");             
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-480.webm");              
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-360.mp4");             
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-360.webm");
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-216.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-216.mp4");             
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-216.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-216.webm");
                      
 

                        //$l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_video (cid) VALUES ('.$l_xChannel->id.')';   
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();   


                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $new_destination;
                        $l_aKeyValues['image_path'] = $new_image_path;
                        //$l_aKeyValues['thumbnail'] = $new_image_path;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['image_aspect_ratio'] = $aspect_ratio;
                        $l_aKeyValues['time_stamp'] = time();
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                        $l_aKeyValues['is_publish'] = 1;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.'.$l_xChannel->slug.'://'.$l_xChannel->id.'/video/'.$l_nInsertId;
                        $l_aKeyValues['is_oflo'] = 'y';
                        $l_aKeyValues['video_url'] = 0;
                        
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');
                        $l_sBitlyLandingPageOf = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyLandingPageOflo = $l_xChannel-> getBitlyURLForAssetIdAndTypeOf($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
                        $l_aKeyValues['deep_link_web_oflo_lp'] = $l_sBitlyLandingPageOf;
                        $l_aKeyValues['deep_link_web_oflo'] = $l_sBitlyLandingPageOflo;
                        
                        //var_dump($l_aKeyValues);

                        $l_aCleanValues = array();
                        foreach($l_aKeyValues as $k => $v){
                            $l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
                        }
                        $l_sSql = 'UPDATE enr_video SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
                        $l_xSql->runQuery($l_sSql);

                        $l_sSql = 'INSERT INTO rev_share_post (viral_id,content_id,cid) VALUES ('.$l_nViralId.','.$l_nInsertId.','.$l_xChannel->id.')';
                        $l_xSql->runQuery($l_sSql);

                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        $this->logItemToFeed($l_nInsertId,'video');

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'video',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }

                        //sleep(30);

                        foreach($l_aPairingData as $l_xRow){

                           $l_sSql = 'SELECT id FROM enr_content_post_to_social WHERE content_id = '.$l_nInsertId.'';
                           $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

                            foreach($l_aData as $l_xORow){
                                $pts_id = $l_xORow['id'];
                                 $this->postToSocialNetwork($pts_id); 
                                
                            }

                        }


                        $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Video: '.$l_xChannel->title);
                        return true;


        }   


        //  public function getOAuthForServiceByCreatorId($p_sService,$p_nCreatorsId){
            
        //     $l_sSql = 'SELECT * FROM oauth_keys WHERE creators_id = '.intval($p_nCreatorsId).'
        //                 AND service = "'.$this->db_escape_string($p_sService).'"';

        //     $l_xData = $this->getRow($this->runQuery($l_sSql));
        //     if($l_xData){
        //         $l_xData['token'] = Utils::decryptItemId($l_xData['token']);
        //         $l_xData['secret'] = Utils::decryptItemId($l_xData['secret']);
        //     }
        //     return $l_xData;
        // }

        private function storePostToSocialData($p_nChannelId,$p_sMainDescription,$p_nAssetId,$p_nAssetTypeId,$p_aPairingData){

             $l_nAssetId = $p_nAssetId;
             $l_xSql = AppController::getInstance()->getConnection();

            foreach($p_aPairingData as $l_xRow){
                $l_xRow = (array)$l_xRow;
                
                $l_nPropertyId = intval($l_xRow['property_id']);
                $l_sMessage = $p_sMainDescription;
                $l_bMessageCustom = ($l_xRow['message_custom'] == 'Y') ? true : false;
                $l_bActive = ($l_xRow['active'] == 'Y') ? true : false;

                if($l_bMessageCustom){
                    $l_sMessage = ($l_xRow['message']);
                }

                print_r($l_xRow);

                if($l_bActive){

                    $l_sSql = 'INSERT INTO enr_content_post_to_social(post_type_id,channel_id,content_id,social_property_type_id,channel_oauth_id,message,has_sent,created) 
                                VALUES ('.$p_nAssetTypeId.',
                                    '.$p_nChannelId.',
                                    '.$l_nAssetId.',
                                    '.$l_nPropertyId.',
                                    "",
                                    "'.$l_xSql->cleanValue($l_sMessage).'",
                                    "N",
                                    NOW()
                                    )';   
                    echo $l_sSql;
                    $l_xSql->runQuery($l_sSql);
                    $l_nInsertId = $l_xSql->db_last_insert_id();

                    //TODO Fork this process
                    //If post is Photo --- send right awawy 
                    
                        $this->postToSocialNetwork($l_nInsertId); 
                    

                }

            }
        }

        private function rq_testSocial(){
            $l_nInsertId = $_GET['id'];
            echo $l_nInsertId;
            
            $this->postToSocialNetwork($l_nInsertId); 
        }

        private function rq_testPush(){
            $l_sMsg = $_GET['msg'];
            $l_xReponse = $this->sendPush(array(PUSH_LEVEL_ADMIN),$l_sMsg);
            // $l_xReponse = $this->sendPush(array(PUSH_LEVEL_ADMIN,PUSH_LEVEL_DEV),$l_sMsg);
            echo $l_xReponse;
            echo "Push Sent";
        }

        private function rq_testPush2(){
            $l_sMsg = $_GET['msg'];
            require_once(LIB_PATH.'tuscano/VTPush.php');
            $l_sDomain = 'oflo';
            $l_aTokens = array();
            $l_aTokens[] = '730b3166b52071b4163435d783eee94eaa9755a4bbd5fce706808cd319488d0b';
            $l_sMessage = $p_sMessage;
            echo  $l_sDomain;
            print_r($l_aTokens);

            $l_xPush = new VTPush();
            $l_xPush->setCert($l_sDomain);
            $l_xPush->setMessage($l_sMessage);
            $l_xPush->setDeviceIds($l_aTokens);
            $l_xResponse = $l_xPush->pushOut();
            print_R($l_xResponse);

        }

        private function rq_testRollUp(){
            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = '

            SELECT 
            SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "video" THEN 1 END) as total_videos,
            SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "photo" THEN 1 END) as total_photos,
            
            MAX(t1.time_stamp) as lastpost,
            
            t1.*
            FROM (
                SELECT v.id as item_id, "video" as the_type, c.id as channel_id, c.*, v.time_stamp

                        
                        FROM enr_chanel c

                        LEFT JOIN enr_video v 
                        ON c.id = v.cid

                        WHERE c.id >= 37 AND c.id NOT IN (58)
                        AND v.time_stamp > '.time().' - 86400


                        
            UNION 
                SELECT p.id as item_id, "photo" as the_type, c.id as channel_id, c.*, p.time_stamp
                        
                         FROM enr_chanel c

                        LEFT JOIN enr_photo p 
                        ON c.id = p.cid

                        WHERE c.id >= 37 AND c.id NOT IN (58)
                        AND p.time_stamp > '.time().' - 86400
                        ) t1 
            GROUP BY t1.channel_id
            ORDER BY t1.channel_id ASC, t1.created ASC

            ';
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            $l_nTotalVideos = 0;
            $l_nTotalPhotos = 0;

            foreach($l_aData as $l_xRow){
                $l_nTotalVideos += $l_xRow['total_videos'];
                $l_nTotalPhotos += $l_xRow['total_photos'];
            }
            $l_sMessage = "24h Posted Summary: Photos: $l_nTotalPhotos, Videos: $l_nTotalVideos";
            $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),$l_sMessage);
    
        }

        private function rq_flushChannel(){
        
            $l_sSlug = $_GET['slug'];
            echo $l_sSlug;
            echo Memcaching::makeKey($l_sSlug);
            $l_aCachedContent = Memcaching::getInstance()->getKey(Memcaching::makeKey($l_sSlug));
            if($l_aCachedContent){
                print_r($l_aCachedContent);
                echo "FLUSH";
                Memcaching::getInstance()->removeKey(Memcaching::makeKey($l_sSlug));
            }else{
                echo "No mem";
            }
            exit;
        }

        public function logItemToFeed($p_nAssetId,$p_sType){
            
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'INSERT INTO enr_feed (post_type,content_id,time_stamp) VALUES ("'.$p_sType.'",'.$p_nAssetId.',"'.time().'")';
            $l_xSql->runQuery($l_sSql);
        }

        private function rq_testVideoStatus(){
            $l_nInsertId = $_GET['id'];
            
            require_once(LIB_PATH.'brightcove/SCBrightcove.php');
            $l_xBC = new SCBrightcove();
            $l_xBC->isVideoReady($l_nInsertId);
            exit;
        }

        private function rq_updateSocialKeyStatus(&$p_aArgs){
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_aProperty = $p_aArgs['property'];
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'UPDATE enr_channel_oauth SET active = "'.$l_aProperty['active'].'", modified = NOW() WHERE id = '.$l_aProperty['id'];
            $l_xSql->runQuery($l_sSql);
        }


        public function facebookOauth(){

            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';

            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));

                           // $l_xFacebook->setExtendedAccessToken();
                           // $l_nPageId = $l_xRow['page_id']; 

                           // $l_xFacebook->api("/dialog/oauth/","get");

                            $user = $l_xFacebook->getUser();

                            //var_dump($user);

                            $token = $l_xFacebook->getAccessToken();

                            $graph_url   =  "http://www.facebook.com/dialog/oauth?";
                            $graph_url  .=  "client_id=".$l_sFacebookAppId;
                            $graph_url  .=  "&redirect_uri=http://geek.starsite.com/api/test.php";

                            //var_dump($graph_url);

                            $response   = @file_get_contents($graph_url);  

                            //var_dump($reponse);

                            $graph_url   =  "https://graph.facebook.com/oauth/access_token?";
                            $graph_url  .=  "client_id=".$l_sFacebookAppId;
                            $graph_url  .=  "&client_secret=".$l_sFacebookAppSecret;
                            $graph_url  .=  "&grant_type=fb_exchange_token";
                            $graph_url  .=  "&fb_exchange_token=".$token;

                            //var_dump($graph_url);

                            $response   = @file_get_contents($graph_url);  

                            //var_dump($reponse);

                            //$l_aArgs = array(
                             //   'access_token'  => $token
                            // );

                           // $l_xFacebook->setAccessToken($token);

                         // print_r($l_aArgs);
                         // exit;
                           // $user_admin_pages = $l_xFacebook->api("/me/accounts/","get",$l_aArgs);

            
           // $user_admin_pages = $facebok->api('/me/accounts')

            //$reponse = $facebook->api($token, '/me/accounts?fields=name','get');

            //$response = $facebook->api($pageID . '/feed','get',$token);

           //var_dump($user_admin_pages); exit();
        }

        public function twitterOauth(){

            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

        }

        private function rq_storeSocialKeys(&$p_aArgs){
            
            
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];

            $l_xSql = AppController::getInstance()->getConnection();

            $l_aPairingData = json_decode($p_aArgs['pairing_data']);
            $l_sPairingService = strtoupper($p_aArgs['pairing_service']);

            if($l_sPairingService == SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID || $l_sPairingService == SOCIAL_SHARING_PROPERTY_TUMBLR_ID){
                
                $l_nDatabaseId = $p_aArgs['db_id'];
                $l_sSql = 'UPDATE enr_channel_oauth SET channel_id ='.$l_nChannelId.', modified = UTC_TIMESTAMP() WHERE id = '.$l_nDatabaseId.' ';
                $l_xSql->runQuery($l_sSql);
                

            }else if($l_sPairingService == 'FACEBOOK'){

                $l_xUserData = json_decode($p_aArgs['user_data']);
                $l_sUserToken = ($p_aArgs['user_token']);
                
                $l_xUserTokenData = array();
                $l_xUserTokenData['id'] = $l_xUserData->id;
                $l_xUserTokenData['name'] = $l_xUserData->name;
                $l_xUserTokenData['access_token'] = $l_sUserToken;
                $l_xUserTokenData['category'] = 'User';
                $l_xUserTokenData = (object) $l_xUserTokenData;

                $l_aPages = $l_aPairingData->data;
                array_push($l_aPages, $l_xUserTokenData);


                foreach($l_aPages as $l_xRow){
                    $l_nSocialTypeId = SOCIAL_SHARING_PROPERTY_FACEBOOK_ID;
                    $l_nPageId = $l_xRow->id;
                    $l_sPageName = $l_xRow->name;
                    $l_sPageAccessToken = $l_xRow->access_token;
                    $l_sPageCategory = $l_xRow->category;

                    $l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,page_category,social_token,created,active) 
                            VALUES ('.$l_nChannelId.',
                                "'.$l_xSql->cleanValue($l_nSocialTypeId).'",
                                "'.$l_xSql->cleanValue($l_nPageId).'",
                                "'.$l_xSql->cleanValue($l_sPageName).'",
                                "'.$l_xSql->cleanValue($l_sPageCategory).'",
                                "'.$l_xSql->cleanValue($l_sPageAccessToken).'",
                                NOW(),
                                "Y"
                                )
                                ON DUPLICATE KEY UPDATE page_name = VALUES(page_name), social_token = VALUES(social_token), active = VALUES(active), modified = NOW() ';   
                    $l_xSql->runQuery($l_sSql);
                    
                }
            }else if($l_sPairingService == 'TWITTER'){
                $l_aPairingDataMapped = array();
                foreach($l_aPairingData as $l_xRow){
                    $l_aParts = explode("=",$l_xRow);
                    $l_aPairingDataMapped[$l_aParts[0]] = $l_aParts[1];
                }
            
                $l_nSocialTypeId = SOCIAL_SHARING_PROPERTY_TWITTER_ID;
                $l_nPageId = $l_aPairingDataMapped['user_id'];
                $l_sPageName = $l_aPairingDataMapped['screen_name'];
                $l_sPageAccessToken = $l_aPairingDataMapped['oauth_token'];
                $l_sPageAccessTokenSecret = $l_aPairingDataMapped['oauth_token_secret'];

                $l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,social_token,social_token_secret,created,active) 
                        VALUES ('.$l_nChannelId.',
                            "'.$l_xSql->cleanValue($l_nSocialTypeId).'",
                            "'.$l_xSql->cleanValue($l_nPageId).'",
                            "'.$l_xSql->cleanValue($l_sPageName).'",
                            "'.$l_xSql->cleanValue($l_sPageAccessToken).'",
                            "'.$l_xSql->cleanValue($l_sPageAccessTokenSecret).'",
                            NOW(),
                            "Y"
                            )
                            ON DUPLICATE KEY UPDATE page_name = VALUES(page_name), social_token = VALUES(social_token), social_token_secret = VALUES(social_token_secret), active = VALUES(active), modified = NOW() ';   
                $l_xSql->runQuery($l_sSql);
                
            
            }
            $l_aJSON = array();
            $l_aJSON['status'] = 1;
            $l_aJSON['pairing'] = $this->returnCurrentPairingForChannelId($l_nChannelId);
            echo json_encode($l_aJSON);

             


        }


        private function sendVideoToBrightcove($p_sFilePath,$p_sDescription,$p_xChannel){
            

            require_once(LIB_PATH.'brightcove/SCBrightcove.php');
            $l_xBC = new SCBrightcove();

            if(strlen($p_sDescription) == 0){
                $p_sDescription = 'Video';
            }
            
            $l_aMetaData = array(
                    'name' => $p_sDescription,
                    'tags' => array($p_xChannel->slug),
                    'shortDescription' => $p_sDescription);

            $l_nNewId = $l_xBC->createVideo($p_sFilePath,$l_aMetaData);
            $l_nNewId = $l_nNewId->result;

            $l_xBC->addOverlayLogo($l_nNewId, $p_xChannel->brightcove_overlay_logo,'The Tooltip','http://www.starsite.com/test');
            return $l_nNewId;
            
        }





        public function postToSocialNetwork($p_nSocialPropertyRowId){

            require_once(FACEBOOK_OAUTH_CLASS);
            require_once(TWITTER_OAUTH_CLASS);

            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT post_type_id FROM enr_content_post_to_social WHERE id = '.intval($p_nSocialPropertyRowId);
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            
            $l_bIsVideo = $l_aData['post_type_id'] == CONTENT_TYPE_ID_VIDEO;
            

            $l_sSql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp, a.deep_link_web_oflo
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($p_nSocialPropertyRowId);
                            echo $l_sSql;
                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            
            
            foreach($l_aData as $l_xRow){

                $l_nSocialPropertyId = $l_xRow['social_property_type_id'];
                $l_sEmbedCodeInfo = Utils::encryptItemId($l_xRow['channel_id'].','.(($l_bIsVideo) ? 'v' : 'p').','.$l_xRow['content_id']);
                $l_sEmbedCodeInfo = 'http://'.$l_xRow['channel_url'].'.on.oflo.co/embed/'.$l_sEmbedCodeInfo;
                

                switch (intval($l_nSocialPropertyId)){

                    case SOCIAL_SHARING_PROPERTY_FACEBOOK_ID:   

// print_r($l_xRow);
                        // if($l_xRow['channel_id'] <= 43){
                        // if($l_xRow['channel_id'] == 8 || $l_xRow['channel_id'] == 33 || $l_xRow['channel_id'] > 52){
                            //NEW
//                            $l_sFacebookAppId = '265098000358000';
//                            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
                        // }else{
                        //     $l_sFacebookAppId = '1440671026187879';
                        //     $l_sFacebookAppSecret = '6860feb62899de0d9785d9c562d64088';

                        // } 
                        
//                        echo "<BR>\nFACEBOOK_APP_ID: ".FACEBOOK_APP_ID;
//                        echo "<BR>\nFACEBOOK_APP_SECRET: ".FACEBOOK_APP_SECRET;

                        $l_xFacebook = new Facebook\Facebook(
                            array(
                                'app_id'  => FACEBOOK_APP_ID,
                                'app_secret' => FACEBOOK_APP_SECRET,
                                'default_graph_version' => 'v2.6',
                                )
                        );

                        $l_nPageId = $l_xRow['page_id'];

                        // setting default access token to be used in script
                        $l_xFacebook->setDefaultAccessToken($l_xRow['social_token']);

                        $l_aArgs = array(
//                            'access_token'  => $l_xRow['social_token'],
                            // 'link'       => 'http://'.$l_xRow['channel_url'].'.on.oflo.co/ocontent/'.($l_bIsVideo ? 'video' : 'photo').'/'.$l_xRow['content_id'].'/',
                            'link'       => $l_xRow['deep_link_web_oflo_lp'],
                            // 'link'       => 'http://'.$l_xRow['channel_url'].'.on.starsite.com/viewpost_auth.php?'.($l_bIsVideo ? 'video' : 'photo').'='.$l_xRow['content_id'].'&type=landingpage',
                            // 'link'       => 'http://www.starclubltd.com/',
                            // 'caption'       => "caption here".time(),
                            // 'description'       => "description here".time(),
                            // 'name'       => "name here".time(),
                            'picture' => CDN_FRONT.'/'.$l_xRow['channel_slug'].'/assets/ufile/watermark/'.$l_xRow['content_file'],
                            'message'       => $l_xRow['message']
                         );

                         // print_r($l_aArgs);
                         // exit;

                        $posted = $l_xFacebook->post("/$l_nPageId/feed",$l_aArgs);
                        $posted_arr = $posted->getGraphNode()->asArray();
                        $l_nPostedId = $posted_arr['id'];
                        $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                        $l_xSql->runQuery($l_sSql);

                        break;


                    case SOCIAL_SHARING_PROPERTY_TWITTER_ID:
                        $l_sOauthToken = $l_xRow['social_token'];
                        $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                        $l_nId = $l_xRow['page_id'];
                     
                        $l_xTwitter = new TwitterOAuth('60idd0BWQe7Iu4tE6jXAzS1CZ', 'flevwmiG3CrU1shHwW0DsHN9fQBvpdsUEg6b0Exi7XqCULbV58', $l_sOauthToken, $l_sOauthTokenSecret);
                        // $content = $connection->get('account/verify_credentials');
                        $l_xRe = $l_xTwitter->post('statuses/update', array('status' => $l_xRow['deep_link_web_oflo_lp'].' '.$l_xRow['message']));

                       

                        $l_nPostedId = $l_xRe->id;
                        $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                        $l_xSql->runQuery($l_sSql);
                        break;


                      case SOCIAL_SHARING_PROPERTY_TUMBLR_ID:

                        $l_sOauthToken = $l_xRow['social_token'];
                        $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                        echo "\n\n";

                        $l_sURL = preg_replace("/https?:\/\//","",$l_xRow['page_name']);
                        $l_sURL = preg_replace("/\//","",$l_sURL);

                        $l_bSuppressOAuth = true;
                        require_once(LIB_PATH.'tumblr/tumblroauth.php');

                        $l_xTumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$l_sOauthToken,$l_sOauthTokenSecret);
                        $l_sPostURL = 'http://api.tumblr.com/v2/blog/'.$l_sURL.'/post';

                        $l_aParameters = array();
                        if($l_bIsVideo){
                            $l_aParameters['type'] = "video";
                            $l_aParameters['caption'] = $l_xRow['message'];
                            $l_aParameters['embed'] = '<iframe scrolling="no" frameborder="0" width="680" height="383" src="'.$l_sEmbedCodeInfo.'"></iframe>';
                        }else{
                            $l_aParameters['type'] = "photo";
                            $l_aParameters['link'] = $l_xRow['deep_link_web_lp'];
                            $l_aParameters['source'] = CDN_FRONT.'/'.$l_xRow['channel_slug'].'/assets/ufile/watermark/'.$l_xRow['content_file'];
                            // $l_aParameters['source'] = 'https://40.media.tumblr.com/tumblr_m2j7o8Y3EN1qafar4o1_540.jpg';
                            $l_aParameters['caption'] = $l_xRow['message'];
                        }
                    
                        $l_xResponse = $l_xTumblr->post($l_sPostURL,$l_aParameters);

                        if (201 == $l_xResponse->meta->status) {
                            $l_nPostedId = $l_xResponse->response->id;
                            $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                            $l_xSql->runQuery($l_sSql);
                        } else {
                          echo "error";
                          print_r($l_xResponse);
                        }

 
 
                        break;
                    
                    default:
                        break;
                }


            }

            
 
        }



        public function rq_heatMapData(&$p_aArgs){
            
            
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
            $l_nReportingId = $l_xChannel->starstats_site_id;


            if($l_nChannelId==30){

                $l_nChannelId = $p_aArgs['channel_id'];
                $l_xChannelManager = new ChannelManager();
                $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
                $l_nReportingId = $l_xChannel->starstats_site_id;

                //IF 25 == DEMO
                if($l_nReportingId == 25)
                    $l_nReportingId = 1;

                //Support for legacy 
                // Enrique Channel id 1 
                

                //tyrese
                // $l_nReportingId = 4;

                //Switch to Reporting Database
                $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'reportsdb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'developer';
                $DB_OVERRIDE['DB_PASSWORD'] = 'dev@2014sg'; 
                $DB_OVERRIDE['DB_NAME'] = 'devdatabase';

                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

                $l_sSql = 'SELECT idsite,location_country,location_region,location_city,location_latitude,location_longitude 
                FROM piwik_log_visit 
                WHERE location_latitude IS NOT NULL
                AND idsite = '.$l_nReportingId.'
                ORDER BY idvisit DESC limit 2000';
            // AND location_country LIKE "%gb"
                $l_aPoints = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
                $l_aPointsCleaned = array();
                $l_aCountries = array();
                $l_aCountriesCleaned = array();
                foreach($l_aPoints as $l_xRow){
                    $l_aFormatted = (array)$l_xRow;
                    $l_aFormatted['lat'] = $l_aFormatted['location_latitude'];
                    $l_aFormatted['lng'] = $l_aFormatted['location_longitude'];
                    $l_aCountries[strtoupper($l_aFormatted['location_country'])] += 1;
                    $l_aPointsCleaned[] = $l_aFormatted;
                }
                $l_aJSON['status'] = 1;
                $l_aJSON['data'] = $l_aPointsCleaned;
                arsort($l_aCountries, SORT_NUMERIC);
                $l_aAllCountries = Utils::getAllCountries();
                $l_nCount = 0;
                $l_nTotal = count($l_aPoints);
                $l_sPercentsDisplay = array();
                $l_nPercentLeft = 100;
                foreach($l_aCountries as $k => $v){
                    if($l_nCount++ > 3){
                        // $l_sPercentsDisplay .= 'Others ('.$l_nPercentLeft.'%) ';
                        break;
                    }
                    $l_nPercent = number_format(($v/$l_nTotal)*100,1);
                    $l_nPercentLeft -= $l_nPercent;
                    $l_sTitle = $l_aAllCountries[$k];
                    if($k == 'US')
                        $l_sTitle = 'USA';
                    else if($k == 'GB')
                        $l_sTitle = 'UK';
                    $l_aCountriesCleaned[] = array('title' => $l_sTitle, 'percent' => $l_nPercent, 'amount' => $v);
                    $l_sPercentsDisplay[] = $l_sTitle.' ('.$l_nPercent.'%)';
                }
                $l_sPercentsDisplay = join(' • ',$l_sPercentsDisplay);

                $l_aJSON['percents'] = $l_aCountriesCleaned;
                $l_aJSON['percents_display'] = $l_sPercentsDisplay;
                echo json_encode($l_aJSON);
            }else{

                //IF 25 == DEMO
                if($l_nReportingId == 25)
                    $l_nReportingId = 1;

                //Support for legacy 
                // Enrique Channel id 1 
                

                //tyrese
                // $l_nReportingId = 4;
                /*
                //Switch to Reporting Database
                $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'reportsdb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'developer';
                $DB_OVERRIDE['DB_PASSWORD'] = 'dev@2014sg'; 
                $DB_OVERRIDE['DB_NAME'] = 'devdatabase';

                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);


                $l_sSql = 'SELECT idsite,location_country,location_region,location_city,location_latitude,location_longitude 
                FROM piwik_log_visit 
                WHERE location_latitude IS NOT NULL
                AND idsite = '.$l_nReportingId.'
                ORDER BY idvisit DESC limit 2000';

                */

                 $gaObject = new Gastar();
                $analytics = $gaObject->getService();

                $channel_id = $value['id'];

                $profileID = $l_xChannel->google_analytics_internal_id;        
                   $start_date = date('Y-m-d');

        $end_date = date('Y-m-d');
     

              

                    $vid = $value['id'];
                    $videoId = "$vid";
                    $pageurl = "/content/video/$videoId/";

                    $tableId = "ga:$profileID";

                    $data = $gaObject->getGeoLocations($tableId, $analytics,$start_date,$end_date);
              
                

            // AND location_country LIKE "%gb"
                //$l_aPoints = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
                $l_aPointsCleaned = array();
                $l_aCountries = array();
                $l_aCountriesCleaned = array();
                foreach($data['rows'] as $l_xRow){
                    $l_aFormatted = (array)$l_xRow;
                    $l_aFormatted['lat'] = $l_aFormatted[3];
                    $l_aFormatted['lng'] = $l_aFormatted[4];
                    $l_aCountries[strtoupper($l_aFormatted[0])] += 1;
                    $l_aPointsCleaned[] = $l_aFormatted;
                }
                
                $l_aJSON['status'] = 1;
                $l_aJSON['data'] = $l_aPointsCleaned;
                arsort($l_aCountries, SORT_NUMERIC);
                $l_aAllCountries = Utils::getAllCountries();
                $l_nCount = 0;
                $l_nTotal = count($data['rows']);
               
                $l_sPercentsDisplay = array();
                $l_nPercentLeft = 100;
                foreach($l_aCountries as $k => $v){
                    if($l_nCount++ > 3){
                        // $l_sPercentsDisplay .= 'Others ('.$l_nPercentLeft.'%) ';
                        break;
                    }
                    $l_nPercent = number_format(($v/$l_nTotal)*100,1);
                    $l_nPercentLeft -= $l_nPercent;
                    $l_sTitle = $l_aAllCountries[$k];
                    if($k == 'US')
                        $l_sTitle = 'USA';
                    else if($k == 'GB')
                        $l_sTitle = 'UK';
                    $l_aCountriesCleaned[] = array('title' => $l_sTitle, 'percent' => $l_nPercent, 'amount' => $v);
                    $l_sPercentsDisplay[] = $l_sTitle.' ('.$l_nPercent.'%)';
                }
                $l_sPercentsDisplay = join(' • ',$l_sPercentsDisplay);

                $l_aJSON['percents'] = $l_aCountriesCleaned;
                $l_aJSON['percents_display'] = $l_sPercentsDisplay;
                echo json_encode($l_aJSON);
            }
        }


        

 
        public function rq_resendStarInviteEmail(){

            $l_nUserId = $_GET['id'];
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT c.*, u.email FROM enr_chanel c, users u
                        WHERE u.cid = c.id
                        AND u.id = '.$l_nUserId;
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            if(!$l_aData)
                return;

            print_r($l_aData);
            $l_nChannelId = $l_aData['id'];

            $l_sEmail = $l_aData['email'];
            $l_sNewPass = 'star'.rand(100,999);
            $l_sChannelName = $l_aData['title'];
            file_get_contents('http://admincms.oflo.co/api?cid='.$l_nChannelId.'&option=updateAdminInfo&email='.$l_sEmail.'&password='.$l_sNewPass);

            $l_sChannelURL = 'http://'.$l_aData['url'].'.on.oflo.co';             
            $l_aUserData = array();
            $l_aUserData['email'] = $l_sEmail;
            $l_aUserData['pass'] = $l_sNewPass;
            $l_sEncodeData = base64_encode(json_encode($l_aUserData));
            $l_sDownloadLink = 'http://api.oflo.co/api/api4/auth2.php?t='.$l_sEncodeData; 
            ob_start();
            include(__DIR__.'/_inviteEmail2.php');
            $l_sHTML = ob_get_clean();
            $l_aSendData = array();
            // $l_aSendData['to'] = $l_sEmail.',vincent@starclubltd.com';
            $l_aSendData['to'] = $l_sEmail.',starinvite@starclubltd.com';
            $l_aSendData['subject'] = 'Your StarClub Invitation';
            $l_aSendData['message'] = $l_sHTML;
//            $l_aSendData['from'] = "\"StarClub\" <no-reply@starclubltd.com>\n\r";

            $to = $l_aSendData['to'];
            $subject = $l_aSendData['subject'];
            $message = $l_aSendData['message'];
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: StarClub <no-reply@starsite.com>\n\r";

    mail($to, $subject, $message, $headers);
    
          //  print_R($l_aSendData);
            
          //  $l_aSendData = base64_encode(json_encode($l_aSendData));
       //     $l_sUrl = 'http://tuscanostudios.com/othersitesDependent/genericGet.php?data='.urlencode($l_aSendData);
          //  file_get_contents($l_sUrl);

  //          $this->sendPush(array(PUSH_LEVEL_ADMIN),'Invite Sent: '.$l_sChannelName);


        }

        public function rq_sendStarInviteEmail(&$p_aArgs){



            // print_R($p_aArgs);
            $l_sEmail = Utils::cleanPostDataValue($p_aArgs['email']);
            $l_sNewPass = 'star'.rand(100,999);
            $l_sChannelName = Utils::cleanPostDataValue($p_aArgs['name']);
            $l_sSlugName = trim(preg_replace("/\..*/","",Utils::cleanPostDataValue($p_aArgs['slug'])));

            $template = Utils::cleanPostDataValue($p_aArgs['template']);

            if(strlen($l_sSlugName) <= 1){
                $l_sSlugName = time();
                $l_sSlugName = VTBaseConvert::encode($l_sSlugName);
            }

            $l_sChannelURL = 'http://'.$l_sSlugName.'.on.oflo.co';             
            $l_aUserData = array();
            $l_aUserData['email'] = $l_sEmail;
            $l_aUserData['pass'] = $l_sNewPass;

            $l_sEncodeData = base64_encode(json_encode($l_aUserData));
/*
            $l_sDownloadLink = 'http://darwin.on.starsite.com/api/auth.php?t='.$l_sEncodeData; 
            $l_sDownloadLoginLink = '<?= BUNDLEID ?>://login-email/'.($l_sEncodeData).'/'; 

*/

            $l_sDownloadLink2 = 'http://api.oflo.co/api/api4/auth2.php?t='.$l_sEncodeData; 
            $l_sDownloadLoginLink2 = '<?= BUNDLEID ?>://login-email/'.($l_sEncodeData).'/'; 

            //check if user exists
            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT * FROM users WHERE LOWER(email) = "'.strtolower($l_sEmail).'"';
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            if($l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Email Exists';
                $l_aJSON['msg'] = 'This email already exists on starsite, please use another email';
                $l_aJSON['btn'] = 'Close';
                echo json_encode($l_aJSON);
                exit;
            }

            $l_sSql = 'SELECT * FROM enr_chanel WHERE LOWER(url) = "'.strtolower($l_sSlugName).'"';
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            if($l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Domain Exists';
                $l_aJSON['msg'] = 'The URL ('.$l_sSlugName.').on.starsite.com already exists, please try something other than "'.$l_sSlugName.'"';
                $l_aJSON['btn'] = 'Close';
                echo json_encode($l_aJSON);
                exit;
            }

           
            //Good, create account
            
            $l_sSql = 'INSERT INTO users (username,email,password,cid,created_on,active)   
                        VALUES (
                            "'.$l_sEmail.'",
                            "'.$l_sEmail.'",
                            "pass",
                            "-1",
                            '.time().',
                            1)';
            $l_xSql->runQuery($l_sSql);
            $l_nUserInsertedId = $l_xSql->db_last_insert_id();


            $l_sSql = 'INSERT INTO enr_chanel (name,title,user_id,url,slug,is_publish,created)   
                        VALUES (
                            "'.$l_sChannelName.'",
                            "'.$l_sChannelName.'",
                            "'.$l_nInsertId.'",
                            "'.$l_sSlugName.'",
                            "'.$l_sSlugName.'",
                            1,NOW())';
            $l_xSql->runQuery($l_sSql);
            $l_nChannelId = $l_xSql->db_last_insert_id();

            $l_sSql = 'UPDATE users SET cid = '.$l_nChannelId.' WHERE id = '.$l_nUserInsertedId;
            $l_xSql->runQuery($l_sSql);

            $l_sSql = 'UPDATE enr_chanel SET user_id = '.$l_nUserInsertedId.' WHERE id = '.$l_nChannelId;
            $l_xSql->runQuery($l_sSql);

            file_get_contents('http://admincms.oflo.co/api?cid='.$l_nChannelId.'&option=updateAdminInfo&email='.$l_sEmail.'&password='.$l_sNewPass);


         
            ob_start();

            if($template==1){
                include(__DIR__.'/_inviteEmail.php');
            }else if($template==2){
                include(__DIR__.'/_inviteEmail2.php');
            }else{}
            $l_sHTML = ob_get_clean();
            $l_aSendData = array();
            $l_aSendData['to'] = $l_sEmail.' , starinvite@starclubltd.com';


            $l_aSendData['subject'] = 'Your StarClub Invitation';
            $l_aSendData['message'] = $l_sHTML;
//            $l_aSendData['from'] = "\"StarClub\" <no-reply@starclubltd.com>\n\r";


            $to = $l_aSendData['to'];
            $subject = $l_aSendData['subject'];
            $message = $l_aSendData['message'];
 //           $headers = $l_aSendData['from'] .
//           'X-Mailer: PHP/' . phpversion();

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: StarClub <no-reply@starsite.com>\n\r";

    mail($to, $subject, $message, $headers);
            //$l_aSendData = base64_encode(json_encode($l_aSendData));
 //           $l_sUrl = 'http://tuscanostudios.com/othersitesDependent/genericGet.php?data='.urlencode($l_aSendData);
            //file_get_contents($l_sUrl);
 //           $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),'Invite Sent: '.$l_sChannelName);

            $l_aJSON['status'] = 1;

            echo json_encode($l_aJSON);


            $DB_OVERRIDE = array();
            $DB_OVERRIDE['DB_HOST'] = 'reportsdb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
            $DB_OVERRIDE['DB_USERNAME'] = 'developer';
            $DB_OVERRIDE['DB_PASSWORD'] = 'dev@2014sg'; 
            $DB_OVERRIDE['DB_NAME'] = 'devdatabase';
            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_sSql = 'INSERT INTO piwik_site (name,main_url,ts_created,timezone,currency,type,sitesearch_keyword_parameters,sitesearch_category_parameters)
             VALUES (
                "'.$l_sChannelName.'",
                "http://'.$l_sSlugName.'.on.starsite.com",
                "'.date("Y-m-d H:i:s").'",
                "America/Los_Angeles",
                "USD",
                "website",
                "q,query,s,search,searchword,k,keyword",
                "option,page,cid,token,user_id,"
                )';
            
            $this->_xSQLManager->runQuery($l_sSql);
            $l_nInsertId = $this->_xSQLManager->db_last_insert_id();


            $DB_OVERRIDE = array();
            $DB_OVERRIDE['DB_HOST'] = DB_HOST;
            $DB_OVERRIDE['DB_USERNAME'] = DB_USERNAME;
            $DB_OVERRIDE['DB_PASSWORD'] = DB_PASSWORD;
            $DB_OVERRIDE['DB_NAME'] = DB_NAME;
            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_sSql = 'UPDATE enr_chanel SET starstats_site_id = '.$l_nInsertId.' WHERE id ='.$l_nChannelId;
            
            $l_xSql->runQuery($l_sSql);
            

        }

        //=======================================================================================
        //                                  INSIGHT FUNCTIONS
        //=======================================================================================


        public function twitterInsightsR($content_id, $channel_id){
            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("0P9k6pzvqgbyR7gLUrXO3xGAz", 'nM4E6ibLYTdUw6Hdiy9HapJNC8gSIVIGEh4FT7lnD3FR5dvGOC', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
           
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->retweet_count;


        }


        public function twitterInsightsRB($content_id, $channel_id){
            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("rOnDmArOdei9TqZpqqJ53x4Il", '9sJv3d8zjTb9WNrYTdtlDiiblhCog2plq6A0ZvS4Mli2bqxCRL', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
           
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->retweet_count;


        }

        public function twitterInsightsF($content_id, $channel_id){

            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("0P9k6pzvqgbyR7gLUrXO3xGAz", 'nM4E6ibLYTdUw6Hdiy9HapJNC8gSIVIGEh4FT7lnD3FR5dvGOC', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
            
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->favorite_count;


        }

        public function twitterInsightsFB($content_id, $channel_id){

            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("rOnDmArOdei9TqZpqqJ53x4Il", '9sJv3d8zjTb9WNrYTdtlDiiblhCog2plq6A0ZvS4Mli2bqxCRL', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
            
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->favorite_count;


        }

        public function facebookInsightsS($content_id, $channel_id){


            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,s.posted_id FROM enr_channel_oauth o JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 1 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 1 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           


                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

            $i = 0;

            

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));
            //$user_id = $l_xFacebook->getUser();

            $today = date("Y-m-d");

            $since = strtotime($today. " -365 days");             
            $until = strtotime($today);

            foreach($l_aData as $l_xRow){
                $l_sOauthToken[$i] = $l_xRow['social_token'];
              
                $l_sPgeId[$i] = $l_xRow['page_id'];
                $page_id = $l_sPgeId[$i];
                $posted_id[$i] = $l_xRow['posted_id'];
                $pid = $posted_id[$i];
                $l_xRe[$i] =  "https://graph.facebook.com/$pid/?fields=shares&access_token=".$l_sOauthToken[$i];
             $i++;
            }

            return $l_xRe;

        }

        public function facebookInsightsL($content_id, $channel_id){


            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,s.posted_id FROM enr_channel_oauth o JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 1 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 1 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           


                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

            $i = 0;

            

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));
            //$user_id = $l_xFacebook->getUser();

            $today = date("Y-m-d");

            $since = strtotime($today. " -365 days");             
            $until = strtotime($today);

            foreach($l_aData as $l_xRow){
                $l_sOauthToken[$i] = $l_xRow['social_token'];
              
                $l_sPgeId[$i] = $l_xRow['page_id'];
                $page_id = $l_sPgeId[$i];
                $posted_id[$i] = $l_xRow['posted_id'];
                $pid = $posted_id[$i];
                $l_xRe[$i] =  "https://graph.facebook.com/$pid/likes?summary=1&filter=toplevel&access_token=".$l_sOauthToken[$i];
             $i++;
            }

            //$params = array(
            //'since' => $since, 
            //'until' => $until,
            //'access_token'=>$l_sOauthToken
            //);

            //$l_xRe = $l_xFacebook->api("/$posted_id/likes", "GET", $params);
            //$l_xRe = $l_xRe;
           



            return $l_xRe;



        }


        //=======================================================================================
        //                                  STAR ALERT FUNCTIONS
        //=======================================================================================


        private function rq_loginStarAlert(&$p_aArgs){
            
            $l_nPin = intval(Utils::cleanPostDataValue($p_aArgs['pin']));
            $l_aJSON = array();

            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT id,first_name FROM users WHERE star_alert_pin = '.$l_nPin;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            if($l_xData){
                $l_aJSON = array();
                $l_aJSON['status'] = 1;
                $l_aJSON['token'] = $this->makeToken($l_xData['id']);
                echo json_encode($l_aJSON);          
                $this->sendPush(array(PUSH_LEVEL_ADMIN),'Signup: '.$l_xData['first_name']);
                $l_sSql = 'UPDATE users SET star_alert_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_xData['id'];
                $l_xSql->runQuery($l_sSql);
            }else{
                $l_aJSON = array();
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Invalid Login';
                $l_aJSON['msg'] = 'Your pin is incorrect, please try again.';
                echo json_encode($l_aJSON);          
                $this->sendPush(array(PUSH_LEVEL_ADMIN),'Signup PIN FAIL: '.$l_nPin);
            }
        }

        // public static function externalSendPush($p_aPushLevels,$p_sMessage){
        // }
        private function sendPush($p_aPushLevels,$p_sMessage){

            ob_start();
            $l_xSql = AppController::getInstance()->getConnection();
            $p_aPushLevels[] = PUSH_LEVEL_ADMIN;
            $l_sLevels = join(",",$p_aPushLevels);
            $l_sSql = 'SELECT * FROM push_not';
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            $l_aTokens = array();
            foreach($l_aData as $l_xRow){
                $l_aTokens[] = $l_xRow['star_alert_device_id'];
            }

            require_once(LIB_PATH.'tuscano/VTPush.php');
                
            //if(SERVER_ENV_SETTINGS == 'PRODUCTION'){
              //  $l_sDomain = 'star-alert-prod';
                // $l_aTokens = array();
                // $l_aTokens[] = '0baa2ab27a7b9b5fb7425bf87d7b2ccbb5bcf55a0c28df4674cb8a2df3c746d8';
                // $l_aTokens[] = '89b64f5ba64855d7e8865f3950593ed2dde438ae320eff4697df7a509c962468';
           // }else{
                $l_sDomain = 'ck';
                $l_aTokens = array();
                $l_aTokens[] = '730b3166b52071b4163435d783eee94eaa9755a4bbd5fce706808cd319488d0b';
                return;
            //}
            // $l_sMessage = '⚠️ '.$p_nMessage;
            $l_sMessage = $p_sMessage;
            echo  $l_sDomain;
            print_r($l_aTokens);
            $l_xPush = new VTPush();
            $l_xPush->setCert($l_sDomain);
            $l_xPush->setMessage($l_sMessage);
            $l_xPush->setDeviceIds($l_aTokens);
            $l_xResponse = $l_xPush->pushOut();
                
            return ob_get_clean();
        }





        private function rq_updatePushDeviceIdStarAlert(&$p_aArgs){
            
            Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $l_xUserManager = new UserManager();
            
            $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_sToken = Utils::cleanPostDataValue($p_aArgs['token']);          
            $l_nUserId = $this->decodeToken($l_sToken);
            
            if($_GET['android'] == '1'){
                $l_aKeyValue = array();
                $l_aKeyValue['user_device_token_android'] = $l_sDeviceToken;
                $this->_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);   
                exit;
            }else{
                
                $l_aLocationData = $p_aArgs['loc'];
                
                $l_aKeyValue = array();
                $l_aKeyValue['city'] = $l_aLocationData['city'];
                $l_aKeyValue['country'] = $l_aLocationData['country'];
                $l_aKeyValue['country_code'] = $l_aLocationData['countryCode'];
                $l_aKeyValue['isp'] = $l_aLocationData['isp'];
                $l_aKeyValue['lat'] = $l_aLocationData['lat'];
                $l_aKeyValue['lng'] = $l_aLocationData['lon'];
                $l_aKeyValue['ip_address'] = $l_aLocationData['query'];
                $l_aKeyValue['region'] = $l_aLocationData['region'];
                $l_aKeyValue['region_name'] = $l_aLocationData['regionName'];
                $l_aKeyValue['timezone'] = $l_aLocationData['timezone'];
                $l_aKeyValue['zip'] = $l_aLocationData['zip'];
                $l_aKeyValue['star_alert_device_id'] = $l_sDeviceToken;

                if(strlen($l_aKeyValue['lat']) <= 3){
                    $l_aKeyValue = array();
                    $l_aKeyValue['star_alert_device_id'] = $l_sDeviceToken;
                    $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           
                }else{
                    $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           
                }
            }


        }



        private function rq_starAlertDashboard(&$p_aArgs){

            $l_nUserId = intval($this->decodeToken($_GET['t']));
            $l_nApiVersion = ($_GET['apiversion']);
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT * FROM users WHERE id = '.$l_nUserId;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            $l_sSql = 'UPDATE users SET star_alert_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_nUserId;
            $l_xSql->runQuery($l_sSql);
            //com.starsite.staralert
            ?>
            <html>
            <head>
            <style type="text/css">
            *{ margin:0; padding:0; outline:none;}
            body{ background:#FDDA00; font-family: 'Arial'; font-size:18px; color:#111;}
            .container{ text-align: center; padding:50px 30px}
            .container span{  font-size:10px}

            </style> 
            <script type="text/javascript">
            // setTimeout( function() {
                // document.location.href = document.location.href;
            // },1000);    
            </script>
            </head>
            <body>
            <div class="container">
            Thank you <?= $l_xData['first_name'] ?>.<br /><br />You are set to receive notifications.
            <br /><br /><br /><br />
            <span>Version : <?= $l_nApiVersion ?></span>
            
            <?
            // print_r($p_aArgs);
            // print_r($_GET);
            ?>
            </div>
            </body>
            </html>
            <?

        }



        //=======================================================================================
        //                                  PRIVATE FUNCTIONS
        //=======================================================================================


        private function cmp($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        }



  
        private function makeToken($p_sStr){
            $l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
            $p_sStr = base64_encode($l_sPaddingText.base64_encode(strrev(base64_encode($p_sStr))).base64_encode($l_sPaddingText));
            return $p_sStr;
        }

        private function decodeToken($p_sStr){
            $l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
            $l_sEnding = base64_encode($l_sPaddingText);
            $p_sStr = str_replace($l_sEnding,'',base64_decode($p_sStr));
            $p_sStr = str_replace($l_sPaddingText,'',$p_sStr);
            $p_sStr = base64_decode(strrev(base64_decode($p_sStr)));
            return $p_sStr;
        }

        private function cleanForReturn(&$p_aData){
            foreach($p_aData as $k=>$v){
                if(!$v && $v != '0')
                    unset($p_aData[$k]);
            }
        }



        private function makeHumanFriendlyNumbers($p_sStr){
            
                // $x = round($p_sStr);
              $x = ($p_sStr);
              $x_number_format = number_format($x);
              $x_array = explode(',', $x_number_format);
              $x_parts = array('k', 'm', 'b', 't');
              $x_count_parts = count($x_array) - 1;
              $x_display = $x;
              $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
              $x_display .= $x_parts[$x_count_parts - 1];
              return $x_display;
            
            

        }
        
        private function buildStatObject($p_xObj,$p_nMainChannelId){
                 // print_R($p_xObj);
                $p_nMainChannelId=$p_nMainChannelId;
                $l_bIsVideo = $p_xObj['post_type'];

              

                if($l_bIsVideo=="video" || $l_bIsVideo=="Video Post"){ $l_bIsVideo = "video"; }else{ $l_bIsVideo = ""; }
                
                $l_aJSON = array();
                if($l_bIsVideo!=""){ $l_aJSON['isPhoto']=0; $l_aJSON['isVideo']=1;}else{ $l_aJSON['isPhoto']=1; $l_aJSON['isVideo']=0;}
                $l_aJSON['itemId'] = $p_xObj['id'];
                $likencomentsid = $l_aJSON['itemId'];


                $l_aJSON['id'] = $p_xObj['id'];
               
               if($l_bIsVideo!=""){ $l_aJSON['photoId'] = "";  $l_aJSON['videoId'] = $p_xObj['id']; }else{ $l_aJSON['photoId'] = $p_xObj['id'];  $l_aJSON['videoId'] = "";  }
                
                if($l_bIsVideo!=""){ $l_aJSON['photo_db_id'] = ""; $l_aJSON['video_db_id'] = $p_xObj['video_db_id']; $whatIWant = substr($l_aJSON['videoId'], strpos($l_aJSON['videoId'], "_") + 1);     }else{ $l_aJSON['photo_db_id'] = $p_xObj['photo_db_id']; $l_aJSON['video_db_id'] = ""; $whatIWant = substr($l_aJSON['photoId'], strpos($l_aJSON['photoId'], "_") + 1);   }
               $l_aJSON['postId'] = $p_xObj['id'];
                $l_aJSON['postType'] = $p_xObj['post_type'];
                 $l_aJSON['post_type'] = $p_xObj['post_type'];
                  $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                  //$whatIWant =$p_xObj['id'];
                   $l_aJSON['commentcount'] = $this->getCommentCount($likencomentsid);
                $l_aJSON['likes'] = $this->getLikes($likencomentsid);
            

                if($p_nMainChannelId==64){ $p_xObj['slug'] = "viral"; }
                // $l_aJSON['thumbnail'] = $p_xObj['thumbnail'];
                // if($l_aJSON['post_type'] == 'video')
                if($l_bIsVideo!=""){  
                    if($p_nMainChannelId==64){
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/watermark/small_thumb/'.$p_xObj['image_path']; 
                    }
                    $l_aJSON['big_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/big_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['med_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['small_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_square_thumb/'.$p_xObj['image_path'];   

                }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['destination']; 
                }
                   
                // else
                    // $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                if(!$l_bIsVideo){
                    $p_xObj['start_date'] = $p_xObj['photo_start_date'];
                }
                 
                $l_aJSON['message'] = $p_xObj['description'];
                           //$l_aJSON['description'] = $p_xObj['description'];
                           // $l_aJSON['caption'] = $p_xObj['description'];
               
               
                   $content_id =$p_xObj['id'];
                   $total_v_views = $total_v_revenue = $total_daily_views = 0;
                     if($p_nMainChannelId!=64 && $p_nMainChannelId!=30){
                    $l_xSql = AppController::getInstance()->getConnection();
                    if($l_bIsVideo!=""){  
                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$l_aJSON['video_db_id']."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }

                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }

                    }else{

                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$l_aJSON['photo_db_id']."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }

                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }
                   }
               }

               
                  

                        if($p_nMainChannelId==30){
                            $l_aJSON['tw_retweets'] = rand(3000,7507);
                            $l_aJSON['tw_favorites'] = rand(3000,7507);
                            $l_aJSON['gp_likes'] = rand(1000,5507);
                            $l_aJSON['gp_comments'] = rand(1000,5507);
                            $l_aJSON['tu_likes'] = rand(1000,5507);
                            $l_aJSON['tu_comments'] = rand(1000,5507);
                            $l_aJSON['message'] = $p_xObj['description'];
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['fb_likes'] =  rand(3000,30507);
                            $l_aJSON['fb_comments'] = rand(3000,30507);
                            $l_aJSON['fb_shares'] = rand(3000,30507);
                            $l_aJSON['fb_reaches'] = rand(3000,30507);
                            $l_aJSON['ss_impressions'] = rand(3000,30507);
                            $l_aJSON['ss_engagements'] = rand(3000,30507);
                            $l_aJSON['ss_pageViews'] = rand(3000,30507);
                            $l_aJSON['ss_videoViews'] = rand(3000,30507);
                            $money = rand(100,5507);
                            $l_aJSON['ss_earnings'] = ("$".number_format($money));
                            $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                            $l_aJSON['url'] =  'http://techslides.com/demos/sample-videos/small.mp4';
                            


                        }else{ 
                            if($total_v_views>0){
                        $l_aJSON['ss_pageViews'] = ($total_v_views) ? ($total_v_views) : '';
                        $l_aJSON['total_views'] = ($total_v_views) ? ($total_v_views) : '';
                        }
                        if($total_daily_views>0){
                        $l_aJSON['live_views'] = ($total_daily_views) ? ($total_daily_views) : '';
                        }
                            $l_aJSON['ss_earnings'] = ("$".number_format(round(($total_v_revenue),0),0)) ? ("$".number_format(round(($total_v_revenue),0),0)) : "N/A";
                            $l_aJSON['ss_impressions'] = ($total_v_views) ? ($total_v_views) : 'N/A';

                            $l_aJSON['tw_retweets'] = $l_xData['tw_retweets'] ? $l_xData['tw_retweets'] : "N/A";
                            $l_aJSON['tw_favorites'] = $l_xData['tw_favorites'] ? $l_xData['tw_favorites'] : "N/A";
                            $l_aJSON['gp_likes'] = "N/A";
                            $l_aJSON['gp_comments'] = "N/A";
                            $l_aJSON['tu_likes'] = "N/A";
                            $l_aJSON['tu_comments'] = "N/A";
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime('-7 hours',$l_aJSON['created_time']));
                            $l_aJSON['fb_likes'] =  $l_xData['fb_likes'] ? $l_xData['fb_likes'] : "N/A";
                            $l_aJSON['fb_comments'] = "N/A";
                            $l_aJSON['fb_shares'] = $l_xData['fb_shares'] ? $l_xData['fb_shares'] : "N/A";
                            $l_aJSON['fb_reaches'] = "N/A";
                           // $l_aJSON['ss_impressions'] = ($l_nData['page_views_ga']*3) ? ($l_nData['page_views_ga']*3) : "N/A";
                            $l_aJSON['ss_engagements'] = ($l_nData['engagement']) ? ($l_nData['engagement']) : "N/A";
                            $l_aJSON['ss_pageViews'] = ($l_nData['page_views_ga']*3) ? ($l_nData['page_views_ga']*3) : "N/A";
                            $l_aJSON['ss_videoViews'] = ($l_nData['video_views']*3) ? ($l_nData['video_views']*3) : "N/A";
                          //  $l_aJSON['ss_earnings'] = ("$".number_format(round(($l_nData['revenue']*3),0),0)) ? ("$".number_format(round(($l_nData['revenue']*3),0),0)) : "N/A";
                            $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                            $l_aJSON['url'] =  'http://'.$p_xObj['slug'].'.on.oflo.co/ocontent/'.(($l_aJSON['postType'] == 'video') ? 'video/'.$p_xObj['video_db_id'].'/' : 'photo/'.$p_xObj['photo_db_id']).'/';

                        }
                
                $l_sEmbedCodeInfo = Utils::encryptItemId($p_nMainChannelId.','.((1 || $l_bIsVideo) ? 'v' : 'p').','.$p_xObj['id']);
                $l_sEmbedCodeInfo = 'http://'.$p_xObj['slug'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $l_aJSON['embed'] =  base64_encode('<iframe src="'.$l_sEmbedCodeInfo.'"></iframe>');

            
                $l_aJSON['video_length'] = $p_xObj['video_length'];
                $l_sSeconds = $p_xObj['video_length']/1000;
                $l_sHours = floor($l_sSeconds/60/60);
                $l_sMin = floor(($l_sSeconds-($l_sHours*60))/60);
                $l_sSec = round(($l_sSeconds-($l_sHours*60) - ($l_sMin*60)));

                $l_aJSON['ss_videoLengthHuman'] = str_pad($l_sHours,2,"0",STR_PAD_LEFT).':'.str_pad($l_sMin,2,"0",STR_PAD_LEFT).':'.str_pad($l_sSec,2,"0",STR_PAD_LEFT);
                $l_aJSON['ss_videoLengthAndPercent'] = "1000";
                $l_aJSON['video_engagement'] = $p_xObj['engagement_score'].'%';
                $l_aJSON['video_views'] = $this->makeHumanFriendlyNumbers($p_xObj['video_view']);
                $l_aJSON['video_impression'] = $p_xObj['video_impression'];
                 if($p_nMainChannelId == 30){
                $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                }else{

                     $webm = str_replace(".mp4","-480.webm",$p_xObj['destination']);
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];

                    $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                $l_aJSON['teaser_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                }
                if($p_nMainChannelId == 15){
                    $l_aJSON['file_url'] = 'http://enrique.starsite.com/forapi/'.$p_xObj['destination'];
                    $l_aJSON['thumbnail'] = 'http://cms.enrique.starsite.com/assets/ufile/'.$p_xObj['image_path'];
                }
               
                if($p_xObj['video_url']!=""){
                    if($p_xObj['video_type']==1){
                    parse_str( parse_url( $p_xObj['video_url'], PHP_URL_QUERY ), $end);
     
                    $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                    
                        if($end['v']!="" && $end['v']!=NULL){
                            $l_aJSON['video_type'] = "youtube";
                            $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                            $l_aJSON['embed_id'] = $end['v'];
                            $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        }

                    }else if($p_xObj['video_type']==2){    
                        $l_aJSON['vimeo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vimeo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==3){ 
                        $l_aJSON['vevo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vevo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==4){ 
                        $l_aJSON['vine_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vine";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==5){ 
                        $l_aJSON['instagram_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "instagram";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                    else if($p_xObj['video_type']==NULL){
                     $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                    }else{
                       $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                
                }

                $l_aJSON['deep_link_web_oflo_lp'] = $p_xObj['deep_link_web_oflo_lp'];
                $l_aJSON['deep_link_web_oflo'] = $p_xObj['deep_link_web_oflo'];

                $l_aJSON['channel_id'] = $p_nMainChannelId;
                $l_xSql = AppController::getInstance()->getConnection();
                $l_pSql = "SELECT name, image_path FROM enr_chanel WHERE id = ".$l_aJSON['channel_id']."";  
                $l_nData = $l_xSql->getRow($l_xSql->runQuery($l_pSql));

                $l_aJSON['channel_name'] = $l_nData['name'];
                //$l_aJSON['channel_image'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$l_nData['image_path'].'?v='.time();
                $l_aJSON['channel_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/images/chanel.jpg'; 
                
                // $l_aJSON['file_url'] = 'http://sc-media-assets.s3.amazonaws.com/tyrese/assets/ufile/video/2014/11/102e94a832a06cffdf5385f81790ab84.mp4';
                

        // define('STATIC_FILES_HOST','http://d6058xu8xpm39.cloudfront.net');
        // define('CDN_FRONT','http://d1o8tuak29oxqa.cloudfront.net');

                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 2)   +  (( $p_xObj['video_view']/1000) * 10));
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                //$l_aJSON['ss_earnings'] = (  (  $p_xObj['ss_impressions']/1000  * 1.0 ) + ( $p_xObj['ss_videoViews']/1000 * 3.0  * 0.16 ) ) * 0.33;
               
                if($this->_bDemo)
                    $l_aJSON['earnings'] = $p_xObj['video_view']/1000 * 1;

                // 20%
                // $l_aJSON['earnings'] = $l_aJSON['earnings']*.16 *.33;
                $l_aJSON['earnings_title'] = 'Projected Earnings';

                $l_aJSON = (array)$l_aJSON;
                foreach($l_aJSON as $k=>$v){
                    if(!$v)
                        unset($l_aJSON[$k]);
                }

                return $l_aJSON;
            }

             public function buildStatObjectNoReports($p_xObj,$p_nMainChannelId){
                 // print_R($p_xObj);
                $p_nMainChannelId=$p_nMainChannelId;
                $l_bIsVideo = $p_xObj['post_type'];

               

                if($l_bIsVideo=="video" || $l_bIsVideo=="Video Post"){ $l_bIsVideo = "video"; }else{ $l_bIsVideo = ""; }
                
                $l_aJSON = array();
                if($l_bIsVideo!=""){ $l_aJSON['isPhoto']=0; $l_aJSON['isVideo']=1;}else{ $l_aJSON['isPhoto']=1; $l_aJSON['isVideo']=0;}
                $l_aJSON['itemId'] = $p_xObj['id'];
                $likencomentsid = $l_aJSON['itemId'];


                $l_aJSON['id'] = $p_xObj['id'];
               
               if($l_bIsVideo!=""){ $l_aJSON['photoId'] = "";  $l_aJSON['videoId'] = $p_xObj['id']; }else{ $l_aJSON['photoId'] = $p_xObj['id'];  $l_aJSON['videoId'] = "";  }
                
                if($l_bIsVideo!=""){ $l_aJSON['photo_db_id'] = ""; $l_aJSON['video_db_id'] = $p_xObj['video_db_id']; $whatIWant = substr($l_aJSON['videoId'], strpos($l_aJSON['videoId'], "_") + 1);     }else{ $l_aJSON['photo_db_id'] = $p_xObj['photo_db_id']; $l_aJSON['video_db_id'] = ""; $whatIWant = substr($l_aJSON['photoId'], strpos($l_aJSON['photoId'], "_") + 1);   }
               $l_aJSON['postId'] = $p_xObj['id'];
                $l_aJSON['postType'] = $p_xObj['post_type'];
                 $l_aJSON['post_type'] = $p_xObj['post_type'];
                 // $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                  $whatIWant =$likencomentsid;
                   $l_aJSON['commentcount'] = $this->getCommentCount($whatIWant);
                $l_aJSON['likes'] = $this->getLikes($whatIWant);
                

                if($p_nMainChannelId==64){ $p_xObj['slug'] = "viral"; }
                // $l_aJSON['thumbnail'] = $p_xObj['thumbnail'];
                // if($l_aJSON['post_type'] == 'video')
                if($l_bIsVideo!=""){  
                    if($p_nMainChannelId==64){
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/watermark/small_thumb/'.$p_xObj['image_path']; 
                    }
                    $l_aJSON['big_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/big_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['med_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['small_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_square_thumb/'.$p_xObj['image_path'];   

                }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['destination']; 
                }
                   
                // else
                    // $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                if(!$l_bIsVideo){
                    $p_xObj['start_date'] = $p_xObj['photo_start_date'];
                }
                 
                $l_aJSON['message'] = $p_xObj['description'];
                           //$l_aJSON['description'] = $p_xObj['description'];
                           // $l_aJSON['caption'] = $p_xObj['description'];
               
               
                    $content_id =$p_xObj['id'];
                    $total_v_views = $total_daily_views = 0;
                    $total_v_revenue = 0;
                     if($p_nMainChannelId!=64 && $p_nMainChannelId!=30){
                    $l_xSql = AppController::getInstance()->getConnection();
                    if($l_bIsVideo!=""){  
                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue  FROM oflo_reports_monthly WHERE content_id = ".$whatIWant."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 

                            $total_v_revenue += $rep_value['revenue'];
                        }
                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }

                    }else{

                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$whatIWant."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }

                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }
                   }
               }

                  

                        if($p_nMainChannelId==30){
                            $l_aJSON['tw_retweets'] = rand(3000,7507);
                            $l_aJSON['tw_favorites'] = rand(3000,7507);
                            $l_aJSON['gp_likes'] = rand(1000,5507);
                            $l_aJSON['gp_comments'] = rand(1000,5507);
                            $l_aJSON['tu_likes'] = rand(1000,5507);
                            $l_aJSON['tu_comments'] = rand(1000,5507);
                            $l_aJSON['message'] = $p_xObj['description'];
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['fb_likes'] =  rand(3000,30507);
                            $l_aJSON['fb_comments'] = rand(3000,30507);
                            $l_aJSON['fb_shares'] = rand(3000,30507);
                            $l_aJSON['fb_reaches'] = rand(3000,30507);
                            $l_aJSON['ss_impressions'] = rand(3000,30507);
                            $l_aJSON['ss_engagements'] = rand(3000,30507);
                            $l_aJSON['ss_pageViews'] = rand(3000,30507);
                            $l_aJSON['ss_videoViews'] = rand(3000,30507);
                            $money = rand(100,5507);
                            $l_aJSON['ss_earnings'] = ("$".number_format($money));
                            $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                            $l_aJSON['url'] =  'http://techslides.com/demos/sample-videos/small.mp4';
                            


                        }else{ 

                            if($total_v_views>0){
                        $l_aJSON['ss_pageViews'] = ($total_v_views) ? ($total_v_views) : '';
                        $l_aJSON['total_views'] = ($total_v_views) ? ($total_v_views) : '';
                        }
                        if($total_daily_views>0){
                        $l_aJSON['live_views'] = ($total_daily_views) ? ($total_daily_views) : '';
                        }
                            $l_aJSON['ss_earnings'] = ("$".number_format(round(($total_v_revenue),0),0)) ? ("$".number_format(round(($total_v_revenue),0),0)) : "N/A";
                            $l_aJSON['ss_impressions'] = "1200";
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime('-7 hours',$l_aJSON['created_time']));
                           
                            $l_aJSON['url'] =  'http://'.$p_xObj['slug'].'.on.oflo.co/ocontent/'.(($l_aJSON['postType'] == 'video') ? 'video/'.$p_xObj['video_db_id'].'/' : 'photo/'.$p_xObj['photo_db_id']).'/';

                        }
                
                $l_sEmbedCodeInfo = Utils::encryptItemId($p_nMainChannelId.','.((1 || $l_bIsVideo) ? 'v' : 'p').','.$p_xObj['id']);
                $l_sEmbedCodeInfo = 'http://'.$p_xObj['slug'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $l_aJSON['embed'] =  base64_encode('<iframe src="'.$l_sEmbedCodeInfo.'"></iframe>');

            
                $l_aJSON['video_length'] = $p_xObj['video_length'];
                $l_sSeconds = $p_xObj['video_length']/1000;
                $l_sHours = floor($l_sSeconds/60/60);
                $l_sMin = floor(($l_sSeconds-($l_sHours*60))/60);
                $l_sSec = round(($l_sSeconds-($l_sHours*60) - ($l_sMin*60)));

                $l_aJSON['ss_videoLengthHuman'] = str_pad($l_sHours,2,"0",STR_PAD_LEFT).':'.str_pad($l_sMin,2,"0",STR_PAD_LEFT).':'.str_pad($l_sSec,2,"0",STR_PAD_LEFT);
                $l_aJSON['ss_videoLengthAndPercent'] = "1000";
                $l_aJSON['video_engagement'] = $p_xObj['engagement_score'].'%';
                $l_aJSON['video_views'] = $this->makeHumanFriendlyNumbers($p_xObj['video_view']);
                $l_aJSON['video_impression'] = $p_xObj['video_impression'];
                 if($p_nMainChannelId == 30){
                $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                }else{

                     $webm = str_replace(".mp4","-480.webm",$p_xObj['destination']);
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];

                    $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                }
                if($p_nMainChannelId == 15){
                    $l_aJSON['file_url'] = 'http://enrique.starsite.com/forapi/'.$p_xObj['destination'];
                    $l_aJSON['thumbnail'] = 'http://cms.enrique.starsite.com/assets/ufile/'.$p_xObj['image_path'];
                }
               
                if($p_xObj['video_url']!=""){
                    if($p_xObj['video_type']==1){
                    parse_str( parse_url( $p_xObj['video_url'], PHP_URL_QUERY ), $end);
     
                    $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                    
                        if($end['v']!="" && $end['v']!=NULL){
                            $l_aJSON['video_type'] = "youtube";
                            $l_aJSON['embed_id'] = $end['v'];
                            $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        }

                    }else if($p_xObj['video_type']==2){    
                        $l_aJSON['vimeo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vimeo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==3){ 
                        $l_aJSON['vevo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vevo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==4){ 
                        $l_aJSON['vine_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vine";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==5){ 
                        $l_aJSON['instagram_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "instagram";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                    else if($p_xObj['video_type']==NULL){
                     $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                    }else{
                       $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                
                }
                $l_aJSON['deep_link_web_oflo_lp'] = $p_xObj['deep_link_web_oflo_lp'];
                $l_aJSON['deep_link_web_oflo'] = $p_xObj['deep_link_web_oflo'];
                

                 $l_aJSON['channel_id'] = $p_nMainChannelId;
                $l_xSql = AppController::getInstance()->getConnection();
                $l_pSql = "SELECT name, image_path FROM enr_chanel WHERE id = ".$l_aJSON['channel_id']."";  
                $l_nData = $l_xSql->getRow($l_xSql->runQuery($l_pSql));

                $l_aJSON['channel_name'] = $l_nData['name'];
               //$l_aJSON['channel_image'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$l_nData['image_path'].'?v='.time();  
                $l_aJSON['channel_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/images/chanel.jpg';  
                  
                                // $l_aJSON['file_url'] = 'http://sc-media-assets.s3.amazonaws.com/tyrese/assets/ufile/video/2014/11/102e94a832a06cffdf5385f81790ab84.mp4';
                 $l_aJSON['global_ga_key'] = 'UA-63548102-57';
                 $l_aJSON['channel_slug'] = $p_xObj['slug'];

        // define('STATIC_FILES_HOST','http://d6058xu8xpm39.cloudfront.net');
        // define('CDN_FRONT','http://d1o8tuak29oxqa.cloudfront.net');

                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 2)   +  (( $p_xObj['video_view']/1000) * 10));
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                //$l_aJSON['ss_earnings'] = (  (  $p_xObj['ss_impressions']/1000  * 1.0 ) + ( $p_xObj['ss_videoViews']/1000 * 3.0  * 0.16 ) ) * 0.33;
               
                if($this->_bDemo)
                    $l_aJSON['earnings'] = $p_xObj['video_view']/1000 * 1;

                // 20%
                // $l_aJSON['earnings'] = $l_aJSON['earnings']*.16 *.33;
                $l_aJSON['earnings_title'] = 'Projected Earnings';

                $l_aJSON = (array)$l_aJSON;
                foreach($l_aJSON as $k=>$v){
                    if(!$v)
                        unset($l_aJSON[$k]);
                }

                return $l_aJSON;
            }

            public function buildStatObjectNoReportsSlug($p_xObj,$p_nMainChannelId){
                 // print_R($p_xObj);
                $p_nMainChannelId=$p_nMainChannelId;
                $l_bIsVideo = $p_xObj['post_type'];
                $l_mSql = AppController::getInstance()->getConnection();
                $l_rSql = "SELECT slug FROM enr_chanel WHERE id = ".$p_nMainChannelId."";  
                $l_mData = $l_mSql->getRow($l_mSql->runQuery($l_rSql));
               $p_xObj['slug'] = $l_mData['slug'];

                if($l_bIsVideo=="video" || $l_bIsVideo=="Video Post"){ $l_bIsVideo = "video"; }else{ $l_bIsVideo = ""; }
                
                $l_aJSON = array();
                if($l_bIsVideo!=""){ $l_aJSON['isPhoto']=0; $l_aJSON['isVideo']=1;}else{ $l_aJSON['isPhoto']=1; $l_aJSON['isVideo']=0;}
                $l_aJSON['itemId'] = $p_xObj['id'];
                $likencomentsid = $l_aJSON['itemId'];


                $l_aJSON['id'] = $p_xObj['id'];
               
               if($l_bIsVideo!=""){ $l_aJSON['photoId'] = "";  $l_aJSON['videoId'] = $p_xObj['id']; }else{ $l_aJSON['photoId'] = $p_xObj['id'];  $l_aJSON['videoId'] = "";  }
                
                if($l_bIsVideo!=""){ $l_aJSON['photo_db_id'] = ""; $l_aJSON['video_db_id'] = $p_xObj['video_db_id']; $whatIWant = substr($l_aJSON['videoId'], strpos($l_aJSON['videoId'], "_") + 1);     }else{ $l_aJSON['photo_db_id'] = $p_xObj['photo_db_id']; $l_aJSON['video_db_id'] = ""; $whatIWant = substr($l_aJSON['photoId'], strpos($l_aJSON['photoId'], "_") + 1);   }
               $l_aJSON['postId'] = $p_xObj['id'];
                $l_aJSON['postType'] = $p_xObj['post_type'];
                 $l_aJSON['post_type'] = $p_xObj['post_type'];
                 // $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                  $whatIWant =$likencomentsid;
                   $l_aJSON['commentcount'] = $this->getCommentCount($likencomentsid);
                $l_aJSON['likes'] = $this->getLikes($likencomentsid);

                if($p_nMainChannelId==64){ $p_xObj['slug'] = "viral"; }
                // $l_aJSON['thumbnail'] = $p_xObj['thumbnail'];
                // if($l_aJSON['post_type'] == 'video')
                if($l_bIsVideo!=""){  
                    if($p_nMainChannelId==64){
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/watermark/small_thumb/'.$p_xObj['image_path']; 
                    }
                    $l_aJSON['big_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/big_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['med_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['small_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_square_thumb/'.$p_xObj['image_path'];    

                }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['destination']; 
                }
                   
                // else
                    // $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                if(!$l_bIsVideo){
                    $p_xObj['start_date'] = $p_xObj['photo_start_date'];
                }
                 
                $l_aJSON['message'] = $p_xObj['description'];
                           //$l_aJSON['description'] = $p_xObj['description'];
                           // $l_aJSON['caption'] = $p_xObj['description'];
               
               
                    $content_id =$p_xObj['id'];
                   
                    $total_v_views = $total_daily_views = 0;
                     if($p_nMainChannelId!=64 && $p_nMainChannelId!=30){
                    $l_xSql = AppController::getInstance()->getConnection();
                    if($l_bIsVideo!=""){  
                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$whatIWant."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }
                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }

                    }else{

                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$whatIWant."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }
                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }
                   }
               }

                  

                        if($p_nMainChannelId==30){
                            $l_aJSON['tw_retweets'] = rand(3000,7507);
                            $l_aJSON['tw_favorites'] = rand(3000,7507);
                            $l_aJSON['gp_likes'] = rand(1000,5507);
                            $l_aJSON['gp_comments'] = rand(1000,5507);
                            $l_aJSON['tu_likes'] = rand(1000,5507);
                            $l_aJSON['tu_comments'] = rand(1000,5507);
                            $l_aJSON['message'] = $p_xObj['description'];
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['fb_likes'] =  rand(3000,30507);
                            $l_aJSON['fb_comments'] = rand(3000,30507);
                            $l_aJSON['fb_shares'] = rand(3000,30507);
                            $l_aJSON['fb_reaches'] = rand(3000,30507);
                            $l_aJSON['ss_impressions'] = rand(3000,30507);
                            $l_aJSON['ss_engagements'] = rand(3000,30507);
                            $l_aJSON['ss_pageViews'] = rand(3000,30507);
                            $l_aJSON['ss_videoViews'] = rand(3000,30507);
                            $money = rand(100,5507);
                            $l_aJSON['ss_earnings'] = ("$".number_format($money));
                            $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                            $l_aJSON['url'] =  'http://techslides.com/demos/sample-videos/small.mp4';
                            


                        }else{ 

                           if($total_v_views>0){
                        $l_aJSON['ss_pageViews'] = ($total_v_views) ? ($total_v_views) : '';
                        $l_aJSON['total_views'] = ($total_v_views) ? ($total_v_views) : '';
                        }
                        if($total_daily_views>0){
                        $l_aJSON['live_views'] = ($total_daily_views) ? ($total_daily_views) : '';
                        }
                            $l_aJSON['ss_earnings'] = ("$".number_format(round(($total_v_revenue),0),0)) ? ("$".number_format(round(($total_v_revenue),0),0)) : "N/A";
                            $l_aJSON['ss_impressions'] = "1200";
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime('-7 hours',$l_aJSON['created_time']));
                           
                            $l_aJSON['url'] =  'http://'.$p_xObj['slug'].'.on.oflo.co/ocontent/'.(($l_aJSON['postType'] == 'video') ? 'video/'.$p_xObj['video_db_id'].'/' : 'photo/'.$p_xObj['photo_db_id']).'/';

                        }
                
                $l_sEmbedCodeInfo = Utils::encryptItemId($p_nMainChannelId.','.((1 || $l_bIsVideo) ? 'v' : 'p').','.$p_xObj['id']);
                $l_sEmbedCodeInfo = 'http://'.$p_xObj['slug'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $l_aJSON['embed'] =  base64_encode('<iframe src="'.$l_sEmbedCodeInfo.'"></iframe>');

            
                $l_aJSON['video_length'] = $p_xObj['video_length'];
                $l_sSeconds = $p_xObj['video_length']/1000;
                $l_sHours = floor($l_sSeconds/60/60);
                $l_sMin = floor(($l_sSeconds-($l_sHours*60))/60);
                $l_sSec = round(($l_sSeconds-($l_sHours*60) - ($l_sMin*60)));

                $l_aJSON['ss_videoLengthHuman'] = str_pad($l_sHours,2,"0",STR_PAD_LEFT).':'.str_pad($l_sMin,2,"0",STR_PAD_LEFT).':'.str_pad($l_sSec,2,"0",STR_PAD_LEFT);
                $l_aJSON['ss_videoLengthAndPercent'] = "1000";
                $l_aJSON['video_engagement'] = $p_xObj['engagement_score'].'%';
                $l_aJSON['video_views'] = $this->makeHumanFriendlyNumbers($p_xObj['video_view']);
                $l_aJSON['video_impression'] = $p_xObj['video_impression'];
                 if($p_nMainChannelId == 30){
                $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                }else{

                     $webm = str_replace(".mp4","-480.webm",$p_xObj['destination']);
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];

                    $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                }
                if($p_nMainChannelId == 15){
                    $l_aJSON['file_url'] = 'http://enrique.starsite.com/forapi/'.$p_xObj['destination'];
                    $l_aJSON['thumbnail'] = 'http://cms.enrique.starsite.com/assets/ufile/'.$p_xObj['image_path'];
                }
            
                if($p_xObj['video_url']!=""){
                    if($p_xObj['video_type']==1){
                    parse_str( parse_url( $p_xObj['video_url'], PHP_URL_QUERY ), $end);
     
                    $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                    
                        if($end['v']!="" && $end['v']!=NULL){
                            $l_aJSON['video_type'] = "youtube";
                            $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                            $l_aJSON['embed_id'] = $end['v'];
                            $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        }

                    }else if($p_xObj['video_type']==2){    
                        $l_aJSON['vimeo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vimeo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==3){ 
                        $l_aJSON['vevo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vevo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==4){ 
                        $l_aJSON['vine_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vine";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }else if($p_xObj['video_type']==5){ 
                        $l_aJSON['instagram_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "instagram";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                    else if($p_xObj['video_type']==NULL){
                     $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                    }else{
                       $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                
                }
                $l_aJSON['deep_link_web_oflo_lp'] = $p_xObj['deep_link_web_oflo_lp'];
                $l_aJSON['deep_link_web_oflo'] = $p_xObj['deep_link_web_oflo'];
                

                 $l_aJSON['channel_id'] = $p_nMainChannelId;
                $l_xSql = AppController::getInstance()->getConnection();
                $l_pSql = "SELECT name, image_path FROM enr_chanel WHERE id = ".$l_aJSON['channel_id']."";  
                $l_nData = $l_xSql->getRow($l_xSql->runQuery($l_pSql));

                $l_aJSON['channel_name'] = $l_nData['name'];
              //  $l_aJSON['channel_image'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$l_nData['image_path'].'?v='.time();   
                $l_aJSON['channel_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/images/chanel.jpg';    
                 
                             // $l_aJSON['file_url'] = 'http://sc-media-assets.s3.amazonaws.com/tyrese/assets/ufile/video/2014/11/102e94a832a06cffdf5385f81790ab84.mp4';
                 $l_aJSON['global_ga_key'] = 'UA-63548102-57';
                 $l_aJSON['channel_slug'] = $p_xObj['slug'];


        // define('STATIC_FILES_HOST','http://d6058xu8xpm39.cloudfront.net');
        // define('CDN_FRONT','http://d1o8tuak29oxqa.cloudfront.net');

                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 2)   +  (( $p_xObj['video_view']/1000) * 10));
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                //$l_aJSON['ss_earnings'] = (  (  $p_xObj['ss_impressions']/1000  * 1.0 ) + ( $p_xObj['ss_videoViews']/1000 * 3.0  * 0.16 ) ) * 0.33;
               
                if($this->_bDemo)
                    $l_aJSON['earnings'] = $p_xObj['video_view']/1000 * 1;

                // 20%
                // $l_aJSON['earnings'] = $l_aJSON['earnings']*.16 *.33;
                $l_aJSON['earnings_title'] = 'Projected Earnings';

                $l_aJSON = (array)$l_aJSON;
                foreach($l_aJSON as $k=>$v){
                    if(!$v)
                        unset($l_aJSON[$k]);
                }

                return $l_aJSON;
            }

            public function buildStatObjectNoReportsSlugOD($p_xObj,$p_nMainChannelId){
                 // print_R($p_xObj);
                $p_nMainChannelId=$p_nMainChannelId;
                $l_bIsVideo = $p_xObj['post_type'];


                $l_mSql = AppController::getInstance()->getConnection();
                $l_rSql = "SELECT slug FROM enr_chanel WHERE id = ".$p_nMainChannelId."";  
                $l_mData = $l_mSql->getRow($l_mSql->runQuery($l_rSql));
               $p_xObj['slug'] = $l_mData['slug'];

                if($l_bIsVideo=="video" || $l_bIsVideo=="Video Post"){ $l_bIsVideo = "video"; }else{ $l_bIsVideo = ""; }
                
                $l_aJSON = array();
                if($l_bIsVideo!=""){ $l_aJSON['isPhoto']=0; $l_aJSON['isVideo']=1;}else{ $l_aJSON['isPhoto']=1; $l_aJSON['isVideo']=0;}
                $l_aJSON['itemId'] = $p_xObj['id'];

                $likencomentsid = $l_aJSON['itemId'];


                $l_aJSON['id'] = $p_xObj['id'];
               
               if($l_bIsVideo!=""){ $l_aJSON['photoId'] = "";  $l_aJSON['videoId'] = $p_xObj['id']; }else{ $l_aJSON['photoId'] = $p_xObj['id'];  $l_aJSON['videoId'] = "";  }
                
                if($l_bIsVideo!=""){ $l_aJSON['photo_db_id'] = ""; $l_aJSON['video_db_id'] = $p_xObj['video_db_id']; $whatIWant = substr($l_aJSON['videoId'], strpos($l_aJSON['videoId'], "_") + 1);     }else{ $l_aJSON['photo_db_id'] = $p_xObj['photo_db_id']; $l_aJSON['video_db_id'] = ""; $whatIWant = substr($l_aJSON['photoId'], strpos($l_aJSON['photoId'], "_") + 1);   }
               $l_aJSON['postId'] = $p_xObj['id'];
                $l_aJSON['postType'] = $p_xObj['post_type'];
                 $l_aJSON['post_type'] = $p_xObj['post_type'];
                  
                  $whatIWant =$likencomentsid;
                   $l_aJSON['commentcount'] = $this->getCommentCount($likencomentsid);
                $l_aJSON['likes'] = $this->getLikes($likencomentsid);  //$l_aJSON['commentcount'] = $this->getCommentCount($likencomentsid);

        
                if($p_nMainChannelId==64){ $p_xObj['slug'] = "viral"; }
                // $l_aJSON['thumbnail'] = $p_xObj['thumbnail'];
                // if($l_aJSON['post_type'] == 'video')
                if($l_bIsVideo!=""){  
                    if($p_nMainChannelId==64){
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/watermark/small_thumb/'.$p_xObj['image_path']; 
                    }
                    $l_aJSON['big_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/big_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['med_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['small_thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['image_path']; 
                    $l_aJSON['spotlight_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_square_thumb/'.$p_xObj['image_path'];   

                }else{
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$p_xObj['destination']; 
                }
                   
                // else
                    // $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                if(!$l_bIsVideo){
                    $p_xObj['start_date'] = $p_xObj['photo_start_date'];
                }
                 
                $l_aJSON['message'] = $p_xObj['description'];
                           //$l_aJSON['description'] = $p_xObj['description'];
                           // $l_aJSON['caption'] = $p_xObj['description'];
               
               
                    $content_id =$p_xObj['id'];
                   $total_v_views = $total_daily_views = 0;
                     if($p_nMainChannelId!=64 && $p_nMainChannelId!=30){
                    $l_xSql = AppController::getInstance()->getConnection();
                    if($l_bIsVideo!=""){  
                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$content_id."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }
                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }   
                        }


                    }else{

                        // $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$whatIWant."";   
                        // $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT is_mobile, page_views_ga, video_views, revenue FROM oflo_reports_monthly WHERE content_id = ".$content_id."";   
                        $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                        foreach($l_nData as $rep_value){
                            if($rep_value['is_mobile']==1){
                                $total_v_views += $rep_value['video_views'];
                            }else{
                                $total_v_views += $rep_value['page_views_ga'];
                            } 
                            $total_v_revenue += $rep_value['revenue'];
                        }

                        if($total_v_views<1){
                            $l_bSql = "SELECT mobile_views FROM oflo_daily_views WHERE content_id = ".$content_id."";   
                            $l_aData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_bSql));

                            foreach($l_aData as $daily_view){
                                $total_daily_views += $daily_view['mobile_views'];

                            }
                        }
                   }
               }

                  

                        if($p_nMainChannelId==30){
                            $l_aJSON['tw_retweets'] = rand(3000,7507);
                            $l_aJSON['tw_favorites'] = rand(3000,7507);
                            $l_aJSON['gp_likes'] = rand(1000,5507);
                            $l_aJSON['gp_comments'] = rand(1000,5507);
                            $l_aJSON['tu_likes'] = rand(1000,5507);
                            $l_aJSON['tu_comments'] = rand(1000,5507);
                            $l_aJSON['message'] = $p_xObj['description'];
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['fb_likes'] =  rand(3000,30507);
                            $l_aJSON['fb_comments'] = rand(3000,30507);
                            $l_aJSON['fb_shares'] = rand(3000,30507);
                            $l_aJSON['fb_reaches'] = rand(3000,30507);
                            $l_aJSON['ss_impressions'] = rand(3000,30507);
                            $l_aJSON['ss_engagements'] = rand(3000,30507);
                            $l_aJSON['ss_pageViews'] = rand(3000,30507);
                            $l_aJSON['ss_videoViews'] = rand(3000,30507);
                            $money = rand(100,5507);
                            $l_aJSON['ss_earnings'] = ("$".number_format($money));
                            $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                            $l_aJSON['url'] =  'http://techslides.com/demos/sample-videos/small.mp4';
                            


                        }else{ 
                        if($total_v_views>0){
                        $l_aJSON['ss_pageViews'] = ($total_v_views) ? ($total_v_views) : '';
                        $l_aJSON['total_views'] = ($total_v_views) ? ($total_v_views) : '';
                        }

                        if($total_daily_views>0){
                        $l_aJSON['live_views'] = ($total_daily_views) ? ($total_daily_views) : '';
                        }
                            $l_aJSON['ss_earnings'] = ("$".number_format(round(($total_v_revenue),0),0)) ? ("$".number_format(round(($total_v_revenue),0),0)) : "N/A";
                            $l_aJSON['ss_impressions'] = "1200";
                            $l_aJSON['created'] = $p_xObj['start_date'];
                            $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                            $l_aJSON['createdDate'] = date('U',strtotime('-0 hours',$l_aJSON['created_time']));
                           
                            $l_aJSON['url'] =  'http://'.$p_xObj['slug'].'.on.oflo.co/ocontent/'.(($l_aJSON['postType'] == 'video') ? 'video/'.$p_xObj['video_db_id'].'/' : 'photo/'.$p_xObj['photo_db_id']).'/';

                        }
                
                $l_sEmbedCodeInfo = Utils::encryptItemId($p_nMainChannelId.','.((1 || $l_bIsVideo) ? 'v' : 'p').','.$p_xObj['id']);
                $l_sEmbedCodeInfo = 'http://'.$p_xObj['slug'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $l_aJSON['embed'] =  base64_encode('<iframe src="'.$l_sEmbedCodeInfo.'"></iframe>');

            
                $l_aJSON['video_length'] = $p_xObj['video_length'];
                $l_sSeconds = $p_xObj['video_length']/1000;
                $l_sHours = floor($l_sSeconds/60/60);
                $l_sMin = floor(($l_sSeconds-($l_sHours*60))/60);
                $l_sSec = round(($l_sSeconds-($l_sHours*60) - ($l_sMin*60)));

                $l_aJSON['ss_videoLengthHuman'] = str_pad($l_sHours,2,"0",STR_PAD_LEFT).':'.str_pad($l_sMin,2,"0",STR_PAD_LEFT).':'.str_pad($l_sSec,2,"0",STR_PAD_LEFT);
                $l_aJSON['ss_videoLengthAndPercent'] = "1000";
                $l_aJSON['video_engagement'] = $p_xObj['engagement_score'].'%';
                $l_aJSON['video_views'] = $this->makeHumanFriendlyNumbers($p_xObj['video_view']);
                $l_aJSON['video_impression'] = $p_xObj['video_impression'];
                 if($p_nMainChannelId == 30){
                $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/ludacris/assets/ufile/video/2015/07/05/195599641308dbc.mp4';
                }else{

                     $webm = str_replace(".mp4","-480.webm",$p_xObj['destination']);
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];

                    $l_aJSON['fileURL'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                $l_aJSON['file_url'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                }
                if($p_nMainChannelId == 15){
                    $l_aJSON['file_url'] = 'http://enrique.starsite.com/forapi/'.$p_xObj['destination'];
                    $l_aJSON['thumbnail'] = 'http://cms.enrique.starsite.com/assets/ufile/'.$p_xObj['image_path'];
                }
                if($p_xObj['video_url']!=""){
           
                    if($p_xObj['video_type']==1){
                    parse_str( parse_url( $p_xObj['video_url'], PHP_URL_QUERY ), $end);
     
                    $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                    
                        if($end['v']!="" && $end['v']!=NULL){
                            $l_aJSON['video_type'] = "youtube";
                            $l_aJSON['youtube_url'] = "https://youtu.be/".$end['v'];
                            $l_aJSON['embed_id'] = $end['v'];
                            $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        }

                    }else if($p_xObj['video_type']==2){   
                    $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        $l_aJSON['vimeo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vimeo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                    }else if($p_xObj['video_type']==3){ 
                       $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        $l_aJSON['vevo_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vevo";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                    }else if($p_xObj['video_type']==4){ 
                       $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        $l_aJSON['vine_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "vine";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                    }else if($p_xObj['video_type']==5){ 
                        $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                        $l_aJSON['instagram_url'] = $p_xObj['video_url'];
                        $l_aJSON['video_type'] = "instagram";
                        $endid = substr($p_xObj['video_url'], strrpos($p_xObj['video_url'], '/')+1);
                        $l_aJSON['embed_id'] = $endid;
                    }
                    else if($p_xObj['video_type']==NULL){
                     $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio'];
                    }else{
                       $l_aJSON['video_type'] = "flowplayer";   
                     $l_aJSON['image_aspect_ratio'] = $p_xObj['image_aspect_ratio']; 
                    }
                }
                $l_aJSON['deep_link_web_oflo_lp'] = $p_xObj['deep_link_web_oflo_lp'];
                $l_aJSON['deep_link_web_oflo'] = $p_xObj['deep_link_web_oflo'];

                

                 $l_aJSON['channel_id'] = $p_nMainChannelId;
                $l_xSql = AppController::getInstance()->getConnection();
                $l_pSql = "SELECT name, image_path FROM enr_chanel WHERE id = ".$l_aJSON['channel_id']."";  
                $l_nData = $l_xSql->getRow($l_xSql->runQuery($l_pSql));

                $l_aJSON['channel_name'] = $l_nData['name'];
               // $l_aJSON['channel_image'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/small_thumb/'.$l_nData['image_path'].'?v='.time(); 
                $l_aJSON['channel_image'] = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$p_xObj['slug'].'/images/chanel.jpg';     
                
                         // $l_aJSON['file_url'] = 'http://sc-media-assets.s3.amazonaws.com/tyrese/assets/ufile/video/2014/11/102e94a832a06cffdf5385f81790ab84.mp4';
                $l_aJSON['global_ga_key'] = 'UA-63548102-57';
                $l_aJSON['channel_slug'] = $p_xObj['slug'];


        // define('STATIC_FILES_HOST','http://d6058xu8xpm39.cloudfront.net');
        // define('CDN_FRONT','http://d1o8tuak29oxqa.cloudfront.net');

                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 2)   +  (( $p_xObj['video_view']/1000) * 10));
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                //$l_aJSON['ss_earnings'] = (  (  $p_xObj['ss_impressions']/1000  * 1.0 ) + ( $p_xObj['ss_videoViews']/1000 * 3.0  * 0.16 ) ) * 0.33;
               
                if($this->_bDemo)
                    $l_aJSON['earnings'] = $p_xObj['video_view']/1000 * 1;

                // 20%
                // $l_aJSON['earnings'] = $l_aJSON['earnings']*.16 *.33;
                $l_aJSON['earnings_title'] = 'Projected Earnings';

                $l_aJSON = (array)$l_aJSON;
                foreach($l_aJSON as $k=>$v){
                    if(!$v)
                        unset($l_aJSON[$k]);
                }

                return $l_aJSON;
            }

            private function cleanArg($p_sStr){
                return trim(strip_tags($p_sStr));
            }

            protected function showError($p_nErrorCode,$p_sMessage){
                $this->outputJSON('{
                                "status": "'.$p_nErrorCode.'",
                                "msg": '.json_encode($p_sMessage).'
                                }');
            }

            protected function outputJSON($p_sJSON,$p_bExit = true){
                $datemod = $this->getLastModifiedDate();
                header('Content-Type: application/json');
                header('Last-Modified: '.$datemod);
                echo $p_sJSON;
                if($p_bExit)
                    exit;
            }

            public function format_interval(DateInterval $interval) {
                $result = "";
                if ($interval->y) { $result .= $interval->format("%y years "); }
                if ($interval->m) { $result .= $interval->format("%m months "); }
                if ($interval->d) { $result .= $interval->format("%d days "); }
                if ($interval->h) { $result .= $interval->format("%h hours "); }
                if ($interval->i) { $result .= $interval->format("%i minutes "); }
                if ($interval->s) { $result .= $interval->format("%s seconds "); }

                return $result;
            }


            public function getLastModifiedDate(){

                $l_xSql = AppController::getInstance()->getConnection();
                $l_sSql = "SELECT start_date FROM enr_video WHERE is_publish=1 ORDER BY id  DESC";

                $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
                $i = 0;
                foreach($l_xData as $l_xRow){
                    if($i>0){ break; }
                    $newvar = strtotime($l_xRow['start_date']);
                    $i++;
                }
                $futureDate = $newvar;
                $cleandate = date('m/d/Y H:i:s', $futureDate);
                
                return $cleandate;
            

            }
        
            public function rq_getLastModifiedChannelDate(){

                $l_xSql = AppController::getInstance()->getConnection();
                $l_sSql = "SELECT last_updated_timestamp FROM channel_update ORDER BY last_updated_timestamp  DESC";

                $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
                $i = 0;
                foreach($l_xData as $l_xRow){
                    if($i>0){ break; }
                    $newvar = $l_xRow['last_updated_timestamp'];
                    $i++;
                }
                $futureDate = $newvar;
                $cleandate = date('m/d/Y H:i:s', $futureDate);
                $l_aReturnData['status'] = 1;
                $l_aReturnData['last_modified'] = $cleandate;
                $this->outputJSON(json_encode($l_aReturnData));
                
                return $cleandate;
            

            }

            public function rq_getLastModifiedCategoriesDate(){

                $l_xSql = AppController::getInstance()->getConnection();
                $l_sSql = "SELECT last_updated_timestamp FROM category_update ORDER BY last_updated_timestamp  DESC";

                $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));


                $i = 0;
                foreach($l_xData as $l_xRow){
                    if($i>0){ break; }
                    $newvar = $l_xRow['last_updated_timestamp'];
                    $i++;
                }
                $futureDate = $newvar;
                $cleandate = date('m/d/Y H:i:s', $futureDate);
                $l_aReturnData['status'] = 1;
                $l_aReturnData['last_modified'] = $cleandate;
                $this->outputJSON(json_encode($l_aReturnData));
                
                return $cleandate;
            

            }

            public function rq_updateTodaysViews($number_of_views){
                $number_of_views = array();

                $number_of_views = $_POST['number_of_views'];
                $number_of_views = json_decode($number_of_views, true);
               
                $l_xSql = AppController::getInstance()->getConnection();

                foreach(array_keys($number_of_views) as $l_xRow=>$key){

                  
                   $content_id = $key;

                   $view_increase = $number_of_views[$key];

                   $l_sSql = "INSERT INTO oflo_daily_views (content_id,mobile_views) VALUES ($content_id,$view_increase) ON DUPLICATE KEY UPDATE mobile_views = mobile_views + $view_increase";
                   $l_xSql->runQuery($l_sSql);

                }

                $l_aReturnData['status'] = 1;
                $this->outputJSON(json_encode($l_aReturnData));

                return;

            }








}


class VTBaseConvert
{
    // public static $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    public static $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    public static $base = 36;
    public static function encode($val) 
    {
        // can't handle numbers larger than 2^31-1 = 2147483647
        $str = '';
        do {
            $i = $val % self::$base;
            $str = self::$chars[$i] . $str;
            $val = ($val - $i) / self::$base;
        } while($val > 0);
        return $str;
    }
 
    public static function decode($str) 
    {
        $len = strlen($str);
        $val = 0;
        $arr = array_flip(str_split(self::$chars));
        for ($i = 0; $i < $len; ++$i) 
        {
            $val += $arr[$str[$i]] * pow(self::$base, $len-$i-1);
        }
        return $val;
    }
}
    

?>