<!-- BEGIN PAGE -->
<div class="page-content">
     <div class="container-fluid" id="dashboard">
    
    
		
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Posts
                    
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Thumbnail</th>
                            <th>Caption</th>
                            <th>Type</th>
                            <th>Duration in seconds</th>
                            <th>Revenue</th>
                            <th>Views</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Actions</th>                            
                        </tr>
                    </thead>
                    <tbody>     
                    <?php 
                    foreach($feeds as $feed): 
                    $ids = explode("_", $feed['id']);
                    if ($ids[0] == "video" && ($feed['video_row']['video_url'] != '0' && $feed['video_row']['video_url'] != "NULL" && $feed['video_row']['video_url'] != NULL)) {
                        $post_controller_name = "videoEmbed";
                    } else {
                        $post_controller_name = $ids[0];
                    }
                    ?>
                        <tr class="postfeed">
                            <td class="feed-thumbnail-td grid">
                            <?php if($feed['image_path']!=""){ 
							
                                echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="'.$feed['image_path'].'"></a><br/>';
                            } else {  echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="/assets/img/click-to-view.jpg"></a><br/>'; } ?>
                            </td>
                            <td class="span2 grid">
                                <div style='word-wrap:break-word; width: 200px'><?php if($feed['caption']){echo substr($feed['caption'], 0, 200);}else{ echo "Processing - Content will be available shortly"; } ?>
                                    <?php if($feed['is_sticky']) :?>
                                    <span style="color:red;font-weight:bold;"><sup>STICKY</sup></span>
                                    <?php endif;?>
                                </div>
                            </td>
                            <td class="grid"><?=$feed['post_type'];?></td>

                            <td class="grid">

                                <?php if($feed['duration'] != "0" && $feed['duration'] != ""){

                                    echo floor($feed['duration']);

                                }else{ echo "N/A"; } ?>
                            </td>
                            <td class="grid">$<?=number_format(floor(($feed['revenue'])), 0);?></td>
<!--                            <td class="grid">--><?//=number_format($feed['page_views_ga']);?><!--</td>-->
                            <td class="grid"><?=number_format($feed['total_views']);?></td>

                            <?php if ($feed['is_publish']==0){
                                $permission = "Transcoding";
                            } elseif ($feed['is_publish']==1) {
                                $permission = "Published";
                            } elseif ($feed['is_publish']==2) {
                                $permission = "Draft";
                            } elseif ($feed['is_publish']==3) {
                                $permission = "Removed";
                            } ?>
                            <td class="grid"><?php echo $permission ?></td>
                            <td class="grid"><?php echo $feed['start_date']; ?></td>
                            
                            <td class="grid">
                            <?php
                                $ids = explode("_", $feed['id']);
                                $post_controller_name = $ids[0];

                                if ($feed['video_row']['video_url'] != '0' && $feed['video_row']['video_url'] != 'NULL' && $feed['video_row']['video_url'] != NULL){
                                    $feed_copy_id = str_replace('video', 'videoEmbed', strtolower($feed['id']));
                                } else {
                                    $feed_copy_id = $feed['id'];
                                }

                                if ($feed['is_publish'] =='1'){

                            echo
                            ' <a class="btn" href="javascript:post_unpublish(\''.$feed['id'].'\')">'.
                              'Unpublish'.
                             '</a>'; } 
                             if ($feed['is_publish'] =='3') {echo' <a class="btn" href="javascript:post_republish(\''.$feed['id'].'\')">'.
                              'Re-Publish'.
                             '</a>'; }


                            echo
                            '<a class="btn" href="javascript:confirm_del(\''.$feed['id'].'\')">'.
                            'Delete'.
                            '</a>';
                            ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
		
    </div>
</div>
<script>
    function confirm_del(id) {
        if(confirm("Do you want to delete this feed?")) {
            document.location.href = "<?php echo site_url('content/delete_feed'); ?>/" + id;
        }
    }    

    function post_unpublish(id) {
        if(confirm("Do you want to unpublish this post?")) {
            document.location.href = "<?php echo site_url('content/post_unpublish'); ?>/" + id;
        }
    }   

    function post_republish(id) {
        if(confirm("Do you want to re-publish this post?")) {
            document.location.href = "<?php echo site_url('content/post_republish'); ?>/" + id;
        }
    }     
</script>