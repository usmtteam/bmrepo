<div id="fancybox_container" style="width:640px; height: 360px;display: none;">
    <?php
        $start_key =  $page * $page_count;
        $end_key = min(count($feeds), $start_key + $page_count) - 1;

        for($key = $page * $page_count; $key <= $end_key; $key++) :
            $feed = $feeds[$key];

            if($feed['link']!=""){
                $url = $feed['link'];
            }else{
                $novideo = true;
                $url = ASSETS_DIR.'/img/no-video-found.jpg';

            }
        ?>

        <div class="video center" id='video_container_<?php echo $feed['id']; ?>' style='height:100%; '>
            <iframe width="640" height="360" id="player<?php echo $feed['id']; ?>" preload="none" src="<?=$feed['link']?>"></iframe>
        </div>

    <?php endfor; ?>
</div>

<input type="hidden" value="<?php echo $base_url; ?>" id='base_url'>

<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <div class="page-title">
                    Master Curator
                </div>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>

        <div class="row-fluid">
            <div class="portlet-body">

                <form action="" method="get">
                    Viral Media Partners:
                    <select class="gray" name="partner">
                        <option value="all">All</option>

                        <?php foreach($viral_partners as $item): ?>

                        <option value="<?=$item['partner_name']?>" <?php if(strtolower($partner_name) == strtolower($item['partner_name'])){echo "selected";}?> ><?=$item['partner_name']?></option>

                        <?php endforeach; ?>
                    </select>

                    Category:
                    <select class="gray" name="category">
                        <option value="">All Categories</option>

                        <?php foreach($viral_media_category as $category): ?>

                        <option value="<?=$category['cat_name']?>" <?php if(strtolower($selected_category) == strtolower($category['cat_name'])){echo "selected";}?> ><?=$category['cat_name']?></option>

                        <?php endforeach; ?>
                    </select>

                    <input type="submit" name="filter_btn" value="Filter" class="btn btn_mastercur">

                    <br><br>

                    <select class="gray" name="items_per_page" style="width: 77px" onchange="this.form.submit();">
                        <?php foreach ($items_per_page_array as $item_per_page): ?>
                            <option value="<?=$item_per_page?>" <?php if ($page_count == $item_per_page){echo "selected";} ?> ><?=$item_per_page?></option>
                        <?php endforeach; ?>
                    </select>
                    Items per page

                    <div style="float: right">
                        <input type="text" name="search" value="<?=$search?>" class="">
                        <input type="submit" name="search_btn" value="Search" class="btn btn_mastercur">
                    </div>
                </form>

                <table class="table bootstrap-datatable">
                    <thead>
                    <tr>
                        <th>Thumbnail</th>
                        <th>Title</th>
                        <th>Duration</th>
                        <th>Resolution</th>
                        <th>Tags</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                        <?php
//                            $start_key =  $page * $page_count;
//                            $end_key = min(count($feeds), $start_key + $page_count) - 1;

                            for($key = $page * $page_count; $key <= $end_key; $key++) :
                                $feed = $feeds[$key];
//                                echo "<br><br>\$feed[$key]: ";
//                                var_dump($feed);
                        ?>


                        <tr>
                            <td class="feed-thumbnail-td grid">
                                <?php if($feed['thumbnail_url']!=""){

                                    echo '<a title="'.$feed['title'].'" class="fancybox" id="fancybox_'.$feed['id'].'" style="cursor: pointer;">
                                            <input type="hidden" class="video_info" value="'.$feed['id'].'">
                                            <div class="gallery-video-container">
                                            <img class="feed-thumbnail-large pull-left" src="'.$feed['thumbnail_url'].'"></div></a>';
                                } else {  echo '<a href="'.site_url('master_curater/curate/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="/assets/img/click-to-view.jpg"></a><br/>'; }
                                ?>
                            </td>
                            <td class="grid"><?php if($feed['title']!=""){ echo $feed['title']; }else{ echo "No title provided"; } ?></td>
                            <td class="grid"><?php if($feed['asset_length']!=""){ 

                                $seconds = $feed['asset_length'];
                                

                                $hours = floor($seconds / 3600);
                                $mins = floor(($seconds - ($hours*3600)) / 60);
                                $secs = floor($seconds % 60);

                                 if($hours<1){ 
                                    if($secs<10){ $secs = str_pad($secs, 2, '0', STR_PAD_LEFT); }
                                    echo $mins.":".$secs; 
                                 }else{ 
                                    if($mins<10){ $mins = str_pad($mins, 2, '0', STR_PAD_LEFT); }
                                    echo $hours.":".$mins.":".$secs; 
                                 }


                                 }else{ echo "N/A"; } ?></td>
                            <td class="grid"><?php if($feed['hd']!=""){ echo $feed['hd']; }else{ echo "N/A"; } ?></td>
                            <td class="span2 grid"><div style='word-wrap:break-word; width: 200px'><?php if($feed['keywords']){echo str_replace("unwatermarked","",$feed['keywords']); }else{ echo "No tags available"; } ?></div></td>
                            <td class="grid">
                                <?php

                                echo '<a class="btn" href="'.site_url('master_curater/curate/'.$feed['id']).'">Curate</a>'
                                    .'<a class="btn" href="'.site_url('master_curater/remove/'.$feed['id']).'">Remove</a>'
                                ;

                                ?>
                            </td>
                        </tr>
                        <?php if($key % 4 == 3 || $key == $end_key) : ?>
                        </div><!-- end of portlet-body? -->
<!--                        <div class="space10"></div>-->
                        <?php if($key != $end_key) :?>
<!--                        <div class="row-fluid"><!-- row-fluid  -->
                        <?php
                            endif;
                        endif;
//                        if($key==$page_count){ break; }
//                        $key++;
                        ?>
                        <?php endfor; ?>


                    </tbody>
                </table>

                
                <div class="span6">
                <?php //$big_count = $total_page_count/$page_count; echo "BIG COUNT".$big_count; ?>
                <div class="dataTables_info">Showing <?php echo (($page_count*($page))+1); ?> to <?php if($total_count<($page_count*($page+1))){ echo $total_count; }else{ echo ($page_count*($page+1)); } ?> of <?php echo $total_count; ?> entries</div>
                </div><!-- end of row-fluid -->
                    <div class="dataTables_paginate paging_bootstrap pagination">
                        <ul>
                            <li class="prev<?php echo $page == 0 ? ' disabled' : ''?>">
                                <a data-page="<?php echo max(0, $page - 1);?>" style="cursor: pointer;">← Previous</a>
                            </li>
                            <?php for($i = 0; $i < $total_page_count; $i++) :?>
                                
                            <li class="<?php echo $page == $i ? ' active' : ''; ?>">
                                <a data-page="<?php echo $i;?>" style="cursor: pointer;"><?php echo $i+1; ?>
                                </a>
                            </li>
                            
                            <?php endfor; ?>
                            <li class="next<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>">
                                <a data-page="<?php echo min($total_page_count-1, $page + 1); ?>" style="cursor: pointer;">Next →</a>
                            </li>
                        </ul>
                    </div>
                
                <script language="javascript">
                    $(document).ready(function() {
                        $('.pagination a').click(function(e) {
                            var base_url = jQuery('#base_url').val();
//                            var tag = $('.video_tags_search').val();
//                            if(tag == '') tag = '0';
//                            document.location.href = base_url + "/" + tag + '/' + $(e.target).attr('data-page');
                            document.location.href = base_url + "&page=" + $(e.target).attr('data-page');
                        });
                    });
                </script>
            </div>
    
            <!-- END PAGE PORTLET BODY-->
        </div>
        <!-- END PAGE ROW-FLUID-->

    </div>
    <!-- END GALLERY MANAGER PORTLET-->
</div>
<!-- END PAGE-->

<link href="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<script src="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.pack.js" ></script>
<script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/htmlplayer/mediaelement-and-player.min.js"></script>
<link href="<?php echo ASSETS_DIR; ?>/htmlplayer/mediaelementplayer.min.css" rel="stylesheet" />

<script>

//    $('video').mediaelementplayer({
//        success: function(media, node, player) {
//            $('#' + node.id + '-mode').html('mode: ' + media.pluginType);
//        }
//    });


</script>