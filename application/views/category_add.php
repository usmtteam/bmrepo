
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Add Category
                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Add Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
               
                    <?php 

                    echo form_open_multipart("", 'id="videofileupload" class="form-horizontal"');
                        echo form_error("cat_name");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <div class="row-fluid">
                        <div class="control-group">
                          
                                <label class="control-label" name="category" for="url" value="<?php echo set_value('category'); ?>">Name</label>
                            <div class="controls customarea">
                           
                            <?php  echo form_input('cat_name',$cat_name,'class="large m-wrap"');?>
                            
                            </div>
                        
                        </div>

                        <div class="control-group">
                                <label class="control-label" for="url">Breaking Movement Category?</label>
                                <input type='checkbox' name='oflo' check >
                            
                        </div>
                         <div class="control-group">
                               
                                <label class="control-label" for="url">Breaking Movement Menu?</label>
                                <input type='checkbox' name='oflo_menu' check >
                        </div>


                        <div class="control-group">
                          
                                <div class="control-group"> <label class="control-label" for="url" value="<?php echo set_value('hex_color');?>" >Hex Color</label>
                            <div class="controls customarea">

                        <input type="text" class="form-control" placeholder="#" id="hex_color" name="hex_color" /> 
                            </div></div>

                      <!--   <div class="control-group"> <label class="control-label" for="url">Category Image</label> -->

                         <div class="control-group"> <label class="control-label" for="url">Category Image</label>
                         </div>
                          <div class="span6" style="margin:0px;">
                          <div class="control-group">
                          <input type="hidden" id="photo_width" name="photo_width" val="" />
                            <input type="hidden" id="photo_height" name="photo_height" val="" />
                            <input type="hidden" id="photo_crop_x1" name="photo_crop_x1" val="" />
                            <input type="hidden" id="photo_crop_y1" name="photo_crop_y1" val="" />
                            <input type="hidden" id="photo_crop_x2" name="photo_crop_x2" val="" />
                            <input type="hidden" id="photo_crop_y2" name="photo_crop_y2" val="" />
                            <input type="hidden" id="photo_crop_w" name="photo_crop_w" val="" />
                            <input type="hidden" id="photo_crop_h" name="photo_crop_h" val="" />
                            <input type="hidden" id="photo_img_value" name="photo_img_value" value="">
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="border-style:none !important; width: 200px; height: 200px;">
                                    
                                   
                                </div>

                                   <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100%;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input style="float:right; width:200px;" type="file" name="image" class="default"/></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                     <?php echo form_submit('submit', "submit",' class="btn blue"');?>

                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.0/js/bootstrap-colorpicker.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.0/css/bootstrap-colorpicker.min.css">

<script>
$(function(){
    $('#hex_color').colorpicker();
});
</script>

<script language="javascript">
            $('div.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
              var img = $(e.target).find('img');
              if(img.length > 0) {
                img.load(function(e) {
                    var w = 1000;
                    var h = 1000;
                  $('#photo_width').val(w);
                  $('#photo_height').val(h);
                  img.attr('width', w + 'px' );
                  img.attr('height', h + 'px');
                  img.Jcrop({
                    onSelect: updateCoords,
                    onChange: updateCoords,
                     bgOpacity: 0.5,
                    setSelect: [140,102, 0, 0], 
                    aspectRatio : 102/114,

                   


                    
                    
                    
                  });

                });
              }
            });
            function updateCoords(c) {
              $('#photo_crop_x1').val(c.x);
              $('#photo_crop_y1').val(c.y);
              $('#photo_crop_x2').val(c.x2);
              $('#photo_crop_y2').val(c.y2);
              $('#photo_crop_w').val(c.w);
              $('#photo_crop_h').val(c.h);
            }
          </script>


                     <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>
 <script src="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.js"></script>
<link href="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.css" rel="stylesheet" />



