<?php

require_once("../../include/lib/livefyre-php-utils/src/Livefyre.php");
require_once("../../include/lib/php-jwt/src/JWT.php");
require_once("../../include/lib/requests/library/Requests.php");

use Livefyre\Livefyre;
use Livefyre\Exceptions\ApiException;
Requests::register_autoloader();

// Check for asset result's URL
if (!empty($l_xAssetResult['url'])) {
    $oflourl = "http://".$l_xAssetResult['url'].".".OFLO_LANDING;

    $network = Livefyre::getNetwork("starclub.fyre.co", "9GlDgawpgrNRGprDbwi82+RYm5s=");
    //update a Network's name and key
    $network->getData()->setName("starclub.fyre.co");
    $network->getData()->setKey("9GlDgawpgrNRGprDbwi82+RYm5s=");

    //set SSL off
    $network->setSsl(false);

    //get system and user tokens
    $systemToken = $network->buildLivefyreToken();

    //make sure a system token is still valid
    $isValid = $network->validateLivefyreToken($systemToken);

    //get the $network URN
    $networkUrn = $network->getUrn();

    $site = $network->getSite("379527", "Wlxen62aRHqR5qIPE9fRkbvugWg=");

    $newsite = $site->getData()->getId();

    $siteUrn = $site->getUrn();

    // 255 character limit on title
    $commentsCollection = $site->buildCommentsCollection(substr($l_sMetaTitle, 0, 255), LIVEFYRE_ARTICLE.$l_nAssetId, $oflourl);

    $articleId = $commentsCollection->getData();

    $commentCollectionMetaToken = $commentsCollection->buildCollectionMetaToken();

    $commentCollectionCheckSum = $commentsCollection->buildChecksum();

    $commentsCollection->createOrUpdate();

    try {
        $test = $commentsCollection->getCollectionContent();
    } catch (ApiException $e) {
        print("LOG: can't retrieve content of a collection that has not been created!");
    }
}

?>

<div class="">

    <? include '_menu.php';

    if ($l_sBackgroundImage) : ?>

        <div class="fixed-width margin-bottom pos-rel box-menu ch-bn"><?= $l_sBackgroundImage ?>
            <h1<?= (strlen($l_sBackgroundImage) > 5) ? ' class="ch-name" ' : '' ?>><?= $l_xChannel->site_header_title ?></h1>
        </div>

        <?
    endif;

    if (strpos($l_xAssetResult['destination'], '.mov') !== FALSE){
        $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mov/",".jpg",$l_xAssetResult['destination']));
        $small_thumb='http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/big_thumb/'.preg_replace("/video/","photo",preg_replace("/\.mov/",".jpg",$l_xAssetResult['destination']));
    }else{
        $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mp4/",".jpg",$l_xAssetResult['destination']));
        $small_thumb='http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/big_thumb/'.preg_replace("/video/","photo",preg_replace("/\.mp4/",".jpg",$l_xAssetResult['destination']));
    }
    $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.$l_xAssetResult['image_path'];
    $small_thumb='http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/big_thumb/'.$l_xAssetResult['image_path'];


    ?>
    <div class="fixed-width">
        <div class="col1">

            <? if (!empty($l_xAssetResult['url']) && $l_bIsVideo){ ?>

                <?php if( $l_xAssetResult['video_type'] != 0 && $l_xAssetResult['video_type'] != NULL && $l_xAssetResult['video_type'] != "NULL" ){ ?>


                    <?php  if ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){
                        $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                        $parsed_url_arr = explode('/', $parsed_url);
                        $count = count($parsed_url_arr);
                    ?>

                     <div class="container-instagram">

                         <script src="//platform.instagram.com/en_US/embeds.js"></script>
                         <iframe class="instagram-iframe" allowtransparency="true" frameborder="0" src="http://instagr.am/p/<?=$parsed_url_arr[$count - 1]?>/embed" ></iframe>

                    <?php } else { ?>

                    <div class="iframe-wrapper">

                        <?php } if ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE){
                            parse_str( parse_url( $l_xAssetResult['video_url'], PHP_URL_QUERY), $end );
                            ?>
                            <iframe class="embed-iframe" width="659" height="422" src="https://www.youtube.com/embed/<?=$end['v']?>" frameborder="0" allowfullscreen></iframe>
                        <?php } elseif ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_VIMEO){
                            $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                            $parsed_url_arr = explode('/', $parsed_url);
                            $count = count($parsed_url_arr);
                            ?>
                            <iframe class="embed-iframe" src="https://player.vimeo.com/video/<?=$parsed_url_arr[$count - 1]?>" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                        <?php } elseif ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_VINE){
                            $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                            $parsed_url_arr = explode('/', $parsed_url);
                            $count = count($parsed_url_arr);
                            ?>
                            <iframe class="embed-iframe" src="https://vine.co/v/<?=$parsed_url_arr[$count - 1]?>/embed/simple" width="600" height="600" frameborder="0"></iframe>

                        <?php } ?>
                    </div>

                    <?php } else {
                        $path_parts = pathinfo($l_xAssetResult['destination']);
                        $video_height = $l_xAssetResult['video_height'];
                        $video_duration = $l_xAssetResult['video_duration'];
                    ?>
                        <div class="videojs-player-holder">

                            <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>

                                <video id="my-video" class="video-js vjs-big-play-centered" controls preload="none" width="100%" height="100%"
                                       poster="<?= $small_thumb ?>">

                            <? } else {

                                $a = array();
//                                $video_destination = str_replace('.mp4', '', $l_xAssetResult['destination']);
                                $video_destination = $path_parts['dirname']."/".$path_parts['filename'];
                                $firstHalfImgURL = CDN_FRONT."/".$l_xChannel->url."/assets/ufile/watermark/";
                                $secondHalfImgUrl = str_replace($firstHalfImgURL, "", $l_sImageUrl);
                                $a['video'] = $video_destination;
                                $a['video_height'] = $video_height;
                                $a['video_duration'] = $video_duration;
                                $a['id'] = $l_nAssetId;
                                $a['slug'] = $l_xChannel->url;
                                $a['ga_tag'] = $l_xChannel->google_analytics_key;
                                $a['img_url'] = $secondHalfImgUrl;
                                $a['title'] = $l_xAssetResult['description'];
                                $a['previous_id'] = $l_xPreviousVideoResult['id'];
                                $hash = base64_encode(json_encode($a));
//                                $longEmbedUrl = "http://".$l_xChannel->url.".".OFLO_LANDING."/oembed.php?a=".$hash;
                                $longEmbedUrl = "http://".$l_xChannel->url.".".OFLO_LANDING."/oembed/".$hash;
                                $shortEmbedUrl = Utils::get_bitly_url($longEmbedUrl);
                                $shortEmbedUrl = str_replace(array("\n", "\r"), '', $shortEmbedUrl);

                            ?>

                                <video id="my-video" class="video-js vjs-big-play-centered vjs-default-skin" preload="none" width="676" height="380"
                                       poster="<?= $small_thumb ?>" data-setup='{"controls": true, "plugins": {"embed": {"embeddable": true, "location":"<?= $shortEmbedUrl ?>"}}}'>

                            <? } ?>

                            <!--  Video source loop -->
                            <?php
                                $video_res_arr = ["360", "480", "720", "1080"];

                            if (!empty($video_height) || $video_height != 0){

                                for ($ctr = 0; $video_res_arr[$ctr] <= $video_height; $ctr++ ) {
                            ?>
                                <source type='application/x-mpegurl' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>/master.m3u8" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                                <source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-<?=$video_res_arr[$ctr]?>.mp4" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                                <source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-<?=$video_res_arr[$ctr]?>.webm" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                                <source type='video/flash' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-<?=$video_res_arr[$ctr]?>.mp4" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                            <?php
                                }
                            } ?>
                                <source type='application/x-mpegurl' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>/master.m3u8" label="216" res="216">
                                <source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-216.mp4" label="216" res="216">
                                <source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-216.webm" label="216" res="216">
                                <source type='video/flash' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-216.mp4" label="216" res="216">

                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                </p>
                            </video>

                            <?php if($l_nDeviceType != DEVICE_TYPE_MOBILE) { ?>

                            <div class="endscreen">
                            <span>
                                <a class="button"
                                   href="http://<?=$_SERVER['HTTP_HOST']?>/ocontent/video/<?=$l_xPreviousVideoResult['id']?>">Next video</a>
                                <a class="vjs-toggle">Play it again!</a>
                                <BR><b>Embed code</b>
                                <BR><textarea id="embed"><?=htmlspecialchars('<iframe src="'.$shortEmbedUrl.'" width="100%" height="100%" frameborder="0"></iframe>'); ?>
                                </textarea>

                            </span>
                            </div>

                            <?php } ?>

                        </div>

                        <!-- If you'd like to support IE8 -->
                        <script src="//vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
                        <script src="//vjs.zencdn.net/5.4.6/video.js"></script>
<!--                        <script src="--><?//=STATIC_FILES_HOST?><!--/assets/videojs-embed-master-0.3.3/dist/videojs.embed.js"></script>-->
                        <script src="<?=STATIC_FILES_HOST?>/assets/videojs-embed-master-0.3.3/dist/videojs.embed.min.js"></script>

<!--                        <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-ga-master-4.6.1/libs/video-js-4.6.1/video.dev.js"></script>-->
<!--                        <script src="--><?//=STATIC_FILES_HOST?><!--/assets/videojs-ga-master-4.6.1/dist/videojs.ga.min.js"></script>-->

                        <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
                        <script src="<?=STATIC_FILES_HOST?>/assets/videojs-contrib-ads-master-3.1.2/src/videojs.ads.js"></script>
                        <!--                    <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-contrib-ads-1.1.0/src/videojs.ads.js"></script>-->
                        <script src="<?=STATIC_FILES_HOST?>/assets/videojs-ima-master-5.3/src/videojs.ima.js"></script>
                        <script src="<?=STATIC_FILES_HOST?>/assets/videojs-resolution-switcher-master-0.4.2/lib/videojs-resolution-switcher.js"></script>

                    <?php } ?>

                <? } else if (!empty($l_xAssetResult['url']) && !$l_bIsVideo){ ?>
                    <div id="gallery_back">
                        <div id="image-back"><img id="main_img" class="backup_picture" src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xAssetResult['url'] ?>/assets/ufile/<?php echo $l_xAssetResult['destination'] ?>" />
                        </div>
                        <a class="back_button <?= ($l_xPreviousPhotoResult) ? 'full' : 'inactive' ?>" href="/ocontent/photo/<?= $l_xPreviousPhotoResult['id'] ?>/"><</a>
                        <a class="next_button <?= ($l_xNextPhotoResult) ? 'full' : 'inactive' ?>" href="/ocontent/photo/<?= $l_xNextPhotoResult['id'] ?>/">></a>
                    </div>
                <? } else{} ?>

                <div class="desc-area">
                    <?php if (!empty($l_xAssetResult['url'])) { ?>
                        <div class="desc">
                            <h3><?= $l_xAssetResult['description'] ?></h3>
                            <span><? echo $l_xAssetResult['total_views'].' Views - '.Utils::time_passed($l_xAssetResult['start_date_timestamp']+25200); ?></span>

                            <br />
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont fb">f</a></li>
                                <li><a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>?page_type=twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tw">t</a></li>
                                <li><a href="http://tumblr.com/widgets/share/tool?canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tb">u</a></li>
                                <li><a href="https://plus.google.com/share?url=canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont gp">g</a></li>
                            </ul>
                            <div id="likecount_wrapper">
                                <div id="post_like_count">
                                    <?= $l_nTotalLikes ?>
                                </div>

                                <div id="addlike" class="a-do-add-like <?= (($l_nUserTotalLikes>0) ? 'icon-star-full' : 'icon-star-empty' ) ?> ng-scope button_star<?= (($l_nUserTotalLikes>0) ? '1' : '' ) ?>" ></div>
                            </div>
                        </div>

                        <div id="livefyre-app-<?= LIVEFYRE_ARTICLE.$l_nAssetId ?>"></div>

                        <script>
                            Livefyre.require(["fyre.conv#3","auth","lfep-auth-delegate#0"],

                                function (Conv, auth, LFEPAuthDelegate) {
                                    var authDelegate = new LFEPAuthDelegate({
                                        engageOpts: {
                                            app: "starclub.auth.fyre.co"
                                        }
                                    });
                                    auth.delegate(authDelegate);
                                });
                        </script>
                        <? /*        <script>
                          Livefyre.require(["identity#1", "auth"],
                          function (Identity, auth) {
                            var identity = new Identity({
                                app: "https://identity.livefyre.com/starclub.fyre.co"
                            });
                            auth.delegate(identity);
                          }
            );
            </script> */ ?>
                        <script>
                            (function() {
                                Livefyre.require(['fyre.conv#3'], function(Conv) {
                                    new Conv({
                                        network: 'starclub.fyre.co'

                                    }, [{
                                        app: 'main',
                                        siteId: "<?php echo $newsite ?>",
//                                        articleId: 'prod-oflo-<?//= $l_nAssetId ?>//',
//                                        el: 'livefyre-app-prod-oflo-<?//= $l_nAssetId ?>//',
                                        articleId: '<?= LIVEFYRE_ARTICLE.$l_nAssetId ?>',
                                        el: 'livefyre-app-<?= LIVEFYRE_ARTICLE.$l_nAssetId ?>',

                                    }], function (widget) {
                                        // Initialize or Auth

                                        widget.on('commentPosted', function (data) {
                                            // Do something, perhaps using data
                                            //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                            commentPosted(data);
                                        });



                                        widget.on('commentCountUpdated', function (data) {
                                            // Do something, perhaps using data
                                            $.ajax({
                                                type: "GET", // GET, POST , PUT, PATCH
                                                url: "http://<?=OFLO_HOME?>/commentcount.php",
                                                data: {data:data, id:<?= $l_nAssetId ?>},
                                                success: function(data) { // Data will be your HTML code returned from the PHP script.
                                                    // Process data object
                                                }
                                            });
                                            commentCountUpdated(data);
                                            //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                        });
                                        widget.on('userLoggedIn', function (data) {
                                            // Do something, perhaps using data

                                            var user = data.id;
                                            var avatar = data.avatar;
                                            var displayname = data.displayName;
                                            setCookie('user', user, 365);
                                            setCookie('avatar', avatar, 365);
                                            setCookie('displayname', displayname, 365);
                                            userLoggedIn(data);


                                            //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                        });

                                        widget.on('userLoggedOut', function (data) {
                                            // Do something, perhaps using data

                                            deleteCookie('user');
                                            deleteCookie('avatar');
                                            deleteCookie('displayname');
                                            userLoggedOut(data);

                                            //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                        });

                                        function commentPosted(data){
                                            return data;
                                        }
                                        function userLoggedIn(data){
                                            return data;
                                        }
                                        function userLoggedOut(data){
                                            return data;
                                        }
                                        function commentCountUpdated(data){
                                            return data;
                                        }


                                    });

                                });
                            }());
                        </script>

                     <?php } else { ?>
                         <div class="desc">
                            <h3>This channel has no content available yet. Please check back later.</h3>
                         </div>
                    <?php } ?>

                </div><!-- end of desc-area -->
                <div class="assets-list">
                    <ul class="nav nav-tabs">
                        <li class="asset-head <?= $l_bIsVideo ? 'active': ''?>"><a data-toggle="tab" href="#videos">Videos</a></li>
                        <li class="asset-head <?= !$l_bIsVideo ? 'active': ''?>"><a data-toggle="tab" href="#photos">Photos</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="videos" class="tab-pane fade <?= $l_bIsVideo ? 'in active': ''?>">
                            <h5>Latest Videos</h5>
                            <ul>

                                <? AppController::getInstance()->outputOfloVideoListItemsFlowPlayerMobile($l_aFPVideos,$l_xChannel, $channelViews);  ?>
                            </ul>
                        </div>

                        <div id="photos" class="tab-pane fade <?= !$l_bIsVideo ? 'in active': ''?>">
                            <h5>Latest Photos</h5>
                            <ul>
                                <div class="fp-playlist">
                                    <?php foreach($l_aAllFoundAssets as $photo_list_result){ ?>
                                        <li>
                                            <a href="/ocontent/photo/<?= $photo_list_result['id'] ?>/" class="fp-list">
                                                <div class='vid_thumb_desc'>
                                                    <img class='vid_thumb backup_picture'
                                                        src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $photo_list_result['url'] ?>/assets/ufile/small_thumb/<?= $photo_list_result['destination']; ?>">
                                                </div>
                                                <p class="vid_desc"><?= substr($photo_list_result['description'], 0, 45) . '...'; ?><br>
                                                    <span class="dates"><? echo Utils::time_passed($photo_list_result['start_date_timestamp']); ?></span>
                                                </p>
                                                <div class="clr"></div>
                                            </a>
                                        </li>
                                    <? } ?>
                                </div><!-- end of videojs-playlist -->
                            </ul>
                        </div>

                    </div>
                </div>
            </div><!-- end of col1 -->

            <div class="col2" style="text-align: center">
                <div class="page-unit-2">
                  
                            <div id="channel_top_wrap">
                            <div id="channel_title_right"><? if($l_xChannel->site_title!=""){ echo $l_xChannel->site_title; } ?></div>
                            <div id="channel_type_right"><?  if($l_xChannel->channel_type!=""){ echo $l_xChannel->channel_type; } ?></div>
                            </div>
                            <div id="image_wrap">
                            <img class="profile-img" src="http://d3hl6zmfu9utfu.cloudfront.net/<? echo $l_xChannel->slug; ?>/images/chanel.jpg?v=<?= rand(0000000000,99999999999); ?>>">
                            </div>
                            <div id="channel_description_right"><? if($l_xChannel->channel_description!=""){ echo substr($l_xChannel->channel_description,0,255); } ?><a class="more_link" href="<? if($l_xChannel->channel_bio!=""){ echo $l_xChannel->channel_bio; } ?>">  More...</a></div>
                            <div id="channel_socials">
                            <a href="<? if($l_xChannel->channel_site_link!=""){ echo $l_xChannel->channel_site_link; } ?>"><img src="/assets/images/website.png"></a>
                            <a href="<? if($l_xChannel->channel_twitter_link!=""){ echo $l_xChannel->channel_twitter_link; } ?>"><img src="/assets/images/twitter.png"></a>
                            <a href="<? if($l_xChannel->channel_facebook_link!=""){ echo $l_xChannel->channel_facebook_link; } ?>"><img src="/assets/images/facebook.png"></a>
                            <a href="<? if($l_xChannel->channel_instagram_link!=""){ echo $l_xChannel->channel_instagram_link; } ?>"><img src="/assets/images/instagram.png"></a>
                            </div>
                    

                    

                </div><!-- end of page-unit-2 -->
                <div class="page-unit-2">
                    <?php if (stripos($l_xAssetResult['url'], "catandmaya") === FALSE){

                        if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                            <? if (stripos($l_xAssetResult['project_widget'], "script") === FALSE){ ?>
                                <div style="position:relative;float:left; display:inline-block;">
                               <? echo $l_xAssetResult['product_widget']; ?>
                               </div>
                               <? }else{ ?>
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- GlobalUnit-320x50 -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:320px;height:50px"
                                 data-ad-client="ca-pub-7963321383486508"
                                 data-ad-slot="6866262471"></ins>
                            <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                          <?  }
                            }else{ ?>
                            <!--/*
                              *
                              * Starclub Adserver Asynchronous JS Tag
                              * - Generated with Starclub Adserver v3.2.1
                              *
                              */-->

                            <!--/*  * Replace all instances of INSERT_RANDOM_NUMBER_HERE with
                              * a generated random number (or timestamp).
                              *
                              */-->
                     
                            <? if (stripos($l_xAssetResult['project_widget'], "script") === FALSE){ ?>
                                <div style="position:relative;float:right; display:block; max-width:320px;">
                               <? echo $l_xAssetResult['product_widget']; ?>
                               </div>
                               <? }else{ ?>
                            <div style="position:relative; height:250px; width:320px; display:block;">
                            <ins data-revive-zoneid="22" data-revive-ct0="INSERT_ENCODED_CLICKURL_HERE" data-revive-id="1971854465e6781f5bd42454610c8a7f"></ins>
                            <script async src="//scads.starsite.com/www/delivery/asyncjs.php"></script></div>

                        <? } 
                        } // end of ads

                    } else { ?>

                        <div class="page-unit-2-filler" style="width:300px; height:254px;"></div>

                    <?php }// end of catandmaya channel check?>

                </div><!-- end of page-unit-2 -->



            <? if($l_bIsMobile){ ?>

            </div>
            <!-- close div on line 77 -->

            <div class="clr"></div>
            <div>
            <? } /* ?>
                <div class="page-unit-3 hidden-xs">
                    <? if (stripos($l_xAssetResult['url'], "catandmaya") === FALSE){

                        if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

                            <!-- Mobile Large 320x100 -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:320px;height:100px"
                                 data-ad-client="ca-pub-7963321383486508"
                                 data-ad-slot="2284614472"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        <? }else{ ?>
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- Large Skyscraper -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:300px;height:600px"
                                 data-ad-client="ca-pub-7963321383486508"
                                 data-ad-slot="8908182473"></ins>
                            <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                        <? } // end of ads ?>

                    <?php } else { ?>

                            <div class="page-unit-3-filler" style="display:inline-block;width:160px;height:600px"></div>

                    <?php }// end of catandmaya channel check ?>
                </div><!-- end of page-unit-3 --> 
                */ ?>
            </div>

            <div class="clr"></div>
            <!-- tested -->
            <div class="hidden-xs fixed-width">
                <div class="well margin-bottom col-md-12">
                    <div id="myCarousel" class="carousel slide">
                        <p>Related <?php echo $p_cats;?> videos...</p>
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active landing-page-carousel">
                                <div class="inner-carousel">
                                    <?php $l_xOfloGetCats_count = count($l_xOfloGetCats);
                                    for ($i = 0; $i < $l_xOfloGetCats_count && $i <= 3; $i++) : ?>

                                        <div class="col-sm-3 landing_slider_image">
                                            
                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE ||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM ){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endfor; ?>
                                </div>
                                <!--/-->
                            </div>
                            <!--/item active-->
                            <?php for ($i; $i < $l_xOfloGetCats_count && $i >= 3; $i++) : ?>
                                <!--/item-->
                                <div class="item landing-page-carousel">
                                    <div class=" inner-carousel">
                                        <div class="col-sm-3 landing_slider_image">
                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                        <?php while($i < $l_xOfloGetCats_count - 1 && $i >= 3 && (($i + 1) % 4 != 0)): ?>
                                            <div class="col-sm-3 landing_slider_image">
                                            <?php $i++ ?>
                                               <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <!--/-->

                                </div>
                                <!--/item-->

                            <?php endfor; ?>

                            <!--/carousel-inner-->
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="width:2%; color:black; background-image: none; outline: none;" >
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="width:2%; color:black; background-image: none; outline: none;">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <!--/myCarousel-->
                </div>
                <!-- well -->
            </div>
           
            <!-- tested -->
            <div class="hidden-xs fixed-width">
                <div class="well col-md-12">
                    <div id="myCarousel2" class="carousel slide">
                        <p class="categoryText">Related <?php echo $p_cat;?> videos...</p>
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active landing-page-carousel">
                                <div class=" inner-carousel">
                                    <?php  $l_xOfloGetCat_count = count($l_xOfloGetCat);
                                    for ($i = 0; $i < $l_xOfloGetCat_count && $i <= 3; $i++) : ?>
                                        <div class="col-sm-3 landing_slider_image">

                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>

                                    <?php endfor; ?>
                                </div>
                                <!--/-->
                            </div>
                            <!--/item active-->
                            <?php for ($i; $i < $l_xOfloGetCat_count && $i >= 3; $i++) : ?>
                                <!--/item-->
                                <div class="item landing-page-carousel">
                                    <div class=" inner-carousel">
                                        <div class="col-sm-3 landing_slider_image">

                                           
                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                        <?php while($i < $l_xOfloGetCat_count - 1 && $i >= 3 && (($i + 1) % 4 != 0)): ?>
                                            <div class="col-sm-3 landing_slider_image">
                                             <?php $i++ ?>
                                                
                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <!--/-->
                                </div>
                                <!--/item-->
                            <?php endfor; ?>
                            <!--/carousel-inner-->
                        </div>
                        <a class="left carousel-control" href="#myCarousel2" data-slide="prev" style="width:2%; color:black; background-image: none; outline: none;" >
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel2" data-slide="next" style="width:2%; color:black; background-image: none; outline: none;">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <!--/myCarousel-->
                </div>
                <!-- well -->
            </div>
        </div>

        <?php if (stripos($l_xAssetResult['url'], "catandmaya") === FALSE){ ?>

        <div class="page-unit-1 fixed-width hidden-lg hidden-md hidden-sm">
            <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

                 <!-- GlobalUnit-320x50 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:100%;height:50px"
                     data-ad-client="ca-pub-7963321383486508"
                     data-ad-slot="6866262471"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            <? }else{ ?>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- GA-SY-SS-Leaderboard-Ad6 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:100%;height:90px"
                 data-ad-client="ca-pub-7963321383486508"
                 data-ad-slot="3012923272"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            <? } // end of ads ?>
        </div><!-- end of page-unit-1 -->

        <?php } // end of catandmaya channel check ?>


        <div class="hidden-lg hidden-md hidden-sm">
            <div class="well margin-bottom">
                <div id="myCarousel3" class="carousel slide">
                    <p>Related <?php echo $p_cats;?> videos...</p>
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="item active landing-page-carousel">
                            <div class=" inner-carousel">

                                <?php $l_xOfloGetCats_count = count($l_xOfloGetCats);
                                for ($i = 0; $i < $l_xOfloGetCats_count && $i <= 1; $i++) : ?>

                                    <div class="col-xs-6 landing_slider_image_mobile">
                                      <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM) { echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                        </a>
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <!--/-->
                        </div>
                        <!--/item active-->
                        <?php for ($i; $i < $l_xOfloGetCats_count && $i > 1; $i++) : ?>
                            <!--/item-->
                            <div class="item landing-page-carousel">
                                <div class=" inner-carousel">
                                    <div class="col-xs-6 landing_slider_image_mobile">
                                        <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                        </a>
                                    </div>
                                    <?php while($i < $l_xOfloGetCats_count - 1 && $i > 1 && (($i + 1) % 2 != 0)): ?>
                                        <div class="col-xs-6 landing_slider_image_mobile">
                                         <?php $i++ ?>
                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCats[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCats[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <!--/-->
                            </div>
                            <!--/item-->
                        <?php endfor; ?>
                        <!--/carousel-inner-->
                    </div>
                    <a class="left carousel-control" href="#myCarousel3" data-slide="prev" style="width:1%; color:black; background-image: none; outline: none;" >
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel3" data-slide="next" style="width:1%; color:black; background-image: none; outline: none;">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <!--/myCarousel-->
            </div>
            <!-- well -->
        </div>

        <div class="hidden-lg hidden-md hidden-sm">
            <div class="well margin-bottom">
                <div id="myCarousel4" class="carousel slide">
                    <p>Related <?php echo $p_cat;?> videos...</p>
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="item active landing-page-carousel">
                            <div class=" inner-carousel">

                                <?php $l_xOfloGetCat_count = count($l_xOfloGetCat);
                                for ($i = 0; $i < $l_xOfloGetCat_count && $i <= 1; $i++) : ?>

                                    <div class="col-xs-6 landing_slider_image_mobile">
                                        <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <!--/row-->
                        </div>
                        <!--/item active-->
                        <?php for ($i; $i < $l_xOfloGetCat_count && $i > 1; $i++) : ?>
                            <!--/item-->
                            <div class="item landing-page-carousel">
                                <div class="inner-carousel">
                                    <div class="col-xs-6 landing_slider_image_mobile">
                                        <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                    <?php while($i < $l_xOfloGetCat_count - 1 && $i > 1 && (($i + 1) % 2 != 0)): ?>
                                        <div class="col-xs-6 landing_slider_image_mobile">
                                         <?php $i++ ?>
                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <div <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='content-img-wrap-embeded'";}else{echo "class='content-img-wrap'";} ?>>
                                                <img class=" backup_picture <?php if ($l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE||$l_xOfloGetCat[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'content_carousel_embeded';}else{echo 'content_carousel';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/small_thumb/';
                                                echo $l_xOfloGetCat[$i]['image_path'];?>" class="img-responsive response margin">
                                                </div>
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                        <?php endfor; ?>
                        <!--/carousel-inner-->
                    </div>
                    <a class="left carousel-control" href="#myCarousel4" data-slide="prev" style="width:1%; color:black; background-image: none; outline: none;" >
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel4" data-slide="next" style="width:1%; color:black; background-image: none; outline: none;">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <!--/myCarousel-->
            </div>
            <!-- well -->
        </div>
    </div>
    <div class="clr"></div>
    <div class="powered-by" >
        <p><a target="_blank" href="http://starsite.com"><img src="/assets/images/powered-by-starsite.png"></a></p>
    </div>
</div>

<?php
    $siteurl = "http://".$l_xChannel->slug.".on.breakingmuscle.com";
    $end_of_url = str_replace($siteurl,"",$l_sCurrentAbsoluteURLNoVars);
?>

<script type="text/javascript">

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>.on.breakingmuscle.com');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
    ga('set', 'page', '<?= $end_of_url ?>');
</script>

<?php if( $l_bIsVideo && !is_numeric($l_xAssetResult['video_type']) ){ ?>

    <script type="text/javascript">

        var player = videojs('my-video');

        <? if ($l_nDeviceType == DEVICE_TYPE_DESKTOP) {
            if ($video_height >= 480){
                $default_resolution = 480;
            } elseif ($video_height >= 360){
                $default_resolution = 360;
            } else {
                $default_resolution = 216;
            }
        ?>

            player.videoJsResolutionSwitcher(
                {default: '<?=$default_resolution?>'}
            );

        <?php } ?>

        player.ready( function(){

            console.log('++++++++++++++++ready');
//            player.poster("<?//= $l_sImage ?>//");

            <?php // Just play 30+ video or catandmaya channel
            if (($l_nDeviceType != DEVICE_TYPE_MOBILE && $video_duration < 30) || stripos($l_xAssetResult['url'], "catandmaya") !== FALSE) { ?>

                console.log("start");
                console.log("play");

                this.play();

            <? } ?>

            this.one('loadedmetadata', function(){

                console.log('++++++++++++++++loadedmetadata');

                this.trigger('video_ga_events');

            }).on('play', function(){

                console.log('++++++++++++++++play');

                this.poster(""); // Remove Android video poster bug
                $('.endscreen').hide();
                $('.vjs-big-play-button').hide();

            }).on('contentended', function(){ // listener for the 'ended' event of the video player

                console.log('++++++++++++++++contentended');

                var dur = Math.round(this.duration());
                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);

                <?php if (preg_match("/android/i", $Device_Make)){ ?>
//                    $('.vjs-big-play-button').show();
//                    $('.vjs-control-bar').hide();
                <?php } else { ?>
                    $('.endscreen').show();
                <? } ?>

                this.one('play', function(){
                    this.trigger('video_ga_events');
//                    $('.vjs-control-bar').show(); // Android replay control fix
                });

                $('.vjs-control-bar, .vjs-menu-button').one('click', function(){
                    console.log('vjs-control-bar area');
                    $('.endscreen').hide();
                });

                this.one('timeupdate', function(){
                    console.log('one timeupdate');
                    $('.endscreen').hide();
                });

            }).on('error', function(){

                console.log('++++++++++++++++error');

                var dur = Math.round(this.duration());
                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);

            }).on('video_ga_events', function(){

                console.log('video_ga_events');

                var dur = parseInt(this.duration());
//              console.log("dur: " + dur);

                var pause = Math.round(dur * .25) + 1;
                pause = pause * 1000;

                var pausehalf = Math.round(dur * .5) + 1;
                pausehalf = pausehalf * 1000;

                var pausethreefourths = Math.round(dur * .75) + 1;
                pausethreefourths = pausethreefourths * 1000;

//              console.log('this.duration(): ' + this.duration());
//              console.log('dur: ' + dur);
//              console.log('pause: ' + pause);
//              console.log('pausehalf: ' + pausehalf);
//              console.log('pausethreefourths: ' + pausethreefourths);

                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Impression', dur);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
                },pause);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Half', dur);
                },pausehalf);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Three-Fourths', dur);
                },pausethreefourths);

            }).one('resolutionchange', function(){
                console.info('autoplay');
                this.autoplay(true); // Autoplay resolution switcher
            }).on('resolutionchange', function(){
                console.info('Source changed to %s', this.src());
            });

            <?php // Fire contentended event on ended event for non-mobile Android, short videos, or catandmaya channel
            if ( ($l_nDeviceType != DEVICE_TYPE_MOBILE && preg_match("/android/i", $Device_Make)) || $video_duration < 30 ||
                        stripos($l_xAssetResult['url'], "catandmaya") !== FALSE){ ?>

                console.log('enable pseudo-contentended');

                this.on('ended', function(){

                    this.trigger('contentended');
                });

            <? } ?>

        });

        $(function() {

            console.log('doc ready');

            <?php // Mobile video 30+ sec video, catandmaya channel
            if ($l_nDeviceType == DEVICE_TYPE_MOBILE && $video_duration > 30 && stripos($l_xAssetResult['url'], "catandmaya") === FALSE){ ?>

                $("#my-video").one('touchstart', function(e){

                    console.log('touchstart');
                    var player = videojs('my-video');
                    player.pause();

                    init_ima_plugin();

                });

            <? } ?>

            <?php // Non-Android, non-mobile video 30+ sec video, catandmaya channel
            if ( (!preg_match("/android/i", $Device_Make) && $l_nDeviceType != DEVICE_TYPE_MOBILE) && $video_duration > 30 &&
                    stripos($l_xAssetResult['url'], "catandmaya") === FALSE) { ?>

                console.log("Start");
                init_ima_plugin();

            <? } ?>

            $('.vjs-toggle').click(function () {

//    		        console.log('.vjs-toggle');

                player.play();
            });

            function init_ima_plugin(){

                player.bigPlayButton.hide();

                console.log('init_ima_plugin');

                // Initialize the IMA plugin
                var options = {
                    id: 'my-video',
                    //adTagUrl: '<?=$l_sAdServerURL?>',
//                    debug: true,
                    numRedirects: 2,
                    vpaidMode: google.ima.ImaSdkSettings.VpaidMode.INSECURE,
                };
                player.ima(options);
                player.ima.addContentEndedListener = "contentended";
                player.ima.initializeAdDisplayContainer();

                // Request ads and play the video
                player.ima.requestAds();
                console.log("Play-Pause");
                player.play();
                player.pause();
            }

        });

    </script>
<? } // end of if( $l_bIsVideo && !is_numeric($l_xAssetResult['video_type'])) { ?>