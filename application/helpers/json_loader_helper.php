<?php

/**
 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
 * array containing the HTTP server response header fields and content.
 */
function _get_json( $url, $header = '', $post = '', $cookie = '' )
{
	try {
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_SSL_VERIFYHOST => FALSE
		);
		
		if(!empty($header)) {
			$options[CURLOPT_HTTPHEADER] = $header;
		}
		if(!empty($post)) {
			$options[CURLOPT_POSTFIELDS] = $post;
		}
		if(!empty($cookie)) {
			$options[CURLOPT_COOKIESESSION] = true;
			$options[CURLOPT_COOKIEJAR] = $cookie;
			$options[CURLOPT_COOKIEFILE] = $cookie;
		}
		
		$url = str_replace(' ', '%20', $url);
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		
		return json_decode($content);
	}
	catch(Exception $e) {
		throw new Exception('_get_json() : curl error!');
	}
}

function _get_text( $url, $header = '', $post = '', $cookie = '' ) {
	try {
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_SSL_VERIFYHOST => FALSE
		);
		
		if(!empty($header)) {
			$options[CURLOPT_HTTPHEADER] = $header;
		}
		if(!empty($post)) {
			$options[CURLOPT_POSTFIELDS] = $post;
		}
		if(!empty($cookie)) {
			$options[CURLOPT_COOKIESESSION] = true;
			$options[CURLOPT_COOKIEJAR] = $cookie;
			$options[CURLOPT_COOKIEFILE] = $cookie;
		}
		
		$url = str_replace(' ', '%20', $url);
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		
		return $content;
	}
	catch(Exception $e) {
		throw new Exception('_get_text() : curl error!');
	}
}