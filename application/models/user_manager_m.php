<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Manager_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

     // get list of user managers
    
    function get_user_manager(){

     $sql = "SELECT *
                FROM users";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }

    // add user manager

    function add_user_manager($post){


     $this->load->database();
     $username= $this->input->post('username');
     $email = $this->input->post('email');
     $cid= $this->input->post('cid');
     $user_group_id = $this->input->post('user_group_id');

     


          $post = array (
            'username' => $username, 
            'email' => $email,
           'user_group_id' => $user_group_id,
           

        );


         $insert_query = "INSERT IGNORE INTO users (username, email, user_group_id) 
                     VALUES('$username', '$email', '$user_group_id')";


                   
          $this->db->query($insert_query); 
    


     }



     //  delete user manager

    function confirm_deletion($data){
     $this->db->select('*');
     $this->db->from('users');
     $this->db->where('id',$data);
     $query = $this->db->get();
     $result=$query->result();
     return $result;

     }


     // update user manager


     function update_user_manager($id,$data){

     $this->db->where('id', $id);
     $result = $this->db->update('users', $data);
      return $result;

     }


     // get user manager id

    function get_user_manager_id($data){
     $this->db->select('*');
     $this->db->from('users');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
     
     }







   
}