<?php



    require_once(__DIR__.'/../../../config/settings.php');
    require_once(__DIR__.'/../front/FrontCoreSql.php');
    require_once(__DIR__.'/../front/FrontCoreManager.php');
    define('ZENCODER_API_KEY','b34cc8e8b92ae58bf3f1a151608a0ef6');
    define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');



    $l_sNewS3Mov  = $_SERVER['argv'][1];
    $l_nItemsId   = intval($_SERVER['argv'][2]);
    $video_height = intval($_SERVER['argv'][3]);

    if ($video_height === 0) {
        $video_height = 1280;
    }

    $l_sS3RelativeFile = str_replace(FILES_PATH,"",$l_sNewS3Mov);

    echo "\n\n\$l_sNewS3Mov: ";
    echo "\n\n".$l_sNewS3Mov;

    echo "\n\nFILES_PATH: ";
    echo "\n\n".FILES_PATH;

    echo "\n\n\$l_sS3RelativeFile: ";
    echo "\n\n".$l_sS3RelativeFile;

    echo "\n";

    $l_aPathInfo = pathinfo($l_sS3RelativeFile);
	$l_sS3RelativeFolder = $l_aPathInfo['dirname'];
	$l_sRelativeFileWithoutExtension = $l_aPathInfo['dirname'].'/'.$l_aPathInfo['filename'];
	$l_sJPG = $l_aPathInfo['filename'];


    Utils::pushFileToS3($l_sNewS3Mov,$l_sS3RelativeFile);


    $l_aFileTypes = array(

    						array( 'label' => 'mp4',  'output' => $l_sRelativeFileWithoutExtension, 'ext' => '.mp4'),
                            array( 'label' => 'webm', 'output' => $l_sRelativeFileWithoutExtension, 'ext' => '.webm')

    					);

    $l_aGrouping = array();

    foreach($l_aFileTypes as $l_nCount => $l_aFileType){

        while (TRUE){

            if ($video_height >= 1080) {

                $l_aGrouping[] = array(
                    "label" => $l_aFileType['label'].'-1080',
                    "public" => 1,
                    'width' => '1920',
                    'height' => '1080',
                    'h264_profile' => 'high',
                    "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_aFileType['output'].'-1080'.$l_aFileType['ext']);
            }

            if ($video_height >= 720) {

                $l_aGrouping[] = array(
                    "label" => $l_aFileType['label'].'-720',
                    "public" => 1,
                    'width' => '1280',
                    'height' => '720',
                    'h264_profile' => 'high',
                    "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_aFileType['output'].'-720'.$l_aFileType['ext']);

            }

            if ($video_height >= 480) {

                $l_aGrouping[] = array(
                    "label" => $l_aFileType['label'].'-480',
                    "public" => 1,
                    'width' => '720',
                    'height' => '480',
                    'h264_profile' => 'high',
                    "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_aFileType['output'].'-480'.$l_aFileType['ext']);
            }

            if ($video_height >= 360) {

                $l_aGrouping[] = array(
                    "label" => $l_aFileType['label'].'-360',
                    "public" => 1,
                    'width' => '640',
                    'height' => '360',
                    'h264_profile' => 'baseline',
                    "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_aFileType['output'].'-360'.$l_aFileType['ext']);
            }

            $l_aGrouping[] = array(
                "label" => $l_aFileType['label'].'-216',
                "public" => 1,
                'width' => '384',
                'height' => '216',
                'h264_profile' => 'baseline',
                "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_aFileType['output'].'-216'.$l_aFileType['ext']);

            break;
        }

    }


    //  $l_aSingleFile = array(
    //     "label" => 'mp4-360',
    //     "public" => 1,
    //     'width' => '640',
    //     'height' => '360',
    //     'h264_profile' => 'baseline',
    //     "url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_sRelativeFileWithoutExtension.'-360.mp4');

    //  $l_aGrouping[] = $l_aSingleFile;

    // print_R($l_aGrouping);


// exit;

		// if($l_nCount == 0){

		// 	$l_aSingleFile["thumbnails"] = array(
	 //                                        "format" => "jpg",

	 //                                        "base_url" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_sS3RelativeFolder,
	 //                                        "filename" => $l_sJPG,
	 //                                        "public" => 1
	 //                                        );

  //           //"start_at_first_frame" => 1,

	 //       	// $l_aSingleFile["watermarks"] = array(
	 //        //                                 "url" => SITE_DOMAIN_FOR_CALLBACKS."img/global/watermark-photos.png",
	 //        //                                 "width" => 60,
	 //        //                                 "opacity" => 0.5
	 //        //                                 );

	 //        // $l_aSingleFile["notifications"] = array(
	 //        //                                 "url" => SITE_DOMAIN_FOR_CALLBACKS."zencoderCallback/".$l_nItemsId,
	 //        //                                 "format" => "json",
	 //        //                                 "event" => "output_finished"
	 //        //                                 );
		// }



    $l_sFilepathBase = 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_sRelativeFileWithoutExtension.'/';


    $l_aOutputs  = array(
        array( 'audio_bitrate' => 32,  'audio_sample_rate' => 22050,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 152,  'decoder_buffer_size' => 152,  'filename' => 'file-150.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 120, 'width' => 400, 'height' => 224, 'h264_profile' => baseline, 'h264_level' => 3,   'max_frame_rate' => 10, 'format' => 'ts'),
        array( 'audio_bitrate' => 32,  'audio_sample_rate' => 22050,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 264,  'decoder_buffer_size' => 264,  'filename' => 'file-200.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 200, 'width' => 416, 'height' => 234, 'h264_profile' => baseline, 'h264_level' => 3,   'max_frame_rate' => 15, 'format' => 'ts'),
        array( 'audio_bitrate' => 32,  'audio_sample_rate' => 22050,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 364,  'decoder_buffer_size' => 364,  'filename' => 'file-300.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 300, 'width' => 448, 'height' => 252, 'h264_profile' => baseline, 'h264_level' => 3,   'max_frame_rate' => 15, 'format' => 'ts'),
        array( 'audio_bitrate' => 40,  'audio_sample_rate' => 22050,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 464,  'decoder_buffer_size' => 464,  'filename' => 'file-400.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 400, 'width' => 480, 'height' => 270, 'h264_profile' => main,     'h264_level' => 3,   'max_frame_rate' => 30, 'format' => 'ts'),
        array( 'audio_bitrate' => 64,  'audio_sample_rate' => 44100,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 664,  'decoder_buffer_size' => 664,  'filename' => 'file-600.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 600, 'width' => 640, 'height' => 360, 'h264_profile' => baseline, 'h264_level' => 3,   'max_frame_rate' => 30, 'format' => 'ts'),
        array( 'audio_bitrate' => 128, 'audio_sample_rate' => 44100,  'base_url' => $l_sFilepathBase, 'decoder_bitrate_cap' => 1928, 'decoder_buffer_size' => 1928, 'filename' => 'file-1800.m3u8','public' => 1, 'type' => 'segmented', 'video_bitrate' => 1800,'width' => 960, 'height' => 540, 'h264_profile' => main,     'h264_level' => 3.1, 'max_frame_rate' => 30, 'format' => 'ts'),


        array( 'base_url' => $l_sFilepathBase, 'filename' => 'cellular.m3u8', 'public' => 1,

            'streams' =>  array(
                array( 'bandwidth' => 300,  'path' => 'file-300.m3u8'),
                array( 'bandwidth' => 200,  'path' => 'file-200.m3u8'),
                array( 'bandwidth' => 150,  'path' => 'file-150.m3u8'),
            ),

            'type' => 'playlist'
        ),

        array( 'base_url' => $l_sFilepathBase, 'filename' => 'master.m3u8', 'public' => 1,

            'streams' =>  array(
                array( 'bandwidth' => 1800, 'path' => 'file-1800.m3u8'),
                array( 'bandwidth' => 600,  'path' => 'file-600.m3u8'),
                array( 'bandwidth' => 400,  'path' => 'file-400.m3u8'),
                array( 'bandwidth' => 300,  'path' => 'file-300.m3u8'),
                array( 'bandwidth' => 200,  'path' => 'file-200.m3u8'),
                array( 'bandwidth' => 150,  'path' => 'file-150.m3u8'),
            ),

            'type' => 'playlist'

        ),

    );

    $l_aGrouping = array_merge($l_aGrouping,$l_aOutputs);


 	$l_aData = array("input" => 's3://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'/'.$l_sS3RelativeFile,
                    "outputs" => $l_aGrouping);


     print_R($l_aData);

        try {

            $l_xFrontCoreSql = new FrontCoreSql();
            spl_autoload_unregister('coreAutoload');
            require(ZENCODER_CLASS);

            $l_xZencoder    = new Services_Zencoder(ZENCODER_API_KEY);
            $l_xZencoderCheck = new Services_Zencoder(ZENCODER_API_KEY);
            $l_xEncodingJob = $l_xZencoder->jobs->create($l_aData);

            // Success if we got here
            echo "w00t! \n\n";
            echo "Job ID: ".$l_xEncodingJob->id."\n";
            echo "Output ID: ".$l_xEncodingJob->outputs['web']->id."\n";

            // Store Job/Output IDs to update their status when notified or to check their progress.
            $l_sSql = 'UPDATE enr_video
                       SET zencoder_id = "'.$l_xEncodingJob->id.'"
                       WHERE id = '.$l_nItemsId;

            echo "\n\n".$l_sSql;
            $l_xFrontCoreSql->runQuery($l_sSql);

            echo "\n\n----------------------------------------------------------------------------------------------------";

            $l_xEncodingJobCheck = $l_xZencoderCheck->jobs->progress($l_xEncodingJob->id);

            echo "\n\n\$l_xEncodingJobCheck: ";
            var_dump($l_xEncodingJobCheck);

            echo "\n\n1. \$l_xEncodingJobCheck->state: ";
            var_dump($l_xEncodingJobCheck->state);

            while ($l_xEncodingJobCheck->state != "finished") {

                sleep(60);

                $l_xEncodingJobCheck = $l_xZencoderCheck->jobs->progress($l_xEncodingJob->id);

                echo "\n\n\$l_xEncodingJobCheck: ";
                var_dump($l_xEncodingJobCheck);

                echo "\n\n2. \$l_xEncodingJobCheck->state: ";
                var_dump($l_xEncodingJobCheck->state);

//                if ($l_xEncodingJobCheck->state == "pending")    { echo "\n\nPending"; }
//                if ($l_xEncodingJobCheck->state == "waiting")    { echo "\n\nWaiting"; }
//                if ($l_xEncodingJobCheck->state == "processing") { echo "\n\nProcessing"; }
//                if ($l_xEncodingJobCheck->state == "finished")   { echo "\n\nFinished"; }
                if ($l_xEncodingJobCheck->state == "failed")     {
                    error_log("Zencoder Job ID ".$l_xEncodingJob->id. ": Failed");
                    exit;
                } elseif ($l_xEncodingJobCheck->state == "cancelled")  {
                    error_log("Zencoder Job ID ".$l_xEncodingJob->id. ": nCancelled");
                    exit;
                }

            }

            // Update status from transcoding to published.
            $l_sSql = 'UPDATE enr_video
                       SET is_publish = 1
                       WHERE id = '.$l_nItemsId;
            echo "\n\n".$l_sSql;
            $l_xFrontCoreSql->runQuery($l_sSql);


        } catch (Services_Zencoder_Exception $e) {
            // If were here, an error occured


             echo "Fail :(\n\n";
             echo "Errors:\n";
             foreach ($e->getErrors() as $error) echo $error."\n";
             echo "Full exception dump:\n\n";
             print_r($e);
        }
        spl_autoload_register('coreAutoload');

?>


