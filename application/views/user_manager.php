<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    User Manager
                    <small></small>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('user_manager/user_manager_add'); ?>" class="btn blue">
                        Add New User
                        </a>
                    </div>
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>User ID </th>
                            <th>Username </th>
                            <th>Email </th>
                             <th>Channel ID </th>
                            <th>User Group</th>
                            <th class="span2">Edit</th>
                            <th class="span2">Delete</th>
                        </tr>
                    </thead>
                    <tbody>  
                           
                        <tr>
                         
                       
                            <?php foreach($user_manager as $user): ?>
                       
                            <td>
                                <?php echo $user['id']; ?>
                                                          
                            </td>

                             <td>
                                <?php echo $user['username']; ?>
                                                          
                            </td>
                            
                            <td>
                                <?php echo $user['email']; ?>
                                                          
                            </td>

                             <td>
                                <?php echo $user['cid']; ?>
                                                          
                            </td>

                             <td>
                                <?php echo $user['user_group_id']; ?>
                                                          
                            </td>

                            <td class="center" style="text-align: center;">
                          
                            <?php
                                echo 
                                '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="'.site_url('user_manager/user_manager_edit/'.$user['id']).'">'.
                                '<i class="icon-edit" ></i>Edit'.
                                '</a>';
                            ?>
                        
                            </td>
                            <td class="center" style="text-align: center;">
                                
                            <?php
                                echo 
                                '<a class="btn" style="padding-left: 1px; padding-right: 17px;" href="javascript:confirm_deletion(\''.$user['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                       
                            </td>
                        </tr>

                             
            
           
                               <?php endforeach; ?> 
                 

               
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    function confirm_deletion(id) {
        if(confirm("Do you want to delete this user?")) {
            document.location.href = "<?php echo site_url('user_manager/confirm_deletion'); ?>/" +id;
        }
    }    
</script>