<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Settings
                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
      
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="pb-feed">
                 <div class="portlet-body form">

                     <?php 
                    echo form_open_multipart("design", 'class="chanel-design form-horizontal"');
                        echo form_error("description");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }

                         //                     var_dump($this->data['post']['facebook']);
    
                         if (!empty($this->session->flashdata('facebook'))){
                             echo "<div class='alert alert-success'>".$this->session->flashdata('facebook')."</div>";
                         }
                    ?>  

                    <h3>Connected Social Media</h3>
                        <div class="span12">
                            <div class="row-fluid" style="float:left; min-height:100px;">
                    
                    <h4>Facebook</h4>
                    <div class= "fb ssfont facebook-new-cms facebook-active float-left" "style="float:left; margin-top:10px;">f</div>
                    <div style="float:left; margin-left:50px; width:90%">
                    
                   <?php 
                        $noneConnected = true;
                        if($post['facebook']){
                            foreach($post['facebook'] as $connected){
                                    if ($connected['active']=="Y"){
                                        $noneConnected = false;
                                        echo  $connected['title'].'<br>';
                                    
                                    }
                           
                            }  
                        }

                        if($noneConnected==true || $post['facebook'] == null) {
                            echo "<span style='color:red'>None Connected</span>";
                        }
                        
                            
                    ?>
                    <br />

    <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Manage Connections</button>

    <!-- Modal -->
    <div style='background-color:#000020' id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-style: blue";>Manage Facebook Connections</h4>
                </div>


                <div class="modal-body">
                    <table>
                    <tr>
                    <?php 
                        $i=0;
                        if($post['facebook']){
                        foreach($post['facebook'] as $connected){
                            $i++;
                            
                    ?>
                        <td>
                        <div> 
                            <?php echo  $connected['title']; ?>
                            
                        </div>
                        </td> 
                        <td>
    
                        <div class="onoffswitch">
                            <input type="checkbox" style="opacity:0; position:absolute; width: 62px; height: 17px; margin-top: 3px;" name='<?php echo $connected['id']; ?>' <?php if($connected['active']=='Y'){echo 'checked';}?>  class="onoffswitch-checkbox" id="myonoffswitch">
                            <label class="onoffswitch-label" for="myonoffswitch">
                                <span class="onoffswitch-inner"></span>
                            </label>
                        </div>
                        </td>
                        <td>
                        <div><a href="<?php echo base_url('design/delete_fb')."/".$connected['id']?>" class="btn">Delete</a></div>
                        </td>
                    <?php if($i==2){echo'</tr><tr>'; $i=0;} } }   ?>
                    </tr>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <?php echo form_submit('facebook_pair', "Save",' class="btn blue"');?>
                </div>
            </div>

        </div>
    </div>



    <?php echo form_submit('connect_facebook', "Add Connection",' class="btn btn-info btn-lg" style="width:130px"');?>

    </div>

    </div>
    <div class="row-fluid" style="float:left; min-height:100px;">
    <h4>Twitter</h4>
    <div class="tw ssfont twitter-new-cms twitter-active float-left" style="float:left; margin-top:10px;">t</div>
    <div style="float:left; margin-left:50px;width:90%">

                    <?php 
                        $noneConnected = true;
                        if($post['twitter']){
                            foreach($post['twitter'] as $connected){
                                    if ($connected['active']=="Y"){
                                        $noneConnected = false;
                                        echo  $connected['title'].'<br>';
                                    
                                    }
                           
                            }  
                        }

                        if($noneConnected==true || $post['twitter'] == null) {
                            echo "<span style='color:red'>None Connected</span>";
                        }
                        
                            
                    ?>
                    <br />

                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalone">Manage Connections</button>

                    <!-- Modal -->
            <div style="background-color:#000020" id="myModalone" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-style: blue";>Manage Twitter Connections</h4>
                        </div>


                        <div class="modal-body">
                            <table>
                            <tr>
                            <?php 
                                $i=0;
                                if($post['twitter']){
                                foreach($post['twitter'] as $connected){
                                    $i++;
                                    
                            ?>
                                <td>
                                <div> 
                                    <?php echo  $connected['title']; ?>
                                    
                                </div>
                                </td> 
                                <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" style="opacity:0; position:absolute; width: 62px; height: 17px; margin-top: 3px;" name='<?php echo $connected['id']; ?>' <?php if($connected['active']=='Y'){echo 'checked';}?>  class="onoffswitch-checkbox" id="myonoffswitch">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                        <span class="onoffswitch-inner"></span>
                                       
                                    </label>
                                </div>
                                </td>
                                <td>
                                <div><a href="<?php echo base_url('design/delete_fb')."/".$connected['id']?>" class="btn">Delete</a></div>
                                </td>
                            <?php if($i==2){echo'</tr><tr>'; $i=0;} } }   ?>
                            </tr>
                            </table>
                        </div>
                        
                        <div class="modal-footer">
                            <?php echo form_submit('twitter_pair', "Save",' class="btn blue"');?>
                        </div>
                    </div>

                </div>
            </div>
                
                   <?php echo form_submit('connect_twitter', "Add Connection",' class="btn btn-info btn-lg" style="width:130px" ');?>

                      </div>
                      </div>
                      <div class="row-fluid" style="float:left; min-height:100px; display:none;">
                      <h4>Tumblr</h4>
                      <div class="tb ssfont tumblr-new-cms tumblr-active float-left" style="float:left; margin-top:10px;">u</div>
                      <div style="float:left; margin-left:50px;width:90%">
                    <?php 
                        $noneConnected = true;
                        if($post['tumblr']){
                            foreach($post['tumblr'] as $connected){
                                    if ($connected['active']=="Y"){
                                        $noneConnected = false;
                                        echo  $connected['title'].'<br>';
                                    
                                    }
                           
                            }  
                        }

                        if($noneConnected==true || $post['tumblr'] == null) {
                            echo "<span style='color:red'>None Connected</span>";
                        }
                        
                            
                    ?>
                    <br />

                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalTwo">Manage Connections</button>

                    <!-- Modal -->
            <div style="background-color:#000020" id="myModalTwo" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-style: blue";>Manage Tumblr Connections</h4>
                        </div>


                        <div class="modal-body">
                            <table>
                            <tr>
                            <?php 
                                $i=0;
                                if($post['tumblr']){
                                foreach($post['tumblr'] as $connected){
                                    $i++;
                                    
                            ?>
                                <td>
                                <div> 
                                    <?php echo  $connected['title']; ?>
                                    
                                </div>
                                </td> 
                                <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" style="opacity:0; position:absolute; width: 62px; height: 17px; margin-top: 3px;" name='<?php echo $connected['id']; ?>' <?php if($connected['active']=='Y'){echo 'checked';}?>  class="onoffswitch-checkbox" id="myonoffswitch">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                        <span class="onoffswitch-inner"></span>
                                        
                                    </label>
                                </div>
                                </td>
                                <td>
                                <div><a href="<?php echo base_url('design/delete_fb')."/".$connected['id']?>" class="btn">Delete</a></div>
                                </td>
                            <?php if($i==2){echo'</tr><tr>'; $i=0;} } }   ?>
                            </tr>
                            </table>
                        </div>
                        
                        <div class="modal-footer" style="position:left; background-color:#000020">
                            <?php echo form_submit('tumblr_pair', "Save",' class="btn blue"');?>
                        </div>
                    </div>
                        
                </div>
            </div>
            <?php echo form_submit('connect_tumblr', "Add Connection",' class="btn btn-info btn-lg" style="width:130px"');?>

                    </div>
                    </div>
                    <h3>Design</h3>

                        
                        <div class="span12">
           
                            <?php
                                if(($post['header_image']!="") && (THEME_CLOUD."ef-logo.png"!="") ){
                                    $header_image = THEME_CLOUD."ef-logo.png?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $header_image = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                }
                            ?>
                           
                            <div id="header_img" class="hidden fileupload fileupload-new" data-provides="fileupload" style="margin-bottom:20px;">
                                <div class="fileupload-new thumbnail" style="width: 70%; height: 150px; background:#cecece;">
                                    <img src="<?php echo $header_image; ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 70%; max-height: 150px; line-height: 20px; background:none; border:none;"></div>
                                <div class="image_label_container">
                                    <span class="image_label" style="left: 150px;">Logo for title bar (480px x 90px) - <font style="color:red">Please Crop & PNG only</font></span>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image1" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>

                            
                              
                            <?php
                                if(($post['watermark']!="") && (THEME_CLOUD."watermark.png"!="") ){
                                    $watermark_image = THEME_CLOUD."watermark.png?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $watermark_image = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                }
                            ?>
                            <div class="hidden fileupload fileupload-new" data-provides="fileupload" style="margin-bottom:20px;">
                                <div class="fileupload-new thumbnail" style="width: 50%;  background:none; border:none;">
                                    <img src="<?php echo $watermark_image; ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50%;  line-height: 20px; background:none; border:none;"></div>
                                <div class="image_label_container">
                                    <span class="image_label" style="left: 150px;">Watermark - for photos (424px x 308) - <font style="color:red">PNG only</font></span>
                                   <span class="btn btn-file" style="background:#1f262d; height:50px; width:250px; color:#fff;"><span class="fileupload-new upload-bckgrd-img">+Upload backgrouund Image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image2" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>

                           

                             <h4>Background Image</h4>
                            
                               <?php
                                if(($post['background_image']!="") && (THEME_CLOUD."bg-sn.jpg.jpg"!="") ){
                                    $background_image = THEME_CLOUD."bg-sn.jpg?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $background_image = "";
                                }
                            ?>
                            <input type="hidden" id="photo3_width" name="photo3_width" val="" />
                            <input type="hidden" id="photo3_height" name="photo3_height" val="" />
                            <input type="hidden" id="photo3_crop_x1" name="photo3_crop_x1" val="" />
                            <input type="hidden" id="photo3_crop_y1" name="photo3_crop_y1" val="" />
                            <input type="hidden" id="photo3_crop_x2" name="photo3_crop_x2" val="" />
                            <input type="hidden" id="photo3_crop_y2" name="photo3_crop_y2" val="" />
                            <input type="hidden" id="photo3_crop_w" name="photo3_crop_w" val="" />
                            <input type="hidden" id="photo3_crop_h" name="photo3_crop_h" val="" />
                            <input type="hidden" id="image3" name="image3" value="">

                            <div id="background_img" class="fileupload fileupload-new" data-provides="fileupload" style="float:left; width:60%;">
                            <div class="gray fileupload-new thumbnail" style="width: 100%; height:100px; background:#cecece;">
                             <img src="<?php echo $background_image; ?>" alt="" />
                              </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 100%; max-height: 150px; line-height: 20px; background:none; border:none;"></div>
                           
                             <div class="image_label_container">
                            
                                    <? /* Make classes for styles */ ?>
                                  
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   
                                   <input type="file" name="image3" class="default"/></span>
                                  <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
         
                                 </div>
                               
                                
                       </div>
                        </div>
                          <div class="span12">
                            
                             <h4>Channel Icon</h4>

                             <?php
                                if(($post['chanel_icon']!="") && (THEME_CLOUD."chanel.jpg"!="") ){
                                    $chanel_icon = THEME_CLOUD."chanel.jpg?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $chanel_icon = "";
                                }
                            ?>
                            <input type="hidden" id="photo_width" name="photo_width" val="" />
                            <input type="hidden" id="photo_height" name="photo_height" val="" />
                            <input type="hidden" id="photo_crop_x1" name="photo_crop_x1" val="" />
                            <input type="hidden" id="photo_crop_y1" name="photo_crop_y1" val="" />
                            <input type="hidden" id="photo_crop_x2" name="photo_crop_x2" val="" />
                            <input type="hidden" id="photo_crop_y2" name="photo_crop_y2" val="" />
                            <input type="hidden" id="photo_crop_w" name="photo_crop_w" val="" />
                            <input type="hidden" id="photo_crop_h" name="photo_crop_h" val="" />
                            <input type="hidden" id="image7" name="image7" value="">

                            <div id="chanel_img" class="fileupload fileupload-new" data-provides="fileupload" style="float:left; width:40%;" >
                            <div class="gray fileupload-new thumbnail" style="width: 220px; height:160px; background:#cecece;">
                             <img src="<?php echo $chanel_icon; ?>" alt=""/>
                             </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" data-provides="fileupload" style="width: 220px; line-height: 20px; background:none; border:none;"></div>

                             <div class="image_label_container">
                            
                                    <? /* Make classes for styles */ ?>
                                
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image7" class="default"/></span>
                                  <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
         
                                 </div>
                               
                                
                         </div>
                        </div>
                            <?php
                                if(($post['logo_image']!="") && (THEME_CLOUD."logo.png"!="") ){
                                    $logo_image = THEME_CLOUD."logo.png?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $logo_image = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                }
                            ?>
                            
                            <div id="logo_img" class="hidden fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 100%; height: 150px;">
                                    <img src="<?php echo $logo_image; ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 100%; max-height: 150px; line-height: 20px; background:none; border:none;"></div>
                                <div class="image_label_container">
                                    <span class="image_label">Logo Image(130px x 75px) - <font style="color:red">Please Crop & PNG only</font></span>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image4" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                            <?php
                                if(($post['banner_image']!="") && (THEME_CLOUD."banner.png"!="") ){
                                    $banner_image = THEME_CLOUD."banner.png?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $banner_image = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                }
                            ?>
                            
                            <div id="banner_img" class="hidden fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 100%; height: 150px;">
                                    <img src="<?php echo $banner_image; ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 100%; max-height: 150px; line-height: 20px;, background:none; border:none;"></div>
                                <div class="image_label_container">
                                    <span class="image_label">Banner Image(130px x 75px)</span>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image5" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>

                             
                            <?php
                                if(($post['brightcove']!="") && (THEME_CLOUD."brightcove.png"!="") ){
                                    $brightcove_image = THEME_CLOUD."brightcove.png?v=".md5('2').rand();
//                                    echo $header_image;exit;
                                }else{
                                    $brightcove_image = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                }
                            ?>
                            <div class="hidden fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 100%; height: 150px;">
                                    <img src="<?php echo $brightcove_image; ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 100%; max-height: 150px; line-height: 20px; background:none; border:none;"></div>
                                <div class="image_label_container">
                                    <span class="image_label">Brightcove (424px x 308) - <font style="color:red">PNG only</font></span>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image6" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="bio_link">Breaking Muscle Bio Link</label>
                                <div class="controls">
                                <?php  echo form_input("bio_link",$post['bio_link'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="site_link">Web Site Link</label>
                                <div class="controls">
                                <?php  echo form_input("site_link",$post['site_link'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="twitter_link">Twitter Link</label>
                                <div class="controls">
                                <?php  echo form_input("twitter_link",$post['twitter_link'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="facebook_link">Facebook Link</label>
                                <div class="controls">
                                <?php  echo form_input("facebook_link",$post['facebook_link'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="instagram_link">Instagram Link</label>
                                <div class="controls">
                                <?php  echo form_input("instagram_link",$post['instagram_link'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="channel_description">Channel Description</label>
                                <div class="controls">
                                <?php  echo form_input("channel_description",$post['channel_description'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Type</label>
                                <div class="controls">
                                <?php  echo form_input("channel_type",$post['channel_type'],'class="large m-wrap"');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                            
                    <div class="form-actions" style="border-top:0px;">
                        
                        <?php echo form_submit('del_header', "Delete header",' class="btn blue hidden"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('del_watermark', "Delete watermark",' class="btn blue hidden"');?>
<!--                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>-->
                               <?// if($background_image!=""){ ?>
                               <?php //echo form_submit('del_background', "Remove",' class="btn blue" style="background:none; border:none; box-shadow:none;"');?>
                               <?// } ?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('del_banner', "Delete banner",' class="btn blue hidden"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('del_logo', "Delete logo",' class="btn blue hidden"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('del_brightcove', "Delete brightcove logo",' class="btn blue hidden"');?>
                         <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('del_chanel', "Delete channel icon",' class="btn blue hidden"');?>
                    </div>
                   
                   <?php echo form_submit('submit', "Update",' class="btn blue" style="float:left;clear:both; margin-bottom:50px;"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <?php echo form_close();?>
                    <!-- END FORM-->
</div>
 <script language="javascript">
                        $('div#background_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');
                            if(img.length > 0) {
                                img.load(function(e) {
                                    var w = 1900;
                                    var h = 320;
                                    $('#photo3_width').val(w);
                                    $('#photo3_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    img.Jcrop({
                                        onSelect: showCoords3,
                                        onChange: showCoords3,
                                        aspectRatio : 16 / 3,
                                        maxSize : [2000, 2000]
                                    });
                                });
                            }
                        });
                        function showCoords3(c) {
                            $('#photo3_crop_x1').val(c.x);
                            $('#photo3_crop_y1').val(c.y);
                            $('#photo3_crop_x2').val(c.x2);
                            $('#photo3_crop_y2').val(c.y2);
                            $('#photo3_crop_w').val(c.w);
                            $('#photo3_crop_h').val(c.h);

                       
                        }

                         $('div#chanel_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');
                            if(img.length > 0) {
                                img.load(function(e) {
                                    var w = 212;
                                    var h = 160;
                                    $('#photo_width').val(w);
                                    $('#photo_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    img.Jcrop({
                                        onSelect: showCoords,
                                        onChange: showCoords,
                                        aspectRatio : 1,
                                        maxSize : [1000, 1000]
                                    });
                                });
                            }
                        });
                        function showCoords(c) {
                            $('#photo_crop_x1').val(c.x);
                            $('#photo_crop_y1').val(c.y);
                            $('#photo_crop_x2').val(c.x2);
                            $('#photo_crop_y2').val(c.y2);
                            $('#photo_crop_w').val(c.w);
                            $('#photo_crop_h').val(c.h);
                            }
                          
                    </script>
                  

                    <!-- <script language="javascript">
                        $(document).ready(function() {
                            $('div#header_img.fileupload[data-provides=fileupload] div.fileupload-preview').trigger('contentchange');
                           
                        });
                        $('div#header_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');

                            

                            if(img.length > 0) {
                                img.load(function(e) {

                                    var w = 580;
                                    var h = img[0].clientHeight* w/img[0].clientWidth;
                                    $('#photo_width').val(w);
                                    $('#photo_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    
                                     img.Jcrop({
                                        onSelect: showCoords,
                                        onChange: showCoords,
                                        aspectRatio : 16 / 3,
                                        maxSize : [1000, 1000]
                                    });
                                });
                            }
                        });


                       $('div#background_img.fileupload[data-provides=fileupload] div.fileupload-preview').trigger('contentchange');
                           
                        });
                        $('div#background_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');

                            

                            if(img.length > 0) {
                                img.load(function(e) {

                                    var w = 1900;
                                    var h = img[0].clientHeight* w/img[0].clientWidth;
                                    $('#photo_width').val(w);
                                    $('#photo_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    
                                   img.Jcrop({
                                        onSelect: showCoords,
                                        onChange: showCoords,
                                        aspectRatio : 2 / 3,
                                        maxSize : [1900, 1000]
                                    });
                                });
                            }
                        });

                         $(document).ready(function() {
                            $('div#chanel_img.fileupload[data-provides=fileupload] div.fileupload-preview').trigger('contentchange');
                           
                        });
                        $('div#chanel_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');

                            

                            if(img.length > 0) {
                                img.load(function(e) {

                                    var w = 212;
                                    var h = img[0].clientHeight* w/img[0].clientWidth;
                                    $('#photo2_width').val(w);
                                    $('#photo2_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    
                                    img.Jcrop({
                                        onSelect: showCoords2,
                                        onChange: showCoords2,
                                        aspectRatio : 16 / 9,
                                        maxSize : [1000, 1000]
                                    });
                                });
                            }
                        });


                        $(document).ready(function() {
                            $('div#logo_img.fileupload[data-provides=fileupload] div.fileupload-preview').trigger('contentchange');
                           
                        });
                        $('div#logo_img.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
                            var img = $(e.target).find('img');

                            

                            if(img.length > 0) {
                                img.load(function(e) {

                                    var w = 220;
                                    var h = img[0].clientHeight* w/img[0].clientWidth;
                                    $('#photo2_width').val(w);
                                    $('#photo2_height').val(h);
                                    img.attr('width', w + 'px');
                                    img.attr('height', h + 'px');
                                    
                                    img.Jcrop({
                                        onSelect: showCoords2,
                                        onChange: showCoords2,
                                        aspectRatio : 16 / 9,
                                        maxSize : [1000, 1000]
                                    });
                                });
                            }
                        });



                        

                        

                        function showCoords(c) {
                            $('#photo_crop_x1').val(c.x);
                            $('#photo_crop_y1').val(c.y);
                            $('#photo_crop_x2').val(c.x2);
                            $('#photo_crop_y2').val(c.y2);
                            $('#photo_crop_w').val(c.w);
                            $('#photo_crop_h').val(c.h);
                        }

                        function showCoords2(c) {
                            $('#photo2_crop_x1').val(c.x);
                            $('#photo2_crop_y1').val(c.y);
                            $('#photo2_crop_x2').val(c.x2);
                            $('#photo2_crop_y2').val(c.y2);
                            $('#photo2_crop_w').val(c.w);
                            $('#photo2_crop_h').val(c.h);
                        }
                    </script> -->
                    <style>
                    .jcrop-holder {
                        background-color: #CECECE !important;
                    }
                    </style>
                 </div>
                 </div>
              <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
</div>
<link href="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<script src="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.pack.js" ></script>
<script src="<?php echo ASSETS_DIR; ?>/jscolor/jscolor.js" ></script>
<script src="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.js"></script>
<script src="<?php echo ASSETS_DIR; ?>/jquery-contentchange-event/jquery.contentchange-event.js"></script>
<link href="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.css" rel="stylesheet" />

