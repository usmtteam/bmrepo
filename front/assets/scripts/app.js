var enriqueApp = angular.module('enriqueApp', ['ngRoute', 'infinite-scroll', 'ui.bootstrap', 'ngCookies', 'angularFileUpload', 'NgSwitchery','angularytics'])
.config(function($routeProvider, $locationProvider,AngularyticsProvider) {
	 AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);	
	$routeProvider
	.when('/drafts', {
		templateUrl: 'views/drafts.html',
		controller: 'draftsController'
	})
	.when('/community', {
		templateUrl: 'views/community.html',
		controller: 'communityController'
	})
	.when('/photos', {
		templateUrl: 'views/photos.html',
		controller: 'photosController'
	})
	.when('/videos', {
		templateUrl: 'views/videos.html',
		controller: 'videosController'
	})
	.when('/tourdates', {
		templateUrl: 'views/tourdates.html',
		controller: 'tourDatesController'
	})
	.when('/polls_quizzes', {
		templateUrl: 'views/pollsandquizzes.html',
		controller: 'pollsAndQuizzesController'
	})
	.when('/shop', {
		templateUrl: 'views/shop.html',
		controller: 'shopController'
	})
	.when('/ranking', {
		templateUrl: 'views/ranking.html',
		controller: 'rankingController'
	})
	.when('/viewprofile/:user_id', {
		templateUrl: 'views/viewprofile.html',
		controller: 'viewProfileController'
	})
	.when('/settings', {
		templateUrl: 'views/settings.html',
		controller: 'settingsController'
	})
	.when('/inbox', {
		templateUrl: 'views/inbox.html',
		controller: 'inboxController'
	})
	.when('/help', {
		templateUrl: 'views/help.html',
		controller: 'helpController'
	})
	.when('/homefeed/:post_type/:post_id', {
		templateUrl: 'views/mainfeed.html',
		controller: 'mainFeedController'
	})
	.otherwise( {
		templateUrl: 'views/mainfeed.html',
		controller: 'mainFeedController'
	});
 
	// configure html5 to get links working on jsfiddle
	//$locationProvider.html5Mode(true);
        
        }).run(function(Angularytics) {
    Angularytics.init();
	
});