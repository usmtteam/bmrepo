<?php

//    require(__DIR__.'/../../config/init.php');
require(__DIR__.'/../../config/settings.php');
require(__DIR__.'/init.php');
require(__DIR__.'/db_home.php');
include '_head.php';
include '_menu.php' ;

// var_dump($l_xOfloCategoryByName[1]); exit;
?>



<div class="box-menu fixed-width">
    <!-- make this dynamic so it will show up  -->



        <h1 class="catsTitle">
            <span><?php
                $cat = $_GET['cat'];
            if (!$cat) { echo "Latest Videos";}else{echo $cat;} ?></span></h1>
  		<br>

     
         <!-- row of thumnails 3x4 -->
        <div class="">

       <div class="col-md-12 col-sm-12 col-xs-12 cats-menu">
              <div>
                    <?php

                    $total_count = count($l_xOfloCategoryByName);
  
                  for ($i = ($page * 16); $i < $total_count && $i < ($page + 1) * 16; $i++) : ?>
                      <div class="item <?php if ($i == 0) { echo ' active'; } ?>">

                        <a style="margin-bottom: 20px;" class="img-responsive col-md-3 col-sm-12" href="http://<?=$l_xOfloCategoryByName[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloCategoryByName[$i]['content_type'].'/'.$l_xOfloCategoryByName[$i]['id'] ?>/">
                            
                            <div <?php if ($l_xOfloCategoryByName[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloCategoryByName[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='cat-pics-wrap-embeded'";}else{echo "class='cat-pics-wrap'";} ?>>
                            <img class="backup_picture <?php if ($l_xOfloCategoryByName[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloCategoryByName[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'col-md-12 cat-page-pics-embeded';}else{echo 'col-md-12 cat-page-pics';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloCategoryByName[$i]['url'].'/assets/ufile/thumb/'; echo $l_xOfloCategoryByName[$i]['image_path'];?>" alt="Image">
                            </div>
                            
                            <div class="cat-pics-description">
                            <p><strong><?=substr($l_xOfloCategoryByName[$i]['description'],0,70);?><? $strlen =strlen($l_xOfloCategoryByName[$i]['description']); if($strlen>70){echo "...";} ?></strong><br>  <span>Posted By <span class="by_channel"><?php echo $l_xOfloCategoryByName[$i]['url'] ?></span>

                            <br>

                            <?php 
                              foreach($categoryViews as $views){
                                if($views['content_id'] == $l_xOfloCategoryByName[$i]['id']){
                            echo number_format($views['total_views']).' Views<span class="dot_between"></span> '.Utils::time_passed($l_xOfloCategoryByName[$i]['feed_date']+25200);}}?></span></p>
                            </div>
                        </a>

                       </div>
                  <?php endfor; ?>
                  
                </div>

            </div>



        <div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="pager">
            <div class="span">
                  <div class="pager">Showing <?php echo (($page_count*($page))+1); ?> to <?php if($total_count<($page_count*($page+1))){ echo $total_count; }else{ echo ($page_count*($page+1)); } ?> of <?php echo $total_count; ?> entries</div>
            </div>
          
       
             <ul>
                            <li class="prev<?php echo $page == 0 ? ' disabled' : ''?>">
                                <a data-page="<?php echo max(0, $page - 1);?>" style="<?php echo $page == 0 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">Previous</a>
                            </li>
                            <!-- first page -->
                            <?php if ($page > 2) : ?>
                                <li class="">
                                    <a data-page="<?php echo 0;?>" style="cursor: pointer;"><?php echo 1; ?></a>
                                </li>
                            <?php endif; ?>

                             <?php if ($page >= 4) : ?>
                                <li class="">
                                    <a data-page="" src="" style="pointer-events: none; cursor: none;">...</a>
                                </li>
                            <?php endif; ?>
                          
                            <!-- pages inbetween -->
                            <?php for($i = $first_page; $i < $last_page; $i++) :?>
                                <li class="<?php echo $page == $i ? ' active' : ''; ?>">
                                    <a data-page="<?php echo $i;?>" style="cursor: pointer;"><?php echo $i+1; ?></a>
                                </li>
                            <?php endfor; ?>

                            <!-- ellipsis -->
                            <?php if ($last_page <= $total_page_count - 2) : ?>
                                <li class="">
                                    <a data-page="" style="pointer-events: none; cursor: pointer;">...</a>
                                </li>
                            <?php endif; ?>

                            <!-- last page -->
                            <?php if ($last_page <= $total_page_count - 1) : ?>
                                <li class="">
                                    <a data-page="<?php echo $total_page_count - 1;?>" style="cursor: pointer;"><?php echo $total_page_count; ?></a>
                                </li>
                            <?php endif; ?>
    
                 
                            <li class="<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>">
                                <a data-page="<?php echo min($total_page_count-1, $page + 1 ); ?>" style="<?php echo $page == $total_page_count-1 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">Next </a>
                            </li>
                        </ul>
                     

              </div>

    <input type="hidden" value="<?php echo $cat_base_url; ?>" id='base_url'>

       <script language="javascript">
                     $(document).ready(function() {
                   
                    + $('.pager a').click(function(e) {
                             var base_url = jQuery('#base_url').val();
                          document.location.href = base_url + "&page=" + $(e.target).attr('data-page');
                         });

                    });
                </script>
       
</div>
<?php include '_home_footer.php' ?>
</div>


    <script type="text/javascript">

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= $oflo_global_ga_key ?>','breakingmuscle.com/category');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
        ga('set', 'page', '<?= $_SERVER['REQUEST_URI'] ?>');
    </script>

    <?php include "_footer.php" ?>
