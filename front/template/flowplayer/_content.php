
<div id="fb-root"></div>
<script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '1440671026187879',
                    status: true,
                    xfbml: true
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

               function statusChangeCallback(response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      _i();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }  
</script>

<!-- DIV CONTAINER STARTS HERE-->
<div id="container" ng-app="">
  <div id="jqSmartBanner"></div>
  <header>
  </header>  

   
  <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
    <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
      <ul class="cd-switcher">
        <li><a href="#0">Sign in</a></li>
        <li><a href="#0">New account</a></li>
      </ul>

      <div id="cd-login"> <!-- log in form -->
      <button id="button_signin_with_facebook" onclick="_login();">
Sign in with
<b>Facebook</b>
</button>

        <form class="cd-form">
          <p class="fieldset">
            <label class="image-replace cd-email" for="signin-email">E-mail</label>
            <input class="full-width has-padding has-border" id="text_email" type="email" placeholder="E-mail">
            <span class="cd-error-message">Please enter a valid email</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-password" for="signin-password">Password</label>
            <input class="full-width has-padding has-border" id="text_password" type="text"  placeholder="Password">
            <a href="#0" class="hide-password">Hide</a>
            <span class="cd-error-message">Please enter a valid password</span>
          </p>

          <p class="fieldset">
            <input type="checkbox" id="remember-me" checked>
            <label for="remember-me">Remember me</label>
          </p>

          <p class="fieldset">
            <input class="full-width  black" type="submit" value="Login" onclick="login_email();">
          </p>
        </form>
        
        
        <!-- <a href="#0" class="cd-close-form">Close</a> -->
      </div> <!-- cd-login -->

      <div id="cd-signup"> <!-- sign up form -->
        <form class="cd-form">
          <p class="fieldset">
            <label class="image-replace cd-username" for="signup-username">Username</label>
            <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username">
            <span class="cd-error-message">Please enter a valid username</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-username" for="signup-username">First Name</label>
            <input class="full-width has-padding has-border" id="signup-firstname" type="text" placeholder="First Name">
            <span class="cd-error-message">Please enter a valid first name</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-username" for="signup-username">Last Nname</label>
            <input class="full-width has-padding has-border" id="signup-lastname" type="text" placeholder="Last Name">
            <span class="cd-error-message">Please enter a valid last name</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-email" for="signup-email">E-mail</label>
            <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">
            <span class="cd-error-message">Please enter a valid email</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-password" for="signup-password">Password</label>
            <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
            <a href="#0" class="hide-password">Hide</a>
            <span class="cd-error-message">Please enter a valid password</span>
          </p>

          <p class="fieldset">
            <input type="checkbox" id="accept-terms">
            <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
          </p>

          <p class="fieldset">
           <input class="full-width has-padding black" type="submit" value="Create account" onclick="signup_email();">
          </p>
        </form>

        <!-- <a href="#0" class="cd-close-form">Close</a> -->
      </div> <!-- cd-signup -->
      <a href="#0" class="cd-close-form">Close</a>
    </div> <!-- cd-user-modal-container -->
  </div> <!-- cd-user-modal -->
  <div class="top_video">
    <div class="container">
  <div class="dropdown icon-menu3">
      <a href="#" class="my-btn-default dropdown-toggle" type="button"  data-toggle="dropdown">&nbsp;</a>
          <ul class="my-dropdown-menu" role="menu">
          <li><a href="/content/photo/recent/">PHOTOS</a></li>
          <li><a href="/content/video/recent/">VIDEOS</a></li>
          <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
          
          <? } ?>  
          <li class="bottom_content hidden"><p>Unlock <strong>live chat, exclusive content, text messages, phone calls, forums & more.</strong></p><img width="198" src="<?= STATIC_FILES_HOST ?>/images/vip.png"><img width="95" src="<?= STATIC_FILES_HOST ?>/images/app_itune.png">&nbsp;<img width="95" src="<?= STATIC_FILES_HOST ?>/images/app_google_play.png"></li>        
          </ul>

      </div>
     
  <div id="title_background">
 
    <a href="http://starclubltd.com" target="_blank"><div id="logo_star"></div></a>

      
      <div class="top_title"><? if($backgroundImage==""){  echo $l_xChannel->site_header_title; } ?></div>
      
       
          <div class="login_top <? if(isset($_COOKIE['current_user'])){ echo "hide"; } ?>"><nav class="main-nav">
      <ul>
        <!-- inser more links here -->
        <li><a class="cd-signin" href="#0">Log in</a></li></ul></nav></div>
     
          <div class="login_top_out <? if(isset($_COOKIE['current_user'])){ echo "show"; } ?> hide"><a class="cd-signin" onclick="_logout();" href="#">Log out</a></div>
      
      <div class="clear"></div>
    </div>
    <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
    <div id="title_background_mobile">
    </div>
    <? } ?>
  <?
    if(SHOW_BANNER_ADS){ 
        //<!-- 728x90 --> 
        ?>
        <div id="feedTag2" ng-hide="false" align="center" style="margin-left:0px;"><?= $l_xChannel->topAdUnit; ?></div>
        <? } 

    ?>
    
    
  
  
      <div data-content-id="" class="post_frame">
        <div class="post_details"> 

          <!-- Start of Brightcove Player -->
          <div style="display:none"> </div>
          
          <? if($l_bIsVideo) { ?>
              <div class=" ng-scope_video">
                <? /* 
                <object id="myExperience<?= $l_xAssetResult['brightcove_media_id']; ?>" class="BrightcoveExperience">
                  <param name="bgcolor" value="#FFFFFF" />
                  <?php if($l_bIsIE10){ ?>
                  <param name="width" value="640" />
                  <?php }else{ ?>
                  <param name="width" value="100%" />
                  <?php } ?>
                  <!--<![endif]-->
                  <param name="height" value="360" />
                  <param name="playerID" value="<?php echo $l_xChannel->bc_playerid_web; ?>" />
                  <param name="playerKey" value="<?php echo $l_xChannel->brightcove_key; ?>" />
                  <param name="isVid" value="true" />
                  <param name="isUI" value="true" />
                  <param name="adServerURL" value="<?php echo $l_xChannel->adURL ?>" />
                  <param name="contentURL" value="<?= $l_sCanonicalLink ?>" />
                  <param name="dynamicStreaming" value="true" />
                  <param name="htmlFallback" value="true" />
                  <param name="linkBaseURL" value="<?= $l_sCanonicalLink ?>" />
                  <param name="@videoPlayer" value="<?= $l_xAssetResult['brightcove_media_id'] ?>" />
                  <param name="viralPlayerID" value="<?= $l_xChannel->bc_playerid_web; ?>" />
                  <param name="viralPlayerWidth" value="480" />
                  <param name="viralPlayerHeight" value="270" />
                  <param name="wmode" value="transparent">
                </object> 
                <? */ ?>
                <div id="myPlayer" class="flowplayer is-ready minimalist is-mouseout" data-analytics="<?= $l_xChannel->google_analytics_key ?>" data-logo="//d3hl6zmfu9utfu.cloudfront.net/assets/images/star.png">
                <div class="fp-engine"></div>

                

                </div> 
              </div>
          <?
           } else if(!$l_bIsVideo){ ?>
          
                <div id="gallery_back">
                    <a class="back_button <?= ($l_xPreviousPhotoResult) ? 'full' : 'inactive' ?>" href="/flowplayer/photo/<?= $l_xPreviousPhotoResult['id'] ?>/">Back</a>
                    <img style=" margin:0 auto; max-height:360px; max-width:360px; vertical-align:middle; text-align:center;" src="http://d3hl6zmfu9utfu.cloudfront.net/<?php echo $l_xChannel->slug;?>/assets/ufile/watermark/<?php echo $l_xAssetResult['destination'] ?>" /> 
                    <a class="next_button <?= ($l_xNextPhotoResult) ? 'full' : 'inactive' ?>" href="/flowplayer/photo/<?= $l_xNextPhotoResult['id'] ?>/">Next</a></div>
          <? } ?>
          <ul class="list_view desktop">
            <div id="scrollbar1">
              <div class="scrollbar">
                <div class="track">
                  <div class="thumb">
                    <div class="end"></div>
                  </div>
                </div>
              </div>
              <div class="viewport">
                <div class="overview">
                  <?
                    if($l_bIsVideo) {
                        AppController::getInstance()->outputVideoListItemsFlowPlayer($l_aFPVideos,$l_xChannel); 
                    }else{
                   
                    foreach($l_aAllFoundAssets as $photo_list_result){
                        ?>
                        <a href="/flowplayer/photo/<?= $photo_list_result['id'] ?>/">
                        <li><span class="thumb_video_list"><div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?php echo $l_xChannel->slug;?>/assets/ufile/watermark/<?= $photo_list_result['destination']; ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div></span>
                            <p><strong><?= substr($photo_list_result['description'], 0, 45) . '...'; ?> </strong>
                            <p style="color:#666"><? echo Utils::time_passed($photo_list_result['time_stamp']); ?></p>
                            <?php /*<span><?= Utils::get_time_ago($photo->publishedDate); ?></span></p> */ ?>
                            <div class="clear"></div>
                            </li>
                        </a>
                        <? 
                        
                        }
                    } ?>
                </div>
              </div>
            </div>
          </ul>
          <div class="clear"></div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>




  <div class="layout video_view">
    <section>
      <div ng-view id="page" class="maincontent"> 
        <div  class="ng-scope">
          <ul class="feed_list ng-scope">
            <li class="feed_item">
              <div class="post_comments_wrapper">
              <div class="post_comments post_comments_web">
                <? if($l_nDeviceType != DEVICE_TYPE_MOBILE){ ?>
                  <div class="message"></div>
                <? } ?>

                <a href="/#/community"><div id="landing_page_banner"></div></a>

                <div class="post_description">
                  <?= $l_xAssetResult['description'] ?>
                  <p class="small_date"><? echo Utils::time_passed($l_xAssetResult['time_stamp']); ?></p>
                </div>

                <div class="post_like_count float-right" id = "post_like_count">
                  <?= $l_nTotalLikes ?>
                </div>
                
                <div id="addlike" class="a-do-add-like <?= (($l_nUserTotalLikes>0) ? 'icon-star-full' : 'icon-star-empty' ) ?> float-right ng-scope button_star<?= (($l_nUserTotalLikes>0) ? '1' : '' ) ?>" ></div>


                <a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;">
                  <div class="icon-facebook3 facebook-new float-left" ></div>
                </a>
                <a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;">
                  <div class="icon-twitter3 twitter-new float-left" ></div>
                </a>
                <div class="clear"></div>
                </div>
                <div class="post_comments post_comments_web down">
                <div class="time_comments">
                  <form id="comment_form">
                    <input name="userid" class="finput-area a-check-login comment-input-obj" type="text" id="user_id" placeholder="Enter your comment" />
                    <input type="button" value ="SEND" id="form_submit" class="input_submit"/>
                    <div class="clearfix"></div>
                  </form>
                
                </div>
              

                <div class="clear"></div>
                <ul class="post_comments">
                    <? AppController::getInstance()->outputComments($l_aComments);?>
                </ul>
                 <button class="show_comments"> Show More...</button> 
                </div>

                <div class="clear"></div>  
                </div>


            <? if(SHOW_BANNER_ADS){ ?>
              <div class="advertising_space"> 
                <?  
                    //<!-- 300x250 --> 
                    echo $l_xChannel->rightTopAdUnit;
                    echo $l_xChannel->rightBottomAdUnit;
                ?> 
                </div>
              <? } ?>
              <div class="clear"></div>
            </li>
          </ul>
        </div>


        




        
        <?php /* Mobile */ ?>
        <div class="animated-slide-2">
          
            
            <div class="tab-content">
              
              <div id="tab1">
                <ul class="feed_list ng-scope">
                  <li class="feed_item">
                    <div class="comments_mobile">
                      <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                      <div class="message"></div>
                      <? } ?>
                      <div style="background:#fff; text-align:center; margin:20px 0 10px 0; width:100%; overflow:hidden"> <?= $l_xChannel->mobileAdUnit; ?> </div>
                      <a href="/#/community"><div id="landing_page_banner"></div></a>

                      <div id="likes_wrapper">
                        <div class="post_description">
                          <?= $l_xAssetResult['description'] ?>
                          <p class="small_date"><? echo Utils::time_passed($l_xAssetResult['time_stamp']); ?></p>
                          <div style="clear:both; height:10px; float:left; width:100%;" ></div>
                        </div>
                        <div class="post_like_count float-right" id="post_like_count">
                          <?= $l_nTotalLikes ?>
                        </div>

                        <div id="addlike" class="a-do-add-like <?= (($l_nUserTotalLikes>0) ? 'icon-star-full' : 'icon-star-empty' ) ?>  float-right ng-scope button_star<?= (($l_nUserTotalLikes>0) ? '1' : '' ) ?>"></div>


                        <a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;">
                  <div class="icon-facebook3 facebook-new float-left" ></div>
                </a>
                <a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;">
                  <div class="icon-twitter3 twitter-new float-left" ></div>
                </a>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div id="tab2">
                <ul class="list_view mlv">
                    <?
                    if($l_bIsVideo) {
                        AppController::getInstance()->outputVideoListItemsFlowPlayer($l_aFPVideos,$l_xChannel); 
                    }else{
                    $k=0;
                    foreach($l_aAllFoundAssets as $photo_list_result){
                        ?>
                        <a class="<? if($k<5){ echo 'active';} ?>" href="/flowplayer/photo/<?= $photo_list_result['id'] ?>/">
                        <li><span class="thumb_video_list"><div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?php echo $l_xChannel->slug;?>/assets/ufile/watermark/<?= $photo_list_result['destination']; ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div></span>
                           
                            <p><strong><?= substr($photo_list_result['description'], 0, 45) . '...'; ?> </strong>
                            <p style="color:#000"><? echo Utils::time_passed($photo_list_result['time_stamp']); ?></p>
                            <div class="clear"></div>
                            </li>
                        </a>
                        <? 
                        $k++;
                        }
                    } ?>
                </ul>
                <button class="show_thumbs"> Show More...</button> 

              </div>
              <style>
                  ul.mlv a, ul.post_comments li, ul.mobile_comments li{
                      display:none;
                  }
                  ul.mlv a.active, ul.post_comments li.active, ul.mobile_comments li.active{
                      display:block;
                  }
                  ul.post_comments li.mci.active a, ul.post_comments li.active a:hover, ul.post_comments li.active a { background:none;}

              </style>
              <div style="background:#fff; text-align:center; margin:0px 0 0px 0; width:100%;height:50px;"> <?= $l_xChannel->mobileSecondAdUnit; ?></div>
              <div id="tab3">
                <ul class="feed_list ng-scope">
                  <li class="feed_item">
                    <div class="comments_mobile">
                      <div class="time_comments">
                        <form id="comment_form">
                          <input name="userid" type="text" id="userid_mobile" class="comment-input-obj a-check-login" placeholder="Enter your comment" />
                          <input type="button" value ="SEND" id="form_submit_mobile" class="input_submit"/>
                          <div class="clearfix"></div>
                        </form>
                      </div>
                    </div>
                    <ul class="post_comments mobile_comments">
                    <div class="mobile_comments_list"></div>
                      <? AppController::getInstance()->outputComments($l_aComments); ?>
                      </ul>
                    <button class="show_mobile_comments"> Show More...</button> 
                  </li>
                </ul>
                <div class="clearfix"></div>
                <div style="background:#fff; text-align:center; margin:0px auto; width:300px;height:100px; margin-left:0px;"> <?= $l_xChannel->mobileLargeAdUnit; ?></div>
              </div>
            </div>
        
        </div>
      </div>
    </section>
  </div>
</div>

<script>
              $(".show_mobile_comments").click(function() { 
  $("ul.mobile_comments li:not(.active):lt(5)").addClass('active'); 
});

              $(".show_comments").click(function() { 
  $("ul.post_comments li:not(.active):lt(5)").addClass('active'); 
});

              $(".show_thumbs").click(function() { 
  $("ul.mlv a:not(.active):lt(5)").addClass('active'); 
});
              
              </script>




