enriqueApp.controller('videosController', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
	$scope.page = 0;
    $scope.feeds = [];
    $scope.star = {};
    $scope.filter = 'All';
    $scope.filterList = [];
    $scope.show_comment_box = false;
    $scope.is_loading = false;
    
    $scope.loadData = function () {
    	if($scope.is_loading) return;
    	CommonLib.show_loading_bubble();
    	$scope.is_loading= true;
    	var url = $rootScope.get_api_url() + '&option=get_videos&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&count=5&page=' + $scope.page;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {

        	$rootScope.updateBrightCoveInfo(data);
        	CommonLib.hide_loading_bubble();
        	if(data.status == true) {
        		for(var i =0 ; i < data.videos.length; i++) {
        			data.videos[i].content_id = data.videos[i].id;
        			data.videos[i].post_type = 'video';
        			data.videos[i].video_url = data.videos[i].destination;
        		}
        		
        		$scope.star = data.star;
        		$scope.feeds = $scope.feeds.concat( data.videos );
        		$scope.filterList = data.filters;
        		if(data.videos.length > 0) $scope.page ++;
        	}
        	else {
        		$rootScope.showConfirmDialog({
    				type: 'ALERT',
    				title: 'Videos',
    				description: 'You need to login to see videos',
    				onOk : function() {
    					document.location = $rootScope.WEBSITE_URL;
    				}
    			});
        	}
        	$scope.is_loading = false;
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        	$scope.is_loading = false;
        });
    };
    //$scope.loadData();
    
	$scope.onChangeFilter = function(e) {
		var target = $(e.target);
		$scope.filter = target.attr('data-filter');
	}
}]);