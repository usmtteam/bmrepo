<?php

require_once 'vendor/autoload.php';

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\AmountDetails;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\ExecutePayment;

class Paypal {
	public $apiContext = null;
	function __construct() {
		$authCredential = new OAuthTokenCredential(
				PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET);
		$apiContext = new ApiContext($authCredential);
	}
	function createCreditCard($card_info) {
		$card = new CreditCard();
		
		$card->setType($card_info['type']);
		$card->setNumber($card_info['number']);
		$card->setExpire_month($card_info['expire_month']);
		$card->setExpire_year($card_info['expire_year']);
		$card->setCvv2($card_info['cvv2']);
		$card->setFirst_name($card_info['first_name']);
		$card->setLast_name($card_info['last_name']);
		try {
			$card->create($this->apiContext);
			if($card->getState() == 'ok') {
				return $card->getId();
			}
		} catch(Exception $e) {
		}
		return '';
	}
	
	/**
	 *
	 * @param unknown $card_id
	 *
	 * return
	 * {
	 * 		"card_type" : "visa"
	 * 		"number" : "number"
	 * 		"expire_month" : "04"
	 * 		"expire_year" : "2014"
	 * 		"cvv2" : "cvvw"
	 * 		"first_name" : "first_name"
	 * 		"last_name" : "last_name"
	 * }
	 */
	function getCreditCard($card_id) {
		try {
			$card = CreditCard::get($card_id, $this->apiContext);
		
			return array(
					'card_type' => $card->getType(),
					'number' => $card->getNumber(),
					'expire_month' => $card->getExpire_month(),
					'expire_year' => $card->getExpire_year(),
					'cvv2' => $card->getCvv2(),
					'first_name' => $card->getFirst_name(),
					'last_name' => $card->getLast_name()
			);
		}
		catch(Exception $e) {
			return array();
		}
	}
	/**
	 *
	 * @param unknown $card_id
	 * @param unknown $detail
	 * 	{
	 * 		"currency" : "USD"
	 * 		"amount" : "10.70"
	 * 		"shipping_price": "1.20",
	 * 		"tax" : "1.30"
	 * 		"description" : "bla bla bla..."
	 * 	}
	 */
	function createPayment($card_id, $detail) {
		
		
		$cardToken = new CreditCardToken();
		$cardToken->setCredit_card_id($card_id);
		
		$fi = new FundingInstrument();
		$fi->setCredit_card_token($cardToken);
	
		$payer = new Payer();
		$payer->setPayment_method('credit_card');
		$payer->setFunding_instruments(array($fi));
	
		$amountDetails = new AmountDetails();
		$amountDetails->setSubtotal(paypal_price_format($detail['amount']));
		$amountDetails->setTax(paypal_price_format($detail['tax']));
		$amountDetails->setShipping(paypal_price_format($detail['shipping_price']));
		
		$total = $detail['amount'] + $detail['tax'] + $detail['shipping_price'];
	
		$amount = new Amount();
		$amount->setCurrency($detail['currency']);
		$amount->setTotal(paypal_price_format($total));
		$amount->setDetails($amountDetails);
		
		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription($detail['description']);
	
		$payment = new Payment();
		$payment->setIntent('sale');
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));
	
		try {
			$payment->create($this->apiContext);
	
			if($payment->getState() == 'approved')
				return $payment->getId();
			else
				return '';
		} catch (Exception $e) {
			return "";
		}
	
	}
	
	function createPaymentByPaypal($detail, $url) {
		$payer = new Payer();
		$payer->setPayment_method("paypal");

		$amountDetails = new AmountDetails();
		$amountDetails->setSubtotal(paypal_price_format($detail['amount']));
		$amountDetails->setTax(paypal_price_format($detail['tax']));
		$amountDetails->setShipping(paypal_price_format($detail['shipping_price']));
		
		$total = $detail['amount'] + $detail['tax'] + $detail['shipping_price'];

		$amount = new Amount();
		$amount->setCurrency($detail['currency']);
		$amount->setTotal(paypal_price_format($total));
		$amount->setDetails($amountDetails);

		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription($detail['description']);
	
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturn_url($url['success']);
		$redirectUrls->setCancel_url($url['cancel']);
		
		$payment = new Payment();
		$payment->setIntent('sale');
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));
		$payment->setRedirect_urls($redirectUrls);
		
		$response = array(
			'payment_id' => '',
			'approve_link' => ''	
		);
		try {
			$payment->create($this->apiContext);
		} catch (Exception $e) {
			var_dump($e);
			return $response;
		}
		
		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$response['approve_link'] = $link->getHref();
				break;
			}
		}
		$response['payment_id'] = $payment->getId();
		
		return $response;
		
	}
	
	function executePaypalPayment($info) {
		$payment = Payment::get($info['payment_id'], $this->apiContext);
		
		$execution = new PaymentExecution();
		$execution->setPayer_id($info['payer_id']);
		
		$result = $payment->execute($execution, $this->apiContext);

		if($result->getState() == 'approved') return true;
		return false;
	}
	
}


function paypal_price_format($price) {
	return number_format((float)$price, 2, '.', '');
}