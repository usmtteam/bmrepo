<?php


    class Utils{
        

        public static function getBitlyURL($p_sURL){
            $l_sURL = 'http://api.bit.ly/v3/shorten?version=v3&login='.BITLY_LOGIN . '&apikey='.BITLY_APIKEY.'&longUrl='.urlencode($p_sURL).'&format=txt';        
            $l_aData = file_get_contents($l_sURL);
            $l_aData = preg_replace("/\n|\r/","",trim($l_aData));
            return $l_aData;
        }

        public static function getDeviceTypeFromBrowser(){

            $Device_User_Agent = $_SERVER['HTTP_USER_AGENT'];
            $IP = $_SERVER['REMOTE_ADDR'];
            $browsercap = get_browser(null, true);
            $Device_Platform = $browsercap['platform'];
            $Device_Platform_Version = $browsercap['platform_version'];
            $Device_Make = Utils::getMake($Device_User_Agent);
            $Device_Model = $Device_Make;
            
            $pgURL= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;
            $pgURL=rawurlencode($pgURL);
            if ($browsercap['ismobiledevice'] != '') {
                $l_nDeviceType = 1;
            } else if ($browsercap['istablet'] != '') {
                $l_nDeviceType = 2;
            } else {
                $l_nDeviceType = 3;
            }

            return $l_nDeviceType;

        }


        public static function getMake($user_agent){

            $make = "Unknown";

            $os_array = array(
                '/windows nt 6.2/i' => 'Windows 8',
                '/windows nt 6.1/i' => 'Windows 7',
                '/windows nt 6.0/i' => 'Windows Vista',
                '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
                '/windows nt 5.1/i' => 'Windows XP',
                '/windows xp/i' => 'Windows XP',
                '/windows nt 5.0/i' => 'Windows 2000',
                '/windows me/i' => 'Windows ME',
                '/win98/i' => 'Windows 98',
                '/win95/i' => 'Windows 95',
                '/win16/i' => 'Windows 3.11',
                '/macintosh|mac os x/i' => 'Mac OS X',
                '/mac_powerpc/i' => 'Mac OS 9',
                '/linux/i' => 'Linux',
                '/ubuntu/i' => 'Ubuntu',
                '/iphone/i' => 'iPhone',
                '/ipod/i' => 'iPod',
                '/ipad/i' => 'iPad',
                '/android/i' => 'Android',
                '/blackberry/i' => 'BlackBerry',
                '/webos/i' => 'Mobile'
            );

            foreach ($os_array as $regex => $value) {
                if (preg_match($regex, $user_agent)) {
                    $make = $value;
                }
            }

            return $make;
        }

        public static function time_passed($timestamp) {
            //type cast, current time, difference in timestamps
            
            $timestamp = (int) $timestamp;
            if($timestamp!=""){
            $current_time = time();
            $diff = $current_time - $timestamp;

            //intervals in seconds
            $intervals = array(
                'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
            );

            //now we just find the difference
            if ($diff == 0) {
                return 'just now';
            }

            if ($diff < 60) {
                return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
            }

            if ($diff >= 60 && $diff < $intervals['hour']) {
                $diff = floor($diff / $intervals['minute']);
                return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
            }

            if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
                $diff = floor($diff / $intervals['hour']);
                return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
            }

            if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
                $diff = floor($diff / $intervals['day']);
                return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
            }

            if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
                $diff = floor($diff / $intervals['week']);
                return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
            }

            if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
                $diff = floor($diff / $intervals['month']);
                return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
            }

            if ($diff >= $intervals['year']) {
                $diff = floor($diff / $intervals['year']);
                return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
            }
        }
        }
        
        public static function get_time_ago($time_stamp) {

            $time = time() - ($time_stamp / 1000);

            if ($time <= 0)
                $time = 1;

            $divisor = 0;
            $time_unit = '';
            if ($time >= 60 * 60 * 24 * 365.242199) {
                $divisor = 60 * 60 * 24 * 365.242199;
                $time_unit = 'year';
            } else if ($time >= 60 * 60 * 24 * 30.4368499) {
                $divisor = 60 * 60 * 24 * 30.4368499;
                $time_unit = 'month';
            } else if ($time >= 60 * 60 * 24 * 7) {
                $divisor = 60 * 60 * 24 * 7;
                $time_unit = 'week';
            } else if ($time >= 60 * 60 * 24) {
                $divisor = 60 * 60 * 24;
                $time_unit = 'day';
            } else if ($time >= 60 * 60) {
                $divisor = 60 * 60;
                $time_unit = 'hour';
            } else {
                $divisor = 60;
                $time_unit = 'minute';
            }
            $time_units = floor($time / $divisor);


            if ($time_units === 0) {
                return $time_unit == 'hour' ? 'less than an hour ago' : 'less than a ' . $time_unit . ' ago';
            } else if ($time_units === 1) {
                return ($time_unit == 'hour' ? 'an ' : 'a ') . $time_unit . ' ago';
            } else {

                return $time_units . ' ' . $time_unit . 's ago';
            }
        }
            

        public static function outputJSONMinified($p_sStr){
            $p_sStr = preg_replace("{[\t\n\r]+}", '', $p_sStr);
            $p_sStr = preg_replace("{[  ]+}", ' ', $p_sStr);
            return json_encode($p_sStr);
        }

        public static function outputJSONMinifiedAndEncoded($p_sStr,$p_sChunk = false){
            $p_sStr = Utils::outputJSONMinified($p_sStr);
            $p_sStr = stripcslashes($p_sStr);
            $p_sStr = preg_replace("/^\"|\"$/",'',$p_sStr);
            $p_sStr = base64_encode($p_sStr);

            if($p_sChunk)
                $p_sStr = chunk_split($p_sStr,76,"\"+\"");

           return $p_sStr;
        }

        public static function fixCDNImages($p_sStr){
            if(!preg_match("/^https?:/",$p_sStr)){
                return trim(strip_tags(CDN_ASSETS_FOR_API.$p_sStr));
            }
            return trim(strip_tags($p_sStr));
        }


        public static function show404(){
            if (!headers_sent()) {
                header('Location: /404/');
            }else{
                '<script> document.location = "/404/"; </script>';
            }
            exit;
        }

        public static function showMaintenance(){
            if (!headers_sent()) {
                header('Location: /maintenance/');
            }else{
                '<script> document.location = "/maintenance/"; </script>';
            }
            exit;
        }

        
        public static function getImageSized($p_sStr){
            $l_xFileparts = pathinfo($p_sStr);
            return $l_xFileparts['dirname'].'/'.$l_xFileparts['filename'].'_sized.'.$l_xFileparts['extension'];
        }   


       public static function sanitizeOutput($p_sStr){
            $l_aSearch = array(
                '/\>[^\S ]+/s', //strip whitespaces after tags, except space
                '/[^\S ]+\</s', //strip whitespaces before tags, except space
                '/(\s)+/s'  // shorten multiple whitespace sequences
                );
            $l_aReplace = array(
                '>',
                '<',
                '\\1'
                );
            return preg_replace($l_aSearch, $l_aReplace, $p_sStr);
        }


        public static function closeBufferAndContinueProcess($p_sEchoContent){
            
            @ob_end_clean();
            #header('Content-type: application/json');
            header('Connection: close');
            header('Content-Encoding: none');
            //ignore_user_abort(true); // optional
            ob_start();
            echo $p_sEchoContent;
            $size = ob_get_length();
            header("Content-Length: $size");
            ob_end_flush();     
            flush();
            ob_end_clean();
        }
 

        public static function cleanForMailTo($p_sStr){
            return str_replace('%26nbsp%3B','',str_replace("%26%2339%3B","'",rawurlencode(strip_tags($p_sStr)))); 
        }

        public static function returnImagePathForType($p_sURI,$p_xType){
            $l_aType = unserialize($p_xType);
            return Utils::getFilePathWithoutExtension($p_sURI).$l_aType['ext'].'.'.Utils::getFilenameExtension($p_sURI);
        }

        public static function getFilenameExtension($p_sFilename){
            $l_aParts = pathinfo($p_sFilename);
            return $l_aParts['extension'];
        }

        public static function getFilePathWithoutExtension($p_sFilename){
            $l_aParts = pathinfo($p_sFilename);
            return $l_aParts['dirname'].'/'.$l_aParts['filename'];
        }

        public static function makeAltTitle($p_sStr){
            $p_sStr = preg_replace("/[^a-zA-Z0-9\- ]*/","",strip_tags($p_sStr));
            return $p_sStr;
        }

        public static function replaceURLWithHTMLLinks($p_sText){
    
            $p_sRegEx = '/(\b(((https?|ftp|file):\/\/)|(www\.))[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%‌​=~_|])/i';
            $p_sText = preg_replace($p_sRegEx,"<a href=\"$1\" target=\"_blank\">$1</a>", $p_sText);
            $p_sRegEx = '/\ href=\"www/i';
            $p_sText = preg_replace($p_sRegEx," href=\"http://www", $p_sText);
            return $p_sText;
        }

        public static function formValueOutput($p_sStr){
            return htmlspecialchars(trim(strip_tags($p_sStr)));
        }

        public static function cleanPostDataValue($p_sStr){
            return trim(stripslashes(strip_tags($p_sStr)));
        }

        public static function getGeoLocationNameFromLatLong($p_sLat,$p_sLong){
            if( $l_xData = Utils::getGeoLocationDataFromLatLong($p_sLat,$p_sLong) ){
                $l_sLocation = $l_xData->address->city.', '.$l_xData->address->state;
                if(preg_match("/^,$/",trim($l_sLocation)))
                    return '';
                return $l_sLocation;
            }

            return '';
        }

        public static function getGeoLocationDataFromLatLong($p_sLat,$p_sLong){
            
            if(!($p_sLat && $p_sLong))
                return null;

            try{
                $f = file_get_contents(OPENSTREETMAP_API.'/reverse?format=json&lat='.$p_sLat.'&lon='.$p_sLong.'');

                $l_xData = json_decode($f);
                if($l_xData){
                    #$l_sCity = $l_xData->address->city;
                    #$l_sState = $l_xData->address->state;
                    return $l_xData;
                }
            }catch(Exception $e){
                return null;
            }
            return null;

        }
    

        public static function uploadFileAndReturnRelativePathOnSuccess($p_sFolderBase,$p_nUserId,$p_xFile){


            $l_aFileParts  = pathinfo($p_xFile['name']);
            $l_sNewPath = $p_sFolderBase.'/'.date('Y/m/d/');

            if(!is_dir(FILES_PATH.$l_sNewPath)) 
                mkdir(FILES_PATH.$l_sNewPath,0777,true);

            $l_sNewRelativeFilePath = $l_sNewPath.$p_nUserId.uniqid().'.'.strtolower($l_aFileParts['extension']);
            $l_sNewAbsoluteFilePath = FILES_PATH.$l_sNewRelativeFilePath;

            if(!move_uploaded_file($p_xFile['tmp_name'], $l_sNewAbsoluteFilePath)){
                return false;
            }

            //Utils::pushFileToS3($l_sNewAbsoluteFilePath);

            return $l_sNewRelativeFilePath;
        }


        public static function encryptItemId($p_nId){
            return bin2hex(strrev(base64_encode($p_nId)));            
        }

        public static function decryptItemId($p_sEncryptedString){
            return base64_decode(strrev(pack("H*",($p_sEncryptedString))));
        }

        public static function getLocationInfoFromPostalCodeAndCountry($p_sPostal,$p_sCountry){

            $l_sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$p_sPostal.','.$p_sCountry.'&sensor=false';
            if($l_xData = file_get_contents($l_sURL)){
                $l_xData = json_decode($l_xData);
                if($l_xData){
                    return $l_xData;
                }
            }
        }
      

        public static function sendEmail($p_sTo,$p_sSubject,$p_sHtml,$p_sText,$p_sFrom){
            exit;
            $p_sText = strip_tags($p_sText);
            $l_aParams = array('to' => $p_sTo,'subject' => $p_sSubject,'html' => $p_sHtml,'text' => $p_sText,'from' => $p_sFrom);
            $l_sCommand = PHP_PATH.' '.SEND_EMAIL_SCRIPT.' '.base64_encode(serialize($l_aParams));
            exec($l_sCommand . " > /dev/null &");     
            
        }


        public static function buildImageSizesAndCrop($p_sFilePath,$p_aImageTypesArray,$p_sPushToAmazon = true){

            // Forcing to JPG
            require_once(RESIZE_IMAGE_CLASS);
            foreach($p_aImageTypesArray as $l_xAssetsSizeType){
                $l_aImageType = unserialize($l_xAssetsSizeType);
                $image = new resize($p_sFilePath);
                $image->resizeImage($l_aImageType['width'],$l_aImageType['height'],$l_aImageType['type']);
                $l_sNewPath = preg_replace("/\.png/i",".jpg",Utils::returnImagePathForType($p_sFilePath,$l_xAssetsSizeType));

                $l_nQuality = ($l_xAssetsSizeType == FRONT_ASSET_SIZE_TYPE_MOBILE_GRID) ? 72 : 88;
                $image->saveImage($l_sNewPath,$l_nQuality);

                // if(in_array($l_xAssetsSizeType,array(FRONT_ASSET_SIZE_TYPE_SIZED,FRONT_ASSET_SIZE_TYPE_MOBILE_SIZED)))
                //     Utils::watermarkImage($l_sNewPath,$l_sNewPath);

                // Utils::pushFileToS3($l_sNewPath);
            }  
        }  

        public static function getThumbnailWidthAndHeight($p_sFilePath,$p_xAssetsSizeType){
            list($l_nWidth,$l_nHeight) = getimagesize(Utils::returnImagePathForType($p_sFilePath,$p_xAssetsSizeType));
            return array($l_nWidth,$l_nHeight);
        }  

        public static function watermarkImage($p_sImagePath,$p_sFileOutput){
            try{
                $l_xWatermarkOriginal = imagecreatefrompng(BASE_PATH.'/img/global/watermark-photos.png');
                $l_xImage = imagecreatefromjpeg($p_sImagePath);

                $l_nRightMargin = 10;
                $l_nBottomMargin = 10;
                $sx = imagesx($l_xWatermarkOriginal);
                $sy = imagesy($l_xWatermarkOriginal);
                
                $l_nNewWatermarkWidth = $sx/2;
                $l_nNewWatermarkHeight = $sy/2;


                $l_xWatermark = imagecreatetruecolor($l_nNewWatermarkWidth, $l_nNewWatermarkHeight);
                imagecolortransparent($l_xWatermark, imagecolorallocatealpha($l_xWatermark, 0, 0, 0, 127));
                imagealphablending($l_xWatermark, false);
                imagecopyresampled($l_xWatermark, $l_xWatermarkOriginal, 0, 0, 0, 0, $l_nNewWatermarkWidth, $l_nNewWatermarkHeight, $sx,$sy);


                imagecopy($l_xImage, $l_xWatermark, imagesx($l_xImage) - $l_nNewWatermarkWidth - $l_nRightMargin, imagesy($l_xImage) - $l_nNewWatermarkHeight - $l_nBottomMargin, 0, 0, imagesx($l_xWatermark), imagesy($l_xWatermark));
                imagejpeg($l_xImage,$p_sFileOutput,90);
                imagedestroy($l_xImage);
            }catch(Exception $e){

            }
        }   


        public static function copyAndFixPhotoRotation($p_sFilePath){

            $l_aPathInfo = pathinfo($p_sFilePath);
            copy($p_sFilePath,$p_sFilePath.'_rgnl.'.$l_aPathInfo['extension']);

            list($l_aMetaData,$l_xFileInfo) = Utils::getMediaMetaInfo($p_sFilePath);              
           
            $l_aMetaData['meta_tags'] = serialize($l_xFileInfo['jpg']);

            $l_nOrientation = $l_xFileInfo['jpg']['exif']['IFD0']['Orientation'];
            
            $p_sNewFilePath = $p_sFilePath;
            $p_sNewFilePath = preg_replace("/\.png/i",".jpg",$p_sNewFilePath);
            $p_sNewFilePath = preg_replace("/\.jpeg/i",".jpg",$p_sNewFilePath);
            $p_sNewFilePath = preg_replace("/\.gif/i",".jpg",$p_sNewFilePath);

            #$l_nOrientation";
            //8 Veritcal Upsidedown
            //6 Vertical 
            //3 Upside down
            //1 Horizontal

            switch(strtolower($l_aPathInfo['extension'])){

                case 'png':
                    $l_xImage = imagecreatefrompng($p_sFilePath);
                    break;
                case 'gif':
                    $l_xImage = imagecreatefromgif($p_sFilePath);
                    break;

                default:
                    $l_xImage = imagecreatefromjpeg($p_sFilePath);
            }


            if($l_nOrientation == 6){
                $l_xImage = imagerotate($l_xImage,-90, 0);
            }elseif($l_nOrientation == 8){
                $l_xImage = imagerotate($l_xImage,90, 0);
            }elseif($l_nOrientation == 3){
                $l_xImage = imagerotate($l_xImage,-180, 0);
            }elseif($l_nOrientation == 1){
            }

            imagejpeg($l_xImage,$p_sNewFilePath,90);
            imagedestroy($l_xImage); 
            return $p_sNewFilePath;
        }



        public static function getMediaMetaInfo($p_sFilePath){
            require_once(GETID3_CLASS);
            $getID3 = new getID3;
            $l_xFileInfo = $getID3->analyze($p_sFilePath);
            $l_aMetaData['meta_tags'] = serialize($l_xFileInfo['tags']);
            $l_aMetaData['width'] = $l_xFileInfo['video']['resolution_x'];
            $l_aMetaData['height'] = $l_xFileInfo['video']['resolution_y'];
            $l_aMetaData['seconds'] = round($l_xFileInfo['playtime_seconds']);
            $l_aMetaData['fileformat'] = $l_xFileInfo['fileformat'];
            return $l_aMetaData;
        }

        public static function getDuration($file){
            if ($file!=""){
             ## open and read video file
            $handle = fopen($file, "r");
            ## read video file size
            $contents = fread($handle, filesize($file));
            fclose($handle);
            $make_hexa = hexdec(bin2hex(substr($contents,strlen($contents)-3)));
            if (strlen($contents) > $make_hexa){
            $pre_duration = hexdec(bin2hex(substr($contents,strlen($contents)-$make_hexa,3))) ;
            $post_duration = $pre_duration/1000;
            $timehours = $post_duration/3600;
            $timeminutes =($post_duration % 3600)/60;
            $timeseconds = ($post_duration % 3600) % 60;
            $timehours = explode(".", $timehours);
            $timeminutes = explode(".", $timeminutes);
            $timeseconds = explode(".", $timeseconds);
            $duration = $timehours[0]. ":" . $timeminutes[0]. ":" . $timeseconds[0];}
            return $timeseconds[0];
            }
            else {
            return false;
            }
        }

        const ACL_PRIVATE = 'private';
        const ACL_PUBLIC_READ = 'public-read';
        const ACL_PUBLIC_READ_WRITE = 'public-read-write';
        const ACL_AUTHENTICATED_READ = 'authenticated-read';

        const STORAGE_CLASS_STANDARD = 'STANDARD';
        const STORAGE_CLASS_RRS = 'REDUCED_REDUNDANCY';

        public static function pushFileToS3($p_sFilePath,$p_sFileLocation = null){
            
            if(!USE_EC2){
                return;
            }
            
            require_once(AMAZON_AWS_S3_CLASS);
            $l_xS3 = new S3(AMAZON_AWS_KEY, AMAZON_AWS_SECRET);
            
            if(!$p_sFileLocation)
                $p_sFileLocation = str_replace(FILES_PATH,"",$p_sFilePath);
            
            $l_sBucketName = AMAZON_AWS_BUCKET_FOR_ASSETS;
            $l_aContentType = null;

         
            $l_Parts = pathinfo($p_sFilePath);
            switch(strtolower($l_Parts['extension'])){
                case 'css': $l_aContentType = 'text/css'; break;
                case 'js': $l_aContentType = 'application/javascript'; break;
                case 'woff': $l_aContentType = 'font/x-woff'; break;
                case 'ttf': $l_aContentType = 'font/ttf'; break;
                case 'eot': $l_aContentType = 'application/vnd.ms-fontobject'; break;
                case 'svg': $l_aContentType = 'image/svg+xml'; break;
            }

            $l_aHeaders = array();
            $l_aHeaders['Cache-Control'] = 'max-age='.(3600 * 24 * 10);
            $l_aHeaders['Content-Type'] = $l_aContentType;
            
            if($l_xS3->putObjectFile($p_sFilePath, $l_sBucketName, $p_sFileLocation, S3::ACL_PUBLIC_READ,null,$l_aHeaders)) {
                // echo ":\n $p_sFilePath  <br />: $l_sBucketName  <br />:\n $p_sFileLocation";
            }else{
                // echo "(\n)FAIL:: \n $p_sFilePath  <br />: $l_sBucketName  <br />:\n $p_sFileLocation";
            }

            return $p_sFileLocation;
        }
 
        public static function copyFileOnS3($viral_path,$new_path){
            require_once(AMAZON_AWS_S3_CLASS);
            $l_xS3 = new S3(AMAZON_AWS_KEY, AMAZON_AWS_SECRET);
            $l_sBucketName = AMAZON_AWS_BUCKET_FOR_ASSETS;

            if($l_xS3->copyObject($l_sBucketName, $viral_path, $l_sBucketName, $new_path, $acl = self::ACL_PUBLIC_READ, $metaHeaders = array(), $requestHeaders = array(), $storageClass = self::STORAGE_CLASS_STANDARD)){

            }else{
                error_log('UNABLE TO COPY TO S3: '.$new_path."\n",0);
            }
           return $new_path;

        } 

        public static function GetBetween($start, $end, $str){
            $matches = array();
            $regex = "/$start([a-zA-Z0-9_]*)$end/";
            preg_match_all($regex, $str, $matches);
            return $matches;
        }

        public static function getFileOnS3($viral_path){

            require_once(AMAZON_AWS_S3_CLASS);
            $l_xS3 = new S3(AMAZON_AWS_KEY, AMAZON_AWS_SECRET);
            $l_sBucketName = AMAZON_AWS_BUCKET_FOR_ASSETS;

            $file = $l_xS3->getObject($l_sBucketName, $viral_path, $saveTo = false);
        
            return $file;
        }



 


        public static function getAllCountries(){

            return array(
                "US" => "United States",
                "AU" => "Australia",
                "AF" => "Afghanistan",
                "AL" => "Albania",
                "DZ" => "Algeria",
                "AS" => "American Samoa",
                "AD" => "Andorra",
                "AO" => "Angola",
                "AI" => "Anguilla",
                "AQ" => "Antarctica",
                "AG" => "Antigua & Barbuda",
                "AR" => "Argentina",
                "AM" => "Armenia",
                "AW" => "Aruba",
                "AT" => "Austria",
                "AZ" => "Azerbaijan",
                "BS" => "Bahamas",
                "BH" => "Bahrain",
                "BD" => "Bangladesh",
                "BB" => "Barbados",
                "BY" => "Belarus",
                "BE" => "Belgium",
                "BZ" => "Belize",
                "BJ" => "Benin",
                "BM" => "Bermuda",
                "BT" => "Bhutan",
                "BO" => "Bolivia",
                "BA" => "Bosnia/Hercegovina",
                "BW" => "Botswana",
                "BV" => "Bouvet Island",
                "BR" => "Brazil",
                "IO" => "British Indian Ocean Territory",
                "BN" => "Brunei Darussalam",
                "BG" => "Bulgaria",
                "BF" => "Burkina Faso",
                "BI" => "Burundi",
                "KH" => "Cambodia",
                "CM" => "Cameroon",
                "CA" => "Canada",
                "CV" => "Cape Verde",
                "KY" => "Cayman Is",
                "CF" => "Central African Republic",
                "TD" => "Chad",
                "CL" => "Chile",
                "CN" => "China, People's Republic of",
                "CX" => "Christmas Island",
                "CC" => "Cocos Islands",
                "CO" => "Colombia",
                "KM" => "Comoros",
                "CG" => "Congo",
                "CD" => "Congo, Democratic Republic",
                "CK" => "Cook Islands",
                "CR" => "Costa Rica",
                "CI" => "Cote d'Ivoire",
                "HR" => "Croatia",
                "CU" => "Cuba",
                "CY" => "Cyprus",
                "CZ" => "Czech Republic",
                "DK" => "Denmark",
                "DJ" => "Djibouti",
                "DM" => "Dominica",
                "DO" => "Dominican Republic",
                "TP" => "East Timor",
                "EC" => "Ecuador",
                "EG" => "Egypt",
                "SV" => "El Salvador",
                "GQ" => "Equatorial Guinea",
                "ER" => "Eritrea",
                "EE" => "Estonia",
                "ET" => "Ethiopia",
                "FK" => "Falkland Islands",
                "FO" => "Faroe Islands",
                "FJ" => "Fiji",
                "FI" => "Finland",
                "FR" => "France",
                "FX" => "France, Metropolitan",
                "GF" => "French Guiana",
                "PF" => "French Polynesia",
                "TF" => "French South Territories",
                "GA" => "Gabon",
                "GM" => "Gambia",
                "GE" => "Georgia",
                "DE" => "Germany",
                "GH" => "Ghana",
                "GI" => "Gibraltar",
                "GR" => "Greece",
                "GL" => "Greenland",
                "GD" => "Grenada",
                "GP" => "Guadeloupe",
                "GU" => "Guam",
                "GT" => "Guatemala",
                "GN" => "Guinea",
                "GW" => "Guinea-Bissau",
                "GY" => "Guyana",
                "HT" => "Haiti",
                "HM" => "Heard Island And Mcdonald Island",
                "HN" => "Honduras",
                "HK" => "Hong Kong",
                "HU" => "Hungary",
                "IS" => "Iceland",
                "IN" => "India",
                "ID" => "Indonesia",
                "IR" => "Iran",
                "IQ" => "Iraq",
                "IE" => "Ireland",
                "IL" => "Israel",
                "IT" => "Italy",
                "JM" => "Jamaica",
                "JP" => "Japan",
                "JT" => "Johnston Island",
                "JO" => "Jordan",
                "KZ" => "Kazakhstan",
                "KE" => "Kenya",
                "KI" => "Kiribati",
                "KP" => "Korea, Democratic Peoples Republic",
                "KR" => "Korea, Republic of",
                "KW" => "Kuwait",
                "KG" => "Kyrgyzstan",
                "LA" => "Lao People's Democratic Republic",
                "LV" => "Latvia",
                "LB" => "Lebanon",
                "LS" => "Lesotho",
                "LR" => "Liberia",
                "LY" => "Libyan Arab Jamahiriya",
                "LI" => "Liechtenstein",
                "LT" => "Lithuania",
                "LU" => "Luxembourg",
                "MO" => "Macau",
                "MK" => "Macedonia",
                "MG" => "Madagascar",
                "MW" => "Malawi",
                "MY" => "Malaysia",
                "MV" => "Maldives",
                "ML" => "Mali",
                "MT" => "Malta",
                "MH" => "Marshall Islands",
                "MQ" => "Martinique",
                "MR" => "Mauritania",
                "MU" => "Mauritius",
                "YT" => "Mayotte",
                "MX" => "Mexico",
                "FM" => "Micronesia",
                "MD" => "Moldavia",
                "MC" => "Monaco",
                "MN" => "Mongolia",
                "MS" => "Montserrat",
                "MA" => "Morocco",
                "MZ" => "Mozambique",
                "MM" => "Union Of Myanmar",
                "NA" => "Namibia",
                "NR" => "Nauru Island",
                "NP" => "Nepal",
                "NL" => "Netherlands",
                "AN" => "Netherlands Antilles",
                "NC" => "New Caledonia",
                "NZ" => "New Zealand",
                "NI" => "Nicaragua",
                "NE" => "Niger",
                "NG" => "Nigeria",
                "NU" => "Niue",
                "NF" => "Norfolk Island",
                "MP" => "Mariana Islands, Northern",
                "NO" => "Norway",
                "OM" => "Oman",
                "PK" => "Pakistan",
                "PW" => "Palau Islands",
                "PS" => "Palestine",
                "PA" => "Panama",
                "PG" => "Papua New Guinea",
                "PY" => "Paraguay",
                "PE" => "Peru",
                "PH" => "Philippines",
                "PN" => "Pitcairn",
                "PL" => "Poland",
                "PT" => "Portugal",
                "PR" => "Puerto Rico",
                "QA" => "Qatar",
                "RE" => "Reunion Island",
                "RO" => "Romania",
                "RU" => "Russian Federation",
                "RW" => "Rwanda",
                "WS" => "Samoa",
                "SH" => "St Helena",
                "KN" => "St Kitts & Nevis",
                "LC" => "St Lucia",
                "PM" => "St Pierre & Miquelon",
                "VC" => "St Vincent",
                "SM" => "San Marino",
                "ST" => "Sao Tome & Principe",
                "SA" => "Saudi Arabia",
                "SN" => "Senegal",
                "SC" => "Seychelles",
                "SL" => "Sierra Leone",
                "SG" => "Singapore",
                "SK" => "Slovakia",
                "SI" => "Slovenia",
                "SB" => "Solomon Islands",
                "SO" => "Somalia",
                "ZA" => "South Africa",
                "GS" => "South Georgia and South Sandwich",
                "ES" => "Spain",
                "LK" => "Sri Lanka",
                "XX" => "Stateless Persons",
                "SD" => "Sudan",
                "SR" => "Suriname",
                "SJ" => "Svalbard and Jan Mayen",
                "SZ" => "Swaziland",
                "SE" => "Sweden",
                "CH" => "Switzerland",
                "SY" => "Syrian Arab Republic",
                "TW" => "Taiwan, Republic of China",
                "TJ" => "Tajikistan",
                "TZ" => "Tanzania",
                "TH" => "Thailand",
                "TL" => "Timor Leste",
                "TG" => "Togo",
                "TK" => "Tokelau",
                "TO" => "Tonga",
                "TT" => "Trinidad & Tobago",
                "TN" => "Tunisia",
                "TR" => "Turkey",
                "TM" => "Turkmenistan",
                "TC" => "Turks And Caicos Islands",
                "TV" => "Tuvalu",
                "UG" => "Uganda",
                "UA" => "Ukraine",
                "AE" => "United Arab Emirates",
                "GB" => "United Kingdom",
                "UM" => "US Minor Outlying Islands",
                "US" => "United States",
                "HV" => "Upper Volta",
                "UY" => "Uruguay",
                "UZ" => "Uzbekistan",
                "VU" => "Vanuatu",
                "VA" => "Vatican City State",
                "VE" => "Venezuela",
                "VN" => "Vietnam",
                "VG" => "Virgin Islands (British)",
                "VI" => "Virgin Islands (US)",
                "WF" => "Wallis And Futuna Islands",
                "EH" => "Western Sahara",
                "YE" => "Yemen Arab Rep.",
                "YD" => "Yemen Democratic",
                "YU" => "Yugoslavia",
                "ZR" => "Zaire",
                "ZM" => "Zambia",
                "ZW" => "Zimbabwe"
                );

        }

        public static function makeSlug($p_sOriginal){
            $slug = $p_sOriginal;
            $slug = strtolower ($slug);
            $slug = preg_replace('/[^0-9a-z\s]/','', $slug);
            $slug = preg_replace('/\s+/','-', $slug);
            return $slug;
        }

        public static function numberToLetterForPromo($num, $uppercase = FALSE){
            if($num == 0)
                return 'T';

            if($num == 9)
                return 'N';

            $num -= 1;
            $letter = chr(($num % 26) + 97);
            $letter .= (floor($num/26) > 0) ? str_repeat($letter, floor($num/26)) : '';
            return ($uppercase ? strtoupper($letter) : $letter);
        }


        public function get_bitly_url($longurl) {
            $url = 'http://api.bit.ly/v3/shorten?login='.BITLY_LOGIN . '&apikey='.BITLY_APIKEY.'&longUrl='.urlencode($longurl).'&format=txt';
            
            return _get_text($url);
        }
}
   
  



function publishVideoDateCompare($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a->publishedDate < $b->publishedDate) ? 1 : -1;
}


function time_ago_in_words($from_time, $include_seconds = false) {
    if(is_string($from_time)) $from_time = strtotime($from_time);
    $to_time = time();

    $distance_in_minutes = floor(abs($to_time - $from_time) / 60);
    $distance_in_seconds = floor(abs($to_time - $from_time));

    $yest_start = strtotime(date("Y-m-d 00:00:00", (time()-(3600*24))));
    $today_start = strtotime(date("Y-m-d 00:00:00"));

    $string = '';
    $parameters = array();
    if ($distance_in_minutes < 60) {
        $t = $distance_in_minutes;
        if($t==0) $t=1;
        if($t==1) return $t . ' minute ago';
        else return  $t . ' minutes ago';
    } else if ($from_time > $today_start) {
        $t = max(1,floor($distance_in_minutes / 60));
        if($t==1) return $t.' hour ago';
        else return  $t.' hours ago';
    } else if ($from_time > $yest_start){
        return 'yesterday';
    } else {
        return date("m/d/Y", $from_time);
    }

}

function day_ago_in_words($from_time) {
    if(is_string($from_time)) $from_time = strtotime($from_time);
    $to_time = time();

    $yest_start = strtotime(date("Y-m-d 00:00:00", (time()-(3600*24))));
    $today_start = strtotime(date("Y-m-d 00:00:00"));

    if ($from_time > $today_start) {
        return  'Today';
    } else if ($from_time > $yest_start){
        return 'Yesterday';
    } else {
        return date("F jS", $from_time);
    }

}



function time_to_in_words($to_time) {
    if(is_string($to_time)) $from_time = strtotime($to_time);
    $from_time = time();

    $distance_in_minutes = floor(abs($to_time - $from_time) / 60);
    $distance_in_seconds = floor(abs($to_time - $from_time));

    $tom_start = strtotime(date("Y-m-d 00:00:00", (time()+(3600*24))));
    $today_start = strtotime(date("Y-m-d 00:00:00"));
    $to_start = strtotime(date("Y-m-d 00:00:00", $to_time));

    $distance_in_days = round(($to_start - $today_start)/86400);

    $string = '';
    $parameters = array();
    if ($distance_in_minutes < 60) {
        $t = $distance_in_minutes;
        if($t==0) $t=1;
        if($t==1) return 'one minute';
        else return  $t . ' minutes';
    } else if ($to_time < $tom_start) {
        $t = max(1,floor($distance_in_minutes / 60));
        if($t==1) return 'one hour';
        else return  $t.' hours';
    } else if ($to_time == $tom_start){
        return 'tomorrow';
    } else if($distance_in_days < 31) {
        return $distance_in_days.' days';
    } else if($distance_in_days > 31) {
        $m = round($distance_in_days/31);
        if($m==1) return 'about a month';
        else return 'about '.$m.' months';
    }

 }

 function _get_json( $url, $header = '', $post = '', $cookie = '' )
        {
            try {
                $options = array(
                    CURLOPT_RETURNTRANSFER => true,     // return web page
                    CURLOPT_HEADER         => false,    // don't return headers
                    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
                    CURLOPT_ENCODING       => "",       // handle all encodings
                    CURLOPT_USERAGENT      => "spider", // who am i
                    CURLOPT_AUTOREFERER    => true,     // set referer on redirect
                    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
                    CURLOPT_TIMEOUT        => 120,      // timeout on response
                    CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_SSL_VERIFYHOST => FALSE
                );
                
                if(!empty($header)) {
                    $options[CURLOPT_HTTPHEADER] = $header;
                }
                if(!empty($post)) {
                    $options[CURLOPT_POSTFIELDS] = $post;
                }
                if(!empty($cookie)) {
                    $options[CURLOPT_COOKIESESSION] = true;
                    $options[CURLOPT_COOKIEJAR] = $cookie;
                    $options[CURLOPT_COOKIEFILE] = $cookie;
                }
                
                $url = str_replace(' ', '%20', $url);
                $ch      = curl_init( $url );
                curl_setopt_array( $ch, $options );
                $content = curl_exec( $ch );
                $err     = curl_errno( $ch );
                $errmsg  = curl_error( $ch );
                $header  = curl_getinfo( $ch );
                curl_close( $ch );
                
                return json_decode($content);
            }
            catch(Exception $e) {
                throw new Exception('_get_json() : curl error!');
            }
        }

        function _get_text( $url, $header = '', $post = '', $cookie = '' ) {
        try {
            $options = array(
                CURLOPT_RETURNTRANSFER => true,     // return web page
                CURLOPT_HEADER         => false,    // don't return headers
                CURLOPT_FOLLOWLOCATION => true,     // follow redirects
                CURLOPT_ENCODING       => "",       // handle all encodings
                CURLOPT_USERAGENT      => "spider", // who am i
                CURLOPT_AUTOREFERER    => true,     // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
                CURLOPT_TIMEOUT        => 120,      // timeout on response
                CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE
            );
            
            if(!empty($header)) {
                $options[CURLOPT_HTTPHEADER] = $header;
            }
            if(!empty($post)) {
                $options[CURLOPT_POSTFIELDS] = $post;
            }
            if(!empty($cookie)) {
                $options[CURLOPT_COOKIESESSION] = true;
                $options[CURLOPT_COOKIEJAR] = $cookie;
                $options[CURLOPT_COOKIEFILE] = $cookie;
            }
            
            $url = str_replace(' ', '%20', $url);
            $ch      = curl_init( $url );
            curl_setopt_array( $ch, $options );
            $content = curl_exec( $ch );
            $err     = curl_errno( $ch );
            $errmsg  = curl_error( $ch );
            $header  = curl_getinfo( $ch );
            curl_close( $ch );
            
            return $content;
        }
        catch(Exception $e) {
            throw new Exception('_get_text() : curl error!');
        }
    }


?>