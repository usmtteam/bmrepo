<?php


class Html_Text extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('language');
	}
	public function index() {
		$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
		$content_id = isset($_REQUEST['content_id']) ? $_REQUEST['content_id'] : '';
		
		if(empty($type) || empty($content_id)) exit();

        $this->load->model('main_m');

		if($type == 'poll'){
            $feedPoll = $this->main_m->getFeedPoll($content_id);
			echo $feedPoll['end_description'];
        }elseif($type == 'quiz'){
            $feedQuiz = $this->main_m->getFeedQuiz($content_id);
			echo $feedQuiz['end_description'];
        }
	}
}