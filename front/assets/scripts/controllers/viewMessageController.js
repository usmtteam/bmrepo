enriqueApp.controller('viewMessageController', function($scope, $rootScope, $sce, $http, message) {
	$scope.message = message;
	$scope.message_text = $sce.trustAsHtml($scope.message.message);
	console.log($scope.message.message);
	$scope.onReplyMessage_click = function() {
		$rootScope.dialogViewMessage.close();
		$rootScope.showSendMessageDialog($scope.message.sender_id, $scope.message.name, $scope.message.img_url, message);
	}
});