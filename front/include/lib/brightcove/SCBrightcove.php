<?



class SCBrightcove {

        public $token_read = 'v2_tp1lbll_k9JjauR8-RcCMzw26Tq-KD87rxfhK7KdRmlkBsrGsKQ..';
        public $token_write = 'W_5TbK4wOV0TY_avk07vRJVIVXIwj6hH-23Yi0q-JS0GdvQaiM-gCg..';
        public $read_url = 'http://api.brightcove.com/services/library?';
        public $write_url = 'http://api.brightcove.com/services/post';

	public function __construct($p_sReadKey,$p_sWriteKey) {
		if($p_sReadKey)
			$this->token_read = $p_sReadKey;
		if($p_sWriteKey)
			$this->token_write = $p_sWriteKey;
	}

	public function createVideo($file = NULL, $meta) {
		$request = array();
		$post = array();
		$params = array();
		$video = array();


		foreach($meta as $key => $value) {
			$video[$key] = $value;
		}
		$params['token'] = $this->token_write;
		$params['video'] = $video;
		$params['encode_to'] = 'MP4';
		$params['create_multiple_renditions'] = 'True';
		$post['params'] = $params;
		$post['method'] = 'create_video';
		$request['json'] = json_encode($post);
	
		if($file) {
			$request['file'] = '@' . $file;
		}
		
		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);

		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);
		
		// Check request error code and re-call createVideo if request
                // returned a 213 error. A 213 error occurs when you have 
                // exceeded your allowed number of concurrent write requests

		if(isset($json->error))	{
			if($json->error->code == 213) {
				return $this->createVideo($file, $meta);
			} else {
				return FALSE;
			}
		} else {
			return $json;
		}
	}






	
	public function isVideoReady($p_nVideoId) {

		$request = array();
		$post = array();
		$params = array();

		$params['video_id'] = $p_nVideoId;
		$params['token'] = $this->token_write;
		$post['method'] = 'get_upload_status';
		$post['params'] = $params;
		$request['json'] = json_encode($post);

		print_R($request);

		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);
		
		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);
		print_R($json);
		
		if($json->result == 'COMPLETE') return true;
		
		return false;
	}
	






	public function addOverlayLogo($p_nVideoId,$p_nAssetId,$tooltip,$linkurl) {
		
		$request = array();
		$post = array();
		$params = array();
		
		$params['token'] = $this->token_write;
		$params['logooverlay'] = array(
			'tooltip' => $tooltip,
			'linkURL' => $linkurl,
			'image' => array(
			'type' => 'LOGO_OVERLAY',
			'resize' => 'false',
			//              'displayName' => $displayname,
			'id' => $p_nAssetId
			),
			'alignment' => 'BOTTOM_RIGHT'
		);
		$params['video_id'] = $p_nVideoId;
		$post['params'] = $params;
		$post['method'] = 'add_logo_overlay';
		$request['json'] = json_encode($post);
		
		if($file) {
			$request['file'] = '@' . $file;
		}
		print_r($request);
		
		
		// Utilize CURL library to handle HTTP request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->write_url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		$response = curl_exec($curl);
		
		curl_close($curl);
		
		// Responses are transfered in JSON, decode into PHP object
		$json = json_decode($response);

		print_r($json);
		
		// Check request error code and re-call createVideo if request
		// returned a 213 error. A 213 error occurs when you have
		// exceeded your allowed number of concurrent write requests
		if(isset($json->error))	{
				return FALSE;
		} else {
			return $response;
		}
	}

	



}