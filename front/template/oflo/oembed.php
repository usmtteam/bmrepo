<?php

    include(__DIR__.'/../../config/settings.php');

    $Device_User_Agent = $_SERVER['HTTP_USER_AGENT'];
    $Device_Make = Utils::getMake($Device_User_Agent);

    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($Device_User_Agent))) {
        $l_nDeviceType = DEVICE_TYPE_TABLET;
        if (preg_match('/(ipad)/i', strtolower($Device_User_Agent))){ $Device_Platform = "iOS";}
    }
    else if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($Device_User_Agent))) {
        $l_nDeviceType = DEVICE_TYPE_MOBILE;
        if (preg_match('/(iphone)/i', strtolower($Device_User_Agent))){ $Device_Platform = "iOS";}
    }
    else{
        $l_nDeviceType = DEVICE_TYPE_DESKTOP;
    }

    $l_bIsMobile = false;

    if($l_nDeviceType == DEVICE_TYPE_MOBILE){
        $l_bIsMobile = true;
//        $l_aZoneUnits = array('ZONE_1'=>11,'ZONE_2'=>12,'ZONE_3'=>13);
        $l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=14';
    }else{
//        $l_aZoneUnits = array('ZONE_1'=>16,'ZONE_2'=>1,'ZONE_3'=>2);
        $l_sAdServerURL = 'https://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=4';
    }

    $a = $_GET['p_sCode'];
    $a = json_decode(base64_decode($a));

    //var_dump($a); exit;

    $video_destination = strip_tags($a->video);
    $video_height = strip_tags($a->video_height);
    $video_duration = strip_tags($a->video_duration);
    $slug = strip_tags($a->slug);
    $l_nAssetId = strip_tags($a->id);
    $ga_key = strip_tags($a->ga_tag);
    $img_url = strip_tags($a->img_url);
    $small_thumb = CDN_FRONT."/".$slug."/assets/ufile/big_thumb/".$img_url;
    $title = strip_tags($a->title);
    $description = strip_tags($a->title);
    $l_xPreviousVideoResult['id'] = strip_tags($a->previous_id);
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $shortenedUrl = Utils::get_bitly_url($actual_link);
    $shortenedUrl = str_replace(array("\n", "\r"), '', $shortenedUrl);

?>

<html>
<head>
    <meta property="og:title" content="<?= $title; ?>" />
    <meta property="og:description" content="<?= $description; ?>"/>
    <meta property="og:type" content="website" />
<!--    <meta property="og:url" content="http://--><?//=$slug;?><!--.on.starsite.com/flowplayer/video/--><?//=$l_nAssetId;?><!--/" />-->
    <meta property="og:url" content="http://<?=$slug;?>.<?=OFLO_LANDING?>/ocontent/video/<?=$l_nAssetId;?>/" />
    <meta property="og:image" content="<?=CDN_FRONT?>/<?=$slug;?>/assets/ufile/watermark/<?= $img_url; ?>" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />

    <!-- Load videojs player dependent stylesheets. -->
    <link href="//vjs.zencdn.net/5.4.6/video-js.css" rel="stylesheet">

    <link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/assets/videojs-contrib-ads-master-3.1.2/src/videojs.ads.css" />
    <!--    <link rel="stylesheet" href="--><?//=STATIC_FILES_HOST?><!--/videojs-contrib-ads-1.1.0/src/videojs.ads.css" />-->
    <link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/assets/videojs-ima-master-5.3/src/videojs.ima.css" />
    <link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/assets/videojs-resolution-switcher-master-0.4.2/lib/videojs-resolution-switcher.css" />

    <style type="text/css">
        .videojs-player-holder{ background:#000; margin: 0; box-shadow:0 0 5px #E2E2E2;}
        .videojs-player-holder{ overflow: hidden; position: relative; } /* Fix for static videojs-ima */
        .video-js, /*.vjs-volume-level, .vjs-play-progress,*/ .vjs-menu-button { color: #b20000; }
        .video-js { width: 100% !important; height: 0px; padding-bottom: 56.25%; }
        /*.vjs-loading-spinner { opacity: 0 }*/
        /*.vjs-default-skin.vjs-ended .vjs-play-control:before { content: ''; }  /*Garbled on embed*/
        .vjs-embedoverlay { font-family: Avenir, Arial; position: absolute; top: 10px; left: 40px; background-color: #333; padding: 3px 5px; border-radius: 3px; box-shadow: 0 0 3px #ccc; font-size: 12px; text-align: center; }
        .vjs-embedoverlay-input { font-family: monaco,"courier new",verdana; background-color: transparent; width: 400px; border: none; color: #b20000; }

        /* Preroll div wrapper	*/
        #ima-ad-container > div:nth-child(1) { display: flex; width: 100% !important; /* Overlays videojs on error *//*height: 100% !important; padding-bottom: 8.5%;*/ }
        /* Preroll iframe */
        #ima-ad-container > div:nth-child(1) > iframe { width: 100% !important; height: 100% !important; }

        .endscreen {
            top: 10%;
            left: 10%;
            position: absolute;
            width: 80%;
            /*height: 70%;*/
            height: 80%;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            font-size: 14px;
            background-color: #777;
            text-align: center;

            /* do not show endscreen by default - a simpler, non-animated alternative would be: */
            display: none;
            /*opacity: 0;*/
            /*filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);*/
            -webkit-transition: opacity .5s;
            -moz-transition: opacity .5s;
            transition: opacity .5s;

            /* overlay player endscreen when video is finished */
            z-index: 2;

            /* show endscreen - a simpler alternative would be:
            * display: block; */
            opacity: 1;
            filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
            overflow-wrap: break-word;

        }
        /*.endscreen h3 {*/
            /*font-size: 160%;*/
            /*font-weight: bold;*/
            /*color: #ccc;*/
            /*margin-top: 10%;*/
        /*}*/
        /*.endscreen h4 {*/
            /*font-size: 120%;*/
            /*color: #eee;*/
        /*}*/
        .endscreen .button {
            /*background-color: #00a6bc;*/
            background-color: #b20000;
            padding: 20px 30px;
            margin: 15px 0;
            -webkit-border-radius: 60px;
            -moz-border-radius: 60px;
            border-radius: 60px;
            font-size: 110%;
            color: #eee;
            cursor: pointer;
            display: inline-block;
            -webkit-transition: background-color .1s;
            -moz-transition: background-color .1s;
            transition: background-color .1s;

        }
        .endscreen .button:hover {
            /*background-color: #00afc6;*/
            background-color: #b20000;
        }
        .endscreen .button:active {
            /*background-color: #009db2;*/
            background-color: #5abdb2;
        }
        .endscreen .vjs-toggle {
            display: block;
            color: #eee;
            cursor: pointer;
        }
        .endscreen .vjs-toggle:hover {
            color: #444;
        }

        .endscreen b {
            color: #eee;
        }
    </style>
    <!--    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>-->
    <script src="//code.jquery.com/jquery-2.0.3.min.js"></script>
</head>
<body>
    <div class="videojs-player-holder" style="width: 680px; height: 383px;">

        <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>

        <video id="my-video" class="video-js vjs-big-play-centered" preload="none" width="100%" height="100%"
               poster="<?= $small_thumb ?>" data-setup='{"controls": true}'>

        <? } else { ?>

        <video id="my-video" class="video-js vjs-big-play-centered vjs-default-skin" width="680" height="383"
                poster="<?= $small_thumb ?>" data-setup='{"controls": true, "plugins": {"embed": {"embeddable": true, "location":"<?= $shortenedUrl ?>"}}}'>

        <? } ?>

            <!--  Video source loop -->
            <?php

            $video_res_arr = ["360", "480", "720", "1080"];

            if (!empty($video_height) || $video_height != 0){

                for ($ctr = 0; $video_res_arr[$ctr] <= $video_height; $ctr++ ) {
                    ?>
                    <source type='application/x-mpegurl' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $slug ?>/assets/ufile/<?=$video_destination?>/master.m3u8" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                    <source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $slug ?>/assets/ufile/<?=$video_destination?>-<?=$video_res_arr[$ctr]?>.mp4" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                    <source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $slug ?>/assets/ufile/<?=$video_destination?>-<?=$video_res_arr[$ctr]?>.webm" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                    <source type='video/flash' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $slug ?>/assets/ufile/<?=$video_destination?>-<?=$video_res_arr[$ctr]?>.mp4" label="<?=$video_res_arr[$ctr]?>" res="<?=$video_res_arr[$ctr]?>">
                    <?php
                }
            } ?>

            <source src="<?=CDN_FRONT?>/<?=$slug;?>/assets/ufile/<?=$video_destination;?>/master.m3u8" type="application/x-mpegurl" label="216" res="216">
            <source src="<?=CDN_FRONT?>/<?=$slug;?>/assets/ufile/<?=$video_destination;?>-216.webm" type="video/webm" label="216" res="216">
            <source src="<?=CDN_FRONT?>/<?=$slug;?>/assets/ufile/<?=$video_destination;?>-216.mp4" type="video/mp4" label="216" res="216">
            <source src="mp4:<?=CDN_FRONT?>/<?=$slug;?>/assets/ufile/<?=$video_destination;?>-216" type="video/flash" label="216" res="216">
        </video>

<!--        --><?php //if($l_nDeviceType != DEVICE_TYPE_MOBILE) { ?>

            <div class="endscreen">
                <span>
                    <a class="button"
                       href="http://<?=$_SERVER['HTTP_HOST']?>/ocontent/video/<?=$l_xPreviousVideoResult['id']?>"
                       target="_blank">Next video</a>
                    <a class="vjs-toggle">Play it again!</a>
                </span>
            </div>

<!--        --><?php //} ?>
    </div>

    <!-- If you'd like to support IE8 -->
    <script src="//vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
    <script src="//vjs.zencdn.net/5.4.6/video.js"></script>
    <!--                        <script src="--><?//=STATIC_FILES_HOST?><!--/assets/videojs-embed-master-0.3.3/dist/videojs.embed.js"></script>-->
    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-embed-master-0.3.3/dist/videojs.embed.min.js"></script>

<!--    <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-ga-master-4.6.1/libs/video-js-4.6.1/video.dev.js"></script>-->
<!--    <script src="--><?//=STATIC_FILES_HOST?><!--/assets/videojs-ga-master-4.6.1/dist/videojs.ga.min.js"></script>-->

    <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-contrib-ads-master-3.1.2/src/videojs.ads.js"></script>
    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-ima-master-5.3/src/videojs.ima.js"></script>
    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-resolution-switcher-master-0.4.2/lib/videojs-resolution-switcher.js"></script>

    <script type="text/javascript">

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

////        ga('create', '<?////= $l_xChannel->google_analytics_key ?>////','<?////= $l_xChannel->domain ?>////.on.oflo.co');
        ga('create', '<?= $ga_key ?>','<?= $slug ?>.on.breakingmuscle.com');
        ga('require', 'displayfeatures');
////        ga('send', 'pageview');
////        ga('set', 'page', '<?////= $end_of_url ?>////');
//
//        ga('set', 'hostname', window.location.hostname);
//        ga('set', 'page', window.location.pathname);
//        ga('send', 'pageview');

    </script>

    <script type="text/javascript">

        var player = videojs('my-video');

        <? if ($l_nDeviceType == DEVICE_TYPE_DESKTOP) {
            if ($video_height >= 480){
                $default_resolution = 480;
            } elseif ($video_height >= 360){
                $default_resolution = 360;
            } else {
                $default_resolution = 216;
            }
        ?>

            player.videoJsResolutionSwitcher(
                {default: '<?=$default_resolution?>'}
            );

        <?php } ?>

        player.ready( function(){

            console.log('++++++++++++++++ready');
//            player.poster("<?//= $l_sImage ?>//");

            <? if ($l_nDeviceType != DEVICE_TYPE_MOBILE && $video_duration < 30) { ?>

            console.log("start");
//            console.log("play");
//
//            this.play();

            <? } ?>

            this.one('loadedmetadata', function(){

                console.log('++++++++++++++++loadedmetadata');

            }).one('play', function(){

                console.log('++++++++++++++++play');
                this.trigger('video_ga_events');

                $('.endscreen').hide();
                $('.vjs-big-play-button').hide();

            }).on('play', function(){

                console.log('++++++++++++++++play');

                $('.endscreen').hide();
                $('.vjs-big-play-button').hide();

            }).on('contentended', function(){ // listener for the 'ended' event of the video player

                console.log('++++++++++++++++contentended');

                var dur = Math.round(this.duration());
                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);

                <?php if (preg_match("/android/i", $Device_Make)){ ?>
//                    $('.vjs-big-play-button').show();
//                    $('.vjs-control-bar').hide();
                <?php } else { ?>
                $('.endscreen').show();
                <? } ?>

                this.one('play', function(){
                    this.trigger('video_ga_events');
//                    $('.vjs-control-bar').show(); // Android replay control fix
                });

                $('.vjs-control-bar, .vjs-menu-button').one('click', function(){
                    console.log('vjs-control-bar area');
                    $('.endscreen').hide();
                });

                this.one('timeupdate', function(){
                    console.log('one timeupdate');
                    $('.endscreen').hide();
                });

            }).on('error', function(){

                console.log('++++++++++++++++error');

                var dur = Math.round(this.duration());
                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);

            }).on('video_ga_events', function(){

                console.log('video_ga_events');

                var dur = parseInt(this.duration());
//              console.log("dur: " + dur);

                var pause = Math.round(dur * .25) + 1;
                pause = pause * 1000;

                var pausehalf = Math.round(dur * .5) + 1;
                pausehalf = pausehalf * 1000;

                var pausethreefourths = Math.round(dur * .75) + 1;
                pausethreefourths = pausethreefourths * 1000;

//              console.log('this.duration(): ' + this.duration());
//              console.log('dur: ' + dur);
//              console.log('pause: ' + pause);
//              console.log('pausehalf: ' + pausehalf);
//              console.log('pausethreefourths: ' + pausethreefourths);

                ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Impression', dur);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
                },pause);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Half', dur);
                },pausehalf);
                setTimeout(function(){
                    ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Three-Fourths', dur);
                },pausethreefourths);

            }).one('resolutionchange', function(){
                console.info('autoplay');
                this.autoplay(true); // Autoplay resolution switcher
            }).on('resolutionchange', function(){
                console.info('Source changed to %s', this.src());
            });

            // Fire contentended event on ended event for non-mobile Android or short videos
            <?php if (($l_nDeviceType != DEVICE_TYPE_MOBILE && preg_match("/android/i", $Device_Make)) ||
                      $video_duration < 30){ ?>

            console.log('enable pseudo-contentended');

            this.on('ended', function(){

                this.trigger('contentended');
            });

            <? } ?>

        });

        $(function() {

            console.log('doc ready');

            // Mobile video greater than 30 sec video
            <?php if ($l_nDeviceType == DEVICE_TYPE_MOBILE &&
                      $video_duration > 30){ ?>

            $("#my-video").one('touchstart', function(e){

                console.log('touchstart');

                player.pause();

                init_ima_plugin();

            });

            <? } ?>

            // Non-Android or non-mobile video greater than 30 sec video
            <? if ((!preg_match("/android/i", $Device_Make) && $l_nDeviceType != DEVICE_TYPE_MOBILE) &&
                    $video_duration > 30) { ?>

            console.log("Start");
            init_ima_plugin();

            <? } ?>

            $('.vjs-toggle').click(function () {

//    		        console.log('.vjs-toggle');

                player.play();
            });

            function init_ima_plugin(){

//                player.bigPlayButton.hide();

                console.log('init_ima_plugin');

                // Initialize the IMA plugin
                var options = {
                    id: 'my-video',
                    adTagUrl: '<?=$l_sAdServerURL?>',
//                    debug: true,
                    numRedirects: 2,
                    vpaidMode: google.ima.ImaSdkSettings.VpaidMode.INSECURE,
                };
                player.ima(options);
                player.ima.addContentEndedListener = "contentended";
                player.ima.initializeAdDisplayContainer();

                // Request ads and play the video
                player.ima.requestAds();
//                console.log("Play-Pause");
//                player.play();
//                player.pause();
            }

        });

    </script>
</body>

</html>
