<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Custom Define
|--------------------------------------------------------------------------
 */

define('RT_PATH', dirname(dirname(__DIR__)));


$base_url = "http://".$_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);


define('MODE_DEV', false);
define('BASE_URL',    $base_url);


define ('FACEBOOK_ID', '1440671026187879');

define( 'PRODUCTION_BUNDLEID' , 'com.starclub.' );  // lowercase only!!!!!!


define('ASSETS_DIR',	$base_url.'assets');
define('IMG_DIR',	$base_url.'assets/img');
define('CSS_DIR',	$base_url.'assets/css');
define('JS_DIR',	$base_url.'assets/js');
//define('EDT_DIR', 	RT_PATH.'/assets/fckeditor');
define('UPLOAD_DIR', RT_PATH.'/assets/ufile/');
define('UPLOAD_DIR_WATER', RT_PATH.'/assets/ufile/watermark/');
define('UPLOAD_URL', $base_url.'assets/ufile/');

define('SITE_NAME', 'Starclub Inc.');
define('ADMIN_EMAIL', 'support@starsite.com');
define('MEMCACHE_KEY_CACHE_RELEASE',40);
define('MEMCACHE_KEY_PREPEND','mem-sc-');

define('MEMCACHE_DURATION_CHANNEL',0);
define('MEMCACHE_DURATION_BC_VIDEOS',0);

define('LIB_PATH',RT_PATH.'/../front/include/lib/');
define('PHP_PATH','/usr/bin/php');
define('ZENCODER_API_KEY','15eb9baaab0f8bc3f689e6559b9e66f3');
define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');
define('ZENCODER_PROCESS_VIDEO_SCRIPT',LIB_PATH.'zencoder/clProcessVideo.php');
define('ZENCODER_FAIL_LOG','/var/log/zencoder_fail.log');
define('ZENCODER_SUCCESS_LOG','/var/log/zencoder_success.log');


define('BITLY_LOGIN', 'scsocial');
define('BITLY_APIKEY', 'R_ecc44a0059c842039e65b4594f0599c6');

define('BRIGHTCOVE_WRITE_API_KEY', 'W_5TbK4wOV0TY_avk07vRJVIVXIwj6hH-23Yi0q-JS0GdvQaiM-gCg..');
define('BRIGHTCOVE_READ_API_KEY', 'v2_tp1lbll_k9JjauR8-RcCMzw26Tq-KD87rxfhK7KdRmlkBsrGsKQ..');

define('PAYPAL_MODE_SANDBOX', true);
define('PAYPAL_CLIENT_ID', PAYPAL_MODE_SANDBOX ? 'AWnNSxCO0eAiuMYpuGWe2HAATn7KgEPLEMlk6WWYmJzFeOW7XlQPbgUcsh2Q' : '');
define('PAYPAL_CLIENT_SECRET', PAYPAL_MODE_SANDBOX ? 'EMLWThAioaiJDZMC41xOmJZvyfimVX2Klxxf1DxClcLpzJsdKXpBU1Q39_IS' : '');

/* End of file constants.php */
/* Location: ./application/config/constants.php */