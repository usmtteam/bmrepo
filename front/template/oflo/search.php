<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);
$url = $_SERVER['REQUEST_URI'];
$query = parse_url($url, PHP_URL_QUERY);

if($query!=NULL){
     if(isset($_GET['search'])){


      if(!isset($_GET['page'])){
        $l_sSearch = $_GET['search'];
    

        header('Location: /search/'.$l_sSearch.'/');

        //exit;
      }else if(isset($_GET['page'])){

        $l_sSearch = $_GET['search'];
        $l_sPage = $_GET['page'];

        header('Location: /search/'.$l_sSearch.'/'.$l_sPage.'/');

       // exit;

      }else{

     //   header('Location: /');

      }
      exit();
    }
}


//    require(__DIR__.'/../../config/init.php');
require(__DIR__.'/../../config/settings.php');

require(__DIR__.'/init.php');
require(__DIR__.'/db_home.php');

include '_head.php';

include '_menu.php' ;

?>




    <!-- make this dynamic so it will show up  -->
<div class="box-menu fixed-width">
  <h1 class="catsTitle">
            <?php  if($l_xOfloSearchResults){

              echo "
            <span>Search results for ".$_GET['search']."</span></h1>";}else{

            
                    echo "<div id='hidead'>There is no result found for ".$_GET['search']."</div>";}

                    ?>
  		<br>
      
     
         <!-- row of thumnails 3x4 -->
        <div class="">
            
          

            <div class="col-md-12 col-sm-12 col-xs-12 cats-menu">
              <div>
                <?php
                $search_total_count = count($l_xOfloSearchResults);
                for ($i = ($page*16); $i < $search_total_count && $i < ($page + 1) * 16; $i++) : ?>
                
                  

                   <div class="item <?php if ($i == 0) { echo ' active'; } ?>">
                     <a style="margin-bottom: 20px;" class="img-responsive col-md-3 col-sm-12"  href="http://<?=$l_xOfloSearchResults[$i]['url'].'.'.OFLO_LANDING?>/ocontent/<?php echo $l_xOfloSearchResults[$i]['content_type'].'/'.$l_xOfloSearchResults[$i]['id'] ?>/">
                        
                        <div <?php if ($l_xOfloSearchResults[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloSearchResults[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='cat-pics-wrap-embeded'";}else{echo "class='cat-pics-wrap'";} ?>>

                        <img class="backup_picture <?php if ($l_xOfloSearchResults[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloSearchResults[$i]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ 
                          echo 'col-md-12 cat-page-pics-embeded';}else{echo 'col-md-12 cat-page-pics';} ?>" src="<?php echo CDN_FRONT.'/'.$l_xOfloSearchResults[$i]['url'].'/assets/ufile/thumb/'; echo $l_xOfloSearchResults[$i]['image_path'];?>" alt="Image">
                        
                        </div>
                        
                        <div class="cat-pics-description">
                        <p><strong><?=substr($l_xOfloSearchResults[$i]['description'],0,70);?><? $strlen =strlen($l_xOfloSearchResults[$i]['description']); if($strlen>70){echo "...";} ?></strong><br>  <span>Posted By <span class="by_channel"><?php echo $l_xOfloSearchResults[$i]['url'] ?></span>

                            <br>

                            <?php echo number_format($l_xOfloSearchResults[$i]['total_views']).' Views <span class="dot_between"></span> '. Utils::time_passed($l_xOfloSearchResults[$i]['feed_date']+25200);?></span></p>
                        </div>
                     </a>


                   </div>

                <?php endfor; ?>
            </div>

        </div>


                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <ul class="pager">
           <div class="span">
      <div class="pager">Showing <?php echo (($search_page_count*($page))+1); ?> to <?php if($search_total_count<($search_page_count*($page+1))){ echo $search_total_count; }else{ echo ($search_page_count*($page+1)); } ?> of <?php echo $search_total_count; ?> entries</div>
</div>
          
       
             <ul>
                            <li class="prev<?php echo $page == 0 ? ' disabled' : ''?>">
                                <a data-page="<?php echo max(0, $page - 1);?>" style="<?php echo $page == 0 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">Previous</a>
                            </li>
                            <!-- first page -->
                            <?php if ($page > 2) : ?>
                                <li class="">
                                    <a data-page="<?php echo 0;?>" style="cursor: pointer;"><?php echo 1; ?></a>
                                </li>
                            <?php endif; ?>

                             <?php if ($page >= 4) : ?>
                                <li class="">
                                    <a data-page="" src="" style="pointer-events: none; cursor: none;">...</a>
                                </li>
                            <?php endif; ?>
                          
                            <!-- pages inbetween -->
                            <?php for($i = $search_first_page; $i < $search_last_page; $i++) :?>
                                <li class="<?php echo $page == $i ? ' active' : ''; ?>">
                                    <a data-page="<?php echo $i;?>" style="cursor: pointer;"><?php echo $i+1; ?></a>
                                </li>
                            <?php endfor; ?>

                            <!-- ellipsis -->
                            <?php if ($search_last_page <= $search_total_page_count - 2) : ?>
                                <li class="">
                                    <a data-page="" style="pointer-events: none; cursor: pointer;">...</a>
                                </li>
                            <?php endif; ?>

                            <!-- last page -->
                            <?php if ($search_last_page <= $search_total_page_count - 1) : ?>
                                <li class="">
                                    <a data-page="<?php echo $search_total_page_count - 1;?>" style="cursor: pointer;"><?php echo $search_total_page_count; ?></a>
                                </li>
                            <?php endif; ?>
    
                 
                            <li class="<?php echo $page == $search_total_page_count-1 ? ' disabled' : ''?>">
                                <a data-page="<?php echo min($search_total_page_count-1, $page + 1 ); ?>" style="<?php echo $page == $search_total_page_count-1 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">Next </a>
                            </li>
                        </ul>
                     

              </div>
                       

            <input type="hidden" value="<?php echo $search_base_url; ?>" id='base_url'>

                <script language="javascript">
                    $(document).ready(function() {
                        $('.pager a').click(function(e) {
                            var base_url = jQuery('#base_url').val();
                         document.location.href = base_url + "&page=" + $(e.target).attr('data-page');
                        });
                    });
                </script>



    </div>


    <script type="text/javascript">

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= $oflo_global_ga_key ?>','breakingmuscle.com/search');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
        ga('set', 'page', '<?= $_SERVER['REQUEST_URI'] ?>');
    </script>


 <?php include '_home_footer.php' ?>
  <?php include "_footer.php" ?>