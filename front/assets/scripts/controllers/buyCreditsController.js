enriqueApp.controller('buyCreditsController', function($scope, $rootScope, $http, $sce) {
	
	$scope.card_option = 2;
	$scope.credit_card_info = null;
	$scope.error = '';
	$scope.creditCardTypeName = {
		visa : "Visa",
		amex : 'American Express',
		discover : "Discover",
		mastercard : "Mastercard"
	};
	
	$scope.load = function() {
		CommonLib.show_loading_bubble();
		var url = $rootScope.get_api_url() + '&option=get_credit_card_info';
		$http({method: 'GET', url: url}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		$scope.credit_card_info =data.card_info;
	    		if($scope.credit_card_info == "" || $scope.credit_card_info == null || typeof $scope.credit_card_info == undefined) {
	    			$scope.card_option = 2;
	    			$scope.credit_card_info = null;
	    		}
	    		else {
	    			$scope.card_option = $rootScope.ACCEPT_CREDITCARD ? 0 : 2;
	    		}
	    	}        	
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
	$scope.onUseExistingCardInfo_click = function() {
		$scope.card_option = 0;
	};
	$scope.onEnterNewCardInfo_click = function() {
		$scope.card_option = 1;
	};
	$scope.onPaybyPaypal_click = function() {
		$scope.card_option = 2;
	};
	$scope.onRemoveCard_click = function() {
		CommonLib.show_loading_bubble();
		var url = $rootScope.get_api_url() + '&option=clear_credit_card_info';
		$http({method: 'GET', url: url}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		$scope.credit_card_info = null;
	    		$scope.card_option = 1;
	    	}        	
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
	$scope.onBuyCredits_click = function() {
		$scope.error = '';
		var url = $rootScope.get_api_url() + '&option=create_payment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '');
		
		var post_data = {};
		post_data.pay_option = $scope.card_option;
		post_data.credit_count = $('#credits_count').val();
		if($scope.card_option == 1) {
			var type = $('#card_type').val();
			var number = $('#card_number').val();
			var expire_month = $('#card_expire_month').val();
			var expire_year = $('#card_expire_year').val();
			var cvv2 = $('#card_cvv').val();
			var first_name = $('#card_first_name').val();
			var last_name = $('#card_last_name').val();
			
			if(first_name == '') {
				$scope.error = 'Please enter card holder\'s first name.';
				return;
			}
			else if(last_name == '') {
				$scope.error = 'Please enter card holder\'s last name.';
				return;
			} 
			else if(number == '') {
				$scope.error = 'Please enter card number.';
				return;
			}
			else if(cvv2 == '') {
				$scope.error = 'Please enter card CVV.';
				return;
			}
			
			
			post_data.type = type;
			post_data.number = number;
			post_data.expire_month = expire_month;
			post_data.expire_year = expire_year;
			post_data.cvv2 = cvv2;
			post_data.first_name = first_name;
			post_data.last_name = last_name;
		}
		CommonLib.show_loading_bubble();
		$http({
			method: 'POST', 
			url: url, 
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
			data: CommonLib.serializeData(post_data)
		}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		if($scope.card_option == 2) {
	    			$rootScope.openPaypalDialog(data.approve_link);
	    		}
	    		else {
		    		$rootScope.credit = data.credits;
		    		$rootScope.saveCookie();
		    		$rootScope.buyCreditsDialog.close();
	    		}
	    	}   
	    	else {
	    		$scope.error = data.message;
	    	}
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
	if($rootScope.ACCEPT_CREDITCARD) $scope.load();
});