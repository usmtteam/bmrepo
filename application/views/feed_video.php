<!-- BEGIN PAGE -->
<?php
//    phpinfo();
?>
<style>

</style>

<div class="page-content">
    <?php 
        $this->load->view('header_feed');
    ?>
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
        
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("content/feed/video", 'id="videofileupload" class="form-horizontal"');

                        echo form_error("description");
                        echo form_error("tags");
                        echo form_error("video");
                        
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <input type="hidden" id="ajaxuploadvideo" value="<?php echo site_url("content/upload_video"); ?>">
                    
                    <script LANGUAGE="JavaScript">
                        function CountLeft(field, count, max) {
                            if (field.value.length > max)
                            field.value = field.value.substring(0, max);
                            else
                            count.value = max - field.value.length;
                        }
                    </script>
              

                    <div class="row-fluid">
                        <div class="span6">
                          
                          <?php include(__DIR__.'/feed_template_description.php'); ?>

                            <div class="control-group" style="margin:0px; width:220px; height:24px;">
                                
                                <div class="span8">
                                    <div class="row-fluid fileupload-buttonbar">
                                        <div class="span12">




                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn fileinput-button">
                                                <i class="icon-plus icon-white" style="background: none !important;"></i>


                                                <span>Add Video File</span>
                                                <input type="file" id="some-file-input-field" name="video"  style="
                                                    left: 180px;
                                                    top: 0px;
                                                    width: 100px;
                                                    height: 10px;
                                                    right: 0px;
                                                    margin-left: ‒3;
                                                    border-right-width: 0px;
                                                    border-left-width: -400;
                                                    border-left-width: 0px;">
                                            </span>
                                        </div>
                                        
                                        <!-- The global progress information -->
                                        <div class="span5 fileupload-progress fade" style="display: none;">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                <div class="bar" style="width:0%;"></div>
                                            </div>
                                            <!-- The extended global progress information -->
                                            <div class="progress-extended">&nbsp;</div>
                                        </div>
                                    </div>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped" style='margin-bottom: 0px;'>
                                        <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
                                        </tbody>
                                    </table>
                                    <!-- The loading indicator is shown during file processing -->
                                    <div class="fileupload-loading"></div>
                                    <br>
                                </div>
                            </div>
                           
                            <div class="row-fluid" style="visibility:hidden">
                                <div class="span8">
                                    <script id="template-upload" type="text/x-tmpl">
                                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                                            <tr class="template-upload fade">
                                                <td class="name"><span>{%=file.name%}</span></td>
                                                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                {% if (file.error) { %}
                                                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                                                {% } else if (o.files.valid && !i) { %}
                                                    <td>
                                                        <div id="progressbar" class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
                                                    </td>
                                                    <td class="start" >{% if (!o.options.autoUpload) { %}
                                                        <button class="btn">
                                                            <i class="icon-upload icon-white"></i>
                                                            <span>Start</span>
                                                        </button>
                                                    {% } %}</td>
                                                {% }  %}
                                                    
                                                <td class="cancel"></td>
                                            </tr>
                                        {% } %}
                                    </script>
                                    <!-- The template to display files available for download -->
                                    <script id="template-download" type="text/x-tmpl">
                                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                                            <tr class="template-download fade">
                                                {% if (file.error) { %}
                                                    <td class="name"><span>{%=file.name%}</span></td>
                                                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                                                {% } else { %}
                                                    
                                                    <td class="name">
                                                        <a id="fileuploaded" href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                                                
                                                    </td>
                                                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                    <td colspan="2"></td>
                                                    
                                                {% } %}
                                                <td class="delete" style="position:absolute; border:none">
                                                    <input type="hidden" name='destination' value='{%=file.path%}'>
                                                    <input type="hidden" name='video_mime' value='{%=file.video_mime%}'>
                                                    <input type="hidden" id="video_size" name='video_size' value='{%=file.video_size%}'>
                                                    <input type="hidden" id="video_width" name='video_width' value='{%=file.video_width%}'>
                                                    <input type="hidden" id="video_height" name='video_height' value='{%=file.video_height%}'>
                                                    <input type="hidden" id="video_duration" name='video_duration' value='{%=file.video_duration%}'>
                                                    <button style="background:none; border:none; color:#aeaeae" id="deletefile" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                        <i class="icon-trash icon-white"></i>
                                                        <span>Replace</span>
                                                    </button>
                                                </td>
                                            </tr>
                                        {% } %}
                                    </script>
                                    
                                </div>
                            </div>

                        <?php include(__DIR__.'/feed_template.php'); ?>

                        <div class="span6" >
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="border-style:none !important; width: 400px; height: 300px;">
                                    <input type="hidden" id="video_img_value" name="video_img_value" value="">
                                   
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" id="video_custom_img" style="max-width: 400px; max-height: 300px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file">
                                        <span class="fileupload-new">Select image</span>
                                        <span class="fileupload-exists">Change</span>

                                        <input style="float:right; width:200px;" type="file" name="image" class="default" id="videoPhotoUpload" /> 
                                    </span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                   <br>
                                   <font size="1" face="arial, helvetica, sans-serif"> Thumbnail automatically created if left blank!
                                    </font><br>
                                </div>
                             </div>
                             <div class="control-group">
                                
                                <div class="customarea">
                                <?php  echo form_textarea("product_widget",$post['product_widget'],'class="large m-wrap" id="product_widget" maxlength="5000" placeholder="Product_wiget" style="height:400px; margin-left:40px;"');?>
                                </div>
                            </div>
                        </div>
                    </div><!-- end of row-fluid -->
                    <div class="form-actions">
                        <?php echo form_submit('submit', "Share",' id="submit_now", class="btn blue"');?>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                     
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->

              </div>
        </div>
    </div>
</div>


    <div id="spinner" class="spinner" style="display:none;">
            <img id="img-spinner" src="<?php echo ASSETS_DIR; ?>/img/loader2.gif" alt="Loading"/>
    </div>

    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>
</script>