 <?php

 $user_id = $this->session->userdata('user_id');
 $selfuser = $this->ion_auth->user($user_id)->row();

?>
  <!-- BEGIN PAGE -->  
<div class="page-content">
     <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
           <div class="span12">
              <h3 class="page-title">
                 Edit Channel
              </h3>
              <!--<ul class="breadcrumb">
                <li><a href="<?php echo site_url("question/question_list"); ?>">Question</a></li>
                <li>&raquo;</li>
                <li><a href="<?php echo site_url("question/question_edit/".$question[0]->qid); ?>">Edit Question</a></li>
              </ul>-->
           </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
           <div class="span12">
<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Edit Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    
                    <?php 
                        echo form_open_multipart("dashboard/chanel_edit/".$chanel['id'], 'class="form-horizontal"');
                            echo form_error("chanel_name");
                            echo form_error("chanel_admin");
                            echo form_error("chanel_password");
                            echo form_error("chanel_confirm_password");
                            if(isset($show_errors)) {
                                if (is_array($show_errors)) {
                                    foreach($show_errors as $error) {
                                        echo "<div class='alert alert-error'>".$error."</div>";
                                    }
                                }
                                else{
                                    echo "<div class='alert alert-error'>".$show_errors."</div>";
                                }
                            }
                    ?>
                    <?php if(isset($success_message)): ?> 
                        <div class="alert alert-block alert-success fade in"><?php echo $success_message; ?></div>
                    <?php endif; ?>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Name</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_name",$chanel['name'],'class="large m-wrap"');?>
                                </div>
                            </div>
                        </div>
                    </div>
									
					
                      
                    <div class="form-actions">
                        <?php echo form_submit('submit', "Save",' class="btn blue"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('delimg', "Delete image",' class="btn blue"');?>
                    </div>

                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    <!-- END PAGE CONTENT--> 
    
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 

