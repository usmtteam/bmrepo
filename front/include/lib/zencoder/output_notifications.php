<?php

require_once(__DIR__.'/../../../config/settings.php');
require_once(__DIR__.'/../front/FrontCoreSql.php');
require_once(__DIR__.'/../front/FrontCoreManager.php');
define('ZENCODER_API_KEY','b34cc8e8b92ae58bf3f1a151608a0ef6');
define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');

try {

    spl_autoload_unregister('coreAutoload');
    require(ZENCODER_CLASS);

    // Initialize the Services_Zencoder class
    $zencoder = new Services_Zencoder(ZENCODER_API_KEY);

//    $zencoder_notification = new Services_Zencoder_Notification();

    // Catch notification
    $notification = $zencoder->notifications->parseIncoming();
//    $incoming_data = json_decode(trim(file_get_contents('php://input')));
//    $zencoder_notification = json_decode(trim(file_get_contents('php://input')));

    file_put_contents("notifications.txt", $notification, FILE_APPEND);

    // Check output/job state
    if($notification->job->outputs[0]->state == "finished") {
        echo "w00t!\n";

        error_log("w00t!\n");

        // If you're encoding to multiple outputs and only care when all of the outputs are finished
        // you can check if the entire job is finished.
        if($notification->job->state == "finished") {
            echo "Dubble w00t!";

            error_log("Dubble w00t!");
        }
    } elseif ($notification->job->outputs[0]->state == "cancelled") {
    //    echo "Cancelled!\n";
        error_log("Cancelled!\n");
    } else {
        echo "Fail!\n";
        echo $notification->job->outputs[0]->error_message."\n";
        echo $notification->job->outputs[0]->error_link;

        error_log("Fail!\n");
        error_log($notification->job->outputs[0]->error_message."\n");
        error_log($notification->job->outputs[0]->error_link);
    }
} catch (Services_Zencoder_Exception $e) {
    // If were here, an error occured


    echo "Fail :(\n\n";
    echo "Errors:\n";
    foreach ($e->getErrors() as $error) echo $error."\n";
    echo "Full exception dump:\n\n";
    print_r($e);
}

spl_autoload_register('coreAutoload');

?>