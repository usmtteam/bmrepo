<?


	class Channel {


		function __construct($p_xObj){

			if($p_xObj){
				foreach($p_xObj as $k => $v){
					$this->$k = $v;
				}
			}
		}


		public function getBitlyURLForAssetIdAndType($p_nId,$p_sAssetType,$p_sPageType){
			$l_sAppend = '';
			switch ($p_sPageType) {
				case 'TWITTER': $l_sAppend = '?page_type=twitter'; break;
				case 'LANDING_PAGE': $l_sAppend = '?page_type=landingpage'; break;
				case 'INLINE': $l_sAppend = '?page_type=inline'; break;
				default:break;
			}
			$l_sURL = 'http://'.$this->url.'.on.starsite.com/content/'.$p_sAssetType.'/'.$p_nId.'/'.$l_sAppend;
			echo "\nBITLY: $l_sURL\n";
			return Utils::getBitlyURL($l_sURL);
		}

		public function getBitlyURLForAssetIdAndTypeOf($p_nId,$p_sAssetType,$p_sPageType){
			$l_sAppend = '';
			switch ($p_sPageType) {
				case 'TWITTER': $l_sAppend = '?page_type=twitter'; break;
				case 'LANDING_PAGE': $l_sAppend = '?page_type=landingpage'; break;
				case 'INLINE': $l_sAppend = '?page_type=inline'; break;
				default:break;
			}
			$l_sURL = 'http://'.$this->url.'.on.oflo.co/ocontent/'.$p_sAssetType.'/'.$p_nId.'/'.$l_sAppend;
			echo "\nBITLY: $l_sURL\n";
			return Utils::getBitlyURL($l_sURL);
		}

		public function getProfileImage(){
			return CDN_FRONT.'/'.$this->slug.'/assets/ufile/'.$this->image_path;
			
		}


	}

?>