
                          <div class="control-group">
<!-- 
                              <div class="checkbox-list">
                                <input type="checkbox" id="manual_time" name="manual_time" value="1" /><span>Set new post time</span>
                                
                                <div id="timestamp_div" style="display:none;" class="input-append">
                                  <input type="text" id="time_update" name="time_update" value="<?php 
                                        date_default_timezone_set('America/Los_Angeles');
                                        
                                        echo $post['start_date'] ?>" readonly />
                                  <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                                </div>
                                <script language="javascript">
                    $(document).ready(function() {
                      $('#timestamp_div').datetimepicker({
                            format: 'yyyy-MM-dd hh:mm:ss'
                         });
                      $('#manual_time').change(function(e) {
                        $('#timestamp_div').toggle(this.checked);
                      });
                    });
                                </script>
                              </div><br> -->
  
                                 <?php
                            // Only admin will see checkboxes
                            if($this->session->userdata('username')=="administrator"){ ?>
                                <div class="control-group">
                                    <div class="checkbox-list">

                                        <label>
                                            <input type="checkbox" name="is_oflo" <?php if($post['is_oflo'] != 'n') echo "checked='true'"; ?>   >
                                            <span>Published</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" name="is_oflo_slider" id="is_oflo_slider" <?php if($post['is_oflo_slider'] != 'n') echo "checked='true'"; ?>   >
                                            <span>Slider</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" name="is_premium" id="is_premium" <?php if($post['is_premium'] != 0){echo 'checked';} ?>><span>
                                            Premium</span>
                                        </label>
                                      

                                    </div>
                                </div>

                            <div class="span6" style="margin:0px;" >

                        <div class="slider_thumbs" id="slider_thumbs">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="gray fileupload-new thumbnail" style="border-style: none !important; width: 324px; height: 100px;">
                                        <input type="hidden" id="slider_thumb" name="slider_thumb" value="">
                                       <a href="<?php echo UPLOAD_CLOUD.$post['slider_thumb']; ?>"><img id='slider_thumb' style="max-height: 100%" src="<?php echo ((isset($post['slider_thumb']) and $post['slider_thumb']) ? UPLOAD_CLOUD.$post['slider_thumb'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" /></a>

                                    </div>
                                     <div class="fileupload-preview fileupload-exists thumbnail" id="slider_thumb_preview" style="max-width: 324px; max-height: 100px; line-height: 20px;"></div>
                                    <div>
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select Slider Banner</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input style="float:right; width:200px;" type="file" name="image_slider" class="default" id="videoPhotoUpload" /> 
                                        </span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                       <br>
                                    </div>
                                 </div>
                            </div>
                          </div>

                            <?php } ?>

                         </div>

            <div class="control-group">

            <div class="span12" style="margin-bottom:15px;">
         
                <label>Category:</label>
                 <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalupload">Select Categories</button>

            </div>
                    <!-- Modal -->
                    <div id="myModalupload" class="modal fade" role="dialog" style="background-color:#000020">
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Choose Categories</h4>
                          </div>
                          <div class="modal-body">

                            <table>
                                <tr>


                                   <?php
                                        $i=0;
                                        foreach($viral_media_category as $category){
                                        $i++;
                                    ?>
                                    <td>
                                        <div style="display: inline-block; vertical-align: middle; text-align:center;">
                                            <?php echo $category['cat_name']; ?>
                                            <input style="text-align: center; vertical-align: middle; width: 60px;" type='checkbox' name='<?php echo $category['id'];?>'
                                                <?php foreach($viral_media_cat_post as $cat_post){
                                                            if ($post_id == $cat_post['post_id']){
                                                                if ($category['id'] == $cat_post['cat_id']){
                                                                        echo checked;
                                                                        break;
                                                                }
                                                            }elseif($post_id != $cat_post['post_id']){
                                                                if($post['copy_origin_id'] == $cat_post['post_id']){
                                                                    if ($category['id'] == $cat_post['cat_id']){
                                                                        echo checked;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                ?>
                                            >

                                        </div>
                                    </td>
                                    <?php if($i==6){echo'</tr><tr>'; $i=0;}} ?>

                                </tr>
                            </table>
                          </div>

                          <div class="modal-footer">
                            <?php echo form_submit('save', "Save", 'class="btn blue" data-dismiss="modal"');?>
                        </div>
                        </div>

                      </div>
                    </div>

        </div>

          <?php if($this->session->userdata('username')=="administrator"){ ?>
                          <div class="priority">

                        <input id="priorityCheckBox" name="priorityCheckBox" type="checkbox"><span>Is this a Priority Post? </span></input>


<!--                             <input id="priorityCheckBox"  name="priorityCheckBox" type="checkbox" <?php if($priority[0]['is_priority'] == 'y') echo "checked" ?>><span>Is This a Priority Post? </span></input>
 -->
                            <div class="priorityChecked" <?php if($priority[0]['is_priority'] == 'y') echo "style='display:block'" ; ?> >

                                <select id="prioritySelected" name="prioritySelected">
                                <option value="0">Priority Level</option>


                                <?php
                                    for ($prioritySelected = 1; $prioritySelected <= 20; $prioritySelected++) {
                                ?>
                                        <option value='<?php echo $prioritySelected ?>' <?php if ($prioritySelected == $priority[0]['priority_number']){ echo "selected"; } ?>><?=$prioritySelected.' priority' ?></option>
                                <?php    }
                                ?>

                                 </select>
                            </div>

                            <div class="priorityDay" <?php if($priority[0]['is_priority'] == 'y') echo "style='display:block'" ; ?>>

                                <select id="priorityDaySelected" name="priorityDaySelected">
                                <option value="0">Days As Priority</option>


                                <?php
                                    for ($priorityDaySelected = 1; $priorityDaySelected <= 30; $priorityDaySelected+=5) {
                                ?>
                                        <option value='<?php echo $priorityDaySelected ?>' <?php if ($priorityDaySelected == $priority[0]['duration']){ echo "selected"; } ?>><?=$priorityDaySelected.' days'?></option>


                                <?php    }
                                ?>

                                 </select>
                            </div>
                          </div>

              
                              <br>

                             <div class="checkbox-list">

                                            <input type="checkbox" name="is_feed" id="is_feed" <?php if($post['is_feed'] != 'n') echo "checked='true'"; ?>   ><span>Add to Feed</span>



                                <div id="timestamp2_div" style="display:none;" class="input-append">
                                  <input type="text" id="feed_update" name="feed_update" value="<?php echo $post['feed_date'] ?>" />
                                  <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                                </div>
                             </div>
                                <script language="javascript">
                    $(document).ready(function() {
                      $('#timestamp2_div').datetimepicker({
                            format: 'yyyy-MM-dd hh:mm:ss'
                         });
                      $('#is_feed').change(function(e) {
                        $('#timestamp2_div').toggle(this.checked);
                      });
                    });
                                </script>
                        <?php } ?>
                        <br>
                                    <br></br>

                            <div class="control-group">
                                <div class="span12" style="margin-bottom:15px;">
                                    <label>Syndication:</label>
                                </div>

                               <!--  <div class="row-fluid"> -->
                                    <div class="span3">
                                        <input type="hidden" id="post_to_facebook" name="post_to_facebook" value="false"  />
                                        <a class="fb ssfont facebook-new-cms float-left">f</a>
                                        <span class="social-title">Facebook</span>
                                    </div>
                              <!--   </div> -->
                               <!--  <div class="row-fluid"> -->
                                    <div class="span3">
                                        <input type="hidden" id="post_to_twitter" name="post_to_twitter" value=""  />
                                        <a class="tw ssfont twitter-new-cms float-left">t</a>
                                        <span class="social-title">Twitter</span>
                                    </div>
                              <!--   </div> -->
                                <? /*<div class="row-fluid">
                                    <div class="span3">
                                        <input type="hidden" id="post_to_tumblr" name="post_to_tumblr" value=""  />
                                        <a class="tb ssfont tumblr-new-cms float-left">u</a><span class="social-title">Tumblr</span>
                                    </div>
                                </div>
                                  <div class="row-fluid">
                                <div class="span8">
                                <input type="hidden" id="post_to_gp" name="post_to_gp" value=""  />
                                <div class="icon-twitter3 gp-new-cms float-left"></div><span class="social-title">Google +</span>
                                </div>
                                </div> */ ?>
                           </div>

                           <div class="control-group">
                                <div class="span8">
                                <label>Reports</label>
                                </div>
                           </div>
                            <div class="control-group" style="display:none">

                                <div class="span4">
                                    <font size="1" face="arial, helvetica, sans-serif"> Start Date
                                    </font>
                                    <br>
                                    <div id="start_date_div" class="input-append">
                                        <input type="text" id="start_date_report" name="start_date_report" value="<?php echo $post['start_date_report']; ?>" readonly />
<!--                                        <span class="add-on">-->
<!--                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>-->
<!--                                        </span>-->
                                    </div>
                                </div>
                                <div class="span4">
                                    <font size="1" face="arial, helvetica, sans-serif"> End Date
                                    </font>
                                    <br>
                                    <div id="end_date_div" class="input-append">
                                        <input type="text" id="end_date_report" name="end_date_report" value="<?php echo $post['end_date_report']; ?>" readonly />
<!--                                        <span class="add-on">-->
<!--                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>-->
<!--                                        </span>-->
                                    </div>
                                </div>
                            </div>

<!--                          --><?// if($feed['page_views_ga']<1){ ?>
                          <? if($feed['total_views']<1){ ?>
                              <div class="row-fluid control-group">
                                  <font style="color:#95999b; font-size:20px; text-align:center;font-weight:300"></font>
                              </div>
                          <? }else { ?>
                              <div>

                                  <div class="row-fluid control-group">
                                      <div class="span3 " style="text-align:center;"><font style="color:#39f4ca; font-size:30px; text-align:center;font-weight:300;">$<?=number_format(round($feed['revenue'],0),0);?></font></div>
<!--                                      <div class="span3" style="text-align:center;"><font style="color:#c8636d; font-size:30px; text-align:center;font-weight:300;">--><?//=number_format($feed['page_views_ga']);?><!--</font></div>-->
                                      <div class="span3" style="text-align:center;"><font style="color:#c8636d; font-size:30px; text-align:center;font-weight:300;"><?=number_format($feed['total_views']);?></font></div>
                                  </div>
                                  <div class="row-fluid control-group">
                                      <div class="span3" style="text-align:center;"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Revenue</font></div>
                                      <div class="span3" style="text-align:center;"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Views</font></div>
                                  </div>
                              </div>
                          <? } ?>

                            <script language="javascript">
                                $(document).ready(function() {
                                    // $('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});

                                    // $('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});


                                    $('.facebook-new-cms').click(function() {
                                        $('.facebook-new-cms').toggleClass('facebook-active');
                                        var hiddenField = $('#post_to_facebook'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.twitter-new-cms').click(function() {
                                        $('.twitter-new-cms').toggleClass('twitter-active');
                                        var hiddenField = $('#post_to_twitter'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.tumblr-new-cms').click(function() {
                                        $('.tumblr-new-cms').toggleClass('tumblr-active');
                                        var hiddenField = $('#post_to_tumblr'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.gp-new-cms').click(function() {
                                        $('.gp-new-cms').toggleClass('gp-active');
                                        var hiddenField = $('#post_to_gp'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('#priorityCheckBox').click(function() {
                                        $(".priorityChecked").toggle(this.checked);
                                        $(".priorityDay").toggle(this.checked);
                                    });

                                    $('#scheduleCheckBox').click(function() {
                                        $(".priorityDates").toggle(this.checked);
                                    });


                                        $('#is_oflo_slider').click(function() {
                                        $(".slider_thumbs").toggle(this.checked);
                                    });

                                    $('#submit_now').click(function() {
                                        $('#spinner').show();
                                        $('#submit_now').hide();
                                        $('#refresh_now').hide();

                                    });
                                });
                            </script>

                        </div>

         
                        