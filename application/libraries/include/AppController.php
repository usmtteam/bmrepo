<?


class AppController{

    private static  $_xInstance = null;
    private $_bConnected = false;
    private $_xDBConnection  = null;
    private $_xChannel = null;


    public static function getInstance() {
        if (!self::$_xInstance){
            self::$_xInstance = new AppController();
        }
        return self::$_xInstance;
    }

    public function getConnection(){
        if(!$this->_xDBConnection){
            $this->_xDBConnection = new FrontCoreSql();
        }
        return $this->_xDBConnection;
    }

    public function setChannel($p_xChannel){
        $this->_xChannel = $p_xChannel;
    }

    public function getChannel($p_xChannel){
        return $this->_xChannel;
    }


    public function outputComments($p_aComments){
        $j=0;
        foreach ($p_aComments as $l_xComment) { ?>

            <li class="comment_item <? if($j<5){ echo 'active';} ?>">
                <div class="comment_info">
                    <div class="comment_name_time">

                        <img src="https://graph.facebook.com/<?= $l_xComment['facebook_page_id'];?>/picture?type=square" class="com_img com_img_new <? if($l_xComment['facebook_page_id']!="") { echo 'is-visible'; }else{ echo 'hidden'; } ?>">
                        <img src="http://scmedia.starsite.com/default_avatar.png" class="com_img com_img_default <? if($l_xComment['facebook_page_id']=="") { echo 'is-visible'; }else{ echo 'hidden'; } ?>">
                        <div class="comment_name"><a href="#"><?= $l_xComment['name']; ?></a></div>
                        <div class="comment_time">
                            <?= Utils::time_passed($l_xComment['time_stamp']) ?>
                        </div>
                        <div class="comment_content"><?= $l_xComment['comment']; ?></div>
                    </div>

                </div>
                <div class="clear"></div>
            </li>
            <?
            $j++;
        }
    }

    public function outputVideoListItems($p_aBCVideos){
        $i=0;
        foreach ($p_aBCVideos as $l_xVideo) {
            ?>
            <li class="a-load-video <? if($i<5){ echo 'active';} ?>" rel="<?= $l_xVideo->id; ?>"> <span class="thumb_video_list"> <div class="vid_thumb" style="background:url(<?= $l_xVideo->videoStillURL ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div> <!-- insted of image insert video tag --> 
              </span>
                <p><strong><?= substr($l_xVideo->shortDescription, 0, 60); ?> </strong> <span>
                <?= Utils::get_time_ago($l_xVideo->publishedDate); ?>
                </span></p>
                <div class="clear"></div>
            </li>
            <?
            $i++;
        }
    }

    public function outputVideoListItemsFlowPlayer($l_aFPVideos, $l_xChannel){
        ?>
        <div class="fp-playlist">
            <?
            $i=0;

            foreach ($l_aFPVideos as $l_xVideo) {
                ?>
                <a href="/content/video/<?= $l_xVideo['id'] ?>/" class="<? if($i<5){ echo 'active';} ?> fp-list"> <span class="thumb_video_list"> <div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug; ?>/assets/ufile/<?= $l_xVideo['image_path'] ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div> <!-- insted of image insert video tag -->
              </span>
                    <p><strong><?= substr($l_xVideo['description'], 0, 60); ?> </strong> <span>
                <?= Utils::time_passed($l_xVideo['time_stamp']); ?>
                </span></p>
                    <div class="clear"></div>
                </a>
                <?
                $i++;
            }
            ?>
        </div>
    <?
    }

    /**
     * Get Video list items for Flow Player mobile.
     *
     * @param $l_aFPVideos
     * @param $l_xChannel
     */
    public function outputVideoListItemsFlowPlayerMobile($l_aFPVideos, $l_xChannel){
        ?>
        <div class="fp-playlist">
            <?
            $i=0;

            foreach ($l_aFPVideos as $l_xVideo) {
                ?>
                <li>
                    <a href="/content/video/<?= $l_xVideo['id'] ?>/" class="<? if($i<5){ echo 'active';} ?> fp-list">

                        <div class="thumb">
                            <img class="backup_picture" src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug; ?>/assets/ufile/<?= $l_xVideo['image_path'] ?>" width="150">
                        </div>

                        <p>
                            <?= substr($l_xVideo['description'], 0, 60); ?><br>
                            <span><?= Utils::time_passed($l_xVideo['time_stamp']); ?></span>
                        </p>
                        <div class="clr"></div>
                    </a>
                </li>
                <?
                $i++;
            }
            ?>
        </div>
    <?
    }

    /**
     * Outout O-Flo video list items for Flow player.
     *
     * @param $l_aFPVideos
     * @param $l_xChannel
     */
    public function outputOfloVideoListItemsFlowPlayer($l_aFPVideos, $l_xChannel){
        ?>
        <div class="fp-playlist">
            <?
            $i=0;

            foreach ($l_aFPVideos as $l_xVideo) {
                ?>
                <a href="/ocontent/video/<?= $l_xVideo['id'] ?>/" class="<? if($i<5){ echo 'active';} ?> fp-list">
                    <span class="thumb_video_list">
                        <div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xVideo['url']; ?>/assets/ufile/<?= $l_xVideo['image_path'] ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div> <!-- insted of image insert video tag -->
                    </span>
                    <p>
                        <strong><?= substr($l_xVideo['description'], 0, 60); ?> </strong>
                        <span><?= Utils::time_passed($l_xVideo['start_date_timestamp']); ?></span>
                    </p>
                    <div class="clear"></div>
                </a>
                <?
                $i++;
            }
            ?>
        </div>
    <?
    }

    /**
     * Get Video list items for O-Flo Flow Player mobile.
     *
     * @param $l_aFPVideos
     * @param $l_xChannel
     */
    public function outputOfloVideoListItemsFlowPlayerMobile($l_aFPVideos, $l_xChannel){
        ?>
        <div class="fp-playlist">
            <?
            $i=0;

            foreach ($l_aFPVideos as $l_xVideo) {
                // var_dump($l_xVideo); 
                ?>
                <li>

                    <a href="/ocontent/video/<?= $l_xVideo['id'] ?>/" class="<? if($i<5){ echo 'active';} ?> fp-list">
                        <div <?php if ($l_xVideo['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE ||$l_xVideo['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM ){ echo "class='vid_thumb_desc_embeded'";}else{echo "class='vid_thumb_desc'";} ?>>

                            <img <?php if ($l_xVideo['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE ||$l_xVideo['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM ){ echo "class='vid_thumb_embeded backup_picture''";}else{echo "class='vid_thumb backup_picture'";} ?> src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xVideo['url']; ?>/assets/ufile/small_thumb/<?= $l_xVideo['image_path'] ?>">
                        </div>
                        <p class="vid_desc">
                            <?= substr($l_xVideo['description'], 0, 60); ?><br>
                            <span><?= Utils::time_passed($l_xVideo['start_date_timestamp']); ?></span><br>
                            <span><?php echo $l_xVideo['total_views'].' views'?></span>
                        </p>
                        <div class="clr"></div>
                    </a>
                </li>
                <?
                $i++;
            }
           ?>
        </div>
    <?
    }
}

?>