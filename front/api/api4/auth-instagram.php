<html>
<style type="text/css">
	body{background:#000; color:#fff; font-size:18px; font-family:'Arial'}

</style>
<body>
Loading...
<?

	require(__DIR__.'/../../config/settings-4.php');
	
	if($_GET['error']){
		//Error, return to App
		?>
		<script type="text/javascript">
			document.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=<?= INSTAGRAM_CLIENT_ID ?>&redirect_uri=<?= INSTAGRAM_REDIRECT ?>&response_type=code';
		</script>
		<?
	}else if($_GET['code']){
		
		$l_sInstagramCode = $_GET['code'];
		$l_sInstagramBid = $_SESSION['bid'];
		var_dump($l_sInstagramBid);

		$l_sURL = "https://api.instagram.com/oauth/access_token";
		
		$l_aParams = array(
			'client_id'                =>     INSTAGRAM_CLIENT_ID,
			'client_secret'            =>     INSTAGRAM_CLIENT_SECRET,
			'grant_type'               =>     'authorization_code',
			'redirect_uri'             =>     INSTAGRAM_REDIRECT,
			'code'                     =>     $l_sInstagramCode
		);

		
		$curl = curl_init($l_sURL);    
		curl_setopt($curl,CURLOPT_POST,true);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$l_aParams);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$l_xResult = curl_exec($curl);
		curl_close($curl);

		if($l_xResult){
			$l_aData = json_decode($l_xResult,true);			
			// print_R($l_aData);
		}

		$l_sPageId = $l_aData['user']['id'];
		if(!$l_sPageId){
			?>
			<script type="text/javascript">
				document.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=<?= INSTAGRAM_CLIENT_ID ?>&redirect_uri=<?= INSTAGRAM_REDIRECT ?>&response_type=code';
			</script>
			<?
			exit;
		}
		$l_sPageName = $l_aData['user']['username'];
		$l_sCategory = 'instagram';
		$l_sToken = $l_aData['access_token'];

		
		$l_xSql = AppController::getInstance()->getConnection();

		$l_nReturnId = 0;
 
		$l_sSql = 'SELECT * FROM enr_channel_oauth WHERE social_property_type_id = '.SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID.' AND page_id = '.$l_sPageId;
		$l_aExisting = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
		// print_r($l_aExisting);
		if($l_aExisting){

			$l_nReturnId = $l_aExisting['id'];
			$l_sSql = 'UPDATE enr_channel_oauth SET 
					page_name = "'.$l_xSql->cleanValue($l_sPageName).'",
					social_token = "'.$l_xSql->cleanValue($l_sToken).'",
					modified = UTC_TIMESTAMP()
					WHERE id = '.$l_nReturnId;
			$l_xSql->runQuery($l_sSql);
		}else{

			$l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,page_category,social_token,created,active)
				VALUES (0,'.SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID.',
					"'.$l_xSql->cleanValue($l_sPageId).'",
					"'.$l_xSql->cleanValue($l_sPageName).'",
					"'.$l_xSql->cleanValue($l_sCategory).'",
					"'.$l_xSql->cleanValue($l_sToken).'",
					UTC_TIMESTAMP(),
					"Y"
					)';
			
			$l_xSql->runQuery($l_sSql);
			$l_nReturnId = $l_xSql->db_last_insert_id();
		}

		// echo "inset $l_nReturnId";
		?>
		<script type="text/javascript">
			document.location.href = 'com.starclub.'.$l_sInstagramBid.'://instagram-pair/<?= $l_nReturnId ?>';
		</script>
		<?
	}else{
		?>
		<script type="text/javascript">
				document.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=<?= INSTAGRAM_CLIENT_ID ?>&redirect_uri=<?= INSTAGRAM_REDIRECT ?>&response_type=code';
			</script>
			<?
	}
?>
</body>
</html>