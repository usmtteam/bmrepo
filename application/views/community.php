<!-- BEGIN PAGE -->
<input type="Hidden" id="page" value="<?php echo $page; ?>" />
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Community
                    <small></small>
                    <!--<div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('content/feed/text'); ?>" class="btn green">
                        New Post 
                        </a>
                    </div>-->
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
				<center>
				Search : <input type="text" name="search" id="search" value="<?php echo $search;?>" />
				</center>
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Post Type</th>
                            <th class="feed-thumbnail-noborder"></th>
                            <th>Caption</th>
                            <th>Tags</th>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Published</th>
                            <th>Sharing</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>     
                    <?php 
                    foreach($feeds as $feed): 
                        $ids = explode("_", $feed['id']);
                    $post_controller_name = $ids[0];   
                    ?> 
                        <tr>
                            <td class="feed-thumbnail-td ">
                            <?php if($feed['image_path']){ 
							
                            echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="'.$feed['image_path'].'"></a><br/>';
                            }          ?>
                            </td>
                            <td class="feed-thumbnail-noborder"><?php echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">'.$feed['post_type'].'</a>' ?></td>
                            <td class="span2"><div style='word-wrap:break-word; width: 100px'><?php echo $feed['caption']; ?></div></td>
                            <td><?php echo $feed['tags']; ?></td>
                            <td><?php echo $feed['date']; ?></td>
                            <td><?php echo $feed['username']; ?></td>
                            <?php $permission = ($feed['is_publish']==2 ? "Draft" : ($feed['is_publish']==1 ? "Publish" : "Nothing")); ?>
                            <td><?php echo $permission ?></td>
                            <td class="center">
                            	<a href="#" onclick="SocialShareEngine.shareOnFacebook('<?php echo $feed['content_type']?>', '<?php echo $feed['content_id']; ?>', '<?php echo $social['facebook_page']; ?>'); "><img src="<?php echo ASSETS_DIR; ?>/img/icon_facebook.png" /></a>
                            	<a href="#" onclick="SocialShareEngine.shareOnTwitter('<?php echo $feed['content_type']?>', '<?php echo $feed['content_id']; ?>'); "><img src="<?php echo ASSETS_DIR; ?>/img/icon_twitter.png" /></a>
                            </td>
                            <td class="center">
                            <?php
                                $ids = explode("_", $feed['id']);
                                $post_controller_name = $ids[0];
                                echo 
                                '<a class="btn mini purple" href="'. base_url() . 'content/community/'.$post_controller_name.'_edit/'.$feed['id'].'">'.
                                '<i class="icon-edit"></i>Edit'.
                                '</a>';
                            ?>
                            </td>
                            <td class="center">
                            <?php
                                echo 
                                '<a class="btn mini black" href="javascript:confirm_del(\''.$feed['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <center>
                <div class="dataTables_paginate paging_bootstrap pagination"><ul>
                	<li class="prev<?php echo $page == 0 ? ' disabled' : ''?>"><a data-page="<?php echo max(0, $page - 1);?>">â†� Previous</a></li>
                	<?php $s = max(0, $page - 5); for($i = $s; $i < min($total_page_count, $s + $page_count); $i++) :?>
                	<li class="<?php echo $page == $i ? ' active' : ''; ?>"><a data-page="<?php echo $i;?>"><?php echo $i+1; ?></a></li>
                	<?php endfor; ?>
                	<li class="next<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>""><a data-page="<?php echo min($total_page_count-1, $page + 1);?>">Next â†’</a></li>
				</ul></div>
                </center>
                <script language="javascript">
                $(document).ready(function() {
                    $('.pagination a').click(function(e) {
	                    $('#page').val($(e.target).attr('data-page'));
	                    refresh();
                    });
                });
                </script>
            </div>
        </div>
    </div>
</div>
<script>
    function confirm_del(id) {
        if(confirm("Do you want to delete this feed?")) {
            document.location.href = "<?php echo site_url('content/delete_community'); ?>/" + id;
        }
    }    
    $(document).ready(function() {
    	$('#search').keydown(function(e) {
    		if(e.keyCode == 13) {
    			e.preventDefault();
                $('#page').val(0);
    			refresh();
    		}
    	});
    });
    function refresh() {

        var base_url = '<?php echo site_url('content/community'); ?>';
        var search_val = $('#search').val();
        var url = base_url + '/' + $('#page').val() + '/' + search_val;
        document.location.href = url;
        
    }
</script>