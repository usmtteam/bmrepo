
  <div class="control-group">
                                
            <div class="customarea">
            <?php  echo form_textarea("description",$post['description'],'class="large m-wrap" id="description" maxlength="5000" placeholder="Caption" style="height:80px;"');?>
            </div>
        </div>
        <div class="control-group">
               <div id="textarea_feedback"></div>

                <?php  echo form_input("tags",$post['tags'],'class="large m-wrap" maxlength="512" id="tag" placeholder="Tags"');?>

        </div>

<script type="text/javascript">

        $(document).ready(function() {
            
           
            var description_max = 5000;
             var tag_max = 512;

            $('#textarea_feedback').html(description_max + ' characters remaining');

            $('#description').keyup(function() {
                var text_length = $('#description').val().length;
                var text_remaining = description_max - text_length;


                $('#textarea_feedback').css("display", "block");
                $('#textarea_feedback').html(text_remaining + ' characters remaining');
            });

            $('#textarea_feedback').html(tag_max + ' characters remaining');

            $('#tag').keyup(function() {
                var text_length = $('#tag').val().length;
                var text_remaining = tag_max - text_length;

                $('#textarea_feedback').css("display", "block");
                $('#textarea_feedback').html(text_remaining + ' characters remaining');
            });
        });

</script>