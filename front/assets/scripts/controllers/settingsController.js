enriqueApp.controller('settingsController', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
	$scope.user = null;
	$scope.updating = false;
	$scope.error = '';
	
	$scope.loadData = function() {
		CommonLib.show_loading_bubble();
    	var url = $rootScope.get_api_url() + '&option=get_user&self_user_id=' + $rootScope.current_user.id;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.user = data.user;
        		if(!$rootScope.isDate($scope.user.birthday)) {
        			$scope.user.birthday = new Date();
        		}
        		$scope.$watch("user.enable_facebook", $scope.updateUser);
        		$scope.$watch("user.enable_twitter", $scope.updateUser);
        	}
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
	};
	
	$scope.updateUser = function() {
		if($scope.updating) return;
		$scope.updating = true;
		var target = $('#button_update');
		var text_fullname = $('#text_fullname');
		var text_email = $('#text_email');
		var text_password = $('#text_password');
		var text_password1 = $('#text_password1');
		var text_birthday = $('#text_birthday');
		var text_city = $('#text_city');
		var text_state = $('#text_state');
		var text_country = $('#text_country');
		
		
		var gender = -1;
		var radio_gender = $('input[name=text_gender]:checked');
		if(radio_gender.length > 0) gender = radio_gender.val();
		
		
		if(text_password.val() != text_password1.val()) {
			$scope.error = 'Confirm password does not match.';
		}
		else {
			CommonLib.show_waiting_circle(target, 1);
	    	var url = $rootScope.get_api_url() + '&option=update_user&username=' + text_fullname.val() + '&password=' + text_password.val() + '&birthday=' + text_birthday.val() + '&gender=' + gender + '&state='+text_state.val() + '&city=' + text_city.val() + '&country=' + text_country.val();
	    	url += '&enable_facebook=' + $scope.user.enable_facebook;
	    	url += '&enable_twitter=' + $scope.user.enable_twitter;
	    	$http({method: 'GET', url: url}).
	        success(function(data, status, headers, config) {
	        	if(data.status == true) {
	        		$scope.user = data.info;
	        		$rootScope.current_user = $scope.user;
	        		$rootScope.saveCookie();
	        	}
	        	else {
	        		$scope.error = data.message;
	        	}
	        	CommonLib.hide_waiting_circle();
	        	$scope.updating = false;
	        }).
	        error(function(data, status, headers, config) {
	        	$scope.updating = false;
	        	CommonLib.hide_waiting_circle();
	        	$scope.error = 'Please try again.';
	        });
		}
	};
	
	$scope.loadData();
}]);