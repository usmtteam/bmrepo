<
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Edit User


                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Edit User</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
               
                    <?php 
                    echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("name");
                         
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <div class="row-fluid">
                        <div class="control-group">

                    <form>
                        <?php  ?>
                  
                        
                          <label for="username">Username</label>
                         
                          
                         <div class="input-group">
                                <input type="text" name="username" id="username" value="<?=$username ?>"required><br>
                            </div>
                        
                    
                         <br><br>
             

                       
                          <label for="email">Email</label>
                             <div class="input-group">
                            <input type="text" name="email" id="email" value="<?=$email ?>"required><br>
                            </div>
                        
                           <br><br>

                      
                   

                     
                        <div class="form-control">
                            <label for="hub">User Group</label>
                            <input type="text" name="user_group_id" id="user_group_id" value="<?=$user_group_id ?>" required><br>
                        

                        </div>
                               <br><br>

              
        
                        
                  
                            
                            </div>
                        
                        </div>

                        </div>
                    </div>
                    <div class="form-actions">
                     <?php echo form_submit('submit', "submit",' class="btn blue"');?>

                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>