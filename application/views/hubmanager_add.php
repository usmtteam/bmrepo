
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Add Hub Form


                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Add Hub Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
               
                    <?php 
                    echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("name");
                         
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <div class="row-fluid">
                        <div class="control-group">

                    <form>

               
                        
                          <label for="name">Name</label>
                         
                          
                         <div class="input-group">
                                <input type="text" name="name" id="name" required><br>
                            </div>
                        
                    
                         <br><br>
             

                       
                          <label for="template_path">Template Path</label>
                             <div class="input-group">
                            <input type="text" name="template_path" id="template_path" required><br>
                            </div>
                        
                           <br><br>

                      
                   
                        <div class="form-control">
                            <label for="api_path">API Path</label>
                            <input type="text" name="api_path" id="api_path" required><br>
                        
                        </div>
                               <br><br>

                     
                        <div class="form-control">
                            <label for="front_style_path">Front Style Path</label>
                            <input type="text" name="front_style_path" id="front_style_path" required><br>
                        

                        </div>
                               <br><br>

                        <div class="form-control">
                            <label for="back_style_path">Back Style Path</label>
                            <input type="text" name="back_style_path" id="back_style_path" required><br>
                        
                        </div>
                               <br><br>

                   
                        <div class="form-control">
                            <label for="push_cert_path">Push Cert Path</label>
                            <input type="text" name="push_cert_path" id="push_cert_path" required><br>
                        
                        </div>
                               <br><br>

                       
                        <div class="form-control">
                            <label for="logo_backend">Logo Backend</label>
                            <input type="text" name="logo_backend" id="logo_backend" required><br>
                        
                        </div>
                     
              
        
                        
                  
                            
                            </div>
                        
                        </div>

                        </div>
                    </div>
                    <div class="form-actions">
                     <?php echo form_submit('submit', "submit",' class="btn blue"');?>

                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>


