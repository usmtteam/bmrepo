<!-- BEGIN PAGE -->
<div class="page-content">
    <?php 
        $this->load->view('header_feed');
    ?>
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box">
                    <div id="form_head_back">Edit Poll</div>
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("content/feed/poll_edit/".$post['feed_id'], 'class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("credit");
                        echo form_error("question0[0]");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <h4 class="alert-heading">Thumbnail</h4>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Caption</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("description",$post['description'],'class="large m-wrap"');?>
                                </div>
                            </div>
                        </div>    
                        <div class="control-group">
                                <label class="control-label" >Open</label>
                                <div class="controls fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img src="<?php echo ((isset($post['image_path']) and $post['image_path']) ? UPLOAD_URL.$post['image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="image1" class="default" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Opening Page</h4>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Text</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("text",$post['text'],'id="text" class="large m-wrap"');?>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Tags</label>
                                <div class="controls">
                                <?php  echo form_input("tags",$post['tags'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <?php if(!$is_community): ?>
                            <div class="control-group">
                                <label class="control-label" for="url">Credits</label>
                                <div class="controls">
                                <?php  echo form_input("credit",$post['credit'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="control-group">
                                <label class="control-label" for="url">Permission</label>
                                <div class="controls">
                                    <?php echo form_dropdown("is_publish", array('Not Published', 'Publish', 'Draft'), $post['is_publish'], 'class="large m-wrap"');?>
                                </div>
                            </div>
	                        <div class="control-group">
	                            <label class="control-label" for="url">Sticky Flag</label>
	                            <div class="controls">
	                            <?php  echo form_checkbox("is_sticky", 1, $post['is_sticky'],'class=""');?>
	                            </div>
	                        </div>
                            
	                        <div class="control-group">
	                            <label class="control-label" for="url">Post time</label>
	                            <div class="controls">
		                            <input type="checkbox" id="manual_time" name="manual_time" value="1" />
		                            Set new post time
		                            <div id="timestamp_div" style="display:none;" class="input-append">
		                            	<input type="text" id="time_stamp" name="time_stamp" value="<?php echo date('Y-m-d H:i:s'); ?>" />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
		                            <script language="javascript">
										$(document).ready(function() {
											$('#timestamp_div').datetimepicker({
										        format: 'yyyy-MM-dd hh:mm:ss'
										     });
											$('#manual_time').change(function(e) {
												$('#timestamp_div').toggle(this.checked);
											});
										});
		                            </script>
	                            </div>
	                        </div>
	                        
                           
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Start date</label>
                                <div class="controls">
                                	<div id="start_date_div" class="input-append">
		                            	<input type="text" id="start_date" name="start_date" value="<?php echo $post['start_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="url">End date</label>
                                <div class="controls">
                                	<div id="end_date_div" class="input-append">
		                            	<input type="text" id="end_date" name="end_date" value="<?php echo $post['end_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            <script language="javascript">
								$(document).ready(function() {
									$('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
									$('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
								});
							</script>
							
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(Web app)</label>
                                <div class="controls">
                                	<a href="<?php echo get_web_app_link($chanel_info, 'poll', $post['id']); ?>" target="_blank">
                                	<?php echo get_web_app_link($chanel_info, 'poll', $post['id']); ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(iOS, Android app)</label>
                                <div class="controls">
                                	<a href="<?php echo get_mobile_app_link($chanel_info, 'poll', $post['id']); ?>" target="_blank">
                                	<?php echo get_mobile_app_link($chanel_info, 'poll', $post['id']); ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(bit.ly)</label>
                                <div class="controls">
                                	<a href="<?php echo $post['deep_link_web']; ?>" target="_blank">
                                	<?php echo $post['deep_link_web']; ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL Twitter(bit.ly)</label>
                                <div class="controls">
                                    <a href="<?php echo $post['deep_link_web_tw']; ?>" target="_blank">
                                    <?php echo $post['deep_link_web_tw']; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" >Background</label>
                                <div class="controls fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img src="<?php echo ((isset($post['background_image_path']) and $post['background_image_path']) ? UPLOAD_URL.$post['background_image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="background_image" class="default" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Questions</h4>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                               <div class="controls customarea">
                                 <h5 style="font-weight: bold;">Question</h5>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <table class="table answer">
                                <tr>
                                    <th>Answers</th>
                                    <!--<th width='90px'>True / False</th>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id='question_container'>
                        <input type="hidden" id='qcount' value="<?php echo count($pollQuestions); ?>">
                        <?php foreach($pollQuestions as $key => $pollQuestion): ?>
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                       <div class="controls customarea">
                                        <?php  echo form_textarea("question[{$key}]",$pollQuestion['question_name'],'class="large m-wrap"');?>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <table class="table answer">
                                        <?php for($akey=0; $akey<8;  $akey++){
                                             if(isset($pollQuestion['answer'][$akey])){
                                                 $answer = $pollQuestion['answer'][$akey];
                                             }else{
                                                 $answer = array();
                                                 $answer['name'] = '';
                                                 $answer['is_correct'] = 0;
                                             }
                                        ?>
                                        <tr>
                                            <td>
                                                <?php  echo form_input("answer[{$key}][]",$answer['name'],'class="large m-wrap"');?>
                                            </td>
                                            <!--<td class="center" width='90px'>
                                                <span><input type="checkbox" name='<?php echo "is_correct_{$key}_{$akey}"; ?>' <?php if($answer['is_correct']) echo "checked='true'"; ?> ></span>
                                            </td>-->
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="control-group">                    
                        <span class="add-more" onclick="return addMoreQuestion();">Add More</span>
                    </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Closing Page</h4>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Caption</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("end_description",$post['end_description'],'id = "end_description" class="large m-wrap"');?>
                                
                                <script language="javascript">
									bkLib.onDomLoaded(function() {
										new nicEditor({fullPanel : true, iconsPath : '<?php echo base_url(); ?>nicEditorIcons.gif'}).panelInstance('end_description');
									});
								</script>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                   <img src="<?php echo ((isset($post['end_image_path']) and $post['end_image_path']) ? UPLOAD_URL.$post['end_image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image2" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                             </div>
                        </div>
                    </div>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Post",' class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
                </div>
              <!-- END SAMPLE FORM PORTLET-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box">
                                <div id="form_head_back">Fans Information for this Quiz</div>
                            <div class="portlet-body pb-feed">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th ></th>
                                            <th>Username</th>
                                            <th>E-Mail</th>
                                            <th class="">Last login</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th class="">Anwer Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($fans as $user):?>

                                        <tr class="odd gradeX">
                                            <td class="photo-thumbnail-td ">
                                                <img height="50" width="50" class='photo-thumbnail pull-right' src="<?php if(is_file(UPLOAD_DIR.$user['img_url'])){ echo UPLOAD_URL.$user['img_url']; }else{ echo UPLOAD_URL."photo/no-image-50.png"; } ?>">
                                            </td>
                                            <!--<td ><a href='<?php echo site_url('fan/view')."/".$user['id']; ?>'><?php echo $user['name'];?></a></td>-->
                                            <td ><?php echo $user['name'];?></td>
                                            <td ><a href="mailto:<?php echo $user['email'];?>"><?php echo $user['email'];?></a></td>
                                            <td class="span2"><?php echo $user['last_login'];?></td>
                                            <td class="span2 center" >
                                                <?php echo $user['qName']; ?>
                                            </td>
                                            <td class="span2 center" align="center">
                                                <?php echo $user['aName']; ?>
                                            </td>
                                            <td class="span2"><?php echo $user['anwerdate'];?></td>
                                        </tr>
                                    <?php endforeach;?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                
                 
                    <div id="form_head_back">Comments</div>

                 <div class="portlet-body form pb-feed">
                 	<table class="table table-bordered table-hover bootstrap-datatable datatable">
		                    <thead>
		                        <tr>
		                            <th style="width:30px;">No</th>
		                            <th style="width:200px;">Username</th>
		                            <th>Comments</th>
		                            <th style="width:120px;">Date</th>
		                        </tr>
		                    </thead>
		                    <tbody> 
		                    <?php 
		                     $post_type = explode('_', $post['feed_id']);
		                     $post_type = $post_type[0];
		                     foreach($comments as $i => $comment) :?>
		                    	<tr>
		                    		<td><?php echo $i+1; ?></td>
		                    		<td><a href="<?php echo site_url('fan/edit_user/'.$comment['user_id']).'?ref='.site_url('content/feed/'.$post_type.'_edit/'.$post['feed_id']); ?>"><?php echo $comment['name']; ?></a></td>
		                    		<td><?php echo $comment['comment']; ?></td>
		                    		<td><?php echo date('Y-n-j', $comment['time_stamp']); ?></td>
		                    	</tr>
		                    <?php endforeach; ?>
		                    </tbody>
						</table>
                 </div>
            </div>
        </div>
    </div>
</div>
