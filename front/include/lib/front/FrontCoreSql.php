<?php

	class FrontCoreSingleton{

		private static  $_xInstance = null;
		private $_bConnected = false;
		private $_xDBConnection  = null;
		
		

		public static function getInstance() { 
	    	if (!self::$_xInstance){
	        	self::$_xInstance = new FrontCoreSingleton(); 
	        	self::$_xInstance->connectToResource();
	        }
			return self::$_xInstance; 
		}  

		public function connectToResource($p_DB_OVERRIDE){
		
			
			//check for override
			if($p_DB_OVERRIDE){
				$DB_HOST = $p_DB_OVERRIDE['DB_HOST'];
				$DB_USERNAME = $p_DB_OVERRIDE['DB_USERNAME'];
				$DB_PASSWORD = $p_DB_OVERRIDE['DB_PASSWORD'];
				$DB_NAME = $p_DB_OVERRIDE['DB_NAME'];
			}else{
//    			define('DB_HOST','mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com');
//                define('DB_USERNAME','stars_dba');
//                define('DB_PASSWORD','xZUsd2124Ut-#~4W}');
//                define('DB_NAME','starsite_stars');

    			// define('DB_HOST','localhost');
       //          define('DB_USERNAME','root');
       //          define('DB_PASSWORD','root');
       //          define('DB_NAME','stars');

				$DB_HOST = DB_HOST;
				$DB_USERNAME = DB_USERNAME;
				$DB_PASSWORD = DB_PASSWORD;
				$DB_NAME = DB_NAME;
			}

			$this->_xDBConnection = new mysqli($DB_HOST,$DB_USERNAME,$DB_PASSWORD,$DB_NAME);
			if(!$this->_xDBConnection){
				echo 'Unable to connect to host: '.$DB_HOST;
				exit;
			}elseif($this->_xDBConnection->connect_errno > 0){
				echo 'Unable to connect to host: '.$DB_HOST;
				exit;
			}
			$this->_xDBConnection->set_charset('utf8');
			$this->_bConnected = true;
		}

		public function getConnection(){
			return $this->_xDBConnection;
		}

		public function setConnectionOverride($p_aParams){
			$this->_DB_OVERRIDE = $p_aParams;
		}

		public function severConnection(){
			$this->_bConnected = false;
			$this->_xDBConnection = null;
		}
	}


 
	class FrontCoreSql{
	
		private $_xDBConnection;
		private $_sTable;
		private $_nNumberOfRequestsForDebug = 0;
		
		function __construct() {
			$this->init();
		}	
		
		private function coreInit() {
			
			if(!$this->_xDBConnection){
				$this->_xDBConnection = FrontCoreSingleton::getInstance()->getConnection();
			}
		} 

		public function establishNewConnection($p_aParams){
			FrontCoreSingleton::getInstance()->connectToResource($p_aParams);
            $this->coreInit();
        }

        public function severConnection(){
            FrontCoreSingleton::getInstance()->severConnection();
            $this->_xDBConnection = null;
        }
		
		public function init(){
			// Please Extend
		}

		public function db_escape_string($p_sStr){
            return mysqli_real_escape_string(FrontCoreSingleton::getInstance()->getConnection(),$p_sStr);
        }

        public function cleanValue($p_sStr){
            return mysqli_real_escape_string(FrontCoreSingleton::getInstance()->getConnection(),$p_sStr);
        }

        public function db_last_insert_id(){
            return mysqli_insert_id($this->_xDBConnection);
        }
        

		
		public function runQuery($p_sSql){
			if(!$this->_xDBConnection)
				$this->coreInit();

			// echo "<br />~~~~ QUERY ~~~~~".$p_sSql.'<br/ >' ;

			if(!$l_xData = $this->_xDBConnection->query($p_sSql)){
				if(DEBUG)
					print("<strong>SQL Error </strong> $p_sSql: ".$this->_xDBConnection->error."  --><br /> ");

				return false;
			} 
			return $l_xData;
		}
		
		public function buildObject($p_xDataSet,$p_aInjectArray = ''){
		 	$l_xSet = array();
			if(mysqli_num_rows($p_xDataSet) > 0){
				while($l_xRow = mysqli_fetch_assoc($p_xDataSet)){
					if(is_array($p_aInjectArray)){
						foreach($p_aInjectArray as $l_xK=>$l_xV){
						$l_xRow[$l_xK] =$l_xV;
						}
					}
					array_push($l_xSet,$l_xRow);
				}
			}
			return $l_xSet;
		}


		public function buildObjectWithKey($p_sKey,$p_xDataSet,$p_aInjectArray = ''){
		 	$l_xSet = array();
			if(mysqli_num_rows($p_xDataSet) > 0){
				while($l_xRow = mysqli_fetch_assoc($p_xDataSet)){
					if(is_array($p_aInjectArray)){
						foreach($p_aInjectArray as $l_xK=>$l_xV){
						$l_xRow[$l_xK] =$l_xV;
						}
					}

					$l_xSet[$l_xRow[$p_sKey]] = $l_xRow;
				}
			}
			
			return $l_xSet;
		}

		public function getAllSelectCount(){
			return $this->getRow($this->runQuery("SELECT FOUND_ROWS() as totalRowCount"));
		}


		public function getRow($p_xDataSet){ return mysqli_fetch_assoc($p_xDataSet);	}		  
		public function getNumberOfRows(){ return mysqli_num_rows($p_xDataSet); }
		public function getNumberOfAffectedRows(){ return mysqli_affected_rows($this->_xDBConnection); }

		public function setTable($p_sTable){ $this->_sTable = $p_sTable; }
		public function getTable(){ return $this->_sTable; }
		
		 	 
		public function getByID($p_nID){		
                    $p_nID = intval($p_nID);
			$l_sSql = 'SELECT * FROM '.$this->getTable()."  WHERE id = $p_nID";
			return $this->getRow($this->runQuery($l_sSql));
		} 
		
		public function delete($p_nID){
                    $p_nID = intval($p_nID);
			$l_sSql = 'DELETE FROM '.$this->getTable()."  WHERE id = $p_nID";
			$this->runQuery($l_sSql);	
			  
		}  
		
		public function getAssetById($p_nId){
                    $p_nId = intval($p_nId);
			$l_sSql = "SELECT * FROM assets WHERE id = $p_nId";
			return $this->getRow($this->runQuery($l_sSql));
		}				
		 
	}
	
	 
?>