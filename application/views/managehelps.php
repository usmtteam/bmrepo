<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
         <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Social Settings
                 
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>  

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                    <div id="form_head_back">Manage <?php echo $title; ?></div>
                 <div class="portlet-body form pb-feed">
                 	<form method="post" enctype="multipart/form-data">
                    <?php 
                    //echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("credit");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <!--<div class="row-fluid">
                        <div class="control-group" style="text-align: center;">
                        <?php// echo $post['type'];exit; ?>
                            <?php  echo form_dropdown("type", array('shop'=>'Shop', 'help'=>'Help', 'terms'=>'Terms', 'pravacy'=>'Pravacy'), $post['type'], 'class="large m-wrap help-dropdown"');?>
                        </div> 
                    </div>-->
                    <!--<input type="hidden" value="<?php echo site_url('helps/managehelps'); ?>" id='base_url'>-->
                     <div class="row-fluid">
                        <div class="span8">
                           <div class="control-group">
                                <label class="control-label" for="url">Content</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("content",$post['content'],'id="content" style="width:100%;height:500px" class="large "');?>
                                <script language="javascript">
									bkLib.onDomLoaded(function() {
										new nicEditor({fullPanel : true, iconsPath : '<?php echo base_url(); ?>nicEditorIcons.gif'}).panelInstance('content');
									});
								</script>
                                </div>
                            </div>
                        </div>
                        <!--<div class="span4">
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                   <img src="<?php echo ((isset($post['destination']) and $post['destination']) ? UPLOAD_URL.$post['destination'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                             </div>
                        </div>-->
                    </div>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Post",' class="btn blue"');?>
                            <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </form>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
<style type="">
    .btn.fileupload-exists{
        display: none;
    }
</style>