<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('THUMBNAIL_WIDTH',640);
define('SMALL_THUMBNAIL_WIDTH', 180);
require('wrapper.php');
class API extends REST_Controller {

    function __construct()
    {
 $this->_Wraper = new Wrapper();
        parent::__construct();
        $this->load->library('ion_auth');
        
        $this->load->library('email');
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );
        date_default_timezone_set('UTC');
        $this->email->initialize($config);

        $this->_getOptions();
        $this->result = array('status'=>true);
//        echo $token;exit;
//        if(!($this->_user = $this->_checkToken())){
//            $this->result['status'] = false;
//            $this->result['message'] = 'token_error';
//            $this->response($this->result);
//        }
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('language');
        $this->load->library('BrightCove');

        $this->load->helper('json_loader');
        $this->load->helper('bitly');
        $this->load->model('main_m');

        $this->chanel['id'] = $this->_cid;

        if($this->chanel['id']==""){
          $this->_cid = $this->set_slug_dependent_constants_off_email($this->_email);
          $this->chanel['id'] = $this->_cid;
        }
        
       
        
        $this->result['channel_info'] = $this->main_m->get_chanel($this->_cid);
        
        if(!$this->_cid && $this->_option!="get_helpurl"){
            $this->result['status'] = false;
            $this->result['message'] = 'chanel ID is required';
            $this->response($this->result);
        }elseif($this->_option!="get_helpurl"){
            $chanel = $this->main_m->get_chanel($this->_cid);
            if(!$chanel['is_publish']){
                $this->result['status'] = false;
                $this->result['message'] = 'The chanel is offline';
                $this->response($this->result);
            }
        }
        $this->set_slug_dependent_constants($this->chanel['id']);
        
        $email_config = $this->config->item('email_config', 'ion_auth');

//        if ($this->config->item('use_ci_email', 'ion_auth') && isset($email_config) && is_array($email_config))
//        {
            $this->email->initialize($email_config);
//        }
       

        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->router_func();
        exit;
    }
    private function _request($name){
        $value = $this->input->post($name, false);
        if($value === false){
            $value = $this->input->get($name);
        }
        if(!$value)
            $value = '';
        return $value;
    }

    public function router_func()
    {
        $option = $this->_option;
        call_user_func_array(array($this, "response_{$option}"), array());
        $this->response($this->result);
    }
    public function set_slug_dependent_constants($cid){

      /*
      $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result();
      $cid = $rows[0]->cid;
      
      $this->chanel = $this->get_chanel($cid);
      */

      $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $slug = $rows[0]->slug;

      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
      define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return;

      }

       public function set_slug_dependent_constants_off_email($email){

      
      $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result();
      $cid = $rows[0]->cid;
      
      $this->chanel = $this->main_m->get_chanel($cid);
      

      $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $slug = $rows[0]->slug;
    

      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
      define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return $cid;

      }

    public function checkPassword($password, $old_password){
        $salt = substr($old_password, 0, 10);
        $db_password =  $salt . substr(sha1($salt . $password), 0, -10);
        if($db_password == $old_password)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function appendRandomNum(){

        $digits_needed=12;

        $random_number=''; // set up a blank string

        $count=0;

        while ( $count < $digits_needed ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }

        return "$random_number";
    }
    
    public function register($password, $email, $extra_datas){
        
        if(!$this->_email ){
            return 2;
        }
        $fan = $this->main_m->getFanByEmail($email, $this->_cid );
        if($fan){
            if($this->_facebook){
                unset($extra_datas['credit']);
                $fan_id = $this->updateUser($fan['id'], $extra_datas);
                $user_os = $this->_Wraper->getOS($_SERVER['HTTP_USER_AGENT']);
                $browser = $this->_Wraper->getBrowser($_SERVER['HTTP_USER_AGENT']);
                $headers = apache_request_headers();
                //$insert_user_details = $this->main_m->InsertUserDetails($fan_id, $_SERVER['REMOTE_ADDR'], $user_os, $browser, $headers);
                $insert_user_details = $this->main_m->InsertUserDetails($fan_id, $_SERVER['REMOTE_ADDR'], $user_os, $browser, $headers);
                return $fan_id;
            }else{
                $this->result['message'] = "This email is already exist";
                $this->result['errorcode'] = 5;
                $this->result['status'] = false;
                return false;
            }
        }
        
        $extra_datas['credit'] = 75;
        $extra_datas['email'] = $email;
        $extra_datas['joined'] = time();
        $extra_datas['password'] = $this->ion_auth->hash_password($password);
        $extra_datas['facebook_page_id'] = $this->_facebook_page_id;
        // var_dump($extra_datas); exit();
        $this->db->insert('enr_fan', $extra_datas);
        $fan_id = $this->db->insert_id();
        
         /* -----Registerd users wrapper code ----- */
            $user_os = $this->_Wraper->getOS($_SERVER['HTTP_USER_AGENT']);
            $browser = $this->_Wraper->getBrowser($_SERVER['HTTP_USER_AGENT']);
            $registerd_user_details = $this->main_m->RegisterdUserDetails($fan_id, $_SERVER['REMOTE_ADDR'], $user_os, $browser, $extra_datas['gender'], $extra_datas['joined'], $extra_datas['birthday']);
        return $fan_id;
    }
    
    public function response_update_user(){
        $result = array();
        if(!$this->_user_id ){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_cid ){
            $this->result['message'] = "The chanel ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        /*$sql = "select * from enr_fan where name='{$this->_username}' and id<>'{$this->_user_id}' ";
        $query = $this->db->query($sql);
        if($query->row_array()){
            $this->result['message'] = "The name is already exist.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }*/
        $additional_data = array();
        if($this->_username) $additional_data['name'] = $this->_username;
        if($this->_birthday) $additional_data['birthday'] = $this->_birthday;
        if(isset($_REQUEST['gender'])) $additional_data['gender'] = $_REQUEST['gender'];
        if($this->_city) $additional_data['city'] = $this->_city;
        if($this->_state) $additional_data['state'] = $this->_state;
        if($this->_cid) $additional_data['cid'] = $this->_cid;
        if($this->_credit) $additional_data['credit'] = $this->_credit;
        if($this->_country) $additional_data['country'] = $this->_country;
        if($this->_password) $additional_data['password'] = $this->ion_auth->hash_password($this->_password);
        if(isset($_REQUEST['enable_facebook'])) $additional_data['enable_facebook'] = $_REQUEST['enable_facebook'];
        if(isset($_REQUEST['enable_twitter'])) $additional_data['enable_twitter'] = $_REQUEST['enable_twitter'];
        if(isset($_REQUEST['enable_instagram'])) $additional_data['enable_instagram'] = $_REQUEST['enable_instagram'];
        if($this->_facebook_page_id) $additional_data['facebook_page_id'] = $this->_facebook_page_id;
        if(isset($_REQUEST['enable_facebook_custom_caption'])) $additional_data['enable_facebook_custom_caption'] = $_REQUEST['enable_facebook_custom_caption'];
        if(isset($_REQUEST['enable_twitter_custom_caption'])) $additional_data['enable_twitter_custom_caption'] = $_REQUEST['enable_twitter_custom_caption'];
        if(isset($_REQUEST['enable_instagram_custom_caption'])) $additional_data['enable_instagram_custom_caption'] = $_REQUEST['enable_instagram_custom_caption'];
        if(isset($_REQUEST['shareOptions'])) $additional_data['shareOptions'] = $_REQUEST['shareOptions'];
        
        if($id = $this->updateUser($this->_user_id, $additional_data)){
            $this->result['status'] = true;
            $this->result['user_id'] = $id;
            $this->_user_id = $id;
            $this->response_photoupdate();

            $user = $this->main_m->getFan($id);
            $user['img_url'] = $this->getImageUrl($user['img_url']);
            $user['img_width'] = $this->img_width;
            $user['img_height'] = $this->img_height;

//            $token = $this->_createToken();
//            $this->result['token'] = $token;
            $this->result['info'] = $user;
//            $this->_insertToken($id, $token);
            $this->result['status'] = true;
        }else{
            $this->result['status'] = false;
        }

        $this->response($this->result);
        
    }
    
    public function updateUser($id, $extra_datas){
        $this->db->where('id', $id);
        $this->db->update('enr_fan', $extra_datas);
        return $id;
    }

    public function updateAdmin($id, $extra_datas){
        $this->db->where('id', $id);
        $this->db->update('enr_fan', $extra_datas);
        return $id;
    }
    

    public function response_updateAdminInfo($password){
        $data['email'] = $this->_email;
        $data['password'] = $this->_password;
        $data['cid']  = $this->_cid;
        $this->main_m->updateAdmin($data, 0);
        return true;
    }


    public function response_loginToCms($password){
        $data['identity'] = $this->_email;
        $data['password'] = $this->_password;
         $data['cid'] = $this->_cid = $this->main_m->set_slug_dependent_constants($this->_email);
        $status = $this->main_m->loginToCms($data);
        $this->result['status'] = $status;
        return $this->result['status'];
    }

    public function response_photoupdate(){
        $result = $this->main_m->updateProfilePhoto($this->_user_id);
        $this->result['status'] = $result;
//        $this->response($this->result);       
    }
    
    private function _sendEmail($id, $verification_code=''){
        $user = $this->main_m->getUser($id);
        $verification_url = $this->_makeActivationUrl($verification_code);
        $message = $this->main_m->getConfiguration('email_verification_message');
        $message .= "\n".$verification_url;

        $this->email->clear();
        $this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
        $this->email->to($user->email);
        $this->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_activation_subject'));
        $this->email->message($message);
        
        if ($this->email->send() == TRUE){
            
        }
    }
    
    private function _makeActivationUrl($verification_code=''){
        $url = site_url('confirm/confirm_verification');
        $url .= "/".$verification_code;
        return $url;
    }
    
    private function _createToken(){
        $len = rand(1,1000);
        $token = md5(time().$len);
        $query = $this->db->query("select * from enr_token where token='{$token}'");
        $row = $query->result();
        if($row){
            $token = $this->_createToken();
        }
        return $token;
    }

    private function _insertToken($user_id, $token){
        $sql = array('user_id'=>$user_id, 'token'=>$token);
        $this->db->where('user_id', $user_id);
        
        if($this->db->update('enr_token', $sql) && !$this->db->affected_rows()){
            $this->db->insert('enr_token', $sql);
        }
    }
    
    private function _checkToken(){
//        if($this->_option == "login" || $this->_option == "register" ){
        if($this->_option == "login" || $this->_option == "register" || $this->_option == "photoupdate" || $this->_option == "forgotpassword" || $this->_option == "get_helpurl"  ){
            return true;
        }
        if(!$this->_token)
            return false;
        $query = $this->db->query("
            select t1.token, t2.* 
            from enr_token t1 join enr_fan t2 on t1.user_id=t2.id
            where 
                t1.token='{$this->_token}'
        ");
        $user = $query->row_object();
        if(is_object($user)){
            return $user;
        }else{
            return false;
        }
    }
    
    public function _login($email, $password, $cid){
        $fan = $this->main_m->getFanByEmail($email, $cid);
        $db_password = $fan['password'];
        if($this->checkPassword($password,$db_password)){
            return $fan['id'];
        }else{
            $this->result['errorcode'] = 1;
            $this->result['message'] = "Password error";
            return false;
        }
    }
    
    public function getImageUrl($img_path){

        if(UPLOAD_CLOUD.$img_path!=""){

            $photo = $this->db->get_where('enr_photo', array('destination'=>$img_path))->row_array();
            
            if($photo){

                //$this->main_m->saveResizeImage($img_path);
              
                $realpath = UPLOAD_CLOUD.$img_path;
                
//                $class = new Watermark();
//                $class->main_m->apply($file, $file, $watermark, $position);
                
                /**
                * set image width
                */
//                $relativepath = str_replace(BASE_URL, '', UPLOAD_CLOUD.$img_path);
//                $img_url = BASE_URL."phpThumb.php?src=".$relativepath."&wp=640";
                $img_path = rtrim($img_path);
                if($photo['fan_id'] != -1){
                    $img_url =  UPLOAD_CLOUD.$img_path;
                }else{
                   // $img_url =  site_url('image/watermark/'.str_replace("/", "-",$img_path)."/".$this->_cid);
                    $img_url = UPLOAD_CLOUD."watermark/".$img_path;
                }
                
            }else{
//                $realpath = UPLOAD_DIR.$img_path;
//                $watermark = new Watermark();
//                $sizes = @$watermark->applyForSize($realpath);
//                $this->img_width = $sizes['width'];
//                $this->img_height = $sizes['height'];
                
                $img_url = UPLOAD_CLOUD.$img_path;
            }
        }else{
            $img_url = "";
        }
        return $img_url;
    }

    private function _getOptions(){

        $this->_option = $this->db->escape_str($this->_request('option'));
        $this->_token = $this->db->escape_str($this->_request('token'));
        $this->_name = $this->_request('name');
        $this->_password = $this->_request('password');
        
        $this->_cid = $this->_request('cid');
        $this->session->set_userdata('chanel', array('id' => $this->_cid));        
        //register
        $this->_username = $this->_request('username');
        $this->_firstname = $this->_request('firstname');
        $this->_lastname = $this->_request('lastname');
        $this->_email = $this->_request('email');
        $this->_gender = $this->_request('gender');
        $this->_birthday = $this->_request('birthday');
        $this->_country = $this->_request('country');
        $this->_facebook = $this->_request('facebook');
        $this->_facebook_page_id = $this->_request('facebook_page_id');
        $this->_state = $this->_request('state');
        $this->_city = $this->_request('city');
        $this->_gender = $this->_request('gender');
        $this->_credit = $this->_request('credit');
        //login
        $this->_email = $this->_request('email');
        $this->_password = $this->_request('password');
        
        $this->_enable = $this->_request('enable');
        //get user
        $this->_self_user_id = $this->_request('self_user_id');
        //main feed
        $this->_page = $this->_request('page');
        $this->_cnt_of_page = $this->_request('cnt_of_page');
        $this->_post_type = $this->_request('post_type');
        $this->_user_id = $this->_request('user_id');
        $this->_ud_token = $this->_request('ud_token');
        //get photo
        $this->_photo_id = $this->_request('photo_id');
        
        // comment
        $this->_post_type = $this->_request('post_type');
        $this->_comment = $this->_request('comment');
        $this->_content_id = $this->_request('content_id');
        $this->_user_id = $this->_request('user_id');
        $this->_like  = $this->_request('like');
        $this->_approval = $this->_request('approval');
        $this->_comment_id  = $this->_request('comment_id');
        
        //search user
        $this->_username = $this->_request('username');
        
        //add text
        $this->_description = $this->_request('description');
        $this->_tags = $this->_request('tags');
        $this->_credit = $this->_request('credit');
        $this->_video_url = $this->_request('_video_url');
        
        
        //set follow
        $this->_followed_user_id = $this->_request('followed_user_id');
        $this->_is_unfollow = $this->_request('is_unfollow');

        //verification code
        $this->_confirm_verification = $this->_request('confirm_verification');
        
        //Add Mail content
        $this->_sender = $this->_request('sender');
        $this->_receiver = $this->_request('receiver');
        $this->_message = $this->_request('message');
        $this->_mail_id = $this->_request('mail_id');
        $this->_time_zone = $this->_request('time_zone');
        $this->_current_time = $this->_request('current_time');
        
        //get poll answer
        $this->_question_id = $this->_request('question_id');
        $this->_answer_id = $this->_request('answer_id');
        $this->_ud_token = $this->_request('ud_token');
        //get quiz answer
        $this->_question_count = $this->_request('question_count');
        $this->_correct_count = $this->_request('correct_count');
        $this->_content_id = $this->_request('content_id');
        
        
        
    }

    public function response_login(){
        if(!$this->_ud_token){
            $this->result['message'] = "The ud token is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        $this->load->library('ion_auth');
        $result = array();
        
        if($id = $this->_login($this->_email, $this->_password, $this->_cid)){
            $this->db->update('enr_fan', array('last_login'=>time()), array('id'=>$id));
            $user = $this->main_m->getFan($id);
//            if(!$user->verification){
//                $this->result['status'] = false;
//                $this->result['message'] = "You need to verificate using the code you received.";
//                $this->response($this->result);
//            }
            $token = $this->_createToken();
            $this->result['token'] = $token;
            $user['img_url'] = $this->getImageUrl($user['img_url']);
            $user['img_width'] = $this->img_width;
            $user['img_height'] = $this->img_height;

            $this->result['info'] = $user;
            $this->_insertToken($id, $token);
            
            $ud_type = isset($_REQUEST['ud_type']) ? $_REQUEST['ud_type'] : 'IPHONE';
            $this->main_m->addUdToken($id, $ud_type, $this->_ud_token);
        }else{
            $this->result['status'] = false;
        }
        $this->response($this->result);
    }

    public function response_register(){
        $result = array();
        if( !$this->_password && $this->_facebook == 0 ){
            $this->result['message'] = "The password is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_username ){
            $this->result['message'] = "The username is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_cid ){
            $this->result['message'] = "The chanel ID is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_email ){
            $this->result['message'] = "The email is required.";
            $this->result['errorcode'] = 4;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_firstname ){
            $this->result['message'] = "The first name is required.";
            $this->result['errorcode'] = 5;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_lastname ){
            $this->result['message'] = "The last name is required.";
            $this->result['errorcode'] = 6;
            $this->result['status'] = false;
            return false;
        }
        
        $additional_data = array(
            'name' => $this->_username,
            'firstname' => $this->_firstname,
            'lastname' => $this->_lastname,
            'birthday' => $this->_birthday,
            'gender' => isset($_REQUEST['gender']) ? $_REQUEST['gender'] : -1,
            'city' => $this->_city,
            'state' => $this->_state,
            'cid' => $this->_cid,
            'country' => $this->_country,
            'facebook' => (empty($this->_facebook) || !isset($this->_facebook)) ? 0 : $this->_facebook,
            'facebook_page_id' => $this->_facebook_page_id,
            'is_publish' => 1
        );
        if(!$this->_facebook && $this->db->get_where('enr_fan', array('name'=>$this->_username))->row_array()){
            $this->result['message'] = "The name is already exist.";
            $this->result['errorcode'] = 6;
            $this->result['status'] = false;
            return false;
        }
        
        if($id = $this->register($this->_password, $this->_email, $additional_data)){
            $this->result['user_id'] = $id;
            $this->_user_id = $id;

            $ud_type = isset($_REQUEST['ud_type']) ? $_REQUEST['ud_type'] : 'IPHONE';
            $this->main_m->addUdToken($id, $ud_type, $this->_ud_token);
            
            $this->response_photoupdate();

            $user = $this->main_m->getFan($id);
            $user['img_url'] = $this->getImageUrl($user['img_url']);
            $user['img_width'] = $this->img_width;
            $user['img_height'] = $this->img_height;

            $token = $this->_createToken();
            $this->result['token'] = $token;
            $this->result['info'] = $user;
            $this->_insertToken($id, $token);
            $this->result['status'] = true;
        }else{
            $this->result['status'] = false;
        }

        $this->response($this->result);
    }
    
    private function getPagenation($lists, $page, $cntOfPage){
        if(!$page)
            $page = 1; 
        if(!$cntOfPage)
            $cntOfPage = 8; 
        $start = ($page-1) * $cntOfPage;
        $result = array();
        for($i=$start; $i<$start+$cntOfPage; $i++){
            if($i<count($lists))
                $result[] = $lists[$i];
            else
                break;
        }
        return $result;
    }
    private function _filterFeed($lists, $type=false){
//        if(!$type || $type=='all'){
//            return $lists;
//        }
        $results = array();
        foreach($lists as $list){
            if($type == 'poll_quiz'){
                if(strtolower($list['post_type']) == 'poll post' || strtolower($list['post_type']) == 'quiz post'){
                    $results[] = $list;
                }
            }elseif(!$type || $type=='all'){
                if(strtolower($list['post_type']) != 'poll post' && strtolower($list['post_type']) != 'quiz post' ){
                    $results[] = $list;
                }   
            }else{
                if(strtolower($list['post_type']) == strtolower($type.' post')){
                    $results[] = $list;
                }
            }
        }
        return $results;
    }
    public function parseFeed(&$feed){
        $ids = explode("_",$feed['id']);
        $post_type = $ids[0];
        $content_id = $ids[1];
        $feed['post_type'] = $post_type;
        $feed['content_id'] = $content_id;
        if($post_type == 'video'){
            $video = $this->db->get_where('enr_video', array('id'=>$content_id))->row_array();
            if($video['video_url']){
                $feed['video_url'] = $video['video_url'];
            }else{
                $feed['video_url'] = $this->getImageUrl($video['destination']);
            }
        }
        if($post_type == 'poll' || $post_type == 'quiz'){           
            $quiz = $this->db->get_where('enr_'.$post_type, array('id'=>$content_id))->row_array();
            $feed['text'] = $quiz['text'];
//            print_r($feed);exit;
        }
        if($post_type == 'photo') {
            $photo = $this->db->get_where('enr_'.$post_type, array('id'=>$content_id))->row_array();
            $feed['url_link'] = $photo['url_link'];
        }
        //user part
        if($feed['fan_id']){
            $fan = $this->main_m->getFan($feed['fan_id']);
            $fan['img_url'] = $this->getImageUrl($fan['img_url']);
            $fan['img_width'] = $this->img_width;
            $fan['img_height'] = $this->img_height;

            $feed['fan_name'] = $fan['name'];
            $feed['fan_img_url'] = $fan['img_url'];
        }
        //comments

        $rows = $this->main_m->getComments($post_type, $content_id, UPLOAD_CLOUD);
        $feed['comments_count'] = count($rows);
        $comment_count = isset($_REQUEST['comment_count']) ? $_REQUEST['comment_count'] : -1;
        $rows = $this->main_m->getComments($post_type, $content_id, UPLOAD_CLOUD, $comment_count);
        $feed['comments'] = $rows;
        //like
        $rows = $this->db->get_where('enr_like', array('post_type'=>$post_type, 'content_id'=>$content_id))->result_array();
        $feed['numberoflike'] = count($rows);
        //check liked
        $like_row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>$post_type, 'content_id'=>$content_id))->row();
        if($like_row)
            $feed['did_like'] = true;
        else
            $feed['did_like'] = false;
        
        //approval
        if($feed['is_publish'] == 2) {
            //approval
            $rows = $this->db->query('select * from enr_approval join enr_fan on enr_approval.user_id = enr_fan.id where post_type = ? and content_id = ? and is_draft = 1 and is_suspend = 0 and is_block = 0 and admin_type is NULL', array($post_type, $content_id))->result_array();
            $feed['numberofapproval'] = count($rows);
            $rows = $this->db->get_where('enr_fan', array('is_draft' => 1, 'is_suspend' => 0, 'is_block' => 0, 'admin_type is NULL' => null))->result_array();
            $feed['numberofdraftuser'] = count($rows);
            //check approval
            $approve_row = $this->db->get_where('enr_approval', array('user_id'=>$this->_user_id, 'post_type'=>$post_type, 'content_id'=>$content_id))->row();
            if($approve_row)
                $feed['did_approve'] = true;
            else
                $feed['did_approve'] = false;
        }
        //check buy content
        $data = array(
            'user_id' => $this->_user_id,
            'post_type' => $post_type,
            'content_id' => $content_id
        );
        if($this->main_m->checkBuyContent($data)){
            $feed['unlock'] = true;
        }else{
            $feed['unlock'] = false;
        }
    }
    public function response_get_extradatas(){
        if(!$this->_post_type){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_content_id){
            $this->result['message'] = "The content ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        //comments
        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id);
        foreach($rows as &$row){
            $row['img_url'] = $this->getImageUrl($row['img_url']);
            $row['img_width'] = $this->img_width;
            $row['img_height'] = $this->img_height;
        }
        $comments = $rows;
        //like
        $rows = $this->db->get_where('enr_like', array('post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->result_array();
        $numberoflike = count($rows);
        //check liked
        $like_row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->row();
        if($like_row)
            $did_like = true;
        else
            $did_like = false;
        $this->result['comments'] = $comments;
        $this->result['numberoflike'] = $numberoflike;
        $this->result['did_like'] = $did_like;
//        print_r($this->result);exit;
    }
    public function response_get_main_feed(){
        $draft_only = isset($_REQUEST['draft']) ? $_REQUEST['draft'] : 0;
        $user_id = $this->_user_id;
        if($draft_only) $feeds = $this->main_m->getDraftFeedList($user_id);
        else {
            $feeds = $this->main_m->getHomeFeedList(true, $user_id, true);
            $feeds = $this->_filterFeed($feeds, $this->_post_type);
        }
        $feeds = $this->getPagenation($feeds, $this->_page, $this->_cnt_of_page);
        $new_img_force = $this->appendRandomNum();
        foreach($feeds as &$feed){
            $img = $feed['image_path'];
            
            



            $this->parseFeed($feed);


            if($feed['post_type'] != 'photo') {
                $feed['image_path'] = UPLOAD_CLOUD.$img."?new=".$new_img_force;
                $feed['img_width'] = $this->img_width;
                $feed['img_height'] = $this->img_height;
            }
            if($feed['post_type'] == 'video') {
                $feed['image_path'] = UPLOAD_CLOUD.$img."?new=".$new_img_force;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = $s_height * THUMBNAIL_WIDTH / $s_width;
            }
            elseif($feed['post_type'] == 'photo') {
                $feed['image_path'] = UPLOAD_CLOUD."watermark/thumb/".$img."?new=".$new_img_force;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
                
                $feed['img_small_thumb'] = UPLOAD_CLOUD."watermark/small_thumb/".$img."?new=".$new_img_force;
                $feed['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
                $feed['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
                
                $feed['fullimage_path'] = UPLOAD_CLOUD."watermark/".$img."?new=".$new_img_force;
                $feed['fullimg_width'] = $s_width;
                $feed['fullimg_height'] = $s_height;
            }
            elseif($feed['post_type'] == 'poll') {
                $query = 'select count(*) as `count` from enr_answer_history where user_id=? and question_id in (select id from enr_poll_question where poll_id=?)';
                $result = $this->main_m->db->query($query, array($user_id, $feed['content_id']))->result_array();
                if($result[0]['count'] == 0) {
                    $feed['answered'] = false;
                }
                else {
                    $feed['answered'] = true;
                }
            }
            elseif($feed['post_type'] == 'quiz') {

                $query = 'select count(*) as `count` from enr_answer_history_quiz where user_id=? and content_id=? and correct_count = question_count';
                $result = $this->main_m->db->query($query, array($user_id, $feed['content_id']))->result_array();
                if($result[0]['count'] == 0) {
                    $feed['answered'] = false;
                }
                else {
                    $feed['answered'] = true;
                }
            }
        }
        $user = $this->main_m->get_chanel($this->_cid);
//        print_r($user);exit;
        $star = array(
            'email' => $user['email'],
            'img_url' => $this->getImageUrl($user['chanel_img_url']),
            'img_width' => $this->img_width,
            'img_height' => $this->img_height,
//            'name' => $user['first_name']
            'name' => $user['name']
        );
//        print_r($user);exit;
        $this->result['star'] = $star;
        $this->result['feeds'] = $feeds;
//        print_r($feeds);exit;
//        print_r($this->result);exit;
        
                if ($feed['post_type'] == 'poll') {
            $insertMenuPollQuiz = $this->main_m->menu_tracking_details($this->_user_id, 'polls_quizzes'); //Menu Tracking
        } else if ($feed['post_type'] == 'video') {
            $insertMenuPollQuiz = $this->main_m->menu_tracking_details($this->_user_id, 'video'); //Menu Tracking
        } else {
            if ($this->_user_id != 0) {
                $insertMenuAllAccess = $this->main_m->menu_tracking_details($user_id, 'all_access'); //Menu Tracking
            }
        }
        
        
        
        
        
    }
    
    public function response_push_notification(){
        if(!$this->_user_id){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $this->main_m->setPushNotification($this->_enable, $this->_user_id);
    }
    
    public function response_get_push(){
        if(!$this->_user_id){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $row = $this->db->get_where('enr_push_notification', array('user_id'=>$this->_user_id))->row();
        if($row){
            $this->result['enable'] = true;
        }
        else{
            $this->result['enable'] = false;
        }
    }
    
    public function response_set_comment(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_comment ){
            $this->result['message'] = "The comment is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 4;
            $this->result['status'] = false;
            return false;
        }

        $options = array(
            'post_type' => $this->_post_type,
            'comment' => $this->_comment,
            'content_id' => $this->_content_id,
            'user_id' => $this->_user_id
        );
        $this->main_m->setComment($options);
        $this->main_m->wrp_setComment($options);
        
//         $rows = $this->main_m->getComments($this->_post_type, $this->_content_id);
//         foreach($rows as &$row){
//             $row['img_url'] = $this->getImageUrl($row['img_url']);
//             $row['img_width'] = $this->img_width;
//             $row['img_height'] = $this->img_height;
//         }
//         $this->result['comments'] = $rows;
        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id, UPLOAD_CLOUD);
        $this->result['comments_count'] = count($rows);
        $comment_count = isset($_REQUEST['comment_count']) ? $_REQUEST['comment_count'] : -1;
        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id, UPLOAD_CLOUD, $comment_count);
        $this->result['comments'] = $rows;
    }
    
    public function response_get_comment(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        $count = isset($_REQUEST['comment_count']) ? $_REQUEST['comment_count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        
        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id, UPLOAD_CLOUD, $count, $page);
//         foreach($rows as &$row){
//             $row['img_url'] = $this->getImageUrl($row['img_url']);
//             $row['img_width'] = $this->img_width;
//             $row['img_height'] = $this->img_height;
//         }
        $this->result['comments'] = $rows;
    }
    public function response_set_like(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }

        $options = array(
            'post_type' => $this->_post_type,
            'content_id' => $this->_content_id,
            'user_id' => $this->_user_id
        );
        $this->main_m->setLike($options, $this->_like);
    
        $rows = $this->db->get_where('enr_like', array('post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->result_array();
        $this->result['numbersoflike'] = count($rows);
    }
    public function response_set_publish(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
    
        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        
        $timestamp = isset($_REQUEST['time_stamp']) ? $_REQUEST['time_stamp'] : 0;
        $is_sticky = isset($_REQUEST['is_sticky']) ? $_REQUEST['is_sticky'] : -1;
        
        $query = 'update enr_' . $this->_post_type . ' set is_publish = 1';
        if($timestamp != 0) $query .= ', time_stamp = ' . time();
        if($is_sticky != -1) $query .= ', is_sticky = ' . $is_sticky;
        $query .= ' where id = ' . $this->_content_id;
        
        $this->db->query($query);
        
        $query = 'delete from enr_comment where post_type = ? and content_id = ?';
        

        $this->db->query($query, array($this->_post_type, $this->_content_id));   // delete any draft comments made during approval

	$feed_id = $this->main_m->get_feed_by_content_id($this->_post_type, $this->_content_id);
		if(!empty($feed_id)) {
			$this->main_m->sendpush($feed_id);   // send out push notification that this is now published
		}


    }
    
    public function response_get_like(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_user_id ){
            $this->result['message'] = "The fan id is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }
        $rows = $this->db->get_where('enr_like', array('post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->result_array();
        $row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->row();
        $this->result['numbers'] = count($rows);
        if($row)
            $this->result['did_like'] = true;
        else
            $this->result['did_like'] = false;
    }
    public function response_set_approval(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
    
        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
    
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }
    
        $options = array(
                'post_type' => $this->_post_type,
                'content_id' => $this->_content_id,
                'user_id' => $this->_user_id
        );
        $this->main_m->setApproval($options, $this->_approval);
    
        $rows = $this->db->get_where('enr_approval', array('post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->result_array();
        $this->result['numbersofapproval'] = count($rows);
    }
    
    public function response_get_approval(){
        if(!$this->_post_type ){
            $this->result['message'] = "The post type is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
    
        if(!$this->_content_id ){
            $this->result['message'] = "The content id is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_user_id ){
            $this->result['message'] = "The fan id is required.";
            $this->result['errorcode'] = 3;
            $this->result['status'] = false;
            return false;
        }
        $rows = $this->db->get_where('enr_approval', array('post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->result_array();
        $row = $this->db->get_where('enr_approval', array('user_id'=>$this->_user_id, 'post_type'=>$this->_post_type, 'content_id'=>$this->_content_id))->row();
        $this->result['numbers'] = count($rows);
        if($row)
            $this->result['did_approve'] = true;
        else
            $this->result['did_approve'] = false;
    }
    public function response_get_user(){
        if(!$this->_user_id ){
            $this->result['message'] = "User ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $user = $this->main_m->getFan($this->_user_id);
        //get numbers of unread message 
        $user['numberofunreadmessage'] = count($this->main_m->getUnreadMessages($this->_self_user_id));
        
        $user['img_url'] = $this->getImageUrl($user['img_url']);
        $user['img_width'] = $this->img_width;
        $user['img_height'] = $this->img_height;

        $followings = $this->main_m->getFollowings($user['id'], UPLOAD_CLOUD);
        $followeds = $this->main_m->getFolloweds($user['id'], UPLOAD_CLOUD);
        $follow_param = isset($_REQUEST['follow_param']) ? $_REQUEST['follow_param'] : 0;
        if($follow_param) {
            $user['followings'] = count($followings);
            $user['followeds'] = count($followeds);
        }
        else {
            $user['followings'] = $followings;
            $user['followeds'] = $followeds;
        }
//         foreach($followings as $row){
//             $f_user = $this->main_m->getFan($row['followed_user_id']);
//             if(!$f_user){
//                 continue;
//             }
            
//             $f_user['img_url'] = $this->getImageUrl($f_user['img_url']);
//             $f_user['img_width'] = $this->img_width;
//             $f_user['img_height'] = $this->img_height;
            
            
//             $user['followings'][] = $f_user;
//         }
//         foreach($followeds as $row){
//             $f_user = $this->main_m->getFan($row['user_id']);
//             if(!$f_user){
//                 continue;
//             } 
//             $f_user['img_url'] = $this->getImageUrl($f_user['img_url']);
//             $f_user['img_width'] = $this->img_width;
//             $f_user['img_height'] = $this->img_height;
//             $user['followeds'][] = $f_user;
//         }
        $this->result['did_follow'] = $this->main_m->checkFollow($this->_self_user_id, $this->_user_id);
        $user['point'] = $this->main_m->getPoint($this->_user_id);
        $this->result['did_block'] = $this->main_m->checkBlockFan($this->_self_user_id, $this->_user_id);
        
        
        //$feeds = $this->getPagenation($feeds, $this->_page, $this->_cnt_of_page);
        
        $feeds = $this->main_m->getCommunityFeedList(false, $this->_self_user_id);
        $results = array();
        foreach($feeds as $feed){
            if($this->_user_id == $feed['fan_id']){
                $results[] = $feed;
            }
        }
        
        $results = $this->getPagenation($results, $this->_page, $this->_cnt_of_page);
        foreach($results as &$result){
            $img = $result['image_path'];

            $this->parseFeed($result);
            
            if($result['post_type'] != 'photo') {
                $result['image_path'] = $this->getImageUrl($result['image_path']);
                $result['img_width'] = $this->img_width;
                $result['img_height'] = $this->img_height;
            }
            
            if($result['post_type'] == 'video') {
                $result['image_path'] = UPLOAD_CLOUD.$img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $result['img_width'] = THUMBNAIL_WIDTH;
                $result['img_height'] = $s_height * THUMBNAIL_WIDTH / $s_width;
            }
            elseif($result['post_type'] == 'photo') {
                $result['image_path'] = UPLOAD_CLOUD.$img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $result['img_width'] = THUMBNAIL_WIDTH;
                $result['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
                
                $result['img_small_thumb'] = UPLOAD_CLOUD."/watermark/".$img;
                $result['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
                $result['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
                
                $result['fullimage_path'] = UPLOAD_CLOUD.$img;
                $result['fullimg_width'] = $s_width;
                $result['fullimg_height'] = $s_height;
            }
            
        } 

        $this->result['feeds'] = $results;
        $this->result['user'] = $user;
    }
    
    public function response_get_followusers_followed_me(){
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        $follows = $this->main_m->getFolloweds($this->_user_id, UPLOAD_CLOUD, $count, $page);
//         $users = array();
//         foreach($follows as $row){
//             $f_user = $this->main_m->getFan($row['user_id']);
//             $f_user['img_url'] = $this->getImageUrl($f_user['img_url']);
//             $f_user['img_width'] = $this->img_width;
//             $f_user['img_height'] = $this->img_height;

//             $users[] = $f_user;
//         }
        $this->result['users'] = $follows;
    }
    
    public function response_get_followusers_by_me(){
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        $follows = $this->main_m->getFollowings($this->_user_id, UPLOAD_CLOUD, $count, $page);
//         $users = array();
//         foreach($follows as $row){
//             $f_user = $this->main_m->getFan($row['followed_user_id']);
//             $f_user['img_url'] = $this->getImageUrl($f_user['img_url']);
//             $f_user['img_width'] = $this->img_width;
//             $f_user['img_height'] = $this->img_height;

//             $users[] = $f_user;
//         }
        $this->result['users'] = $follows;
    }
    
    public function response_get_owner_contents(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        $feeds = $this->main_m->getCommunityFeedList(false, $this->_user_id);

        $results = array();
        foreach($feeds as $feed){
            if($this->_user_id == $feed['fan_id']){
                $results[] = $feed;
            }
        }

        $feeds = $this->getPagenation($results, $this->_page, $this->_cnt_of_page);
        foreach($feeds as &$feed){
            $feed['image_path'] = $this->getImageUrl($feed['image_path']);
            $feed['img_width'] = $this->img_width;
            $feed['img_height'] = $this->img_height;

            $this->parseFeed($feed);
        }
        
        $this->result['feeds'] = $feeds;
    }
    public function response_get_community_feed(){
        $feeds = $this->main_m->getCommunityFeedList();
        $feeds = $this->_filterFeed($feeds, $this->_post_type);
        $feeds = $this->getPagenation($feeds, $this->_page, $this->_cnt_of_page);
        $new_img_force = $this->appendRandomNum();
        foreach($feeds as &$feed){
            $img = $feed['image_path'];

            $this->parseFeed($feed);
            
            if($feed['post_type'] != 'photo') {
                $feed['image_path'] = $this->getImageUrl($feed['image_path']);
                $feed['img_width'] = $this->img_width;
                $feed['img_height'] = $this->img_height;
            }
            
            
            if($feed['post_type'] == 'video') {
                $feed['image_path'] = UPLOAD_CLOUD.$img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = $s_height * THUMBNAIL_WIDTH / $s_width;
            }
            elseif($feed['post_type'] == 'photo') {
                $feed['image_path'] = UPLOAD_CLOUD.$img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
                
                $feed['img_small_thumb'] = UPLOAD_CLOUD.$img;
                $feed['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
                $feed['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
                
                $feed['fullimage_path'] = UPLOAD_CLOUD.$img;
                $feed['fullimg_width'] = $s_width;
                $feed['fullimg_height'] = $s_height;
            }
        }
        
        $insertMenuCommunity = $this->main_m->menu_tracking_details($this->_user_id, 'community'); //Menu Tracking
        $this->result['feeds'] = $feeds;
    }
    
    public function response_get_photos(){
        /*if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }*/
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        $photos = $this->main_m->getPhotos(true, $this->_user_id, $count, $page);
        
        $tags = array('All');//$this->main_m->getPhotoTagOtions($this->_cid, true);
        
        foreach($photos as &$photo){
            if($photo['tags'] && !in_array($photo['tags'], $tags) ){
                $tags[$photo['id']] = $photo['tags'];
            }
            
            $img = str_replace(UPLOAD_CLOUD, "", $photo['destination']);

            
            $photo['destination'] = $this->getImageUrl(str_replace(UPLOAD_CLOUD, "", $photo['destination']) );
            $photo['img_width'] = $this->img_width;
            $photo['img_height'] = $this->img_height;
            
            $photo['image_path'] = UPLOAD_CLOUD."watermark/".$img;
            list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
            $photo['img_width'] = THUMBNAIL_WIDTH;
            $photo['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
        
            
            $photo['img_small_thumb'] = UPLOAD_CLOUD."watermark/small_thumb/".$img;
            $photo['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
            $photo['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
            
            $photo['fullimage_path'] = UPLOAD_CLOUD."watermark/".$img;
            $photo['fullimg_width'] = $s_width;
            $photo['fullimg_height'] = $s_height;
            
          
            $comments = $this->main_m->getComments('photo', $photo['id']);
            $photo['numbersofcomments'] = count($comments);
            //check liked
            $like_row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>'photo', 'content_id'=>$photo['id']))->row();
            if($like_row)
                $photo['did_like'] = true;
            else
                $photo['did_like'] = false;
            //like
            $rows = $this->db->get_where('enr_like', array('post_type'=>'photo', 'content_id'=>$photo['id']))->result_array();
            $photo['numberoflike'] = count($rows);

            /**
            * check if locked or not
            */
            $data = array(
                'user_id' => $this->_user_id,
                'post_type' => 'photo',
                'content_id' => $photo['id']
            );
            if($this->main_m->checkBuyContent($data)){
                $photo['unlock'] = true;
            }else{
                $photo['unlock'] = false;
            }
        }
//        print_r($photos);exit;
        $this->result['photos']= $photos;
        

        $user = $this->main_m->get_chanel($this->_cid);
        //        print_r($user);exit;
        $star = array(
                'email' => $user['email'],
                'img_url' => $this->getImageUrl($user['chanel_img_url']),
                'img_width' => $this->img_width,
                'img_height' => $this->img_height,
                //            'name' => $user['first_name']
                'name' => $user['name']
        );
        //        print_r($user);exit;
        $this->result['star'] = $star;
        
        
        foreach($tags as $tag){
            $this->result['filters'][]= $tag;
        }
//print_r($photos);exit;
        $insertMenuPhotos = $this->main_m->menu_tracking_details($this->_user_id, 'photos'); //Menu Tracking
    }

    public function response_get_photo(){
        $photo = $this->main_m->getPhoto($this->_photo_id);
        $photo['photo_url'] = $photo['destination'];
        $photo['destination'] = $this->getImageUrl(str_replace(UPLOAD_CLOUD, "", $photo['destination']) );
        $photo['img_width'] = $this->img_width;
        $photo['img_height'] = $this->img_height;
        $photo['photo_description'] = $photo['description'];
        
        $new_img_force = appendRandomNum();
        //comments
        $rows = $this->main_m->getComments('photo', $this->_photo_id);

        foreach($rows as &$row){
            $img = $row['img_url'];
            
            $row['img_url'] = $this->getImageUrl($row['img_url']);
            $row['img_width'] = $this->img_width;
            $row['img_height'] = $this->img_height;
            
            $row['image_path'] = UPLOAD_CLOUD."watermark/".$img;
            list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
            $row['img_width'] = THUMBNAIL_WIDTH;
            $row['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
            
            
            $row['img_small_thumb'] = UPLOAD_CLOUD."watermark/small_thumb/".$img;
            $row['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
            $row['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
            
            
            $row['fullimage_path'] = UPLOAD_CLOUD."watermark/".$img;
            $row['fullimg_width'] = $s_width;
            $row['fullimg_height'] = $s_height;

        }
        $photo['comments'] = $rows;

        $rows = $this->db->get_where('enr_like', array('post_type'=>'photo', 'content_id'=>$this->_photo_id))->result_array();
        $photo['numberoflike'] = count($rows);
        
        //check liked
        $like_row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>'photo', 'content_id'=>$this->_photo_id))->row();
        if($like_row)
            $photo['did_like'] = true;
        else
            $photo['did_like'] = false;
        /**
        * check if locked or not
        */
        $data = array(
            'user_id' => $this->_user_id,
            'post_type' => 'photo',
            'content_id' => $photo['id']
        );
        if($this->main_m->checkBuyContent($data)){
            $photo['unlock'] = true;
        }else{
            $photo['unlock'] = false;
        }

        $this->result['photo']= $photo;
        

        $user = $this->main_m->get_chanel($this->_cid);
        //        print_r($user);exit;
        $star = array(
                'email' => $user['email'],
                'img_url' => $this->getImageUrl($user['chanel_img_url']),
                'img_width' => $this->img_width,
                'img_height' => $this->img_height,
                //            'name' => $user['first_name']
                'name' => $user['name']
        );
        //        print_r($user);exit;
        $this->result['star'] = $star;
    }

    public function response_get_videos(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }

        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        $videos = $this->main_m->getVideos(true, $this->_user_id, $count, $page);
        
        $videos = $this->getPagenation($videos, $this->_page, $this->_cnt_of_page);
        $tags = array('All');
        
        
        foreach($videos as &$video){
            if($video['tags'] && !in_array($video['tags'], $tags) ){
                $tags[] = $video['tags'];
            }
            
            
            if($video['video_url']){
                $video['destination'] = $video['video_url'];
            }else{
                $video['destination'] = $this->getImageUrl($video['destination']);
            }
//            $video['image_path'] =  site_url('image/videoplay/'.str_replace("/", "-",$video['image_path']));
            $img = $video['image_path'];
            $video['image_path'] = $this->getImageUrl($video['image_path']);
            $video['img_width'] = $this->img_width;
            $video['img_height'] = $this->img_height;


            $video['image_path'] = UPLOAD_CLOUD.$img;
            list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
            $video['img_width'] = THUMBNAIL_WIDTH;
            $video['img_height'] = $s_height * THUMBNAIL_WIDTH / $s_width;
            
            $comments = $this->main_m->getComments('video', $video['id']);
            $video['numbersofcomments'] = count($comments);
            //check liked
            $like_row = $this->db->get_where('enr_like', array('user_id'=>$this->_user_id, 'post_type'=>'video', 'content_id'=>$video['id']))->row();
            if($like_row)
                $video['did_like'] = true;
            else
                $video['did_like'] = false;
            //like
            $rows = $this->db->get_where('enr_like', array('post_type'=>'video', 'content_id'=>$video['id']))->result_array();
            $video['numberoflike'] = count($rows);
            /**
            * check if locked or not
            */
            $data = array(
                'user_id' => $this->_user_id,
                'post_type' => 'video',
                'content_id' => $video['id']
            );
            if($this->main_m->checkBuyContent($data)){
                $video['unlock'] = true;
            }else{
                $video['unlock'] = false;
            }
                
        }
        $this->result['videos']= $videos;
        $this->result['filters'] = $tags;

        $user = $this->main_m->get_chanel($this->_cid);
        //        print_r($user);exit;
        $star = array(
                'email' => $user['email'],
                'img_url' => $this->getImageUrl($user['chanel_img_url']),
                'img_width' => $this->img_width,
                'img_height' => $this->img_height,
                //            'name' => $user['first_name']
                'name' => $user['name']
        );
        //        print_r($user);exit;
         $insertMenuVideos = $this->main_m->menu_tracking_details($this->_user_id, 'videos'); //Menu Tracking
        $this->result['star'] = $star;
    }

    public function response_get_shops(){
        $shops = $this->main_m->getProductList();
        
        foreach($shops as &$shop){
            $shop['images'] = unserialize($shop['images']);
            foreach($shop['images'] as $key=> $image){
                $image = $this->getImageUrl($image);
                $shop['images'][$key] = array();
                $shop['images'][$key]['image'] = $image;
                $shop['images'][$key]['img_width'] = $this->img_width;
                $shop['images'][$key]['img_height'] = $this->img_height;
            }
        }
        $this->result['shops']= $shops;

        $tags = $this->main_m->getShpTagOtions($this->_cid);
        foreach($tags as $tag){
            $this->result['filters'][]= $tag;
        }
    
    }

    public function response_get_musics(){
        $musics = $this->main_m->getMusicList();
        foreach($musics as $key=>&$music){
            $music['destination'] = $this->getImageUrl($music['destination']);
            $music['image_path'] = $this->getImageUrl($music['image_path']);
            $music['img_width'] = $this->img_width;
            $music['img_height'] = $this->img_height;
        }
        $this->result['musics']= $musics;
    }

    public function response_get_events(){
        $events = $this->main_m->getTourdateList();
        $this->result['events']= $events;
    }
    
    public function response_set_followuser(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_followed_user_id ){
            $this->result['message'] = "The user id that you want to follow is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        $is_unfollow = $this->_is_unfollow;
        $this->main_m->followUser($this->_user_id, $this->_followed_user_id, $is_unfollow);
                /* -Wrapper functio to follow users- */

        $this->main_m->wrp_followUser($this->_user_id, $this->_followed_user_id, $is_unfollow);
        $this->result['numbers'] = count($this->main_m->getFolloweds($this->_followed_user_id));
    }
    
    public function response_get_rating(){
        
        $search = isset($_REQUEST['search']) ? $_REQUEST['search'] : '';
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : 100;
        $fans = $this->main_m->getFansByRanking(UPLOAD_CLOUD, $this->_cid, $search, $page, $count);
        $this->result['rankings'] = $fans;
    }
    
    public function response_add_text(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $fan = $this->main_m->getFan($this->_user_id);
        if($fan['admin_type'] != 0){
            $this->_user_id = -1;
        }
        $post = array(
            'cid' => $this->_cid,
            'fan_id' => $this->_user_id,
            'description' => $this->_description,
            'tags' => $this->_tags,
            'credit' => $this->_credit,
            'is_publish' => 1,
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => 0
        );
        $feed_id = $this->main_m->addFeedText($post);
        
                /* --wrapper functoion for adding feed -- */
        $this->main_m->wrp_addFeed($post,$feed_id, 'text');
        $this->print_post_detail('text', $feed_id);
    }

    public function response_add_photo(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $fan = $this->main_m->getFan($this->_user_id);
        if($fan['admin_type'] != 0){
            $this->_user_id = -1;
        }

        $post = array(
            'cid' => $this->_cid,
            'fan_id' => $this->_user_id,
            'description' => $this->_description,
            'tags' => $this->_tags,
            'credit' => $this->_credit,
            'url_link' => isset($_REQUEST['url_link']) ? $_REQUEST['url_link'] : '',
            'is_publish' => 1,
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => 0
        );
        $feed_id = $this->main_m->addFeedPhoto($post);
           /* --wrapper functoion for adding feed -- */
        $this->main_m->wrp_addFeed($post,$feed_id, 'photo');
        $this->print_post_detail('photo', $feed_id);
    }

    public function response_add_video(){
        if(!$this->_user_id ){
            $this->result['message'] = "The user id is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        $fan = $this->main_m->getFan($this->_user_id);

        $video_publish = 1;  // default for fans in community

        if($fan['admin_type'] != 0){
            $this->_user_id = -1;
            $video_publish = 0;   // must be transcoding if Admin uploaded
        }


        $post = array(
            'cid' => $this->_cid,
            'fan_id' => $this->_user_id,
            'description' => $this->_description,
            'tags' => $this->_tags,
            'credit' => $this->_credit,
            'video_url' => $this->_video_url,
            'is_publish' => $video_publish,
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => 0
        );
        $feed_id = $this->main_m->addFeedVideoFromApi($post);
               /* --wrapper functoion for adding feed -- */
        $this->main_m->wrp_addFeed($post,$feed_id, 'video');
        $this->result['content_id'] = $feed_id;
    }
    
    public function response_get_push_contents(){
        $rows = $this->main_m->checkPushinfo($this->_user_id);
        $this->result['contents'] = $rows;
    }
    
    public function response_set_block(){
        if(!$this->_self_user_id ){
            $this->result['message'] = "The self user ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_user_id ){
            $this->result['message'] = "The user ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        $this->main_m->setBlockFan($this->_self_user_id, $this->_user_id);
    }
    public function response_unset_block(){
        $this->main_m->setUnblockFan($this->_self_user_id, $this->_user_id);
    }
    
    public function response_get_community_of_follow(){
        $feeds = $this->main_m->getCommunityFeedList(false, $this->_self_user_id, true);
//         $results = array();
//         foreach($feeds as $feed){
//             if($this->main_m->checkFollow($this->_self_user_id, $feed['fan_id'])){
//                 $results[] = $feed;
//             }
//         }
//         $feeds = $results;
        
        $feeds = $this->_filterFeed($feeds, $this->_post_type);
        $feeds = $this->getPagenation($feeds, $this->_page, $this->_cnt_of_page);
        foreach($feeds as &$feed){
            $img = $feed['image_path'];
            $this->parseFeed($feed);
            if($feed['post_type'] != 'photo') {
                $feed['image_path'] = $this->getImageUrl($feed['image_path']);
                $feed['img_width'] = $this->img_width;
                $feed['img_height'] = $this->img_height;
            }
            
            
            if($feed['post_type'] == 'video') {
                $feed['image_path'] = UPLOAD_CLOUD.$img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = $s_height * THUMBNAIL_WIDTH / $s_width;
            }
            elseif($feed['post_type'] == 'photo') {
                $feed['image_path'] = UPLOAD_CLOUD."thumb/". $img;
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $img);
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
                
                    
                $feed['img_small_thumb'] = UPLOAD_CLOUD."watermark/small_thumb/".$img;
                $feed['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
                $feed['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
                
                
                $feed['fullimage_path'] = UPLOAD_CLOUD.$img;
                $feed['fullimg_width'] = $s_width;
                $feed['fullimg_height'] = $s_height;
            }
        }
        $this->result['feeds'] = $feeds;
    }
    
    public function response_send_message(){
        if(!$this->_sender ){
            $this->result['message'] = "The sender ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
            return false;
        }
        if(!$this->_receiver ){
            $this->result['message'] = "The receiver ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
            return false;
        }
        $data = array(
            'sender' => $this->_sender,
            'receiver' => $this->_receiver,
            'message' => $this->_message
        );
        $this->main_m->insertMail($data);
    }
    
    public function response_get_message(){
        if(!$this->_user_id){
            $this->result['status'] = false;
            $this->result['error_code'] = 1;
            $this->result['message'] = 'User ID is required';
            return false;
        }
        
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
        
        $mails = $this->main_m->getMailList($this->_user_id, false, false, $count, $page);
        $this->result['messages'] = $mails;
    }
    
    public function response_get_helpurl(){
        $this->result['url'] = BASE_URL."help.html";
    }
    
    public function response_update_seen(){
        if(!$this->_mail_id){
            $this->result['status'] = false;
            $this->result['message'] = "Mail ID is required";
            $this->result['error_code'] = 1;
            return false;
        }
        
        $this->db->where('id', $this->_mail_id);
        $res = $this->db->update('enr_mail', array('is_seen'=>1));
    }
    
    public function response_buy_content(){
        if(!$this->_user_id){
            $this->result['status'] = false;
            $this->result['message'] = "Fan ID is required";
            $this->result['error_code'] = 1;
            return false;
        }
        if(!$this->_post_type){
            $this->result['status'] = false;
            $this->result['message'] = "Post type is required";
            $this->result['error_code'] = 2;
            return false;
        }
        if(!$this->_content_id){
            $this->result['status'] = false;
            $this->result['message'] = "Content Id is required";
            $this->result['error_code'] = 3;
            return false;
        }
        $data = array(
            'user_id' => $this->_user_id,
            'post_type' => $this->_post_type,
            'content_id' => $this->_content_id
        );
        $this->main_m->buyContent($data);
    }
    public function response_get_poll_quiz_list(){
        $user_id = $this->_user_id;
        
        $feeds = $this->main_m->getHomeFeedList(true, $user_id, true);
        $feeds = $this->_filterFeed($feeds, 'poll_quiz');
        $feeds = $this->getPagenation($feeds, $this->_page, $this->_cnt_of_page);
        foreach($feeds as &$feed){
            $feed['image_path'] = $this->getImageUrl($feed['image_path']);
            $feed['img_width'] = $this->img_width;
            $feed['img_height'] = $this->img_height;
            $this->parseFeed($feed);
        }
        $user = $this->main_m->get_chanel($this->_cid);
//        print_r($user);exit;
        $star = array(
            'email' => $user['email'],
            'img_url' => $this->getImageUrl($user['chanel_img_url']),
            'img_width' => $this->img_width,
            'img_height' => $this->img_height,
            'name' => $user['name']
        );

        $this->result['star'] = $star;
        $this->result['feeds'] = $feeds;
    }
    public function response_get_poll_quiz_content(){
        if(!$this->_content_id){
            $this->result['status'] = false;
            $this->result['message'] = "Content Id is required";
            $this->result['error_code'] = 1;
            return false;
        }
        if(!$this->_post_type){
            $this->result['status'] = false;
            $this->result['message'] = "Post type is required";
            $this->result['error_code'] = 2;
            return false;
        }
        if($this->_post_type == 'poll'){
            $feedPoll = $this->main_m->getFeedPoll($this->_content_id);
            $this->result['feed'] = $feedPoll;
            $pollQuestion = $this->main_m->getFeedPoll_question_answer($this->_content_id);
            $this->result['questions'] = $pollQuestion;
        }elseif($this->_post_type == 'quiz'){
            $feedQuiz = $this->main_m->getFeedQuiz($this->_content_id);
            $this->result['feed'] = $feedQuiz;
            $quizQuestion = $this->main_m->getFeedQuiz_question_answer($this->_content_id);
            $this->result['questions'] = $quizQuestion;
        }
        $this->result['feed']['end_description'] = base_url() . 'html_text?type=' . $this->_post_type . '&content_id=' . $this->_content_id;
        list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD.$this->result['feed']['image_path']);
        $this->result['feed']['image_path'] = $this->getImageUrl($this->result['feed']['image_path']);
        $this->result['feed']['img_width'] = $s_width;
        $this->result['feed']['img_height'] = $s_height;
        
        list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD.$this->result['feed']['background_image_path']);
        $this->result['feed']['background_image_path'] = $this->getImageUrl($this->result['feed']['background_image_path']);
        $this->result['feed']['background_img_width'] = $s_width;
        $this->result['feed']['background_img_height'] = $s_height;
        
        list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD.$this->result['feed']['end_image_path']);
        $this->result['feed']['end_image_path'] = $this->getImageUrl($this->result['feed']['end_image_path']);
        $this->result['feed']['end_img_width'] = $s_width;
        $this->result['feed']['end_img_height'] = $s_height;
//        print_r($this->result);exit;
    }
    
    public function response_poll_answer(){
        if(!$this->_answer_id){
            $this->result['status'] = false;
            $this->result['message'] = "Answer id is required";
            $this->result['error_code'] = 1;
            return false;
        }
        if(!$this->_question_id){
            $this->result['status'] = false;
            $this->result['message'] = "Question id is required";
            $this->result['error_code'] = 1;
            return false;
        }
        if(!$this->_user_id){
            $this->result['status'] = false;
            $this->result['message'] = "User ID is required";
            $this->result['error_code'] = 1;
            return false;
        }
        $this->main_m->insertAnswerHistory($this->_answer_id, $this->_user_id);
        $this->result['ratings'] = $this->main_m->getAnswerPercent($this->_question_id);
    }
    public function response_get_adminlist(){
        $cid = $this->_cid;
       $chanel = $this->main_m->get_chanel($cid);
        
        $admins = array();
        $admins[] = array('email' => $chanel['feedback_email'], 'admin_type' => 0);
        $this->result['admins'] = $admins;
    }
    public function response_quiz_answer(){
        if(!$this->_user_id){
            $this->result['status'] = false;
            $this->result['message'] = "Fan ID is required";
            $this->result['error_code'] = 1;
            return false;
        }
        if(!$this->_correct_count){
            $this->result['status'] = false;
            $this->result['message'] = "The number of correct answer is required";
            $this->result['error_code'] = 2;
            return false;
        }
        if(!$this->_question_count){
            $this->result['status'] = false;
            $this->result['message'] = "The number of question is required";
            $this->result['error_code'] = 3;
            return false;
        }
        if(!$this->_content_id){
            $this->result['status'] = false;
            $this->result['message'] = "The quiz ID is required";
            $this->result['error_code'] = 4;
            return false;
        }
        $data = array(
            'content_id' => $this->_content_id,
            'user_id' => $this->_user_id,
            'correct_count' => $this->_correct_count,
            'question_count' => $this->_question_count
        );
        $this->main_m->insertQuizAnswerHistory($this->_user_id, $data);
    }
    public function response_delete_feed(){
        if(!$this->_user_id){
            $this->result['message'] = "User ID is required.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        if(!$this->_content_id){
            $this->result['message'] = "Content ID is required.";
            $this->result['error_code'] = 2;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        if(!$this->_post_type){
            $this->result['message'] = "Post type is required.";
            $this->result['error_code'] = 3;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        $this->main_m->deleteFeed($this->_user_id, $this->_post_type, $this->_content_id);
    }
    
    public function response_delete_comment(){
        if(!$this->_user_id){
            $this->result['message'] = "User ID is required.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        if(!$this->_content_id){
            $this->result['message'] = "Content ID is required.";
            $this->result['error_code'] = 2;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        if(!$this->_post_type){
            $this->result['message'] = "Post type is required.";
            $this->result['error_code'] = 3;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        if(!$this->_comment_id){
            $this->result['message'] = "Comment ID is required.";
            $this->result['error_code'] = 3;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        $res = $this->main_m->deleteComment($this->_user_id, $this->_content_id, $this->_post_type, $this->_comment_id);
        $this->result['success'] = $res ? 1 : 0;
        

        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id, UPLOAD_CLOUD);
        $this->result['comments_count'] = count($rows);
        $comment_count = isset($_REQUEST['comment_count']) ? $_REQUEST['comment_count'] : -1;
        $rows = $this->main_m->getComments($this->_post_type, $this->_content_id, UPLOAD_CLOUD, $comment_count);
        $this->result['comments'] = $rows;
    }
    public function response_get_post_detail() {
        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : '';
        $id= isset($_REQUEST['content_id']) ? $_REQUEST['content_id'] : '';
        $this->print_post_detail($post_type, $id);
    }
    private function print_post_detail($post_type, $id) {
        $feed = null;
        switch($post_type) {
            case 'text': {
                $feed = $this->main_m->getFeedText($id);
                $feed['id'] = 'text_' . $feed['id'];
                $feed['post_type'] = 'text';
                $feed['image_path'] = '';
                $feed['caption'] = $feed['description'];
                $this->parseFeed($feed);
                break;
            }
            case 'photo': {
                $feed = $this->main_m->getFeedPhoto($id);
                $feed['id'] = 'photo_' . $feed['id'];
                $feed['post_type'] = 'photo';
                $this->parseFeed($feed);

                if($feed['fan_id'] == '' || $feed['fan_id'] == -1){

                    $watermark_selctor = "watermark/";
                }else{
                    $watermark_selctor = "";
                }
                 
                $feed['image_path'] = UPLOAD_CLOUD.$watermark_selctor."thumb/".$feed['destination'];
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $feed['destination']);
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
        
                $feed['img_small_thumb'] = UPLOAD_CLOUD.$watermark_selctor."small_thumb/".$feed['destination'];
                $feed['img_small_thumb_width'] = SMALL_THUMBNAIL_WIDTH;
                $feed['img_small_thumb_height'] = round($s_height * SMALL_THUMBNAIL_WIDTH / $s_width);
        
                $feed['fullimage_path'] = UPLOAD_CLOUD.$watermark_selctor.$feed['destination'];
                $feed['fullimg_width'] = $s_width;
                $feed['fullimg_height'] = $s_height;
                 
                $feed['caption'] = $feed['description'];
                 
                break;
            }
            case 'video': {
                $feed = $this->main_m->getFeedVideo($id);
                 
                 
        
                $feed['id'] = 'video_' . $feed['id'];
                $feed['post_type'] = 'video';
                $this->parseFeed($feed);
        
                list($s_width, $s_height) = getimagesize(UPLOAD_CLOUD . $feed['image_path']);
                $feed['image_path'] = UPLOAD_CLOUD.$feed['image_path'];
                $feed['img_width'] = THUMBNAIL_WIDTH;
                $feed['img_height'] = round($s_height * THUMBNAIL_WIDTH / $s_width);
                 
                if( UPLOAD_CLOUD . $feed['destination']!="" )
                    $feed['video_url'] = UPLOAD_CLOUD . $feed['destination'];
                 
        
                $feed['caption'] = $feed['description'];
                 
                break;
            }
            case 'quiz': {
                $feed= $this->main_m->getFeedQuiz($id);
                $feed['answers'] = $this->main_m->getFeedQuiz_question_answer($id);
                 
                $feed['id'] = 'quiz_' . $feed['id'];
                $feed['post_type'] = 'quiz';
        
                $this->parseFeed($feed);
        
                $feed['image_path'] = $this->getImageUrl($feed['image_path']);
                $feed['img_width'] = $this->img_width;
                $feed['img_height'] = $this->img_height;
        
        
                $feed['caption'] = $feed['description'];
                 
                break;
            }
            case 'poll': {
                $feed = $this->main_m->getFeedPoll($id);
                $feed['answers'] = $this->main_m->getFeedPoll_question_answer($id);
        
        
                $feed['id'] = 'poll_' . $feed['id'];
                $feed['post_type'] = 'poll';
        
                 
                $this->parseFeed($feed);
                 
                $feed['image_path'] = $this->getImageUrl($feed['image_path']);
                $feed['img_width'] = $this->img_width;
                $feed['img_height'] = $this->img_height;
                 
        
                $feed['caption'] = $feed['description'];
                 
                 
                break;
            }
            default: {
                $this->result['message'] = "Post type is invalid.";
                $this->result['error_code'] = 1;
                $this->result['status'] = false;
            }
        }
        $this->result['post'] = array($feed);
        $user = $this->main_m->get_chanel($this->_cid);
        //        print_r($user);exit;
        $star = array(
                'email' => $user['email'],
                'img_url' => $this->getImageUrl($user['chanel_img_url']),
                'img_width' => $this->img_width,
                'img_height' => $this->img_height,
                //            'name' => $user['first_name']
                'name' => $user['name']
        );
        //        print_r($user);exit;
        $this->result['star'] = $star;
        $this->response($this->result);
    }
    public function response_forget_password() {
        
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        if(empty($email)) {
            
            $this->result['message'] = "Email is required.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        elseif(($fan = $this->main_m->getFanByEmail($email)) == NULL) {
            
            $this->result['message'] = "Email is invalid.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        elseif($fan['facebook']) {
            
            $this->result['message'] = "Please login by facebook.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        else {
            /*$new_password = $this->randomPassword();
            $hash_pass = $this->ion_auth->hash_password($new_password);
            //update password
            $update = array(
                'password' => $hash_pass
            );
            $this->main_m->db->where('id', $this->_user_id);
            $this->main_m->db->update('enr_fan', $update);
            */
            $verification_code = $this->randomPassword(32);
            //update password
            $update = array(
                'verification_code' => $verification_code
            );
            $this->main_m->db->where('id', $fan['id']);
            $this->main_m->db->update('enr_fan', $update);
            
            
            $this->email->clear();
            $this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
            $this->email->to($fan['email']);
            $this->email->subject($this->config->item('site_title', 'ion_auth') . ' - You have requested to reset your password.');
            
            
            $message = 'You have requested to reset your password recently.<BR><BR>
If it was not you, please ignore this message.<BR><BR>
Please click following link to reset your password.<BR><BR>
<a href="' . site_url('verify') . '?v='.$verification_code.'">' . site_url('verify') . '?v='.$verification_code.'</a>';
            $this->email->message($message);
            $this->email->send();
        }
    }
    public function response_get_like_users() {
        $content_id = isset($_REQUEST['content_id']) ? $_REQUEST['content_id'] : '';
        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : '';
        if(empty($content_id)) {
                
            $this->result['message'] = "Content id is required.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        elseif(empty($post_type)) {
                
            $this->result['message'] = "Post type is required.";
            $this->result['error_code'] = 2;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        else {
            $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
            $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
            
            $query = 'select enr_fan.id as user_id, enr_fan.admin_type, enr_fan.name as userName, IF(enr_fan.img_url="", "", concat("' . UPLOAD_CLOUD . '", enr_fan.img_url)) as img_url 
                    from enr_like join enr_fan on enr_like.user_id = enr_fan.id
                    where enr_like.content_id = ? and enr_like.post_type = ?';
            if($count != -1) {
                $query .= '
                        limit ' . ($count * $page) . ', ' . $count;
            }
            $this->result['users'] = $this->main_m->db->query($query, array($content_id, $post_type))->result();
            
            $this->result['error_code'] = 0;
            $this->result['message'] = '';
            $this->result['status'] = true;
            
            $this->response($this->result);
        }
    }
    public function response_get_approval_users() {
        $content_id = isset($_REQUEST['content_id']) ? $_REQUEST['content_id'] : '';
        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : '';
        if(empty($content_id)) {
    
            $this->result['message'] = "Content id is required.";
            $this->result['error_code'] = 1;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        elseif(empty($post_type)) {
    
            $this->result['message'] = "Post type is required.";
            $this->result['error_code'] = 2;
            $this->result['status'] = false;
            $this->response($this->result);
        }
        else {
            $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : -1;
            $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
                
            $query = 'select enr_fan.id as user_id, enr_fan.admin_type, enr_fan.name as userName, IF(enr_fan.img_url="", "", concat("' . UPLOAD_CLOUD . '", enr_fan.img_url)) as img_url,
                    (select count(*) from enr_approval 
                    where enr_approval.user_id = enr_fan.id and enr_approval.content_id = ? and enr_approval.post_type = ?) as approval
                    from enr_fan
                    where enr_fan.is_draft = 1 and enr_fan.is_block = 0 and enr_fan.is_suspend = 0 and enr_fan.admin_type is null';
            if($count != -1) {
                $query .= '
                        limit ' . ($count * $page) . ', ' . $count;
            }
            $this->result['users'] = $this->main_m->db->query($query, array($content_id, $post_type))->result();
                
            $this->result['error_code'] = 0;
            $this->result['message'] = '';
            $this->result['status'] = true;
                
            $this->response($this->result);
        }
    }
    function randomPassword($length) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    function response_get_brightcove_media_id() {
        $this->load->library('BrightCove');
        
        $video_id = isset($_REQUEST['content_id']) ? $_REQUEST['content_id'] : '';
        
        $query = 'select brightcove_media_id from enr_video where id = ?';
        
        $result = $this->main_m->db->query($query, array($video_id))->result_array();
        
        if(count($result) == 0) {
            $this->result['error_code'] = 1;
            $this->result['message'] = 'no video found';
        }
        else {
            if($result[0]['brightcove_media_id'] == '') {
                $this->result['media_id'] = $this->main_m->create_brightcove_media_id($video_id);
            }
            else {
                $this->result['media_id'] = $result[0]['brightcove_media_id'] ;
            }
        }
        
        $this->response($this->result);
    }
    
    function response_get_credit_card_info() {
        $this->load->library('paypal');
        
        $result = array();
        if(!$this->_user_id ){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
        }
        else if(!$this->_cid ){
            $this->result['message'] = "The chanel ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
        }
        else {
            $fan = $this->main_m->getFan($this->_user_id);
            
            if($fan['paypal_card_token'] == '') {
                $this->result['card_info'] = '';
            }
            else {
                $card = $this->paypal->getCreditCard($fan['paypal_card_token']);
                $len = strlen($card['number']);
                $tmp = substr($card['number'], -4);
                for($i = 0; $i < $len - 4; $i++) $tmp = 'X' . $tmp;
                $card['number'] = $tmp;
                unset($card['expire_month']);
                unset($card['expire_year']);
                unset($card['cvv2']);
                $this->result['card_info'] = $card;
            }
        }

        $this->response($this->result);
    }
    function response_clear_credit_card_info() {
        $result = array();
        if(!$this->_user_id ){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
        }
        else if(!$this->_cid ){
            $this->result['message'] = "The chanel ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
        }
        else {
            $query = 'update enr_fan set paypal_card_token = "" where id = ?';
            $this->main_m->db->query($query, array($this->_user_id));
        }

        $this->response($this->result);
    }
    function response_create_payment() {
        $this->load->library('paypal');
        
        $result = array();
        if(!$this->_user_id ){
            $this->result['message'] = "The fan ID is required.";
            $this->result['errorcode'] = 1;
            $this->result['status'] = false;
        }
        else if(!$this->_cid ){
            $this->result['message'] = "The chanel ID is required.";
            $this->result['errorcode'] = 2;
            $this->result['status'] = false;
        }
        else {
            $fan = $this->main_m->getFan($this->_user_id);
            
            $pay_option = $this->input->post('pay_option', 2);
            if($pay_option == 1) {
                $card_info = array(
                    'type' => $this->input->post('type', ''),
                    'number' => $this->input->post('number', ''),
                    'expire_month' => $this->input->post('expire_month', ''),
                    'expire_year' => $this->input->post('expire_year', ''),
                    'cvv2' => $this->input->post('cvv2', ''),
                    'first_name' => $this->input->post('first_name', ''),
                    'last_name'  => $this->input->post('last_name', '')
                );
                $card_id = $this->paypal->createCreditCard($card_info);
                
                if($card_id != '') {
                    $query = 'update enr_fan set paypal_card_token = ? where id = ?';
                    $this->main_m->db->query($query, array($card_id, $this->_user_id));
                }
            }
            elseif($pay_option == 0){
                $fan = $this->main_m->getFan($this->_user_id);
                
                $card_id = $fan['paypal_card_token'];
            }
            elseif($pay_option == 2) {
                
            }
            
            if($pay_option != 2 && empty($card_id)) {
                $this->result['message'] = 'Invalid credit card';
                $this->result['errorcode'] = 3;
                $this->result['status'] = false; 
            }
            else {
                $credit_count = $this->input->post('credit_count', 0);
                $item = array(
                    'currency' => 'USD',
                    'amount' => $credit_count * 0.0099,
                    'shipping_price' => 0,
                    'tax' => 0,
                    'description' => $credit_count . ' credits purchased @ USD ' . ($credit_count * 0.0099)
                );
                

                if($pay_option != 2) {
                    $payment_id = $this->paypal->createPayment($card_id, $item);
                
                    if(empty($payment_id)) {
                        $this->result['message'] = 'Payment failed.';
                        $this->result['errorcode'] = 4;
                        $this->result['status'] = false;
                    }
                    else {
                        
                        //updates credits
                        $credits = $fan['credit'] + $credit_count;
    
                        $query = 'update enr_fan set credit = ? where id = ?';
                        $this->main_m->db->query($query, array($credits, $this->_user_id));
                        
                        $this->result['message'] = 'Ok.';
                        $this->result['errorcode'] = 0;
                        $this->result['status'] = true;
                        $this->result['credits'] = $credits;
                    }
                }
                else {

                    $urls = array(
                        'success' => site_url('payment?success=true&user_id='.$this->_user_id.'&channel_id=' . $this->_cid),
                        'cancel' => site_url('payment?sucess=false')        
                    );

                    $response = $this->paypal->createPaymentByPaypal($item, $urls);
                    
                    if($response['approve_link'] != '') {
                        
                        $query = 'insert into enr_paypal(cid, user_id, payment_id, credits) values("' . $this->_cid . '", "' . $this->_user_id . '", "' . $response['payment_id'] . '","' . $credit_count . '")';
                        $this->main_m->db->query($query);
                        
                        $this->result['message'] = 'Ok.';
                        $this->result['errorcode'] = 0;
                        $this->result['status'] = true;
                        $this->result['approve_link'] = $response['approve_link'];
                    }
                    else {
                        $this->result['message'] = 'Payment failed.';
                        $this->result['errorcode'] = 4;
                        $this->result['status'] = false;
                    }
                }
            }
        }
        
        $this->response($this->result);
    }
        
            /* Tracking code */

    public function response_track_video() {
        $userid = $_REQUEST['user_id'];
        $contentid = $_REQUEST['content_id'];
        $insertTrackVideo = $this->main_m->track_video($userid, $contentid, "video");
    }

    public function response_track_image() {
        $userid = $_REQUEST['user_id'];
        $contentid = $_REQUEST['content_id'];
        $insertTrackVideo = $this->main_m->track_image($userid, $contentid, "image");
    }

    public function response_track_like_userevent() {
        $userid = $_REQUEST['user_id'];
        $contentid = $_REQUEST['content_id'];
        $type = $_REQUEST['type'];
        $insertTrackEvent = $this->main_m->track_like_event($userid, $contentid, $type);
    }

    public function response_track_social_share_events() {
        $userid = $_REQUEST['user_id'];
        $contentid = $_REQUEST['content_id'];
        $type = $_REQUEST['type'];
        $sharetype = $_REQUEST['share_type'];
        $insertTrackShareEvent = $this->main_m->track_share_event($userid, $contentid, $type, $sharetype);
    }

    public function response_shoplinks() {
        $userid = $_REQUEST['user_id'];
        $linkid = $_REQUEST['link_id'];
        $insertMenuShoplink = $this->main_m->shop_link_details($linkid, $userid);
    }

    /* End of Tracking code */

// End of SCSS_TRACKING
        
        
        
}

