<?php
// This should include an init, saveslidervideostomemcache function, savechannelstomemcache function, savehomepagevideostomemcache function.
// May have to create categories for slider and/or homepage that are separate from are current category structure.
// Super admin will be able to select which videos go in the home slider

class HubManager extends FrontCoreManager
{
    private $_xSql;
    private $_nCacheObjectDuration = MEMCACHE_DURATION_OFLO;

    public function init()
    {
        $this->_xSql = AppController::getInstance()->getConnection();
    }

    /**
     * Get O-Flo homepage slider content.
     *
     * @param $p_sSlug
     * @return mixed
     */
    public function getSliderContent($p_sSlug){
        $l_xCached = $this->getSliderContentFromMemcacheAndStoreIfFail($p_sSlug);

        return $l_xCached;
    }

   public function getSliderBanners($banners){
        $l_xCached = $this->getSliderBannersFromMemcacheAndStoreIfFail($banners);

        return $l_xCached;
    }

    /**
     * Get O-Flo Channels.
     *
     * @param $p_sSlug
     * @return mixed
     */
    public function getOfloChannels($p_sSlug)
    {
        $l_xCached = $this->getOfloChannelsFromMemcacheAndStoreIfFail($p_sSlug);

        return $l_xCached;
    }

    /**
     * Get O-Flo homepage video grid.
     *
     * @param $p_sSlug
     * @return mixed
     */
    public function getHomepageVideoGrid($p_sSlug)
    {
        $l_xCached = $this->getHomepageVideoGridFromMemcacheAndStoreIfFail($p_sSlug);

        return $l_xCached;
    }

    //   public function getHomepageTrending($p_sSlug)
    // {
    //     $l_xCached = $this->getHomepageTrendingFromMemcacheAndStoreIfFail($p_sSlug);

    //     return $l_xCached;
    // }

    //  public function getHomepagePriorityVideoGrid($p_sSlug)
    // {
    //     $l_xCached = $this->getHomepagePriorityGridFromMemcacheAndStoreIfFail($p_sSlug);

    //     return $l_xCached;
    // }


    public function getSearchResults($search)
    {
        $l_xCached = $this->getSearchResultsFromMemcacheAndStoreIfFail($search);

        return $l_xCached;
    }


    // MEMCACHING

    public function getSliderContentFromMemcacheAndStoreIfFail($memcache_key_name, $p_bBypassMemcache = false)
    {
         date_default_timezone_set('UTC');
         $utc_time = date("Y-m-d h:i:sa"); 

        date_default_timezone_set('America/Los_Angeles');
         $pacific_time = date("Y-m-d h:i:sa"); 

         $hours_diff = $utc_time - $pacific_time;

        $l_sMemcacheKey = Memcaching::makeKey($memcache_key_name);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if ($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = "SELECT * FROM
                        (SELECT ev.id, ev.cid, ev.slider_thumb, 'video' as content_type, ec.url,
                              IF (ev.feed_date != 0, ev.feed_date, ev.start_date) as feed_date
                           FROM enr_video ev
                           INNER JOIN enr_chanel ec ON ec.id = ev.cid
                           WHERE ev.is_oflo_slider = 'y' AND ev.is_publish = 1 
                           ORDER BY ev.feed_date DESC
                       ) as t1
                       WHERE t1.feed_date <= DATE_SUB(NOW(), INTERVAL $hours_diff HOUR)
                       LIMIT 16";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;

        } else {

            return $l_aCachedContent['value'];
        }
    }

    public function getOfloChannelsFromMemcacheAndStoreIfFail($memcache_key_name, $p_bBypassMemcache = false)
    {
        $l_sMemcacheKey = Memcaching::makeKey($memcache_key_name);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if ($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = "SELECT ec.*, ec.is_oflo as 'ec_is_oflo', u.is_oflo as 'u_is_oflo' FROM enr_chanel ec
                       LEFT JOIN enr_design ed ON ed.cid = ec.id
                       LEFT JOIN users u ON u.id = ec.user_id
                       WHERE ec.is_oflo = 'y'
                       AND u.is_oflo='y'
                       ORDER BY ec.name ASC";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;

        } else {

            return $l_aCachedContent['value'];
        }
    }


    public function getHomepageVideoGridFromMemcacheAndStoreIfFail($memcache_key_name, $p_bBypassMemcache = false)
    {
        //  date_default_timezone_set('UTC');
        //  $utc_time = date("Y-m-d h:i:sa"); 

        // date_default_timezone_set('America/Los_Angeles');
        //  $pacific_time = date("Y-m-d h:i:sa"); 

        //  $hours_diff = $utc_time - $pacific_time;

        $l_sMemcacheKey = Memcaching::makeKey($memcache_key_name);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);


        if ($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = " SELECT * FROM (
                               SELECT ec.url, ev.id, ev.description, ev.video_type, ev.image_path,
                                    IF (ev.feed_date != 0, UNIX_TIMESTAMP(ev.feed_date), UNIX_TIMESTAMP(ev.start_date)) as new_feed_date
                               FROM enr_video ev
                               LEFT JOIN enr_chanel ec ON ec.id = ev.cid
                               WHERE ev.is_oflo = 'y'  AND ev.is_publish= 1
                               ORDER BY feed_date DESC) as t1
                               WHERE t1.new_feed_date <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 HOUR))
                           LIMIT 16                       

                       ";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;

        } else {

            return $l_aCachedContent['value'];
        }
    }

    // public function getHomepagePriorityGridFromMemcacheAndStoreIfFail($memcache_key_name, $p_bBypassMemcache = false)
    // {
    //     $l_sMemcacheKey = Memcaching::makeKey($memcache_key_name);
    //     $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    //     if ($p_bBypassMemcache || !$l_aCachedContent){

    //         $l_sSql = "SELECT p.duration, ec.url, ev.id, ev.video_type, ev.description, ev.image_path, ev.time_stamp, ev.is_publish, ev.start_date 
    //                    FROM enr_video ev
    //                    RIGHT JOIN enr_chanel ec ON ec.id = ev.cid
    //                    RIGHT JOIN priority p ON p.content_id= ev.id
    //                    WHERE p.is_priority='y' AND ev.is_publish='1'
    //                    ORDER BY p.priority_number ASC

    //                    LIMIT 20";

    //         $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

    //         Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

    //         return $l_aData;

    //     } else {

    //         return $l_aCachedContent['value'];
    //     }
    // }


    // public function getHomepageTrendingFromMemcacheAndStoreIfFail($memcache_key_name, $p_bBypassMemcache = false)
    // {
    //     $l_sMemcacheKey = Memcaching::makeKey($memcache_key_name);
    //     $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    //     if ($p_bBypassMemcache || !$l_aCachedContent){

    //         $l_sSql = "SELECT ec.url, ev.id, ev.video_type, ev.description, ev.image_path, ev.time_stamp, ev.is_publish,(orm.video_views + ormb.page_views_ga) AS overall_views FROM enr_video ev
    //                    RIGHT JOIN enr_chanel ec ON ec.id = ev.cid
    //                    RIGHT JOIN oflo_reports_monthly orm ON orm.content_id= ev.id AND orm.is_mobile = 1
    //                    RIGHT JOIN oflo_reports_monthly ormb ON ormb.is_mobile = 0
    //                    WHERE ev.is_publish='1'
    //                    ORDER BY overall_views DESC
    //                    LIMIT 20";

    //         $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

    //         Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

    //         return $l_aData;

    //     } else {

    //         return $l_aCachedContent['value'];
    //     }
    // }
    

    public function getSearchResultsFromMemcacheAndStoreIfFail($search, $p_bBypassMemcache = false)
    {
        
        //  date_default_timezone_set('UTC');
        //  $utc_time = date("Y-m-d h:i:sa"); 

        // date_default_timezone_set('America/Los_Angeles');
        //  $pacific_time = date("Y-m-d h:i:sa"); 

        //  $hours_diff = $utc_time - $pacific_time;

        $l_sMemcacheKey = Memcaching::makeKey(HubManager::returnItemKey($search));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if ($p_bBypassMemcache || !$l_aCachedContent){

            $search = $this->_xSql->db_escape_string($search);

            $searchTerms = explode(' ', $search);
            $searchTermBits = array();

            foreach ($searchTerms as $term) {
                $term = trim($term);
                if (!empty($term)) {
                    $searchTermBits[] = " AND ev.description LIKE '%$term%'";
                }
            }

            $l_sSql = "SELECT *, SUM(views) as total_views FROM
                           (SELECT ec.url, ev.id, ev.description, ev.video_type, ev.image_path,
                                IF (ev.feed_date != 0, UNIX_TIMESTAMP(ev.feed_date), UNIX_TIMESTAMP(ev.start_date)) as feed_date, 'video' as content_type,
                                   CASE 
                                    WHEN orm.is_mobile = 1
                                      THEN orm.video_views
                                      ELSE  orm.page_views_ga
                                END as views
                           FROM enr_video ev  
                           LEFT JOIN oflo_reports_monthly orm ON ev.id = orm.content_id
                           LEFT JOIN enr_chanel ec ON ec.id = ev.cid
                           WHERE ev.is_oflo = 'y' AND ev.is_publish= 1
                           ".implode('', $searchTermBits)."

                       ) as tbl1
                       WHERE tbl1.feed_date <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 HOUR))
                       GROUP BY id
                       ORDER BY feed_date DESC
                       LIMIT 20";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));
          
           
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;


        } else {

            return $l_aCachedContent['value'];
        }
    }

    // STATIC

    public static function returnItemKey($p_sSlug){
        return 'oflo-hub-object-'.$p_sSlug;
    }
}
?>