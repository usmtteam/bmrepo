<?php 


class Memcaching extends Memcache{

	private static  $_xInstance = null;
	private $_bConnected = false;
	private $_nFailedAttempts = 0;

	public static function getInstance() { 
		
    	if (!self::$_xInstance){
        	self::$_xInstance = new Memcaching(); 
        	self::$_xInstance->connectToResource();
        }
		return self::$_xInstance; 
	}  

	private function connectToResource(){
		
		if(!$this->connect(MEMCACHE_SERVER, 11211)){
			if($this->_nFailedAttempts++ < 5){
				usleep(1000000 * 0.25);
				$this->connectToResource();
			}else{
				echo "memcache fail ".MEMCACHE_SERVER;
				exit;
			}
		}else{
			$this->_bConnected = true;
		}
	}

	public function store($p_sMemcacheKey,$p_sValue,$p_nDuration=30,$p_sEchoValue = false,$p_aObjects = null){

		if($this->_bConnected){
			if(!$p_sMemcacheKey)
				$p_sMemcacheKey = Memcaching::makeDefaultPageKey();
		
			$this->set($p_sMemcacheKey,array('value'=>$p_sValue,'objects'=>$p_aObjects),false,$p_nDuration);
		}
		if($p_sEchoValue)
			echo $p_sValue;
	}

	public function getKey($p_sKey){
		if($this->_bConnected){
			return $this->get($p_sKey);
		}
		return;
	}

	public function removeKey($p_sKey){
		if($this->_bConnected){	
			$this->delete($p_sKey);
		}
	}

	public function pushIntoCronUpdate($p_sType,$p_aArgs){
		if($this->_bConnected){
			$l_sMemcacheKey = $p_sType;
			$l_xCachedData = $this->getKey($l_sMemcacheKey);
			if(!$l_xCachedData){
				$l_xCachedData = array();
			}

			$l_aObjects = $l_xCachedData['objects'];
			$l_aObjects[] = $p_aArgs;
			$this->store($l_sMemcacheKey,1,0,false,$l_aObjects);
		}
	}


	public function checkAndReturnFullCache($p_sMemcacheKey = null){

		if($this->_bConnected){

			if(!$p_sMemcacheKey)
				$p_sMemcacheKey = Memcaching::makeDefaultPageKey();

			if(!$l_sCachedData = Memcaching::getInstance()->getKey($p_sMemcacheKey) ){
				ob_start();
			}else{
				echo $l_sCachedData['value'];
				echo "<style> #container{ border:1px solid #f00}</style>";
				global $l_sCachedDataObjects;
				$l_sCachedDataObjects = $l_sCachedData['objects'];
				return true;
			}
		}
		return false;
	}

	
	public static function makeKey($p_sStr){
		return MEMCACHE_KEY_PREPEND.$p_sStr.MEMCACHE_KEY_CACHE_RELEASE;
	}

	public static function makePageKeyWithSlug($p_sSlug){
		return 'page_'.md5($p_sSlug);
	}

	public static function makeDefaultPageKey(){
		return 'page_'.md5($_SERVER['REDIRECT_URL']);
	}

	public static function makePageKeyWithURLParameters(){
		return 'page_'.md5($_SERVER['REDIRECT_URL'].'?'.$_SERVER['QUERY_STRING']);
	}
	
}

 