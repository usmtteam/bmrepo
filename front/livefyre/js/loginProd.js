function ofloGoToLogin() {
    ofloLoginRedirect();
    return 2;
}

function ofloGoToLoginDelayed() {
    window.setTimeout(ofloLoginRedirectDelayed, 7500);
    return 0;
}

function ofloLoginRedirect() {
    Livefyre.auth.login();
}

function ofloLoginRedirectDelayed() {
    ofloLoginRedirect();
    webkit.messageHandlers.redirectedToLogin.postMessage('User redirect to login');
}

function livefyreInit() {
    var theApi = 'fyre.conv#3';
    var theApp = 'main';
    var theNetwork = 'starclub.fyre.co';
    var theSiteId = "379527";
    var theArticleId = 'prod-oflo-2400';
    var theEl = 'livefyre-app-prod-oflo-2400';
    
    Livefyre.require([theApi], function(Conv) {
        new Conv(
        { network: theNetwork },
        [{
            app: theApp,
            siteId: theSiteId,
            articleId: theArticleId,
            el: theEl,
        }],
        
        function (widget) {    
            widget.on('userLoggedIn', function (data) {
                webkit.messageHandlers.userLoggedIn.postMessage(data);
            });
                 
            widget.on('userLoggedOut', function (data) {
                webkit.messageHandlers.userLoggedOut.postMessage(data);
            });
                 
            widget.on('initialRenderComplete', function (data) {
                webkit.messageHandlers.initialRenderComplete.postMessage(data);
            });
        });
    });
}

livefyreInit();
