<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Facebook Class.
 */

//require(__DIR__.'/../config/settings.php');
//require_once(FACEBOOK_OAUTH_CLASS);

//define('FACEBOOK_APP_ID','265098000358000');
//define('FACEBOOK_APP_SECRET','d88198b3a3d59056fdb186a6170c0c80');

require(__DIR__.'/../config/constants.php');

class Facebook_Star{

//    // We'll use a constructor, as you can't directly call a function
//    // from a property definition.

    function get_Facebook_Accounts($fb){

        // Create our Application instance
//        $fb = new Facebook(array('appId'  => FACEBOOK_APP_ID,'secret' => FACEBOOK_APP_SECRET));
        if (empty($fb)){ // non-CI use
            $fb = new Facebook\Facebook(
                array(
                    'app_id'  => FACEBOOK_APP_ID,
                    'app_secret' => FACEBOOK_APP_SECRET,
                    'default_graph_version' => 'v2.6',
                )
            );
        }

        // Facebook logout to release user access_token
//        $logoutUrl = $fb->getLogoutUrl();
//        echo $logoutUrl;
//        exit;

//        $user = $fb->getUser(); // Get User ID

        $helper = $fb->getRedirectLoginHelper();
        $permissions = array('scope' => 'public_profile,email,manage_pages,publish_actions,read_insights,publish_pages'); // optional

        // We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.

        try {
            if (isset($_SESSION['facebook_access_token'])) {
                $accessToken = $_SESSION['facebook_access_token'];
            } else {
                $accessToken = $helper->getAccessToken();
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $error = 'Graph returned an error: ' . $e->getMessage();
//            echo '<BR>'.$error;
            error_log($error);
            return array('show_errors' => $error);

            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $error = 'Facebook SDK returned an error: ' . $e->getMessage();
//            echo $error;
            error_log($error);
            return array('show_errors' => $error);

            exit;
        }

//        echo "tried";
//        exit;

        if (isset($accessToken)) {
            
            if (isset($_SESSION['facebook_access_token'])) {
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            } else {
                // getting short-lived access token
                $_SESSION['facebook_access_token'] = (string) $accessToken;
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
                // setting default access token to be used in script
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }

//            echo "<BR>\$_SESSION['facebook_access_token']: ";
//            var_dump($_SESSION['facebook_access_token']);

            // redirect the user back to the same page if it has "code" GET variable
            if (isset($_GET['code'])) {

//              echo "<BR>\$_GET['code']: ";
//              var_dump($_GET['code']);
//              exit;

                // getting basic info about user
                try {
                    $profile_request = $fb->get('/me?fields=name,first_name,last_name,email,permissions');
                    $profile = $profile_request->getGraphNode()->asArray();
                    $profile['access_token'] = $accessToken->getValue();
                    $profile_accts = [];
                    $publish_actions_permission = FALSE;
                    $manage_pages_permission    = FALSE;

                    foreach($profile['permissions'] as $permission_arr){

//                        echo "<br><br>\$permission_arr: ";
//                        var_dump($permission_arr);

                        $pa_key = array_search('publish_actions', $permission_arr);

//                        echo "<BR>\$pa_key: ";
//                        var_dump($pa_key);

                        $mpp_key = array_search('manage_pages', $permission_arr);

                        if ($pa_key !== FALSE && $permission_arr['status'] == "granted") {

                            $publish_actions_permission = TRUE;
//                            echo "<br><br>\$publish_actions_permission ";

                        } elseif ($mpp_key !== FALSE && $permission_arr['status'] == "granted"){

                            $manage_pages_permission = TRUE;
//                            echo "<br><br>\$manage_pages_permission ";
                        }
                    }

                    // Check user and manage pages permissions
                    if ($publish_actions_permission === FALSE){

//                        echo "<BR>Do not have user permissions";
//                      exit;

                        return array('show_errors' => "Did not okay user permissions");

                    } elseif ($manage_pages_permission === FALSE){
//
////                        echo "<BR>Do not have manage pages permissions";
////                      exit;

                    } elseif ($manage_pages_permission === TRUE) {

//                        echo "<BR>We have manage pages permissions";

                        $profile_accts_request = $fb->get('/me/accounts?fields=access_token,name,category,perms');
                        $profile_accts = $profile_accts_request->getGraphEdge()->asArray();
//                        var_dump($profile_accts);

                        array_unshift($profile_accts, $profile);

//                        echo "<BR>New \$profile_accts: ";
//                        var_dump($profile_accts);
                    }

                    if($publish_actions_permission === TRUE && $manage_pages_permission === FALSE) {

//                        echo "<BR>We have user permissions ONLY";
                        $profile_accts[] = $profile;
                    }

//                    echo "<BR>New \$profile_accts: ";
//                    var_dump($profile_accts);

                    return $profile_accts;
//                    exit;

                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    // When Graph returns an error
                    $error = 'Graph returned an error: ' . $e->getMessage();
//                    echo '<BR>'.$error;
                    error_log($error);

//            session_destroy();
//                    unset($_SESSION['facebook_access_token']);
//            exit;

                    // redirecting user back to app login page
//        header("Location: ./");
//        echo '<script type="text/javascript"> document.location.href ="./"</script>';
//            echo '<script type="text/javascript"> document.location.href ="'.$_SERVER['SCRIPT_NAME'].'"</script>';
                    return array('show_errors' => $error);
//                    exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    // When validation fails or other local issues
                    $error = 'Facebook SDK returned an error: ' . $e->getMessage();
//                    echo $error;
//                    error_log($error);
                    return array('show_errors' => $error);
//                    exit;
                }

                // printing $profile array on the screen which holds the basic info about user
                // Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
            }
        }

// replace your website URL same as added in the developers.facebook.com/apps e.g. if you used http instead of https and you used non-www version or www version of your website then you must add the same here
        $loginUrl = $helper->getLoginUrl("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], $permissions);

//    echo "<BR>\$loginUrl: ";
//    var_dump($loginUrl);
//    exit;
//    echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
        echo '<BR>Redirecting you to Facebook ...';
//        unset($_SESSION['facebook_access_token']);
        echo '<script type="text/javascript"> document.location.href ="'.$loginUrl.'"</script>';
//        header("Location: " .$loginUrl);

//        exit;

    }
}

?>