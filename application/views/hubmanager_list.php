<style>

</style>
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Hub Manager
                    <small></small>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('hub_master/hubmanager_add'); ?>" class="btn blue">
                        Add New 
                        </a>
                    </div>
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Hub Name</th>
                            <th class="span2">Edit</th>
                            <th class="span2">Delete</th>
                        </tr>
                    </thead>
                    <tbody>  
                           
                        <tr>
                         
                       
                            <?php foreach($hubmanager_list as $hub): ?>
                       
                            <td>
                                <?php echo $hub['name']; ?>
                                                          
                            </td>

                            <td class="center" style="text-align: center;">
                          
                            <?php
                                echo 
                                '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="'.site_url('hub_master/hubmanager_edit/'.$hub['id']).'">'.
                                '<i class="icon-edit" ></i>Edit'.
                                '</a>';
                            ?>
                        
                            </td>
                            <td class="center" style="text-align: center;">
                                
                            <?php
                                echo 
                                '<a class="btn" style="padding-left: 1px; padding-right: 17px;" href="javascript:confirm_deletion(\''.$hub['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                       
                            </td>
                        </tr>

                             
            
           
                               <?php endforeach; ?> 
                 

               
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    function confirm_deletion(id) {
        if(confirm("Do you want to delete this hub?")) {
            document.location.href = "<?php echo site_url('hub_master/confirm_deletion'); ?>/" +id;
        }
    }    
</script>
