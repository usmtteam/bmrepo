<?php


class ItemManager extends FrontCoreManager{

    private $_xSql;
    private $_nCacheObjectDuration = 4;


    public function init(){
        $this->_xSql = AppController::getInstance()->getConnection();
    }

    public function getByIdWithTypeId($p_nAssetId,$p_nTypeId,$p_bBypassMemcache = false){

        $l_xCached = $this->getFromMemcacheAndStoreIfFail($p_nAssetId,$p_nTypeId,$p_bBypassMemcache);
        return $l_xCached;

    }

    public function getMostRecentByTypeIdForChannelId($p_nTypeId,$p_nChannelId,$p_bBypassMemcache = false){

        $l_xCached = $this->getMostRecentForChannelIdFromMemcacheAndStoreIfFail($p_nTypeId,$p_nChannelId,$p_bBypassMemcache);
        return $l_xCached;

    }

    /**
     * Get most recent O-Flo content.
     *
     * @param $p_nAssetId
     * @param $p_nChannelId
     * @param bool $p_bBypassMemcache
     * @return mixed
     */
    public function getMostRecentOflo($p_nAssetId, $p_nChannelId, $p_bBypassMemcache = false){
        $l_xCached = $this->getMostRecentOfloFromMemcacheAndStoreIfFail($p_nAssetId, $p_nChannelId, $p_bBypassMemcache);

        return $l_xCached;
    }

    /**
     * Get O-Flo content by ID.
     *
     * @param $p_nAssetId
     * @param $p_nTypeId
     * @param bool $p_bBypassMemcache
     * @return mixed
     */
    public function getOfloContentById($p_nAssetId, $p_nTypeId, $l_nChannelId, $p_bBypassMemcache = false){
        $l_xCached = $this->getOfloContentByIdFromMemcacheAndStoreIfFail($p_nAssetId,$p_nTypeId, $l_nChannelId,$p_bBypassMemcache = false);

        return $l_xCached;
    }
    public function breakCacheForId($p_nAssetId){
        $l_xCached = $this->getFromMemcacheAndStoreIfFail($p_nAssetId,CONTENT_TYPE_ID_PHOTO,$p_bBypassMemcache);
        $l_xCached = $this->getFromMemcacheAndStoreIfFail($p_nAssetId,CONTENT_TYPE_ID_VIDEO,$p_bBypassMemcache);
    }





    // MEMCACHING


    public function getFromMemcacheAndStoreIfFail($p_nAssetId,$p_nTypeId,$p_bBypassMemcache = false){

        $l_sMemcacheKey = Memcaching::makeKey(ItemManager::returnItemKey($p_nAssetId,$p_nTypeId));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if($p_bBypassMemcache || !$l_aCachedContent){
            $l_sSql = 'SELECT * FROM '.(($p_nTypeId == CONTENT_TYPE_ID_VIDEO) ? 'enr_video' : 'enr_photo' ).'
                       WHERE id = '.intval($p_nAssetId);
            $l_aData = $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);
            return $l_aData;
        }else{
            return $l_aCachedContent['value'];
        }
    }


    public function getMostRecentForChannelIdFromMemcacheAndStoreIfFail($p_nTypeId,$p_nChannelId,$p_bBypassMemcache = false){

        $l_sMemcacheKey = Memcaching::makeKey('MOST-RECENT-'.$p_nChannelId.'-'.$p_nTypeId);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = 'SELECT * FROM '.(($p_nTypeId == CONTENT_TYPE_ID_VIDEO) ? 'enr_video' : 'enr_photo' ).'
                       WHERE cid = '.intval($p_nChannelId).'
                       AND is_publish = 1
                       ORDER BY id DESC LIMIT 1';
            $l_aData = $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);
            return $l_aData;
        }else{
            return $l_aCachedContent['value'];
        }
    }

    /**
     * Get Most Recent O-Flo content from Memcache and store if fail.
     *
     * @param $p_nTypeId
     * @param $p_nChannelId
     * @param bool $p_bBypassMemcache
     * @return mixed
     */
    public function getMostRecentOfloFromMemcacheAndStoreIfFail($p_nTypeId,$p_nChannelId,$p_bBypassMemcache = false){

        // Los Angeles timezone
        date_default_timezone_set('UTC');
        $utc_time = date("Y-m-d h:i:sa");

        date_default_timezone_set('America/Los_Angeles');
        $pacific_time = date("Y-m-d h:i:sa");

        $hours_diff = $utc_time - $pacific_time;

        $l_sMemcacheKey = Memcaching::makeKey('MOST-RECENT-OFLO-'.$p_nChannelId.'-'.$p_nTypeId);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = "SELECT ec.url, ct.*, UNIX_TIMESTAMP(ct.start_date) as start_date_timestamp
                       FROM ".(($p_nTypeId == CONTENT_TYPE_ID_VIDEO) ? "enr_video" : "enr_photo" )." ct
                       LEFT JOIN enr_chanel ec ON ec.id = ct.cid
                       WHERE ct.is_oflo = 'y'
                       AND ct.is_publish = 1
                       AND ct.start_date <= DATE_SUB(NOW(), INTERVAL $hours_diff HOUR)
                       ".(!empty($p_nChannelId) ? " AND ct.cid = $p_nChannelId ": '')."
                       ORDER BY ct.id DESC LIMIT 1";

            $l_aData = $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,MEMCACHE_DURATION_OFLO);

            return $l_aData;

        }else{
            return $l_aCachedContent['value'];
        }
    }

    /**
     * Get O-Flo Content (Photo/Video) By ID from Memcache and store if fail.
     *
     * @param $p_nAssetId
     * @param $p_nTypeId
     * @param bool $p_bBypassMemcache
     * @return mixed
     */
    public function getOfloContentByIdFromMemcacheAndStoreIfFail($p_nAssetId,$p_nTypeId, $l_nChannelId,$p_bBypassMemcache = false){

        // Los Angeles timezone
        date_default_timezone_set('UTC');
        $utc_time = date("Y-m-d h:i:sa");

        date_default_timezone_set('America/Los_Angeles');
        $pacific_time = date("Y-m-d h:i:sa");

        $hours_diff = $utc_time - $pacific_time;

        $l_sMemcacheKey = Memcaching::makeKey('oflo-item-object-'.$p_nAssetId.'-'.$p_nTypeId);
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if($p_bBypassMemcache || !$l_aCachedContent){

            $l_sSql = "
                SELECT *, SUM(views) as total_views FROM
                      (

                    SELECT ec.url, ct.*, UNIX_TIMESTAMP(ct.start_date) as start_date_timestamp,
                      CASE 
                                    WHEN orm.is_mobile = 1
                                       THEN orm.video_views
                                    
                               END as views

                       FROM ".(($p_nTypeId == CONTENT_TYPE_ID_VIDEO) ? "enr_video" : "enr_photo" )." ct 
                       LEFT JOIN oflo_reports_monthly orm ON ct.id = orm.content_id
                       LEFT JOIN enr_chanel ec ON ec.id = ct.cid
                       WHERE ct.id = ".intval($p_nAssetId)."
                       AND ct.is_oflo = 'y'
                       AND ct.is_publish = 1
                       AND ct.start_date <= DATE_SUB(NOW(), INTERVAL $hours_diff HOUR)
                       ".(!empty($l_nChannelId) ? " AND ct.cid = $l_nChannelId " : '')."
                       
                       UNION

                       SELECT ec.url, ct.*, UNIX_TIMESTAMP(ct.start_date) as start_date_timestamp, 
                    CASE 
                                    WHEN orm.is_mobile = 0
                                       THEN orm.page_views_ga
                                    
                               END as views
                       FROM ".(($p_nTypeId == CONTENT_TYPE_ID_VIDEO) ? "enr_video" : "enr_photo" )." ct 
                       LEFT JOIN oflo_reports_monthly orm ON ct.id = orm.content_id
                       LEFT JOIN enr_chanel ec ON ec.id = ct.cid
                       WHERE ct.id = ".intval($p_nAssetId)."
                       AND ct.is_oflo = 'y'
                       AND ct.is_publish = 1
                       AND ct.start_date <= DATE_SUB(NOW(), INTERVAL $hours_diff HOUR)
                       ".(!empty($l_nChannelId) ? " AND ct.cid = $l_nChannelId " : '')."

                       ) as t1 
                       GROUP BY id
                       LIMIT 1
                       ";

            $l_aData = $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,MEMCACHE_DURATION_OFLO);

            return $l_aData;

        }else{
            return $l_aCachedContent['value'];
        }
    }


    // STATIC

    public static function returnItemKey($p_nAssetId,$p_nTypeId){
        return 'item-object-'.$p_nAssetId.'-'.$p_nTypeId;
    }
}

?>