enriqueApp.controller('signupController', ['$scope', '$rootScope', '$http', '$modal', function($scope, $rootScope, $http, $modal) {
	$scope.error = '';
	
	$scope.onSignupButton_click = function() {
		var username = $('#text_username');
		var first_name = $('#text_first_name');
		var last_name = $('#text_last_name');
		var email = $('#text_email');
		var password = $('#text_password');
		var password1 = $('#text_password1');
		
		if(username.val() == '') {
			$scope.error = 'Please enter your username.';
			username.focus();
		}
		else if(first_name.val() == '') {
			$scope.error = 'Please enter your first name.';
			first_name.focus();
		}
		else if(last_name.val() == '') {
			$scope.error = 'Please enter your last name.';
			last_name.focus();
		}
		else if(email.val() == '') {
			$scope.error = 'Please enter your email.';
			email.focus();
		}
		else if(password.val() == '') {
			$scope.error = 'Please enter your password.';
			password.focus();
		}
		else if(password.val() != password1.val()) {
			$scope.error = 'Confirm password does not match.';
			password1.focus();
		}
		else {
			$http({method: 'GET', url: $rootScope.get_api_url() + '&option=register&username=' + username.val() + '&firstname=' + first_name.val() + '&lastname=' + last_name.val() + '&email=' + email.val() + '&password=' + password.val()}).
	        success(function(data, status, headers, config) {
	        	if(data.status == true) {
	        		$rootScope.user_token = data.token;
	        		$rootScope.current_user = data.info;
	        		
	        		$rootScope.dialogSignup.close();
	        		
	        		$rootScope.saveCookie();
	        	}
	        	else {
	        		$scope.error = data.message;
	        	}
	        }).
	        error(function(data, status, headers, config) {
	        	$scope.error = 'Please try again.';
	        });
		}
	};
	
	$scope.onCloseButton_click = function() {
		$rootScope.dialogSignup.close();
		$rootScope.onShowLoginButton_click();
	};
}]);