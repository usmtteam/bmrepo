<style type="text/css">
    .icon-plus, .icon-tab, .icon-list, .icon-wrench { 
  background: none !important;
    }
</style>

<?php
	$curPage = $this->router->fetch_class().'/'.$this->router->fetch_method(); //.'/'.(empty($this->uri->segment(3))); //? '' : '/' . $this->uri->segment(3));
    $chanel = $this->session->userdata('chanel') ? $this->session->userdata('chanel') : 0;
    $user_id = $this->session->userdata('user_id');
    $selfuser = $this->ion_auth->user($user_id)->row();
    $submenus = $this->ion_auth->getContentMenuList();

    if ($curPage == ('content/feed_videoEmbed'))   $curPage = 'content/feed_video' ;
    if ($curPage == ('content/feed_photo'))     $curPage = 'content/feed_video' ;
    if ($curPage == ('content/feed/photo_edit'))  $curPage = 'content/postfeed';
    if ($curPage ==('master_curater/edit'))     $curPage = 'master_curater/feed';
    if ($curPage == ('push_notifications/notifications_add'))  $curPage = 'push_notifications/index';
    if ($curPage ==('push_notifications/notifications_edit'))     $curPage = 'push_notifications/index';
    if ($curPage == ('hub_master/hubmanager_add'))  $curPage = 'hub_master/index';
    if ($curPage ==('hub_master/hubmanager_edit'))     $curPage = 'hub_master/index';
    if ($curPage == ('user_manager/user_manager_add'))  $curPage = 'user_manager/index';
    if ($curPage ==('user_manager/user_manager_edit'))     $curPage = 'user_manager/index';
    if ($curPage == ('users_group_manager/users_group_manager_add'))  $curPage = 'users_group_manager/index';
    if ($curPage ==('users_group_manager/users_group_manager_edit'))     $curPage = 'users_group_manager/index';



   
    if($submenus){
        $content_menus = array();
        foreach($submenus as $submenu){
            $content_menus[] = array($submenu['url'], $submenu['alter_name']);
        }
    }
    if(!$chanel){
        $side_menus = array(
            0    =>    array( 'dashboard/index',     'Dashboard', 'icon-home'    ),
            11    =>    array( 'auth/logout',     'Logout', 'icon-user'    ),
        ); 
    }/*elseif($selfuser->superadmin){
//        $this->session->unset_userdata('chanel');
        $side_menus = array(
            0    =>    array( 'dashboard/index',     'Dashboard', 'icon-home'    ),
//            1    =>    array( 'configuration/index',     'Manage configuration', 'icon-th-list' ),
            4    =>    array( '#', 'Content', 'icon-th-list', 
                            $content_menus
//                        array(
//                            0    =>    array('content/feed',    'Home Feed'),
//                            1    =>    array('content/community',    'Community'),
//                            2    =>    array('content/photo_gallery',    'Photo Gallery'),
//                            3    =>    array('content/video_gallery',    'Video Gallery'),
//                            4    =>    array('content/tour_date',    'Tour Dates'),
//                            5    =>    array('content/music_player',    'Music Player'),
//                            6    =>    array('content/product',    'Shop'),
//                        ) 
                    ),
            5    =>    array( '#',     'Fans', 'icon-th-list'    , 
                        array(
                            0    =>    array('fan/active',    'Active Fans'),
                            1    =>    array('fan/block',    'Blocked Fans')
                        ) 
                    ),
            6    =>    array( 'design/index',     'Design', 'icon-th-list'    ),
 //           7    =>    array( 'currency/index',     'Currency', 'icon-th-list'    ),
//            8    =>    array( 'stats/index',     'Stats', 'icon-th-list'    ),
            9    =>    array( 'social/index',     'Settings', 'icon-th-list'    ),
            10    =>    array( '#',     'Manage Helps', 'icon-th-list',
						array(
							0 => array('helps/managehelps', 'Help' ),
							1 => array('helps/manageterms', 'Terms' ),
							2 => array('helps/managepravacy', 'Privacy' )
						)),
            11    =>    array( '#', 'Attribute', 'icon-th-list', 
                        array(
                            0    =>    array('attribute/category',    'Category'),
                            1    =>    array('attribute/color',    'Color'),
                            2    =>    array('attribute/size',    'Size')
                        ) 
                    ),
            12    =>    array( '#', 'Admins', 'icon-th-list', 
                        array(
                            0    =>    array('auth/index',            'Admin List'),
//                            1    =>    array('auth/create_user',    'Create Admin'),
                        ) 
                    ),
            13    =>    array( 'auth/logout',     'Logout', 'icon-user'    ),
        ); 
    }*/elseif($selfuser->admin_type=='0'){
        $side_menus = array(
            0    =>    array( 'content/feed',     'Dashboard', 'icon-stats-dots'    ),
            4    =>    array( 'content/feed_video', 'Upload', 'icon-plus'),
            5    =>    array( 'content/curate',     'Curate', 'icon-tab'),
            6    =>    array( 'content/postfeed',     'Posts', 'icon-list'    ),
            9    =>    array( 'design/index',     'Settings', 'icon-wrench'    ),
            // 28   =>    array( 'payments_master/index',     'Payments', 'icon-list'    ),
        );
        if($user_id==1 || $user_id==170 || $user_id==192){
            $side_menus = array(
                0    =>    array( 'content/feed',     'Dashboard', 'icon-stats-dots'    ),
                4    =>    array( 'content/feed_video', 'Upload', 'icon-plus'),
                5    =>    array( 'content/curate',     'Curate', 'icon-tab'),
                6    =>    array( 'content/postfeed',     'Posts', 'icon-list'    ),
                9    =>    array( 'design/index',     'Settings', 'icon-wrench'    ),
                20    =>    array( 'master_curater/feed',     'Master Curater', 'icon-list'    ),
                21    =>    array( 'category_list/index',     'Category', 'icon-list'    ),
                22    =>    array( 'content/ofloslider',     'Breaking Movement Slider', 'icon-list'    ),
                23    =>    array( 'push_notifications/index',     'Notifications', 'icon-list'    ),
                24    =>    array( 'hub_master/index',     'Hub Manager', 'icon-list'    ),
                25    =>    array( 'user_manager/index',     'User Manager', 'icon-list'    ),
                26   =>    array( 'users_group_manager/index',     'Group Manager', 'icon-list'    ),
                27   =>    array( 'reports_master/index',     'Report Manager', 'icon-list'    ),
                    
                
            ); 

        }

    }else{
        $side_menus = array(
            0    =>    array( 'dashboard/index',     'Dashboard', 'icon-home'    ),
            4    =>    array( '#', 'Content', 'icon-th-list', 
                        array(
                            0    =>    array('content/feed',    'Home Feed'),
                            1    =>    array('content/community',    'Community'),
                            2    =>    array('content/photo_gallery',    'Photo Gallery'),
                            3    =>    array('content/video_gallery',    'Video Gallery'),
                            4    =>    array('helps/managetourdate',    'Tour Dates'),
                            5    =>    array('content/music_player',    'Music Player'),
                            6    =>    array('helps/manageshop',    'Shop'),
                        ) 
                    ),
            5    =>    array( '#',     'Fans', 'icon-th-list'    , 
                        array(
                            0    =>    array('fan/active',    'Active Fans'),
                            1    =>    array('fan/block',    'Blocked Fans')
                        ) 
                    ),
            6    =>    array( 'design/index',     'Design', 'icon-th-list'    ),
 //           7    =>    array( 'currency/index',     'Currency', 'icon-th-list'    ),
 //           8    =>    array( 'stats/index',     'Stats', 'icon-th-list'    ),
            9    =>    array( 'social/index',     'Settings', 'icon-th-list'    ),
            10    =>    array( '#',     'Manage Helps', 'icon-th-list',
						array(
							0 => array('helps/managehelps', 'Help' ),
							1 => array('helps/manageterms', 'Terms' ),
							2 => array('helps/managepravacy', 'Privacy' )
						)
					),
            12    =>    array( 'auth/logout',     'Logout', 'icon-user'    ),
        ); 
    }

?>
	<!-- if uri (very top uri sectment 3)= video
    and link  -->
    	<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
	
    <?php
	
//	print_r($side_menus);exit;
	 foreach($side_menus as $item) {
	 	
		$cls1 = "";
		$cls2 = "";
		$sel1 = "";
		$lnk = "";
		
	 	
        if($item[0] == $curPage) {
	 		$cls1 = 'active';
            $sel1 = '<span class="selected"></span>';
	 		$lnk = ($item[0]);
         
            
        }

        else{
	 		$cls1 = '';
	 		$sel1 = '';
	 		if($item[0]=="#") {
	 			$lnk = "";
	 			foreach ($item[3] as $subitem) {

	 				if($subitem[0] == $curPage)
                        echo ($subitem[0] == $curPage); exit;
                     {
	 					$cls1 = 'active';
	 					$cls2 = 'open';
	 					$sel1 = '<span class="selected"></span>';
                    
	 					break;
	 				}
	 			}

                
	 		}else{
	 			$lnk = ($item[0]);

	 		}
	 	}
	 	if(count($item)<4)
       {
            if($item[1]=='Dashboard'||$item[1]=='Upload'||$item[1]=='Curate'||$item[1]=='Posts'||$item[1]=='Settings'||$item[1]=='Payments'){

				echo '<li class="'.$cls1.'">'.
							'<a'.(empty($lnk) ? '' : ' href="'.base_url() . $lnk.'"').' style="cursor:pointer;">'.
							'<i class="'.$item[2].'"></i> '.
							'<span class="title">'.$item[1].'</span>'.
							$sel1.
							'</a>'.
						'</li>';
            }

          
	 
        }else{
	 		$cls1 .= " has-sub";
	 		echo '<li class="'.$cls1.'">'.
						'<a'.(empty($lnk) ? '' : ' href="'.base_url() . $lnk.'"').' style="cursor:pointer;">'.
						'<i class="'.$item[2].'"></i>'.
						'<span class="title">'.$item[1].'</span>'.
	 					$sel1.
						'<span class="arrow '.$cls2.'"></span>'.
						'</a>'.
						'<ul class="sub">';
				 		foreach ($item[3] as $subitem)

                        { 

				 			$cls1 =	$subitem[0]==$curPage? 'class="active"' : '';
                            


							echo '<li '.$cls1.'><a href="'.(base_url() . $subitem[0]).'">'.$subitem[1].'</a></li>';


				 		}
						echo '</ul>'.
					'</li>';
	 	}
	 }
?> 

			</ul>

    <?php if($user_id==1 || $user_id==170 || $user_id==192){ ?>         
			<!-- END SIDEBAR MENU -->
    <i class="icon-list stand_alone_icon"></i>
    <select id='sidebar_menu' onChange="window.location.href=this.value" style="-webkit-appearance: none; -moz-appearance: none; text-indent: 1px; text-overflow: '';">
        
        <option>More...</option>
            <?php
    
//  print_r($side_menus);exit;
     foreach($side_menus as $item) {
        
        $cls1 = "";
        $cls2 = "";
        $sel1 = "";
        $lnk = "";
        
        
        if($item[0] == $curPage) {
            $cls1 = 'active';
            $sel1 = '<span class="selected"></span>';
            $lnk = ($item[0]);
         
            
        }

        else{
            $cls1 = '';
            $sel1 = '';
            if($item[0]=="#") {
                $lnk = "";
                foreach ($item[3] as $subitem) {

                    if($subitem[0] == $curPage)
                        echo ($subitem[0] == $curPage); exit;
                     {
                        $cls1 = 'active';
                        $cls2 = 'open';
                        $sel1 = '<span class="selected"></span>';
                    
                        break;
                    }
                }

                
            }else{
                $lnk = ($item[0]);

            }
        }
        if(count($item)<4)
       {
            if ($item[1]=='Master Curater'||$item[1]=='Category'||$item[1]=='Breaking Movement Slider'||$item[1]=='Notifications'||$item[1]=='Hub Manager'||$item[1]=='User Manager' || $item[1]=='Group Manager'||$item[1]=='Report Manager') {
                    
                    echo '<option value="'.base_url() . $lnk.'" class="'.$cls1.'">'.
                            '<a'.(empty($lnk) ? '' : ' href="'.base_url() . $lnk.'"').' style="cursor:pointer;">'.
                            '<i class="'.$item[2].'"></i> '.
                            '<span class="title">'.$item[1].'</span>'.
                            $sel1.
                            '</a>'.
                        '</option>';
            } {

            }

          
     
        }else{
            $cls1 .= " has-sub";
            echo '<option class="'.$cls1.'">'.
                        '<a'.(empty($lnk) ? '' : ' href="'.base_url() . $lnk.'"').' style="cursor:pointer;">'.
                        '<i class="'.$item[2].'"></i>'.
                        '<span class="title">'.$item[1].'</span>'.
                        $sel1.
                        '<span class="arrow '.$cls2.'"></span>'.
                        '</a>'.
                        '<ul class="sub">';
                        foreach ($item[3] as $subitem)

                        { 

                            $cls1 = $subitem[0]==$curPage? 'class="active"' : '';
                            


                            echo '<li '.$cls1.'><a href="'.(base_url() . $subitem[0]).'">'.$subitem[1].'</a></li>';


                        }
                        echo '</ul>'.
                    '</option>';
        }
     }
?> 

            </select>
            <?php } ?>

		</div>
		<!-- END SIDEBAR -->
