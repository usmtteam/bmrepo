<div id="fancybox_container" style="width:640px; height: 360px;display: none;">

    <?php
        $start_key =  $page * $page_count;
        $end_key = min(count($feeds), $start_key + $page_count) - 1;

        for($key = $page * $page_count; $key <= $end_key; $key++) :

            $feed = $feeds[$key];

            if($feed['destination']!=""){
                $url = $feed['destination'];
            }else{
                $novideo = true;
                $url = ASSETS_DIR.'/img/no-video-found.jpg';

            }

    ?>

    <div class="video center" id='video_container_<?php echo $feed['content_id']; ?>' style='height:100%; '>
            <video width="640" height="360" id="player<?php echo $feed['content_id']; ?>" preload="none" >
                <!-- Pseudo HTML5 -->
                <source id='htmlvideoplayer' src="<?php echo $feed['destination']; ?>"/>
                <!--<source  src="http://127.0.0.1/dan/1.flv" />-->
            </video>
    </div>

    <?php endfor; ?>
</div>

<input type="hidden" value="<?php echo $base_url; ?>" id='base_url'>

<!-- BEGIN PAGE -->
<div class="page-content">
     <div class="container-fluid" id="dashboard">
    
    
		
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Curate
                    
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">

                <form action="" method="get">

                    Category:
                    <select class="gray" name="category">
                        <option value="">All Categories</option>

                        <?php foreach($viral_media_categories as $category): ?>

                            <option value="<?=$category['cat_name']?>" <?php if(strtolower($selected_category) == strtolower($category['cat_name'])){echo "selected";}?> ><?=$category['cat_name']?></option>

                        <?php endforeach; ?>
                    </select>

                    <input type="submit" name="filter_btn" value="Filter" class="btn btn_mastercur">

                    <br><br>

                    <select class= "gray" name="items_per_page" style="width: 77px" onchange="this.form.submit();">
                        <?php foreach ($items_per_page_array as $item_per_page): ?>
                            <option value="<?=$item_per_page?>" <?php if ($page_count == $item_per_page){echo "selected";} ?> ><?=$item_per_page?></option>
                        <?php endforeach; ?>
                    </select>
                    Items per page

                    <div style="float: right">
                        <input type="text" name="search" value="<?=$search?>" class="">
                        <input type="submit" name="search_btn" value="Search" class="btn btn_mastercur">
                    </div>

                </form>

                <table class="table bootstrap-datatable">
                    <thead>
                        <tr>
                            <th>Thumbnail</th>
                            <th>Suggested Caption</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Actions</th>                            
                        </tr>
                    </thead>
                    <tbody>

                    <?php 

                    for($key = $page * $page_count; $key <= $end_key; $key++) :
                        
                        $feed = $feeds[$key];
                        $ids = explode("_", $feed['id']);
                        $post_controller_name = $ids[0];

                    ?>
                
                        <tr>
                            <td class="feed-thumbnail-td ">
                            <?php if($feed['image_path']!=""){ 
							
                                echo '<a title="'.substr($feed['description'],0,100).'" class="fancybox" id="fancybox_'.$ids[1].'" style="cursor: pointer;">
                                <input type="hidden" class="video_info" value="'.$ids[1].'">
                                <div class="gallery-video-container"> 
   <img class="feed-thumbnail-large pull-left" src="'.$feed['image_path'].'"></div></a>';
                            } else {  echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'"> <img class="feed-thumbnail-large pull-left" src="/assets/img/click-to-view.jpg"></a><br/>'; } ?>
                            </td>
                            <td class="span2"><div style='word-wrap:break-word; width: 200px'><?php if($feed['description']){echo $feed['description'];}else{ echo "Processing - Content will be available shortly"; } ?>
                            	
                            </div></td>
                            <td><?=$ids[0];?></td>
                            <?php $permission = ($feed['is_publish']==2 ? "Draft" : ($feed['is_publish']==1 ? "Trending" : "Transcoding")); ?>
                            <td><?php echo $permission ?></td>
                             <td><?php echo $feed['date']; ?></td>
                            
                            <td>
                            <?php
                                echo
                                '<a class="btn" href="'.site_url('content/feed_curate_copy/'.$feed['id']).'">'.
                                'Curate'.
                                '</a>';
                                //delete curation and only admin will see delete button
                                if($this->session->userdata('username')=="administrator"){
                                    echo 
                                    '<a class="btn" href="javascript:confirm_del(\''.$feed['id'].'\')">'.
                                    'Delete'.
                                    '</a>';
                                }
                            ?>
        
                            </td>
                        </tr>
                        <script>
                            function confirm_del(id) {
                                if(confirm("Do you want to delete this curation?")) {
                                    document.location.href = "<?php echo site_url('content/delete_curate'); ?>/" + id;
                                }
                            }    
                        </script>
                    <?php endfor; ?>

                    </tbody>
                </table>
                
                     <div class="dataTables_info">Showing <?php echo (($page_count*($page))+1); ?> to <?php if($total_count<($page_count*($page+1))){ echo $total_count; }else{ echo ($page_count*($page+1)); } ?> of <?php echo $total_count; ?> entries</div>
                </div><!-- end of row-fluid -->
                <center>

                    <div class="dataTables_paginate paging_bootstrap pagination">

                        <ul>
                            <li class="prev<?php echo $page == 0 ? ' disabled' : ''?>">
                                <a data-page="<?php echo max(0, $page - 1);?>" style="<?php echo $page == 0 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">← Previous</a>
                            </li>
                            <!-- First page -->
                            <?php if ($page > 2) : ?>
                                <li class="">
                                    <a data-page="<?php echo 0;?>" style="cursor: pointer;"><?php echo 1; ?></a>
                                </li>
                            <?php endif; ?>

                            <!-- Ellipsis -->
                            <?php if ($page >= 4) : ?>
                                <li class="">
                                    <a data-page="" src="" style="pointer-events: none; cursor: none;">...</a>
                                </li>
                            <?php endif; ?>

                            <!-- Surrounding pages -->
                            <?php for($i = $start_page; $i < $end_page; $i++) :?>
                                <li class="<?php echo $page == $i ? ' active' : ''; ?>">
                                    <a data-page="<?php echo $i;?>" style="cursor: pointer;"><?php echo $i+1; ?></a>
                                </li>
                            <?php endfor; ?>

                            <!-- Ellipsis -->
                            <?php if ($end_page <= $total_page_count - 2) : ?>
                                <li class="">
                                    <a data-page="" style="pointer-events: none; cursor: pointer;">...</a>
                                </li>
                            <?php endif; ?>

                            <!-- Last page -->
                            <?php if ($end_page <= $total_page_count - 1) : ?>
                                <li class="">
                                    <a data-page="<?php echo $total_page_count  - 1;?>" style="cursor: pointer;"><?php echo $total_page_count; ?></a>
                                </li>
                            <?php endif; ?>

                            <li class="next<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>">
                                <a data-page="<?php echo min($total_page_count-1, $page + 1); ?>" style="<?php echo $page == $total_page_count-1 ? 'pointer-events: none; ' : '' ?>cursor: pointer;">Next →</a>
                            </li>
                        </ul>
                    </div>
                </center>
                <script language="javascript">
                $(document).ready(function() {
                    $('.pagination a').click(function(e) {
                        var base_url = jQuery('#base_url').val();
                        document.location.href = base_url + "&page=" + $(e.target).attr('data-page');
                    });
                });
                </script>
            </div>
        </div>
        
    </div>
    <!-- END GALLERY MANAGER PORTLET-->
</div>
<link href="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<script src="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.pack.js" ></script>
<script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/htmlplayer/mediaelement-and-player.min.js"></script>
<link href="<?php echo ASSETS_DIR; ?>/htmlplayer/mediaelementplayer.min.css" rel="stylesheet" />

<script>
    $('video').mediaelementplayer({
        success: function(media, node, player) {
            $('#' + node.id + '-mode').html('mode: ' + media.pluginType);
        }
    });


</script>
