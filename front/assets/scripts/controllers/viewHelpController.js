enriqueApp.controller('viewHelpController', function($scope, $rootScope, $http, $sce, url) {
	$scope.contents = '';
    
    $scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	$http({method: 'GET', url: $rootScope.SERVER_URL + '/viewhelp/' + url + '/' + $rootScope.CHANNEL}).
        success(function(data, status, headers, config) {
        	$('#help_contents').html(data);
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    }

    $scope.loadData();
});