<?php
class Category_List extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('cat_name', 'Name', 'required');
		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('main_m');
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

      if($user->email!="admin@admin.com"){
//            $id =  $this->set_slug_dependent_constants($user->email);
            $id =  $this->set_slug_dependent_constants($this->chanel['id']);
        }else{ // this set does not work with admin login
//            $id = $this->uri->segment(3, 0);
//            echo "<BR>\$id :".$id;
//            $SQL = 'SELECT email, id FROM users WHERE cid = "'.$id.'"';
//            $query = $this->db->query($SQL);
//            $rows = $query->result();
//            $email = $rows[0]->email;
//            $useid = $rows[0]->id;
//
//            $id =  $this->set_slug_dependent_constants($email);
//            $user_id = $useid;
//            $user = $this->ion_auth_model->user($user_id)->row();

            $id =  $this->set_slug_dependent_constants($this->chanel['id']);
      }
   $this->data['is_oflo'] =  $this->main_m->isChanelOflo();



	}
  function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }

  


function index(){
            
    		$this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
            $this->load->view('category_list', $this->data);
    
    }


 public function set_slug_dependent_constants($cid){
      // $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      // $query = $this->db->query($SQL);
      // $rows = $query->result(); 
      $slug = "admin";

      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
      define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('UPLOAD_VIRAL_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/viral/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return;

      }
    
    
public function add_category($post){
        
         

        $this->form_validation->set_rules('cat_name', 'name', 'required|max_length[30]|xss_clean');
        if ($this->form_validation->run()==false){}
            else
{
        if ($this->form_validation->run() == true) 
     
        { 

    
             if($this->input->post('cat_name',false) == true)

            {
                
            
                {

               

                    $this->data['post'] = array(
                    'cat_name' => $cat_name,
                    'hex_color' => $hex_color,
                    'image'  => $filepath,

                    );
                    $this->data['viral_media_category'] = $this->main_m->add_viral_media_category($cat_name, $hex_color, $filepath);


                     redirect(site_url('category_list/'));
                }
            }
           
            
                {
            
             
                    if(empty($cat_name)){
            show_error("Enter a category name!");
              return;
                
                    }
                }

            }
           
            
            
        }
         
        
           $this->load->view('category_add', $this->data);


    }
    

        

        public function edit_category($post){
        $id = $this->uri->segment(3);
        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();


        $this->data['category'] = $this->main_m->get_category_id($id);
        $this->data['id'] = $this->data['category']['id'];
        $this->data['cat_name'] = $this->data['category'][0]->cat_name;
    
        $this->data['cat_image'] = $this->data['category'][0]->cat_image;

        // var_dump($this->data['cat_image'] = $this->data['category'][0]->cat_image); exit;
        $this->data['hex_color'] = $this->data['category'][0]->hex_color;
        $this->data['oflo'] = $this->data['category'][0]->is_oflo;
        $this->data['oflo_menu'] = $this->data['category'][0]->oflo_menu;
        $this->update_viral_media_category_id($id);
        $this->load->view('category_edit', $this->data);
        

       }

       public function update_viral_media_category_id($id) {
        $this->form_validation->set_rules('cat_name', 'name', 'required|max_length[30]|xss_clean');
        
        if ($this->form_validation->run() == true)
        {

            if($this->input->post('oflo')=='on'){
                $oflo = 1;
            }else{
                $oflo = 0;
            }

            if($this->input->post('oflo_menu')=='on'){
                $oflo_menu = 1;
            }else{
                $oflo_menu = 0;
            }

           
         
            if($this->main_m->update_viral_media_category($id, $this->input->post('cat_name'), $this->input->post('hex_color'), $this->input->post('cat_image'), $oflo, $oflo_menu))
            {
                redirect("category_list/", '');
            }
        }
        return $data;
 
}
  


 public function confirm_del(){
     
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("viral_media_category");
        $this->db->where('id', $id);
        $this->db->delete("oflo_category");

         $sql_time = array(
            
            'last_updated_timestamp' => time()
        );
        $this->db->update('category_update', $sql_time);

         redirect(site_url('category_list/'));
    


    }
 
}
// 	