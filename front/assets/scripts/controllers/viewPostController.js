enriqueApp.controller('viewPostController', function($scope, $rootScope, $http, $sce, post, star, auto_play) {
	$scope.feeds = [post];
	$scope.post = post;
	$scope.star = star;
	$scope.auto_play = (auto_play == null || typeof auto_play == undefined || post.unlock == false) ? false : auto_play;
	
	$scope.thumbnail_clickable = post.post_type == 'quiz' || post.post_type == 'poll' ? true : false;
	$scope.all_comments = false;
	$scope.commentbox_position = 1;
	
	$scope.video_play = false;
    $scope.show_comment_box = true;
    $scope.view_m_comments = true;
    
	$scope.load = function() {
		if(post.post_type == 'video') {
			$scope.video_play = true;
			$scope.view_m_comments = true;
			if(typeof $scope.post.video_url.$$unwrapTrustedValue != 'undefined') {
				$scope.post.video_url = $scope.post.video_url.$$unwrapTrustedValue();
			}
			$scope.post.isCDN = $rootScope.isCDNData($scope.post.video_url);
			if($scope.post.isCDN) {
				$scope.post.video_url = $sce.trustAsResourceUrl($scope.post.video_url);
			}
			else {
				var url = $rootScope.SERVER_URL + '/get_ajax.php?url=' + 'http://www.youtube.com/oembed?url='+$scope.post.video_url + '&format=json';
				
		    	$http({method: 'GET', url: url}).
		        success(function(data, status, headers, config) {
					if($scope.auto_play) {
						var pos1 = data.html.indexOf(' src="');
						var pos2 = data.html.indexOf('"', pos1 + 7);
						data.html = data.html.slice(0, pos2) + '&autoplay=1' + data.html.slice(pos2); 
					}
					$scope.post.embed_html = $sce.trustAsHtml(data.html);
		        }).
		        error(function(data, status, headers, config) {
		        });
	
			}
		};
		
		//load comments if not loaded
		CommonLib.show_loading_bubble();
		var url = $rootScope.get_api_url() + '&option=get_comment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&post_type=' + post.post_type + '&content_id=' + post.content_id;
		$http({method: 'GET', url: url}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		$scope.post.comments = data.comments;
	    	}        	
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
	
	$scope.load();

	$scope.Limit = 5;
   
    $scope.getMore = function (e, feeds) {

       
        $scope.Limit = $scope.Limit + 5;
      
       
        
        // Call service to get more data
        var target = $(e.target);
        var content_id = target.attr('data-content-id');
        var comment_count = target.attr('data-comment-count');
        
        var show_all = false;
        var i;
        for(i = 0; i < feeds.length; i++) {
            if(feeds[i].content_id == content_id) {
                CommonLib.show_loading_bubble();
                var url = $rootScope.get_api_url() + '&option=get_comment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&post_type=' + feeds[i].post_type + '&content_id=' + feeds[i].content_id;
                $http({method: 'GET', url: url}).
                success(function(data, status, headers, config) {
                    CommonLib.hide_loading_bubble();
                    if(data.status == true) {
                        feeds[i].comments = data.comments;
                        
                        
                        show_all = feeds[i].show_all_comment;
                        if(show_all == null || typeof show_all == 'undefined') show_all = true;
                        
                        
                        feeds[i].show_all_comment = show_all;
                        

                        if(show_all) {
                            target.html('View more comments');
                        }
                        else {
                            target.html('View more comments');
                        }
                        var comm_diff = comment_count - $scope.Limit;

                        if(comm_diff<5)
                        {
                            target.html('Hide comments');
                        }

                        if($scope.Limit>comment_count){
                            show_all = !show_all;
                            feeds[i].show_all_comment = show_all;
                            target.html('View more comments');
                        }
                    }           
                }).
                error(function(data, status, headers, config) {
                    CommonLib.hide_loading_bubble();
                });
              
                break;
            }
        }
    };

	$scope.onCloseButton_click = function() {
		$rootScope.dialogViewPost.close();
		
	};
});