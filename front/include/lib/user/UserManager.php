<?php


	class UserManager extends FrontCoreManager{

		public function init(){
			$this->_xSql = AppController::getInstance()->getConnection();
		}

        public function getById($p_nId){
            $l_sSql = 'SELECT * FROM users WHERE id = '.intval($p_nId);
            return $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
        }

        public function getByToken($p_sToken){
            $l_sSql = 'SELECT * FROM users WHERE token = "'.$this->_xSql->db_escape_string($p_sToken).'"';
            return $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
        }

        public function updateProfile($p_nId,$p_aKeyValues){
            $l_sUpdates = array();
            foreach($p_aKeyValues as $k=>$v){
                $l_sUpdates[] = $k.' = "'.$this->_xSql->db_escape_string($v).'"';
            }
            $l_sSql = 'UPDATE users SET '.join(", ",$l_sUpdates).'
                        WHERE id = '.intval($p_nId);
            return $this->_xSql->runQuery($l_sSql);
        } 
        

        public function updateProfileOflo($p_nId,$p_aKeyValues){
            
            $l_sSql = "INSERT INTO push_not (device_id,token) VALUES ('$p_nId','$p_aKeyValues') ON DUPLICATE KEY
                            UPDATE token='$p_aKeyValues'";
            return $this->_xSql->runQuery($l_sSql);

        } 
	 }
?>