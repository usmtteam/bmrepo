<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_Curater extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('json_loader');
        $this->load->helper('bitly');
//        $this->load->library('BrightCove');
        $this->load->library('Utils');
//        include(realpath(dirname(__FILE__) . '/../../../front/include/lib/google-api-php-client/src/Google/autoload.php'));
//        $this->load->library('Gastar');
        require_once(__DIR__.'/../../../front/include/lib/front/FrontCoreManager.php');
        require_once(__DIR__.'/../../../front/include/lib/front/FrontCoreSql.php');
        $this->load->library('upload');

        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }

        $this->load->helper('language');
        $this->load->helper('login');
        $this->load->model('main_m');

        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'),
            $this->config->item('error_end_delimiter')
        );

        $this->chanel = $this->session->userdata('chanel');
      $this->data['is_oflo'] =  $this->main_m->isChanelOflo();


//        $this->data['chanel'] = $this->chanel;
//        $this->data['chanel_info'] = is_array($this->chanel) ? $this->main_m->get_chanel($this->chanel['id']) : $this->main_m->get_chanel($this->chanel);
//        $this->data['feeds'] = $this->main_m->getHomeFeedList();
        $this->data['show_errors'] = array();
        $this->data['selfuser'] = $this->ion_auth->user($user_id)->row();

        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);

        // Set Master Curater to Viral Channel

        if ($user->email == "admin@admin.com" || $user->email == "aahmek@me.com" || $user->email == "aahmek@gmail.com" ||
            $user->email == "andrew@starclubltd.com" || $user->email == "viral@starclubltd.com" || $user->email == "max@starclubltd.com" ||
            $user->email == "desarae@starclubltd.com" || $user->email == "beta@starsite.com" ||
            $user->email == "curator@starsite.com"|| $user->email == "awesome@starsite.com") {
//            $id = $this->set_slug_dependent_constants($user->email);;
//        } else {
//            $id = $this->uri->segment(3, 0);
//            $SQL = 'SELECT email, id FROM users WHERE cid = "' . $id . '"';
            $SQL = 'SELECT email, id FROM users WHERE cid = "64"';
            $query = $this->db->query($SQL);
            $rows = $query->result();
            $email = $rows[0]->email;
            $useid = $rows[0]->id;

            $id = $this->main_m->set_slug_dependent_constants($email);
            $user_id = $useid;
            $user = $this->ion_auth_model->user($user_id)->row();
        } else {
            redirect('content/feed');
        }
    }

    function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
    }

    function index(){
        redirect('master_curater/feed?partner=all');
    }

    /**
     * Load enr_video id's data to edit tags and captions.
     */
    function edit(){

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];
        $viral_media_cat_post = $this->data['viral_media_cat_post'];
        // var_dump($this->data['viral_media_category'][0]['id']);
        // exit;
        
//        print_r($_FILES);exit;
        //video id
        $feed_id = $this->uri->segment(3, false);
        $this->data['post'] = $this->main_m->getFeedVideo($feed_id);
       
        // var_dump($this->data['origin_id']);exit;
        $this->data['post_id'] = $feed_id;
       


        $i=0; 
          while($viral_media_category[$i]){
            
            $cat_id = $viral_media_category[$i]['id'];

            $cat_update = $this->input->post('submit');

            if($cat_update){
                if($this->input->post($cat_id, $feed_id)){
                  $this->main_m->cat_post_add($cat_id, $feed_id, CONTENT_TYPE_ID_VIDEO);
                }elseif (!$this->input->post($cat_id, $feed_id)){
                  $this->main_m->cat_post_delete($cat_id, $feed_id);
                }
                
            }
            $i++;
          
          }



        if($this->input->post('submit', false) !== false){
            $this->data['posted'] = array(
                'feed_id' => $feed_id,
                'description' => $this->input->post('description'),
                'tags' => $this->input->post('tags')            );
        }

        if($this->input->post('refresh', false) == true){

            if ($this->form_validation->run() == true) {
                  redirect("master_curater/edit/".$feed_id, '');
            }
        } else {

            $this->_proc_feed_video_edit();

        }

        $this->load->view('master_video_edit', $this->data);
    }

    /**
     * Process video edit.
     */
    public function _proc_feed_video_edit(){
        $var = $this->form_validation->set_rules('description', 'description', 'required|max_length[65535]|xss_clean');

        $this->form_validation->set_rules('tags','tags', 'max_length[32]|xss_clean');
        
        if ($this->form_validation->run() == true)
        {
            if ($this->main_m->updateEnrVideoViralVideoXML($this->data['posted'])){
                
                $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.'viral/assets/ufile/'.$this->data['post']['destination'].' '.$this->data['posted']['feed_id'];

                exec($l_sCommand . " > /dev/null &");

                if (isset($_FILES['image']) && $_FILES['image']['size']){

                    $filename = md5(time().rand(0.1, 99.9));
                    $ext = $this->main_m->image_content_type($_FILES['image']['type']);
                    $filename = $filename.$ext;

                    $year = date('Y');
                    $month = date('m');

                    $photo_filepath = $this->main_m->build_file_path("photo", $year, $month);

                    if (move_uploaded_file($_FILES['image']['tmp_name'], UPLOAD_DIR.$photo_filepath.$filename)){
//                        echo "Move success<br>";

                        if ($new_path = $this->main_m->updateEnrVideoS3Thumbnail($this->data['posted']['feed_id'], $photo_filepath.$filename,
                            "viral/assets/ufile/".$photo_filepath.$filename)){
                        }
                    } else {
//                        echo "Move fail<br>";
                        $data['show_errors']['image'] = "Error uploading photo.";
                    }
                }

                redirect("content/curate", '');
            }
        }
    }

    /**
     * Partner function to obtain Viral Media XML
     */

    public function feed()
    {
        // Get viral partners
        $this->data['viral_partners'] = $this->main_m->get_viral_partners();

        // GET queries
        $viral_partner_name = $this->input->get('partner', TRUE);
        $selected_category = $this->input->get('category', TRUE);
        $search = $this->input->get('search', TRUE);
        $page = $this->input->get('page', TRUE);
        $items_per_page = $this->input->get('items_per_page', TRUE);

        // Set default values
        if (empty($viral_partner_name)) {$viral_partner_name = "all"; }

        if (empty($page)) { $page = 0; }

        if (empty($items_per_page)) { $items_per_page = 20; }


        // Get results
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml($viral_partner_name, $selected_category, $search);
        $this->data['feeds'] = $this->data['viral_media_xml'];
        // Remember GET queries
        $testing = $this->data['feeds'];
        // var_dump($testing); exit;

        // Partner name
        $this->data['partner_name'] = $viral_partner_name;
        // Selected category
        $this->data['selected_category'] = $selected_category;
        // Search
        $this->data['search'] = $search;

        // Total pagination count
        $total_count = count($this->data['viral_media_xml']);
        $this->data['total_count'] = $total_count;

        // Pagingation
        $this->data['items_per_page_array'] = array(10, 25, 50, 100);
        if (!in_array($items_per_page, $this->data['items_per_page_array'])){ $items_per_page = 10; }
        $this->data['page'] = $page;
        $this->data['page_count'] = $items_per_page;
        $this->data['total_page_count'] = ceil($total_count / $this->data['page_count']);

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        
        // Clean up base_url
        $this->data['base_url'] = preg_replace('/&page=\d+/u', '', $_SERVER['REQUEST_URI']);

        // Add GET
        if (strpos($this->data['base_url'], "?") === FALSE){
            $this->data['base_url'] .= "?";
        }

        // Load view
        $this->load->view('master_curater', $this->data);
    }

    /**
     * Curate Master Viral Media XML to Viral Channel
     */
    public function curate(){
//
        $var = $this->form_validation->set_rules('description', 'description', 'required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','tags', 'max_length[32]|xss_clean');

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
//        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];
//        $viral_media_cat_post = $this->data['viral_media_cat_post'];
//
        $id = $this->uri->segment(3);
        $this->data['post_id'] = $id;

        if ($id == ""){
            redirect('master_curater/partner');
        }

        $feed = $this->main_m->get_viral_media_xml_by_id($id);
        $this->data['post'] = $feed;

        if (!empty($feed)){

            if($this->input->post('submit', false) !== false){

                if ($this->form_validation->run() == true)
                {
                    $l_bCLOSE_BUFFER = 1;

                    if(($feed['id'] != "0" || $feed['id'] != "")&& $l_bCLOSE_BUFFER){
                        Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>
                            window.location.href='/master_curater/feed/';
                            </SCRIPT>");
                    }

                    $feed['description'] = $this->input->post('description');
                    $feed['tags'] = $this->input->post('tags');

                    $id = $this->main_m->addViralVideoXML($feed);

                    $i=0;
                    while($viral_media_category[$i]){

                        $cat_id = $viral_media_category[$i]['id'];

                        $cat_update = $this->input->post('submit');

                        if($cat_update){

                            if($this->input->post($cat_id, $id)){
                                $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                            }elseif (!$this->input->post($cat_id, $id)){
                                $this->main_m->cat_post_delete($cat_id, $id);
                            }

                        }
                        $i++;

                    }

                    redirect("content/curate", '');
                }
            }
        }

        if($this->input->post('refresh', false) == true) {

        }

//        redirect(site_url('master_curater/edit/'.$id));
//        $this->load->view('master_video_edit', $this->data);
        $this->load->view('master_curate', $this->data);

    }

    /**
     * Remove Viral Video XML from Master Curater feed.
     */
    public function remove(){
        $id = $this->uri->segment(3);

        if ($id == ""){
            redirect('master_curater/');
        }

        if ($feed = $this->main_m->get_viral_media_xml_by_id($id)){
            $res = $this->main_m->blacklist_viral_video_xml($feed);
        }

        redirect(site_url('master_curater/'));
    }
    
}