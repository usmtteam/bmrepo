      <!-- BEGIN PAGE -->  
      <div class="page-content">

         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <h3 class="page-title">
                     Edit fan
                     <small><?php echo $user->first_name." ".$user->last_name; ?></small>
                  </h3>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
               <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box">

                        <div id="form_head_back">Edit Fan</div>

                     <div class="portlet-body form pb-feed">
                        <!-- BEGIN FORM-->
                        <?php echo form_open_multipart(uri_string(),'class="form-horizontal"');?>
                        <?php echo $message;?>
                        <div class="row-fluid">
                            <div class="span7">

                                <div class="control-group">
                                    <label for="username" class="control-label">Name:</label>
                                    <div class="controls">
                                    <?php echo form_input($name,'','class="span6 m-wrap"');?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="email" class="control-label">Email:</label>
                                    <div class="controls">
                                    <?php echo form_input($email,'','class="span6 m-wrap"');?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="credit" class="control-label">Credit:</label>
                                    <div class="controls">
                                    <?php echo form_input($credit,'','class="span6 m-wrap"');?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <?php echo lang('edit_user_password_label', 'password');?>
                                    <div class="controls">
                                    <?php echo form_input($password,'','class="span6 m-wrap"');?>
                                    <span class="help-inline"> (if changing password) </span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
                                    <div class="controls">
                                    <?php echo form_input($password_confirm,'','class="span6 m-wrap"');?>
                                    <span class="help-inline"> (if changing password) </span>
                                    </div>
                                </div>

                            </div>
                            <div class="span5">
                                <div class="fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img id='video_img' style="max-height: 100%" src="<?php echo is_file(UPLOAD_DIR.$user['img_url']) ? UPLOAD_URL.$user['img_url'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'; ?>" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="picture" class="default" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                        </div>
                        <div class="form-actions">
                          <?php echo form_submit('submit', lang('edit_user_submit_btn'),' class="btn blue"');?>
                          &nbsp;&nbsp;&nbsp;
                          <input type="button" name="button_back" value="Back" class="btn blue" onclick="document.location='<?php echo $ref_url; ?>';">
                        </div>
                    <?php echo form_close();?>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  
 
                    <div id="form_head_back">Posts</div>

                 <div class="portlet-body form backwhite pb-feed padded-fan">
                 	<table class="table table-bordered table-hover bootstrap-datatable datatable">
	                    <thead>
	                        <tr>
	                            <th></th>
	                            <th class="feed-thumbnail-noborder"></th>
	                            <th >Caption</th>
	                            <th>Date</th>
	                            <th>Edit</th>
	                        </tr>
	                    </thead>
	                    <tbody>     
	                    	<?php foreach($posts as $feed): ?>
	                        <tr>
	                            <td class="feed-thumbnail-td ">
	                            <?php if($feed['image_path']){  ?>
	                                <img class='feed-thumbnail-large pull-left' src="<?php echo UPLOAD_URL . $feed['image_path']; ?>"><br/>
	                            <?php }          ?>
	                            
	                                <span id="posting-type" style="color: #797979; font-size:11px; font-style:italic;">(<?php echo $feed['post_type']; ?>)</span>
	                            </td>
	                            <td class="feed-thumbnail-noborder"><?php // echo $feed['post_type']; ?></td>
	                            <td class="span2"><div style='word-wrap:break-word; width: 200px'><?php echo $feed['caption']; ?></div></td>
	                            <td><?php echo $feed['date']; ?></td>
	                            <td class="center" style="width:80px;">
	                            <?php
	                                echo 
	                                '<a class="btn mini purple" href="javascript:confirm_feed_del(\''.$feed['id'].'\')">'.
	                                '<i class="icon-edit"></i>Delete'.
	                                '</a>&nbsp;';
	                            ?>
	                            </td>
	                        </tr>
	                   		<?php endforeach; ?>
	                    </tbody>
					</table>
                 </div>
                 

                    <div id="form_head_back">Comments</div>
              
                 <div class="portlet-body form backwhite pb-feed padded-fan" style="margin-bottom:10px;">
                 	<table class="table table-bordered table-hover bootstrap-datatable datatable">
		                    <thead>
		                        <tr>
		                            <th style="width:30px;">No</th>
		                            <th style="width:200px;">Username</th>
		                            <th style="width:200px;">Post</th>
		                            <th>Comments</th>
		                            <th style="width:120px;">Date</th>
		                            <th>Delete</th>
		                        </tr>
		                    </thead>
		                    <tbody> 
		                    <?php foreach($comments as $i => $comment) :?>
		                    	<tr>
		                    		<td><?php echo $i+1; ?></td>
		                    		<td><?php echo $comment['name']; ?></td>
		                    		<td><a href="<?php echo site_url('content/feed/'.$comment['post_type'].'_edit/'.$comment['content_id']); ?>"><?php echo $comment['post_type']; ?></a></td>
		                    		<td><?php echo $comment['comment']; ?></td>
		                    		<td><?php echo date('Y-n-j', $comment['time_stamp']); ?></td>
		                    		<td style="width:80px;">
		                            <?php
		                                echo 
		                                '<a class="btn mini black" href="javascript:confirm_del(\''.$comment['id'].'\')">'.
		                                '<i class="icon-trash"></i>Delete'.
		                                '</a>';
		                            ?></td>
		                    	</tr>
		                    <?php endforeach; ?>
		                    </tbody>
						</table>
                 </div>
                  <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT--> 
    
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 
<script language="javascript">

function confirm_feed_del(id) {
    if(confirm("Do you want to delete this post?")) {
        document.location.href = "<?php echo site_url('content/delete_community'); ?>/" + id + '?ref=<?php echo site_url('fan/edit_user') . '/' . $user['id']; ?>';
    }
}    
function confirm_del(id) {
	if(!confirm('Do you really want to delete this comment?')) return;

	document.location = "<?php echo site_url('fan/delete_comment/'.$user['id']); ?>" + '/'+id + "?ref=<?php echo $ref_url; ?>";
}
</script>

