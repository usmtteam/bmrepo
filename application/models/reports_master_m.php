<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_Master_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

     
      // get a list of all content for reports
    
    // function get_oflo_reports_monthly($id){

    //  $sql = "SELECT orm.video_views, orm.page_views_ga
    //          FROM oflo_reports_monthly orm
    //          WHERE channel_id = $id
    //             ";

    //  $query = $this->db->query($sql);
    //  $rows = $query->result_array();

    //   return $rows;

    // }


    function get_reports_by_id($id, $month_input){

     $sql = "SELECT *, SUM(views) as total_views, SUM(revenue) as total_revenue FROM

            (SELECT orm.content_id, orm.video_views as views, orm.revenue, orm.month, ev.description, ev.is_premium, ev.time_stamp
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             
             ";


        if($month_input == 'all' || $month_input == 'by_months'){
            $sql .= " WHERE channel_id = $id AND is_mobile = 1
             
             UNION
             
             SELECT orm.content_id, orm.page_views_ga as views, orm.revenue, orm.month, ev.description, ev.is_premium, ev.time_stamp
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             WHERE channel_id = $id AND is_mobile = 0

             ) as tbl1";
        }else {
            $sql .=" WHERE channel_id = $id AND is_mobile = 1 AND month = '$month_input'
             
             UNION
             
             SELECT orm.content_id, orm.page_views_ga as views, orm.revenue, orm.month, ev.description, ev.is_premium, ev.time_stamp
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             WHERE channel_id = $id AND is_mobile = 0 AND month = '$month_input'

             ) as tbl1";
        }
    


        if (isset($_POST['premiumCheckbox'])){
            if($month_input == 'by_months'){
                $sql .= " WHERE tbl1.is_premium = 1
                            GROUP BY month";

                }else{
                $sql .= " WHERE tbl1.is_premium = 1
                            GROUP BY content_id";
            }
        }else {

            if($month_input == 'by_months'){
                $sql .=" GROUP BY month";
            }else{

                $sql .=" GROUP BY content_id";
            }
        }
    

     $query = $this->db->query($sql);
     $rows = $query->result_array();


      return $rows;    

    }
    function updateDuration($video_id, $duration){

         $sql = "UPDATE enr_video 
                SET edit_duration = $duration 
                WHERE id = $video_id
                ";

        $querynew = $this->db->query($sql);
    }
    
     function get_duration_by_id($id){

     $sql = "SELECT id, cid, edit_duration as total_duration, time_stamp, description
             FROM enr_video
             WHERE cid = $id


            ";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }

     function get_channel_names(){

     $sql = "SELECT name, id
             FROM enr_chanel
             WHERE is_oflo = 'y'";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }


     function get_channel_name_by_id($id){

     $sql = "SELECT name
             FROM enr_chanel
             WHERE id = $id";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }

    // $l_nSql = "SELECT * FROM oflo_reports_monthly WHERE content_id = ".$l_aJSON['video_db_id']."";  
    //                     $l_nData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_nSql));
                       
    //                     foreach($l_nData as $rep_value){
    //                         if($rep_value['is_mobile']==1){
    //                             $total_v_views += $rep_value['video_views'];
    //                         }else{
    //                             $total_v_views += $rep_value['page_views_ga'];
    //                         }
    //                         $total_v_revenue += $rep_value['revenue'];
    //                     }
   
}