<?

    
	require(dirname(__FILE__).'/../../../config/settings.php');



	$l_nItemId = Utils::cleanPostDataValue($_GET['p_nItemId']);    
    $l_xItemManager = new ItemManager();
    
    //=======================================================================================
    //                                          LIVE
    //=======================================================================================
    if(preg_match("/^live/",$l_nItemId)){

        $l_xItem = $l_xItemManager->getByUri($l_nItemId);

        
        if(!$l_xItem)
            return;
        
        $l_sOutputId = $l_xItem->getZencoderOutputId();
        
        try {
        
            spl_autoload_unregister('coreAutoload');
            require(ZENCODER_CLASS);
            $l_xZencoder = new Services_Zencoder(ZENCODER_API_KEY);
            $l_xEncodingJob = $l_xZencoder->outputs->details($l_sOutputId);   
            
            spl_autoload_register('coreAutoload');
            if($l_xEncodingJob->state == 'finished'){
                sleep(30);
                $l_xItemManager->endLiveStreamById($l_xItem->getId());

                $l_sCommand = PHP_PATH.' '.CRON_LIVE_VIDEO_UPDATE;
                exec($l_sCommand . " > /dev/null &");   
            }
            
        } catch (Services_Zencoder_Exception $e) { }

        exit;

    }else{
        $l_nItemId = intval($l_nItemId);
    }





    //=======================================================================================
    //                                          ITEMS
    //=======================================================================================

	if($l_nItemId){

		if($l_xItemManager->setItemActive($l_nItemId)){

        	$l_xItem = $l_xItemManager->getById($l_nItemId);
            $l_sOutputId = $l_xItem->getZencoderOutputId();


            if($l_sOutputId){

                try {

                    spl_autoload_unregister('coreAutoload');
                    require(ZENCODER_CLASS);                    
                    $l_xZencoder = new Services_Zencoder(ZENCODER_API_KEY);
                    $l_xEncodingJob = $l_xZencoder->outputs->details($l_sOutputId);   
                    $l_nWidth = $l_xEncodingJob->width;
                    $l_nHeight = $l_xEncodingJob->height;

                    $l_nDuration = round($l_xEncodingJob->duration_in_ms)/1000;

                    if($l_nDuration == 0 || !$l_nDuration)
                        $l_nDuration = $l_xItem->getSeconds();

                    spl_autoload_register('coreAutoload');
                    $l_xItemManager->setItemDurationAndOrientationAndCallbackData($l_nItemId,intval($l_nDuration),(   (intval($l_nWidth) < intval($l_nHeight)) ? 1 : 0 ), serialize($l_xEncodingJob)   );
                    $l_xItem = $l_xItemManager->getById($l_nItemId);

                } catch (Services_Zencoder_Exception $e) { }
            }

            spl_autoload_register('coreAutoload');

            


            try{

                if($l_xItem->getItemsTypesId() == ITEM_AUDIO || $l_xItem->getItemsTypesId() == ITEM_ROBOCALL){

                    $l_sMp3File = 'http://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'.s3.amazonaws.com/'.preg_replace("/\.aac$/i",".mp3",$l_xItem->getAssetsURI());
                    $l_sMp3PreviewFile = 'http://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'.s3.amazonaws.com/'.preg_replace("/\.aac$/i","22301a.mp3",$l_xItem->getAssetsURI());
                    
                    echo "l_sMp3File : $l_sMp3File <Br>\n";
                    echo "l_sMp3PreviewFile : $l_sMp3PreviewFile <Br>\n";


                    if($l_xItem->getItemsTypesId() == ITEM_ROBOCALL){
                        
                        $l_sSearchFor = preg_replace("/\..+/","",$l_xItem->getAssetsURI());
                        $l_sSql = 'SELECT * FROM reach_log WHERE message like "%'.$l_sSearchFor.'%" LIMIT 1';
                    
                        $l_xData = $l_xItemManager->_xSqlManager->getRow($l_xItemManager->_xSqlManager->runQuery($l_sSql));
                        if($l_xData){
                            ///Duplicate Call - seen robo go out 4 times
                            echo "Duplicate";
                            return;
                        }

                        // CHECK IF ZERO;
                        if(intval($l_xItem->getSeconds()) < 1){
                            echo "zero seconds";
                            return;
                        }
                    }


                    



                    if($l_xItem->getItemsTypesId() == ITEM_ROBOCALL){
                        $l_xReachManager = new ReachManager();
                        $l_xReachManager->makeRoboCallToUsers($l_xItem->getCreatorsId(),$l_sMp3File);
                    }

                }else if($l_xItem->getItemsTypesId() == ITEM_VIDEO){

                    $l_sFileWithoutExtension = FILES_PATH.preg_replace("/\..{3,4}$/","",$l_xItem->getAssetsURI());
                    $l_sJpgFile = 'http://'.AMAZON_AWS_BUCKET_FOR_ASSETS.'.s3.amazonaws.com/'.preg_replace("/\..{3,4}$/",".jpg",$l_xItem->getAssetsURI());

                    $l_sSavingFolder = dirname($l_sFileWithoutExtension);

                    if(!is_dir($l_sSavingFolder)){
                        mkdir($l_sSavingFolder,0777,true);
                    }

                    $f = file_get_contents($l_sJpgFile);

                    file_put_contents($l_sFileWithoutExtension.'.jpg',$f);
                    Utils::buildImageSizesAndCrop($l_sFileWithoutExtension.'.jpg',array(FRONT_ASSET_SIZE_TYPE_PREVIEW,FRONT_ASSET_SIZE_TYPE_SIZED,FRONT_ASSET_SIZE_TYPE_2_BLOCKS_WIDE,FRONT_ASSET_SIZE_TYPE_2_BLOCKS_TALL,FRONT_ASSET_SIZE_TYPE_MOBILE_GRID,FRONT_ASSET_SIZE_TYPE_MOBILE_SIZED,FRONT_ASSET_SIZE_TYPE_MOBILE_2_BLOCKS_WIDE,FRONT_ASSET_SIZE_TYPE_MOBILE_2_BLOCKS_TALL));
                }
            }catch(Exception $e){}


            if($l_xItem->getItemsTypesId() == ITEM_AUDIO || $l_xItem->getItemsTypesId() == ITEM_VIDEO ){

                $l_xCreatorManager = new CreatorManager();
                $l_xItemManager = new ItemManager();

                $l_xBitlyURL = $l_xItemManager->generateBitlyPreviewURLWithItem($l_xItem);
                $l_xItem = new Item($l_xItemManager->_xSqlManager->insertShortURLForId($l_xItem->getId(),$l_xBitlyURL));
                    
                $l_xItemManager->setItemInMemcacheForItemId($l_nItemId,$l_xItem);
                $l_xItemManager->newItemInsertedForCreatorId($l_xItem->getCreatorsId(),$l_xItem);
                

                $l_xCreator = $l_xCreatorManager->getCreatorFromMemcacheById($l_xItem->getCreatorsId());
                if($l_xItem->shouldSendToTwitter())     $l_xItemManager->postToSocialNetwork($l_xCreator,'TWITTER',$l_xItem->getItemsTypesId(),$l_xItem->getDescription(),$l_xItem->getId());
                if($l_xItem->shouldSendToFacebook())    $l_xItemManager->postToSocialNetwork($l_xCreator,'FACEBOOK',$l_xItem->getItemsTypesId(),$l_xItem->getDescription(),$l_xItem->getId());

                //Post On Upfront Twitter Feed
                $l_xItemManager->postToSocialNetwork($l_xCreator,'TWITTER',$l_xItem->getItemsTypesId(),$l_xItem->getDescription(),$l_xItem->getId(),true);

                $l_sCommand = PHP_PATH.' '.CRON_SEND_BADGE_FOR_NEW_ITEM.' '.$l_xItem->getCreatorsId();
                #exec($l_sCommand . " > /dev/null &");   

                #$l_sCommand = PHP_PATH.' '.CRON_SEND_PUSH_NOTICE_FOR_NEW_ITEM.' '.$l_xItem->getItemsTypesId().' '.$l_xItem->getCreatorsId();
                #exec($l_sCommand . " > /dev/null &"); 


                // try{ 
    //                 $l_sShortURL = $this->generateBitlyPreviewURLWithItem($l_xItem); 
    //                 $l_xItem = new Item($this->_xSqlManager->insertShortURLForId($l_nSuccessId,$l_sShortURL));
    //             }catch(Exception $e){}  

                if($l_xItem->getItemsTypesId() == ITEM_AUDIO){

                   


                    // echo "Sound cloud";
                    // // Push Preview audio to sound cloud
                    // require_once SOUNDCLOUD_BASE_DIR.'Services/Soundcloud.php';

                    // try {
                    //     //$l_xCreator

                    //     echo "Sound cloud try";
                    //     $l_sFileWithoutExtension = FILES_PATH.preg_replace("/\..{3,4}$/","",$l_xItem->getAssetsURI());
                    //     echo "l_sFileWithoutExtension : $l_sFileWithoutExtension <Br>\n";
                    //     $l_sPreviewFileSavePath = $l_sFileWithoutExtension.'22301a.mp3';
                    //     $f = file_get_contents($l_sMp3PreviewFile) or die('cannot get file'); 
                        
                    //     file_put_contents($l_sPreviewFileSavePath,$f) or die('cannot write file'); 

                    //     $client = new Services_Soundcloud(SOUNDCLOUD_API_CLIENT_ID,SOUNDCLOUD_API_CLIENT_SECRET);
                    //     $client->setAccessToken(SOUNDCLOUD_ACCESS_TOKEN);

                    //     echo "\nTrying to send: $l_sPreviewFileSavePath \n";

                    //     $track = json_decode($client->post('tracks', array(
                    //     'track[title]' => 'Preview item',
                    //     'track[asset_data]' => '@'.$l_sPreviewFileSavePath,
                    //     )));

                    //     print_r($track);
                    //     //$track->permalink_url
                    //     // SET SOund cloud file
                    //     //soundcloud_url

                    //     } catch (Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) {
                    //         print $e->getMessage();
                    //     }
                    // // print track link
                    // //print $track->permalink_url;




                }

            }





        }else{
        	
        }
        
    }


 






?>