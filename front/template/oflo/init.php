<?php

//require(__DIR__.'/settings.php');
$oflo_global_ga_key = "UA-73046225-1";

//==================================================================
// Get URI segments
//==================================================================

$p_nContentId = strip_tags(trim($_GET['p_nContentId']));
$l_nAssetId = intval($p_nContentId); 

$p_Category = strip_tags(trim($_GET['cat']));

$search = strip_tags(trim($_GET['search']));

$l_bIsIE10 = preg_match("/MSIE (10|9|8|7)/",$_SERVER['HTTP_USER_AGENT']);

if (!empty($_GET['page'])){
    $page = strip_tags(trim($_GET['page']));
} else {
    $page = 0;
}


$items_per_page = strip_tags(trim($_GET['items_per_page']));

//echo "<BR><BR>\$_SERVER: ";
//var_dump($_SERVER);
//exit;

//echo "<BR><BR>\$_SERVER['SC_CHANNEL']: ";
//var_dump($_SERVER['SC_CHANNEL']);
//exit;

//==================================================================
// Perform a regular expression match on $_SERVER['HTTP_HOST']
//==================================================================
//    preg_match("/(.+\.)?(.+)\.(oflo)/", $_SERVER['HTTP_HOST'], $l_aMatches);
preg_match("/(.+)\.(.+)\.(breakingmuscle)/", $_SERVER['HTTP_HOST'], $l_aMatches);
$matches_count = count($l_aMatches);

//if($_SERVER['SC_CHANNEL']){
//    $l_sSlug = strtolower($_SERVER['SC_CHANNEL']);
//}

//==================================================================
// Set O-Flo Channel Slug
//==================================================================
if ($matches_count > 3) {
    $l_sSlug = strtolower($l_aMatches[1]);
} else {
    $l_sSlug = "oflo_home"; // O-Flo home page.
}

//==================================================================
// Hardcode for O-Flo Channel development and local testing
//==================================================================
//$l_sSlug = "oflo_home";       // O-Flo home page.
//$l_sSlug = "andrew"; // Channel URL for individual O-Flo page.
//

//echo "<BR><BR>\$l_sSlug: ";
//var_dump($l_sSlug);
//exit;
//
//echo "<BR><BR>\$_SERVER['HTTP_HOST']: ";
//var_dump($_SERVER['HTTP_HOST']);
//exit;

if($l_sSlug != "oflo_home"){
    //==================================================================
    //              GET CACHE BASED ON CHANNEL
    //==================================================================
    $l_aCachedContent = Memcaching::getInstance()->getKey(Memcaching::makeKey($l_sSlug));

    if(!$l_aCachedContent){
        $l_xChannelManager = new ChannelManager();
        $l_xChannel = $l_xChannelManager->setChannelInMemcacheBySlugAndReturn($l_sSlug);

//        if(!$l_xChannel || $l_xChannel->raw['is_oflo'] != 'y') Utils::show404();
        if(!$l_xChannel) Utils::show404();
    }else{
        $l_xChannel = $l_aCachedContent['value'];
    }

    AppController::getInstance()->setChannel($l_xChannel);

    $l_xChannel = AppController::getInstance()->getChannel();
    $l_nChannelId = $l_xChannel->cid;

    // O-Flo Channel individual page
    if ($l_xChannel && $_SERVER['REQUEST_URI'] == '/') {

        header('Location: /ocontent/video/recent/');
        exit;
    }
}

//echo "<BR><BR><BR>\$l_xChannel: ";
//var_dump($l_xChannel);
//echo "<BR><BR><BR>\$l_nChannelId: ";
//var_dump($l_nChannelId);
//exit;

//==================================================================================================================
//                                              GETTERS
//==================================================================================================================

//==================================================================
// O-Flo Homepage
//==================================================================

$l_xHubManager = new HubManager();

if (strlen($_SERVER['REQUEST_URI']) >= 1){

    $l_xOfloHomepageSlider = $l_xHubManager->getSliderContent($l_sSlug."oflo_homepage_slider_content");
    $l_xChannels = $l_xHubManager->getOfloChannels("oflo_channels");
    $l_xOfloHomepageVideos = $l_xHubManager->getHomepageVideoGrid($l_sSlug."oflo_homepage_content");

    // $l_xOfloHomepagePriorityVideos = $l_xHubManager->getHomepagePriorityVideoGrid($l_sSlug."oflo_homepage_priority_content");

    // $l_xOfloHomepageTrending = $l_xHubManager->getHomepageTrending($l_sSlug."oflo_homepage_trending");
    $homeviews = array();
    foreach($l_xOfloHomepageVideos as $l_xOfloVideoId){
      $l_xSql = AppController::getInstance()->getConnection();
      $video_id = $l_xOfloVideoId['id'];
      
        $l_sSql = " SELECT *, SUM(views) as total_views FROM 
                    (SELECT content_id,
                    CASE 
                        WHEN is_mobile = 1
                          THEN video_views
                          ELSE  page_views_ga
                    END as views
                    FROM oflo_reports_monthly 
                    WHERE content_id = $video_id
                    )as tbl1
                    GROUP BY content_id
                ";
      $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
      $homeviews[] = $l_xData;
      
    }
}

//==================================================================
// O-Flo Search
//==================================================================

if  ( !empty ($search)){$l_xOfloSearchResults = $l_xHubManager->getSearchResults($search);

    $search_page_count = 15;
    $search_total_count = count($l_xOfloSearchResults) ;
    $search_total_page_count = ceil($search_total_count / $search_page_count);

      if (($page - 2) < 0) { $search_first_page = 0; }
            else { $search_first_page = $page - 2; }
           

            // last page
            if ($page <= 2){

                $search_last_page = $search_total_page_count;

            } else {

                if (($page + 3) > $search_total_page_count) { $search_last_page = $search_total_page_count; }
                else { $search_last_page = $page + 3; }

            }

            $search_last_page = $search_last_page;

    // Clean up base_url
    $search_base_url = preg_replace('/&page=\d+/u', '', $_SERVER['REQUEST_URI']);

    // Add GET
    if (strpos($search_base_url, "?") === FALSE){
        $search_base_url .= "?";
    }

   }

//==================================================================
// O-Flo Category
//==================================================================

$l_xCategoryManager = new CategoryManager();
$l_xOfloCategories = $l_xCategoryManager->getCategoryMenu('getCategoryMenuFromMemcacheAndStoreIfFail');

if  ( !empty ($p_Category)){$l_xOfloCategoryByName = $l_xCategoryManager->getCatListByCatName($p_Category);
    $page_count = 16;
    $total_count = count($l_xOfloCategoryByName);

    $total_page_count = ceil($total_count / $page_count);

      if (($page - 2) < 0) { $first_page = 0; }
            else { $first_page = $page - 2; }
           

            // last page
            if ($page <= 2){

                $last_page = $total_page_count;

            } else {

                if (($page + 3) > $total_page_count) { $last_page = $total_page_count; }
                else { $last_page = $page + 3; }

            }

            $last_page = $last_page;


    //Clean up base_url
    $cat_base_url = preg_replace('/&page=\d+/u', '', $_SERVER['REQUEST_URI']);

    // // Add GET
    if (strpos($cat_base_url, "?") === FALSE){
        $cat_base_url .= "?";
    }
}
if  ( empty ($p_Category)){$l_xOfloCategoryByName = $l_xCategoryManager->getCatList("all_categories");
    $page_count = 16;
    $total_count = count($l_xOfloCategoryByName);

    $total_page_count = ceil($total_count / $page_count);

         if (($page - 2) < 0) { $first_page = 0; }
            else { $first_page = $page - 2; }
           

            //  last page
            if ($page <= 2){

                $last_page = $total_page_count;

            } else {

                if (($page + 3) > $total_page_count) {$last_page = $total_page_count; }
                else { $last_page = $page + 3; }

            }
              $last_page= $last_page;

    //Clean up base_url
    $cat_base_url = preg_replace('/&page=\d+/u', '', $_SERVER['REQUEST_URI']);

    // // Add GET
    if (strpos($cat_base_url, "?") === FALSE){
        $cat_base_url .= "?";
    }
}

    $categoryViews = array();
    foreach($l_xOfloCategoryByName as $l_xOfloVideoId){
      $l_xSql = AppController::getInstance()->getConnection();
      $video_id = $l_xOfloVideoId['id'];
      
        $l_sSql = " SELECT *, SUM(views) as total_views FROM 
                    (SELECT content_id,
                    CASE 
                        WHEN is_mobile = 1
                          THEN video_views
                          ELSE  page_views_ga
                    END as views
                    FROM oflo_reports_monthly 
                    WHERE content_id = $video_id
                    )as tbl1
                    GROUP BY content_id
                ";
      $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
      $categoryViews[] = $l_xData;
      
    }

// Landing page

//if (stripos($_SERVER['REQUEST_URI'], "/ocontent/") == 0){
if ($l_sSlug != "oflo_home"){

    //==================================================================================================================
    //                                     GET CATEGORY FROM ASSET ID
    //==================================================================================================================
    // landing page carousels.
    $l_xOfloCats = $l_xCategoryManager->getCategoryfromAssetId($l_nAssetId);

     $numbers = array(0,1, 2, 3, 4, 5, 6, 7);
    $number =(array_rand($numbers, 2));

     if ($l_xOfloCats[0]['cat_name']) {
        $p_cat = $l_xOfloCats[1]['cat_name'];
      }else {
        $p_cats = $l_xOfloCats[0]['cat_name'];
      }

     if (empty ($p_cats)){
             // randomizing carousels if empty
            $p_cats = $l_xOfloCategories[$number[0]]['cat_name'];
     }

     if ($l_xOfloCats[1]['cat_name']== $p_cats) {
        $p_cat = $l_xOfloCats[2]['cat_name'];
      }else {
        $p_cat = $l_xOfloCats[1]['cat_name'];
      }

     if (empty ($p_cat)){
             // randomizing carousels if empty
            $p_cat = $l_xOfloCategories[$number[1]]['cat_name'];

     }

    if  ( !empty ($p_cats)){$l_xOfloGetCats = $l_xCategoryManager->getCatListByCatName($p_cats);}
    if  ( !empty ($p_cat)){$l_xOfloGetCat   = $l_xCategoryManager->getCatListByCatName($p_cat);}

    // Cache Latest content for channel

    //======================================================================
    //                  CACHE LATEST VIDEOS LOOK UP
    //======================================================================

    // Los Angeles timezone
    // date_default_timezone_set('UTC');
    // $utc_time = date("Y-m-d h:i:sa");

    // date_default_timezone_set('America/Los_Angeles');
    // $pacific_time = date("Y-m-d h:i:sa");

    // $hours_diff = $utc_time - $pacific_time;

    $l_sMemcacheKey = Memcaching::makeKey($l_sSlug.'oflo-fp-videos');
    $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    if(!$l_aCachedContent){

        $l_xSql = AppController::getInstance()->getConnection();

        $l_sSql = "
                  SELECT ec.url, ev.*, UNIX_TIMESTAMP(ev.start_date) as start_date_timestamp
                    
                   FROM enr_video ev
                   LEFT JOIN enr_chanel ec ON ec.id = ev.cid
                   WHERE ev.is_oflo = 'y'
                   AND ev.is_publish = 1
                   AND ev.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                   ".(!empty($l_nChannelId) ? "AND ev.cid = $l_nChannelId " : '')."
                   ORDER BY id DESC
                   LIMIT 10
                   ";

        $l_aFPVideos =  $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

        $channelViews = array();
        foreach($l_aFPVideos as $l_xOfloVideoId){
          
          $video_id = $l_xOfloVideoId['id'];
          
            $l_sSql = " SELECT *, SUM(views) as total_views FROM 
                        (SELECT content_id,
                        CASE 
                            WHEN is_mobile = 1
                              THEN video_views
                              ELSE  page_views_ga
                        END as views
                        FROM oflo_reports_monthly 
                        WHERE content_id = $video_id
                        )as tbl1
                        GROUP BY content_id
                    ";
          $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
          $channelViews[] = $l_xData;
          
        }
        // $channelViews;

        Memcaching::getInstance()->store($l_sMemcacheKey, $l_aFPVideos, MEMCACHE_DURATION_FP_VIDEOS);

    }else{
        $l_aFPVideos = $l_aCachedContent['value'];
    }

    //======================================================================
    //                  CACHE LATEST OFLO PHOTOS
    //======================================================================

    $l_sMemcacheKey = Memcaching::makeKey($l_sSlug.'oflo-photos');
    $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    if(!$l_aCachedContent) {

        $l_xSql = AppController::getInstance()->getConnection();

        // Latest photos
        $l_sSql = "SELECT ec.url, ep.*, UNIX_TIMESTAMP(ep.start_date) as start_date_timestamp
                   FROM enr_photo ep
                   LEFT JOIN enr_chanel ec on ec.id = ep.cid
                   WHERE ep.is_oflo = 'y'
                   AND ep.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                   ".(!empty($l_nChannelId) ? "AND ep.cid = $l_nChannelId " : '')."
                   ORDER BY ep.id DESC
                   LIMIT 10";

        $l_aAllFoundAssets = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

    }else{

        $l_aAllFoundAssets = $l_aCachedContent['value'];
    }
} // end of Cache Latest content for channel

?>