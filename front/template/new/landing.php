<?php


  //Clean URLS only
  //Must have a content type before hitting the database/memcache
   error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

  if(!$_GET['p_sContentType']){
    
    if($_GET['p_sType']){

      if($_GET['video']){
        $l_sId = $_GET['video'];
        $l_sType = 'video';
      }elseif($_GET['bctid']){
        $l_sId = $_GET['bctid'];
        $l_sType = 'bcvideo';
      }elseif($_GET['photo']){
        $l_sId = $_GET['photo'];
        $l_sType = 'photo';
      }
      header('Location: /content/'.$l_sType.'/'.$l_sId.'/');
    }
    exit;
  }

  
  require(__DIR__.'/../config/init.php');
  require(__DIR__.'/_db.php');

  header( 'Content-type: text/html; charset=utf-8' );
  flush();

  if( ($l_xAssetResult['id'] == 1481 && $l_xAssetResult['cid'] == 24) || $_GET['mtest'] == 1 || $l_bIsVideo || !$l_bIsVideo) {

    if($l_xAssetResult['cid'] != 30){
    include(__DIR__.'/_mobile.php');

    //|| $l_bIsVideo
    // include(INCLUDE_TRACKING); 
    exit;

  }else{
    
  }
  }

  
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?= $l_xChannel->site_title ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <?

        if($l_bIsVideo){ ?>

           <meta property="og:title" content="WATCH VIDEO: <?= addslashes($l_xAssetResult['description']); ?>" />
                <meta property="og:description" content="<?= addslashes($l_xAssetResult['description']); ?>"/>

        <?   if($p_sPageType=="inline"){ ?>
                
                <meta property="og:type" content="movie" />
                <meta property="og:video:height" content="270"/>
                <meta property="og:video:width" content="480"/>
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:video" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
                <meta property="og:video:secure_url" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
                <meta property="og:image" content="<?= $l_sImageUrl ?>"/>
                <meta property="og:video:type" content="application/x-shockwave-flash">
            <? } else if($p_sPageType == "landingpage"){ ?>
               
                <meta property="og:type" content="website" />
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:image" content="<?= $l_sImageUrl ?>" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:width" content="1200" />

                
        <? } else if($p_sPageType=="twitter_player"){ ?>
       
               <meta property="twitter:card" content="player">
               <meta property="twitter:title" content="<?= $l_xChannel->site_title ?>">
               <meta property="twitter:description" content="<?= $l_xAssetResult['description']; ?>">
               <meta property="twitter:image" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/watermark/<?= $l_xAssetResult['image_path']; ?>">
               <meta property="twitter:image:width" content="480">
               <meta property="twitter:image:height" content="270">
               <meta property="twitter:site" content="@starsitenetwork">
               <meta property="twitter:player:width" content="480">
               <meta property="twitter:player:height" content="270">
               <meta property="twitter:player" content="<?= $l_sCurrentAbsoluteSecureURL; ?>">
        <? }else if($p_sPageType=="twitter"){ ?>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_xAssetResult['description']; ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


        <? } else { ?>
              <meta property="og:type" content="website" />
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:image" content="<?= $l_sImageUrl ?>" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:width" content="1200" />
                
        <? }
    }else{  

        if($p_sPageType=="twitter_player"){ ?>

        <? //<meta name="twitter:card" content="photo" /> ?>
         <meta name="twitter:card" content="player" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_xAssetResult['description']; ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
        
        <? }else if($p_sPageType=="twitter"){ ?>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_xAssetResult['description']; ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


        <? }else{} ?>

        <meta property="og:title" content="PHOTO: <?= $l_xAssetResult['description']; ?>" />
        <meta property="og:description" content="<?= $l_xAssetResult['description']; ?>"/>
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
        <meta property="og:image" content="<?= $l_sImageUrl ?>" />
        <meta property="og:image:type" content="image/png" />        
    <? } ?>
    <? include(INCLUDE_HEAD); ?>
</head>
<body class="cbp-spmenu-push viewpost">

<?

flush();
ob_flush();

    switch($p_sPageType){
        case 'twitter_player': include(__DIR__.'/_twitter_content.php'); break;
        case 'twitter': include(__DIR__.'/_content.php'); break;
        default : 
          include(__DIR__.'/_content.php'); break;
    }
      include(INCLUDE_TRACKING); 
      include(INCLUDE_FOOTER); 
?>
    </body>
</html>
