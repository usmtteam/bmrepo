
<meta name="Title" content="Term Sheet">
<meta name="Keywords" content="">
<meta http-equiv="Content-Type" content="text/html; charset=macintosh">
<link rel="Original-File" href="file://localhost/Users/Ian/Library/Containers/com.apple.mail/Data/Library/Mail%20Downloads/20BB53A9-E3A9-46CA-9CA0-ACCE416B1360/Terms_and_Conditions%20FINAL%20FOR%20POSTING%2010.07.14.doc">
<meta name="ProgId" content="Word.Document">
<meta name="Generator" content="Microsoft Word 14">
<meta name="Originator" content="Microsoft Word 14">
<link rel="File-List" href="Word%20Work%20File%20D__files/filelist.xml">
<link rel="Edit-Time-Data" href="Word%20Work%20File%20D__files/editdata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Term Sheet </title>
<!--[if gte mso 9]><xml>
<o:DocumentProperties>
<o:Author>James Polsen</o:Author>
<o:LastAuthor>Charles Sanders</o:LastAuthor>
<o:Revision>2</o:Revision>
<o:TotalTime>2</o:TotalTime>
<o:LastPrinted>2014-09-30T16:38:00Z</o:LastPrinted>
<o:Created>2014-10-07T15:57:00Z</o:Created>
<o:LastSaved>2014-10-07T15:57:00Z</o:LastSaved>
<o:Pages>5</o:Pages>
<o:Words>2005</o:Words>
<o:Characters>11435</o:Characters>
<o:Company>StarClub, Inc.</o:Company>
<o:Lines>95</o:Lines>
<o:Paragraphs>26</o:Paragraphs>
<o:CharactersWithSpaces>13414</o:CharactersWithSpaces>
<o:Version>14.0</o:Version>
</o:DocumentProperties>
<o:OfficeDocumentSettings>
<o:PixelsPerInch>96</o:PixelsPerInch>
<o:TargetScreenSize>800x600</o:TargetScreenSize>
</o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel="themeData" href="Word%20Work%20File%20D__files/themedata.xml">
<!--[if gte mso 9]><xml>
<w:WordDocument>
<w:View>Print</w:View>
<w:Zoom>115</w:Zoom>
<w:TrackRevisions/>
<w:TrackMoves>false</w:TrackMoves>
<w:TrackFormatting/>
<w:PunctuationKerning/>
<w:ValidateAgainstSchemas/>
<w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
<w:IgnoreMixedContent>false</w:IgnoreMixedContent>
<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
<w:DoNotPromoteQF/>
<w:LidThemeOther>DE</w:LidThemeOther>
<w:LidThemeAsian>JA</w:LidThemeAsian>
<w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
<w:Compatibility>
<w:BreakWrappedTables/>
<w:SnapToGridInCell/>
<w:WrapTextWithPunct/>
<w:UseAsianBreakRules/>
<w:DontGrowAutofit/>
<w:DontUseIndentAsNumberingTabStop/>
<w:FELineBreak11/>
<w:WW11IndentRules/>
<w:DontAutofitConstrainedTables/>
<w:AutofitLikeWW11/>
<w:HangulWidthLikeWW11/>
<w:UseNormalStyleForList/>
<w:DontVertAlignCellWithSp/>
<w:DontBreakConstrainedForcedTables/>
<w:DontVertAlignInTxbx/>
<w:Word11KerningPairs/>
<w:CachedColBalance/>
<w:UseFELayout/>
</w:Compatibility>
<m:mathPr>
<m:mathFont m:val="Cambria Math"/>
<m:brkBin m:val="before"/>
<m:brkBinSub m:val="&#45;-"/>
<m:smallFrac m:val="off"/>
<m:dispDef/>
<m:lMargin m:val="0"/>
<m:rMargin m:val="0"/>
<m:defJc m:val="centerGroup"/>
<m:wrapIndent m:val="1440"/>
<m:intLim m:val="subSup"/>
<m:naryLim m:val="undOvr"/>
</m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
<w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
DefSemiHidden="false" DefQFormat="false" LatentStyleCount="276">
<w:LsdException Locked="true" QFormat="true" Name="Normal"/>
<w:LsdException Locked="true" QFormat="true" Name="heading 1"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 2"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 3"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 4"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 5"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 6"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 7"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 8"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="heading 9"/>
<w:LsdException Locked="true" Name="toc 1"/>
<w:LsdException Locked="true" Name="toc 2"/>
<w:LsdException Locked="true" Name="toc 3"/>
<w:LsdException Locked="true" Name="toc 4"/>
<w:LsdException Locked="true" Name="toc 5"/>
<w:LsdException Locked="true" Name="toc 6"/>
<w:LsdException Locked="true" Name="toc 7"/>
<w:LsdException Locked="true" Name="toc 8"/>
<w:LsdException Locked="true" Name="toc 9"/>
<w:LsdException Locked="true" SemiHidden="true" UnhideWhenUsed="true"
QFormat="true" Name="caption"/>
<w:LsdException Locked="true" QFormat="true" Name="Title"/>
<w:LsdException Locked="true" QFormat="true" Name="Subtitle"/>
<w:LsdException Locked="true" QFormat="true" Name="Strong"/>
<w:LsdException Locked="true" QFormat="true" Name="Emphasis"/>
<w:LsdException Locked="true" Name="Table Grid"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 1"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 2"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 3"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 4"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 5"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 6"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 7"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 8"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
UnhideWhenUsed="true" Name="Note Level 9"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true"
Name="Placeholder Text"/>
<w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
<w:LsdException Locked="false" Priority="61" Name="Light List"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
<w:LsdException Locked="false" Priority="99" SemiHidden="true" Name="Revision"/>
<w:LsdException Locked="false" Priority="34" QFormat="true"
Name="List Paragraph"/>
<w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
<w:LsdException Locked="false" Priority="30" QFormat="true"
Name="Intense Quote"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
<w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
<w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
<w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
<w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
<w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
<w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
<w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
<w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
<w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
<w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
<w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
<w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
<w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
<w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
<w:LsdException Locked="false" Priority="19" QFormat="true"
Name="Subtle Emphasis"/>
<w:LsdException Locked="false" Priority="21" QFormat="true"
Name="Intense Emphasis"/>
<w:LsdException Locked="false" Priority="31" QFormat="true"
Name="Subtle Reference"/>
<w:LsdException Locked="false" Priority="32" QFormat="true"
Name="Intense Reference"/>
<w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
<w:LsdException Locked="false" Priority="37" SemiHidden="true"
UnhideWhenUsed="true" Name="Bibliography"/>
<w:LsdException Locked="false" Priority="39" SemiHidden="true"
UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
</w:LatentStyles>
</xml><![endif]-->
<style>
<!--
/* Font Definitions */
@font-face
{font-family:Arial;
panose-1:2 11 6 4 2 2 2 2 2 4;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:3 0 0 0 1 0;}
@font-face
{font-family:Calibri;
panose-1:2 15 5 2 2 2 4 3 2 4;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:3 0 0 0 1 0;}
@font-face
{font-family:Cambria;
panose-1:2 4 5 3 5 4 6 3 2 4;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:3 0 0 0 1 0;}
@font-face
{font-family:"Arial Black";
panose-1:2 11 10 4 2 1 2 2 2 4;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:647 0 0 0 159 0;}
@font-face
{font-family:"Gill Sans Light";
panose-1:2 11 3 2 2 1 4 2 2 3;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:-2147483033 0 0 0 503 0;}
@font-face
{font-family:"MS ??";
panose-1:0 0 0 0 0 0 0 0 0 0;
mso-font-alt:"MS Mincho";
mso-font-charset:128;
mso-generic-font-family:auto;
mso-font-format:other;
mso-font-pitch:variable;
mso-font-signature:1 134676480 16 0 131072 0;}
@font-face
{font-family:"Lucida Grande";
panose-1:2 11 6 0 4 5 2 2 2 4;
mso-font-charset:0;
mso-generic-font-family:auto;
mso-font-pitch:variable;
mso-font-signature:-520090897 1342218751 0 0 447 0;}
@font-face
{font-family:Batang;
mso-font-alt:\00B9ÙÅÁ;
mso-font-charset:129;
mso-generic-font-family:roman;
mso-font-pitch:variable;
mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
{mso-style-unhide:no;
mso-style-qformat:yes;
mso-style-parent:"";
margin:0cm;
margin-bottom:.0001pt;
mso-pagination:widow-orphan;
font-size:12.0pt;
font-family:Cambria;
mso-fareast-font-family:"MS ??";
mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US;}
h1
{mso-style-unhide:no;
mso-style-qformat:yes;
mso-style-link:"Heading 1 Char";
mso-style-next:Normal;
margin-top:24.0pt;
margin-right:0cm;
margin-bottom:6.0pt;
margin-left:0cm;
text-align:center;
mso-pagination:widow-orphan;
page-break-after:avoid;
mso-outline-level:1;
font-size:11.0pt;
mso-bidi-font-size:10.0pt;
font-family:Arial;
text-transform:uppercase;
mso-font-kerning:14.0pt;
mso-ansi-language:EN-US;
mso-bidi-font-weight:normal;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
{mso-style-unhide:no;
mso-style-link:"Header Char";
margin:0cm;
margin-bottom:.0001pt;
mso-pagination:widow-orphan;
tab-stops:center 216.0pt right 432.0pt;
font-size:12.0pt;
font-family:Cambria;
mso-fareast-font-family:"MS ??";
mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
{mso-style-unhide:no;
mso-style-link:"Footer Char";
margin:0cm;
margin-bottom:.0001pt;
mso-pagination:widow-orphan;
tab-stops:center 216.0pt right 432.0pt;
font-size:12.0pt;
font-family:Cambria;
mso-fareast-font-family:"MS ??";
mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US;}
span.MsoPageNumber
{mso-style-noshow:yes;
mso-style-unhide:no;
mso-style-parent:"";
font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";}
p.MsoTitle, li.MsoTitle, div.MsoTitle
{mso-style-unhide:no;
mso-style-qformat:yes;
mso-style-locked:yes;
mso-style-link:"Title Char";
margin-top:0cm;
margin-right:0cm;
margin-bottom:12.0pt;
margin-left:0cm;
text-align:center;
mso-pagination:widow-orphan;
mso-outline-level:1;
font-size:12.0pt;
mso-bidi-font-size:10.0pt;
font-family:"Times New Roman";
mso-fareast-font-family:Batang;
mso-bidi-font-family:"Times New Roman";
mso-font-kerning:14.0pt;
mso-ansi-language:EN-US;
mso-fareast-language:KO;
font-weight:bold;
mso-bidi-font-weight:normal;}
p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2
{mso-style-unhide:no;
mso-style-link:"Body Text 2 Char";
margin-top:0cm;
margin-right:0cm;
margin-bottom:12.0pt;
margin-left:0cm;
line-height:14.0pt;
mso-line-height-rule:exactly;
mso-pagination:widow-orphan;
font-size:12.0pt;
mso-bidi-font-size:10.0pt;
font-family:"Times New Roman";
mso-fareast-font-family:Batang;
mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US;
mso-fareast-language:KO;}
a:link, span.MsoHyperlink
{mso-style-noshow:yes;
mso-style-unhide:no;
mso-style-parent:"";
font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";
color:blue;
text-decoration:underline;
text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
{mso-style-unhide:no;
color:purple;
mso-themecolor:followedhyperlink;
text-decoration:underline;
text-underline:single;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
{mso-style-noshow:yes;
mso-style-unhide:no;
mso-style-link:"Balloon Text Char";
margin:0cm;
margin-bottom:.0001pt;
mso-pagination:widow-orphan;
font-size:9.0pt;
font-family:"Lucida Grande";
mso-fareast-font-family:"MS ??";
mso-ansi-language:EN-US;}
span.HeaderChar
{mso-style-name:"Header Char";
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:Header;
font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";}
span.FooterChar
{mso-style-name:"Footer Char";
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:Footer;
font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";}
span.BalloonTextChar
{mso-style-name:"Balloon Text Char";
mso-style-noshow:yes;
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:"Balloon Text";
mso-ansi-font-size:9.0pt;
mso-bidi-font-size:9.0pt;
font-family:"Lucida Grande";
mso-ascii-font-family:"Lucida Grande";
mso-hansi-font-family:"Lucida Grande";
mso-bidi-font-family:"Lucida Grande";}
span.Heading1Char
{mso-style-name:"Heading 1 Char";
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:"Heading 1";
mso-ansi-font-size:10.0pt;
mso-bidi-font-size:10.0pt;
font-family:Arial;
mso-ascii-font-family:Arial;
mso-hansi-font-family:Arial;
mso-bidi-font-family:"Times New Roman";
text-transform:uppercase;
mso-font-kerning:14.0pt;
font-weight:bold;
mso-bidi-font-weight:normal;}
span.BodyText2Char
{mso-style-name:"Body Text 2 Char";
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:"Body Text 2";
mso-ansi-font-size:12.0pt;
font-family:"Times New Roman";
mso-ascii-font-family:"Times New Roman";
mso-fareast-font-family:Batang;
mso-hansi-font-family:"Times New Roman";
mso-fareast-language:KO;}
span.TitleChar
{mso-style-name:"Title Char";
mso-style-unhide:no;
mso-style-locked:yes;
mso-style-parent:"";
mso-style-link:Title;
mso-ansi-font-size:12.0pt;
font-family:"Times New Roman";
mso-ascii-font-family:"Times New Roman";
mso-fareast-font-family:Batang;
mso-hansi-font-family:"Times New Roman";
mso-font-kerning:14.0pt;
mso-fareast-language:KO;
font-weight:bold;
mso-bidi-font-weight:normal;}
.MsoChpDefault
{mso-style-type:export-only;
mso-default-props:yes;
mso-ascii-font-family:Cambria;
mso-fareast-font-family:"MS ??";
mso-hansi-font-family:Cambria;}
/* Page Definitions */
@page
{mso-footnote-separator:url(":Word Work File D__files:header.htm") fs;
mso-footnote-continuation-separator:url(":Word Work File D__files:header.htm") fcs;
mso-endnote-separator:url(":Word Work File D__files:header.htm") es;
mso-endnote-continuation-separator:url(":Word Work File D__files:header.htm") ecs;}
@page WordSection1
{size:612.0pt 792.0pt;
margin:72.0pt 72.0pt 72.0pt 72.0pt;
mso-header-margin:0cm;
mso-footer-margin:0cm;
mso-header:url(":Word Work File D__files:header.htm") h1;
mso-footer:url(":Word Work File D__files:header.htm") f1;
mso-paper-source:0;}
div.WordSection1
{page:WordSection1;}
/* List Definitions */
@list l0
{mso-list-id:1;
mso-list-type:hybrid;
mso-list-template-ids:1 1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l0:level1
{mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l0:level2
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level3
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level4
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level5
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level6
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level7
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level8
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l0:level9
{mso-level-start-at:0;
mso-level-text:"";
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:0cm;
text-indent:0cm;}
@list l1
{mso-list-id:2068139597;
mso-list-type:hybrid;
mso-list-template-ids:473572448 1501859574 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l1:level1
{mso-level-number-format:roman-upper;
mso-level-tab-stop:none;
mso-level-number-position:left;
margin-left:54.0pt;
text-indent:-36.0pt;
mso-ansi-font-weight:bold;}
@list l1:level2
{mso-level-number-format:alpha-lower;
mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l1:level3
{mso-level-number-format:roman-lower;
mso-level-tab-stop:none;
mso-level-number-position:right;
text-indent:-9.0pt;}
@list l1:level4
{mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l1:level5
{mso-level-number-format:alpha-lower;
mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l1:level6
{mso-level-number-format:roman-lower;
mso-level-tab-stop:none;
mso-level-number-position:right;
text-indent:-9.0pt;}
@list l1:level7
{mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l1:level8
{mso-level-number-format:alpha-lower;
mso-level-tab-stop:none;
mso-level-number-position:left;
text-indent:-18.0pt;}
@list l1:level9
{mso-level-number-format:roman-lower;
mso-level-tab-stop:none;
mso-level-number-position:right;
text-indent:-9.0pt;}
ol
{margin-bottom:0cm;}
ul
{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
/* Style Definitions */
table.MsoNormalTable
{mso-style-name:"Table Normal";
mso-tstyle-rowband-size:0;
mso-tstyle-colband-size:0;
mso-style-noshow:yes;
mso-style-unhide:no;
mso-style-parent:"";
mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
mso-para-margin:0cm;
mso-para-margin-bottom:.0001pt;
mso-pagination:widow-orphan;
font-size:10.0pt;
font-family:Cambria;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1"/>
</o:shapelayout></xml><![endif]-->

<style>
*{  font-size:10px}
</style>



<div class="WordSection1">

<p class="MsoNormal" align="center" style="text-align:center"><b><span lang="EN-US" style="font-family:&quot;Arial Black&quot;;mso-bidi-font-family:Arial"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal"><b><span lang="EN-US" style="font-size:18.0pt;font-family:
&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" align="center" style="text-align:center"><b><span lang="EN-US" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;">TERMS AND CONDITIONS
AGREEMENT<o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-left:108.0pt;text-indent:36.0pt"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" style="margin-bottom:12.0pt;text-align:justify;text-justify:
inter-ideograph;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">For good and
valuable consideration the receipt of which is hereby acknowledged, this
Agreement (the “Agreement”) dated as of the date of acceptance by means of a
click-through, is made by and between StarClub, Inc. (“Company”) and the user
designated by the registration data provided herewith (“Talent Partner”).<span style="mso-spacerun:yes">&nbsp; </span><span style="mso-bidi-font-weight:bold"><o:p></o:p></span></span></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><u><span lang="EN-US" style="mso-bidi-font-size:12.0pt">READ THESE TERMS AND CONDITIONS
CAREFULLY. <o:p></o:p></span></u></b></p>

<p class="MsoBodyText2"><b><span lang="EN-US" style="mso-bidi-font-size:12.0pt">1. <span style="mso-spacerun:yes">&nbsp;</span><u>StarClub Content Management System
Usage</u> -- </span></b><span lang="EN-US" style="mso-bidi-font-size:12.0pt">Company
hereby agrees to permit Talent Partner throughout the Term (as defined herein) to
use Company’s proprietary Content Management System and related software (“CMS”)
for the purposes of syndicating content (including, without limitation, any
images, video, audio or text and any copyrights embodied therein) (collectively
“Content”) to social media outlets on a single and/or multi-platform basis, in
order to facilitate the generation of revenue and the collection of data to be
shared by the parties pursuant to the terms hereof.<span style="mso-spacerun:yes">&nbsp; </span>Talent Partner hereby authorizes Company
to serve on a non-exclusive basis as its CMS provider, for the purposes of
performing, among other related tasks, syndicating all manner of Content at
Talent Partner’s direction to single and/or multiple social media outlets.<o:p></o:p></span></p>

<p class="MsoBodyText2"><b><span lang="EN-US">2. <span style="mso-spacerun:yes">&nbsp;</span><u>Term and Agreement to Abide</u> –
</span></b><span lang="EN-US" style="mso-bidi-font-weight:bold">This Agreement
shall remain in effect until terminated for any reason by either party on five
(5) business days’ written notice to the other (the “Term”).<span style="mso-spacerun:yes">&nbsp; </span></span><span lang="EN-US">Talent Partner
hereby warrants and represents that it shall abide by any and all provisions, terms,
conditions of use, and rules set forth herein, and any additional provisions, terms,
conditions of use, and rules (including but not limited to those concerning
privacy) that may in the future be established and posted by the Company.<span style="mso-spacerun:yes">&nbsp; </span>In such regard, it is specifically
agreed that Company may modify these terms and conditions or may add additional
terms and conditions in the future<b style="mso-bidi-font-weight:normal">. <u><span style="mso-spacerun:yes">&nbsp;</span>In the event that these terms and
conditions change, Company will notify Talent Partner by posting an
announcement on the “ABOUT STARCLUB” webpage.</u></b><u> <b style="mso-bidi-font-weight:
normal"><span style="mso-spacerun:yes">&nbsp;</span>Talent Partner is
encouraged to periodically check the Website to see if the terms and conditions
have changed. Talent Partner agrees that continued use of the CMS following
changes to Company’s terms and conditions constitutes acceptance of Company’s
amended terms and conditions</b></u>.</span></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">3.<span style="mso-spacerun:yes">&nbsp;
</span><u>Copyright Ownership of Content and Authorization by Talent Partner</u></span></b><span lang="EN-US" style="mso-bidi-font-size:12.0pt"> -- Copyright in original content posted
by Talent Partner through the Company’s CMS shall reside with Talent Partner.<span style="mso-spacerun:yes">&nbsp; </span>Talent Partner hereby grants to Company,
for the Term of the Agreement, a world-wide, non-exclusive, royalty-free,
paid-up, transferrable right and license, with the perpetual right to
sublicense and to make copies of, transmit and publicly perform, any Content of
Talent Partner that Talent Partner posts through Company’s CMS.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:12.0pt;text-align:justify;text-justify:
inter-ideograph;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;">4. <span style="mso-spacerun:yes">&nbsp;</span><u>Authorization
to Monetize</u> -- </span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">Talent
Partner hereby authorizes Company to take any and all steps that Company
determines, in its sole discretion, to be proper and reasonable in the interest
of maximizing monetization of the Content posted by Talent Partner hereunder
through Company’s CMS, including but not limited to activities designed to
maximize the generation of advertising, subscription, product sales (as such
sales may be agreed upon in writing by the parties), and data mining revenues.<span style="mso-spacerun:yes">&nbsp; </span><b style="mso-bidi-font-weight:normal"><u>Talent
Partner must notify Company in writing of any specific exclusions limiting the
scope of permissible monetization activities that Company may undertake
hereunder.<o:p></o:p></u></b></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">5.
<span style="mso-spacerun:yes">&nbsp;</span><u>Division of Revenue</u> – </span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;mso-bidi-font-weight:bold">The
parties shall divide all </span><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">Revenues
(as defined herein) actually received by Company on an equal, 50-50 basis between
them.<span style="mso-spacerun:yes">&nbsp; </span>For purposes hereof, “Revenue”
shall be defined as all identifiable income directly attributable to Talent
Partner, less third party fees and costs including but not limited to third
party collection and agency fees. <span style="mso-spacerun:yes">&nbsp;</span>Payment to Talent Partner of Talent
Partner’s share of Revenues shall be made by Company on a quarterly basis
within forty-five (45) days of the end of each calendar quarter, or on a more
frequent basis as Company may determine in its sole discretion.<span style="mso-spacerun:yes">&nbsp; </span>All such payments shall be accompanied
by statements of account, or by information sufficient to detail pertinent
calculations. <span style="mso-spacerun:yes">&nbsp;</span>To ensure proper
payment, Talent Partner is responsible for providing and maintaining accurate
contact and payment information, and shall be responsible for any charges
assessed by its bank or payment provider. <span style="mso-spacerun:yes">&nbsp;</span>Talent Partner shall have full audit
rights to examine at its own expense the books and records of Company as they
relate to this Agreement once per year during the Term, and once within six
months thereafter, at Company’s business offices.<span style="mso-spacerun:yes">&nbsp; </span><b style="mso-bidi-font-weight:normal"><u>Talent
Partner shall be responsible for satisfying its own taxation responsibilities
regarding all Revenue and other remuneration that Talent Partner may receive
hereunder.</u></b> <o:p></o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">6. <span style="mso-spacerun:yes">&nbsp;</span><u>Company Access to Social Media
Accounts</u> --&nbsp;</span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">
In order to maximize the ability of Company and Talent Partner to collect the monies
and data that are the basis of this Agreement, Talent Partner hereby agrees (i)
to follow all log-in procedures required by Company for use of the CMS, and
(ii) to timely take all necessary actions at the request of Company to confirm
to third parties the authority of Company, and to eliminate third-party imposed
restrictions and limitations on the ability of Company, to engage in revenue
and data collection.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US">7.
<span style="mso-spacerun:yes">&nbsp;</span><u>“AS IS” BASIS and LIMITATION OF
LIABILITY</u> --<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>THE
COMPANY’S CMS IS PROVIDED “AS IS.” <span style="mso-spacerun:yes">&nbsp;</span>TALENT
PARTNER AGREES THAT ANY USE OF THE CMS IS AT TALENT PARTNER’S SOLE AND ABSOLUTE
RISK. <span style="mso-spacerun:yes">&nbsp;</span>IN NO EVENT, WILL COMPANY BE
LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF OR INABILITY TO USE THE CMS, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.<span style="mso-spacerun:yes">&nbsp;
</span>SPECIFICALLY, TALENT PARTNER AGREES THAT COMPANY IS NOT RESPONSIBLE FOR
ANY DAMAGES INCURRED BY TALENT PARTNER, INCLUDING BUT NOT LIMITED TO, THOSE
INCURRED AS A RESULT OF LOST PROFITS OR REVENUE, LOSS OF REPUTATION,
DEFAMATION, COPYRIGHT AND/OR TRADEMARK INFRINGEMENT, LOSS OF DATA, DISCLOSURE
OF DATA, OR CLAIMS BY THIRD PARTIES, OR FOR OTHER SIMILAR COSTS. </span></b><b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><span style="mso-spacerun:yes">&nbsp;</span><u>Further, e</u></span></b><b style="mso-bidi-font-weight:normal"><u><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;">ach party acknowledges that the other party has entered into
this Agreement relying on the limitations of liability stated herein and that
those limitations are an essential basis of the bargain between the parties</span></u></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">.<u><o:p></o:p></u></span></b></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">8.<span style="mso-spacerun:yes">&nbsp;
</span><u>Ownership of CMS</u> </span></b><span lang="EN-US" style="mso-bidi-font-size:
12.0pt">-- Company shall retain the ownership of the CMS at all times.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">9.
<u>Prohibited Uses of CMS</u> </span></b><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;">--<span style="mso-spacerun:yes">&nbsp; </span>Talent
Partner agrees that it may not: <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(a) Make copies of or further
distribute the Company’s CMS, including copying it onto any other medium;<span style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(b) Distribute, rent,
sublicense, lease, resell, or assign the Company’s CMS;<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><span style="mso-spacerun:yes">&nbsp;</span><o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(c) Alter, modify or adapt the
Company’s CMS including but not limited to, translating, decompiling, disassembling,
reverse engineering, or creating derivative works; <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(d) Resell, rent or otherwise
provide access to Company’s CMS to a third party; <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(e) Engage in any activity or post
any Content to Company’s CMS that violates local, state, or federal law;<o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(f) Make any statement that would
constitute a defamatory statement regarding any other user or operator of the
CMS, or post Content that is hate speech, threatening speech, harassment,
bullying or pornography, incites violence, or contains graphic or gratuitous
violence, each as determined by Company in its sole and absolute discretion;<o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(g) Post
unauthorized commercial communications (such as spam or other advertising)
through Company’s CMS;<o:p></o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(h) Collect
other CMS users' Content or information, or otherwise access the CMS using
automated means (such as harvesting bots, robots, spiders, or scrapers) without
Company’s prior written permission;<o:p></o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(i) Upload
to the CMS viruses or other malicious code;<o:p></o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(j) Solicit
login information or access an account belonging to someone else or permit
anyone else to use Talent Partner’s account;<o:p></o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(k) Use
the Company’s CMS to do anything unlawful, misleading, malicious, or
discriminatory;<span style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></p>

<p class="MsoBodyText2"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(l) Use
any of the Company’s copyrights or trademarks, or the copyrights or trademarks
of any other user of the CMS;<o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">(m) Facilitate or encourage any
violations of these terms; <o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO">(n) Harm minors in any
way;<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO">(o) Impersonate any
person or entity or falsely state or otherwise misrepresent its affiliation
with any person or entity;<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO">(p) Transmit, access
or communicate any data that Talent Partner does not have a right to transmit
under any law or under contractual or fiduciary relationships (such as copyrights,
trademarks, inside information, proprietary and confidential information
learned or disclosed as part of employment relationships or under
non-disclosure agreements); and<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;;
mso-fareast-font-family:Calibri;mso-fareast-language:KO">(q) Act in a manner
that negatively affects other users' ability to engage in real-time exchanges.<o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(r) TALENT PARTNER FURTHER
AGREES NOT TO ATTEMPT TO ENTER PARTS OF THE STARCLUB WEBSITE THAT TALENT
PARTNER IS NOT AUTHORIZED TO ENTER, TAMPER WITH THE STARCLUB WEBSITE, ATTEMPT
TO ACCESS ANY DATA TALENT PARTNER IS NOT AUTHORIZED TO ACCESS, ATTEMPT TO
DISABLE OR IMPAIR THE STARCLUB WEBSITE, OR USE THE STARCLUB WEBSITE TO COMMIT ANY
CRIMINAL ACT OR INTENTIONAL INJURY OF ANY THIRD PARTY, INCLUDING BUT NOT
LIMITED TO THE COMMISSION OF COPYRIGHT AND/OR TRADEMARK INFRINGEMENT. <o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">(s) Any rights not expressly
granted hereunder are reserved by Company.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
mso-pagination:none;mso-layout-grid-align:none;text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">10.<span style="mso-spacerun:yes">&nbsp;
</span><u>Incorporation of Privacy Policy</u> --</span></b><span lang="EN-US" style="mso-bidi-font-size:12.0pt"> The Company’s Privacy Policy is incorporated
herein. Talent Partner grants Company the right to use Talent Partner’s
personal information in the manner described in the Privacy Policy.<o:p></o:p></span></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">11.<span style="mso-spacerun:yes">&nbsp;&nbsp; </span><u>Confidentiality</u> —</span></b><span lang="EN-US" style="mso-bidi-font-size:12.0pt">Talent Partner agrees that the CMS
and related software is the confidential information of Company and agrees not
to disclose such software to any third party.<span style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></p>

<p class="MsoBodyText2"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:12.0pt">12.<span style="mso-spacerun:yes">&nbsp;
</span><u>Acknowledgement</u> —</span></b><span lang="EN-US" style="mso-bidi-font-size:12.0pt">Talent Partner acknowledges that the Company’s
CMS is protected by US and international copyright laws and that its
functionality is patent-pending.<o:p></o:p></span></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">13.<span style="mso-spacerun:yes">&nbsp;
</span><u>Representation</u> —</span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">Talent Partner represents and warrants
that it has the right and power to enter into this Agreement without infringing
upon the rights and interests of any third party, and that all of the
registration information provided to Company is accurate in all material
respects.<o:p></o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US">14.<span style="mso-spacerun:yes">&nbsp; </span><u>Indemnification</u> --</span></b><span lang="EN-US"> Talent Partner warrants and represents that it shall be solely
responsible for Talent Partner’s use of the Company’s CMS regarding any and all
Content Talent Partner posts, and that Talent Partner is the owner or a valid
licensee of any of the intellectual property rights embodied in Content posted
by Talent Partner through Company’s CMS.<span style="mso-spacerun:yes">&nbsp;
</span><b style="mso-bidi-font-weight:normal"><u>Talent Partner hereby agrees
to indemnify and hold harmless the Company against any and all damages
(including but not limited to reasonable attorney’s fees) resulting or flowing from
in any way, the posting of Content by Talent Partner.</u></b></span><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

<p class="MsoBodyText2" style="margin-bottom:0cm;margin-bottom:.0001pt"><span lang="EN-US" style="mso-bidi-font-size:12.0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">15.<span style="mso-spacerun:yes">&nbsp;
</span><u>Right to Refuse</u> --</span></b><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;"> Company reserves the right and Talent Partner agrees that
Company may (but will not have an obligation to) remove or refuse to distribute
any Content, to suspend or terminate users, and to reclaim usernames. <span style="mso-spacerun:yes">&nbsp;</span>Company also reserves the right to
access, read, preserve, and disclose any information as Company reasonably believes
is necessary to (i) satisfy any applicable law, regulation, legal process or
governmental request, (ii) enforce the terms, including investigation of
potential violations hereof, (iii) detect, prevent, or otherwise address fraud,
security, copyright infringement, trademark infringement or technical issues,
(iv) respond to user support requests, or (v) protect the rights, property or
safety of Company, its users and the public.<u><o:p></o:p></u></span></p>

<p class="MsoNormal"><u><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p><span style="text-decoration:none">&nbsp;</span></o:p></span></u></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">16.<u><span style="mso-spacerun:yes">&nbsp; </span>Entire Agreement</u> -- <u><span style="mso-spacerun:yes">&nbsp;</span></u></span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">The terms of this Agreement constitute
the entire understanding between Talent Partner and Company regarding access
and use of the Company’s CMS and submission of Content. These terms do not
create any third party beneficiary rights, nor do they create a partnership or
any other unspecified business relationship between Company and Talent Partner.
<span style="mso-spacerun:yes">&nbsp;</span>Talent Partner may not transfer any
rights or obligations under the terms hereof to anyone else without Company’s
consent. <span style="mso-spacerun:yes">&nbsp;</span>Any purported assignment
in breach of this agreement is null and void.<b style="mso-bidi-font-weight:
normal"><u><o:p></o:p></u></b></span></p>

<p class="MsoNormal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">17.<span style="mso-spacerun:yes">&nbsp;
</span><u>Severability</u> --</span></b><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;"><span style="mso-spacerun:yes">&nbsp; </span>If any portion
of the terms hereof is found to be unenforceable, the remaining portion will
remain in full force and effect. <span style="mso-spacerun:yes">&nbsp;</span>If
Company fails to enforce any of the terms, it will not be considered a waiver.
All of Company’s rights and obligations under the terms hereof are freely
assignable by Company in connection with a merger, acquisition, or sale of
assets, or by operation of law or otherwise. <span style="mso-spacerun:yes">&nbsp;</span>All rights not expressly granted to Talent
Partner hereunder are reserved by Company. <o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">18.<span style="mso-spacerun:yes">&nbsp;
</span><u>Controlling Law and Jurisdiction</u> -- </span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">The terms hereof and any action related
thereto will be governed by the laws of the State of California without regard
to or application of its conflict of law provisions or Talent Partner’s state
or country of residence. All claims, legal proceedings or litigation arising in
connection with the terms hereof will be brought solely in the federal or state
courts located in Los Angeles County, California, United States, and Talent
Partner consents to the jurisdiction of and venue in such courts and waive any
objection as to inconvenient forum. <b style="mso-bidi-font-weight:normal"><o:p></o:p></b></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">&nbsp;</span></b><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">The parties do hereby agree to the
foregoing terms.<o:p></o:p></span></b></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">End.<o:p></o:p></span></b></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph"><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

</div>