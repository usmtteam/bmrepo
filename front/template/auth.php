<?php


  //Clean URLS only
  //Must have a content type before hitting the database/memcache
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

  if(!$_GET['p_sContentType']){
    if($_GET['p_sType']){
      if($_GET['video']){
        $l_sId = $_GET['video'];
        $l_sType = 'video';
      }elseif($_GET['bctid']){
        $l_sId = $_GET['bctid'];
        $l_sType = 'bcvideo';
      }elseif($_GET['photo']){
        $l_sId = $_GET['photo'];
        $l_sType = 'photo';
      }

      if($_GET['page_type']){
      $p_sPageType = $_GET['page_type'];
      $p_sPageTypeString = '?page_type='.$p_sPageType;
      }else{
      $p_sPageTypeString = '/';  
      }

      header('Location: /content/'.$l_sType.'/'.$l_sId.$p_sPageTypeString);
    }
    exit;
  }

    require(__DIR__.'/../config/init.php');

    session_start();
    include(__DIR__.'/_db.php');

header( 'Content-type: text/html; charset=utf-8' );

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?= $l_xChannel->site_title ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <? if($l_bIsVideo){ 
          if($p_sPageType!="twitter"){ ?>
            <meta property="og:title" content="<?= $l_xAssetResult['description']; ?>" />
            <meta property="og:description" content="<?= $l_xAssetResult['description']; ?>"/>
        <? } 

            if($p_sPageType=="inline"){ ?>
                <meta property="og:type" content="movie" />
                <meta property="og:video:height" content="270"/>
                <meta property="og:video:width" content="480"/>
                <meta property="og:url" content="http://<?php echo $l_xChannel->url; ?>.starsite.com/viewpost_auth.php?video=<?= $l_nAssetId ?>&page_type=inline" />
                <meta property="og:video" content="http://c.brightcove.com/services/viewer/federated_f9/?isVid=1&isUI=1&playerID=<?= $l_xChannel->bc_playerid_web ?>&autoStart=true&videoId=<?= $l_xAssetResult['brightcove_media_id']; ?>&linkBaseURL=<?= $l_sCurrentAbsoluteURLNonQuery ?>">
                <meta property="og:video:secure_url" content="https://secure.brightcove.com/services/viewer/federated_f9/?isVid=1&isUI=1&playerID=<?= $l_xChannel->bc_playerid_web ?>&autoStart=false&videoId=<?= $l_xAssetResult['brightcove_media_id']; ?>&secureConnections=true&linkBaseURL=<?= $l_sCurrentAbsoluteURLNonQuery ?>">
                <meta property="og:image" content="<?= $l_sImageUrl ?>"/>
                <meta property="og:video:type" content="application/x-shockwave-flash">
            <? } else if($p_sPageType == "landingpage"){ ?>
                <meta property="og:type" content="website" />
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>/" />
                <meta property="og:image" content="<?= $l_sImageUrl ?>" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:width" content="1200" />

                <!--- BRIGHTCOVE FB CODE END -->
                <meta content="" name="description" />
                <meta content="" name="author" />
        <? } else if($p_sPageType=="twitter"){?>

       
               <meta property="twitter:card" content="player">
               <meta property="twitter:title" content="<?= $l_xChannel->site_title ?>">
               <meta property="twitter:description" content="<?= $l_xAssetResult['description']; ?>">
               <meta property="twitter:image" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/watermark/<?= $l_xAssetResult['image_path']; ?>">
               <meta property="twitter:site" content="@starsitenetwork">
               <meta property="twitter:player:width" content="480">
               <meta property="twitter:player:height" content="270">
               <meta property="twitter:player" content="<?= $l_sCurrentAbsoluteSecureURL ?>?autoStart=false">
               <meta property="twitter:player:stream:content_type" content="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
               
      
        <?
        } 
    }else{  

        if($p_sPageType=="twitter"){ ?>

        <? //<meta name="twitter:card" content="photo" /> ?>
         <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_xAssetResult['description']; ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURL ?>?page_type=twitter" />
        
        <? } ?>

        <meta property="og:title" content="<?= $l_xAssetResult['description']; ?>" />
        <meta property="og:description" content="<?= $l_xAssetResult['description']; ?>"/>
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
        <meta property="og:image" content="<?= $l_sImageUrl ?>" />
        <meta property="og:image:type" content="image/png" />
        
        
    <? } ?>

    <? include(INCLUDE_HEAD); ?>
</head>
<body class="cbp-spmenu-push viewpost">
<?
flush();
ob_flush();


    switch($p_sPageType){
        case 'twitter': include(__DIR__.'/_twitter_content.php'); break;
        // case 'photo': $l_bIsVideo = false; include(__DIR__.'/_video.php'); break;
        // case 'post': $l_bIsVideo = false; include(__DIR__.'/_post.php'); break;
        default : 
          // $l_bIsVideo = true; 
          include(__DIR__.'/_content.php'); break;

    }
    

      include(INCLUDE_TRACKING); 
      include(INCLUDE_FOOTER); 
?>
<script src="scripts/vendor/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="scripts/vendor/jquery/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="scripts/vendor/bootstrap/bootstrap.min.js" type="text/javascript"></script> 

        <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.0.0/handlebars.min.js"></script>
        <!--[if lt IE 9]>
        <script src="scripts/vendor/respond/respond.min.js"></script>  
        <![endif]-->   
        <!-- END CORE PLUGINS -->   

        <script src="scripts/metro_app.js"></script>    
        <script type="text/javascript">
                                            jQuery(document).ready(function() {
                                                MetroApp.init();
                                            });
        </script>

        <script src="//cdn.jsdelivr.net/headroomjs/0.5.0/headroom.min.js"></script>
        <script src="//cdn.jsdelivr.net/headroomjs/0.5.0/angular.headroom.min.js"></script>
        <script src="//cdn.jsdelivr.net/headroomjs/0.5.0/jQuery.headroom.min.js"></script>
        <script src="scripts/vendor/angular.js/angular.min.js"></script>

        <script src="scripts/vendor/angular.js/angular-route.min.js"></script>
        <script src="scripts/vendor/angular.js/angular-cookies.min.js"></script>
        <script src="scripts/vendor/angular.js/angular-animate.min.js"></script>
        <script src="scripts/vendor/angular.js/angular-sanitize.min.js"></script>
        <script src="scripts/vendor/angular.js/angular-file-upload-shim.min.js"></script>
        <script src="scripts/vendor/angular.js/angular-file-upload.min.js"></script>
        <script src="scripts/vendor/angular.js/ng-infinite-scroll.min.js"></script>
  

        <script src="scripts/vendor/common/common.js"></script>
        <script src="scripts/vendor/common/ss_ad.js"></script>
         <script src="scripts/vendor/bootstrap/ui-bootstrap-tpls-0.10.0.min.js" type="text/javascript"></script> 
        <script src="scripts/app.js"></script>
        <script src="scripts/vendor/swichery/ng-switchery.js"></script>
        <script src="scripts/vendor/swichery/switchery.min.js"></script>

        <script src="scripts/vendor/angular.js/angularytics.js"></script>
        <!-- Directives -->
        <script src="scripts/directives/feedItemDirective.js"></script>
        <script src="scripts/directives/newPostDirective.js"></script>
        <script src="scripts/directives/brightCovePlayerDirective.js"></script>
        <script src="scripts/directives/attrOnOffDirective.js"></script>
        <!-- Controllers -->
        <script src="scripts/controllers/indexController.js"></script>
        <script src="scripts/controllers/loginController1.js"></script>

    </body>
</html>