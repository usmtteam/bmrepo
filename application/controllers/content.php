<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('json_loader');
        $this->load->helper('bitly');
        $this->load->library('BrightCove');
        $this->load->library('Utils');
        $this->load->library('email');

        include(LIB_PATH.'google-api-php-client/src/Google/autoload.php');
         $this->load->library('Gastar');

        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }

        $this->load->helper('language');
        $this->load->helper('login');
        $this->load->model('main_m');

        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->chanel = $this->session->userdata('chanel');

        $this->data['chanel'] = $this->chanel;
        $this->data['chanel_info'] = is_array($this->chanel) ? $this->main_m->get_chanel($this->chanel['id']) : $this->main_m->get_chanel($this->chanel);
        $this->data['feeds'] = $this->main_m->getHomeFeedList();
        $this->data['sliderfeeds'] = $this->main_m->filterofloslider();

        $this->data['show_errors'] = array();
        $this->data['selfuser'] = $this->ion_auth->user($user_id)->row();

        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
        if($user->email!="admin@admin.com"){
//            $id =  $this->set_slug_dependent_constants($user->email);
            $id =  $this->set_slug_dependent_constants($this->chanel['id']);
        }else{ // this set does not work with admin login
//            $id = $this->uri->segment(3, 0);
//            echo "<BR>\$id :".$id;
//            $SQL = 'SELECT email, id FROM users WHERE cid = "'.$id.'"';
//            $query = $this->db->query($SQL);
//            $rows = $query->result();
//            $email = $rows[0]->email;
//            $useid = $rows[0]->id;
//
//            $id =  $this->set_slug_dependent_constants($email);
//            $user_id = $useid;
//            $user = $this->ion_auth_model->user($user_id)->row();

            $id =  $this->set_slug_dependent_constants($this->chanel['id']);
      }
        $this->data['is_oflo'] =  $this->main_m->isChanelOflo();
//         var_dump($this->data['is_oflo']);exit;
     
    }
    
    function _remap($method) {

        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){

        redirect('content/feed');
    }

//    public function set_slug_dependent_constants($email){
    public function set_slug_dependent_constants($cid){

//      $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
//      $query = $this->db->query($SQL);
//      $rows = $query->result();
//      $cid = $rows[0]->cid;

      $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $slug = $rows[0]->slug;

      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
      define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('UPLOAD_VIRAL_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/viral/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return;

      }
     function getThumbNail($image_path, $width=100, $height=100, $default_url=''){
    
        if($image_path== ""){
            return "";
        }
        if($height == 50){
            $height = 49;
        }
        if(!in_array(strtolower(substr(strrchr($image_path,'.'),1)),array('gif','jpg','jpeg','png', 'bmp'))) {
            return UPLOAD_CLOUD.'small_thumb/'.$image_path;
        }
        $path = UPLOAD_CLOUD.'small_thumb/'.$image_path;
        
        return $path;
    }

    function getViralThumbNail($image_path, $width=100, $height=100, $default_url=''){
      
        if($image_path==""){
            return "";
        }
        if($height == 50){
            $height = 49;
        }
        if(!in_array(strtolower(substr(strrchr($image_path,'.'),1)),array('gif','jpg','jpeg','png', 'bmp'))) {
            return 'http://d3hl6zmfu9utfu.cloudfront.net/viral/assets/ufile/small_thumb/'.''.$image_path;
        }
        $path = 'http://d3hl6zmfu9utfu.cloudfront.net/viral/assets/ufile/small_thumb/'.''.$image_path;
        
        return $path;
    }




    function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        } 
    }
  
   function getCroppedThumbNail($image_path, $thumb_width=100, $thumb_height=100, $default_url=''){
       
    
    
    $image = imagecreatefromjpeg(UPLOAD_CLOUD.$image_path);
    $filename = UPLOAD_CLOUD.$image_path;
    
    $thumb_width = 150;
    $thumb_height = 150;
    
    $width = imagesx($image);
    $height = imagesy($image);
    
    $original_aspect = $width / $height;
    $thumb_aspect = $thumb_width / $thumb_height;
    
    if ( $original_aspect >= $thumb_aspect )
    {
       // If image is wider than thumbnail (in aspect ratio sense)
       $new_height = $thumb_height;
       $new_width = $width / ($height / $thumb_height);
    }
    else
    {
       // If the thumbnail is wider than the image
       $new_width = $thumb_width;
       $new_height = $height / ($width / $thumb_width);
    }
    
    $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
    
    // Resize and crop
    imagecopyresampled($thumb,
               $image,
               0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
               0 - ($new_height - $thumb_height) / 2, // Center the image vertically
               0, 0,
               $new_width, $new_height,
               $width, $height);
    imagejpeg($thumb, $filename, 100);
    

 

    return $filename;
    
    
    }
    
    public function feed()
    {
        $feed = $this->uri->segment(3,false);

        $this->data['social'] = $this->main_m->getSocial($this->data['chanel_info']['id']);
        $accountId = GA_ACCOUNT_ID;
        $fullurl = "http://".$this->chanel['url'].".on.starsite.com";
   
         $ganame = $this->chanel['name'].' Channel';

         $gastar = new Gastar();
          $analytics = $gastar->getService();
        /*
         //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
         $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
         if($propertyID!=null){
            $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
         } */
         $profileID = $this->chanel['google_analytics_internal_id'];

        if($feed && method_exists($this, "feed_{$feed}")) {
          $feed_id = $this->uri->segment(4);
          
          if(!empty($feed_id)) {
            $post_type = 'text';
            if($feed == 'video_edit') $post_type = 'video';
            else if($feed == 'videoEmbed_edit') $post_type = 'videoEmbed';
            else if($feed == 'photo_edit') $post_type = 'photo';
            else if($feed == 'poll_edit') $post_type = 'poll';
            else if($feed == 'quiz_edit') $post_type = 'quiz';

            $id = ltrim($feed_id, $post_type . "_");
             
            $this->data['comments'] = $this->main_m->getComments($post_type, $id, UPLOAD_URL);
          }
//            echo "<BR><BR><BR>method exists feed_{$feed}";
//            exit;

            call_user_func_array(array($this, "feed_{$feed}"), array());
        }
        else{
          $feed_id = $this->uri->segment(4);

//            echo "<BR><BR><BR>";

            foreach($this->data['feeds'] as &$feed){
     
              $feed['content_type'] = str_replace(' post', '', strtolower($feed['post_type']));
              $feed['content_id'] = trim($feed['id'], $feed['content_type'] . "_");

              // Check start date has not passed
//                echo "<BR>\$feed_start_date: ";
              $feed_start_date = date_create($feed['start_date']);
//                var_dump($feed_start_date);

//                echo "<BR>\$time_now: ";
              $time_now = date_create(date('Y-n-j H:i:s'));
//                var_dump($time_now);

              $diff = $feed_start_date->getTimestamp() - $time_now->getTimestamp();
//                echo "<BR>\$diff: ";
//                var_dump($diff);

              if ($feed['is_publish'] == 1 && $diff > 0){
//                  echo "<BR><BR><BR>Set to Draft";
                $feed['is_publish'] = 2;
              } else {
//                  echo "<BR><BR><BR>Same";
                $feed['is_publish'] = $feed['is_publish'];
              }

              $feed['time_stamp'] = $feed['time_stamp'];
              $feed['start_date'] = $feed['start_date'];
              $feed['image_path'] = $this->getThumbNail($feed['image_path'], 50, 50);
              $feed['slider_thumb'] = $feed['slider_thumb'];
              // Get video row to check if YouTube
              $feed['video_row'] = $this->main_m->getFeedVideo($feed['content_id']);

              $id = $feed['id'];

//              echo "feed_id_{$feed_id}";
//              exit;

              if($profileID!=null){
                $str = explode('_', $id);
                  $id = $str[1];
                $videoId = "$id";

                $i = 0;
                $tableId = "ga:$profileID";

                $start_date = $this->input->post('start_date_report');

                $end_date = $this->input->post('end_date_report');

                if($start_date==false){
                  $start_date = $feed['start_date'];
                  $str = explode(' ', $start_date);
                  $start_date = $str[0];
                }
                if($end_date==false){
                  $end_date = date('Y-m-d');
                }

                $this->data['post']['start_date_report'] = $start_date;
                $this->data['post']['end_date_report'] = $end_date;
                /*
                $fields = "Video-View";

                $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                $total = Gastar::calcTotals($data);
                */

                $feed_video_views = 0;
                $feed_page_views_ga = 0;
                $feed_revenue = 0;

                if (stripos($feed['content_type'], "photo") !== FALSE){
                    $content_type = "photo";
                } else {
                    $content_type = "video";
                }

//                $SQL = 'SELECT * FROM sc_reports WHERE content_id = "'.$videoId.'"';
                $SQL = 'SELECT * FROM oflo_reports_monthly
                        WHERE content_id = "'.$videoId.'"
                        AND content_type = "'.$content_type.'"';

                $query = $this->db->query($SQL);
                $rows = $query->result();

                foreach($rows as $value){
                    if ($value->is_mobile == 1){
                        $feed_video_views += $value->video_views;
                    } else {
                        $feed_page_views_ga += $value->page_views_ga;
                    }
                  $feed_revenue += $value->revenue;

              }

                if($feed_video_views){
                    $feed['video_views'] = $feed_video_views;
                }else{
                    $feed['video_views'] = "N/A";
                }
                if($feed_page_views_ga){
                    $feed['page_views_ga'] = $feed_page_views_ga;
                }else{
                    $feed['page_views_ga'] = "N/A";
                }

                $feed['total_views'] = $feed_video_views + $feed_page_views_ga;

                if($feed_revenue){
                    $feed['revenue'] = $feed_revenue;
                }else{
                    $feed['revenue'] = "N/A";
                }
              }

           }

          if($profileID!=null){
              /*
              $fields = "Video-View";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_view = Gastar::calcChannelTotals($data);

              $fields = "Video-View-Half";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_view_half = Gastar::calcChannelTotals($data);

              $fields = "Video-View-Three-Fourths";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_view_three_fourths = Gastar::calcChannelTotals($data);

              $fields = "Video-Impression";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_impression = Gastar::calcChannelTotals($data);

              $fields = "Video-Played";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_played = Gastar::calcChannelTotals($data);

              $fields = "Video-Finished";

              $data = Gastar::queryCoreChannelReportingApi($tableId, $analytics,$start_date,$end_date,$fields);

              $video_finished = Gastar::calcChannelTotals($data);

              $video_impression += $video_played;

              $this->data['post']['total_views'] = $video_view;
              $post['total_views'] = $video_view;
              $engagement1 = ($video_view/$video_impression)*100;
              if($video_view_half>0){
              $engagement2 = ($video_view_half/$video_impression)*100;
              }
              if($video_view_three_fourths>0){
              $engagement3 = ($video_view_three_fourths/$video_impression)*100;
              }
              $engagement4 = ($video_finished/$video_impression)*100;
              if($video_view_half>0 && $video_view_three_fourths>0){
              $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
              }else{
              $engagement = ($engagement1+$engagement4)/2;
              } */

              $cid = $this->chanel['id'];
//              $SQL = 'SELECT * FROM sc_reports WHERE channel_id = "'.$cid.'"';
              $SQL = 'SELECT * FROM oflo_reports_monthly WHERE channel_id = "'.$cid.'"';
                $query = $this->db->query($SQL);
                $rows = $query->result();

              $page_view = 0; $video_view=0;$engagement=0;$revenue = 0;$count=0;

              foreach($rows as $value){
                  if ($value->is_mobile == 1){
                      $video_view+=$value->video_views;
                  } else {
                      $page_view+=$value->page_views_ga;
                  }
                $engagement+=$value->engagement;
                $revenue+=$value->revenue;
                $count++;
              }

//              echo "<BR>\$page_view: ".$page_view;
//              echo "<BR>\$video_view: ".$video_view;
//              exit;

              $engagement = $engagement/$count;
              $this->data['post']['total_views'] = $page_view + $video_view;
              $this->data['post']['engagement'] = number_format($engagement, 2, '.', '');

              $tableId = "ga:$profileID"; // Google Analytics Table ID / Profile ID

              $geoData = Gastar::getGeoLocations($tableId, $analytics); // Results in Analytics service object

              // Analytics geo location results
              $this->data['geos'] = $geoData->getRows();
          }

          $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
          $this->data['post']['total_reach'] = $this->chanel['total_reach'];
          $this->data['post']['total_revenue'] = $revenue;

          $this->load->view('homefeed', $this->data);
       }  
        
    }
  
  public function text()
  {
    foreach($this->data['texts'] as &$text){
    $text = $this->db->get_where('enr_text', array('cid'=>$feed['content_type']))->row_array();
    
    $text['description'] = $text['description'];
    }
    $this->load->view('homefeed', $text);
   }
  
  public function postfeed()
    {
        $feed = $this->uri->segment(3,false);
        
        $this->data['social'] = $this->main_m->getSocial($this->data['chanel_info']['id']);

        $accountId = GA_ACCOUNT_ID;
        $fullurl = "http://".$this->chanel['url'].".on.starsite.com";
   
         $ganame = $this->chanel['name'].' Channel';


         $gastar = new Gastar();
          $analytics = $gastar->getService();

         //$adsense = Gasensestar::getService();

         //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
        /* $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics); 
         if($propertyID!=null){
         $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics); 
         } */

         $profileID = $this->chanel['google_analytics_internal_id'];

        if($feed && method_exists($this, "feed_{$feed}")) {
          $feed_id = $this->uri->segment(4);
          
          if(!empty($feed_id)) {
            $post_type = 'text';
            if($feed == 'video_edit') $post_type = 'video';
            else if($feed == 'videoEmbed_edit') $post_type = 'videoEmbed';
            else if($feed == 'photo_edit') $post_type = 'photo';
            else if($feed == 'poll_edit') $post_type = 'poll';
            else if($feed == 'quiz_edit') $post_type = 'quiz';
             
          
            $id = ltrim($feed_id, $post_type . "_");
             
            $this->data['comments'] = $this->main_m->getComments($post_type, $id, UPLOAD_URL);
          }
          
            call_user_func_array(array($this, "feed_{$feed}"), array());

            
        }
        else{
//            echo "<BR><BR><BR>";

            foreach($this->data['feeds'] as &$feed){
              $feed['content_type'] = str_replace(' post', '', strtolower($feed['post_type']));
              $feed['content_id'] = trim($feed['id'], $feed['content_type'] . "_");
              $feed['image_path'] = $this->getThumbNail($feed['image_path'], 150, 150);

              // Check start date has not passed
//                echo "<BR>\$feed_start_date: ";
                $feed_start_date = date_create($feed['start_date']);
//                var_dump($feed_start_date);

//                echo "<BR>\$time_now: ";
                $time_now = date_create(date('Y-n-j H:i:s'));
//                var_dump($time_now);

                $diff = $feed_start_date->getTimestamp() - $time_now->getTimestamp();
//                echo "<BR>\$diff: ";
//                var_dump($diff);

              if ($feed['is_publish'] == 1 && $diff > 0){
//                  echo "<BR><BR><BR>Set to Draft";
                  $feed['is_publish'] = 2;
              } else {
//                  echo "<BR><BR><BR>Same";
                  $feed['is_publish'] = $feed['is_publish'];
              }

              // Get video row to check if YouTube
              $feed['video_row'] = $this->main_m->getFeedVideo($feed['content_id']);

              $no_photo = explode('/', $feed['image_path']);
        
              if($no_photo[3]==""){
                $feed['image_path'] = NULL;
              }
   
              $id = $feed['id'];
              if($profileID!=null){
                $str = explode('_', $id);
                  $id = $str[1];
                $videoId = "$id";

                $i = 0;
                $tableId = "ga:$profileID";

                $start_date = $this->input->post('start_date_report');

                $end_date = $this->input->post('end_date_report');

                if($start_date==false){
                  $start_date = $feed['start_date'];
                  $str = explode(' ', $start_date);
                  $start_date = $str[0];
                }
                if($end_date==false){
                  $end_date = date('Y-m-d');
                }
                
                $this->data['post']['start_date_report'] = $start_date;
                $this->data['post']['end_date_report'] = $end_date;
                 /*
                $fields = "Video-View";

                $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);
                
                $total = Gastar::calcTotals($data); */
                $feed_video_views = 0;
                $feed_page_views_ga = 0;
                $feed_revenue = 0;


                if (stripos($feed['content_type'], "photo") !== FALSE){
                    $content_type = "photo";
                } else {
                    $content_type = "video";
                }

//                $SQL = 'SELECT * FROM sc_reports WHERE content_id = "'.$videoId.'"';
                $SQL = 'SELECT * FROM oflo_reports_monthly
                        WHERE content_id = "'.$videoId.'"
                        AND content_type = "'.$content_type.'"';

                $query = $this->db->query($SQL);
                $rows = $query->result();

                foreach($rows as $value){

                    if ($value->is_mobile == 1){
                        $feed_video_views += $value->video_views;
                    } else {
                        $feed_page_views_ga += $value->page_views_ga;
                    }
                  $feed_revenue += $value->revenue;
                  
                }

                  if($feed_video_views){
                    $feed['video_views'] = $feed_video_views;
                  }else{
                    $feed['video_views'] = "N/A";
                  }
                  if($feed_page_views_ga){
                    $feed['page_views_ga'] = $feed_page_views_ga;
                  }else{
                    $feed['page_views_ga'] = "N/A";
                  }

                  $feed['total_views'] = $feed_video_views + $feed_page_views_ga;

                  if($feed_revenue){
                    $feed['revenue'] = $feed_revenue;
                  }else{
                    $feed['revenue'] = "N/A";
                  }
              }
            }
         
            $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
             $this->data['post']['total_reach'] = $this->chanel['total_reach'];



            $this->load->view('postfeed', $this->data);
        }
    }


/**
*OFLO SLIDER
**/

public function ofloslider(){
        $feed = $this->uri->segment(3,false);
        
        $this->data['social'] = $this->main_m->getSocial($this->data['chanel_info']['id']);

        $accountId = GA_ACCOUNT_ID;
        $fullurl = "http://".$this->chanel['url'].".on.starsite.com";
   
         $ganame = $this->chanel['name'].' Channel';


         $gastar = new Gastar();
          $analytics = $gastar->getService();

         //$adsense = Gasensestar::getService();

         //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
        /* $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics); 
         if($propertyID!=null){
         $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics); 
         } */

         $profileID = $this->chanel['google_analytics_internal_id'];

        if($feed && method_exists($this, "feed_{$feed}")) {
          $feed_id = $this->uri->segment(4);
          
          if(!empty($feed_id)) {
            $post_type = 'text';
            if($feed == 'video_edit') $post_type = 'video';
            else if($feed == 'videoEmbed_edit') $post_type = 'videoEmbed';
            else if($feed == 'photo_edit') $post_type = 'photo';
            else if($feed == 'poll_edit') $post_type = 'poll';
            else if($feed == 'quiz_edit') $post_type = 'quiz';
             
          
            $id = ltrim($feed_id, $post_type . "_");
             
            $this->data['comments'] = $this->main_m->getComments($post_type, $id, UPLOAD_URL);
          }
          
            call_user_func_array(array($this, "feed_{$feed}"), array());

            
        }
        else{
            foreach($this->data['sliderfeeds'] as &$feed){
              $feed['content_type'] = str_replace(' post', '', strtolower($feed['post_type']));
              $feed['content_id'] = trim($feed['id'], $feed['content_type'] . "_");
              $feed['image_path'] = $this->getThumbNail($feed['image_path'], 150, 150);
            
              $feed['date'] = $feed['start_date'];
        
              $no_photo = explode('/', $feed['image_path']);
        
              if($no_photo[3]==""){
                $feed['image_path'] = NULL;
              }
   
              $id = $feed['id'];
              if($profileID!=null){
                $str = explode('_', $id);
                  $id = $str[1];
                $videoId = "$id";

                $i = 0;
                $tableId = "ga:$profileID";

                $start_date = $this->input->post('start_date_report');

                $end_date = $this->input->post('end_date_report');

                if($start_date==false){
                  $start_date = $feed['start_date'];
                  $str = explode(' ', $start_date);
                  $start_date = $str[0];
                }
                if($end_date==false){
                  $end_date = date('Y-m-d');
                }
                
                $this->data['post']['start_date_report'] = $start_date;
                $this->data['post']['end_date_report'] = $end_date;
                 /*
                $fields = "Video-View";

                $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);
                
                $total = Gastar::calcTotals($data); */
                $feed_video_views = 0;
                $feed_page_views_ga = 0;
                $feed_revenue = 0;

                $SQL = 'SELECT * FROM sc_reports WHERE content_id = "'.$videoId.'"';
                $query = $this->db->query($SQL);
                $rows = $query->result();
                foreach($rows as $value){

                  $feed_video_views += $value->video_views;
                  $feed_page_views_ga += $value->page_views_ga;
                  $feed_revenue += $value->revenue;
                  
                }

                  if($feed_video_views){
                    $feed['video_views'] = $feed_video_views;
                  }else{
                    $feed['video_views'] = "N/A";
                  }
                  if($feed_page_views_ga){
                    $feed['page_views_ga'] = $feed_page_views_ga;
                  }else{
                    $feed['page_views_ga'] = "N/A";
                  }
                  if($feed_revenue){
                    $feed['revenue'] = $feed_revenue;
                  }else{
                    $feed['revenue'] = "N/A";
                  }
              }
            }
         
            $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
             $this->data['post']['total_reach'] = $this->chanel['total_reach'];





            $this->load->view('ofloslider', $this->data);
        }
    }


public function ofloslider_del($id, $post_type, $is_oflo_slider){
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];
     

         if($this->input->post('is_oflo_slider')=='on'){
                $is_oflo_slider = y;

            }else{
                $is_oflo_slider = n;
            }


           if($this->main_m->update_is_oflo_slider($feed_id, $post_type,$is_oflo_slider))

            {
                redirect("content/ofloslider", '');
            }
        
        return $data;
      
}



    /**
     * Curate
     */
    public function curate(){
        $feed = $this->uri->segment(3,false);

        // GET queries
        $selected_category = $this->input->get('category', TRUE);
        $search = $this->input->get('search', TRUE);
        $page = $this->input->get('page', TRUE);
        $items_per_page = $this->input->get('items_per_page', TRUE);

        // Remember GET queries
        $this->data['selected_category'] = $selected_category;
        $this->data['search'] = $search;

        // Set default values
        if (empty($page)) { $page = 0; }
        if (empty($items_per_page)) { $items_per_page = 20; }

        $this->data['viral_media_categories'] = $this->main_m->get_viral_media_category_all(); // Get viral media categories

        $this->data['chanel_info'] = $this->main_m->get_chanel('64');
        $this->data['chanel'] = $this->main_m->get_chanel('64');

        $this->data['feeds'] = $this->main_m->getViralPosts($selected_category, $search);
        $total_count = count($this->data['feeds']);
        $this->data['social'] = $this->main_m->getSocial($this->data['chanel_info']['id']);
        if($feed && method_exists($this, "feed_{$feed}")) {
         
          if(!empty($feed_id)) {
            $post_type = 'text';
            if($feed == 'video_edit') $post_type = 'video';
            else if($feed == 'photo_edit') $post_type = 'photo';
            else if($feed == 'poll_edit') $post_type = 'poll';
            else if($feed == 'quiz_edit') $post_type = 'quiz';
             
          
            $id = ltrim($feed_id, $post_type . "_");
             
            $this->data['comments'] = $this->main_m->getComments($post_type, $id, UPLOAD_URL);
          }     
            call_user_func_array(array($this, "feed_{$feed}"), array());
        }
        else{
        
            foreach($this->data['feeds'] as &$feed){
              $feed['content_type'] = str_replace(' post', '', strtolower($feed['post_type']));
              $feed['content_id'] = trim($feed['id'], $feed['content_type'] . "_");
              $feed['destination'] = str_replace(array(".mov", ".mp4", ".3gp"),"", UPLOAD_VIRAL_CLOUD.$feed['destination']);
              $feed['destination'] = $feed['destination']."-480.mp4";
              $feed['image_path'] = $this->getViralThumbNail($feed['image_path'], 150, 150);      
        

              $no_photo = explode('/', $feed['image_path']);
              
              if($no_photo[3]==""){
                $feed['image_path'] = NULL;
              }
            }

            // Pagination
            $this->data['items_per_page_array'] = array(5, 10, 25, 50, 100);
            if (!in_array($items_per_page, $this->data['items_per_page_array'])){ $items_per_page = 10; }

            $this->data['page'] = $page;
            $this->data['page_count'] = $items_per_page;
            $this->data['total_page_count'] = ceil($total_count / $this->data['page_count']);

           // Total Feed Count
            $total_count = count($this->data['feeds']);
            $this->data['total_count'] = $total_count;

            // Calculate start page to show
            if (($page - 2) < 0) { $start_page = 0; }
            else { $start_page = $page - 2; }
            $this->data['start_page'] = $start_page;

            // Calculate last page to show
            if ($page <= 2){

                $end_page = $this->data['total_page_count'];

            } else {

                if (($page + 3) > $this->data['total_page_count']) { $end_page = $this->data['total_page_count']; }
                else { $end_page = $page + 3; }

            }
            $this->data['end_page'] = $end_page;

            // Clean up base_url
            $this->data['base_url'] = preg_replace('/&page=\d+/u', '', $_SERVER['REQUEST_URI']);

            // Add GET
            if (strpos($this->data['base_url'], "?") === FALSE){
                $this->data['base_url'] .= "?";
            }

            $this->load->view('curate', $this->data);
        }
    }
    

    public function feed_copy() {
    
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];
        $user_id = $this->ion_auth->get_user_id();
        $user = $this->main_m->getUser($user_id); // Get User

        //            if(empty($id)){
        if(empty($feed_id)){
            show_error("Select a post to edit!");
            return;
        }

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);

        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = 0;
        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');
        
        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
         }

        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];
        //
        switch($post_type) {
        case 'text': {
            $feed = $this->main_m->getFeedText($feed_id);

            $feed['copy_origin_id'] = $feed['id'];
            $feed['copy_timestamp'] = time();

            $id = $this->main_m->addFeedText($feed);
            redirect(site_url('content/feed/text_edit/text_'.$id));
            break;
        }
        case 'photo': {
            $feed = $this->main_m->getFeedPhoto($feed_id);

            if($this->input->post('submit', false) === false){
//                $FeedPhoto = $this->main_m->getFeedPhoto($id);
                $this->data['post'] = $feed;
                $this->data['post']['feed_id'] = $feed_id;
                $this->data['post']['cid'] = $this->chanel['id'];
            }else{

                //prepare copied version
                $feed['description'] = $this->input->post('description');
                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
//                $feed['is_oflo']        = $is_oflo;
//                $feed['is_oflo_slider'] = $is_oflo_slider;
//                $feed['slider_thumb']   = $feed['slider_thumb'];
//                $feed['is_premium']     = $is_premium;

                unset($feed['id']);

                //insert copied version
                $res = $this->main_m->db->insert("enr_photo", $feed);
                $id = $this->main_m->db->insert_id();
                $this->main_m->insertFeed('photo', $id, $feed['fan_id'], $feed['is_publish']);
                $this->main_m->updateLastPost($feed['fan_id']);

                // Photo Edit
                $i=0;
                while($viral_media_category[$i]){

                    $cat_id = $viral_media_category[$i]['id'];

                    $cat_update = $this->input->post('submit');

                    if($cat_update){
                        if($this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_PHOTO);
                        }elseif (!$this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_delete($cat_id, $id);
                        }

                    }
                    $i++;

                }

                $this->data['post'] = array(
                    'feed_id' => $id,
                    'cid' => $this->chanel['id'],
                    'description' => $this->input->post('description'),
                    'tags' => $this->input->post('tags'),
                    'credit' => $this->input->post('credit'),
                    'url_link' => $this->input->post('url_link'),
                    'slider_thumb'=>$this->input->post('slider_thumb'),
                    'is_publish' => 1,
                    "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
                    "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                    'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                    'is_sticky' => $this->input->post('is_sticky'),
                    'post_to_facebook' => $this->input->post('post_to_facebook'),
                    'post_to_twitter' => $this->input->post('post_to_twitter'),
                    'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                    'is_oflo' => $is_oflo,
                    'is_oflo_slider' => $is_oflo_slider,
                    'is_premium' => $is_premium,
                );

                $accountId = GA_ACCOUNT_ID;

                $fullurl = "http://".$this->chanel['url'].".on.starsite.com";

                $ganame = $this->chanel['name'].' Channel';

                /*
                $analytics = Gastar::getService();

                //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
                $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
                if($propertyID!=null){
                 $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
                }
                */

                $profileID = $this->chanel['google_analytics_internal_id'];

                $photoId = "$id";

                if($profileID!=null){

                    $tableId = "ga:$profileID";

                    $start_date = $this->input->post('start_date_report');

                    $end_date = $this->input->post('end_date_report');




                    if($start_date==false){
//                        $start_date = $FeedVideo['start_date'];
                        $start_date = $feed['start_date'];
                        $str = explode(' ', $start_date);
                        $start_date = $str[0];
                    }
                    if($end_date==false){
                        $end_date = date('Y-m-d');
                    }


                    $this->data['post']['start_date_report'] = $start_date;
                    $this->data['post']['end_date_report'] = $end_date;
                    /*
                    $fields = "Video-View";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view = Gastar::calcTotals($data);

                    $fields = "Video-Impression";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_impression = Gastar::calcTotals($data);

                    $fields = "Video-Played";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_played = Gastar::calcTotals($data);

                    $fields = "Video-Finished";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_finished = Gastar::calcTotals($data);

                    $fields = "Video-View-Half";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view_half = Gastar::calcChannelTotals($data);

                    $fields = "Video-View-Three-Fourths";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view_three_fourths = Gastar::calcChannelTotals($data);

                    $video_played += $video_impression;

                    $this->data['feed']['video_played'] = $video_played;
                    $this->data['feed']['video_views'] = $video_view;
                    $this->data['feed']['video_finished'] = $video_finished;
                    $engagement1 = ($video_view/$video_played)*100;

                    if($video_view_half>0){
                    $engagement2 = ($video_view_half/$video_played)*100;
                    }
                    if($video_view_three_fourths>0){
                    $engagement3 = ($video_view_three_fourths/$video_played)*100;

                    }
                    $engagement4 = ($video_finished/$video_played)*100;
                    if($video_view_half>0 && $video_view_three_fourths>0){
                    $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
                    }else{
                    $engagement = ($engagement1+$engagement4)/2;
                    }*/

                    $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$photoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
                    $query = $this->db->query($SQL);
                    $rows = $query->result();

                    foreach($rows as $value){
                        $feed_video_views += $value->video_views;
                        $feed_page_views_ga += $value->page_views_ga;
                        $feed_revenue += $value->revenue;
                        $feed_impressions += $value->video_impressions;
                        $feed_engagement += $value->engagement;
                        $count++;
                    }

                    if($feed_video_views){
                        $this->data['feed']['video_views'] = $feed_video_views;
                    }else{
                        $this->data['feed']['video_views'] = "N/A";
                    }
                    if($feed_page_views_ga){
                        $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
                    }else{
                        $this->data['feed']['page_views_ga'] = "N/A";
                    }
                    if($feed_revenue){
                        $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
                    }else{
                        $this->data['feed']['revenue'] = "N/A";
                    }
                    if($feed_engagement){
                        $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
                    }else{
                        $this->data['feed']['engagement'] = "N/A";
                    }
                    if($feed_impressions){
                        $this->data['feed']['video_played'] = $feed_impressions;
                    }else{
                        $this->data['feed']['video_played'] = "N/A";
                    }
                    //$this->data['feed']['video_views'] = $rows->video_views;

                }

                $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
                $this->data['post']['total_reach'] = $this->chanel['total_reach'];

                $this->_proc_feed_photo_edit();

                $this->main_m->break_oflo_photo_memcache($feed);
            }

//            redirect(site_url('content/feed/photo_edit/photo_'.$id));
            $this->load->view('feed_photo_edit', $this->data);

            break;
        }
        case 'video': {

            $feed = $this->main_m->getFeedVideo($feed_id);
//
            if($this->input->post('submit', false) === false){

                $feed['video_size'] = $feed['video_size'];
                $feed['format_size'] = $this->_makeSizeFormat($feed['video_size']);
                $this->data['post'] = $feed;
                $this->data['post']['feed_id'] = $feed_id;
                $this->data['post']['cid'] = $this->chanel['id'];

                $this->data['publishable'] = true;
                if($this->data['post']['brightcove_media_id'] != 'Null' && $this->data['post']['brightcove_media_id'] != 'None' && $this->data['post']['brightcove_media_id'] != '' && $this->brightcove->getVideoStatus($this->data['post']['brightcove_media_id']) != 'ACTIVE') {
                    $this->data['publishable'] = false;
                }
            }
            else{
                $l_bCLOSE_BUFFER = 1;

                if (($feed['id'] != "0" || $feed['id'] != "") && $l_bCLOSE_BUFFER){
                    Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

                    window.location.href='/content/postfeed/';
                    </SCRIPT>");
                }
//

                // DB insert
                $feed['description'] = $this->input->post('description');
                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
                $feed['was_curated'] = 1;
//                $feed['is_oflo']        = $is_oflo;
//                $feed['is_oflo_slider'] = $is_oflo_slider;
//                $feed['is_premium']     = $is_premium;

                unset($feed['id']);

                $res = $this->main_m->db->insert("enr_video", $feed);
                $id = $this->main_m->db->insert_id();

                // Video Edit
                $i=0;
                while($viral_media_category[$i]){

                    $cat_id = $viral_media_category[$i]['id'];

                    $cat_update = $this->input->post('submit');

                    if($cat_update){
                        if($this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                        }elseif (!$this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_delete($cat_id, $id);
                        }

                    }
                    $i++;

                }
                //
                $this->main_m->insertFeed('video', $id, $feed['fan_id'], $feed['is_publish']);

                $this->main_m->updateLastPost($feed['fan_id']);
                //

                $this->data['post'] = array(
                    'feed_id' => $id,
                    'cid' => $this->chanel['id'],
                    'description' => $this->input->post('description'),
                    'tags' => $this->input->post('tags'),
                    'credit' => $this->input->post('credit', ''),
                    'video_url' => $this->input->post('video_url', ''),
                    'is_publish' => 1,
                    'slider_thumb' => $this->input->post('slider_thumb'),
                    "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
                    "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                    'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                    'is_sticky' => $this->input->post('is_sticky'),
                    'custom_image' => $this->input->post('video_img_value'),
                    'post_to_facebook' => $this->input->post('post_to_facebook'),
                    'post_to_twitter' => $this->input->post('post_to_twitter'),
                    'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                    "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
                    "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
                    'is_oflo' => $is_oflo,
                    'is_oflo_slider' => $is_oflo_slider,
                    'is_premium' => $is_premium,
                );

//                $this->data['post']['destination'] = $FeedVideo['destination'];
//                $this->data['post']['image_path'] = $FeedVideo['image_path'];
//                $this->data['post']['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']) ;
                $this->data['post']['destination'] = $feed['destination'];
                $this->data['post']['image_path'] = $feed['image_path'];
                

                $this->data['post']['format_size'] = $this->_makeSizeFormat($feed['video_size']) ;
                $accountId = GA_ACCOUNT_ID;

                $fullurl = "http://".$this->chanel['url'].".on.starsite.com";

                $ganame = $this->chanel['name'].' Channel';

                /*
                $analytics = Gastar::getService();

                //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
                $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
                if($propertyID!=null){
                 $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
                }
                */

                $profileID = $this->chanel['google_analytics_internal_id'];

                $videoId = "$id";

                if($profileID!=null){

                    $tableId = "ga:$profileID";

                    $start_date = $this->input->post('start_date_report');

                    $end_date = $this->input->post('end_date_report');


                    if($start_date==false){
//                        $start_date = $FeedVideo['start_date'];
                        $start_date = $feed['start_date'];
                        $str = explode(' ', $start_date);
                        $start_date = $str[0];
                    }
                    if($end_date==false){
                        $end_date = date('Y-m-d');
                    }


                    $this->data['post']['start_date_report'] = $start_date;
                    $this->data['post']['end_date_report'] = $end_date;
                    /*
                    $fields = "Video-View";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view = Gastar::calcTotals($data);

                    $fields = "Video-Impression";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_impression = Gastar::calcTotals($data);

                    $fields = "Video-Played";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_played = Gastar::calcTotals($data);

                    $fields = "Video-Finished";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_finished = Gastar::calcTotals($data);

                    $fields = "Video-View-Half";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view_half = Gastar::calcChannelTotals($data);

                    $fields = "Video-View-Three-Fourths";

                    $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

                    $video_view_three_fourths = Gastar::calcChannelTotals($data);

                    $video_played += $video_impression;

                    $this->data['feed']['video_played'] = $video_played;
                    $this->data['feed']['video_views'] = $video_view;
                    $this->data['feed']['video_finished'] = $video_finished;
                    $engagement1 = ($video_view/$video_played)*100;

                    if($video_view_half>0){
                    $engagement2 = ($video_view_half/$video_played)*100;
                    }
                    if($video_view_three_fourths>0){
                    $engagement3 = ($video_view_three_fourths/$video_played)*100;

                    }
                    $engagement4 = ($video_finished/$video_played)*100;
                    if($video_view_half>0 && $video_view_three_fourths>0){
                    $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
                    }else{
                    $engagement = ($engagement1+$engagement4)/2;
                    }*/

                    $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$videoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
                    $query = $this->db->query($SQL);
                    $rows = $query->result();

                    foreach($rows as $value){
                        $feed_video_views += $value->video_views;
                        $feed_page_views_ga += $value->page_views_ga;
                        $feed_revenue += $value->revenue;
                        $feed_impressions += $value->video_impressions;
                        $feed_engagement += $value->engagement;
                        $count++;
                    }

                    if($feed_video_views){
                        $this->data['feed']['video_views'] = $feed_video_views;
                    }else{
                        $this->data['feed']['video_views'] = "N/A";
                    }
                    if($feed_page_views_ga){
                        $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
                    }else{
                        $this->data['feed']['page_views_ga'] = "N/A";
                    }
                    if($feed_revenue){
                        $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
                    }else{
                        $this->data['feed']['revenue'] = "N/A";
                    }
                    if($feed_engagement){
                        $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
                    }else{
                        $this->data['feed']['engagement'] = "N/A";
                    }
                    if($feed_impressions){
                        $this->data['feed']['video_played'] = $feed_impressions;
                    }else{
                        $this->data['feed']['video_played'] = "N/A";
                    }
                    //$this->data['feed']['video_views'] = $rows->video_views;

                }

                $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
                $this->data['post']['total_reach'] = $this->chanel['total_reach'];

//                if($this->input->post('refresh', false) == true){
//
//                    $data = array();
//                    if ($this->form_validation->run() == true)
//                    {
//                        if($this->main_m->updateFeedVideo($this->data['post'])){
//                            redirect("content/feed/video_edit/video_$videoId", '');
//                        }
//                    }
//                }else{

                $this->_proc_feed_video_edit();

                $this->main_m->break_oflo_video_memcache($feed);

//                }
            }

//            redirect(site_url('content/feed/video_edit/video_'.$id));
            $this->load->view('feed_video_edit', $this->data);

            break;
        }
        case 'videoEmbed': {

            $feed = $this->main_m->getFeedVideo($feed_id);
//
            if($this->input->post('submit', false) === false){
//                $FeedVideo['video_size'] = $FeedVideo['video_size'];
//                $FeedVideo['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']);
//                $this->data['post'] = $FeedVideo;
                $feed['video_size'] = $feed['video_size'];
                $feed['format_size'] = $this->_makeSizeFormat($feed['video_size']);
                $this->data['post'] = $feed;
                $this->data['post']['feed_id'] = $feed_id;
                $this->data['post']['cid'] = $this->chanel['id'];


                $this->data['publishable'] = true;
                if($this->data['post']['brightcove_media_id'] != 'Null' && $this->data['post']['brightcove_media_id'] != 'None' && $this->data['post']['brightcove_media_id'] != '' && $this->brightcove->getVideoStatus($this->data['post']['brightcove_media_id']) != 'ACTIVE') {
                    $this->data['publishable'] = false;
                }
            }
            else{
                $l_bCLOSE_BUFFER = 1;

                if (($feed['id'] != "0" || $feed['id'] != "") && $l_bCLOSE_BUFFER){
                    Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

                window.location.href='/content/postfeed/';
                </SCRIPT>");
                }

                // DB insert
                $feed['description'] = $this->input->post('description');
                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
                //$feed['destination'] = $this->main_m->_copy_file('video',UPLOAD_CLOUD . $feed['destination']);
                //$feed['image_path'] = $this->main_m->_copy_file('photo',UPLOAD_CLOUD . $feed['image_path']);
                $feed['was_curated'] = 1;
//                $feed['is_oflo']        = $is_oflo;
//                $feed['is_oflo_slider'] = $is_oflo_slider;
//                $feed['is_premium']     = $is_premium;

                unset($feed['id']);

                $res = $this->main_m->db->insert("enr_video", $feed);
                $id = $this->main_m->db->insert_id();

                // Video Edit
                $i=0;
                while($viral_media_category[$i]){

                    $cat_id = $viral_media_category[$i]['id'];

                    $cat_update = $this->input->post('submit');

                    if($cat_update){
                        if($this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                        }elseif (!$this->input->post($cat_id, $id)){
                            $this->main_m->cat_post_delete($cat_id, $id);
                        }

                    }
                    $i++;

                }
                //
                $this->main_m->insertFeed('video', $id, $feed['fan_id'], $feed['is_publish']);

                $this->main_m->updateLastPost($feed['fan_id']);
                //

                $this->data['post'] = array(
                    'feed_id' => $id,
                    'cid' => $this->chanel['id'],
                    'description' => $this->input->post('description'),
                    'tags' => $this->input->post('tags'),
                    'credit' => $this->input->post('credit', ''),
                    'video_url' => $this->input->post('video_url', ''),
                    'is_publish' => 1,
                    'slider_thumb' => $this->input->post('slider_thumb'),
                    "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
                    "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                    'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                    'is_sticky' => $this->input->post('is_sticky'),
//                    'custom_image' => $this->input->post('video_img_value'),
                    'custom_image' => NULL,
                    'post_to_facebook' => $this->input->post('post_to_facebook'),
                    'post_to_twitter' => $this->input->post('post_to_twitter'),
                    'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                    "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
                    "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
                    'is_oflo' => $is_oflo,
                    'is_oflo_slider' => $is_oflo_slider,
                    'is_premium' => $is_premium,
                );

//                $this->data['post']['destination'] = $FeedVideo['destination'];
//                $this->data['post']['image_path'] = $FeedVideo['image_path'];
//                $this->data['post']['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']) ;
                $this->data['post']['destination'] = $feed['destination'];
                $this->data['post']['image_path'] = $feed['image_path'];


                $this->data['post']['format_size'] = $this->_makeSizeFormat($feed['video_size']) ;
                $accountId = GA_ACCOUNT_ID;

                $fullurl = "http://".$this->chanel['url'].".on.starsite.com";

                $ganame = $this->chanel['name'].' Channel';


                $profileID = $this->chanel['google_analytics_internal_id'];

                $videoId = "$id";

                if($profileID!=null){

                    $tableId = "ga:$profileID";

                    $start_date = $this->input->post('start_date_report');

                    $end_date = $this->input->post('end_date_report');


                    if($start_date==false){
//                        $start_date = $FeedVideo['start_date'];
                        $start_date = $feed['start_date'];
                        $str = explode(' ', $start_date);
                        $start_date = $str[0];
                    }
                    if($end_date==false){
                        $end_date = date('Y-m-d');
                    }


                    $this->data['post']['start_date_report'] = $start_date;
                    $this->data['post']['end_date_report'] = $end_date;

                    $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$videoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
                    $query = $this->db->query($SQL);
                    $rows = $query->result();

                    foreach($rows as $value){
                        $feed_video_views += $value->video_views;
                        $feed_page_views_ga += $value->page_views_ga;
                        $feed_revenue += $value->revenue;
                        $feed_impressions += $value->video_impressions;
                        $feed_engagement += $value->engagement;
                        $count++;
                    }

                    if($feed_video_views){
                        $this->data['feed']['video_views'] = $feed_video_views;
                    }else{
                        $this->data['feed']['video_views'] = "N/A";
                    }
                    if($feed_page_views_ga){
                        $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
                    }else{
                        $this->data['feed']['page_views_ga'] = "N/A";
                    }
                    if($feed_revenue){
                        $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
                    }else{
                        $this->data['feed']['revenue'] = "N/A";
                    }
                    if($feed_engagement){
                        $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
                    }else{
                        $this->data['feed']['engagement'] = "N/A";
                    }
                    if($feed_impressions){
                        $this->data['feed']['video_played'] = $feed_impressions;
                    }else{
                        $this->data['feed']['video_played'] = "N/A";
                    }
                    //$this->data['feed']['video_views'] = $rows->video_views;

                }

                $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
                $this->data['post']['total_reach'] = $this->chanel['total_reach'];

//                if($this->input->post('refresh', false) == true){
//
//                    $data = array();
//                    if ($this->form_validation->run() == true)
//                    {
//                        if($this->main_m->updateFeedVideo($this->data['post'])){
//                            redirect("content/feed/video_edit/video_$videoId", '');
//                        }
//                    }
//                }else{

                $this->_proc_feed_video_edit();

                $this->main_m->break_oflo_video_memcache($feed);

//                }
            }

//            redirect(site_url('content/feed/video_edit/video_'.$id));
//            $this->load->view('feed_video_edit', $this->data);
            $this->load->view('feed_videoEmbed_edit', $this->data);

            break;
        }
        case 'poll': {
          $feed = $this->main_m->getFeedPoll($feed_id);
          $feed_questions = $this->main_m->getFeedPoll_question_answer($feed_id);
          
          $feed['copy_origin_id'] = $feed['id'];
          $feed['copy_timestamp'] = time();
              $feed['image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['image_path']);

              $feed['background_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['background_image_path']);
              $feed['end_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['end_image_path']);
              unset($feed['id']);
              

              $this->main_m->db->insert('enr_poll', $feed);
              $poll_id = $this->main_m->db->insert_id();
              foreach($feed_questions as $key => $question){
                
                $pollQuestionSql = array(
                    'poll_id' => $poll_id,
                    'name' => $question['question_name']
                );
                $this->main_m->db->insert('enr_poll_question', $pollQuestionSql);
                $question_id = $this->main_m->db->insert_id();
                foreach($question['answer'] as $akey => $answer){
                  $pollAnswerSql = array(
                      'question_id' => $question_id,
                      'name' => $answer['name'],
                      'is_correct' => $answer['is_correct']
                  );
                  $this->main_m->db->insert('enr_poll_answer', $pollAnswerSql);
                }
              }
              $this->main_m->insertFeed('poll', $poll_id, $feed['fan_id'], $feed['is_publish']);

              redirect(site_url('content/feed/poll_edit/poll_'.$poll_id));
              break;
        }
        case 'quiz': {
          $feed = $this->main_m->getFeedQuiz($feed_id);
          $feed_questions = $this->main_m->getFeedQuiz_question_answer($feed_id);

          $feed['copy_origin_id'] = $feed['id'];
          $feed['copy_timestamp'] = time();
              $feed['image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['image_path']);
              $feed['background_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['background_image_path']);
              $feed['end_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['end_image_path']);
              unset($feed['id']);
          
              $this->main_m->db->insert('enr_quiz', $feed);
              $poll_id = $this->main_m->db->insert_id();
              foreach($feed_questions as $key => $question){
              
                $pollQuestionSql = array(
                    'quiz_id' => $poll_id,
                    'name' => $question['question_name']
                );
                $this->main_m->db->insert('enr_quiz_question', $pollQuestionSql);
                $question_id = $this->main_m->db->insert_id();
                foreach($question['answer'] as $akey => $answer){
                  $pollAnswerSql = array(
                      'question_id' => $question_id,
                      'name' => $answer['name'],
                      'is_correct' => $answer['is_correct']
                  );
                  $this->main_m->db->insert('enr_quiz_answer', $pollAnswerSql);
                }
              }
              $this->main_m->insertFeed('quiz', $poll_id, $feed['fan_id'], $feed['is_publish']);

              redirect(site_url('content/feed/quiz_edit/quiz_'.$poll_id));
              break;
        }
      }
    }

    /**
     * New Curate Feed copy.
     *    Video inserts into feed after share.
     *
     *    TODO: others?
     */
    public function feed_curate_copy(){

        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];

        $segment_3 = explode('_', $this->uri->segment(3));
        $post_type = $segment_3[0];
        $feed_id   = $segment_3[1];

        if(empty($feed_id)){
            show_error("Select a post to edit!");
            return;
        }

        $this->data['post_id'] = $feed_id;

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = 0;
        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');

        $l_bCLOSE_BUFFER = true;

        switch ($post_type){
            case 'text':  // Need to insert into feed after share?
                $feed = $this->main_m->getFeedText($feed_id);

                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();

                $id = $this->main_m->addFeedText($feed);
                redirect(site_url('content/feed/text_edit/text_'.$id));
                break;
            case CONTENT_TYPE_PHOTO:  // Need to insert into feed after share?
                $feed = $this->main_m->getFeedPhoto($feed_id);

                $new_photo_path              = "".$this->chanel['slug']."/assets/ufile/".$feed['destination'];
            
                $new_big_thumb_path          = "".$this->chanel['slug']."/assets/ufile/big_thumb/".$feed['image_path'];
                $new_thumb_path              = "".$this->chanel['slug']."/assets/ufile/thumb/".$feed['image_path'];
                $new_small_thumb_path        = "".$this->chanel['slug']."/assets/ufile/small_thumb/".$feed['image_path'];
                $new_small_square_thumb_path = "".$this->chanel['slug']."/assets/ufile/small_square_thumb/".$feed['image_path'];

                //prepare copied version
                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
                $feed['is_oflo']        = $is_oflo;
                $feed['is_oflo_slider'] = $is_oflo_slider;
                $feed['slider_thumb']   = $feed['slider_thumb'];

                unset($feed['id']);

//              $feed['destination'] = $this->main_m->_copy_file('photo','http://d3hl6zmfu9utfu.cloudfront.net/viral/assets/ufile/'. $feed['destination']);
                Utils::copyFileOnS3('viral/assets/ufile/'.$feed['destination'], $new_photo_path);
               
                Utils::copyFileOnS3('viral/assets/ufile/big_thumb/'.$feed['destination'], $new_big_thumb_path);
                Utils::copyFileOnS3('viral/assets/ufile/thumb/'.$feed['destination'], $new_thumb_path);
                Utils::copyFileOnS3('viral/assets/ufile/small_thumb/'.$feed['destination'], $new_small_thumb_path);
                Utils::copyFileOnS3('viral/assets/ufile/small_square_thumb/'.$feed['destination'], $new_small_square_thumb_path);

                //insert copied version
                $res = $this->main_m->db->insert("enr_photo", $feed);
                $id = $this->main_m->db->insert_id();
                $this->main_m->insertFeed('photo', $id, $feed['fan_id'], $feed['is_publish']);
                $this->main_m->updateLastPost($feed['fan_id']);

                $url = $this->controller->chanel['url'];

                // Break O-Flo Homepage Content Slider memcache
                if ($feed['is_oflo_slider'] == 'y'){
                    $oflo_homepage_slider_content_MemcacheKey = Memcaching::makeKey("oflo_homepage_slider_content");

                    Memcaching::getInstance()->removeKey($oflo_homepage_slider_content_MemcacheKey);
                }

                $this->main_m->break_oflo_photo_memcache($feed);

                redirect(site_url('content/feed/photo_edit/photo_'.$id));

                break;

            case CONTENT_TYPE_VIDEO:
                $this->form_validation->set_rules('description', 'Caption', 'required|max_length[65535]|xss_clean');
                $this->form_validation->set_rules('tags','Tags', 'max_length[32]|xss_clean');

//                $FeedVideo = $this->main_m->getFeedVideo($id);
                $FeedVideo = $this->main_m->getFeedVideo($feed_id);

                if($this->input->post('submit', false) === false){
                    $FeedVideo['video_size'] = $FeedVideo['video_size'];
                    $FeedVideo['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']);
                    $this->data['post'] = $FeedVideo;
                    $this->data['post']['feed_id'] = $feed_id;
                    $this->data['post']['cid'] = $this->chanel['id'];

                    $this->data['publishable'] = true;
                    if($this->data['post']['brightcove_media_id'] != 'Null' && $this->data['post']['brightcove_media_id'] != 'None' && $this->data['post']['brightcove_media_id'] != '' && $this->brightcove->getVideoStatus($this->data['post']['brightcove_media_id']) != 'ACTIVE') {
                        $this->data['publishable'] = false;
                    }
                }else{

                    //                if($this->input->post('refresh', false) == true){
//
//                    $data = array();
                    if ($this->form_validation->run() == true)
                    {
//                        if($this->main_m->updateFeedVideo($this->data['post'])){
//                            redirect("content/feed/video_edit/video_$videoId", '');
//                        }

                        // feed_curate_copy

                        $feed = $this->main_m->getFeedVideo($feed_id);

                        $new_path = "".$this->chanel['slug']."/assets/ufile/".$feed['destination'];
                        
                        $new_photo_path       = "".$this->chanel['slug']."/assets/ufile/".$feed['image_path'];
                        $new_big_thumb_path   = "".$this->chanel['slug']."/assets/ufile/big_thumb/".$feed['image_path'];
                        $new_thumb_path       = "".$this->chanel['slug']."/assets/ufile/thumb/".$feed['image_path'];
                        $new_small_thumb_path = "".$this->chanel['slug']."/assets/ufile/small_thumb/".$feed['image_path'];
                        $new_small_square_thumb_path = "".$this->chanel['slug']."/assets/ufile/small_square_thumb/".$feed['image_path'];

                        $new_watermark_photo_path       = "".$this->chanel['slug']."/assets/ufile/watermark/".$feed['image_path'];
                        $new_watermark_big_thumb_path   = "".$this->chanel['slug']."/assets/ufile/watermark/big_thumb/".$feed['image_path'];
                        $new_watermark_thumb_path       = "".$this->chanel['slug']."/assets/ufile/watermark/thumb/".$feed['image_path'];
                        $new_watermark_small_thumb_path = "".$this->chanel['slug']."/assets/ufile/watermark/small_thumb/".$feed['image_path'];
                        $new_watermark_small_square_thumb_path = "".$this->chanel['slug']."/assets/ufile/watermark/small_square_thumb/".$feed['image_path'];

                        

                        $filebroken = explode( '.', $feed['destination']);
                        $extension = array_pop($filebroken);
                        $fileTypeless = implode('.', $filebroken);
                        $nf          = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8");
                        $nf_cellular = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/cellular.m3u8");
                        $middle          = Utils::GetBetween("file-", ".m3u8", $nf->body);
                        $middle_cellular = Utils::GetBetween("file-", ".m3u8", $nf_cellular->body);

//                        var_dump($middle);
//                        var_dump($middle_cellular);

                        Utils::copyFileOnS3('viral/assets/ufile/'.$feed['destination'], $new_path);

                        Utils::copyFileOnS3('viral/assets/ufile/'.$feed['image_path'], $new_photo_path);
                        Utils::copyFileOnS3('viral/assets/ufile/big_thumb/'.$feed['image_path'], $new_big_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/thumb/'.$feed['image_path'], $new_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/small_thumb/'.$feed['image_path'], $new_small_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/small_square_thumb/'.$feed['image_path'], $new_small_square_thumb_path);

                        Utils::copyFileOnS3('viral/assets/ufile/watermark/'.$feed['image_path'], $new_watermark_photo_path);
                        Utils::copyFileOnS3('viral/assets/ufile/watermark/big_thumb/'.$feed['image_path'], $new_watermark_big_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/watermark/thumb/'.$feed['image_path'], $new_watermark_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/watermark/small_thumb/'.$feed['image_path'], $new_watermark_small_thumb_path);
                        Utils::copyFileOnS3('viral/assets/ufile/watermark/small_square_thumb/'.$feed['image_path'], $new_watermark_small_square_thumb_path);
                       
                        if(($feed_id!="0" || $feed_id!="")&& $l_bCLOSE_BUFFER){
                            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

                        window.location.href='/content/feed/';
                        </SCRIPT>");
                        }

                        $playlist = sizeof($middle[1]);
                        for($j=0;$j<($playlist);$j++){
                            Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8");
                            for($i=1;$i<10;$i++){
                                $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                                if($nf!=false){
                                    Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                                }else{
                                    break;
                                }
                            }
                        }

                        $playlist_cellular = sizeof($middle_cellular[1]);
                        for($j=0;$j<($playlist_cellular);$j++){
                            Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle_cellular[1][$j].".m3u8",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/file-".$middle_cellular[1][$j].".m3u8");
                            for($i=1;$i<10;$i++){
                                $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle_cellular[1][$j]."-0000".$i.".ts");
                                if($nf!=false){
                                    Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle_cellular[1][$j]."-0000".$i.".ts",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/file-".$middle_cellular[1][$j]."-0000".$i.".ts");
                                }else{
                                    break;
                                }
                            }
                        }

                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/master.m3u8");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/cellular.m3u8",$this->chanel['slug']."/assets/ufile/".$fileTypeless."/cellular.m3u8");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.mp4",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-1080.mp4");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.webm",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-1080.webm");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.mp4",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-720.mp4");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.webm",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-720.webm");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.mp4",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-480.mp4");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.webm",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-480.webm");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.mp4",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-360.mp4");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.webm",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-360.webm");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-216.mp4",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-216.mp4");
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-216.webm",$this->chanel['slug']."/assets/ufile/".$fileTypeless."-216.webm");

                        // DB insert
                        $feed['description'] = $this->input->post('description');
                        $feed['copy_origin_id'] = $feed['id'];
                        $rev_id = $feed['id'];
                        $feed['copy_timestamp'] = time();
                        $feed['was_curated'] = 1;
                        $feed['destination'] = $feed['destination'];
                        $feed['image_path'] = $feed['image_path'];
                        $feed['slider_thumb'] = $feed['slider_thumb'];
                        $feed['cid'] = $this->chanel['id'];
                        $feed['time_stamp'] = time();
                        $feed['start_date'] = date('Y-n-j H:i:s');
                        $feed['end_date'] = date('Y-n-j H:i:s', time() + 3600 * 24 * 30);
                        $feed['is_oflo'] = $is_oflo;
                        $feed['is_oflo_slider'] = $is_oflo_slider;
                        $feed['is_premium'] = $is_premium;

                        unset($feed['id']);

                        $res = $this->main_m->db->insert("enr_video", $feed);
                        $id = $this->main_m->db->insert_id();

                        // Share
                        $this->main_m->insertFeed('video', $id, $feed['fan_id'], $feed['is_publish']);

                        $this->main_m->updateLastPost($feed['fan_id']);

                        // Video Edit
                        $this->data['post'] = array(
//                            'feed_id' => $feed_id,
                            'feed_id' => $id,
                            'cid' => $this->chanel['id'],
                            'description' => $this->input->post('description'),
                            'tags' => $this->input->post('tags'),
                            'credit' => $this->input->post('credit', ''),
                            'video_url' => $this->input->post('video_url', ''),
                            'is_publish' => 1,
                            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
                            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                            'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                            'is_sticky' => $this->input->post('is_sticky'),
                            'custom_image' => $this->input->post('video_img_value'),
                            'slider_thumb' => $this->input->post('slider_thumb'),
                            'post_to_facebook' => $this->input->post('post_to_facebook'),
                            'post_to_twitter' => $this->input->post('post_to_twitter'),
                            'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                            "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
                            "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
                            'is_oflo' => $is_oflo,
                            'is_oflo_slider' => $is_oflo_slider,
                            'is_premium' => $is_premium,
                        );
                        $this->data['post']['destination'] = $FeedVideo['destination'];
                        $this->data['post']['image_path'] = $FeedVideo['image_path'];
                        $this->data['post']['slider_thumb'] = $FeedVideo['slider_thumb'];
                        $this->data['post']['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']) ;

                        $profileID = $this->chanel['google_analytics_internal_id'];

                        $videoId = "$id";

                        if($profileID != null){

                            $start_date = $this->input->post('start_date_report');

                            $end_date = $this->input->post('end_date_report');

                            if($start_date==false){
                                $start_date = $FeedVideo['start_date'];
                                $str = explode(' ', $start_date);
                                $start_date = $str[0];
                            }
                            if($end_date==false){
                                $end_date = date('Y-m-d');
                            }


                            $this->data['post']['start_date_report'] = $start_date;
                            $this->data['post']['end_date_report'] = $end_date;

                            $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$videoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
                            $query = $this->db->query($SQL);
                            $rows = $query->result();

                            foreach($rows as $value){
                                $feed_video_views += $value->video_views;
                                $feed_page_views_ga += $value->page_views_ga;
                                $feed_revenue += $value->revenue;
                                $feed_impressions += $value->video_impressions;
                                $feed_engagement += $value->engagement;
                                $count++;
                            }

                            if($feed_video_views){
                                $this->data['feed']['video_views'] = $feed_video_views;
                            }else{
                                $this->data['feed']['video_views'] = "N/A";
                            }
                            if($feed_page_views_ga){
                                $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
                            }else{
                                $this->data['feed']['page_views_ga'] = "N/A";
                            }
                            if($feed_revenue){
                                $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
                            }else{
                                $this->data['feed']['revenue'] = "N/A";
                            }
                            if($feed_engagement){
                                $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
                            }else{
                                $this->data['feed']['engagement'] = "N/A";
                            }
                            if($feed_impressions){
                                $this->data['feed']['video_played'] = $feed_impressions;
                            }else{
                                $this->data['feed']['video_played'] = "N/A";
                            }
                            //$this->data['feed']['video_views'] = $rows->video_views;

                        }

                        $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
                        $this->data['post']['total_reach'] = $this->chanel['total_reach'];

                        $i=0;
                        while($viral_media_category[$i]){

                            $cat_id = $viral_media_category[$i]['id'];

                            $cat_update = $this->input->post('submit');

                            if($cat_update){
                                if($this->input->post($cat_id, $id)){
                                    $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                                }elseif (!$this->input->post($cat_id, $id)){
                                    $this->main_m->cat_post_delete($cat_id, $id);
                                }

                            }
                            $i++;

                        }

                        $this->main_m->updateFeedVideo($this->data['post']);

                        $rev = array(
                            'viral_id' => $rev_id,
                            'content_id' => $id,
                            'cid' => $this->chanel['id']
                        );

                        $test = $this->main_m->db->insert('rev_share_post', $rev);

                        $this->main_m->break_oflo_video_memcache($feed);

                    }

                }

                $this->load->view('curate_feed_copy_video', $this->data);
                break;

            case 'poll': { // Need to insert into feed after share?
                $feed = $this->main_m->getFeedPoll($feed_id);
                $feed_questions = $this->main_m->getFeedPoll_question_answer($feed_id);

                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
                $feed['image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['image_path']);
                $feed['background_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['background_image_path']);
                $feed['end_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['end_image_path']);
                unset($feed['id']);


                $this->main_m->db->insert('enr_poll', $feed);
                $poll_id = $this->main_m->db->insert_id();
                foreach($feed_questions as $key => $question){

                    $pollQuestionSql = array(
                        'poll_id' => $poll_id,
                        'name' => $question['question_name']
                    );
                    $this->main_m->db->insert('enr_poll_question', $pollQuestionSql);
                    $question_id = $this->main_m->db->insert_id();
                    foreach($question['answer'] as $akey => $answer){
                        $pollAnswerSql = array(
                            'question_id' => $question_id,
                            'name' => $answer['name'],
                            'is_correct' => $answer['is_correct']
                        );
                        $this->main_m->db->insert('enr_poll_answer', $pollAnswerSql);
                    }
                }
                $this->main_m->insertFeed('poll', $poll_id, $feed['fan_id'], $feed['is_publish']);

                redirect(site_url('content/feed/poll_edit/poll_'.$poll_id));
                break;
            }
            case 'quiz': { // Need to insert into feed after share?
                $feed = $this->main_m->getFeedQuiz($feed_id);
                $feed_questions = $this->main_m->getFeedQuiz_question_answer($feed_id);

                $feed['copy_origin_id'] = $feed['id'];
                $feed['copy_timestamp'] = time();
                $feed['image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['image_path']);
                $feed['background_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['background_image_path']);
                $feed['end_image_path'] = $this->main_m->_copy_file('photo',UPLOAD_DIR . $feed['end_image_path']);
                unset($feed['id']);

                $this->main_m->db->insert('enr_quiz', $feed);
                $poll_id = $this->main_m->db->insert_id();
                foreach($feed_questions as $key => $question){

                    $pollQuestionSql = array(
                        'quiz_id' => $poll_id,
                        'name' => $question['question_name']
                    );
                    $this->main_m->db->insert('enr_quiz_question', $pollQuestionSql);
                    $question_id = $this->main_m->db->insert_id();
                    foreach($question['answer'] as $akey => $answer){
                        $pollAnswerSql = array(
                            'question_id' => $question_id,
                            'name' => $answer['name'],
                            'is_correct' => $answer['is_correct']
                        );
                        $this->main_m->db->insert('enr_quiz_answer', $pollAnswerSql);
                    }
                }
                $this->main_m->insertFeed('quiz', $poll_id, $feed['fan_id'], $feed['is_publish']);

                redirect(site_url('content/feed/quiz_edit/quiz_'.$poll_id));
                break;
            }
        }
    }

        public function feed_text(){
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'description' => $this->input->post('description'),
            'tags' => $this->input->post('tags'),
            'credit' => $this->input->post('credit'),
            'is_publish' => $this->input->post('is_publish'),
          "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
          "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
              'is_sticky' => $this->input->post('is_sticky')
        );
        $this->_proc_feed_text();
        $this->load->view('feed_text', $this->data);
    
    }
  
    public function home_feed_text(){
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'description' => $this->input->post('description'),
            'tags' => $this->input->post('tags'),
            'credit' => $this->input->post('credit'),
            'is_publish' => $this->input->post('is_publish'),
          "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
          "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
              'is_sticky' => $this->input->post('is_sticky')
        );
        $this->_proc_feed_text();
        $this->load->view('homefeed', $this->data);
    
    }
    
    public function feed_text_edit(){
        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "text_");
        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }
        if($this->input->post('submit', false) === false){
            $FeedText = $this->main_m->getFeedText($id);
            $this->data['post'] = $FeedText;
            $this->data['post']['feed_id'] = $feed_id;
            $this->data['post']['cid'] = $this->chanel['id'];
        }else{
            $this->data['post'] = array(
                'feed_id' => $feed_id,
                'cid' => $this->chanel['id'],
                'description' => $this->input->post('description'),
                'tags' => $this->input->post('tags'),
                'credit' => $this->input->post('credit'),
                'is_publish' => $this->input->post('is_publish'),
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
              'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
              'is_sticky' => $this->input->post('is_sticky')
            );
        }
        $this->_proc_feed_text_edit();
        $this->load->view('feed_text_edit', $this->data);
    }
    
    public function _proc_feed_text(){
        $this->form_validation->set_rules('description','description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->addFeedText($this->data['post'])){
                redirect("content/postfeed", '');
            }
        }
        return $data;
    }
    public function _proc_feed_text_edit(){
        $this->form_validation->set_rules('description', 'description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateFeedText($this->data['post'])){
                if(trim($this->uri->segment(2)) == 'community'){
                    redirect("content/community", '');
                }else{
                    redirect("content/postfeed", '');
                }
            }
        }
        return $data;
    }

    /**
     * Upload photo.
     */
    public function feed_photo(){

        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

        $viral_media_category = $this->data['viral_media_category'];




        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo = ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
            $is_feed = ($this->input->post('is_feed') ? 'y' : 'n');

        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = 0;
            $is_feed =  'n';
        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');

        $i=0;
        $results = array();

        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            if($this->input->post($cat_id)){
                $results[] = $cat_id;
            }

            $i++;
        }

        // Persist Form values
        $this->data['cat_id']=$results;
        $this->data['facebook_synd']=$this->input->post('post_to_facebook');
        $this->data['twitter_synd']=$this->input->post('post_to_twitter');
        $this->data['tumblr_synd']=$this->input->post('post_to_tumblr');
        $this->data['is_oflo']=$this->input->post('is_oflo');
        $this->data['is_oflo_slider']=$this->input->post('is_oflo_slider');
        $this->data['is_premium']=$this->input->post('is_premium');
        $this->data['is_feed']=$this->input->post('is_feed');

        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'description' => $this->input->post('description'),
            'tags' => $this->input->post('tags'),
            'credit' => $this->input->post('credit'),
            'url_link' => $this->input->post('url_link'),
            'slider_thumb' =>$this->input->post('slider_thumb'),
            'is_publish' => 1,
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "feed_date" => $this->input->post('feed_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => $this->input->post('is_sticky'),
            'post_to_facebook' => $this->input->post('post_to_facebook'),
            'post_to_twitter' => $this->input->post('post_to_twitter'),
            'post_to_tumblr' => $this->input->post('post_to_tumblr'),
            'is_oflo' => $is_oflo,
            'is_oflo_slider' => $is_oflo_slider,
            'is_premium' => $is_premium,
            'is_feed' => $is_feed,
            'product_widget' => $this->input->post('product_widget'),
        );

        $this->_proc_feed_photo();
        $this->load->view('feed_photo', $this->data);
    }

    private function _proc_feed_photo(){
        $this->form_validation->set_rules('description', 'Caption', 'required|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');

        $data = array();

        $valid_form = $this->form_validation->run();

        if ($valid_form == true && (isset($_FILES['image']) AND $_FILES['image']['size'])) {

            if($this->main_m->addFeedPhoto($this->data['post'])){
                redirect("content/postfeed", '');
            }

        } elseif ($this->input->post("submit", false) !== false && (!isset($_FILES['image']) OR !$_FILES['image']['size'])){

            $this->data['show_errors'][] = "Please select a photo.";
        }

        return $data;
    }

    public function feed_photo_edit($is_community=false){

       $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "photo_");


        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];
         
        $this->data['priority'] = $this->main_m->get_priority_id($id);
        $priority = $this->data['priority'];



        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');

        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "photo_");

        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }

        $this->data['post_id'] = $id;
        $this->data['post'] = $this->main_m->getFeedPhoto($id);

        $i=0; 
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            $cat_update = $this->input->post('submit');

            if($cat_update){
                if($this->input->post($cat_id, $id)){
                  $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_PHOTO);
                }elseif (!$this->input->post($cat_id, $id)){
                  $this->main_m->cat_post_delete($cat_id, $id);
                }

            }
            $i++;

        }


        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
            $is_feed =  ($this->input->post('is_feed') ? 'y' : 'n');

        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = $this->data['post']['is_premium'];
            $is_feed =  'n';
        }

        if($this->input->post('submit', false) === false){
            $FeedPhoto = $this->main_m->getFeedPhoto($id);
            $this->data['post'] = $FeedPhoto;
            $this->data['post']['feed_id'] = $feed_id;
            $this->data['post']['cid'] = $this->chanel['id'];
        }else{
            $this->data['post'] = array(
                'feed_id' => $feed_id,
                'cid' => $this->chanel['id'],
                'description' => $this->input->post('description'),
                'tags' => $this->input->post('tags'),
                'slider_thumb'=>$this->input->post('slider_thumb'),
                'credit' => $this->input->post('credit'),
                'url_link' => $this->input->post('url_link'),
                'is_publish' => 1,
                "start_date" => strtotime($this->input->post('time_update')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('time_update'),
                "feed_date" => $this->input->post('feed_update'),
                "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                'is_sticky' => $this->input->post('is_sticky'),
                'post_to_facebook' => $this->input->post('post_to_facebook'),
                'post_to_twitter' => $this->input->post('post_to_twitter'),
                'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                'is_oflo' => $is_oflo,
                'is_oflo_slider' => $is_oflo_slider,
                'is_premium' => $is_premium,
                'is_feed' => $is_feed,
                'product_widget' => $this->input->post('product_widget'),

            );
        }

         if($this->input->post('priorityCheckBox')){
                   

            $prioritySelected = $this->input->post('prioritySelected');
            $priorityDaySelected = $this->input->post('priorityDaySelected');
            $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

             $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                        VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')
                        ON DUPLICATE KEY UPDATE priority_number=$prioritySelected, duration='$priorityDaySelected', is_priority='$priorityCheckBox'";

              $this->db->query($insert);
      }else{
      

             $delete = "DELETE FROM priority WHERE content_id = $id";


              $this->db->query($delete);
            }
             

        $accountId = GA_ACCOUNT_ID;



   $fullurl = "http://".$this->chanel['url'].".on.starsite.com";
   
   $ganame = $this->chanel['name'].' Channel';

   /*
   $analytics = Gastar::getService();

   //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
   $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
   if($propertyID!=null){ 
    $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics); 
   }
   */

    $profileID = $this->chanel['google_analytics_internal_id'];

    $photoId = "$id";
    
    if($profileID!=null){
    
        $tableId = "ga:$profileID";

        $start_date = $this->input->post('start_date_report');

        $end_date = $this->input->post('end_date_report');


        

        if($start_date==false){
//          $start_date = $FeedVideo['start_date'];
            $start_date = $this->data['post']['start_date'];
            $str = explode(' ', $start_date);
            $start_date = $str[0];
        }
        if($end_date==false){
//          $end_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime($start_date) + 3600 * 24 * 30);

        }

        
        $this->data['post']['start_date_report'] = $start_date;
        $this->data['post']['end_date_report'] = $end_date;
        /*
        $fields = "Video-View";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

        $video_view = Gastar::calcTotals($data);

        $fields = "Video-Impression";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

        $video_impression = Gastar::calcTotals($data);

        $fields = "Video-Played";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

        $video_played = Gastar::calcTotals($data);

        $fields = "Video-Finished";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

        $video_finished = Gastar::calcTotals($data);

        $fields = "Video-View-Half";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);
        
        $video_view_half = Gastar::calcChannelTotals($data);

        $fields = "Video-View-Three-Fourths";

        $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);
        
        $video_view_three_fourths = Gastar::calcChannelTotals($data);

        $video_played += $video_impression;
       
        $this->data['feed']['video_played'] = $video_played;
        $this->data['feed']['video_views'] = $video_view;
        $this->data['feed']['video_finished'] = $video_finished;
        $engagement1 = ($video_view/$video_played)*100;

        if($video_view_half>0){
        $engagement2 = ($video_view_half/$video_played)*100;
        }
        if($video_view_three_fourths>0){
        $engagement3 = ($video_view_three_fourths/$video_played)*100;
     
        }
        $engagement4 = ($video_finished/$video_played)*100;
        if($video_view_half>0 && $video_view_three_fourths>0){
        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
        }else{
        $engagement = ($engagement1+$engagement4)/2;  
        }*/

//        $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$photoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
        $SQL = 'SELECT * FROM oflo_reports_monthly
                WHERE content_id = "'.$photoId.'"
                AND content_type = "photo"
                AND time_stamp
                BETWEEN "'.$start_date.'"
                AND "'.$end_date.'"';

        $query = $this->db->query($SQL);
        $rows = $query->result();

        $feed_video_views = 0;
        $feed_page_views_ga = 0;
        $feed_revenue = 0;
        $feed_impressions = 0;
        $feed_engagement = 0;
        $count = 0;

        foreach($rows as $value){
            if ($value->is_mobile == 1){
                $feed_video_views += $value->video_views;
            } else {
                $feed_page_views_ga += $value->page_views_ga;
            }
          $feed_revenue += $value->revenue;
          $feed_impressions += $value->video_impressions;
          $feed_engagement += $value->engagement;
          $count++;
        }

        if($feed_video_views){
        $this->data['feed']['video_views'] = $feed_video_views;
        }else{
        $this->data['feed']['video_views'] = "N/A";
        }
        if($feed_page_views_ga){
        $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
        }else{
        $this->data['feed']['page_views_ga'] = "N/A";
        }

        $this->data['feed']['total_views'] = $feed_video_views + $feed_page_views_ga;

        if($feed_revenue){
        $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
        }else{
        $this->data['feed']['revenue'] = "N/A";
        }
        if($feed_engagement){
        $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
        }else{
        $this->data['feed']['engagement'] = "N/A";
        }
        if($feed_impressions){
        $this->data['feed']['video_played'] = $feed_impressions;
        }else{
        $this->data['feed']['video_played'] = "N/A";
        }
        //$this->data['feed']['video_views'] = $rows->video_views;



        }

        $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
        $this->data['post']['total_reach'] = $this->chanel['total_reach'];
        
        $this->_proc_feed_photo_edit();
        $this->load->view('feed_photo_edit', $this->data);
    }

    public function _proc_feed_photo_edit(){
        $this->form_validation->set_rules('description', 'Caption', 'required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if ($this->input->post("submit", false) !== false && (isset($_FILES['image']) or $_FILES['image']['size'])) {

                if($this->main_m->updateFeedPhoto($this->data['post'])){

                    if(trim($this->uri->segment(2)) == 'community'){
                        redirect("content/community", '');
                    }else{
                        redirect("content/postfeed", '');
                    }

                }

            } elseif ($this->input->post("submit", false) !== false && (!isset($_FILES['image']) or !$_FILES['image']['size'])){

                $this->data['show_errors'][] = "Please select a photo.";
            }

        }
        return $data;
    }




/**
* Upload video.
* 
*/

    public function feed_video(){


        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

        $viral_media_category = $this->data['viral_media_category'];

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
             $is_feed =  ($this->input->post('is_feed') ? 'y' : 'n');

        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = 0;
            $is_feed =  'n';

        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');


        $i=0;
        $results = array();
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

                if($this->input->post($cat_id)){
                $results[] = $cat_id;

            }
            $i++;

        }

        // Persist Form values
        $this->data['cat_id']=$results;
        $this->data['facebook_synd']=$this->input->post('post_to_facebook');
        $this->data['twitter_synd']=$this->input->post('post_to_twitter');
        $this->data['tumblr_synd']=$this->input->post('post_to_tumblr');
        $this->data['is_oflo']=$this->input->post('is_oflo');
        $this->data['is_oflo_slider']=$this->input->post('is_oflo_slider');
        $this->data['is_premium']=$this->input->post('is_premium');
        $this->data['is_feed']=$this->input->post('is_feed');

         // if (isset($_POST['scheduleCheckBox']));
        // Build array for db query & persisting form data
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'description' => $this->input->post('description', TRUE),
            'destination' => $this->input->post('destination', TRUE),
            'video_mime' => $this->input->post('video_mime', TRUE),
            'video_size' => $this->input->post('video_size', TRUE),
            'video_width' => $this->input->post('video_width', TRUE),
            'video_height' => $this->input->post('video_height', TRUE),
            'video_duration' => $this->input->post('video_duration', TRUE),
            'tags' => $this->input->post('tags'),
            'credit' => $this->input->post('credit', ''),
            'video_url' => $this->input->post('video_url'),
            'slider_thumb' => $this->input->post('slider_thumb'),
            'is_publish' => 0, // Transcoding
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "feed_date" => $this->input->post('feed_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => $this->input->post('is_sticky'),
            'custom_image' => $this->input->post('video_img_value'),
            'post_to_facebook' => $this->input->post('post_to_facebook'),
            'post_to_twitter' => $this->input->post('post_to_twitter'),
            'post_to_tumblr' => $this->input->post('post_to_tumblr'),
            'is_oflo' => $is_oflo,
            'is_oflo_slider' => $is_oflo_slider,
            'is_premium' => $is_premium,
            'is_feed' => $is_feed,
            'product_widget' => $this->input->post('product_widget'),
        );

     
        $this->_proc_feed_video();
        
        $this->load->view('feed_video', $this->data);
    }

    private function _proc_feed_video(){
        $this->form_validation->set_rules('description', 'Caption', 'trim|required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');
        
        $data = array();
   
        $valid_form = $this->form_validation->run();

        if ($this->input->post("submit", false) !== false) {

            $destination = $this->input->post('destination');

            if ( $valid_form && !empty($destination) ) {

                if($this->main_m->addFeedVideo($this->data['post'])) {

                    if (trim($this->uri->segment(2)) == 'community') {
                        redirect("content/community", '');
                    } else {

                          if($this->input->post('is_oflo')){

                              $this->data['feed_video'] = $this->main_m->add_feed_notification();


                                   $l_sCommand = PHP_PATH.' '.PUSH_NOTIFICATION;
       
              shell_exec($l_sCommand);  

                    }



                        redirect("content/postfeed", '');
                    }
                }

            } elseif ( empty($destination) ){

                $this->data['show_errors'][] = "Please select a video.";
            }

        }

        return $data;
    }


   
    public function feed_video_edit(){

        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "video_");

        // var_dump($id);

        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

       

        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();

        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();
         
        $viral_media_category = $this->data['viral_media_category'];

        $this->data['priority'] = $this->main_m->get_priority_id($id);
        $priority = $this->data['priority'];

       $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');


        //video id
        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "video_");
        $this->data['post_id'] = $id;
        $this->data['post'] = $this->main_m->getFeedVideo($id);
        
        
        if ($this->data['post']['video_url'] != '0' &&
            $this->data['post']['video_url'] != "NULL" &&
            $this->data['post']['video_url'] != NULL ){

            redirect('content/feed/videoEmbed_edit/video_'.$id, '');
        }

        $i=0;
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            $cat_update = $this->input->post('submit');

            if($cat_update){
                if($this->input->post($cat_id, $id)){
                    $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                }elseif (!$this->input->post($cat_id, $id)){
                    $this->main_m->cat_post_delete($cat_id, $id);
                }

            }
            $i++;

      }
           
      
        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }
      
        $FeedVideo = $this->main_m->getFeedVideo($id);

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
            $is_feed =  ($this->input->post('is_feed') ? 'y' : 'n');
        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = $FeedVideo['is_premium'];
             $is_feed =  'n';

        }

        if($this->input->post('submit', false) === false){
            $FeedVideo['video_size'] = $FeedVideo['video_size'];
        
            $FeedVideo['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']);
            $this->data['post'] = $FeedVideo;
            $this->data['post']['feed_id'] = $feed_id;
            $this->data['post']['cid'] = $this->chanel['id'];
            

            $this->data['publishable'] = true;
            if($this->data['post']['brightcove_media_id'] != 'Null' && $this->data['post']['brightcove_media_id'] != 'None' && $this->data['post']['brightcove_media_id'] != '' && $this->brightcove->getVideoStatus($this->data['post']['brightcove_media_id']) != 'ACTIVE') {
                $this->data['publishable'] = false;
            }
        }else{
            $this->data['post'] = array(
                'feed_id' => $feed_id,
                'cid' => $this->chanel['id'],
                'description' => $this->input->post('description'),
                'tags' => $this->input->post('tags'),
                'credit' => $this->input->post('credit', ''),
                'video_url' => $this->input->post('video_url', ''),
                'slider_thumb' =>$this->input->post('slider_thumb'),
                'is_publish' => 1,
                "start_date" => strtotime($this->input->post('time_update')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('time_update'),
                "feed_date" =>  $this->input->post('feed_update'),
                "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                'is_sticky' => $this->input->post('is_sticky'),
                'custom_image' => $this->input->post('video_img_value'),
                'post_to_facebook' => $this->input->post('post_to_facebook'),
                'post_to_twitter' => $this->input->post('post_to_twitter'),
                'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
                "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
                'is_oflo' => $is_oflo,
                'is_oflo_slider' => $is_oflo_slider,
                'is_premium' => $is_premium,
                'is_feed' => $is_feed,
                'product_widget' => $this->input->post('product_widget'),
            );
            $this->data['post']['slider_thumb'] =$FeedVideo['slider_thumb'];
            $this->data['post']['destination'] = $FeedVideo['destination'];
            $this->data['post']['image_path'] = $FeedVideo['image_path'];
            $this->data['post']['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']);
         
        }

       if($this->input->post('priorityCheckBox')){
                   

            $prioritySelected = $this->input->post('prioritySelected');
            $priorityDaySelected = $this->input->post('priorityDaySelected');
            $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

             $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                        VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')
                        ON DUPLICATE KEY UPDATE priority_number=$prioritySelected, duration='$priorityDaySelected', is_priority='$priorityCheckBox'";


              $this->db->query($insert);


      }else{
      

             $delete = "DELETE FROM priority WHERE content_id = $id";


              $this->db->query($delete);



      }
             




        $accountId = GA_ACCOUNT_ID;



        $fullurl = "http://".$this->chanel['url'].".on.starsite.com";

        $ganame = $this->chanel['name'].' Channel';

        /*
        $analytics = Gastar::getService();

        //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
        $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
        if($propertyID!=null){
         $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
        }
        */

        $profileID = $this->chanel['google_analytics_internal_id'];

        $videoId = "$id";

        if($profileID!=null){

            $tableId = "ga:$profileID";

            $start_date = $this->input->post('start_date_report');

            $end_date = $this->input->post('end_date_report');




            if($start_date==false){
                $start_date = $FeedVideo['start_date'];
                $str = explode(' ', $start_date);
                $start_date = $str[0];

//                $start_date = date("Y-m-01", strtotime($start_date));
            }
            if($end_date==false){
//                $end_date = date('Y-m-d');
//                $end_date = date("Y-m-t", strtotime($start_date) + 3600 * 24 * 30);
                $end_date = date('Y-m-d', strtotime($start_date) + 3600 * 24 * 30);
            }

//            echo "<BR>\$start_date: ".$start_date;
//            echo "<BR>\$end_date: ".$end_date;
//            exit;

            $this->data['post']['start_date_report'] = $start_date;
            $this->data['post']['end_date_report'] = $end_date;
            /*
            $fields = "Video-View";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view = Gastar::calcTotals($data);

            $fields = "Video-Impression";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_impression = Gastar::calcTotals($data);

            $fields = "Video-Played";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_played = Gastar::calcTotals($data);

            $fields = "Video-Finished";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_finished = Gastar::calcTotals($data);

            $fields = "Video-View-Half";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view_half = Gastar::calcChannelTotals($data);

            $fields = "Video-View-Three-Fourths";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view_three_fourths = Gastar::calcChannelTotals($data);

            $video_played += $video_impression;

            $this->data['feed']['video_played'] = $video_played;
            $this->data['feed']['video_views'] = $video_view;
            $this->data['feed']['video_finished'] = $video_finished;
            $engagement1 = ($video_view/$video_played)*100;

            if($video_view_half>0){
            $engagement2 = ($video_view_half/$video_played)*100;
            }
            if($video_view_three_fourths>0){
            $engagement3 = ($video_view_three_fourths/$video_played)*100;

            }
            $engagement4 = ($video_finished/$video_played)*100;
            if($video_view_half>0 && $video_view_three_fourths>0){
            $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
            }else{
            $engagement = ($engagement1+$engagement4)/2;
            }*/

//            $SQL = 'SELECT * FROM sc_reports_daily WHERE content_id = "'.$videoId.'" AND time_stamp BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
            $SQL = 'SELECT * FROM oflo_reports_monthly
                    WHERE content_id = "'.$videoId.'"
                    AND content_type = "video"
                    AND time_stamp
                    BETWEEN "'.$start_date.'"
                    AND "'.$end_date.'"';

            $query = $this->db->query($SQL);
            $rows = $query->result();

            $feed_video_views = 0;
            $feed_page_views_ga = 0;
            $feed_revenue = 0;
            $feed_impressions = 0;
            $feed_engagement = 0;
            $count = 0;

            foreach($rows as $value){
                if ($value->is_mobile == 1){
                    $feed_video_views += $value->video_views;
                } else {
                    $feed_page_views_ga += $value->page_views_ga;
                }
                $feed_revenue += $value->revenue;
                $feed_impressions += $value->video_impressions;
                $feed_engagement += $value->engagement;
                $count++;
            }

            if($feed_video_views){
                $this->data['feed']['video_views'] = $feed_video_views;
            }else{
                $this->data['feed']['video_views'] = "N/A";
            }
            if($feed_page_views_ga){
                $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
            }else{
                $this->data['feed']['page_views_ga'] = "N/A";
            }

            $this->data['feed']['total_views'] = $feed_video_views + $feed_page_views_ga;
//            var_dump($this->data['feed']['total_views']);
//            exit;

            if($feed_revenue){
                $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
            }else{
                $this->data['feed']['revenue'] = "N/A";
            }
            if($feed_engagement){
                $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
            }else{
                $this->data['feed']['engagement'] = "N/A";
            }
            if($feed_impressions){
                $this->data['feed']['video_played'] = $feed_impressions;
            }else{
                $this->data['feed']['video_played'] = "N/A";
            }
            //$this->data['feed']['video_views'] = $rows->video_views;



        }

        $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
        $this->data['post']['total_reach'] = $this->chanel['total_reach'];

        if($this->input->post('refresh', false) == true){

            $data = array();
            if ($this->form_validation->run() == true)
            {
                if($this->main_m->updateFeedVideo($this->data['post'])){
                    redirect("content/feed/video_edit/video_$videoId", '');
                }
            }
        }else{

            $this->_proc_feed_video_edit();

        }

        $this->load->view('feed_video_edit', $this->data);
}
    
    public function _proc_feed_video_edit(){
        $this->form_validation->set_rules('description', 'Caption', 'required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');
        
        $data = array();
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateFeedVideo($this->data['post'])){
                redirect("content/postfeed", '');
            }
        }
//         print_r($this->form_validation->run());exit;
        return $data;
    }

    public function feed_videoEmbed_edit(){

        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "video_");

        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }


        $this->data['viral_media_cat_post'] = $this->main_m->get_viral_media_cat_post();
        $this->data['viral_media_xml'] = $this->main_m->get_viral_media_xml_all();

        $viral_media_category = $this->data['viral_media_category'];
        
      $this->data['priority'] = $this->main_m->get_priority_id($id);

        $priority = $this->data['priority'];

        //video id
        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "video_");
//        $id = ltrim($feed_id, "youtube_");
        $this->data['post_id'] = $id;
        $this->data['post'] = $this->main_m->getFeedVideo($id);

        $i=0;
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            $cat_update = $this->input->post('submit');

            if($cat_update){
                if($this->input->post($cat_id, $id)){
                    $this->main_m->cat_post_add($cat_id, $id, CONTENT_TYPE_ID_VIDEO);
                }elseif (!$this->input->post($cat_id, $id)){
                    $this->main_m->cat_post_delete($cat_id, $id);
                }

            }
            $i++;

        }

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  ($this->input->post('is_premium') ? 1 : 0);
            $is_feed =  ($this->input->post('is_feed') ? 'y' : 'n');

        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
             $is_feed =  'n';
            $is_premium = $this->data['post']['is_premium'];
        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');

        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }

        $FeedVideo = $this->main_m->getFeedVideo($id);

        if($this->input->post('submit', false) === false){
            $FeedVideo['video_size'] = $FeedVideo['video_size'];
            $FeedVideo['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']);
            $this->data['post'] = $FeedVideo;
            $this->data['post']['feed_id'] = $feed_id;
            $this->data['post']['cid'] = $this->chanel['id'];


            $this->data['publishable'] = true;
            if($this->data['post']['brightcove_media_id'] != 'Null' && $this->data['post']['brightcove_media_id'] != 'None' && $this->data['post']['brightcove_media_id'] != '' && $this->brightcove->getVideoStatus($this->data['post']['brightcove_media_id']) != 'ACTIVE') {
                $this->data['publishable'] = false;
            }
        }else{
            $this->data['post'] = array(
                'feed_id' => $feed_id,
                'cid' => $this->chanel['id'],
                'description' => $this->input->post('description'),
                'tags' => $this->input->post('tags'),
                'credit' => $this->input->post('credit', ''),
                'video_url' => $this->input->post('video_url', ''),
                'video_type' => $this->input->post('video_type'),
                'slider_thumb'=>$this->input->post('slider_thumb'),
                'is_publish' => 1,
                 "start_date" => strtotime($this->input->post('time_update')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('time_update'),
                 "feed_date" => $this->input->post('feed_update'),
                "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
                'time_stamp' => $this->input->post('manual_time') == '' ? 0 : strtotime($this->input->post('time_stamp')),
                'is_sticky' => $this->input->post('is_sticky'),
                'custom_image' => $this->input->post('video_img_value'),
                'post_to_facebook' => $this->input->post('post_to_facebook'),
                'post_to_twitter' => $this->input->post('post_to_twitter'),
                'post_to_tumblr' => $this->input->post('post_to_tumblr'),
                "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
                "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
                'is_oflo' => $is_oflo,
                'is_oflo_slider' => $is_oflo_slider,
                'is_premium' => $is_premium,
                'is_feed' => $is_feed,
                'product_widget' => $this->input->post('product_widget'),
            );
            $this->data['post']['destination'] = $FeedVideo['destination'];
            $this->data['post']['image_path'] = $FeedVideo['image_path'];
            $this->data['post']['slider_thumb'] =$FeedVideo['slider_thumb'];
//            $this->data['post']['format_size'] = $this->_makeSizeFormat($FeedVideo['video_size']) ;
        }



             if($this->input->post('priorityCheckBox')){
                   

            $prioritySelected = $this->input->post('prioritySelected');
            $priorityDaySelected = $this->input->post('priorityDaySelected');
            $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

             $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                        VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')
                        ON DUPLICATE KEY UPDATE priority_number=$prioritySelected, duration='$priorityDaySelected', is_priority='$priorityCheckBox'";

              $this->db->query($insert);
      }else{
      

             $delete = "DELETE FROM priority WHERE content_id = $id";


              $this->db->query($delete);
            }
             
             
                

        $accountId = GA_ACCOUNT_ID;



        $fullurl = "http://".$this->chanel['url'].".on.starsite.com";

        $ganame = $this->chanel['name'].' Channel';

        /*
        $analytics = Gastar::getService();

        //$usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
        $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
        if($propertyID!=null){
         $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
        }
        */

        $profileID = $this->chanel['google_analytics_internal_id'];

        $videoId = "$id";

        if($profileID!=null){

            $tableId = "ga:$profileID";

            $start_date = $this->input->post('start_date_report');

            $end_date = $this->input->post('end_date_report');




            if($start_date==false){
                $start_date = $FeedVideo['start_date'];
                $str = explode(' ', $start_date);
                $start_date = $str[0];
            }
            if($end_date==false){
//                $end_date = date('Y-m-d');
                $end_date = date('Y-m-d', strtotime($start_date) + 3600 * 24 * 30);
            }


            $this->data['post']['start_date_report'] = $start_date;
            $this->data['post']['end_date_report'] = $end_date;
            /*
            $fields = "Video-View";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view = Gastar::calcTotals($data);

            $fields = "Video-Impression";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_impression = Gastar::calcTotals($data);

            $fields = "Video-Played";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_played = Gastar::calcTotals($data);

            $fields = "Video-Finished";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_finished = Gastar::calcTotals($data);

            $fields = "Video-View-Half";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view_half = Gastar::calcChannelTotals($data);

            $fields = "Video-View-Three-Fourths";

            $data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

            $video_view_three_fourths = Gastar::calcChannelTotals($data);

            $video_played += $video_impression;

            $this->data['feed']['video_played'] = $video_played;
            $this->data['feed']['video_views'] = $video_view;
            $this->data['feed']['video_finished'] = $video_finished;
            $engagement1 = ($video_view/$video_played)*100;

            if($video_view_half>0){
            $engagement2 = ($video_view_half/$video_played)*100;
            }
            if($video_view_three_fourths>0){
            $engagement3 = ($video_view_three_fourths/$video_played)*100;

            }
            $engagement4 = ($video_finished/$video_played)*100;
            if($video_view_half>0 && $video_view_three_fourths>0){
            $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
            }else{
            $engagement = ($engagement1+$engagement4)/2;
            }*/

            $SQL = 'SELECT * FROM sc_reports_daily
                    WHERE content_id = "'.$videoId.'"
                    AND content_type = "video"
                    AND time_stamp
                    BETWEEN "'.$start_date.'"
                    AND "'.$end_date.'"';

            $query = $this->db->query($SQL);
            $rows = $query->result();

            $feed_video_views = 0;
            $feed_page_views_ga = 0;
            $feed_revenue = 0;
            $feed_impressions = 0;
            $feed_engagement = 0;
            $count = 0;

            foreach($rows as $value){
                if ($value->is_mobile == 1){
                    $feed_video_views += $value->video_views;
                } else {
                    $feed_page_views_ga += $value->page_views_ga;
                }
                $feed_revenue += $value->revenue;
                $feed_impressions += $value->video_impressions;
                $feed_engagement += $value->engagement;
                $count++;
            }

            if($feed_video_views){
                $this->data['feed']['video_views'] = $feed_video_views;
            }else{
                $this->data['feed']['video_views'] = "N/A";
            }
            if($feed_page_views_ga){
                $this->data['feed']['page_views_ga'] = $feed_page_views_ga;
            }else{
                $this->data['feed']['page_views_ga'] = "N/A";
            }

            $this->data['feed']['total_views'] = $feed_video_views + $feed_page_views_ga;

            if($feed_revenue){
                $this->data['feed']['revenue'] = number_format($feed_revenue, 2, '.', '');
            }else{
                $this->data['feed']['revenue'] = "N/A";
            }
            if($feed_engagement){
                $this->data['feed']['engagement'] = number_format(($feed_engagement/$count), 2, '.', '');
            }else{
                $this->data['feed']['engagement'] = "N/A";
            }
            if($feed_impressions){
                $this->data['feed']['video_played'] = $feed_impressions;
            }else{
                $this->data['feed']['video_played'] = "N/A";
            }
            //$this->data['feed']['video_views'] = $rows->video_views;



        }

        $this->data['post']['current_cpm'] = $this->chanel['current_cpm'];
        $this->data['post']['total_reach'] = $this->chanel['total_reach'];





        if($this->input->post('refresh', false) == true){

//            $data = array();
//            if ($this->form_validation->run() == true)
//            {
//                if($this->main_m->updateFeedVideo($this->data['post'])){
//                    redirect("content/feed/video_edit/video_$videoId", '');
//                }
//            }
        }else{

            $this->_proc_videoEmbed_edit();

        }


        $this->load->view('feed_videoEmbed_edit', $this->data);
    }

    public function _proc_videoEmbed_edit(){
        $this->form_validation->set_rules('description', 'Caption', 'required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');
        $this->form_validation->set_rules('video_type','Embed Video Type', 'trim|required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('video_url','Embed Video URL', 'trim|required|max_length[65535]|xss_clean');

        if ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_YOUTUBE){
            $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_youtube_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VIMEO){
            $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vimeo_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VEVO){
            // $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vevo_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VINE){
        $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vine_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_INSTAGRAM){
          $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_instagram_URL_check');
        }

        $data = array();
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateFeedVideo($this->data['post'])){
                redirect("content/postfeed", '');
            }
        }
//         print_r($this->form_validation->run());exit;
        return $data;
    }

/**
* feed poll
* 
*/
    public function feed_poll(){
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            "caption" => $this->input->post('caption'),
            'description' => $this->input->post('description'),
            'end_description' => $this->input->post('end_description'),
            "text" => $this->input->post('text'),
            'tags' => $this->input->post('tags'),
            'is_publish' => $this->input->post('is_publish'),
          "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
          "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => $this->input->post('is_sticky')
        );
        $this->_proc_feed_poll();
        $this->load->view('feed_poll', $this->data);
    }
    private function _proc_feed_poll(){
        $this->form_validation->set_rules('description', 'description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
//            if($this->input->post("submit", false)!==false && !$_FILES['image1']['size']){
//                $this->data['show_errors'][] = "Please select a opening page image.";
//            }elseif($this->input->post("submit", false)!==false && !$_FILES['image2']['size']){
//                $this->data['show_errors'][] = "Please select a closing page image.";
//            }else
            if($this->main_m->addFeedPoll()){
//                redirect("content/feed/poll", '');
                redirect("content/feed", '');
            }
        }
        return $data;
    }
    public function feed_poll_edit(){
        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "poll_");
        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }

        $this->_proc_feed_poll_edit($id);
        $feedPoll = $this->main_m->getFeedPoll($id);
        $this->data['post'] = $feedPoll;
        $pollQuestion = $this->main_m->getFeedPoll_question_answer($id);
        $this->data['pollQuestions'] = $pollQuestion;
        /**
        * get fans answered this quiz
        */
        $this->data['fans'] = $this->main_m->getFansAnsweredPoll($id);

        $this->data['post']['feed_id']= $feed_id;
        
        $this->load->view('feed_poll_edit', $this->data);
    }
    private function _proc_feed_poll_edit($id){
        $this->form_validation->set_rules('description', 'description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
//            if($this->input->post("submit", false)!==false && !$_FILES['image1']['size']){
//                $this->data['show_errors'][] = "Please select a opening page image.";
//            }elseif($this->input->post("submit", false)!==false && !$_FILES['image2']['size']){
//                $this->data['show_errors'][] = "Please select a closing page image.";
//            }else
            if($this->main_m->updateFeedPoll($id)){
//                redirect("content/feed/poll", '');
                redirect("content/feed", '');
            }
        }
        return $data;
    }
/**
* Feed Quiz
* 
*/
    public function feed_quiz(){
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            "caption" => $this->input->post('caption'),
            'description' => $this->input->post('description'),
            'end_description' => $this->input->post('end_description'),
            "text" => $this->input->post('text'),
            'tags' => $this->input->post('tags'),
            'is_publish' => $this->input->post('is_publish'),
          "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
          "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => $this->input->post('is_sticky')
        );
        $this->_proc_feed_quiz();
        $this->load->view('feed_quiz', $this->data);
    }
    private function _proc_feed_quiz(){
        $this->form_validation->set_rules('description', 'description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
//            if($this->input->post("submit", false)!==false && !$_FILES['image1']['size']){
//                $this->data['show_errors'][] = "Please select a opening page image.";
//            }elseif($this->input->post("submit", false)!==false && !$_FILES['image2']['size']){
//                $this->data['show_errors'][] = "Please select a closing page image.";
//            }else
            if($this->main_m->addFeedQuiz()){
//                redirect("content/feed/poll", '');
                redirect("content/feed", '');
            }
        }
        return $data;
    }
    public function feed_quiz_edit(){
        $feed_id = $this->uri->segment(4);
        $id = ltrim($feed_id, "quiz_");
        if(empty($id)){
            show_error("Select a post to edit!");
            return;
        }

        $this->_proc_feed_quiz_edit($id);
        $feedPoll = $this->main_m->getFeedQuiz($id);
        $this->data['post'] = $feedPoll;
        $pollQuestion = $this->main_m->getFeedQuiz_question_answer($id);
        $this->data['pollQuestions'] = $pollQuestion;
        $this->data['post']['feed_id']= $feed_id;
        
        /**
        * get fans answered this quiz
        */
        $this->data['fans'] = $this->main_m->getFansAnsweredQuiz($id);
        
        
        $this->load->view('feed_quiz_edit', $this->data);
    }
    private function _proc_feed_quiz_edit($id){
        $this->form_validation->set_rules('description', 'description', 'required|xss_clean');
        $this->form_validation->set_rules('credit','credit', 'numeric|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
//            if($this->input->post("submit", false)!==false && !$_FILES['image1']['size']){
//                $this->data['show_errors'][] = "Please select a opening page image.";
//            }elseif($this->input->post("submit", false)!==false && !$_FILES['image2']['size']){
//                $this->data['show_errors'][] = "Please select a closing page image.";
//            }else
            if($this->main_m->updateFeedQuiz($id)){
//                redirect("content/feed/poll", '');
                redirect("content/feed", '');
            }
        }
        return $data;
    }

     public function delete_curate(){
        $id = $this->uri->segment(3);
        $ids = explode("_", $id);
        $feed_type = $ids[0];
        $id = $ids[1];
        call_user_func_array(array($this->main_m, "deleteFeed".$feed_type), array("id"=>$id));
        $result = $this->main_m->deleteFeed_Categories($feed_type, $id);

        redirect('content/curate', '');
    }
    public function delete_feed(){
        $id = $this->uri->segment(3);
        $ids = explode("_", $id);
        $feed_type = $ids[0];
        $id = $ids[1];
        call_user_func_array(array($this->main_m, "deleteFeed".$feed_type), array("id"=>$id));
        $result = $this->main_m->deleteFeed_Categories($feed_type, $id);
        $res = $this->main_m->deleteFeed_priority($id);
        redirect('content/postfeed', '');
    }


public function post_unpublish($id, $post_type, $is_publish){
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];

     

         if($this->input->post('is_publish')=='on'){
                $is_publish = 1;

            }else{
                $is_publish= 3;
            }


           if($this->main_m->update_post_status($feed_id, $post_type,$is_publish))
            // var_dump($feed_id);exit;

             $strSql = "DELETE FROM enr_like WHERE content_id  = $feed_id ";

           // var_dump($strSql); exit;
              $this->db->query($strSql);

            {
                redirect("content/postfeed", '');
            }
        
        return $data;
      
}

public function post_unpublish_homefeed($id, $post_type, $is_publish){
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];
     

         if($this->input->post('is_publish')=='on'){
                $is_publish = 1;

            }else{
                $is_publish= 3;
            }


           if($this->main_m->update_post_status($feed_id, $post_type,$is_publish))
             $strSql = "DELETE FROM enr_like WHERE content_id  = $feed_id ";

           // var_dump($strSql); exit;
              $this->db->query($strSql);


            {
                redirect("content/feed", '');
            }
        
        return $data;
      
}

public function post_republish_homefeed($id, $post_type, $is_publish){
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];
     

         if($this->input->post('is_publish')=='on'){
                $is_publish = 3;

            }else{
                $is_publish= 1;
            }


           if($this->main_m->update_post_status($feed_id, $post_type,$is_publish))

            {
                redirect("content/feed", '');
            }
        
        return $data;
      
}

public function post_republish($id, $post_type, $is_publish){
        $id = explode('_', $this->uri->segment(3));
        $post_type = $id[0];
        $feed_id   = $id[1];
     

         if($this->input->post('is_publish')=='on'){
                $is_publish = 3;

            }else{
                $is_publish= 1;
            }


           if($this->main_m->update_post_status($feed_id, $post_type,$is_publish))

            {
                redirect("content/postfeed", '');
            }
        
        return $data;
      
}



    /**
     * Delete Dashboard feed.
     */
    public function delete_home_feed(){
        $id = $this->uri->segment(3);
        $ids = explode("_", $id);
        $feed_type = $ids[0];
        $id = $ids[1];
        call_user_func_array(array($this->main_m, "deleteFeed".$feed_type), array("id"=>$id));
        $result = $this->main_m->deleteFeed_Categories($feed_type, $id);
        $res = $this->main_m->deleteFeed_priority($id);

        redirect('content/feed', '');
    }

    public function delete_community(){
        $id = $this->uri->segment(3);
        $ref = isset($_REQUEST['ref']) ? $_REQUEST['ref'] : 'content/community';
        $ids = explode("_", $id);
        $feed_type = $ids[0];
        $id = $ids[1];
        call_user_func_array(array($this->main_m, "deleteFeed".$feed_type), array("id"=>$id));
        
        redirect($ref, '');
    }
    /**
    * Community
    * 
    */

    public function community(){
      //pagination
        
        $page = $this->uri->segment(3);//empty($this->uri->segment(4)) ? 0 : $this->uri->segment(4);
        $search = $this->uri->segment(4);
        if(empty($page)) $page = 0;
        $this->data['search'] = $search;
        $this->data['page'] = $page;
        $this->data['page_count'] = 10;
        $total_count = count($this->main_m->getCommunityFeedList(false, false, false, 0, -1, $search));
        $this->data['total_page_count'] = ceil($total_count / $this->data['page_count']);
        $feed = $this->uri->segment(3,false);

        $this->data['social'] = $this->main_m->getSocial($this->data['chanel_info']['id']);
        
        if($feed && method_exists($this, "feed_{$feed}")){
            $this->data['is_community'] = true;
            
            $feed_id = $this->uri->segment(4);
             
            if(!empty($feed_id)) {
              $post_type = 'text';
              if($feed == 'video_edit') $post_type = 'video';
              else if($feed == 'photo_edit') $post_type = 'photo';
              else if($feed == 'poll_edit') $post_type = 'poll';
              else if($feed == 'quiz_edit') $psot_type = 'quiz';
               
               
              $id = ltrim($feed_id, $post_type . "_");
               
              $this->data['comments'] = $this->main_m->getComments($post_type, $id, UPLOAD_URL);
            }
            
            call_user_func_array(array($this, "feed_{$feed}"), array(true));
        }
        else{
            $this->data['feeds'] = $this->main_m->getCommunityFeedList(false, false, false, $page, $this->data['page_count'], $search);
            foreach($this->data['feeds'] as &$feed){

              $feed['content_type'] = str_replace(' post', '', strtolower($feed['post_type']));
              $feed['content_id'] = trim($feed['id'], $feed['content_type'] . "_");
              
                $feed['image_path'] = $this->getCroppedThumbNail($feed['image_path'], 150, 150);
                $feed['slider_thumb'] = $this->getCroppedThumbNail($feed['slider_thumb'], 150, 150);

            }
            $this->load->view('community', $this->data);
        }
    }
   public function watermarkImage ($SourceFile, $WaterMarkText, $DestinationFile) {
       list($width, $height) = getimagesize($SourceFile);
       $image_p = imagecreatetruecolor($width, $height);
       $image = imagecreatefromjpeg($SourceFile);
       imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
       $black = imagecolorallocate($image_p, 0, 0, 0);
       $font = 'arial.ttf';
       $font_size = 10;
       imagettftext($image_p, $font_size, 0, 10, 20, $black, $font, $WaterMarkText);
       if ($DestinationFile<>'') {
          imagejpeg ($image_p, $DestinationFile, 100);
       } else {
          header('Content-Type: image/jpeg');
          imagejpeg($image_p, null, 100);
       }
       imagedestroy($image);
       imagedestroy($image_p);
  }
    /**
    * photo gallery
    */
    public function photo_gallery(){
        $this->data['selectedTags'] = $this->uri->segment(3);
        $this->data['photos'] = $this->main_m->getPhotosByTags($this->uri->segment(3));
        $this->data['tagOptions'] = $this->main_m->getPhotoTagOtions();
        foreach($this->data['photos'] as &$photo){
           

            $photo['destination'] =  UPLOAD_CLOUD."".$photo['destination'];
//            $photo['destination'] = $this->getThumbNail($photo['destination'], 262, 262, ASSETS_DIR.'/img/no-image-found.jpg');
            
        }
        
        //pagination
        
        $page = $this->uri->segment(4);//empty($this->uri->segment(4)) ? 0 : $this->uri->segment(4);
        if(empty($page)) $page = 0;
        
        $this->data['page'] = $page;
        $this->data['page_count'] = 12;
        $this->data['total_page_count'] = ceil(count($this->data['photos']) / $this->data['page_count']);
        
        $this->load->view('photo_gallery', $this->data);
    }
    public function video_gallery(){
        $this->data['selectedTags'] = $this->uri->segment(3);
        $this->data['videos'] = $this->main_m->getVideosByTags($this->uri->segment(3));
        foreach($this->data['videos'] as &$video){
            $video['image_path'] = $this->getThumbNail($video['image_path'], 262, 262, ASSETS_DIR.'/img/no-image-found.jpg');

        }
        $this->data['tagOptions'] = $this->main_m->getVideosTagOtions();
        


        $page = $this->uri->segment(4);//empty($this->uri->segment(4)) ? 0 : $this->uri->segment(4);
        if(empty($page)) $page = 0;
        
        $this->data['page'] = $page;
        $this->data['page_count'] = 12;
        $this->data['total_page_count'] = ceil(count($this->data['videos']) / $this->data['page_count']);
        
        $this->load->view('video_gallery', $this->data);
    }
    public function tour_date(){
        $this->data['searchvalue'] = $this->input->post('searchvalue');
        $this->data['selectedDate'] = $this->uri->segment(3);
        $this->data['events'] = $this->main_m->getTourdateListByDate($this->uri->segment(3), $this->data['searchvalue']);
        $this->data['dateOptions'] = $this->main_m->getEventdateOptions();

        $event = $this->uri->segment(3,'list');
//        $this->data['events'] = $this->main_m->getTourdateList();
        if($event && method_exists($this, "tourdate_{$event}"))
            call_user_func_array(array($this, "tourdate_{$event}"), array());
        else
            $this->load->view('tourdate_list', $this->data);
    }
    public function tourdate_add(){  
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'title' => $this->input->post('title'),
            'position' => $this->input->post('position'),
            'description' => $this->input->post('description'),
            'concert_date' => $this->input->post('concert_date'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'price' => $this->input->post('price'),
            'url' => $this->input->post('url')
        );
        $this->_proc_tourdate_add();
        $this->load->view('tourdate_add', $this->data);
    }
    public function _proc_tourdate_add(){
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->addTourdate($this->data['post'])){
                redirect("content/tour_date", '');
            }
        }
        return $data;
    }
    public function tourdate_edit(){  
        $id = $this->uri->segment(4);
        if(empty($id)){
            show_error("Select a tour date to edit!");
            return;
        }
        if($this->input->post('submit', false) === false){
            $post = $this->main_m->getTourdate($id);
            $this->data['post'] = $post;
            $this->data['post']['id'] = $id;
            $this->data['post']['concert_date'] = date("m/d/Y", $post['concert_time']);
            $this->data['post']['start_date'] = date("m/d/Y", $post['start_time']);
            $this->data['post']['end_date'] = date("m/d/Y", $post['end_time']);
            $this->data['post']['cid'] = $this->chanel['id'];
        }else{
            $this->data['post'] = array(
                'id' => $id,
                'cid' => $this->chanel['id'],
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'position' => $this->input->post('position'),
                'concert_date' => $this->input->post('concert_date'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'price' => $this->input->post('price'),
                'url' => $this->input->post('url')
            );
        } 
        $this->_proc_tourdate_edit();
        $this->load->view('tourdate_edit', $this->data);
    }
    public function _proc_tourdate_edit(){
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateTourdate($this->data['post'])){
                redirect("content/tour_date", '');
            }
        }
        return $data;
    }
    public function tourdate_delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("enr_event");
        
        redirect('content/tour_date', '');
    }
    public function music_player(){
        $event = $this->uri->segment(3,'list');
        $this->data['musics'] = $this->main_m->getMusicList();
        if($event && method_exists($this, "music_{$event}"))
            call_user_func_array(array($this, "music_{$event}"), array());
        else
            $this->load->view('music_list', $this->data);
    }
    public function music_add(){
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'title' => $this->input->post('title'),
            'album' => $this->input->post('album'),
            'destination' => $this->input->post('destination'),
            'music_size' => $this->input->post('music_size'),
            'url' => $this->input->post('url'),
            'url1' => $this->input->post('url1'),
            'is_publish' => $this->input->post('is_publish')
        );
        $this->_proc_music_add();
        $this->load->view('music_add', $this->data);
    }
    public function _proc_music_add(){
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->input->post("submit", false)!==false && !$_FILES['image']['size']){
                $this->data['show_errors'][] = "Please select a photo.";
            }elseif($this->main_m->addMusic($this->data['post'])){
                redirect("content/music_player", '');
            }
        }
        return $data;
    }
    public function music_edit(){  
        $id = $this->uri->segment(4);
        if(empty($id)){
            show_error("Select a song to edit!");
            return;
        }
        $post = $this->main_m->getMusic($id);
        if($this->input->post('submit', false) === false){
            $this->data['post'] = $post;
            $this->data['post']['id'] = $id;
            $this->data['post']['cid'] = $this->chanel['id'];
        }else{
            $this->data['post'] = array(
                'id' => $id,
                'cid' => $this->chanel['id'],
                'title' => $this->input->post('title'),
                'album' => $this->input->post('album'),
                'destination' => $this->input->post('destination'),
                'music_size' => $this->input->post('music_size'),
                'url' => $this->input->post('url'),
              'url1' => $this->input->post('url1'),
                'image_path' => $post['image_path'],
                'is_publish' => $this->input->post('is_publish')
            );
        } 
        $this->_proc_music_edit();
        $this->load->view('music_edit', $this->data);
    }
    public function _proc_music_edit(){
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateMusic($this->data['post'])){
                redirect("content/music_player", '');
            }
        }
        return $data;
    }
    public function music_delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("enr_music");
        
        redirect('content/music_player', '');
    }

    /**
     * Add YouTube video to content feed.
     *
     */
    public function feed_videoEmbed(){

        if ($this->chanel['is_oflo'] == 'y'){
            $this->data['viral_media_category'] = $this->main_m->get_all_oflo_categories();
        } else {
            $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        }

        $viral_media_category = $this->data['viral_media_category'];

        $i=0;
        $results = array();
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            if($this->input->post($cat_id)){
                $results[] = $cat_id;

            }
            $i++;

        }

        $is_oflo = 'n'; // Init

        // Set O-Flo flag
        if($this->session->userdata('username')=="administrator"){
            $is_oflo =  ($this->input->post('is_oflo') ? 'y' : 'n');
            $is_premium =  $this->input->post('is_premium') ? 1 : 0;
            $is_feed  =  ($this->input->post('is_feed') ? 'y' : 'n');
        } else {
            $user_id = $this->ion_auth->get_user_id();
            $user = $this->main_m->getUser($user_id); // Get User
            $is_oflo = $user[0]['is_oflo'];
            $is_premium = 0;
            $is_feed='n';
        }

        $is_oflo_slider =  ($this->input->post('is_oflo_slider') ? 'y' : 'n');

        // Persist Form values
        $this->data['cat_id']=$results;
        $this->data['facebook_synd']=$this->input->post('post_to_facebook');
        $this->data['twitter_synd']=$this->input->post('post_to_twitter');
        $this->data['tumblr_synd']=$this->input->post('post_to_tumblr');
        $this->data['is_oflo']=$this->input->post('is_oflo');
        $this->data['is_oflo_slider']=$this->input->post('is_oflo_slider');
        $this->data['is_premium']=$this->input->post('is_premium');
           $this->data['is_feed']=$this->input->post('is_feed');

        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'description' => $this->input->post('description'),
            'destination' => $this->input->post('destination'),
//            'destination' => NULL,
            'tags' => $this->input->post('tags'),
            'credit' => $this->input->post('credit', ''),
            'video_url' => $this->input->post('video_url'),
            'video_type' => $this->input->post('video_type'),
            'is_publish' => 1,
            "start_date" => strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date'),
            "feed_date" => strtotime($this->input->post('feed_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('feed_date'),
            "end_date" => strtotime($this->input->post('end_date')) == 0 ? date('Y-n-j H:i:s', time() + 3600 * 24 * 30) : $this->input->post('end_date'),
            'is_sticky' => $this->input->post('is_sticky'),
            'custom_image' => $this->input->post('video_img_value'),
            'post_to_facebook' => $this->input->post('post_to_facebook'),
            'post_to_twitter' => $this->input->post('post_to_twitter'),
            'post_to_tumblr' => $this->input->post('post_to_tumblr'),
            'is_oflo' => $is_oflo,
            'is_oflo_slider' => $is_oflo_slider,
            'is_premium' => $is_premium,
            'is_feed' =>$is_feed,
            'product_widget' =>$this->input->post('product_widget'),
        );

        $this->_proc_feed_videoEmbed();

        $this->load->view('feed_videoEmbed', $this->data);
    }

    private function _proc_feed_videoEmbed(){
        $this->form_validation->set_rules('description', 'Caption', 'trim|required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('tags','Tags', 'max_length[512]|xss_clean');
        $this->form_validation->set_rules('video_type','Embed Video Type', 'trim|required|max_length[65535]|xss_clean');
        $this->form_validation->set_rules('video_url','Embed Video URL', 'trim|required|max_length[65535]');

        if ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_YOUTUBE){
            $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_youtube_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VIMEO){
            $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vimeo_URL_check');
//        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VEVO){
//            $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vevo_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_VINE){
           $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_vine_URL_check');
        } elseif ($this->input->post('video_type') == EMBED_VIDEO_TYPE_ID_INSTAGRAM){
          $this->form_validation->set_rules('video_url', 'Embed Video URL', 'callback_instagram_URL_check');
        }

        $data = array();

        $valid_form = $this->form_validation->run();

        if ($this->input->post("submit", false) !== false) {

//            $destination = $this->input->post('destination');
            $video_url = $this->input->post('video_url');

//            if ( $valid_form && !empty($destination) ) {
            if ( $valid_form && !empty($video_url) ) {

                if($this->main_m->addFeedVideoEmbed($this->data['post'])) {

                    if (trim($this->uri->segment(2)) == 'community') {
                        redirect("content/community", '');
                    } else {
                        redirect("content/postfeed", '');
                    }
                }

//            } elseif ( empty($destination) ){
            } elseif ( empty($video_url) ){

                $this->data['show_errors'][] = "Please enter a video URL.";
            }

        }

        return $data;
    }

    /**
     * Validate embed YouTube url.
     *
     * @param $str
     * @return bool
     */
    public function youtube_URL_check($str){
        parse_str( parse_url( $str, PHP_URL_QUERY ), $end );

        if (strpos($str, 'https://www.youtube.com/watch?v=') === FALSE && !is_numeric($end['v']))
        {
            $this->form_validation->set_message('youtube_URL_check', 'The YouTube URL format must be https://www.youtube.com/watch?v=video_id');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
 /**
     * Validate embed Instagram url.
     *
     * @param $str
     * @return bool
     */
    public function instagram_URL_check($str){

        $parsed_url = parse_url( $str, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);


        if (strpos($str,'https://www.instagram.com/p/') === FALSE || !($parsed_url_arr[$count - 1]))
        {
            $this->form_validation->set_message('instagram_URL_check', 'The Instagram URL format must be https://www.instagram.com/p/ and end with video_id');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * Validate embed Vimeo url.
     *
     * @param $str
     * @return bool
     */
    public function vimeo_URL_check($str){

        $parsed_url = parse_url( $str, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);

//        echo "<BR><BR><BR>\$parsed_url: ";
//        var_dump($parsed_url);
//        echo "<BR><BR><BR>\$parsed_url_arr: ";
//        var_dump($parsed_url_arr);
//        echo "<BR><BR><BR>\$count: ";
//        var_dump($count);
//        exit;

        if (strpos($str, 'https://vimeo.com/') === FALSE || !is_numeric($parsed_url_arr[$count - 1]))
        {
            $this->form_validation->set_message('vimeo_URL_check', 'The Vimeo URL format must be https://vimeo.com/ and end with video_id');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

  /**
     * Validate embed Vine url.
     *
     * @param $str
     * @return bool
     */

       

public function vine_URL_check($str){

        $parsed_url = parse_url( $str, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);
        if (strpos($str, 'https://vine.co/v') === FALSE || !($parsed_url_arr[$count - 1]))
        {
            $this->form_validation->set_message('vine_URL_check', 'The Vine URL format must be https://vine.co/v/ and end with video_id');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }




    /**
    * shop
    * 
    */
    public function product(){
        $product = $this->uri->segment(3,'list');
        $this->data['products'] = $this->main_m->getProductList();
        foreach($this->data['products'] as &$row){
            $row['imageArr'] = unserialize($row['images']);
        }
        if($product && method_exists($this, "product_{$product}"))
            call_user_func_array(array($this, "product_{$product}"), array());
        else
            $this->load->view('product_list', $this->data);
    }
    public function product_add(){  
        $this->data['post'] = array(
            'cid' => $this->chanel['id'],
            'title' => $this->input->post('title'),
            'category' => $this->input->post('category'),
            'color' => $this->input->post('color'),
            'size' => $this->input->post('size'),
            'tags' => $this->input->post('tags'),
            'price' => $this->input->post('price'),
            'images' => $this->input->post('photos'),
            'imageArr' => $this->input->post('photos'),
            'is_publish' => $this->input->post('is_publish')
        );
        $this->data['categoryOptions'] = $this->main_m->getCategoryOptions($this->chanel['id']);
        $this->data['colorOptions'] = $this->main_m->getColorOptions($this->chanel['id']);
        $this->data['sizeOptions'] = $this->main_m->getSizeOptions($this->chanel['id']);
        $this->_proc_product_add();
        $this->load->view('product_add', $this->data);
    }

//    {"files": [
//      {
//        "name": "picture1.jpg",
//        "size": 902604,
//        "url": "http:\/\/example.org\/files\/picture1.jpg",
//        "thumbnailUrl": "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
//        "deleteUrl": "http:\/\/example.org\/files\/picture1.jpg",
//        "deleteType": "DELETE"
//      },
//      {
//        "name": "picture2.jpg",
//        "size": 841946,
//        "url": "http:\/\/example.org\/files\/picture2.jpg",
//        "thumbnailUrl": "http:\/\/example.org\/files\/thumbnail\/picture2.jpg",
//        "deleteUrl": "http:\/\/example.org\/files\/picture2.jpg",
//        "deleteType": "DELETE"
//      }
//    ]}
    public function upload_video(){
        $files = $_FILES['video'];
        $result = array();
        $file = array();
        $size = $files['size'];

        if($path = $this->main_m->uploadTempVideo()){
            $file['name'] = $files['name'];
            $file['size'] = $size;
            $file['video_size'] = $size;
            $file['video_mime'] = $files['type'];
            $file['url'] =  UPLOAD_URL.$path;
            $file['thumbnail_url'] =  UPLOAD_URL.$path;
            $file['path'] = $path;

            // ffprobe
            $cmd = "/usr/bin/ffprobe -v quiet -of flat -select_streams v:0 -show_entries stream=width,height,duration ".UPLOAD_DIR.$path." | sed -n 's/streams.stream.0.//p'";
            exec($cmd, $output, $cvh_return_var);

            $output_implode = implode("&", $output);
            $output_implode = str_replace('"', '', $output_implode);
            parse_str($output_implode, $output_parsed);

            $file['video_width']    = $output_parsed['width'];
            $file['video_height']   = $output_parsed['height'];
            $file['video_duration'] = $output_parsed['duration'];

//            error_log("\$cmd");
//            error_log($cmd);
//
//            error_log("\$output");
//            error_log($output);
//
//            error_log("\$return_var");
//            error_log($return_var);

            $result['files'][] = $file;
        }else{
            $file['name'] = $files['name'];
            $file['size'] = $size;
            $file['error'] = $this->data['show_errors'];
            $result['files'][] = $file;
        }
        echo json_encode($result);exit;
    }
    public function upload_music(){
        $files = $_FILES['music'];
        $result = array();
        $file = array();
        $size = $files['size'];
        if($path = $this->main_m->uploadTempMusic()){
            $file['name'] = $files['name'];
            $file['size'] = $size;
            $file['music_size'] = $size;
            $file['music_mime'] = $files['type'];
            $file['url'] =  UPLOAD_URL.$path;
            $file['thumbnail_url'] =  UPLOAD_URL.$path;
            $file['path'] = $path;
            $result['files'][] = $file;
        }else{
            $file['name'] = $files['name'];
            $file['size'] = $size;
            $file['error'] = $this->data['show_errors'];
            $result['files'][] = $file;
        }
        echo json_encode($result);exit;
    }
    private function _makeSizeFormat($size){
        $kbSize = round($size / 1024, 2);
        $mbSize = round($size / 1024 / 1024, 2);
        $gbSize = round($size / 1024 / 1024 / 1024, 2);
        if($mbSize < 1)
            return $kbSize.'KB';
        elseif($gbSize<1)
            return $mbSize.'MB';
        else
            return $gbSize.'GB';
    }
    public function upload_product_photo(){
        $files = $_FILES['image'];
        $result = array();
        $file = array();
        if($path = $this->main_m->uploadTempPhoto()){
            $file['name'] = $files['name'];
            $file['size'] = $files['size'];
            $file['url'] =  UPLOAD_URL.$path;
            $file['thumbnail_url'] =  UPLOAD_URL.$path;
            $file['path'] = $path;
            $result['files'][] = $file;
        }else{
            $file['name'] = $files['name'];
            $file['size'] = $files['size'];
            $file['error'] = $this->data['show_errors'];
            $result['files'][] = $file;
        }
        echo json_encode($result);exit;
    }
    public function _proc_product_add(){
        $this->form_validation->set_rules('title', 'product title', 'required|xss_clean');
        
        $data = array();
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->addProduct($this->data['post'])){
                redirect("content/product", '');
            }
        }
        return $data;
    }
    public function product_edit(){  
        $id = $this->uri->segment(4);
        if(empty($id)){
            show_error("Select a product to edit!");
            return;
        }
        if($this->input->post('submit', false) === false){
            $post = $this->main_m->getProductList($id);
            $this->data['post'] = $post;
            $this->data['post']['id'] = $id;
            $this->data['post']['imageArr'] = unserialize($post['images']);
        }else{
            $this->data['post'] = array(
                'id' => $id,
                'cid' => $this->chanel['id'],
                'title' => $this->input->post('title'),
                'category' => $this->input->post('category'),
                'color' => $this->input->post('color'),
                'size' => $this->input->post('size'),
                'tags' => $this->input->post('tags'),
                'price' => $this->input->post('price'),
                'images' => $this->input->post('photos'),
                'imageArr' => $this->input->post('photos'),
                'is_publish' => $this->input->post('is_publish')
            );
        } 
        $this->data['categoryOptions'] = $this->main_m->getCategoryOptions($this->chanel['id']);
        $this->data['colorOptions'] = $this->main_m->getColorOptions($this->chanel['id']);
        $this->data['sizeOptions'] = $this->main_m->getSizeOptions($this->chanel['id']);

        $this->_proc_product_edit();
        $this->load->view('product_edit', $this->data);
    }
    public function _proc_product_edit(){
        $this->form_validation->set_rules('title', 'product title', 'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
            if($this->main_m->updateProduct($this->data['post'])){
                redirect("content/product", '');
            }
        }
        return $data;
    }
    public function product_delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("enr_product");
        
        redirect('content/product', '');
    }
    






}