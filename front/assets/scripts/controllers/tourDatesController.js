enriqueApp.controller('tourDatesController', ['$scope', '$rootScope', '$http', function($scope,$rootScope, $http) {;
	$scope.contents = '';
    
    $scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	$http({method: 'GET', url: $rootScope.SERVER_URL + '/viewhelp/tour/' + $rootScope.CHANNEL}).
        success(function(data, status, headers, config) {
        	$('#page #help_contents').html(data);
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    }

    $scope.loadData();
}]);