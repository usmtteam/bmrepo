<?php

require(__DIR__.'/../../config/settings.php');

$servername = DB_HOST;
$username = DB_USERNAME;
$password = DB_PASSWORD;
$dbname = DB_NAME;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Oflo get message with status pending
$oflo = "SELECT * FROM platform_notification WHERE status = '0' AND platform_id='2' AND recipients ='All'";
$oflo_result = $conn->query($oflo);
	
if ($oflo_result) {

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', OFLO_PUSH_CERTIFICATE);


	while ($oflo_row = $oflo_result->fetch_assoc()) {

		$messageId = $oflo_row['id'];
		// Put your private key's passphrase here:
		$passphrase = 'overflow';

		// Put your alert message here:
		$message = $oflo_row['message'];
			
		// get token from push_not table
		$getToken = "SELECT * FROM push_not
		WHERE type = 'oflo'";
		$query = $conn->query($getToken);

		while ($row = $query->fetch_assoc()) {

			$deviceToken = $row['token'];
		
			// ////////////////////////////////////////////////////////////////////////////////

			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.sandbox.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			// We can check if an error has been returned while we are sending, but we also need to
			// check once more after we are done sending in case there was a delay with error response.
			checkAppleErrorResponse($fp);

			// Workaround to check if there were any errors during the last seconds of sending.
			// Pause for half a second.
			// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
			usleep(500000);

			checkAppleErrorResponse($fp);

			if (!$result) {

				echo 'Message not delivered' . PHP_EOL;

			} else {
				echo 'Message successfully delivered' . PHP_EOL;


				$updateStatus = "UPDATE platform_notification SET status=1 WHERE id = $messageId";

				$conn->query($updateStatus);
			}


		}

	var_dump($messageId);

	

	}
			

// Close the connection to the server
fclose($fp);

}



//OfloFollowers get message with status pending, feed content turned off
$ofloFollowers = "SELECT pn.*, enr.userid, enr.channel_id, dl.device_id, dl.login_id, dl.type, pn2.device_id AS pndevice, pn2.token  FROM platform_notification pn
        LEFT JOIN enr_follows enr ON enr.channel_id = pn.followers
        LEFT JOIN device_login dl ON dl.login_id = enr.userid
        LEFT JOIN push_not pn2 ON pn2.device_id = dl.device_id
        WHERE pn.status = '0' AND pn.platform_id='2' AND pn.feed_notif= 'n' AND pn.recipients ='Followers' AND dl.type='oflo' AND pn.date <=NOW()";
$ofloFollowers_result = $conn->query($ofloFollowers);

if ($ofloFollowers_result){

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', OFLO_PUSH_CERTIFICATE);


	while ($ofloFollowers_row = $ofloFollowers_result->fetch_assoc()) {

		$messageId = $ofloFollowers_row['id'];
		// Put your private key's passphrase here:
		$passphrase = 'overflow';

		// Put your alert message here:
		$message = $ofloFollowers_row['message'];
			
		// get token from push_not table
	// 	$getToken = "SELECT * FROM users";
	// 	$query = $conn->query($getToken);
	// // 
		// while ($ofloPro_row = $query->fetch_assoc()) 

	       {
			$deviceToken = $ofloFollowers_row['token'];
			var_dump($deviceToken);
			// ////////////////////////////////////////////////////////////////////////////////

			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			// We can check if an error has been returned while we are sending, but we also need to
			// check once more after we are done sending in case there was a delay with error response.
			checkAppleErrorResponse($fp);

			// Workaround to check if there were any errors during the last seconds of sending.
			// Pause for half a second.
			// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
			usleep(500000);

			checkAppleErrorResponse($fp);

			if (!$result) {

				echo 'Message not delivered' . PHP_EOL;

			} else {

				echo 'Message successfully delivered' . PHP_EOL;


				$updateStatus = "UPDATE platform_notification  SET status=1 WHERE id = $messageId ";

				$conn->query($updateStatus);

			}

		} 

	var_dump($messageId);

	

	}
				

	// Close the connection to the server
	fclose($fp);

}

//OfloFollowers get message with status pending, feed content turned on
$ofloFollowersFeed = "SELECT pn.*, enr.userid, enr.channel_id, dl.device_id, dl.login_id, dl.type, dl.feed_notif as dlfeednotif, pn2.device_id AS pndevice, pn2.token  FROM platform_notification pn
        LEFT JOIN enr_follows enr ON enr.channel_id = pn.followers
        LEFT JOIN device_login dl ON dl.login_id = enr.userid
        LEFT JOIN push_not pn2 ON pn2.device_id = dl.device_id
        WHERE pn.status = '0' AND pn.platform_id='2' AND pn.feed_notif ='y'
        AND pn.recipients ='Followers' AND dl.type='oflo' AND dl.feed_notif='y'
        AND pn.date <= NOW()";
$ofloFollowersFeed_result = $conn->query($ofloFollowersFeed);

if ($ofloFollowersFeed_result){

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', OFLO_PUSH_CERTIFICATE);


	while ($ofloFollowersFeed_row = $ofloFollowersFeed_result->fetch_assoc()) {

		$messageId = $ofloFollowersFeed_row['id'];
		// Put your private key's passphrase here:
		$passphrase = 'overflow';

		// Put your alert message here:
		$message = $ofloFollowersFeed_row['message'];
			
		// get token from push_not table
	// 	$getToken = "SELECT * FROM users";
	// 	$query = $conn->query($getToken);
	// // 
		// while ($ofloPro_row = $query->fetch_assoc()) 

	       {
			$deviceToken = $ofloFollowersFeed_row['token'];
			var_dump($deviceToken);
			// ////////////////////////////////////////////////////////////////////////////////

			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			// We can check if an error has been returned while we are sending, but we also need to
			// check once more after we are done sending in case there was a delay with error response.
			checkAppleErrorResponse($fp);

			// Workaround to check if there were any errors during the last seconds of sending.
			// Pause for half a second.
			// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
			usleep(500000);

			checkAppleErrorResponse($fp);

			if (!$result) {

				echo 'Message not delivered' . PHP_EOL;

			} else {

				echo 'Message successfully delivered' . PHP_EOL;


				$updateStatus = "UPDATE platform_notification  SET status=1 WHERE id = $messageId ";

				$conn->query($updateStatus);

			}

		} 

	var_dump($messageId);

	

	}
				

	// Close the connection to the server
	fclose($fp);

}



//OfloPro get message with status pending
$ofloPro = "SELECT pn.*, uo.cid, uo.device_id, uo.type, uo.oflo_type FROM platform_notification pn 
        LEFT JOIN users uo ON uo.cid = pn.channel_recipients
        WHERE pn.status = '0' AND pn.platform_id='3' AND uo.type ='oflopro' OR uo.oflo_type ='1'";
$ofloPro_result = $conn->query($ofloPro);

if ($ofloPro_result){

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', OFLO_PRO_PUSH_CERTIFICATE);


	while ($ofloPro_row = $ofloPro_result->fetch_assoc()) {

		$messageId = $ofloPro_row['id'];
		// Put your private key's passphrase here:
		$passphrase = 'overflow';

		// Put your alert message here:
		$message = $ofloPro_row['message'];
			
		// get token from push_not table
	// 	$getToken = "SELECT * FROM users";
	// 	$query = $conn->query($getToken);
	// // 
		// while ($ofloPro_row = $query->fetch_assoc()) 

	       {
			$deviceToken = $ofloPro_row['device_id'];
			var_dump($deviceToken);
			// ////////////////////////////////////////////////////////////////////////////////

			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			// We can check if an error has been returned while we are sending, but we also need to
			// check once more after we are done sending in case there was a delay with error response.
			checkAppleErrorResponse($fp);

			// Workaround to check if there were any errors during the last seconds of sending.
			// Pause for half a second.
			// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
			usleep(500000);

			checkAppleErrorResponse($fp);

			if (!$result) {

				echo 'Message not delivered' . PHP_EOL;

			} else {

				echo 'Message successfully delivered' . PHP_EOL;


				$updateStatus = "UPDATE platform_notification  SET status=1 WHERE id = $messageId ";

				$conn->query($updateStatus);

			}

		} 

	var_dump($messageId);

	

	}
				

	// Close the connection to the server
	fclose($fp);

}

//ProFlo get message with status pending
$ProFlo = "SELECT pn.*, uo.cid, uo.device_id , uo.type, uo.oflo_type FROM platform_notification pn 
        LEFT JOIN users uo ON uo.cid = pn.channel_recipients
        WHERE pn.status = '0' AND pn.platform_id='4' AND uo.type ='proflo' OR uo.oflo_type='2'  AND pn.date <= NOW()";
$ProFlo_result = $conn->query($ProFlo);

if ($ProFlo_result){

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', PROFLO_PUSH_CERTIFICATE);


	while ($ProFlo_row = $ProFlo_result->fetch_assoc()) {

		$messageId = $ProFlo_row['id'];
		// Put your private key's passphrase here:
		$passphrase = 'overflow';

		// Put your alert message here:
		$message = $ProFlo_row['message'];
			
		// get token from push_not table
	// 	$getToken = "SELECT * FROM users";
	// 	$query = $conn->query($getToken);
	// // 
		// while ($ofloPro_row = $query->fetch_assoc()) 

	       {
			$deviceToken = $ProFlo_row['device_id'];
			var_dump($deviceToken);
			// ////////////////////////////////////////////////////////////////////////////////

			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			// We can check if an error has been returned while we are sending, but we also need to
			// check once more after we are done sending in case there was a delay with error response.
			checkAppleErrorResponse($fp);

			// Workaround to check if there were any errors during the last seconds of sending.
			// Pause for half a second.
			// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
			usleep(500000);

   			checkAppleErrorResponse($fp);

  		    if (!$result) {
				echo 'Message not delivered' . PHP_EOL;
			} else {
				echo 'Message successfully delivered' . PHP_EOL;


				$updateStatus = "UPDATE platform_notification  SET status=1 WHERE id = $messageId ";

	         	$conn->query($updateStatus);
			}

		} 

	var_dump($messageId);

	

	}
				

	// Close the connection to the server
	fclose($fp);

}


mysqli_close($conn);

// FUNCTION to check if there is an error response from Apple
// Returns TRUE if there was and FALSE if there was not
function checkAppleErrorResponse($fp) {

//byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID).
// Should return nothing if OK.

//NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait
// forever when there is no response to be sent.

	stream_set_blocking($fp, 0);

	$apple_error_response = fread($fp, 6);

	if ($apple_error_response) {

// unpack the error response (first byte 'command" should always be 8)
		$error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

		if ($error_response['status_code'] == '0') {
			$error_response['status_code'] = '0-No errors encountered';

		} else if ($error_response['status_code'] == '1') {
			$error_response['status_code'] = '1-Processing error';

		} else if ($error_response['status_code'] == '2') {
			$error_response['status_code'] = '2-Missing device token';

		} else if ($error_response['status_code'] == '3') {
			$error_response['status_code'] = '3-Missing topic';

		} else if ($error_response['status_code'] == '4') {
			$error_response['status_code'] = '4-Missing payload';

		} else if ($error_response['status_code'] == '5') {
			$error_response['status_code'] = '5-Invalid token size';

		} else if ($error_response['status_code'] == '6') {
			$error_response['status_code'] = '6-Invalid topic size';

		} else if ($error_response['status_code'] == '7') {
			$error_response['status_code'] = '7-Invalid payload size';

		} else if ($error_response['status_code'] == '8') {
			$error_response['status_code'] = '8-Invalid token';

		} else if ($error_response['status_code'] == '255') {
			$error_response['status_code'] = '255-None (unknown)';

		} else {
			$error_response['status_code'] = $error_response['status_code'].'-Not listed';

		}

		echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';

		echo 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';

		return true;
	}

	return false;
}
