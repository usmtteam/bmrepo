<?php
class Reports_Master extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


		$this->load->helper('url');
		$this->load->helper('csv');
	 	$this->load->helper('download');
	
		$this->load->helper('language');
		$this->load->model('reports_master_m');
        $this->load->model('main_m');
	
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

	}
	
  function _remap($method) {
        
        $this->{$method}();
      
       
    }

	function index(){
		  
		  $this->load->view('header_n');

      $this->load->view('sidebar_n');
     

      $this->data['channel_names'] = $this->reports_master_m->get_channel_names();
      
       $this->load->view('reports_list', $this->data);

      $this->load->view('footer_v');
    }

    function duration(){
      
      $this->load->view('header_n');

      $this->load->view('sidebar_n');
     

      $this->data['channel_names'] = $this->reports_master_m->get_channel_names();
      
       $this->load->view('reports_duration', $this->data);

      $this->load->view('footer_v');
    }


    function result(){

    	$id = $this->input->post('channel_all');

		  $month_input =$this->input->post('over_all');


    	if($this->input->post('submit') == "Submit") { 

    	$this->load->view('header_n');
        $this->load->view('sidebar_n');
        

    	 $this->data['channel_names'] = $this->reports_master_m->get_channel_names();
		 
		 $channel = $this->reports_master_m->get_channel_name_by_id($id);
	
		 $this->data['channel']= $channel[0]['name']; 
		 

		 // $this->data['oflo_montly'] = $this->reports_master_m->get_oflo_reports_monthly($id);

		 // var_dump($this->data['oflo_montly']); exit;

		 $report_by_channel = $this->reports_master_m->get_reports_by_id($id, $month_input);

         $this->data['report_by_channel'] = $report_by_channel;

          $views = 0;
          $revenue = 0;


	      foreach($report_by_channel as $report){

              $date = date_parse($report['month']);
              $month_number = $date['month'];

              $timeStamp_number = date( 'm', $report['time_stamp'] );

	          	
	          	$views += $report['total_views'];
	          	// $revenue += $report['total_revenue'];

        
	       }

	      $revenue = $views/1000*3/2; 

          $this->data['views']= $views;
          $this->data['revenue']= $revenue;
          $this->data['month_match']= $month_input;
          $this->data['channel_id']= $id;	  

          // var_dump($this->data['report_by_id']); exit;

           $this->load->view('reports_list', $this->data);
           $this->load->view('footer_v');
   
		} else {

			$this->download_csv($id,$month_input);
		    
		}
	  	

    }


function result_duration(){

      $id = $this->input->post('channel_all');

      $month_input =$this->input->post('over_all');
  
  if($this->input->post('submit') == "Update"){

    $duration_is = $this->input->post('duration_is');
       $video_id = $this->input->post('video_id');
       $cid = $this->input->post('cid');
       $month_input =$this->input->post('month_m');

       $str_time = $duration_is;

      $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

      sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

      $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

      

       $update_Duration = $this->reports_master_m->updateDuration($video_id, $time_seconds);


    $this->load->view('header_n');
    $this->load->view('sidebar_n');
    

   $this->data['channel_names'] = $this->reports_master_m->get_channel_names();
 
 $channel = $this->reports_master_m->get_channel_name_by_id($cid);

 $this->data['channel']= $channel[0]['name']; 
 

 $report_by_channel = $this->reports_master_m->get_duration_by_id($cid);

     $this->data['report_by_channel'] = $report_by_channel;

      $duration= 0; 
      $revenue = 0;

  $date = date_parse($month_input);
  $month_number = $date['month'];

// var_dump($id);
// var_dump($report_by_channel); exit;

    foreach($report_by_channel as $report){

        $timeStamp_number = date( 'm', $report['time_stamp'] );

        if ($month_input == 'all'){ 
          $duration += $report['total_duration'];
        }else{
          if($month_number == $timeStamp_number){
           $duration += $report['total_duration'];
          }
        }
           
     }

    $revenue = $duration*3/2; 

    
      $this->data['duration']= $duration;
      $this->data['revenue']= $revenue;
      $this->data['month_match']= $month_input;
      $this->data['channel_id']= $cid;   

      // var_dump($this->data['report_by_id']); exit;

       $this->load->view('reports_duration', $this->data);
       $this->load->view('footer_v');

  }

  if($this->input->post('submit') == "Submit") { 

  $this->load->view('header_n');
    $this->load->view('sidebar_n');
    

   $this->data['channel_names'] = $this->reports_master_m->get_channel_names();
 
 $channel = $this->reports_master_m->get_channel_name_by_id($id);

 $this->data['channel']= $channel[0]['name']; 
 

 $report_by_channel = $this->reports_master_m->get_duration_by_id($id);

     $this->data['report_by_channel'] = $report_by_channel;

      $duration= 0; 
      $revenue = 0;

  $date = date_parse($month_input);
  $month_number = $date['month'];

// var_dump($id);
// var_dump($report_by_channel); exit;

    foreach($report_by_channel as $report){

        $timeStamp_number = date( 'm', $report['time_stamp'] );

        if ($month_input == 'all'){ 
          $duration += $report['total_duration'];
        }else{
          if($month_number == $timeStamp_number){
           $duration += $report['total_duration'];
          }
        }
           
     }

    $revenue = $duration*3/2; 

    
      $this->data['duration']= $duration;
      $this->data['revenue']= $revenue;
      $this->data['month_match']= $month_input;
      $this->data['channel_id']= $id;   

      // var_dump($this->data['report_by_id']); exit;

       $this->load->view('reports_duration', $this->data);
       $this->load->view('footer_v');
   
    } 

  if($this->input->post('submit') == "Download"){

      $this->download_csv($id,$month_input);
  }

      

  }

  function update_Duration(){

     
       

    }

  function download_csv($id, $month_input){


    	setlocale(LC_MONETARY, 'en_US.UTF-8');
		 
		$channel = $this->reports_master_m->get_channel_name_by_id($id);

	
		$channel_name = $channel[0]['name'];


	    $csvHeader = array('Contents', 'Duration', 'Views', 'Revenue');


	    $filename = $month_input.$channel_name;


    	header("Pragma: public");
    	header("Expires: 0");
    	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    	header('Content-type: text/csv');
    	header("Content-Disposition: attachment; filename=\"$filename.csv\";" );

    	echo(implode(',', $csvHeader))."\r\n";

	    $report_by_channel = $this->reports_master_m->get_reports_by_id($id, $month_input);

	   


          $duration= 0; 
          $views = 0;
          $revenue = 0;

          foreach($report_by_channel as $report){

	          	 $duration += $report['total_duration'];
	          	 $views += $report['total_views'];
	          	
	          }

          $revenue = $views/1000*3/2; 
	      
          $csvDuration = gmdate("H:i:s", $duration);
          $csvViews = $views;
          $csvRevenue = money_format('%.2n',$revenue) . "\r\n";

            $csvTotal = array('Total', $csvDuration, $csvViews, $csvRevenue);

          echo(implode(',', $csvTotal));

         
       
          
          foreach ($report_by_channel as $report) {

          	if($month_input == 'by_months'){
          		$description = strtoupper($report['month']);

          	}else{

          		if (!$report['description']){
                    $description= "Deleted Content";
                }else {
                 	$description =  preg_replace('/[.,]/', '', $report['description']);
                 	
                 	$description = substr($description,0,50);
                }
            }

            $duration = gmdate("H:i:s", $report['total_duration']);

        	$views = $report['total_views'];
          $total_revenue = $report['total_views']/1000*3/2;

        	$revenue = money_format('%.2n',$total_revenue) . "\n";

        	$csvAll = array($description, $duration, $views, $revenue);

        	echo(implode(',', $csvAll));
          }

        
       
       
       return;
 
             
	     
	}

}
	