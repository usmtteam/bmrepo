<?php
class Payment extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('main_m');
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();
	}
	
	public function index() {

		$this->load->library('paypal');
		
		$success = $this->input->get('success', '');
		if($success == 'true') {

			$user_id = $this->input->get('user_id');
			$channel_id = $this->input->get('channel_id');
			
			$row = $this->main_m->db->query('select * from enr_paypal where cid = ? and user_id = ?', array($channel_id, $user_id))->result_array();

			if(count($row) > 0) {
				
				
				$row = $row[0];
				

				$this->main_m->db->query('delete from enr_paypal where id = ?', array($row['id']));

				$info = array(
						'payer_id' => $this->input->get('PayerID'),
						'payment_id' => $row['payment_id']
				);
				$result = $this->paypal->executePaypalPayment($info);

				if($result) {

					$fan = $this->main_m->getFan($user_id);
					
					//updates credits
					$credits = $fan['credit'] + $row['credits'];
					
					$query = 'update enr_fan set credit = ? where id = ?';
					$this->main_m->db->query($query, array($credits, $user_id));
				}
				
			}
			
		}
		$channel = $this->main_m->get_chanel($channel_id);
		if(MODE_DEV) {
			$url = rtrim(site_url(), 'index.php') . $channel['url'] . '/#settings';
		}
		else {
			$url = $channel . '.starsite.com' . '/#settings';
		}
		
		header('location: ' . $url);
	}
}