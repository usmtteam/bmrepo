<?


	define('VT_PUSH_SANDBOX_ENDPOINT','ssl://gateway.sandbox.push.apple.com:2195');
	define('VT_PUSH_PRODUCTION_ENDPOINT','ssl://gateway.push.apple.com:2195');


	define('VT_PUSH_SANDBOX_FEEDBACK_ENDPOINT','ssl://gateway.sandbox.push.apple.com:2196');
	define('VT_PUSH_PRODUCTION_FEEDBACK_ENDPOINT','ssl://gateway.push.apple.com:2196');


	
	class VTPush{

		private $_aDevices;
		private $_bDevMode;
		private $_aPayload;
		private $_sCert;
		private $_sCertsDir;
		private $_sMessage;
		private $_nBadgeNumber;
		private $_sSound;
		private $_sExtraData;

		function __construct(){
			$this->_aPayload = array();
			$this->_aPayload['aps'] = array();
			$this->_sCertsDir = __DIR__.'/certs/';
			
		}

		public function storeP12($p_sDomain,$p_xFile){

			$l_sAccountFileP12 = $p_sDomain.'.p12';
			$l_sAccountFilePEM = $p_sDomain.'.pem';

			if($p_xFile){
				
				move_uploaded_file($p_xFile['tmp_name'], $this->_sCertsDir.$l_sAccountFileP12) or die('cannot move');
				// $l_sShell = 'sudo openssl pkcs12 -in '.$this->_sCertsDir.$l_sAccountFileP12.' -out '.$this->_sCertsDir.$l_sAccountFilePEM.' -nodes -clcerts';
				$l_sShell = 'openssl pkcs12 -in '.$this->_sCertsDir.$l_sAccountFileP12.' -out '.$this->_sCertsDir.$l_sAccountFilePEM.' -nodes -clcerts';
							// $ openssl pkcs12 -in cert.p12 -out apple_push_notification_production.pem -nodes -clcerts
				echo "COPY/PASTE:\n<br>\n";
				echo $l_sShell;
				echo "\n";
				shell_exec($l_sShell);				
			}
		}



		public function setDeviceIds($p_aDevices){
			$this->_aDevices = $p_aDevices;
		}

		public function setMessage($p_sMessage){
			// $this->_aPayload['aps']['alert']['body'] = $p_sMessage;
			$this->_aPayload['aps']['alert'] = $p_sMessage;
			$this->_sMessage = $p_sMessage;
		}

		public function setBadge($p_nNumber){
			$this->_aPayload['aps']['badge'] = $p_nNumber;
			$this->_nBadgeNumber = intval($p_nNumber);
		}

		public function setCategory($p_sVal){
			$this->_aPayload['aps']['category'] = $p_sVal;
		}
		public function setExtra($p_sVal){
			$this->_aPayload['aps']['extra'] = $p_sVal;
			$this->_sExtraData = $p_sVal;
		}

		public function setCert($p_sStr){
			$this->_sCert = $p_sStr.'.pem';
		}

		public function setSound($p_sSound = 'default'){
			$this->_aPayload['aps']['sound'] = $p_sSound;
			$this->_sSound = $p_sSound;
		}



		public function checkFeedback(){


			$payload = json_encode($this->_aPayload);			
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $this->_sCertsDir.$this->_sCert);
			$fp = stream_socket_client(VT_PUSH_PRODUCTION_FEEDBACK_ENDPOINT, $err, $errstr, 5, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);


			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS FEEDBACK' . PHP_EOL;

			$feedback_tokens = array();

			while(!feof($fp)) {
				$data = fread($fp, 38);
				if(strlen($data)) {
				$feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
				}
			}
			fclose($fp);

			print_r($feedback_tokens);
			echo "DONE";

		}

		public function pushOut(){
			// $this->sendGCMPush();
			
			if(!file_exists($this->_sCertsDir.$this->_sCert)){
				echo "no cert";
				return;
			}
// 	

			spl_autoload_unregister('coreAutoload');
            require_once __DIR__.'/../ApnsPHP/Autoload.php';
            if(preg_match("/\-dev/", $this->_sCert)){
				$push = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,$this->_sCertsDir.$this->_sCert);
            }else{
            	$push = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,$this->_sCertsDir.$this->_sCert);
            }

			

			// Set the Root Certificate Autority to verify the Apple remote peer
			// $push->setRootCertificationAuthority('entrust_root_certification_authority.pem');

			// Increase write interval to 100ms (default value is 10ms).
			// This is an example value, the 10ms default value is OK in most cases.
			// To speed up the sending operations, use Zero as parameter but
			// some messages may be lost.
			// $push->setWriteInterval(100 * 1000);

			// Connect to the Apple Push Notification Service
			$push->connect();

			foreach($this->_aDevices as $l_nCount => $l_sDeviceToken){
				// Instantiate a new Message with a single recipient
				$message = new ApnsPHP_Message($l_sDeviceToken);
				$message->setCustomIdentifier(sprintf("Message-Badge-%09d", $l_nCount));
				
				
				// $message->setBadge($i);
				$message->setText($this->_sMessage);
				
				if($this->_nBadgeNumber != NULL)
					$message->setBadge($this->_nBadgeNumber);
				
				if($this->_sExtraData != NULL)
					$message->setCustomProperty('EXTRA',$this->_sExtraData);

				$message->setSound($this->_sSound);
				// print_r($message->getPayload());
				$push->add($message);
			}
			$push->send();
			$push->disconnect();

			// Examine the error message container
			$aErrorQueue = $push->getErrors();
			if (!empty($aErrorQueue)) {
				var_dump($aErrorQueue);
			}
			spl_autoload_register('coreAutoload');

		}



		public function sendGCMPush(){
				
        
        		$l_nDeviceBreak = 500;

		        $url = 'https://android.googleapis.com/gcm/send';
				// $message = array("message" => "Test Message");

				$message = array("title" => $this->_sMessage,'pushType' => 'SHOW');
 				$l_aChunks = array_chunk($this->_aDevices,$l_nDeviceBreak);

 				// print_R($l_aChunks);

				// echo "NUMBER OF DEVICES =---->".count($this->_aDevices);


				foreach($l_aChunks as $l_xRow){

					 $fields = array(
		            	'registration_ids' => $l_xRow,
		            	'data' => $message,
		        	);
					// print_R($fields);
					// sleep(1);
					// continue;
		        
		        	$headers = array(
		            	'Authorization: key=' . 'A_7is',
		            	'Content-Type: application/json'
		        	);
		        
		        	$ch = curl_init();
		 
			        curl_setopt($ch, CURLOPT_URL, $url);
			        curl_setopt($ch, CURLOPT_POST, true);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			 
			        // Disabling SSL Certificate support temporarly
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			 
			        // Execute post
			        $result = curl_exec($ch);
			        if ($result === FALSE) {
			            echo ('Curl failed: ' . curl_error($ch));
			        }
			 
			        // Close connection
			        curl_close($ch);
			        echo $result;

				}
		       
		}



	}

?>