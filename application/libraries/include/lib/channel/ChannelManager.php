<?php


	class ChannelManager extends FrontCoreManager{

        private $_xSql;
        private $_nCacheObjectDuration = 4;


		public function init(){
            
			$this->_xSql = AppController::getInstance()->getConnection();

		}

        public function getById($p_nId,$p_bBypassMemcache = false){

            return $this->getFromMemcacheAndStoreIfFail($p_nId,$p_bBypassMemcache);
            
        }

        public function getSocialTokensByChannelId($p_nId){
            
            $l_sSql = 'SELECT * FROM enr_channel_oauth WHERE channel_id = '.intval($p_nId);

            return $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));
            
        }

         public function setChannelInMemcacheBySlugAndReturn($p_sSlug){

            $l_sSlug = strtolower($p_sSlug);
            $l_sMemcacheKey = Memcaching::makeKey($l_sSlug);       
            
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT * FROM enr_chanel WHERE lower(url) = "'.$l_sSlug.'"';
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            if(!$l_aData)
                return null;
            $cid = $l_aData['id'];
            $l_dSql = 'SELECT * FROM enr_design WHERE cid = "'.$cid.'"';
            $l_dData = $l_xSql->getRow($l_xSql->runQuery($l_dSql)); 

            $l_xChannel = new stdClass();
            $l_xChannel->cid = $l_aData['id'];
            $l_xChannel->site_title = ($l_aData['title']) ? $l_aData['title'] : $l_aData['name'];
            $l_xChannel->site_header_title = $l_aData['name'];
            $l_xChannel->google_analytics_key = ($l_aData['google_analytics_key']) ? $l_aData['google_analytics_key'] : 'UA-46624794-23';
            $l_xChannel->url = $l_aData['url'];
            $l_xChannel->domain = ($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->slug = ($l_aData['slug']) ? $l_aData['slug'] : $l_aData['url'];
            $l_xChannel->cms = 'local.cms.'.($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->domain_cookie = '*.'.($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->starstats_site_id = $l_aData['starstats_site_id'];
            $l_xChannel->quantserve_key = ($l_aData['quantserve_key']) ? $l_aData['quantserve_key'] : QUANTSERVE_KEY;
            $l_xChannel->bc_playerid_web = ($l_aData['bc_playerid_web']) ? $l_aData['bc_playerid_web'] : BRIGHTCOVE_WEB;
            $l_xChannel->brightcove_key = ($l_aData['brightcove_key']) ? $l_aData['brightcove_key'] : BRIGHTCOVE_KEY;
            // $l_xChannel->adURL = 'http://ad4.liverail.com/?LR_PUBLISHER_ID=63753&LR_SCHEMA=vast2-vpaid&LR_AUTOPLAY=1&LR_TITLE=starclub&LR_VIDEO_ID=starclub&LR_URL=starclub test.starsite.com';
            $l_xChannel->adURL = ($l_aData['ad_server_url_web']) ? $l_aData['ad_server_url_web'] : AD_SERVER_URL;
            $l_xChannel->topAdUnit = GLOBAL_UNIT_LEADERBOARD;
            $l_xChannel->rightTopAdUnit = GLOBAL_UNIT_300x200;
            $l_xChannel->rightBottomAdUnit = GLOBAL_UNIT_SKYSCRAPER;
            $l_xChannel->mobileAdUnit = GLOBAL_UNIT_320x50;
            $l_xChannel->mobileSecondAdUnit = GLOBAL_UNIT_2_320x50;
            $l_xChannel->mobileLargeAdUnit = GLOBAL_UNIT_300x100;
            $l_xChannel->linkColor = ($l_dData['link_color']) ? $l_dData['link_color'] : '000000';
            $l_xChannel->buttonColor = ($l_dData['button_color']) ? $l_dData['button_color'] : '000000';
            $l_xChannel->backColor = ($l_dData['back_color']) ? $l_dData['back_color'] : 'eaeaea';
            $l_xChannel->textColor = ($l_dData['text_color']) ? $l_dData['text_color'] : '000000';
            $l_xChannel->headerImage = $l_dData['header_image'];
            $l_xChannel->backgroundImage = $l_dData['background_image'];
            $l_xChannel->raw = $l_aData;
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_xChannel,MEMCACHE_DURATION_CHANNEL);
            return $l_xChannel;
        }

            // MEMCACHING


        public function getFromMemcacheAndStoreIfFail($p_nId,$p_bBypassMemcache = false){
           
            $l_sMemcacheKey = Memcaching::makeKey(ItemManager::returnItemKey($p_nId));
            $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);


            if($p_bBypassMemcache || !$l_aCachedContent){
                $l_sSql = 'SELECT * FROM enr_chanel WHERE id = '.intval($p_nId);
                $l_aData = $this->_xSql->getRow($this->_xSql->runQuery($l_sSql));
                $l_aData = new Channel($l_aData);
                Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);
                return $l_aData;
            }else{
                return $l_aCachedContent['value'];
            }  
        }


        // STATIC
        
        public static function returnItemKey($p_nId){
            return 'channel-object-'.$p_nId;
        }
    } 

?>