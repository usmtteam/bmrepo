<?



	class SCReports{

		var $_xSQLManager;

		function __construct(){

		}

		public function processRequest(){

            

			if(!$_GET['p_sAction']){
				exit;
			}else{
				$l_sAction = $_GET['p_sAction'];
				$p_aArgs = $_POST;
			}
			
			$p_aArgs = $_POST;//unserialize(base64_decode($_POST['p_sArgs']));
            
			$l_sAction = 'rq_'.$l_sAction;
			if(method_exists($this,$l_sAction)){
				require_once(__DIR__.'/../config/settings.php');
				$this->_xSQLManager = new FrontCoreSql();
				$this->{$l_sAction}($p_aArgs);
			}

		}


        
        public function showOverview($p_sCreator){
            require_once(__DIR__.'/../config/settings.php');
            $this->_xSQLManager = new FrontCoreSql();

            
            // $l_xChannelManager = new ChannelManager();
            // $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            //IF legacy - then use old Databases
            
            if($p_sCreator == 'TYRESE'){
              $l_sCreatorName = 'Tyrese Gibson';
              $DB_OVERRIDE = array();

              $l_nChannelId = 1;
              $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
              $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
              $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
              $DB_OVERRIDE['DB_NAME'] = 'sc_tyrese';
            }else{
              $l_sCreatorName = 'Enrique Iglesias';
              $l_nChannelId = 7;
              $DB_OVERRIDE = array();
              $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
              $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
              $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
              $DB_OVERRIDE['DB_NAME'] = 'scdevteam';
            }

            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_nQueryChannelId = $l_nChannelId;
            // $l_nQueryChannelId = 1;

              $l_sSql = 'SELECT 
                        CASE WHEN posted >= "'.date('Y-m-d',strtotime(date('Y-m-d').' -7 days')).'" THEN 1 
                        END 
                        as weekly_rate,
                        f.*,v.*,bc.*,ss.*,fbm.* FROM 
                        enr_feed f
                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN enr_brightcove_video_metrics bc 
                        ON bc.video_id = v.brightcove_media_id

                        JOIN ss_social_tracking_details ss 
                        ON ss.content_id = f.content_id

                        JOIN enr_fbposts_key_metrics fbm 
                        ON fbm.post_id = ss.post_id

                        WHERE 
                        f.post_type = "video"
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY posted DESC';
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            $l_nTotalVideoViews = 0;
            $l_nTotalImpressions = 0;
            $l_nTotalReach = 0;
            foreach($l_xData as $l_xRow){
                 
                $l_nTotalVideoViews += $l_xRow['video_view'];
                $l_nTotalImpressions += $l_xRow['impressions'];
                $l_nTotalReach += $l_xRow['reach'];
            }
            

            
            ?>

            <style type="text/css">
            *{ margin:0; padding:0; outline:none}
            body{ font-family: 'Arial'; font-size:14px}
            table td{ font-family: 'Arial'; font-size:14px}
            .content{padding:50px}
            .sc-table{ margin:40px 0 }
            .sc-table td{ border:1px solid #ccc; padding:10px}
            .sc-table tr.h{ font-weight: normal; background:#222; color:#fff}
            .non{ }
            h2{ font-size:24px}
            h3{ font-size:20px; font-weight: normal;}
            .expand .expand-inner{display: none;}
            .sc-table-lrg td{ font-size:18px}
            </style>
            <div class="content">
            <h2><?= $l_sCreatorName ?></h2>
            
            
            <br /><br /><br />
            <h3>REPORT as of End of Feb 2015</h3>


            <table border="0" cellpadding="0" cellspacing="0" class="sc-table sc-table-lrg">
                <tr class="h">
                    <td>Available Visitors / Reach</td>
                    <td>Active Users / Day</td>
                    <td>Active Users / Month</td>
                    <td>Video Views</td>
                    <td>Impressions</td>
                    <td>Downloads App</td>
                </tr>
                <tr>
                  <? if($p_sCreator == 'TYRESE'){ ?>
                    <td class="non">22,663,916</td>
                    <td class="non">83,741</td>
                    <td class="non">1,004,883</td>
                    <td><?= number_format($l_nTotalVideoViews,0) ?></td>
                    <td><?= number_format($l_nTotalImpressions,0) ?></td>
                    <td class="non">53,660</td>
                    <? } else { ?>
                    <td class="non">56,599,431</td>
                    <td class="non">30,369</td>
                    <td class="non">393,381</td>
                    <td><?= number_format($l_nTotalVideoViews,0) ?></td>
                    <td><?= number_format($l_nTotalImpressions,0) ?></td>
                    <td class="non">98,388</td>
                    <?
                  } ?>

                    
                </tr>
            </table>


            <table border="0" cellpadding="0" cellspacing="0" class="sc-table sc-table-lrg">
                <tr class="h">
                    <td>Channel Revenue</td>
                    <td>Marketing Costs</td>
                    <td>Cost for Content</td>
                    <td>Salary</td>
                    <td>Advances</td>
                    <td>Net Account Position</td>
                    <td>Talent Net Revenue Distributions</td>
                    
                </tr>
                <tr>
                  <? if($p_sCreator == 'TYRESE'){ ?>


                    
                    <td>5,724

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>Type</td>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>Advertising - Banner Ads</td>
                                        <td>628</td>
                                        <td>2,585</td>
                                        <td>2,511</td>
                                        <td>5,724</td>
                                    </tr>
                                    <tr>
                                        <td>Advertising - Pre-Roll Ads</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Merchandising</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Other (Adjustment)</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>628</td>
                                        <td>2,585</td>
                                        <td>2,511</td>
                                        <td>5,724</td>
                                    </tr>



                                    
                                </table>
                            </div>
                        </div>


                    </td>

                    <td>405,927

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>Type</td>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>Marketing</td>
                                        <td>263,856</td>
                                        <td>29,101</td>
                                        <td>48,325</td>
                                        <td>341,282</td>
                                    </tr>
                                    <tr>
                                        <td>Web Hosting</td>
                                        <td>26,969</td>
                                        <td>1,779</td>
                                        <td>897</td>
                                        <td>29,645</td>
                                    </tr>
                                    <tr>
                                        <td>Set-up Fee</td>
                                        <td>35,000</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>35,000</td>
                                    </tr>
                                    <tr>
                                        <td><em>Total</em></td>
                                        <td><em>325,825</em></td>
                                        <td><em>30,880</em></td>
                                        <td><em>49,222</em></td>
                                        <td><em>405,927</em></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>           


 


         
                    <td>10,750

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>10,750</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>10,750</td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>


                    </td>
                    <td>66,666</td>
                    <td>200,000</td>
                    <td>-677,619</td>




                    <td>314</td>



                    



                    <? } else { ?>

                     <td>158,555

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>Type</td>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>Advertising - Banner Ads</td>
                                        <td>1,076</td>
                                        <td>484</td>
                                        <td>380</td>
                                        <td>1940</td>
                                    </tr>
                                    <tr>
                                        <td>Advertising - Pre-Roll Ads</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Merchandising</td>
                                        <td>11,664</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>11,664</td>
                                    </tr>
                                    <tr>
                                        <td>Other (Adjustment)</td>
                                        <td>144,951</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>144,951</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>157,691</td>
                                        <td>484</td>
                                        <td>380</td>
                                        <td>158,555</td>

                                    </tr>

                                    
                                </table>
                            </div>
                        </div>


                    </td>

                    <td>1,444,717

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>Type</td>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>Marketing</td>
                                        <td>1,234,579</td>
                                        <td>19,269</td>
                                        <td>24,992</td>
                                        <td>1,278,840</td>
                                    </tr>
                                    <tr>
                                        <td>Web Hosting</td>
                                        <td>128,201</td>
                                        <td>1,779</td>
                                        <td>897</td>
                                        <td>130,877</td>
                                    </tr>
                                    <tr>
                                        <td>Set-up Fee</td>
                                        <td>35,000</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><em>Total</em></td>
                                        <td><em>1,397,780</em></td>
                                        <td><em>21,048</em></td>
                                        <td><em>25,889</em></td>
                                        <td><em>1,444,717</em></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>                    
                    <td>196,703

                        <div class="expand">
                            <a href="#" class="details">Details</a>
                            <div class="expand-inner">
                                
                                <table>
                                    <tr>
                                        <td>2014</td>
                                        <td>Jan</td>
                                        <td>Feb</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>190,769</td>
                                        <td>4,359</td>
                                        <td>1,575</td>
                                        <td>196,703</td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>


                    </td>
                    <td>0</td>
                    <td>750,000</td>
                    <td>-$2,232,865</td>




                    <td>-</td>
                   
                    <?
                  } ?>

                    
                </tr>
            </table>


 
           <div style="display:absolute; margin:0 0 -30px 0">Total Videos: <?= count($l_xData) ?></div>

            <table border="0" cellpadding="0" cellspacing="0" class="sc-table">
            <tr class="h">
                <td>Type</td>
                <td>Posted</td>
                <td>Video Views</td>
                <td>Likes</td>
                <td>Comments</td>
                <td>Shares</td>
                <td>Reach</td>
                <td>Impressions</td>
                <td>Engagement</td>
                <!-- 
                <td>Ad Revenue</td>
                <td>Ad Revenue per video</td>
                <td>Other Forms of Revenue</td> -->
                
            
             </tr>
             <?
             foreach($l_xData as $l_xRow){
                ?>
                <tr>
                <td><?= $l_xRow['description'] ?></td>
                <td><?= date('m/d/Y',strtotime($l_xRow['posted'])) ?></td>
                <td><?= number_format($l_xRow['video_view'],0) ?></td>
                <td><?= number_format($l_xRow['like'],0) ?></td>
                <td><?= number_format($l_xRow['comment'],0) ?></td>
                <td><?= number_format($l_xRow['share'],0) ?></td>
                <td><?= number_format($l_xRow['reach'],0) ?></td>
                <td><?= number_format($l_xRow['impressions'],0) ?></td>
                <td><?= number_format($l_xRow['engagement'],0) ?></td>
<!-- 
                 <td class="non">In Progress</td>
                <td class="non">In Progress</td>
                <td class="non">In Progress</td> -->
                


            </tr>
                <?
             }

             ?>
            </table>
            </div>
        <?

        }
 


        private function cmp($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        }



  
		private function makeToken($p_sStr){
			$l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
			$p_sStr = base64_encode($l_sPaddingText.base64_encode(strrev(base64_encode($p_sStr))).base64_encode($l_sPaddingText));
			return $p_sStr;
		}

		private function decodeToken($p_sStr){
			$l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
			$l_sEnding = base64_encode($l_sPaddingText);
			$p_sStr = str_replace($l_sEnding,'',base64_decode($p_sStr));
			$p_sStr = str_replace($l_sPaddingText,'',$p_sStr);
			$p_sStr = base64_decode(strrev(base64_decode($p_sStr)));
			return $p_sStr;
		}

		private function cleanForReturn(&$p_aData){
			foreach($p_aData as $k=>$v){
				if(!$v && $v != '0')
					unset($p_aData[$k]);
			}
		}
 
                  


            private function cleanArg($p_sStr){
                return trim(strip_tags($p_sStr));
            }

            protected function showError($p_nErrorCode,$p_sMessage){
                $this->outputJSON('{
                                "status": "'.$p_nErrorCode.'",
                                "msg": '.json_encode($p_sMessage).'
                                }');
            }

            protected function outputJSON($p_sJSON,$p_bExit = true){
                header('Content-Type: application/json');
                echo $p_sJSON;
                if($p_bExit)
                    exit;
            }

		









}

	

?>