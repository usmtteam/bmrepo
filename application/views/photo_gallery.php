<link href="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<script src="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.pack.js" ></script>
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title form-horizontal">
                    Media
                    <small></small>
                    <?php if($selfuser->admin_type!=2): ?>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?PHP echo site_url('content/feed/photo') ?>" class="btn green">
                        New Photo
                        </a>
                    </div>
                    <?php endif; ?>
                    <div class="btn-group pull-right control-group" style="margin-right: 30px;">
                        <label class="control-label">
                        Filter By Tags
                        </label>
                        <div class="controls">
                            <?php echo form_dropdown("photo_tags_search", $tagOptions, $selectedTags, 'class="photo_tags_search medium m-wrap"');?>
                        </div>
                    </div>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <input type="hidden" value="<?php echo site_url('content/photo_gallery'); ?>" id='base_url'>
        <div class="row-fluid">
            <div class="portlet-body">
            <div id="form_head_back_media">Photo Gallery</div>
                <div id="form_container_white">
                <!-- BEGIN GALLERY MANAGER LISTING-->
                <div class="row-fluid">
                <?php
                	$start_key =  $page * $page_count;
                	$end_key = min(count($photos), $start_key + $page_count) - 1;
                	for($key = $page * $page_count; $key <= $end_key; $key++) :
                	$photo = $photos[$key];
                ?>
                	<div class="span3">
                        <div class="item">
                            <a class="fancybox-button" data-rel="fancybox-button" title="<?php echo $photo['description']; ?>" href="<?php echo $photo['destination']; ?>">
                                <div class="zoom gallery-image-container" style="text-align: center;"> 
                                    <img src="<?php echo $photo['destination']; ?>" alt="Photo" />                                           <div class="zoom-icon"></div>
                                    <div class="gallery-image-outer">
                                        <div class="gallery-image-inner"><?php echo $photo['description']; ?></div>    
                                    </div>
                                </div>
                            </a>
                            <div class="details">
                                <a href="#" class="icon"><i class="icon-paper-clip"></i></a>
                                <a href="#" class="icon"><i class="icon-link"></i></a>
                                <a href="#" class="icon"><i class="icon-pencil"></i></a>
                                <a href="#" class="icon"><i class="icon-remove"></i></a>        
                            </div>
                        </div>                       
                    </div>
                    <?php if($key % 4 == 3 || $key == $end_key) : ?>
						</div>
                		<div class="space10"></div>
                	<?php if($key != $end_key) :?>
                		<div class="row-fluid">
					<?php 
						endif;
					endif; 
                    ?>
                <?php endfor; ?> 
                </div>
                <center>
                <div class="dataTables_paginate paging_bootstrap pagination"><ul>
                	<li class="prev<?php echo $page == 0 ? ' disabled' : ''?>"><a data-page="<?php echo max(0, $page - 1);?>">← Previous</a></li>
                	<?php for($i = 0; $i < $total_page_count; $i++) :?>
                	<li class="<?php echo $page == $i ? ' active' : ''; ?>"><a data-page="<?php echo $i;?>"><?php echo $i+1; ?></a></li>
                	<?php endfor; ?>
                	<li class="next<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>""><a data-page="<?php echo min($total_page_count-1, $page + 1);?>">Next →</a></li>
				</ul></div>
                </center>
                <script language="javascript">
                $(document).ready(function() {
                    $('.pagination a').click(function(e) {
                        var base_url = jQuery('#base_url').val();
                        var tag = $('.photo_tags_search').val();
                        if(tag == '') tag = '0';
                        document.location.href = base_url + "/" + tag + '/' + $(e.target).attr('data-page');
                    });
                });
                </script>
            </div>
        </div>
    </div>
    <!-- END GALLERY MANAGER PORTLET-->
</div>