window.SocialShareEngine =  {
	site_url : '',
	asset_url : '',
	cid : '',
	bitly_login : '',
	bitly_apikey: '',
	getBitLyUrl : function (longUrl) {
		longUrl = encodeURIComponent(longUrl);
		var url = 'http://api.bit.ly/v3/shorten?login='+SocialShareEngine.bitly_login + '&apikey='+SocialShareEngine.bitly_apikey+'&longUrl='+longUrl+'&format=txt';
		console.log(longUrl);
		return $.ajax({
	        type: "GET",
	        url: url,
	        async: false
	    }).responseText;
	},
	getPostDetail: function(post_type, content_id, callback) {
		var url = SocialShareEngine.site_url + '/api?cid=' + SocialShareEngine.cid + '&option=get_post_detail&post_type=' + post_type + '&content_id=' + content_id;
		$.ajax({url:url,success:function(result){
			callback(result.post[0], result.channel_info);
		}});
	},
	shareOnFacebook: function(post_type, content_id, facebook_page) {
		SocialShareEngine.getPostDetail(post_type, content_id, function(post, channel) {
			var picture_link = '';
			if(post.post_type == 'text') {
				picture_link = '';
			}
			else if(post.post_type == 'photo')	{
				picture_link = post.fullimage_path;
				var postpage = post.post_type;
			}
			else {
				picture_link = post.image_path.replace('&w=360','').replace('/360');
				if(post.post_type == 'video') {
					var string = post.image_path;

			    var new_string = string.replace('http://d3hl6zmfu9utfu.cloudfront.net/' + channel.slug + '/assets/ufile/', '');


				picture_link = 'http://d3hl6zmfu9utfu.cloudfront.net/' + channel.slug + '/assets/ufile/watermark/' + new_string;
				

				var postpage = 'post';
				}
			}
			var fb_options = {
			    method: 'feed',
		        name: channel.name,
		        link: 'http://' + channel.slug + '.on.starsite.com/content/' + post.post_type + '/' + post.content_id + '&page_type=landingpage',
		        picture: picture_link,
		        message: post.caption			    
			    };
			if(facebook_page != '') {
				fb_options.from = facebook_page;
			}
			FB.ui(fb_options);
		});
	},
	shareOnTwitter: function(post_type, content_id) {
		SocialShareEngine.getPostDetail(post_type, content_id, function(post, channel) {
			var body = {
				message: post.caption,
				type: 'picture',
				picture: ''
			};
			var picture_link = '';
			body.message = body.message.replace(/"/g, '\"');
			body.message = escape(body.message);
			var croppedMessage = body.message;
			if(croppedMessage.length>140){
			body.message = croppedMessage.substring(0,136);
			body.message += '...';
			}else{
				body.message = croppedMessage;
			}
			if(post.post_type == 'text') {
				picture_link = SocialShareEngine.asset_url + '/ufile/' + channel.image_path;
				body.picture = SocialShareEngine.getBitLyUrl(picture_link);
			}
			else if(post.post_type == 'photo')	{
				picture_link = post.fullimage_path;
				body.picture = SocialShareEngine.getBitLyUrl(picture_link);
			}
			else {
				//picture_link = post.image_path.replace('&w=360','').replace('/360');
				if(post.post_type == 'video') {
					body.type = 'video';
					
					body.picture = 'https://' +channel.slug +'.on.starsite.com/content/video/' + post.content_id + '&page_type=twitter';
					body.picture = post.deep_link_web_tw;

				}
			}
			
			window.open('https://twitter.com/intent/tweet?text=' + body.message + '&url=' + body.picture,'_blank','toolbar=0,status=0,width=548,height=325');
		});
	}
};