<?php
                            // Only admin will see admin checkboxes
                            if($this->session->userdata('username')=="administrator"){ ?>
                                <div class="control-group">
                                    <div class="checkbox-list">

                                       <!--  <label>
                                            <input type="checkbox" name="feed_date" <?php if($feed_date == "on"){echo 'checked';} ?>>
                                            <span>Add to Feed</span>
                                        </label> -->
                                        <label>
                                            <input type="checkbox" name="is_oflo" <?php if($is_oflo == "on"){echo 'checked';} ?>>
                                            <span>Publish</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" id="is_oflo_slider" name="is_oflo_slider"  <?php if($is_oflo_slider == "on"){echo 'checked';} ?>>
                                            <span>Slider</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" name="is_premium"  <?php if($is_premium == "on"){echo 'checked';} ?>>
                                            <span>Premium</span>
                                        </label>
                                    </div>
                                </div>
                   <div class="span6" style="margin:0px;" >

                        <div class="slider_thumbs" id="slider_thumbs" style="display:none">
                          
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="width: 324px; height: 100px; border-style:none !important;">
                                    <input type="hidden" id="slider_thumb" name="slider_thumb" value="">
                                   
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" id="slider_thumb_preview" style="max-width: 324px; max-height: 100px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file">
                                        <span class="fileupload-new">Select Slider Banner</span>
                                        <span class="fileupload-exists">Change</span>

                                        <input style="float:right; width:200px;" type="file" name="image_slider" class="default" id="slider_thumb" /> 
                                    </span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                 
                                   
                               </div>
                             </div>
                        </div>
                    </div>

                    
           

                            <?php } ?>

<div class="control-group">

            <div class="span12" style="margin-bottom:15px;">
                <label>Category:</label>
                <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalupload">Select Category</button>

            </div>
            
             
                    <!-- Modal -->
                    <div id="myModalupload" class="modal fade" role="dialog" style='background-color:#000020; display: none;'>
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Choose Category</h4>
                          </div>
                          <div class="modal-body" >
                            <table>
                                <tr>

                                   <?php 
                                        $i=0;
                                        foreach($viral_media_category as $category){
                                        $i++;
                                    ?>
                                    <td>
                                        <div style="display: inline-block; vertical-align: middle; text-align:center;"><?php echo $category['cat_name']; ?><br>
                                        <input style="text-align: center; vertical-align: middle; width: 70px;" type='checkbox' name='<?php echo $category['id']; ?>' <?php foreach($cat_id as $cat){if($cat==$category['id']){echo "checked";}} ?>></div>
                                    </td>
                                    <?php if($i==6){echo'</tr><tr>'; $i=0;}} ?>
                    
                                     </tr>
                            </table>
                          </div>
                         <div class="modal-footer">
                            <?php echo form_submit('', "Save",' class="btn blue" data-dismiss="modal"');?>
                        </div>
                        </div>

                      </div>
                    </div>

</div>

                            <div class="control-group">
                                <div class="span12" style="margin-bottom:15px;">
                                    <label>Syndication:</label>
                                </div>
                                
                               <!--  <div class="row-fluid"> -->
                            
                                    <div class="span3">
                                        <input type="hidden" id="post_to_facebook" name="post_to_facebook" value="<?php echo $facebook_synd ?>" />
                                        <a class="fb ssfont facebook-new-cms float-left <?php if($facebook_synd == "true"){echo 'facebook-active';} ?>">f</a>
                                        <span class="social-title">Facebook</span>
                                    </div>
    
                              <!--   </div> -->
                               <!--  <div class="row-fluid"> -->
                                    <div class="span3">
                                        <input type="hidden" id="post_to_twitter" name="post_to_twitter" value="<?php echo $twitter_synd ?>"  />
                                        <a class="tw ssfont twitter-new-cms float-left <?php if($twitter_synd == "true"){echo 'twitter-active';} ?>">t</a>
                                        <span class="social-title">Twitter</span>
                                    </div>
                              <!--   </div> -->
                                <? /*<div class="row-fluid">
                                    <div class="span3">
                                        <input type="hidden" id="post_to_tumblr" name="post_to_tumblr" value=""  />
                                        <a class="tb ssfont tumblr-new-cms float-left">u</a><span class="social-title">Tumblr</span>
                                    </div>
                                </div>
                                  <div class="row-fluid">
                                <div class="span8">
                                <input type="hidden" id="post_to_gp" name="post_to_gp" value=""  />
                                <div class="icon-twitter3 gp-new-cms float-left"></div><span class="social-title">Google +</span>
                                </div>
                                </div> */ ?>
                           </div>

                               <?php if($this->session->userdata('username')=="administrator"){ ?>

      
                                <div class="checkbox-list">  
                             <input type="checkbox"  id="is_feed" name="is_feed" <?php if($is_feed == "on"){echo 'checked';} ?>><span>Add to Feed</span>

                    
            
                          <br>
                                          <div id="timestamp2_div" style="display:none;" class="input-append">
                                      
                                
                                        
                                        <input type="text" id="feed_date" name="feed_date" value="<?php 
                                        date_default_timezone_set('America/Los_Angeles');
                                        
                                        echo date("Y-m-d G:i:s"); ?>" />

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    
                                  
                                 </div> 
                                
                                  
                                 <?php } ?>    
                                 <br>
                        <?php if($this->session->userdata('username')=="administrator"){ ?>
                          <div class="priority">  
                            <input id="priorityCheckBox" name="priorityCheckBox" type="checkbox"><span>Is this a Priority Post? </span></input>
                            
                            <div class="priorityChecked">
                                
                                <select id="prioritySelected" name="prioritySelected">                      
                                <option value="0">Priority Level</option>
                                
                               
                                <?php
                                    for ($prioritySelected = 1; $prioritySelected <= 20; $prioritySelected++) {
                                ?>
                                        <option value='<?php echo $prioritySelected ?>'><?=$prioritySelected." priority" ?></option>
                                <?php    } 
                                ?>

                                 </select>
                            </div>

                            <div class="priorityDay">
                                
                                <select id="priorityDaySelected" name="priorityDaySelected">                      
                                <option value="0">Days As Priority</option>
                                
                               
                                <?php
                                    for ($priorityDaySelected = 1; $priorityDaySelected <= 30; $priorityDaySelected+=5) {
                                ?>
                                        <option value='<?php echo $priorityDaySelected ?>'><?=$priorityDaySelected.' days' ?></option>

                                        
                                <?php    } 
                                ?>

                                 </select>
                            </div>
                          </div> 

                        <?php } ?>    

                        <br> 
                           <div class="schedulePost">  
                            <input id="scheduleCheckBox" name="scheduleCheckBox" type="checkbox"><span>Want to Schedule this Post?<span></input>
                            <div class="priorityDates">
                                <div class="span4">
                                    <font size="1" face="arial, helvetica, sans-serif">Post Date
                                    </font>
                                    <br>
                                    <div id="priority_start_date" class="input-append">
                                
                                        
                                        <input type="text" id="start_date" name="start_date" value="<?php 
                                        date_default_timezone_set('America/Los_Angeles');
                                        
                                        echo date("Y-m-d G:i:s"); ?>" />

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                        
                                    </div>
                                 </div>  
                             </div>

                            </div>
                

                          </div>       
                            
                            <script language="javascript">
                                $(document).ready(function() {
                                                                        
                                    $('#priority_start_date').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
                            
                                    $('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});


                                   $(document).ready(function() {
                      $('#timestamp2_div').datetimepicker({
                            format: 'yyyy-MM-dd hh:mm:ss'
                         });
                      $('#is_feed').change(function(e) {
                        $('#timestamp2_div').toggle(this.checked);
                      });
                    });
                            
                                   




                                    $('.facebook-new-cms').click(function() {
                                        $('.facebook-new-cms').toggleClass('facebook-active');
                                        var hiddenField = $('#post_to_facebook'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.twitter-new-cms').click(function() {
                                        $('.twitter-new-cms').toggleClass('twitter-active');
                                        var hiddenField = $('#post_to_twitter'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.tumblr-new-cms').click(function() {
                                        $('.tumblr-new-cms').toggleClass('tumblr-active');
                                        var hiddenField = $('#post_to_tumblr'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                    $('.gp-new-cms').click(function() {
                                        $('.gp-new-cms').toggleClass('gp-active');
                                        var hiddenField = $('#post_to_gp'),
                                        val = hiddenField.val();
                                        hiddenField.val(val === "true" ? "false" : "true");
                                    });

                                     
                                        $('#submit_now').click(function() {
                                            $('#spinner').show();
                                            $('#submit_now').hide();
                                        });
                                   

                                    $('#priorityCheckBox').click(function() {
                                        $(".priorityChecked").toggle(this.checked);
                                        $(".priorityDay").toggle(this.checked);
                                    });

                                      $('#scheduleCheckBox').click(function() {
                                        $(".priorityDates").toggle(this.checked);
                                    });

                                        $('#is_oflo_slider').click(function() {
                                        $(".slider_thumbs").toggle(this.checked);
                                    });
                                });
                                  </script>


                          