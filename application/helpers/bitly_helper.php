<?php

function get_web_app_link($channel, $post_type, $content_id) {
	$channel_url = $channel['url'];
	if(MODE_DEV) {
 		return rtrim(site_url(), 'index.php') . $channel_url . '/' . '#homefeed/'.$post_type .'/' . $content_id;
	}
	else {
		return '?'.$post_type.'='.$content_id;
	}
}

function get_web_app_link_of($channel, $post_type, $content_id) {
	$channel_url = $channel['url'];
	if(MODE_DEV) {
 		return rtrim(site_url(), 'index.php') . $channel_url . '/' . '#homefeed/'.$post_type .'/' . $content_id;
	}
	else {
		return '/'.$post_type.'/'.$content_id;
	}
}
function get_mobile_app_link($channel, $post_type, $content_id) {
 if(MODE_DEV) {
  return 'com.myapp.Starclub.' . $channel['url'] . '://'.$channel['id'].'/'.$post_type.'/'.$content_id;
 }
 else {
  return PRODUCTION_BUNDLEID . $channel['url'] . '://'.$channel['id'].'/'.$post_type.'/'.$content_id;
 }
}
function get_bitly_url($longurl) {
	$url = 'http://api.bit.ly/v3/shorten?login='.BITLY_LOGIN . '&apikey='.BITLY_APIKEY.'&longUrl='.urlencode($longurl).'&format=txt';
	
	return _get_text($url);
}