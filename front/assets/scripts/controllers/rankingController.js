enriqueApp.controller('rankingController', ['$scope', '$rootScope', '$http', function($scope,$rootScope, $http) {
	$scope.page = 0;
    $scope.users = [];
	
	$scope.is_loading = false;
    //$scope.loadData();
    
    $scope.loadData = function () {
    	if($scope.is_loading) return;

    	
    	$scope.is_loading = true;
    	CommonLib.show_loading_bubble();
    	$http({method: 'GET', url: $rootScope.get_api_url() + '&option=get_rating&page='+$scope.page+'&count=20'}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.users = $scope.users.concat( data.rankings );
        		if(data.rankings.length > 0) $scope.page ++;
        	}
        	CommonLib.hide_loading_bubble();
        	
        	$scope.is_loading = false;
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        	
        	$scope.is_loading = false;
        });
    }
}]);