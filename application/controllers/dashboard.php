<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();

      
        $this->load->library('email');
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url'); 
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
    }
    
    function _remap($method) {

            $this->load->view('header_n');
            $this->load->view('sidebar_n');
            $this->{$method}();
            $this->load->view('footer_v');

    }
    
    public function index()
    {
        $data['searchvalue'] = $this->input->post('searchvalue');
        
        $chanels = $this->main_m->getChanels($data['searchvalue']);
        $user_id = $this->session->userdata('user_id');
        $user = $this->ion_auth_model->user($user_id)->row();
        $results = array();
        
        if(!$user->superadmin){
        //$this->set_slug_dependent_constants($user->email);
            foreach($chanels as $chanel){
                if($chanel->user_id == $user->id){
                    $results[] = $chanel;
                }
            }
        }else{
            $results = $chanels;
        }
        
        $data['chanels'] = $results;
        $data['user'] = $user;
       
        if($user->email!="admin@admin.com"){
         $this->content();
      }else{
        $this->load->view('dashboard_view', $data);
      }
    }

    public function priority()
    {
    

        $chanels = $this->main_m->getPriorityList();
        $user_id = $this->session->userdata('user_id');
        $user = $this->ion_auth_model->user($user_id)->row();
        $results = array();
        
        if(!$user->superadmin){
        //$this->set_slug_dependent_constants($user->email);
            foreach($chanels as $chanel){
                if($chanel->user_id == $user->id){
                    $results[] = $chanel;
                }
            }
        }else{
            $results = $chanels;
        }
        
       
        $data['chanels'] = $results;
        $data['user'] = $user;
       
        if($user->email!="admin@admin.com"){
         $this->content();
      }else{
        $this->load->view('priority_dashboard_view', $data);
      }
    }

    public function priority_update()
    {
       
       $this->main_m->updatePriorityList();
      
    }

       public function priority_activated()
    {

       $this->main_m->activatePriority();
      
    }

     public function priority_keep()
    {
       $this->main_m->keepPriority();
      
    }

    
       public function priority_deactivated()
    {
       $this->main_m->deactivatePriority();

    }
    
    public function content(){
        
        $user_id = $this->session->userdata('user_id');
        $user = $this->ion_auth_model->user($user_id)->row();


        if($user->email!="admin@admin.com"){
            $id =  $this->set_slug_dependent_constants($user->email);;
        }else{
            $id = $this->uri->segment(3, 0);
            $SQL = 'SELECT email, id FROM users WHERE cid = "'.$id.'"';
            $query = $this->db->query($SQL);
            $rows = $query->result();
            $email = $rows[0]->email;
            $useid = $rows[0]->id;

            $id =  $this->set_slug_dependent_constants($email);
            $user_id = $useid;
            $user = $this->ion_auth_model->user($user_id)->row();
      }
       
        if (empty($id)) {
            show_error("Select a channel!");
            return;
        }
        
        
        if(!$user->superadmin && $user->cid!=$id){
            show_error("You have not permission for this channel!");
            return;
        }
        
        $chanel = $this->main_m->get_chanel($id);
        $this->session->set_userdata('chanel', $chanel);
      
       
       
        redirect('content/feed');
     
    }


     public function cpm_control(){


        $id = $this->uri->segment(3, 0);

        $qry = $this->main_m->get_chanel($id);
        $this->data['post']['preview'] = false;
        $this->data['post']['preview_total'] = false;
        $this->data['post']['preview_phrase'] = false;
        $this->data['post'] = array(
            "start_date_report" => strtotime($this->input->post('start_date_report')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report'),
            "end_date_report" => strtotime($this->input->post('end_date_report')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report'),
            'cpm' => $this->input->post('cpm'),
            "start_date_report1" => strtotime($this->input->post('start_date_report1')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report1'),
            "end_date_report1" => strtotime($this->input->post('end_date_report1')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report1'),
            'cpm1' => $this->input->post('cpm1'),
            "start_date_report2" => strtotime($this->input->post('start_date_report2')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report2'),
            "end_date_report2" => strtotime($this->input->post('end_date_report2')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report2'),
            'cpm2' => $this->input->post('cpm2'),
            "start_date_report3" => strtotime($this->input->post('start_date_report3')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report3'),
            "end_date_report3" => strtotime($this->input->post('end_date_report3')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report3'),
            'cpm3' => $this->input->post('cpm3'),
            "start_date_report4" => strtotime($this->input->post('start_date_report4')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report4'),
            "end_date_report4" => strtotime($this->input->post('end_date_report4')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report4'),
            'cpm4' => $this->input->post('cpm4'),
            "start_date_report5" => strtotime($this->input->post('start_date_report5')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report5'),
            "end_date_report5" => strtotime($this->input->post('end_date_report5')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report5'),
            'cpm5' => $this->input->post('cpm5'),
            "start_date_report6" => strtotime($this->input->post('start_date_report6')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report6'),
            "end_date_report6" => strtotime($this->input->post('end_date_report6')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report6'),
            'cpm6' => $this->input->post('cpm6'),
            "start_date_report7" => strtotime($this->input->post('start_date_report7')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report7'),
            "end_date_report7" => strtotime($this->input->post('end_date_report7')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report7'),
            'cpm7' => $this->input->post('cpm7'),
            "start_date_report8" => strtotime($this->input->post('start_date_report8')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report8'),
            "end_date_report8" => strtotime($this->input->post('end_date_report8')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report8'),
            'cpm8' => $this->input->post('cpm8'),
            "start_date_report9" => strtotime($this->input->post('start_date_report9')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date_report9'),
            "end_date_report9" => strtotime($this->input->post('end_date_report9')) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $this->input->post('end_date_report9'),
            'cpm9' => $this->input->post('cpm9'),
            );

    $this->data['chanel'] = $qry;
    $cid = $this->data['chanel']['id'];

    $start_date_total = date('Y-n-j', time() - 3600 * 24 * 365);
    $str = explode(' ', $start_date_total);
    $start_date_total = $str[0];

    $end_date_total = date('Y-m-d');

    $start_date = $this->input->post('start_date_report');
    $end_date = $this->input->post('end_date_report');
    $cpm = $this->input->post('cpm');

    if($start_date==false){
      $start_date = date('Y-n-j', time() - 3600 * 24 * 30);
      $str = explode(' ', $start_date);
      $start_date = $str[0];
    }
    if($end_date==false){
      $end_date = date('Y-m-d');
    }

    $this->data['post']['start_date_report'] = $start_date;
    $this->data['post']['end_date_report'] = $end_date;
    $this->data['post']['cpm'] = $cpm;

    $l_xSql = AppController::getInstance()->getConnection();

    $l_sSql = "SELECT page_views_ga, content_id, time_stamp, id FROM sc_reports_daily WHERE channel_id = $cid AND time_stamp BETWEEN '$start_date' AND '$end_date'";
     
    $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

    $total_revenue = 0;    

    foreach($l_aData as $value){

      $content_id = $value['content_id'];
      $page_view = $value['page_views_ga'];
      $date_post = $value['time_stamp'];
      $id = $value['id'];

      $revenue = ($page_view/1000)*$cpm;

      $l_sSql = "UPDATE sc_reports_daily SET cpm = $cpm, revenue = $revenue WHERE id = $id";

      $total_revenue += $revenue;
      $date_range_total = $total_revenue;
      if($this->input->post('refresh', false) == true){
      $l_xSql->runQuery($l_sSql);
      }

    }
    if($date_range_total>0){

      $this->data['post']['preview_phrase'] = "Total Revenue for ".$start_date."-".$end_date." will be: $".number_format($date_range_total, 2, '.', '')."<br>";
      $this->data['post']['preview'] = true;
    }

    if($total_revenue>0){

          $l_sSql = "SELECT revenue, time_stamp FROM sc_reports_daily WHERE channel_id = $cid";
    
          $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
        
          foreach ($l_aData as $value) {
            //var_dump($value['time_stamp']);
          /*  if($start_date > $value['time_stamp']){ 
                $overall_revenue =+ $value['revenue'];
                var_dump($value['time_stamp']);
            }
            if($end_date < $value['time_stamp'] ){
                $overall_revenue =+ $value['revenue'];
                var_dump($value['time_stamp']);
            }*/
            $overall_revenue =+ $value['revenue'];
          }
          var_dump($overall_revenue);
          //$l_sSql = "UPDATE sc_reports SET cpm = $cpm, revenue = $total_revenue WHERE id = $id";
          $this->data['post']['preview_phrase_total'] = "Total Overall Revenue for Channel will be: $".number_format($overall_revenue, 2, '.', '')."<br>";
          $this->data['post']['preview_total'] = true;
    }

    $start_date = array();
    $end_date = array();
    $cpm = array();
    $date_range_total = array();

    for($i=1;$i<10;$i++){
       
        if($this->input->post("cpm$i")!=""){

            $start_date[$i] = $this->input->post("start_date_report$i");

            $end_date[$i] = $this->input->post("end_date_report$i");
          
            $cpm[$i] = $this->input->post("cpm$i");

            if($start_date[$i]==false){
              $start_date[$i] = date('Y-n-j', time() - 3600 * 24 * 30);
              $str = explode(' ', $start_date[$i]);
              $start_date[$i] = $str[0];
            }
            if($end_date[$i]==false){
              $end_date[$i] = date('Y-m-d');
            }

            $this->data["post"]["start_date_report$i"] = $start_date[$i];
            $this->data["post"]["end_date_report$i"] = $end_date[$i];      

            $l_sSql = "SELECT page_views_ga, content_id, time_stamp, id FROM sc_reports_daily WHERE channel_id = $cid AND time_stamp BETWEEN '$start_date[$i]' AND '$end_date[$i]'";
          
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

            foreach($l_aData as $value){

              $content_id = $value['content_id'];
              $page_view = $value['page_views_ga'];
              $date_post = $value['time_stamp'];
              $id = $value['id'];

              $revenue = ($page_view/1000)*($cpm[$i]);

              $l_sSql = "UPDATE sc_reports_daily SET cpm = $cpm[$i], revenue = $revenue WHERE id = $id";

              $total_revenue += $revenue;
              $date_range_total[$i] +=$revenue;
              if($this->input->post('refresh', false) == true){
              $l_xSql->runQuery($l_sSql);
              }

            }
        }

        if($date_range_total[$i]>0){
          //$l_sSql = "UPDATE sc_reports SET cpm = $cpm, revenue = $total_revenue WHERE id = $id";
          $this->data['post']['preview_phrase_more'][$i] = "Total Revenue for ".$start_date[$i]."-".$end_date[$i]." will be: $".number_format($date_range_total[$i], 2, '.', '')."<br>";
          $this->data['post']['preview_more'] = true;
      }
        
    }

    
    //$overall_revenue = $total_revenue;

    if($overall_revenue>0){

          //$l_sSql = "SELECT revenue, time_stamp FROM sc_reports_daily WHERE channel_id = $cid AND time_stamp BETWEEN '$start_date_total' AND '$end_date_total'";
    
         // $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
        
         // foreach ($l_aData as $value) {

           // $overall_revenue =+ $value['revenue'];
         // }
          //$l_sSql = "UPDATE sc_reports SET cpm = $cpm, revenue = $total_revenue WHERE id = $id";
          $this->data['post']['preview_phrase_total'] = "Total Overall Revenue for Channel will be: $".number_format($overall_revenue, 2, '.', '')."<br>";
          $this->data['post']['preview_total'] = true;
    }

    if($this->input->post('refresh', false) == true){
        $this->data['post']['preview']=false;
        $this->data['post']['finished']=true;


    }



 
        $this->load->view('cpm_control', $this->data);
     
    }

    public function set_slug_dependent_constants($email){

      
      $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $cid = $rows[0]->cid;
      
      //$this->chanel = $this->get_chanel($cid);
      

      $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $slug = $rows[0]->slug;


      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
       define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return $cid;

      }
    
    public function chanel_add(){

        $top_ad_unit = base64_encode($this->input->post('top_ad_unit'));
        $right_top_ad_unit = base64_encode($this->input->post('right_top_ad_unit'));
        $right_bottom_ad_unit = base64_encode($this->input->post('right_bottom_ad_unit'));
        $mobile_ad_unit = base64_encode($this->input->post('mobile_ad_unit'));

        $qry = 
            array(
                'id' => '0',
                'name' => $this->db->escape_str($this->input->post('chanel_name')),
                'title' => $this->db->escape_str($this->input->post('chanel_title')),
                'url' => $this->db->escape_str($this->input->post('chanel_url')),
                'slug' => $this->db->escape_str($this->input->post('chanel_slug')),
                'chanel_admin' => $this->db->escape_str($this->input->post('chanel_admin')),
                'password' => $this->db->escape_str($this->input->post('chanel_password')),
                'confirm_password' => $this->db->escape_str($this->input->post('chanel_confirm_password')),
                'created' => $this->db->escape_str($this->input->post('created')),
                'is_publish' => $this->db->escape_str($this->input->post('is_publish')=='on' ? 1 : 0),
                'bc_playerid_web' => $this->db->escape_str($this->input->post('bc_playerid_web')),
                'bc_playerid_ios' => $this->db->escape_str($this->input->post('bc_playerid_ios')),
                'bc_playerid_android' => $this->db->escape_str($this->input->post('bc_playerid_android')),
                'ad_server_url_web' => $this->db->escape_str($this->input->post('ad_server_url_web')),
                'ad_server_url_mobile' => $this->db->escape_str($this->input->post('ad_server_url_mobile')),
                'ad_server_url_desktop' => $this->db->escape_str($this->input->post('ad_server_url_desktop')),
                'ad_server_url_ios' => $this->db->escape_str($this->input->post('ad_server_url_ios')),
                'ad_server_url_android' => $this->db->escape_str($this->input->post('ad_server_url_android')),
                'feedback_email' => $this->db->escape_str($this->input->post('feedback_email')),
                'enable_banner_ads_ios' => $this->db->escape_str($this->input->post('enable_banner_ads_ios')=='on' ? 1 : 0),
                'enable_banner_ads_android' => $this->db->escape_str($this->input->post('enable_banner_ads_android')=='on' ? 1 : 0),
                'enable_banner_ads_web' => $this->db->escape_str($this->input->post('enable_banner_ads_web')=='on' ? 1 : 0),
                'brightcove_overlay_logo' => $this->db->escape_str($this->input->post('brightcove_overlay_logo')),
                'brightcove_key' => $this->db->escape_str($this->input->post('brightcove_key')),
                'min_inline_video_len' => $this->db->escape_str($this->input->post('min_inline_video_len')),
                'google_analytics_key' => $this->db->escape_str($this->input->post('google_analytics_key')),
                'starstats_site_id' => $this->db->escape_str($this->input->post('starstats_site_id')),
                'quantserve_key' => $this->db->escape_str($this->input->post('quantserve_key')),
                'top_ad_unit' => $this->db->escape_str($top_ad_unit),
                'right_top_ad_unit' => $this->db->escape_str($right_top_ad_unit),
                'right_bottom_ad_unit' => $this->db->escape_str($right_bottom_ad_unit),                
                'mobile_ad_unit' => $this->db->escape_str($mobile_ad_unit),
                'current_cpm' => $this->db->escape_str($this->input->post('current_cpm')),
                'created' => strtotime($this->input->post('created')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('created'),
                'image' => $image_path,

            );

            

        $oflo_channel_user = $this->db->escape_str($this->input->post('oflo_channel_user') ? 'y' : 'n');

      define('PRODUCTION_LANDING' , 'http://'.$this->input->post('chanel_slug').'.on.starsite.com/');
       define('OFLO_LANDING' , 'http://'.$this->input->post('chanel_slug').'.on.oflo.co/');
      define('UPLOAD_AMZ', $this->input->post('chanel_slug').'/assets/ufile/');
      define('THEME_AMZ', $this->input->post('chanel_slug').'/images/');
      define('UPLOAD_AMZ_WATER', $this->input->post('chanel_slug').'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$this->input->post('chanel_slug').'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$this->input->post('chanel_slug').'/images/');
            
        $this->data = array();
     
        $this->data = $this->_proc_add($qry, $oflo_channel_user);


        $this->data['chanel'] = $qry;
    
      
      

        $this->load->view('chanel_add', $this->data);
    }
    private function _proc_add($qry=array(), $image_path, $oflo_channel_user) {
        //validate form input
        $this->form_validation->set_rules('chanel_name', "Channel Name", 'required|xss_clean');
        $this->form_validation->set_rules('chanel_password', "channel password", 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[chanel_confirm_password]');

        
        $data = array();



        if ($this->form_validation->run() == true)
        {
            if(!$this->email->valid_email($this->input->post('chanel_admin'))){

                $data['show_errors'] = "The Email Address field must contain a valid email address.";
            }elseif($this->main_m->isExistEmail($qry)){
                $data['show_errors'][] = 'This email already taken.';
            }elseif($qry['password'] != $qry['confirm_password']){
                $data['show_errors'][] = 'The password does not match.';
            }elseif($this->main_m->isExistChanel($qry)){
                $data['show_errors'][] = 'This channel name already exists.';
            }else{

                if($this->main_m->addChanel($qry, $oflo_channel_user, $image_path)){



                 // send email when new channel is created

               
               $subject = $this->input->post('chanel_name');
               $chanel_name = $this->input->post('chanel_name');
               $user_email = $this->input->post('chanel_admin');
               $channel_password = $this->input->post('chanel_password');
               $date = strtotime($this->input->post('created')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('created');
              

                   $this->email->from('info@starclubltd.com', 'Oflo'); // email address to send from
                   $this->email->to('channelalert@starclubltd.com');  // email address to send to
                   $this->email->subject('Oflo - New Channel - '.$subject); // subject
                   $this->email->message("A new channel has been added with the following details:\r\nChannel Name:".$chanel_name."\r\nDate:" .$date."\r\nUser Email:".$user_email."\r\nChannel Password:".$channel_password); //message
                   $this->email->send();

                   //email sent and processed

                    redirect("dashboard", '');
                }
            }
        }


        return $data;
    }
    public function chanel_edit(){
        $id = $this->uri->segment(3, 0);
        if (empty($id)) {
            show_error("Select a channel to edit!");
            return;
        }

        $this->chanel['id'] = $id; // Set Controller Channel ID
        $channel_users = $this->main_m->getUsers(); // Get Users by Channel ID
        $users_qry = array();

        if($this->input->post('delimg')){
            $this->db->where('id', $id);
            $this->db->update('enr_chanel', array('image_path'=>'', 'image_mime'=>''));
            redirect("dashboard/chanel_edit/".$id, '');
        }
        if(isset($_POST['chanel_name'])){
            $top_ad_unit = base64_encode($this->input->post('top_ad_unit'));
            $right_top_ad_unit = base64_encode($this->input->post('right_top_ad_unit'));
            $right_bottom_ad_unit = base64_encode($this->input->post('right_bottom_ad_unit'));
            $mobile_ad_unit = base64_encode($this->input->post('mobile_ad_unit'));
            $qry =
                array(
                    'id' => $id,
                    'name' => $this->db->escape_str($this->input->post('chanel_name')),
                    'title' => $this->db->escape_str($this->input->post('chanel_title')),
                    'url' => $this->db->escape_str($this->input->post('chanel_url')),
                    'slug' => $this->db->escape_str($this->input->post('chanel_slug')),
                    'chanel_admin' => $this->db->escape_str($this->input->post('chanel_admin')),
                    'password' => $this->db->escape_str($this->input->post('chanel_password')),
                    'confirm_password' => $this->db->escape_str($this->input->post('chanel_confirm_password')),
                    'is_publish' => $this->db->escape_str($this->input->post('is_publish')=='on' ? 1 : 0),
	                'bc_playerid_web' => $this->db->escape_str($this->input->post('bc_playerid_web')),
	                'bc_playerid_ios' => $this->db->escape_str($this->input->post('bc_playerid_ios')),
	                'bc_playerid_android' => $this->db->escape_str($this->input->post('bc_playerid_android')),
                    'ad_server_url_web' => $this->db->escape_str($this->input->post('ad_server_url_web')),
	                'ad_server_url_mobile' => $this->db->escape_str($this->input->post('ad_server_url_mobile')),
	                'ad_server_url_desktop' => $this->db->escape_str($this->input->post('ad_server_url_desktop')),
	                'ad_server_url_android' => $this->db->escape_str($this->input->post('ad_server_url_android')),
                	'ad_server_url_ios' => $this->db->escape_str($this->input->post('ad_server_url_ios')),
                    'feedback_email' => $this->db->escape_str($this->input->post('feedback_email')),
	                'enable_banner_ads_ios' => $this->db->escape_str($this->input->post('enable_banner_ads_ios')=='on' ? 1 : 0),
	                'enable_banner_ads_android' => $this->db->escape_str($this->input->post('enable_banner_ads_android')=='on' ? 1 : 0),
	                'enable_banner_ads_web' => $this->db->escape_str($this->input->post('enable_banner_ads_web')=='on' ? 1 : 0),
	                'brightcove_overlay_logo' => $this->db->escape_str($this->input->post('brightcove_overlay_logo')),
                    'brightcove_key' => $this->db->escape_str($this->input->post('brightcove_key')),
                    'min_inline_video_len' => $this->db->escape_str($this->input->post('min_inline_video_len')),
                    'google_analytics_key' => $this->db->escape_str($this->input->post('google_analytics_key')),
                    'starstats_site_id' => $this->db->escape_str($this->input->post('starstats_site_id')),
                    'quantserve_key' => $this->db->escape_str($this->input->post('quantserve_key')),
                    'top_ad_unit' => $this->db->escape_str($top_ad_unit),
                    'right_top_ad_unit' => $this->db->escape_str($right_top_ad_unit),
                    'right_bottom_ad_unit' => $this->db->escape_str($right_bottom_ad_unit),                
                    'mobile_ad_unit' => $this->db->escape_str($mobile_ad_unit),
                    'current_cpm' => $this->db->escape_str($this->input->post('current_cpm')),
                    'image' => $image_path,
                   
                );

            // Loop through each Channel User and build posted is_flo flag
            foreach ($channel_users as $channel_user) {
                $users_qry[] = array($channel_user['id'] => ($this->input->post('channel_user') ? 'y' : 'n'));
            }
        }else{
            $qry = $this->main_m->get_chanel($id);
            $users_qry = $channel_users;
        }
        define('PRODUCTION_LANDING' , 'http://'.$this->input->post('chanel_slug').'.on.starsite.com/');
         define('OFLO_LANDING' , 'http://'.$this->input->post('chanel_slug').'.on.oflo.co/');
      define('UPLOAD_AMZ', $this->input->post('chanel_slug').'/assets/ufile/');
      define('THEME_AMZ', $this->input->post('chanel_slug').'/images/');
      define('UPLOAD_AMZ_WATER', $this->input->post('chanel_slug').'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$this->input->post('chanel_slug').'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$this->input->post('chanel_slug').'/images/');
        $this->data = $this->_proc_edit($id, $qry, $users_qry);
        $this->data['chanel'] = $qry;
        $this->data['channel_users'] = $users_qry;

        $this->load->view('chanel_edit', $this->data);
    }
    private function _proc_edit($id, $qry=array(), $image_path, $users_qry) {
        //validate form input
        $this->form_validation->set_rules('chanel_name', "Channel Name", 'required|xss_clean');
        if($qry['password']){
            $this->form_validation->set_rules('chanel_password', "channel password", 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[chanel_confirm_password]');
        }

//        $this->form_validation->set_rules('chanel_admin', "ss",'required|xss_clean');
//        $this->form_validation->set_rules('chanel_password', "ss",'required|xss_clean');
//        $this->form_validation->set_rules('chanel_confirm_password', "ss",'required|xss_clean');
        
        $data = array();
        
        if ($this->form_validation->run() == true)
        {
//            echo $this->input->post('chanel_admin');exit;
            if(!$this->email->valid_email($this->input->post('chanel_admin'))){
                $data['show_errors'] = "The Email Address field must contain a valid email address.";
            }elseif($qry['password'] && ($qry['password'] != $qry['confirm_password'])){
                $data['show_errors'][] = 'The password does not match.';
            }elseif($this->main_m->isExistChanel($qry)){
                $data['show_errors'][] = 'This channel name already exists.';
            }else{
                // Update Channel
                if($this->main_m->updateChanel($qry)){

                    // Update Channel user
                    foreach ($users_qry as $user){
                        $this->main_m->updateUserOfloFlag($user);
                    }

                    redirect("dashboard", '');
                }
            }
        }
        return $data;
    }



    public function chanel_del(){
        $id = $this->uri->segment(3, 0);
        if (empty($id)) {
            show_error("Select a channel to delete!");
            return;
        }
        $chanel = $this->main_m->get_chanel($id);
      
        $destination = UPLOAD_DIR.$chanel['image_path'];
        if(is_file($destination)){
            unlink($destination);
        }
       
        
        $strSql = "DELETE FROM enr_chanel WHERE id='{$id}' ";
        $this->db->query($strSql);
      
        $strSql = "DELETE FROM enr_follows WHERE channel_id ='{$chanel['id']}' ";
        $this->db->query($strSql);
       
        $strSql = "DELETE FROM users WHERE id='{$chanel['user_id']}' ";
        $this->db->query($strSql);
        $strSql = "DELETE FROM users_groups WHERE user_id='{$chanel['user_id']}' ";
        $this->db->query($strSql);

        $strSql = "DELETE FROM enr_like WHERE content_id='{$chanel['video_id']}' ";
        $this->db->query($strSql);

        $sql_time = array(
            
            'last_updated_timestamp' => time()
        );
        $this->db->update('channel_update', $sql_time);
  

        redirect("dashboard", '');
    }
    
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */