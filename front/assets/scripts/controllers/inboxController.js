enriqueApp.controller('inboxController', ['$scope','$rootScope', '$http', '$modal', '$sce', function($scope, $rootScope, $http, $modal, $sce) {
	$scope.messages = [];
	
	$scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	
    	var url = $rootScope.get_api_url() + '&option=get_message&user_id='+($rootScope.current_user == null ? '' : $rootScope.current_user.id);
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.messages = data.messages.receiver_messages;
        		for(var i =0; i < $scope.messages.length; i++) {
        			$scope.messages[i].message_text = $scope.messages[i].message.replace(/<br ?\/?>/g, "");
        		}
        	}
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    };
    
    $scope.onViewMessageDetail = function(message) {
//    	for(var i = 0; i < $scope.messages.length; i++) {
//    		if($scope.messages[i].id == message.id) {
//    			$scope.messages[i].is_seen = 1;
//    			console.log($scope.messages[i]);
//    			break;
//    		}
//    	}
    	message.is_seen = 1;
    	$rootScope.dialogViewMessage = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'view_message_dialog',
		    resolve: {
		    	message: function() { return message; }
		    },
			templateUrl: 'views/viewmessage.html',
			controller: 'viewMessageController'
		});
    	
    	$http({method: 'GET', url: $rootScope.get_api_url() + '&option=update_seen&mail_id=' + message.id}).
        success(function(data, status, headers, config) {
        }).
        error(function(data, status, headers, config) {
        });
    }
    
    $scope.loadData();
}]);