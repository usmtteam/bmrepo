<?
  // NO CHANGES NEEDED
?>
<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
 

  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.URL]);
  _paq.push(["setCookieDomain", "<?= $l_xChannel->domain_cookie ?>"]);
  _paq.push(["setCustomVariable", 1, "userid", getCookie("ss_userid"), "visit"]);
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);


  (function() {
  var u = (("https:" == document.location.protocol) ? "https" : "http") + "://reports.starsite.com/";
  _paq.push(["setTrackerUrl", u + "starstats.php"]);
  _paq.push(["setSiteId", "<?= $l_xChannel->starstats_site_id ?>"]);
  var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
  g.type = "text/javascript";
  g.defer = true;
  g.async = true;
  g.src = u + "piwik.js";
  s.parentNode.insertBefore(g, s);
  })();

  var _qevents = _qevents || [];
  (function() {
  var elem = document.createElement('script');
  elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
  elem.async = true;
  elem.type = "text/javascript";
  var scpt = document.getElementsByTagName('script')[0];
  scpt.parentNode.insertBefore(elem, scpt);
  })();

  _qevents.push({
  qacct: "<?= $l_xChannel->quantserve_key ?>"
  });

</script>
<noscript>
  <div style="display:none;"><img src="//pixel.quantserve.com/pixel/<?= $l_xChannel->quantserve_key ?>" border="0" height="1" width="1" alt="Quantcast"/></div>
</noscript>
<noscript>
<!-- Starstats Image Tracker -->
<img src="http://reports.starsite.com/piwik.php?idsite=<?= $l_xChannel->starstats_site_id ?>&amp;rec=1" style="border:0" alt="" />
<!-- End Starstats -->
</noscript>