<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Social Settings
                    <small></small>
                </div>
                                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>    
        <div class="row-fluid">
            <div class="span12">
<!-- BEGIN SAMPLE FORM PORTLET-->   
                 <div class="portlet-body form">
                    <?php 
                    echo form_open_multipart("social", 'class="chanel-design form-horizontal"');
                        echo form_error("description");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <input type="hidden" name="cmd" value="save" />
                    <div class="space10"></div>
                    <div class="space10"></div>
                    <div id="form_head_back_media">Edit Social Settings</div>
                    <div class="row-fluid">
                        <div class="span12">
                        <BR /><BR />
                        <table width="100%">
                            <!--<tr>
                                <td width="30%" align="center">
                                    <div class="control-group">
                                        <a class="btn social" href="<?php echo site_url('social_account/index/twitter') ?>">Connect to twitter</a>
                                    </div>
                                </td>
                                <td align='left' colspan="6">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="twitter" <?php if($social['twitter']) echo "checked='true'"; ?>   >Automatically Post
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="center">
                                    <div class="control-group">
                                        <a class="btn social" href="<?php echo site_url('social_account/index/facebook') ?>" >Connect to Facebook</a>
                                    </div>
                                </td>
                                <td align='left' colspan="6">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="facebook" <?php if($social['facebook']) echo "checked='true'"; ?>   >Automatically Post
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="center">
                                    <div class="control-group">
                                        <a class="btn social" href="<?php echo site_url('social_account/index/google') ?>" >Connect to Google Plus</a>
                                    </div>
                                </td>
                                <td align='left' colspan="6">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="google" <?php if($social['google']) echo "checked='true'"; ?>   >Automatically Post
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="center">
                                    Push Notifications:
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="photos" <?php if($social['photos']) echo "checked='true'"; ?>   >Photos
                                        </label>
                                    </div>
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="videos" <?php if($social['videos']) echo "checked='true'"; ?>   >Videos
                                        </label>
                                    </div>
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="text" <?php if($social['text']) echo "checked='true'"; ?>   >Text
                                        </label>
                                    </div>
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="poll" <?php if($social['poll']) echo "checked='true'"; ?>   >Poll
                                        </label>
                                    </div>
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="quiz" <?php if($social['quiz']) echo "checked='true'"; ?>   >Quiz
                                        </label>
                                    </div>
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="auction" <?php if($social['auction']) echo "checked='true'"; ?>   >Auction
                                        </label>
                                    </div>
                                </td>
                            </tr>-->
                            
                            <tr>
                                <td width="30%" align="center" valign="top">
                                    Push Notifications:
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                        <label class="checkbox line" >
                                            <input class="social_check" type="checkbox" name="push" <?php if(isset($social['push']) && $social['push']) echo "checked='checked'"; ?>   >Enable
                                        </label>
                                    </div>
                                </td>
                                <td align='left'>
                                </td>
							</tr>
							<tr>
                                <td width="30%" align="center" valign="top">
                                    Facebook Page
                                    <input type="hidden" value="<?php echo $social['facebook_page'];?>" id="facebook_page" name="facebook_page" />
                                </td>
                                <td align='left' width="12%">
                                    <div class="control-group">
                                    	<ul  id="facebook_post_page">
                                    	</ul>
                                    </div>
                                    <style>
                    					#facebook_post_page li {
                    						list-style: none;
                    					}
                    					#facebook_post_page li.selected{
                    						font-weight:bold;
                    						font-size:18px;
                    					}
                                    </style>
                                    <script language="javascript">

						        		
										$(document).ready(function() {

											onFacebookLoad();
										});   

										function onFacebookLoad() {

											if(typeof FB == "undefined" || FB == null) {
												setTimeout(onFacebookLoad, 100);
												return;
											}
											FB.init({
										          appId      : '<?php echo FACEBOOK_ID; ?>',
										          status     : true,
										          xfbml      : true
										        });
											FB.getLoginStatus(function(response) {
												if (response.status === 'connected') {
													loadFacebookPages();
												} else {
													FB.login(function(response) {
														if (response.authResponse) {
															loadFacebookPages();
														}
														else {
															
														}
													}, {scope: 'email, manage_pages'});
												}
											});
										}
										function loadFacebookPages() {
											var fb_pages = [{id: '', name: 'My Timeline'}];
											FB.api("/me/accounts", function(response) {
												var ul = $('#facebook_post_page');
												var current_page = $('#facebook_page').val();
									        	var i;
									        	for(i = 0; i < response.data.length; i++) {
									        		fb_pages[i+1] = {
									        			id: response.data[i].id,
									        			name: response.data[i].name
									        		};
													
									        		
									        	}
									        	for(i = 0; i < fb_pages.length; i++) {
									        		ul.append($('<li data-id="' + fb_pages[i].id + '" class="' + (fb_pages[i].id == current_page ? "selected" : "") + '" onClick="onClickFacebookPage(this)">' + fb_pages[i].name + '</li>'));
									        	}
											});
										}       
										function onClickFacebookPage(li) {
											var l = $(li);
											var lis = $('#facebook_post_page li').removeClass('selected');
											l.addClass('selected');
											$('#facebook_page').val(l.attr('data-id'));
										}                      	
                                    </script>
                                </td>
                                <td align='left'>
                                </td>
							</tr>
                        </table>
                        </div>
                        
                        <div class="form-actions backwhite">
                            <?php echo form_submit('submit', "Save",' class="btn blue"');?>
                        </div>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
</div>
<link href="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<script src="<?php echo ASSETS_DIR; ?>/fancybox/source/jquery.fancybox.pack.js" ></script>
<script src="<?php echo ASSETS_DIR; ?>/jscolor/jscolor.js" ></script>
