enriqueApp.controller('sendMessageController', function($scope, $rootScope, $http, user_id, name, image, reply_message) {
	$scope.user_id = user_id;
	$scope.name = name;
	$scope.img_url = image;
	$scope.reply_message = reply_message;
	$scope.message_text = '';
	
	if(reply_message != null) {
		$scope.message_text = '\r\n\r\n<hr>\r\nFrom: ' + reply_message.name + '\r\nTo: ' + $rootScope.current_user.name + '\r\nSent: ' + $rootScope.CommonLib.formatDate(reply_message.time_stamp) + '\r\n\r\n' + reply_message.message.replace(/<br ?\/?>/g, "\n");
	}
	
	$scope.onSendMessage_click = function() {
		var target = $('#text_new_message');
		if(target.val().trim() == '') return;
		target.attr('disabled', 'disabled');
		CommonLib.show_waiting_circle($('#button_send_message'), 1);
		message = target.val();
		message = message.replace(/\n/g, '<br />')
    	var url = $rootScope.get_api_url() + '&option=send_message&receiver='+$scope.user_id+'&sender=' + $rootScope.current_user.id + '&message='+message;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$rootScope.dialogSendMessage.close();
        	}
        	CommonLib.hide_waiting_circle();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_waiting_circle();
        });
	}
});