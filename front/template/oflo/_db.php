<?php

// Todo - remove sessions
// session_start();

//==================================================================================================================
//                                              SETTERS
//==================================================================================================================

$p_nVideoId = strip_tags(trim($_GET['video']));
$p_nPhotoId = strip_tags(trim($_GET['photo']));
$p_nBCTId = strip_tags(trim($_GET['bctid']));
$p_sPageType = strip_tags(trim($_GET['page_type']));
$p_sType = strtolower(strip_tags(trim($_GET['p_sContentType'])));
$p_nContentId = strip_tags(trim($_GET['p_nContentId']));

$l_nDeviceType = Utils::getDeviceTypeFromBrowser();

$l_sCurrentAbsoluteURL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$l_sCurrentAbsoluteSecureURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$l_sCurrentAbsoluteURLNonQuery = "http://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]";

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$l_sCurrentAbsoluteURLNoVars = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];

$l_bIsVideo = false;
$l_nAssetId = 0;

//==================================================================================================================
// TODO: Come back to this - Might not need
//==================================================================================================================
$Device_User_Agent = $_SERVER['HTTP_USER_AGENT'];



if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $IP = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $IP = $_SERVER['REMOTE_ADDR'];
}
$browsercap = get_browser(null, true);
$Device_Platform = $browsercap['platform'];
$Device_Platform_Version = $browsercap['platform_version'];
$Device_Make = Utils::getMake($Device_User_Agent);
$Device_Model = $Device_Make;

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($Device_User_Agent))) {
    $l_nDeviceType = DEVICE_TYPE_TABLET;
    if (preg_match('/(ipad)/i', strtolower($Device_User_Agent))){ $Device_Platform = "iOS";}
}
else if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($Device_User_Agent))) {
    $l_nDeviceType = DEVICE_TYPE_MOBILE;
    if (preg_match('/(iphone)/i', strtolower($Device_User_Agent))){ $Device_Platform = "iOS";}
}
else{
    $l_nDeviceType = DEVICE_TYPE_DESKTOP;
}

if($l_nDeviceType == DEVICE_TYPE_MOBILE)
    $l_xChannel->adURL = 'http://search.spotxchange.com/vast/2.00/98570?VPI=MP4&content_page_url=' . $l_sCurrentAbsoluteURL . "&cb=" . mt_rand() . "&device[os]=" . $Device_Platform . "&device[osv]=" . $Device_Platform_Version . "&device[make]=" . $Device_Make . "&device[model]=" . $Device_Model . "&device[devicetype]=" . $Device_type . "&ip_addr=" . $IP . "&device[ua]=" . $Device_User_Agent;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





//==================================================================================================================
//                                              FIND BY ID
//==================================================================================================================


$l_xSql = AppController::getInstance()->getConnection();
$l_xItemManager = new ItemManager();

if($p_sType == CONTENT_TYPE_PHOTO){

    // Los Angeles timezone
    // date_default_timezone_set('UTC');
    // $utc_time = date("Y-m-d h:i:sa");

    // date_default_timezone_set('America/Los_Angeles');
    // $pacific_time = date("Y-m-d h:i:sa");

    // $hours_diff = $utc_time - $pacific_time;

    if($p_nContentId != 'recent'){
        $l_nAssetId = intval($p_nContentId);
        $l_xAssetResult = $l_xItemManager->getOfloContentById($l_nAssetId, CONTENT_TYPE_ID_PHOTO);
    }

    if($l_xAssetResult){ // Get by Photo ID

        $l_sSql = "SELECT ec.url, ep.* FROM enr_photo ep
                   LEFT JOIN enr_chanel ec on ec.id = ep.cid
                   WHERE ep.id > $l_nAssetId
                   AND ep.is_oflo = 'y'
                   AND ep.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                   ".(!empty($l_nChannelId) ? " AND ep.cid = $l_nChannelId " : '')."
                   ORDER BY ep.id ASC LIMIT 1";

        $l_xNextPhotoResult = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

        $l_sSql = "SELECT ec.url, ep.* FROM enr_photo ep
                   LEFT JOIN enr_chanel ec on ec.id = ep.cid
                   WHERE ep.id < $l_nAssetId
                   AND ep.is_oflo = 'y'
                   AND ep.start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                   ".(!empty($l_nChannelId) ? " AND ep.cid = $l_nChannelId " : '')."
                   ORDER BY ep.id DESC LIMIT 1";

        $l_xPreviousPhotoResult = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

        $l_bIsVideo = false;
        $l_nAssetId = intval($p_nContentId);

    }else{ // All failed, so get recent and redirect
        $l_xAssetResult = $l_xItemManager->getMostRecentOflo(CONTENT_TYPE_ID_PHOTO,$l_nChannelId);

        if($l_xAssetResult){
            header('Location: /ocontent/photo/'.$l_xAssetResult['id']);
            exit;
        }
    }

}elseif($p_sType == CONTENT_TYPE_VIDEO){

    // Los Angeles timezone
    // date_default_timezone_set('UTC');
    // $utc_time = date("Y-m-d h:i:sa");

    // date_default_timezone_set('America/Los_Angeles');
    // $pacific_time = date("Y-m-d h:i:sa");

    // $hours_diff = $utc_time - $pacific_time;

    if($p_nContentId == 'recent'){

        $l_xAssetResult = $l_xItemManager->getMostRecentOflo(CONTENT_TYPE_ID_VIDEO,$l_nChannelId);

        if($l_xAssetResult){

            header('Location: '.preg_replace("/recent/",$l_xAssetResult['id'],$l_sCurrentAbsoluteURLNonQuery));
            exit;
        }

    }else{ // Get by Video ID

        $l_nAssetId = intval($p_nContentId);
        $l_xAssetResult = $l_xItemManager->getOfloContentById($l_nAssetId,CONTENT_TYPE_ID_VIDEO);

        if($l_xAssetResult){

            $l_sSql = "SELECT * FROM enr_video
                       WHERE id < $l_nAssetId
                       AND is_publish = 1
                       AND is_oflo = 'y'
                       AND start_date <= DATE_SUB(NOW(), INTERVAL 7 HOUR)
                       ".(!empty($l_nChannelId) ? "AND cid = $l_nChannelId " : '')."
                       ORDER BY id DESC LIMIT 1";

            $l_xPreviousVideoResult = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

        }else{ // All failed, so get recent and redirect

            $l_xAssetResult = $l_xItemManager->getMostRecentOflo(CONTENT_TYPE_ID_VIDEO,$l_nChannelId);

            header('Location: /ocontent/video/'.$l_xAssetResult['id']);
            exit;

        }
    }

    $l_bIsVideo = true;

}else{
    echo "Invalid Parameters passed";
    exit;
}


// On asset result
if (!empty($l_xAssetResult)) {

    if($l_xAssetResult['id']>1334){ $wm = 'watermark/'; }else{ $wm = ''; }

    $l_sAssetType = ($l_bIsVideo) ? 'video' : 'photo';
    $l_sAssetTypeId = ($l_bIsVideo) ? '1' : '2';

    $l_sImageUrl = "http://d3hl6zmfu9utfu.cloudfront.net/".$l_xAssetResult['url']."/assets/ufile/".$wm.( ($l_bIsVideo) ? $l_xAssetResult['image_path'] : $l_xAssetResult['destination']  );
    $l_sImageThumb = "http://d3hl6zmfu9utfu.cloudfront.net/".$l_xAssetResult['url']."/assets/ufile/".( ($l_bIsVideo) ? $l_xAssetResult['image_path'] : $l_xAssetResult['destination']  );

    $l_sCanonicalLink = "http://$_SERVER[HTTP_HOST]/ocontent/".( ($l_bIsVideo) ? 'video' : 'photo' ).'/'.$l_nAssetId.'/';

    //==================================================================================================================
    //                                      GET COMMENTS AND LIKES
    //==================================================================================================================


    $l_sMemcacheKey = Memcaching::makeKey('item-'.$l_sAssetTypeId.'-'.$l_nAssetId.'comments');
    $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    if(!$l_aCachedContent){

        if($l_nAssetId){

            $l_sSql = 'SELECT content_id, COUNT(content_id) as total_likes FROM enr_like WHERE content_id = '.$l_nAssetId.' AND post_type_id = '.$l_sAssetTypeId;
            $l_nTotalLikes = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            $l_nTotalLikes = $l_nTotalLikes['total_likes'];

            $l_aCommentsAndLikes = array();

            $l_aCommentsAndLikes['l_nTotalLikes'] = $l_nTotalLikes;
        }
        Memcaching::getInstance()->store($l_sMemcacheKey,$l_aCommentsAndLikes,60*0.5);
    }else{
        $l_aData = $l_aCachedContent['value'];
        foreach($l_aData as $k => $v){
            $$k = $v;
        }
    }


    $l_bUserLoggedIn = false;

    if($_COOKIE['user'] !="null" && $_COOKIE['user'] !=""){
        $l_xUserData = $_COOKIE['user'];
    }

    $l_nCurrentUserId = null;



    if($_COOKIE['user']){

        $l_nCurrentUserId = $_COOKIE['user'];
        //echo "TODO (1a)";


//exit;
        $l_bUserLoggedIn = true;
        //TODO REMOVE THIS
        //Careful - this queries every time a user connects
        //change this

        $l_sSql = "SELECT content_id, COUNT(content_id) as total_likes FROM enr_like WHERE content_id = $l_nAssetId AND userid = '$l_nCurrentUserId'";

        $l_nUserTotalLikes = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

        $l_nUserTotalLikes = $l_nUserTotalLikes['total_likes'];

    }
} // end of if (!empty($l_xAssetResult))

?>