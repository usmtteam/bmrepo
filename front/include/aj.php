<?
	session_start();
	
	class SCRequestHandler{

		var $_xSQLManager;

		function __construct(){

		}

		public function processRequest(){

			if($_GET && !$_POST){
				$_POST = $_GET;
			}

			
			if(!$_POST['p_sAction']){
				$p_aParts = explode("/",$_SERVER['REQUEST_URI']);
				$_POST['p_sAction'] = array_pop($p_aParts);
				$p_aArgs = $_POST;
			}else{

				$p_aArgs = unserialize(base64_decode($_POST['p_sArgs']));
			
			}


			$l_sAction = 'rq_'.$_POST['p_sAction'];
			if(method_exists($this,$l_sAction)){

				require(__DIR__.'/../config/init.php');
				$this->_xSQLManager = new FrontCoreSql();
        

				$this->{$l_sAction}($p_aArgs);
			}

		}

		private function rq_like(&$p_aArgs){
			
			$l_bAdd = $p_aArgs['add'] == 1;
			$l_nAssetId = intval($p_aArgs['id']);
			$l_nTypeId = intval(trim($p_aArgs['type_id']));
			$l_sType = $l_nTypeId == 1 ? 'video' : 'photo';
			$l_nUserId = $p_aArgs["userid"];


			
			

			$l_xSql = AppController::getInstance()->getConnection();

			if($l_bAdd){
				   $l_sSql = 'INSERT IGNORE INTO enr_like (userid,post_type,post_type_id,content_id,time_stamp)
			            	VALUES ("'.$l_nUserId.'","'.$l_sType.'",'.$l_nTypeId.','.$l_nAssetId.','.time().')';
			}else{
				 $l_sSql = 'DELETE FROM enr_like WHERE userid ="'.$l_nUserId.'" AND content_id ='.$l_nAssetId;
			}
			$l_xSql->runQuery($l_sSql);
		}

		private function rq_comment(&$p_aArgs){

			$l_nAssetId = intval($p_aArgs['id']);
			$l_sMsg = strip_tags(trim($p_aArgs['msg']));
			$l_nTypeId = intval(trim($p_aArgs['type_id']));
			$l_sType = $l_nTypeId == 1 ? 'video' : 'photo';
			$l_nUserId = intval(base64_decode(trim($p_aArgs["uid"])));
			 
			$l_xSql = AppController::getInstance()->getConnection();
			$l_sSql = 'INSERT INTO enr_comment (user_id,post_type,post_type_id,content_id,comment,time_stamp)
 						VALUES ('.$l_nUserId.',"'.$l_sType.'",'.$l_nTypeId.','.$l_nAssetId.',"'.$l_xSql->db_escape_string($l_sMsg).'",'.time().')';
			$l_xSql->runQuery($l_sSql);
			$l_nInsertId = $l_xSql->db_last_insert_id();

			$l_sSql = 'SELECT c.comment,c.time_stamp, f.name, f.img_url, f.facebook_page_id FROM enr_comment c 
	                    LEFT JOIN enr_fan f ON f.id = c.user_id 
	                    WHERE c.id = '.$l_nInsertId;
	        $l_xNewComment = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
	        
			ob_start();
			AppController::getInstance()->outputComments(array($l_xNewComment));
			$l_sHTML = ob_get_clean();
			
			?>
			<script type="text/javascript">
				$('ul.post_comments').prepend(<?= json_encode($l_sHTML) ?>);

				var fbpage = getCookie("facebook_page");
                var username = getCookie("username");
                

                $('.com_img_new').removeClass( 'hidden');
                $('.com_img_new').addClass( 'is-visible');
                $('.com_img_default').removeClass( 'is-visible');
                $('.com_img_default').addClass( 'hidden');
                $('.comment_name_time img.com_img').first().attr('src','https://graph.facebook.com/' + fbpage + '/picture?type=square');
                $('.comment_name a').first().text(username);


			</script>
			<?
		}
	}

	

	// print_R($_POST);
	// print_R($_GET);


	$l_xSCRequestHandler = new SCRequestHandler();
	$l_xSCRequestHandler->processRequest();

?>

