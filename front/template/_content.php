<?php

require_once("../../include/lib/livefyre-php-utils/src/Livefyre.php");
require_once("../../include/lib/php-jwt/src/JWT.php");
require_once("../../include/lib/requests/library/Requests.php");

use Livefyre\Livefyre;
use Livefyre\Exceptions\ApiException;
Requests::register_autoloader();

//$oflourl = "http://".$l_xChannel->slug.".on.oflo.co".$end_of_url;
$oflourl = "http://".$l_xAssetResult['url'].".oflo.co".$end_of_url;

$network = Livefyre::getNetwork("starclub.fyre.co", "9GlDgawpgrNRGprDbwi82+RYm5s=");
//update a Network's name and key
$network->getData()->setName("starclub.fyre.co");
$network->getData()->setKey("9GlDgawpgrNRGprDbwi82+RYm5s=");

//set SSL off
$network->setSsl(false);

//get system and user tokens
$systemToken = $network->buildLivefyreToken();

//make sure a system token is still valid
$isValid = $network->validateLivefyreToken($systemToken);

//get the $network URN
$networkUrn = $network->getUrn();

$site = $network->getSite("379527", "Wlxen62aRHqR5qIPE9fRkbvugWg=");

$newsite = $site->getData()->getId();

$siteUrn = $site->getUrn();

$commentsCollection = $site->buildCommentsCollection($l_sMetaTitle, "prod-oflo-".$l_nAssetId, $oflourl);



$articleId = $commentsCollection->getData();

$commentCollectionMetaToken = $commentsCollection->buildCollectionMetaToken();

$commentCollectionCheckSum = $commentsCollection->buildChecksum();

$commentsCollection->createOrUpdate();

try {
    $test = $commentsCollection->getCollectionContent();
} catch (ApiException $e) {
    print("LOG: can't retrieve content of a collection that has not been created!");
}

?>

<div class="">

    <? include '_menu.php';

    if ($l_sBackgroundImage) : ?>

        <div class="fixed-width margin-bottom"><?= $l_sBackgroundImage ?>
            <h1<?= (strlen($l_sBackgroundImage) > 5) ? ' style="display:none" ' : '' ?>><?= $l_xChannel->site_header_title ?></h1>
        </div>

        <?
    endif;

    if (strpos($l_xAssetResult['destination'], '.mov') !== FALSE){
        $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mov/",".jpg",$l_xAssetResult['destination']));
    }else{
        $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mp4/",".jpg",$l_xAssetResult['destination']));
    }
    $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xAssetResult['url'].'/assets/ufile/'.$l_xAssetResult['image_path'];
    ?>
    <div class="fixed-width">
        <div class="col1">

            <? if($l_bIsVideo){ ?>

            <div class="flowplayer-holder">


                <?php if( (is_numeric($l_xAssetResult['video_type'])) ){ ?>




                <?php  if ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){
                $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                $parsed_url_arr = explode('/', $parsed_url);
                $count = count($parsed_url_arr);

                ?>
                <div class="iframe-instagram">

                    <iframe allowtransparency="true" height="808px" width="640px" frameborder="0" scrolling="no" src="http://instagr.am/p/<?=$parsed_url_arr[$count - 1]?>/embed" ></iframe>



                    <?php } else { ?>

                    <div class="iframe-wrapper">

                        <?php } if ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE){
                            parse_str( parse_url( $l_xAssetResult['video_url'], PHP_URL_QUERY), $end );
                            ?>
                            <iframe width="659" height="422" src="https://www.youtube.com/embed/<?=$end['v']?>" frameborder="0" allowfullscreen></iframe>
                        <?php } elseif ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_VIMEO){
                            $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                            $parsed_url_arr = explode('/', $parsed_url);
                            $count = count($parsed_url_arr);
                            ?>
                            <iframe src="https://player.vimeo.com/video/<?=$parsed_url_arr[$count - 1]?>" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                        <?php } elseif ($l_xAssetResult['video_type'] == EMBED_VIDEO_TYPE_ID_VINE){
                            $parsed_url = parse_url( $l_xAssetResult['video_url'], PHP_URL_PATH);
                            $parsed_url_arr = explode('/', $parsed_url);
                            $count = count($parsed_url_arr);
                            ?>
                            <iframe src="https://vine.co/v/<?=$parsed_url_arr[$count - 1]?>/embed/simple" width="600" height="600" frameborder="0"></iframe>



                        <?php } ?>
                    </div>

                    <?php } else {
                        $path_parts = pathinfo($l_xAssetResult['destination']);
                    ?>

                    <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>

                    <video id="my-video" class="video-js vjs-big-play-centered" controls preload="auto" width="632" height="353.92"
                           poster="<?= $l_sImage ?>" data-setup="{}">

                    <? } else { ?>

                    <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="778" height="438"
                           poster="<?= $l_sImage ?>" data-setup="{}">

                    <? } ?>

<!--                        <source type="application/x-mpegurl"  src="http://d3hl6zmfu9utfu.cloudfront.net/--><?//= $l_xChannel->slug ?><!--/assets/ufile/--><?//=$path_parts['dirname']?><!--/--><?//=$path_parts['filename']?><!--/master.m3u8">-->
                        <source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-480.mp4">
                        <source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-480.webm">
                        <source type='video/flash' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?=$path_parts['dirname']?>/<?=$path_parts['filename']?>-480.mp4">

                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                        </p>
                    </video>
                    <div class="endscreen">
                    <span>
                        <a class="button"
                           href="http://<?=$_SERVER['HTTP_HOST']?>/ocontent/video/<?=$l_xPreviousVideoResult['id']?>">Next video</a>
                        <a class="vjs-toggle">Play it again!</a>
                        <BR><b>Embed code</b>
                        <BR><textarea id="embed"><?=htmlspecialchars('<iframe width="778" height="438" src="http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.preg_replace("/\.(mov|mp4)/","-480.mp4",$l_xAssetResult['destination']).'"></iframe>'); ?>
                        </textarea>
                    </span>
                    </div>

                    <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
                    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-contrib-ads-master-3.1.2/src/videojs.ads.js"></script>
                    <!--                    <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-contrib-ads-1.1.0/src/videojs.ads.js"></script>-->
                    <script src="<?=STATIC_FILES_HOST?>/assets/videojs-ima-master-5.3/src/videojs.ima.js"></script>

                    <?php } ?>

                </div>

                <?
                } else if(!$l_bIsVideo){ ?>
                    <div id="gallery_back">
                        <div id="image-back"><img id="main_img" src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xAssetResult['url'] ?>/assets/ufile/<?php echo $l_xAssetResult['destination'] ?>" />
                        </div>
                        <a class="back_button <?= ($l_xPreviousPhotoResult) ? 'full' : 'inactive' ?>" href="/ocontent/photo/<?= $l_xPreviousPhotoResult['id'] ?>/"><</a>
                        <a class="next_button <?= ($l_xNextPhotoResult) ? 'full' : 'inactive' ?>" href="/ocontent/photo/<?= $l_xNextPhotoResult['id'] ?>/">></a>
                    </div>
                <? } else{} ?>

                <div class="desc-area">
                    <div class="desc">
                        <h3><?= $l_xAssetResult['description'] ?></h3>
                        <br />
                        <span><? echo Utils::time_passed($l_xAssetResult['time_stamp']); ?></span>
                    </div>

                    <div class="social">
                        <ul>
                            <li><a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont fb">f</a></li>
                            <li><a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>?page_type=twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tw">t</a></li>
                            <li><a href="http://tumblr.com/widgets/share/tool?canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tb">u</a></li>
                            <li><a href="https://plus.google.com/share?url=canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont gp">g</a></li>
                        </ul>
                        <div id="likecount_wrapper">
                            <div class="float-right" id="post_like_count">
                                <?= $l_nTotalLikes ?>
                            </div>

                            <div id="addlike" class="a-do-add-like <?= (($l_nUserTotalLikes>0) ? 'icon-star-full' : 'icon-star-empty' ) ?> float-right ng-scope button_star<?= (($l_nUserTotalLikes>0) ? '1' : '' ) ?>" ></div>
                        </div>
                    </div>

                    <div id="livefyre-app-prod-oflo-<?= $l_nAssetId ?>"></div>

                    <script>
                        Livefyre.require(["fyre.conv#3","auth","lfep-auth-delegate#0"],

                            function (Conv, auth, LFEPAuthDelegate) {
                                var authDelegate = new LFEPAuthDelegate({
                                    engageOpts: {
                                        app: "starclub.auth.fyre.co"
                                    }
                                });
                                auth.delegate(authDelegate);
                            });
                    </script>
                    <script>
                        (function() {
                            Livefyre.require(['fyre.conv#3'], function(Conv) {
                                new Conv({
                                    network: 'starclub.fyre.co'

                                }, [{
                                    app: 'main',
                                    siteId: "<?php echo $newsite ?>",
                                    articleId: 'prod-oflo-<?= $l_nAssetId ?>',
                                    el: 'livefyre-app-prod-oflo-<?= $l_nAssetId ?>',

                                }], function (widget) {
                                    // Initialize or Auth

                                    widget.on('commentPosted', function (data) {
                                        // Do something, perhaps using data
                                        //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                        commentPosted(data);
                                    });



                                    widget.on('commentCountUpdated', function (data) {
                                        // Do something, perhaps using data
                                        $.ajax({
                                            type: "GET", // GET, POST , PUT, PATCH
                                            url: "http://<?=OFLO_HOME?>/commentcount.php",
                                            data: {data:data, id:<?= $l_nAssetId ?>},
                                            success: function(data) { // Data will be your HTML code returned from the PHP script.
                                                // Process data object
                                            }
                                        });
                                        commentCountUpdated(data);
                                        //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                    });
                                    widget.on('userLoggedIn', function (data) {
                                        // Do something, perhaps using data

                                        var user = data.id;
                                        var avatar = data.avatar;
                                        var displayname = data.displayName;
                                        setCookie('user', user, 365);
                                        setCookie('avatar', avatar, 365);
                                        setCookie('displayname', displayname, 365);
                                        userLoggedIn(data);


                                        //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                    });

                                    widget.on('userLoggedOut', function (data) {
                                        // Do something, perhaps using data

                                        deleteCookie('user');
                                        deleteCookie('avatar');
                                        deleteCookie('displayname');
                                        userLoggedOut(data);

                                        //window.webkit.messageHandlers.commentCountUpdated.postMessage(data);
                                    });

                                    function commentPosted(data){
                                        return data;
                                    }
                                    function userLoggedIn(data){
                                        return data;
                                    }
                                    function userLoggedOut(data){
                                        return data;
                                    }
                                    function commentCountUpdated(data){
                                        return data;
                                    }


                                });

                            });
                        }());
                    </script>

                </div>

            </div>
            <!-- close div on line 14-->


            <div class="col2" style="text-align: center">
                <div class="page-unit-2 hidden-xs">
                    <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- GlobalUnit-320x50 -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:50px"
                             data-ad-client="ca-pub-7963321383486508"
                             data-ad-slot="6866262471"></ins>
                        <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    <? }else{ ?>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Medium Rectangle -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:300px;height:250px"
                             data-ad-client="ca-pub-7963321383486508"
                             data-ad-slot="8232028077"></ins>
                        <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    <? } ?>
                </div>

                <div class="assets-list">
                    <ul class="nav nav-tabs">
                        <li class="<?= $l_bIsVideo ? 'active': ''?>"><a data-toggle="tab" href="#videos">Videos</a></li>
                        <li class="<?= !$l_bIsVideo ? 'active': ''?>"><a data-toggle="tab" href="#photos">Photos</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="videos" class="tab-pane fade <?= $l_bIsVideo ? 'in active': ''?>">
                            <h5>Latest Videos</h5>
                            <ul>
                                <? AppController::getInstance()->outputOfloVideoListItemsFlowPlayerMobile($l_aFPVideos,$l_xChannel);  ?>
                            </ul>
                        </div>

                        <div id="photos" class="tab-pane fade <?= !$l_bIsVideo ? 'in active': ''?>">
                            <h5>Latest Photos</h5>
                            <ul>
                                <?php foreach($l_aAllFoundAssets as $photo_list_result){ ?>
                                    <a href="/ocontent/photo/<?= $photo_list_result['id'] ?>/">
                                        <li class="vid_thumb_desc">
                                            <div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?= $photo_list_result['url'] ?>/assets/ufile/<?= $photo_list_result['destination']; ?>);">
                                            </div>

                                            <p class="vid_desc"><?= substr($photo_list_result['description'], 0, 45) . '...'; ?><br>
                                                <span class="dates"><? echo Utils::time_passed($photo_list_result['time_stamp']); ?></span>
                                            </p>
                                        </li>
                                    </a>
                                <? } ?>
                            </ul>
                        </div>

                    </div>
                </div>

                <? if($l_bIsMobile){ ?>

            </div>
            <!-- close div on line 77 -->

            <div class="clr"></div>
            <div>
                <? } ?>

                <div class="page-unit-3 hidden-xs">
                    <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

                        <!-- Mobile Large 320x100 -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:100px"
                             data-ad-client="ca-pub-7963321383486508"
                             data-ad-slot="2284614472"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    <? }else{ ?>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Large Skyscraper -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:300px;height:600px"
                             data-ad-client="ca-pub-7963321383486508"
                             data-ad-slot="8908182473"></ins>
                        <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    <? } ?>
                </div>
            </div>

            <div class="clr"></div>
            <!-- tested -->
            <div class="hidden-xs fixed-width">
                <div class="well margin-bottom col-md-12">
                    <div id="myCarousel" class="carousel slide">
                        <p>Related <?php echo $p_cats;?> videos...</p>
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active landing-page-carousel">
                                <div class="row inner-carousel">
                                    <?php $l_xOfloGetCats_count = count($l_xOfloGetCats);
                                    for ($i = 0; $i < $l_xOfloGetCats_count && $i <= 3; $i++) : ?>

                                        <div class="col-sm-3">

                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endfor; ?>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item active-->
                            <?php for ($i; $i < $l_xOfloGetCats_count && $i > 3; $i++) : ?>
                                <!--/item-->
                                <div class="item landing-page-carousel">
                                    <div class="row inner-carousel">
                                        <div class="col-sm-3">

                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                        <?php while($i < $l_xOfloGetCats_count && $i > 3 && (($i + 1) % 4 != 0)): ?>
                                            <div class="col-sm-3">
                                            <?php $i++ ?>
                                                <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                    <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                    echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                    <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                                </a>

                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <!--/row-->

                                </div>
                                <!--/item-->

                            <?php endfor; ?>

                            <!--/carousel-inner-->
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="width:2%; color:black; background-image: none; outline: none;" >
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="width:2%; color:black; background-image: none; outline: none;">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <!--/myCarousel-->
                </div>
                <!-- well -->
            </div>
             <div class="page-unit-1 fixed-width">
            <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

                 <!-- GlobalUnit-320x50 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:100%;height:114px"
                     data-ad-client="ca-pub-7963321383486508"
                     data-ad-slot="6866262471"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            <? }else{ ?>
                <!--/*
  *
  * Starclub Adserver Asynchronous JS Tag
  * - Generated with Starclub Adserver v3.2.1
  *
  */-->

<!--/*  * Replace all instances of INSERT_RANDOM_NUMBER_HERE with
  * a generated random number (or timestamp).
  *
  */-->

<ins data-revive-zoneid="21" data-revive-ct0="INSERT_ENCODED_CLICKURL_HERE" data-revive-id="1971854465e6781f5bd42454610c8a7f"></ins>
<script async src="//scads.starsite.com/www/delivery/asyncjs.php"></script>
            <? } ?>
        </div>
            <!-- tested -->
            <div class="hidden-xs fixed-width">
                <div class="well col-md-12">
                    <div id="myCarousel2" class="carousel slide">
                        <p>Related <?php echo $p_cat;?> videos...</p>
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active landing-page-carousel">
                                <div class="row inner-carousel">
                                    <?php  $l_xOfloGetCat_count = count($l_xOfloGetCat);
                                    for ($i = 0; $i < $l_xOfloGetCat_count && $i <= 3; $i++) : ?>
                                        <div class="col-sm-3">

                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>

                                    <?php endfor; ?>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item active-->
                            <?php for ($i; $i < $l_xOfloGetCat_count && $i > 3; $i++) : ?>
                                <!--/item-->
                                <div class="item landing-page-carousel">
                                    <div class="row inner-carousel">
                                        <div class="col-sm-3">

                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                        <?php while($i < $l_xOfloGetCat_count && $i > 3 && (($i + 1) % 4 != 0)): ?>
                                            <div class="col-sm-3">
                                             <?php $i++ ?>
                                                <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                                </a>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <!--/row-->
                                </div>
                                <!--/item-->
                            <?php endfor; ?>
                            <!--/carousel-inner-->
                        </div>
                        <a class="left carousel-control" href="#myCarousel2" data-slide="prev" style="width:2%; color:black; background-image: none; outline: none;" >
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel2" data-slide="next" style="width:2%; color:black; background-image: none; outline: none;">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <!--/myCarousel-->
                </div>
                <!-- well -->
            </div>
        </div>

        <div class="hidden-lg hidden-md hidden-sm">
            <div class="well margin-bottom">
                <div id="myCarousel3" class="carousel slide">
                    <p>Related <?php echo $p_cats;?> videos...</p>
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="item active landing-page-carousel">
                            <div class="row inner-carousel">

                                <?php $l_xOfloGetCats_count = count($l_xOfloGetCats);
                                for ($i = 0; $i < $l_xOfloGetCats_count && $i <= 1; $i++) : ?>

                                    <div class="col-xs-6">
                                        <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <!--/row-->
                        </div>
                        <!--/item active-->
                        <?php for ($i; $i < $l_xOfloGetCats_count && $i > 1; $i++) : ?>
                            <!--/item-->
                            <div class="item landing-page-carousel">
                                <div class="row inner-carousel">
                                    <div class="col-xs-6">
                                        <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                    <?php while($i < $l_xOfloGetCats_count && $i > 1 && (($i + 1) % 2 != 0)): ?>
                                        <div class="col-xs-6">
                                         <?php $i++ ?>
                                            <a href="http://<?=$l_xOfloGetCats[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCats[$i]['content_type'].'/'.$l_xOfloGetCats[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCats[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCats[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCats[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCats[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                        <?php endfor; ?>
                        <!--/carousel-inner-->
                    </div>
                    <a class="left carousel-control" href="#myCarousel3" data-slide="prev" style="width:1%; color:black; background-image: none; outline: none;" >
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel3" data-slide="next" style="width:1%; color:black; background-image: none; outline: none;">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <!--/myCarousel-->
            </div>
            <!-- well -->
        </div>

        <div class="hidden-lg hidden-md hidden-sm">
            <div class="well margin-bottom">
                <div id="myCarousel4" class="carousel slide">
                    <p>Related <?php echo $p_cat;?> videos...</p>
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <div class="item active landing-page-carousel">
                            <div class="row inner-carousel">

                                <?php $l_xOfloGetCat_count = count($l_xOfloGetCat);
                                for ($i = 0; $i < $l_xOfloGetCat_count && $i <= 1; $i++) : ?>

                                    <div class="col-xs-6">
                                        <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <!--/row-->
                        </div>
                        <!--/item active-->
                        <?php for ($i; $i < $l_xOfloGetCat_count && $i > 1; $i++) : ?>
                            <!--/item-->
                            <div class="item landing-page-carousel">
                                <div class="row inner-carousel">
                                    <div class="col-xs-6">
                                        <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                    </div>
                                    <?php while($i < $l_xOfloGetCat_count && $i > 1 && (($i + 1) % 2 != 0)): ?>
                                        <div class="col-xs-6">
                                         <?php $i++ ?>
                                            <a href="http://<?=$l_xOfloGetCat[$i]['url'].'.'.OFLO_LANDING ?>/ocontent/<?php echo $l_xOfloGetCat[$i]['content_type'].'/'.$l_xOfloGetCat[$i]['id'] ?>/">
                                                <img class="content_carousel" style='background-image:url("<?php echo CDN_FRONT.'/'.$l_xOfloGetCat[$i]['url'].'/assets/ufile/';
                                                echo $l_xOfloGetCat[$i]['destination'];?>" );' class="img-responsive response margin">
                                                <strong><p><?=substr($l_xOfloGetCat[$i]['description'],0,40); ?><? $strlen =strlen($l_xOfloGetCat[$i]['description']); if($strlen>40){echo "...";} ?></p></strong>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->
                        <?php endfor; ?>
                        <!--/carousel-inner-->
                    </div>
                    <a class="left carousel-control" href="#myCarousel4" data-slide="prev" style="width:1%; color:black; background-image: none; outline: none;" >
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel4" data-slide="next" style="width:1%; color:black; background-image: none; outline: none;">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <!--/myCarousel-->
            </div>
            <!-- well -->
        </div>
    </div>
    <div class="clr"></div>
    <div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10">
        <p><a target="_blank" href="http://starsite.com"><img class="powered-by" src="/assets/images/powered-by-starsite.png"></a></p>
    </div>
</div>

<?php
$siteurl = "http://".$l_xChannel->slug.".on.oflo.co";
$end_of_url = str_replace($siteurl,"",$l_sCurrentAbsoluteURLNoVars);
?>

<script type="text/javascript">

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
    ga('set', 'page', '<?= $end_of_url ?>');
</script>

<script>

    var player = videojs('my-video');

    //                player.ga({
    ////                    'percentsPlayedInterval': 25,
    ////                    'eventsToTrack': ['loaded', 'percentsPlayed', 'start', 'end', 'error'],
    //                    'debug': true
    //                });

    player.ready( function(){
        console.log('++++++++++++++++ready');
        var dur = Math.round(this.duration());
        var pause = Math.round(dur * .25);
        var pausehalf = Math.round(dur * .5);
        var pausethreefourths = Math.round(dur * .75);

        ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Impression', dur);
        setTimeout(function(){
            ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
        },pause);
        setTimeout(function(){
            ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Half', dur);
        },pausehalf);
        setTimeout(function(){
            ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Three-Fourths', dur);
        },pausethreefourths);

        this.on('play', function(){
            console.log('++++++++++++++++play');

        }).on('ended', function(){
            console.log('++++++++++++++++ended');
            var dur = Math.round(this.duration());
            ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);

            $('.endscreen').show();

        }).on('error', function(){
            console.log('++++++++++++++++error');
            var dur = Math.round(this.duration());
            ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);
        });

    });

    <?php if (preg_match("/android/i", $Device_Make)){ ?>

        $(function() {

            $("#my-video").one("tap", tapHandler);

            function tapHandler(event) {
                var player = videojs('my-video');

                //                console.log('tapHandler: ');
                //                console.log(event);

                var options = {
                    id: 'my-video',
                    //                    debug: true,
                    //                    adTagUrl: 'http://adsite.starsite.com/simple/'
                    adTagUrl: '<?=$l_sAdServerURL?>'
                };

                player.ima(options);

                // On mobile devices, you must call initializeAdDisplayContainer as the result
                // of a user action (e.g. button click). If you do not make this call, the SDK
                // will make it for you, but not as the result of a user action. For more info
                // see our examples, all of which are set up to work on mobile devices.
                player.ima.initializeAdDisplayContainer();
                player.ima.requestAds();
                //                var adsMan = player.ima.getAdsManager;
                //                    .resize(77, 43, 'normal');

                player.play();
            }
        });
    <?php } else { ?>

        var options = {
            id: 'my-video',
            //                    debug: true,
            //                    adTagUrl: 'http://adsite.starsite.com/simple/'
            adTagUrl: '<?=$l_sAdServerURL?>'
        };

        player.ima(options);

        // On mobile devices, you must call initializeAdDisplayContainer as the result
        // of a user action (e.g. button click). If you do not make this call, the SDK
        // will make it for you, but not as the result of a user action. For more info
        // see our examples, all of which are set up to work on mobile devices.
        player.ima.initializeAdDisplayContainer();
        player.ima.requestAds();
        //                var adsMan = player.ima.getAdsManager;
        //                    .resize(77, 43, 'normal');

        player.play();

    <?php } ?>


</script>

<script>
    $(function() {

        $('.flowplayer-holder .video-js, .vjs-toggle').click(function () {
            $('.endscreen').hide();
        });

        $('.vjs-toggle').click(function () {
            var player = videojs('my-video');
            player.play();
        });

    });
</script>



 