<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viewhelp extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->chanel = $this->session->userdata('chanel');
        $this->data['chanel'] = $this->chanel;
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
        header('Access-Control-Allow-Origin: *');
        
    }
    
    function _remap($method) {
        $this->{$method}();
    }
    
    function index(){
    }
    
    function shop(){
        $cid = $this->uri->segment(3);
        $helps = $this->main_m->getHelpsContent($cid, 'shop');
        $this->data = $helps;
        $this->load->view('viewhelp', $this->data);
    }
    function tour(){
        $cid = $this->uri->segment(3);
        $helps = $this->main_m->getHelpsContent($cid, 'tourdate');
        $this->data = $helps;
        $this->load->view('viewhelp', $this->data);
    }
    function help(){
        $cid = $this->uri->segment(3);
        $helps = $this->main_m->getHelpsContent($cid, 'help');
        $this->data = $helps;
        $this->load->view('viewhelp', $this->data);
    }
    
    function terms(){
        $cid = $this->uri->segment(3);
        $helps = $this->main_m->getHelpsContent($cid, 'terms');
        $this->data = $helps;
        $this->load->view('viewhelp', $this->data);
    }
    
    function pravacy(){
        $cid = $this->uri->segment(3);
        $helps = $this->main_m->getHelpsContent($cid, 'pravacy');
        $this->data = $helps;
        $this->load->view('viewhelp', $this->data);
    }

}

