<?
  // NO CHANGES NEEDED
?>
<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
</script>
<? /*

  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.URL]);
  _paq.push(["setCookieDomain", "<?= $l_xChannel->domain_cookie ?>"]);
  _paq.push(["setCustomVariable", 1, "userid", getCookie("ss_userid"), "visit"]);
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);


  var _qevents = _qevents || [];
  (function() {
  var elem = document.createElement('script');
  elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
  elem.async = true;
  elem.type = "text/javascript";
  var scpt = document.getElementsByTagName('script')[0];
  scpt.parentNode.insertBefore(elem, scpt);
  })();

  _qevents.push({
  qacct: "<?= $l_xChannel->quantserve_key ?>"
  });
</script>
<noscript>
  <div style="display:none;"><img src="//pixel.quantserve.com/pixel/<?= $l_xChannel->quantserve_key ?>" border="0" height="1" width="1" alt="Quantcast"/></div>
</noscript>


   */ ?>



<script>

    //if (/flash/.test(location.search)) 
        //flowplayer.conf.engine = "flash";

    // for demo info, not in production set up api to report info
    
$(window).load(function () {
        var cdn = "<?= $l_sCDNNoProtocol ?>",
        base = "<?= $l_xChannel->url ?>/assets/ufile/<?=$video_destination?>",
        label = !flowplayer.support.inlineVideo ? "-" + "360" : flowplayer.support.firstframe ? "-" + "480" : "";

  
        var playerElem = $("#myPlayer");
        
        playerElem.flowplayer({
        rtmp: "rtmp://s2otr7ksbek4s2.cloudfront.net/cfx/st",
        key: "$846027373127464",
        advertising: {
            ads: [
                { time : 0, adTagUrl : "<?=$zone;?>" }
            ]            
        },
        ratio: 9/16,
        scaling: "fit",
        logo : 'http://sccms.starsite.com/assets/img/star.png',
        preload:true,
        controls: true,
        analytics: "<?= $l_xChannel->google_analytics_key ?>",
        onMouseOut: function() {
            $("#myPlayer").getControls().show();
        },
        
        playlist: [[   
          { mpegurl: cdn + "/" + base + "/master.m3u8" },
          { webm:    cdn + "/" + base + label + ".webm" },   
          { mp4:     cdn + "/" + base + label + ".mp4" },      
          { flash:   "mp4:" + base + label }
        ]],
        plugins: {
            controls: {
                playlist: true,
                mouseOutDelay: 100,
                hideDelay: 100
            }

        },
        autoplay: true
      });

      playerElem.on("load", function(e, api, video) {

      }).on("ready", function(e, api, video) {
         $(".flowplayer").css("left", "0px"); $(".ng-scope_video").css("z-index","0"); $(".ng-scope_video").css("opacity","1.0");  $(".loader").css("display", "none"); 
        var dur = parseInt(api.video.duration);
        
        ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Played', dur);
          ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
       

      });


      playerElem.on("finish", function(e, api) {
        
        var dur = parseInt(api.video.duration);
        
        ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);
        <? if($l_xPreviousVideoResult['id']!=""){ ?>
           var sec = 10
                 
                    var timer = setInterval(function() {
                       $("#mdtimer span").text(sec--);
                       if (sec == 0) {
                        var url = "/content/video/<?=$l_xPreviousVideoResult['id']?>";    
                        $(location).attr('href',url);
                    
                        $("#mdtimer").hide(1000) .fadeOut(100);}

                    },1000);
                  

                   $('#comment_form input#user_id').focus(function () { 
                       clearInterval(timer);
                       timer = null;
                       $("#mdtimer").html("");
                       $("#mdtimer").append("<br>PAUSED");
                    });

                    $('a.replay').click(function() {
                                   clearInterval(timer);
                       timer = null;
                       $("#mdtimer").html("");

                       location.reload();

  
                  });

                   
        <? } ?>
      });

      playerElem.on("error", function(e, api) {
        
        var dur = parseInt(api.video.duration);
        
        ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);
      });


    });



</script>


<iframe src="/template/tracking_master.php?t=<?= $l_xChannel->domain ?>" width="0" height="0" frameborder="none"></iframe>
