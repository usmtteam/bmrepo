<?php

   include(__DIR__.'/../config/init.php');
header("Location: http://geek.starsite.com/ad-sense/adsense-sample.php");
die();
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en" ng-app = "enriqueApp"> <!--<![endif]-->
<?
    
    //==================================================================================================================
    //                                              HEAD
    //==================================================================================================================
?>
<head>
    <meta charset="utf-8" />
    <title><?= $l_xChannel->site_title ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?= STATIC_FILES_HOST ?>/css/font-awesome.min.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/bootstrap.min.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/style-metronic.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/style.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/style-responsive.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/themes/blue.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?= STATIC_FILES_HOST ?>/css/design.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/custom.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/highlights.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/switchery.min.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <? /*
    <script type="text/javascript">
    if (window.location.href.indexOf("#/") > -1) {

    }else {
    window.location="/viewpost_auth.php?video=1090";

    }
    </script>
    */ ?>
    <link href="<?= STATIC_FILES_HOST ?>/images/favicon.ico?v=<?= RELEASE_VERSION ?>" rel="shortcut icon" />
    <link href="<?= STATIC_FILES_HOST ?>/css/mobile_menus.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" />
    <link href="<?= STATIC_FILES_HOST ?>/css/jquery.smartbanner.css?v=<?= RELEASE_VERSION ?>" rel="stylesheet" type="text/css" media="screen" />

    <script type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
    <script type="text/javascript" src="<?= STATIC_FILES_HOST ?>/scripts/modernizr.custom.js?v=<?= RELEASE_VERSION ?>"></script>
    <script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/jquery/jquery-1.11.0.min.js?v=<?= RELEASE_VERSION ?>" type="text/javascript"></script>
    <script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/jquery/jquery-migrate-1.2.1.min.js?v=<?= RELEASE_VERSION ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?= STATIC_FILES_HOST ?>/scripts/xStarsite.js?v=<?= RELEASE_VERSION ?>"></script>
    <script type="text/javascript" src="<?= STATIC_FILES_HOST ?>/scripts/xUtils.js?v=<?= RELEASE_VERSION ?>"></script>
    <script>
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = expires;
    </script>
    <? require(INCLUDE_ANGULAR_VARIABLES); ?>
</head>
<?
    
    //==================================================================================================================
    //                                              BODY
    //==================================================================================================================
?>
<body ng-controller="indexController" ng-init="init(<?= $l_xChannel->cid; ?>)" class="cbp-spmenu-push">
	
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1440671026187879',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

<div id="container">
    <div id="jqSmartBanner"></div>
        
    <header>
      <div id="header">
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left menuBanner" id="cbp-spmenu-s1">
          <ul id="menu">
            <li ng-class="{active:current_page=='homefeed' || current_page ==''}" class="no-border-top"><a href="/viewpost_auth.php?video=1078">Videos</a></li>
            <li ng-class="{active:current_page=='community'}"><a href="#community">Upload a Video</a></li>
            <div id="login_panel_menu"> <a id="button_login" ng-if="current_user == null" ng-click="onShowLoginButton_click()">Login</a> <a id="button_signup" ng-if="current_user == null" ng-click="onShowSignupButton_click()">Sign up</a> 
              
              <!--<img class="current_user_photo" ng-src="{{ current_user.img_url }}" ng-if="current_user != null && current_user.img_url != ''" />
                <img class="current_user_photo" ng-src="/images/default_photo.png" ng-if="current_user != null && current_user.img_url == ''" />-->
              <ul style="margin-top:70px;" class="nav navbar-nav navbar-right" ng-if="current_user != null">
                <li class="dropdown">
                <li><a href="#viewprofile/{{ current_user.id }}_{{ current_user.name}}">My profile</a></li>
                <li><a href="#settings">Settings</a></li>
                <li><a href="#inbox">Inbox</a></li>
                <li><a href="#" ng-click="onLogout()">Logout</a></li>
              </ul>
            </div>
            </li>
          </ul>
        </nav>
        <section>
          <div class="layout_head">
            <div id="mb_border_mobile">
              <div id="mb_border_s">
                <button id="showLeftPush" title="click here for navigation" class="outer"></button>
              </div>
            </div>
            <ul class="nav navbar-nav navbar-left" ng-if="current_user != null">
              <li class="dropdown"> 
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <div id="mb_border">
                      <div id="mb_border_s">
                        <button id="showLeftPush" title="click here for navigation"></button>
                      </div>
                    </div>
                </a>
                <ul class="dropdown-menu">
                  <li ng-class="{active:current_page=='homefeed' || current_page ==''}" class="no-border-top"><a href="/viewpost_auth.php?video=1078">Videos</a></li>
                  <li ng-class="{active:current_page=='community'}"><a href="#community">Upload a Video</a></li>
                </ul>
              </li>
            </ul>
            <div id="logo"></div>
            <div id="login_panel">
              <div id="lp_border"> 
                <a id="button_login" ng-if="current_user == null" ng-click="onShowLoginButton_click()">Login</a> <a id="button_signup" ng-if="current_user == null" ng-click="onShowSignupButton_click()">Sign up</a> 
                <img class="current_user_photo" ng-src="{{ current_user.img_url }}" ng-if="current_user.facebook == 0 && current_user != null && current_user.img_url != '' && current_user.img_url != 'http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/'" onerror="javascript:this.src='<?= DEFAULT_AVATAR_IMAGE ?>'" /> 
                <img class="current_user_photo" ng-src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/photo/default_photo.png" ng-if="current_user.facebook == 0 && current_user == null && (current_user.img_url == '' || current_user.img_url == 'http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/')" onerror="javascript:this.src='<?= DEFAULT_AVATAR_IMAGE ?>'" /> 
                <img class="current_user_photo" ng-src="http://graph.facebook.com/{{ current_user.facebook_page_id}}/picture?type=large" ng-if="current_user.facebook == 1 || current_user.img_url == ''">
                <ul class="nav navbar-nav navbar-right" ng-if="current_user != null">
                  <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle" ng-cloak>{{ current_user.firstname }}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li class="no-border-top"><a href="#viewprofile/{{ current_user.id }}_{{ current_user.name}}" ng-cloak>My profile</a></li>
                      <li><a href="#settings">Settings</a></li>
                      <li><a href="#inbox">Inbox</a></li>
                      <li><a href="#help">Help</a></li>
                      <li><a href="#" ng-click="onLogout()">Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </div>
    </header>

<?
    
    //==================================================================================================================
    //                                              LAYOUT
    //==================================================================================================================
?>

    <div class="layout">
      <section>
        <div ng-view id="page"> </div>
      </section>
    </div>
</div>


<audio ng-if="music_play && music_file != ''" autoplay loop>
    <source ng-src="{{music_file}}" type="audio/mpeg">
</audio>


<?
    
    //==================================================================================================================
    //                                              TRACKING
    //==================================================================================================================
?>


<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/bootstrap/bootstrap.min.js?v=<?= RELEASE_VERSION ?>" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.0.0/handlebars.min.js"></script>
<!--[if lt IE 9]>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/respond/respond.min.js?v=<?= RELEASE_VERSION ?>"></script>  
<![endif]-->   
<!-- END CORE PLUGINS -->   

<script src="<?= STATIC_FILES_HOST ?>/scripts/metro_app.js?v=<?= RELEASE_VERSION ?>" type="text/javascript"></script>    
<script type="text/javascript">
$(document).ready(function(e){
    MetroApp.init();                    
});
</script>
<script src="//cdn.jsdelivr.net/headroomjs/0.5.0/headroom.min.js"></script>
<script src="//cdn.jsdelivr.net/headroomjs/0.5.0/angular.headroom.min.js"></script>
<script src="//cdn.jsdelivr.net/headroomjs/0.5.0/jQuery.headroom.min.js"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-route.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-cookies.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-animate.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-sanitize.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-file-upload-shim.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angular-file-upload.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/ng-infinite-scroll.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/common/common.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/common/ss_ad.js?v=<?= RELEASE_VERSION ?>"></script>

<script type="text/javascript" src="/ssstats/t.js?v=<?= RELEASE_VERSION ?>"></script>

<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/bootstrap/ui-bootstrap-tpls-0.10.0.min.js?v=<?= RELEASE_VERSION ?>" type="text/javascript"></script> 
<script src="<?= STATIC_FILES_HOST ?>/scripts/app.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/swichery/ng-switchery.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/swichery/switchery.min.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/vendor/angular.js/angularytics.js?v=<?= RELEASE_VERSION ?>"></script>
<!-- Directives -->
<script src="<?= STATIC_FILES_HOST ?>/scripts/directives/feedItemDirective.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/directives/newPostDirective.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/directives/brightCovePlayerDirective.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/directives/attrOnOffDirective.js?v=<?= RELEASE_VERSION ?>"></script>
<!-- Controllers -->
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/indexController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/loginController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/loginEmailController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/signupController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/mainFeedController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/draftsController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/communityController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/photosController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/videosController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/tourDatesController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/shopController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/rankingController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/viewProfileController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/viewPostController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/settingsController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/inboxController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/sendMessageController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/viewMessageController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/pollsAndQuizzesController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/confirmDialogController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/pollQuizController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/musicPlayerController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/likeUsersController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/approvalUsersController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/helpController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/viewHelpController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/buyCreditsController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/selectFacebookPageController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/syndicateController.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/classie.js?v=<?= RELEASE_VERSION ?>"></script>
<script src="<?= STATIC_FILES_HOST ?>/scripts/controllers/previewController.js?v=<?= RELEASE_VERSION ?>"></script>
<script type="text/javascript">
  $(document).ready(function(e){
    $("#header").headroom();
  });
</script>
<? require(INCLUDE_TRACKING); ?> 
</body>
</html>