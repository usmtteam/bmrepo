<?php

require_once 'vendor/autoload.php';


use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\CreditCard;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Transaction;


function createCreditCard($card_info) {
	$apiContext = new ApiContext(new OAuthTokenCredential(
			PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));
	
	$card = new CreditCard();
	$card->setType($card_info['type'])
	->setNumber($card_info['number'])
	->setExpireMonth($card_info['expire_month'])
	->setExpireYear($card_info['expire_year'])
	->setCvv2($card_info['cvv2'])
	->setFirstName($card_info['first_name'])
	->setLastName($card_info['last_name']);
	
	$card->create($apiContext);
	
	if($card->getState() == 'ok') {
		return $card->getId();
	}
	return '';
}

/**
 * 
 * @param unknown $card_id
 * 
 * return
 * {
 * 		"card_type" : "visa"
 * 		"number" : "number"
 * 		"expire_month" : "04"
 * 		"expire_year" : "2014"
 * 		"cvv2" : "cvvw"
 * 		"first_name" : "first_name"
 * 		"last_name" : "last_name"
 * }
 */
function getCreditCard($card_id) {

	$apiContext = new ApiContext(new OAuthTokenCredential(
			PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));
	
	$card = CreditCard::get($card_id, $apiContext);
	
	return array(
		'card_type' => $card->getType(),
		'number' => $card->getNumber(),
		'expire_month' => $card->getExpire_month(),
		'expire_year' => $card->getExpire_year(),
		'cvv2' => $card->getCvv2(),
		'first_name' => $card->getFirst_name(),
		'last_name' => $card->getLast_name()	
	);
}
/**
 * 
 * @param unknown $card_id
 * @param unknown $detail
 * 	{
 * 		"items" : [
 * 			{
 * 				"name" : "Ground cofee 40 oz",
 * 				"quantity" : "1"
 * 				"price" : "7.50"
 * 			}
 * 		],
 * 		"currency" : "USD"
 * 		"shipping_price": "1.20",
 * 		"tax" : "1.30"
 * 		"description" : "bla bla bla..."
 * 	}
 */
function createPayment($card_id, $detail) {

	$apiContext = new ApiContext(new OAuthTokenCredential(
			PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));
	
	$creditCardToken = new CreditCardToken();
	$creditCardToken->setCreditCardId($card_id);
	
	$fi = new FundingInstrument();
	$fi->setCreditCardToken($creditCardToken);
	
	$payer = new Payer();
	$payer->setPaymentMethod("credit_card")
		->setFundingInstruments(array($fi));
	
	
	$sub_total = 0;
	$total = 0;
	$items = array();
	foreach($detail['items'] as $item_detail) {
		$item = new Item();
		$item->setName($item_detail['name'])
			->setCurrency($detail['currency'])
			->setQuantity($item_detail['quantity'])
			->setPrice($item_detail['price']);
		
		$items[] = $item;
		
		$sub_total += $item_detail['quantity'] * $item_detail['price'];
	}
	
	$itemList = new ItemList();
	$itemList->setItems($items);
	
	$details = new Details();
	$details->setShipping(paypal_price_format($detail['shipping_price']))
		->setTax(paypal_price_format($detail['tax']))
		->setSubtotal(paypal_price_format($detail['sub_total']));
	
	
	$total = $sub_total + $detail['shipping_price'] + $detail['tax'];
	$amount = new Amount();
	$amount->setCurrency($detail['currency'])
		->setTotal(paypal_price_format($total))
		->setDetails($details);
	
	
	$transaction = new Transaction();
	$transaction->setAmount($amount)
		->setItemList($itemList)
		->setDescription($detail['description']);
	
	$payment = new Payment();
	$payment->setIntent("sale")
		->setPayer($payer)
		->setTransactions(array($transaction));
	
	try {
		$payment->create($apiContext);
		
		if($payment->getState() == 'approved')
			return $payment->getId();
		else
			return '';
	} catch (PayPal\Exception\PPConnectionException $ex) {
		return "";
	}
		
}

function paypal_price_format($price) {
	return number_format((float)$price, 2, '.', '');
}