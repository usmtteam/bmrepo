      <!-- BEGIN PAGE -->  
      <div class="page-content">

         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <h3 class="page-title">
                     Edit fan
                     <small><?php echo $user->first_name." ".$user->last_name; ?></small>
                  </h3>
				<?php echo $message;?>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
               <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box">

                        <div id="form_head_back">Edit Fan</div>
                      
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                                <?php echo form_open_multipart(uri_string(),'class="form-horizontal"');?>
                        <div class="row-fluid">
                            <div class="span7">

                                      <div class="control-group">
                                            <label for="first_name" class="control-label">First Name:</label>
                                            <div class="controls">
                                            <?php echo form_input($first_name,'','class="span6 m-wrap"');?>
                                            </div>
                                      </div>

                                      <div class="control-group">
                                            <label for="last_name" class="control-label">Last Name:</label>
                                            <div class="controls">
                                            <?php echo form_input($last_name,'','class="span6 m-wrap"');?>
                                            </div>
                                      </div>

                                      <div class="control-group">
                                            <label for="username" class="control-label">Username:</label>
                                            <div class="controls">
                                            <?php echo form_input($username,'','class="span6 m-wrap"');?>
                                            </div>
                                      </div>

                                      <div class="control-group">
                                            <label for="email" class="control-label">Email:</label>
                                            <div class="controls">
                                            <?php echo form_input($email,'','class="span6 m-wrap"');?>
                                            </div>
                                      </div>
                                      
                                      <div class="control-group">
                                            <label for="phone" class="control-label">Phone:</label>
                                            <div class="controls">
                                            <?php echo form_input($phone,'','class="span6 m-wrap"');?>
                                            </div>
                                      </div>
                                      
                                      <!--<div class="control-group">
                                            <?php echo lang('edit_user_company_label', 'company');?>
                                            <div class="controls">
                                            <?php echo form_dropdown("company",$companies,$user->company, 'class="span6 m-wrap"');?>
                                            </div>
                                      </div>-->

                                      <div class="control-group">
                                            <?php echo lang('edit_user_password_label', 'password');?>
                                            <div class="controls">
                                            <?php echo form_input($password,'','class="span6 m-wrap"');?>
                                            <span class="help-inline"> (if changing password) </span>
                                            </div>
                                      </div>

                                      <div class="control-group">
                                            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
                                            <div class="controls">
                                            <?php echo form_input($password_confirm,'','class="span6 m-wrap"');?>
                                            <span class="help-inline"> (if changing password) </span>
                                            </div>
                                      </div>
                                      
                                      <?php echo form_hidden('id', $user->id);?>
                                      <?php echo form_hidden($csrf); ?>


                            </div>
                            <div class="span5">
                                <div class="fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img id='video_img' style="max-height: 100%" src="<?php echo ((isset($user->img_url) && $user->img_url) ? UPLOAD_URL.$user->img_url : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="image" class="default" id="videoPhotoUpload" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                        </div>
                                      <div class="form-actions">
                                          <?php echo form_submit('submit', lang('edit_user_submit_btn'),' class="btn blue"');?>
                                      </div>
                                <?php echo form_close();?>

                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>
    <!-- END PAGE CONTENT--> 
	
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 

