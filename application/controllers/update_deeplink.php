<?php
class Update_DeepLink extends CI_Controller {
	public function index() {

		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('json_loader');
		$this->load->helper('bitly');
        $this->load->model('main_m');
        
        $this->update('text');
        $this->update('photo');
        $this->update('video');
        $this->update('poll');
        $this->update('quiz');
	}
	
	private function update($post_type) {
		echo $post_type;
		$query = 'select * from enr_' . $post_type;
		$list = $this->main_m->db->query($query)->result_array();
		echo count($list);
		foreach($list as $row) {
			$this->chanel = $this->main_m->get_chanel($row['cid']);
			
			$this->main_m->updateDeepLink($post_type, $row['id']);
		}
	}
}