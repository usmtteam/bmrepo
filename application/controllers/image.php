<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );

        
    }
    
    function _remap($method) {
        $this->{$method}();
    }
    
    function index(){
    }
    function watermark(){
		
    }
	
    function no_watermark(){
		
		//source image
		$path = str_replace("-", "/", $this->uri->segment(3));
        $file = UPLOAD_DIR. $path;
		//get original size
		list($sw, $sh) = getimagesize($file);		
		
		//calculate simulated size
		$width = $this->uri->segment(5);
		if(empty($width) || $width == NULL) { $width = $sw; $height = $sh; }
		else {
			$height = round($sh * $width / $sw);
		}

		$cid = $this->uri->segment(4);

		//water mark image
		
		//load original image
		$ext = exif_imagetype ($file);
		$hSource = NULL;		
		if($ext == IMAGETYPE_PNG) $hSource = imagecreatefrompng ($file);
		elseif($ext == IMAGETYPE_JPEG) $hSource = imagecreatefromjpeg ($file);
		elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hSource = imagecreatefromwbmp ($file);
		elseif($ext == IMAGETYPE_GIF) $hSource = imagecreatefromgif ($file);
		//
		
		//result image
		$hDest = imagecreatetruecolor($width, $height);
		imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);
		
		ob_start();
		imagejpeg($hDest);		
		
		$contents = ob_get_clean();
		header('Content-Type: image/jpeg');
		header("Content-length: " . strlen($contents));

		echo $contents;
		
		imagedestroy($hDest);
		imagedestroy($hSource);
    }

    function videoplay(){
        $class = new Watermark();
        $path = str_replace("-", "/", $this->uri->segment(3));
        $file = UPLOAD_DIR. $path;
        
        $watermark = RT_PATH.'/assets/img/play.png';

        $position = 0;
        $class->showImage($file, $watermark, $position);
    }

}

