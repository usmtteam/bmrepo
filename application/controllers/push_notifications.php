<?php
class Push_Notifications extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'message', 'required');
                $this->form_validation->set_rules('title', 'title', 'required');

		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('main_m');
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();
    $this->data['is_oflo'] =  $this->main_m->isChanelOflo();

	}
	
  function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }



	function index(){
          $this->data['platform_notification'] = $this->main_m->get_platform_notification();
         
          $this->load->view('push_notifications', $this->data);
    }
    
    
 

     public function notifications_edit(){
        
        $id = $this->uri->segment(3);

        $this->data['platform_names'] = $this->main_m->get_platform_names();

        $this->data['push_recipients'] =$this->main_m->get_push_recipients();

        $this->data['oflo_channels'] =$this->main_m->get_oflo_channels();

       $this->data['oflo_users'] =$this->main_m->get_oflo_users();

      // $this->data['oflo_follows'] = $this->main_m->get_followers_id();
  
        $this->data['edit_notification'] = $this->main_m->get_notifications_all();




        $this->data['notification'] = $this->main_m->get_notification_id($id);
        $this->data['id'] = $this->data['notification'][0]['id'];
        // var_dump($this->data['category']);
        // var_dump($this->data['cat_name']);
        $this->data['platform_id'] = $this->data['notification'][0]['platform_id'];
        $this->data['recipients'] = $this->data['notification'][0]['recipients'];
        $this->data['channel_recipients'] = $this->data['notification'][0]['channel_recipients'];
        $this->data['followers'] = $this->data['notification'][0]['followers'];
        $this->data['user_recipients'] = $this->data['notification'][0]['user_recipients'];
        $this->data['title'] = $this->data['notification'][0]['title'];
        $this->data['message'] = $this->data['notification'][0]['message'];
        $this->data['date'] = $this->data['notification'][0]['date'];
        $this->data['status'] = $this->data['notification'][0]['status'];
        
      if ($this->input->post('submit')){  
        $this->update_notification_id($id);
      }

        $this->load->view('notifications_edit', $this->data);
    }


    public function update_notification_id($id) {
     
   
      $platform_id= $this->input->post('platform_id');
      $recipients = $this->input->post('recipients');
      $channel_recipients = $this->input->post('channel_recipients');
      $followers = $this->input->post('followers');
      $user_recipients = $this->input->post('user_recipients');
      $subject = $this->input->post('title');
      $message = $this->input->post('message');
      $date = $this->input->post('date');
      $status = $this->input->post('status');

      $data = array (
            'platform_id' => $platform_id, 
            'recipients' => $recipients,
            'channel_recipients' => $channel_recipients,
            'followers' => $followers,
            'user_recipients' => $user_recipients,
            'title' => $subject,
            'message' => $message,
            'date' => $date,
            'status' => $status
        );


            if($this->main_m->update_notification($id, $data))
            {
                redirect(site_url('push_notifications/'));
            }
        
        return;
    
      
    }


     public function notifications_add($post){

       $this->data['platform_names'] = $this->main_m->get_platform_names();
       $this->data['push_recipients'] =$this->main_m->get_push_recipients();
       $this->data['oflo_channels'] =$this->main_m->get_oflo_channels();
       $this->data['oflo_users'] =$this->main_m->get_oflo_users();
       
      // $this->data['oflo_follows'] = $this->main_m->get_followers_id();
  
  

          $this->form_validation->set_rules('message', 'message', 'required|max_length[512]|xss_clean');

          $this->form_validation->set_rules('title', 'Subject', 'required|max_length[100]|xss_clean');

        if ($this->form_validation->run()==false){}
            else
{
        if ($this->form_validation->run() == true) 

        { 

        
             if($this->input->post('message',false) == true)

            {
                
             
                {



        $this->data['platform_notification'] = $this->main_m->add_platform_notification();


         $l_sCommand = PHP_PATH.' '.PUSH_NOTIFICATION;
       
        shell_exec($l_sCommand);  
 


          redirect(site_url('push_notifications/'));
                }
            }
           
              
                {
                  
                    if(empty($message)){
            show_error("Enter a message!");
              return;
                
                    }
                }

            }
           
            
          
        }
                  

    
        
        $this->load->view('notifications_add', $this->data);
    }



      
  


     public function confirm_deletion(){
     
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("platform_notification");
        redirect(site_url('push_notifications/'));
     }


 

 
}
	