<?php // Throws exception on missing Zencoder Job ID record

require_once(__DIR__.'/../../../config/settings.php');
require_once(__DIR__.'/../front/FrontCoreSql.php');
require_once(__DIR__.'/../front/FrontCoreManager.php');
define('ZENCODER_API_KEY','b34cc8e8b92ae58bf3f1a151608a0ef6');
define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');

try {
    $l_xFrontCoreSql = new FrontCoreSql();

    // Store Job/Output IDs to update their status when notified or to check their progress.
//    $l_sSql = 'SELECT * FROM enr_video
//               WHERE video_duration = 0
//               AND zencoder_id IS NOT NULL
//               AND id = 2851
//               OR id = 2850
//               ORDER BY id DESC';

    $l_sSql = 'SELECT * FROM enr_video
               WHERE video_duration = 0
               AND zencoder_id IS NOT NULL
               AND zencoder_id != "197274203"
               AND zencoder_id != "197451759"
               AND zencoder_id != "235525719"
               AND zencoder_id != "235523968"
               AND zencoder_id != "235522283"
               AND zencoder_id != "235504976"
               AND zencoder_id != "235492380"
               AND zencoder_id != "235483921"
               AND zencoder_id != "235270258"
               AND zencoder_id != "235266440"
               AND zencoder_id != "235262038"
               AND zencoder_id != "234762097"
               AND zencoder_id != "234582856"
               AND zencoder_id != "234582653"
               AND zencoder_id != "234577808"
               AND zencoder_id != "234575438"
               AND zencoder_id != "234574465"
               AND zencoder_id != "233994254"
               AND zencoder_id != "209354177"
               AND zencoder_id != "233944360"
               AND zencoder_id != "233861899"
               AND zencoder_id != "233860593"
               AND zencoder_id != "233825065"
               AND zencoder_id != "233711039"
               AND zencoder_id != "233500116"
               AND zencoder_id != "233239799"
               AND zencoder_id != "232937893"
               AND zencoder_id != "232933881"
               AND zencoder_id != "232926745"
               AND zencoder_id != "232894942"
               AND zencoder_id != "232556583"
               AND zencoder_id != "232555429"
               AND zencoder_id != "232506092"
               AND zencoder_id != "232505593"
               AND zencoder_id != "232505460"
               AND zencoder_id != "232501252"
               AND zencoder_id != "232500882"
               AND zencoder_id != "232497589"
               AND zencoder_id != "232464846"
               AND zencoder_id != "232465537"
               AND zencoder_id != "232436742"
               AND zencoder_id != "232191057"
               AND zencoder_id != "232189056"
               AND zencoder_id != "232187034"
               AND zencoder_id != "232182758"
               AND zencoder_id != "232111990"
               AND zencoder_id != "232110944"
               AND zencoder_id != "232105250"
               AND zencoder_id != "232101358"
               AND zencoder_id != "232100019"
               AND zencoder_id != "232092864"
               AND zencoder_id != "232091326"
               AND zencoder_id != "232090076"
               AND zencoder_id != "232037693"
               AND zencoder_id != "231978257"
               AND zencoder_id != "204416619"
               ORDER BY id DESC';

    echo $l_sSql;
    $video_result = $l_xFrontCoreSql->buildObject($l_xFrontCoreSql->runQuery($l_sSql));

    echo "<BR>\n\$video_result: ";
    var_dump($video_result);

    spl_autoload_unregister('coreAutoload');
    require(ZENCODER_CLASS);
    $l_xZencoder = new Services_Zencoder(ZENCODER_API_KEY);

    foreach($video_result as $video){

        echo "<BR>\n----------------------------------------------------------------------------------------------------";

//        echo "<BR>\n\$video: ";
//        var_dump($video);

        echo "<BR>\n\$video['zencoder_id']: ";
        var_dump($video['zencoder_id']);

//        $job_details = $l_xZencoder->jobs->details(253643545); // From oflo production
        $job_details = $l_xZencoder->jobs->details($video['zencoder_id']);

        echo "<BR>\n\$job_details: ";
        var_dump($job_details);

        $duration_in_ms = $job_details->raw_response->job->input_media_file->duration_in_ms;

        echo "<BR>\n\$duration_in_ms: ".$duration_in_ms;
        $duration_in_secs = $duration_in_ms/1000;
        echo "<BR>\n\$duration_in_secs: ".$duration_in_secs;
//        exit;

        // Store duration_in_secs of Job IDs to pay content creators.
        $l_sSql = 'UPDATE enr_video SET video_duration = "'.$duration_in_secs.'"
                   WHERE zencoder_id = '.$video['zencoder_id'];

        echo "<BR>\n\$l_sSql: ".$l_sSql;

        $l_xFrontCoreSql->runQuery($l_sSql);

//        exit;

    }

//    // Success if we got here
//    echo "w00t! \n\n";
//    echo "Job ID: ".$l_xEncodingJob->id."\n";
//    echo "Output ID: ".$l_xEncodingJob->outputs['web']->id."\n";
//    // Store Job/Output IDs to update their status when notified or to check their progress.
//    $l_sSql = 'UPDATE enr_video SET zencoder_id = "'.$l_xEncodingJob->id.'" WHERE id = '.$l_nItemsId;
//    echo $l_sSql;
//    $l_xFrontCoreSql->runQuery($l_sSql);

} catch (Services_Zencoder_Exception $e) {
    // If were here, an error occured

    echo "Fail :(<BR><BR>\n\n";
    echo "Errors:<BR>\n";
    foreach ($e->getErrors() as $error) echo $error."<BR>\n";
    echo "Full exception dump:<BR><BR>\n\n";
    var_dump($e);

}

spl_autoload_register('coreAutoload');

?>


