<?php

include('simplepush.php');
//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_m extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
//        require_once(LIB_PATH.'oauth/facebook/Facebook.php');
        require_once(LIB_PATH.'oauth/facebook/autoload.php');
//        require_once(FACEBOOK_OAUTH_CLASS);
        require_once(TWITTER_OAUTH_CLASS);
    }
    /**
     * errors as array
     *
     * Get the error messages as an array
     *
     * @return array
     * @author Raul Baldner Junior
     **/
    public function get_errors()
    {
        return $this->errors;
    }
    public function set_slug_dependent_constants($email){


        $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
        $query = $this->db->query($SQL);
        $rows = $query->result();
        $cid = $rows[0]->cid;

        $this->chanel = $this->get_chanel($cid);


        $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
        $query = $this->db->query($SQL);
        $rows = $query->result();
        $slug = $rows[0]->slug;


        define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
        define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
        define('UPLOAD_AMZ', $slug.'/assets/ufile/');
        define('THEME_AMZ', $slug.'/images/');
        define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
        define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
        define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

        return $slug;

    }

    function getChanels($searchvalue=false){
        $and = '';
        if($searchvalue){
            $and = " where t1.name like '%{$searchvalue}%'";
        }

        $sql = "select t1.*, t2.email chanel_admin
                from enr_chanel t1
                left join users t2 on t1.user_id=t2.id
                where t1.id != 64
                {$and}";
        $query = $this->db->query($sql);
        $rows = $query->result();

        return $rows;
    }

    function getPriorityList(){
     

        $sql = "SELECT t1.*, t2.email chanel_admin
                FROM enr_chanel t1
                LEFT JOIN users t2 ON t1.user_id=t2.id
                WHERE t1.id != 64 
                ORDER BY t1.priority

                ";
        $query = $this->db->query($sql);
        $rows = $query->result();

        return $rows;
    }

    function updatePriorityList(){

        $list_order = $_POST['list_order'];
        $list = explode(',' , $list_order);

      
        $i = 1 ;

        foreach($list as $p_id) {
         
                $sql  = "UPDATE enr_chanel SET priority = '$i' WHERE id = '$p_id'" ;

                $query = $this->db->query($sql);
           
            $i++ ;
        }

        // if(isset($_POST['priorityActivate'])) {
        //   print_r($_POST); //print all checked elements
        // }

    }


    function activatePriority(){

        $data = $_POST['list_order'];


        $original_num = $_POST['original_num'];

        $sql1  = "SELECT MAX(priority) FROM enr_chanel" ;

        $priorityMax = $this->db->query($sql1);
        $row = $priorityMax->row_array();
        $priorityNum = $row["MAX(priority)"] + 1;
        

        $sql  = "UPDATE enr_chanel SET priority = '$priorityNum' WHERE id = '$data'" ;

            $query = $this->db->query($sql);
      

        // if(isset($_POST['priorityActivate'])) {
        //   print_r($_POST); //print all checked elements
        // }

    }

    function keepPriority(){

        $data = $_POST['list_order'];
        $original_num = $_POST['original_num'];
        

        $sql  = "UPDATE enr_chanel SET priority = '$original_num' WHERE id = '$data'" ;

            $query = $this->db->query($sql);
      

        // if(isset($_POST['priorityActivate'])) {
        //   print_r($_POST); //print all checked elements
        // }

    }

    function deactivatePriority(){

        $data = $_POST['list_order'];

        $sql  = "UPDATE enr_chanel SET priority = 0 WHERE id = '$data'" ;

            $query = $this->db->query($sql);
      

        // if(isset($_POST['priorityActivate'])) {
        //   print_r($_POST); //print all checked elements
        // }

    }

    function get_chanel($id){

        $id = $this->db->escape_str($id);


        $strSql = "
                SELECT t1.*,t1.image_path chanel_img_url, t2.email, t2.username chanel_admin, t2.img_url, t2.first_name, t3.id AS video_id, t3.cid
             FROM enr_chanel t1 left join users t2 on t1.user_id=t2.id left join enr_video t3 on t1.id = t3.cid
              where
              t1.id={$id}";


        $query = $this->db->query($strSql);
        $row = $query->row_array();
        return $row;
    }



    function isChanelOflo(){
        $id = $this->controller->chanel['id'];
        $strSql = "SELECT * FROM enr_chanel where id = $id and is_oflo ='y' ";
        $query = $this->db->query($strSql);
        $rows = $query->result();

        if(count($rows) > 0){
            return true;
        }else{
            return false;
        }
   
    }   
    function isExistChanel($chanel){
        $chanel['name'] = $this->db->escape_str($chanel['name']);
        $strSql = "SELECT * FROM enr_chanel where id!={$chanel['id']} and  name='{$chanel['name']}'";
        $query = $this->db->query($strSql);
        $rows = $query->result();

        if(count($rows) > 0){
            return true;
        }else{
            return false;
        }
    }

    function isExistEmail($user){
        $user['email'] = $this->db->escape_str($user['chanel_admin']);
        $strSql = "SELECT * FROM users where id!={$user['id']} and email='{$user['email']}'";
        $query = $this->db->query($strSql);
        $rows = $query->result();

        if(count($rows) > 0){
            return true;
        }else{
            return false;
        }
    }

    function addChanel($chanel, $oflo_channel_user = 'n'){
        if($_FILES['image']['size']){
            $image_path = $this->_uploadPhoto();
            $image_mime = $_FILES['image']['type'];
        }else{
            $image_path = "";
            $image_mime = "";
        }
        $url = $this->controller->chanel['url'];

             if($this->input->post('is_oflo')=='on'){
                $chanel['is_oflo']= y;
            }else{
                $chanel['is_oflo'] = n;
            }
//        $accountId = GA_ACCOUNT_ID;
//        $fullurl = "http://".$chanel['url'].".on.starsite.com";
//        $ganame = $chanel['name'].' Channel';
//
//        $analytics = Gastar::getService();
//
//        $usedGaAccouts = Gastar::getnumberOfProperties($accountId, $fullurl, $analytics);
//        if($usedGaAccouts<50){
//
//            $property = Gastar::createNewProperty($accountId, $fullurl, $ganame, $analytics);
//            $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
//            $profile = Gastar::createNewProfile($accountId, $fullurl, $propertyID, $analytics);
//            $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
//            Gastar::linkProfileToProperty($accountId, $propertyID, $profileID, $analytics);
//
//            if($usedGaAccouts>40){
//                $remainingGaAccounts = 50 - ($usedGaAccouts+1);
//                Gastar::sendEmail($accountId, $remainingGaAccounts);
//
//            }
//
//        }


        $res = $this->db->insert('enr_chanel',
            array(
                'name'=>$chanel['name'],
                'title'=>$chanel['title'],
                'url'=>$chanel['url'],
                'slug'=>$chanel['slug'],
                'is_publish'=>$chanel['is_publish'],
                'user_id'=>'-2',
                'image_path'=>$image_path,
                'image_mime'=>$image_mime,
                'created' => $chanel['created'],
                'bc_playerid_web' => $chanel['bc_playerid_web'],
                'bc_playerid_ios' => $chanel['bc_playerid_ios'],
                'bc_playerid_android' => $chanel['bc_playerid_android'],
                'ad_server_url_web' => $chanel['ad_server_url_web'],
                'ad_server_url_ios' => $chanel['ad_server_url_ios'],
                'ad_server_url_android' => $chanel['ad_server_url_android'],
                'ad_server_url_mobile' => $chanel['ad_server_url_mobile'],
                'ad_server_url_desktop' => $chanel['ad_server_url_desktop'],
                'brightcove_overlay_logo' => $chanel['brightcove_overlay_logo'],
                'brightcove_key' => $chanel['brightcove_key'],
                'feedback_email' => $chanel['feedback_email'],
                'enable_banner_ads_ios' => $chanel['enable_banner_ads_ios'],
                'enable_banner_ads_android' => $chanel['enable_banner_ads_android'],
                'enable_banner_ads_web' => $chanel['enable_banner_ads_web'],
                'min_inline_video_len' => $chanel['min_inline_video_len'],
                'google_analytics_key' => $chanel['google_analytics_key'],
                'starstats_site_id' => $chanel['starstats_site_id'],
                'quantserve_key' => $chanel['quantserve_key'],
                'current_cpm' => $chanel['current_cpm'],
                'is_oflo'=>$chanel['is_oflo']
            )
        );
        $cid = $this->db->insert_id();
        if($chanel['chanel_admin']){
            $additional_data = array(
                "cid" => $cid
            );
            $user_id = $this->controller->ion_auth->register($chanel['chanel_admin'], $chanel['password'], $chanel['chanel_admin'], $additional_data);
            $this->db->update('enr_chanel', array('user_id'=>$user_id), array('id'=>$cid));
            $this->db->update('users', array('is_oflo' => $oflo_channel_user), array('id' => $user_id));

        }else{
            $user_id = 0;
        }
        
        $sql_time = array(
            'last_updated_timestamp' => time()
        );
        $insert_query = $this->db->insert_string('channel_update', $sql_time);
        // $this->db->insert('channel_update', $sql_time);
        $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
        $this->db->query($insert_query);

                   
             
       
        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        

        return $res;
    }

    /**
     * Get User.
     * @param $id
     * @return mixed
     */
    public function getUser($id){
        $sql = "
            select *
            from users
            where id='{$id}' 
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    /**
     * Update User O-Flo flag.
     * @param $user
     * @return mixed
     */
    public function updateUserOfloFlag($user){
        $this->db->where("id", key($user));
        $update = array(
            'is_oflo' => $user[key($user)]
        );
        $res = $this->db->update('users', $update);

        return $res;
    }
    public function updateLastPost($fan_id) {
        if($fan_id == -1) return;

        $sql = 'update enr_fan set lastpost = "' . date('Y-n-j') . '" where id = ' . $fan_id;

        $this->db->query($sql);

        return;
    }
    function updateChanel($chanel){

        if($v = $this->get_chanel($chanel['id'])){

            $user_id = $v['user_id'];
            if($_FILES['image']['size']){
                $destination = UPLOAD_CLOUD.$v['image_path'];
                if(is_file($destination)){
                    unlink($destination);
                }
                $image_path = $this->_uploadPhoto();
                $image_mime = $_FILES['image']['type'];
            }
        }

        $url = $this->controller->chanel['url'];
           if($this->input->post('is_oflo')=='on'){
                $chanel['is_oflo']= y;
            }else{
                $chanel['is_oflo'] = n;

            }

        if($chanel['chanel_admin']){
            if(!$this->getUser($user_id)){
                $user_id = $this->controller->ion_auth->register($chanel['chanel_admin'], $chanel['password'], $chanel['chanel_admin']);
            }else{
                $this->controller->ion_auth->update($user_id, array('username'=>$chanel['chanel_admin'], 'password'=>$chanel['password'], 'email'=>$chanel['chanel_admin'], 'cid'=>$chanel['id']));
            }
        }

        $this->db->where("id", $chanel['id']);
        $update = array(
            'name'=>$chanel['name'],
            'title'=>$chanel['title'],
            'url'=>$chanel['url'],
            'slug'=>$chanel['slug'],
            'is_publish'=>$chanel['is_publish'],
            'is_oflo'=>$chanel['is_oflo'],
            'user_id'=>$user_id,
            'bc_playerid_web' => $chanel['bc_playerid_web'],
            'bc_playerid_ios' => $chanel['bc_playerid_ios'],
            'bc_playerid_android' => $chanel['bc_playerid_android'],
            'ad_server_url_web' => $chanel['ad_server_url_web'],
            'ad_server_url_ios' => $chanel['ad_server_url_ios'],
            'ad_server_url_android' => $chanel['ad_server_url_android'],
            'ad_server_url_mobile' => $chanel['ad_server_url_mobile'],
            'ad_server_url_desktop' => $chanel['ad_server_url_desktop'],
            'brightcove_overlay_logo' => $chanel['brightcove_overlay_logo'],
            'brightcove_key' => $chanel['brightcove_key'],
            'feedback_email' => $chanel['feedback_email'],
            'enable_banner_ads_ios' => $chanel['enable_banner_ads_ios'],
            'enable_banner_ads_android' => $chanel['enable_banner_ads_android'],
            'enable_banner_ads_web' => $chanel['enable_banner_ads_web'],
            'min_inline_video_len' => $chanel['min_inline_video_len'],
            'google_analytics_key' => $chanel['google_analytics_key'],
            'starstats_site_id' => $chanel['starstats_site_id'],
            'quantserve_key' => $chanel['quantserve_key'],
            'current_cpm' => $chanel['current_cpm']
        );
        if(isset($image_path) && !empty($image_path)) {
            $update['image_path'] = $image_path;
            $update['image_mime'] = $image_mime;
        }
        $res = $this->db->update('enr_chanel', $update);

        $sql_time = array(
            'last_updated_timestamp' => time()
        );
        $this->db->update('channel_update', $sql_time);
  

        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        return $res;
    }


        function get_video_id($id){


        $id = $this->db->escape_str($id);

        $strSql = "
            SELECT t2.*,t1.id, t1. cid, t2.id
            FROM enr_video t1 left join enr_chanel t2 on t1.cid=t2.id
            where 
                t1.id";

        $query = $this->db->query($strSql);
        $row = $query->row_array();
        return $row;
    }
    

    /**
     * content
     */
    function getSubmenus($chanel=0){
        $sql = "
            select t1.*, if(t1.alter_name<>'', t1.alter_name, t2.name) alter_name, t2.url url
            from enr_submenu t1, enr_section t2
            where 
                t1.cid='{$chanel}' and t1.section_id=t2.id
            order by 
                t1.ordering, t2.ordering
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    function insertFeed($post_type, $content_id, $fan_id, $publish_status){
        if($fan_id != -1)
            return;
        $sql = array(
            'post_type' => $post_type,
            'content_id' => $content_id,
            'time_stamp' => time()
        );
        $this->db->insert('enr_feed', $sql);

        $id = $this->db->insert_id();
        if($publish_status == 1) {
            $this->sendpush($id);
        }
    }
    function sendpush($feed_id) {
        //check whether push is enabled
        $cid = $this->controller->chanel['id'];
        $social = $this->main_m->getSocial($cid);
        if(!isset($social['push']) || $social['push'] == 0) return;

        //check whether push has already been sent
        $sql = "select * from enr_feed where id = ?";
        $row = $this->db->query($sql, array($feed_id))->result_array();
        if(count($row) == 0 || $row[0]['push_status'] == 1) return;

        $query = 'insert into enr_feed_thread(feed_id, channel_id) values(?, ?)';
        $this->db->query($query, array($feed_id, $cid));

        //mark as sent
        $sql = 'update enr_feed set push_status=1 where id=?';
        $this->db->query($sql, array($feed_id));
    }
    function get_feed_by_content_id($type, $content_id) {
        $sql = 'select id from enr_feed where post_type = ? and content_id = ?';
        $row = $this->db->query($sql, array($type, $content_id))->result_array();

        if(count($row) == 0) return '';

        return $row[0]['id'];
    }
    function addFeedText($feed){

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        $post = array(
            'cid' => $feed['cid'],
            'fan_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'time_stamp' => time(),
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0
        );
        $res = $this->db->insert("enr_text", $post);
        $id = $this->db->insert_id();

        $this->updateDeepLink('text', $id);
        $this->insertFeed('text', $id, $post['fan_id'], $feed['is_publish']);

        $this->updateLastPost($post['fan_id']);
        return $id;
    }

    function updateFeedText($feed){

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if($feed['time_stamp'] > time()) {
            $this->controller->data['show_errors'] = "Invalid post date and time.";
            return false;
        }
        $id = ltrim($feed['feed_id'], "text_");
        $post = array(
            'cid' => $feed['cid'],
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky']
        );

        if($feed['time_stamp'] != 0) {
            $post['time_stamp'] = $feed['time_stamp'];
        }
        $this->db->where("id", $id);
        $res = $this->db->update("enr_text", $post);

        $feed_id = $this->get_feed_by_content_id('text', $id);
        if(!empty($feed_id) && $feed['is_publish'] == 1) {
            $this->sendpush($feed_id);
        }

        $this->updateDeepLink('text', $id);
        return $res;
    }
    function getDraftFeedList($user_id=false){
        $available_only = false;

        if($user_id){
            $fan = $this->getFan($user_id);
        }else{
        }

        $where = " and (t1.is_publish='2' or t1.is_publish = '0') ";

        $sql = "select t1.*, '' fan_id
            from
                (
                    (
                        select concat('text_',id) id,'Text Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, '' image_path, tags, description caption, time_stamp, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web,  deep_link_web_tw, '' as brightcove_media_id
                        from enr_text
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= NOW() AND end_date >= NOW()" : "") . "
                    )
                    union
                    (
                        select concat('photo_',id) id,'Photo Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination image_path, tags, description caption, time_stamp, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_photo
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= NOW() AND end_date >= NOW()" : "") . "
                    )
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= NOW() AND end_date >= NOW()" : "") . "
                    )
                    union
                    (
                        select concat('poll_',id) id,'Poll Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_poll
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= NOW() AND end_date >= NOW()" : "") . "
                    )
                    union
                    (
                        select concat('quiz_',id) id,'Quiz Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_quiz
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= NOW() AND end_date >= NOW()" : "") . "
                                )
                                ) t1
                                where
                                t1.fan_id=-1 {$where}
                                order by t1.is_sticky desc, t1.time_stamp desc
                                ";
        $query = $this->db->query($sql);

        $rows = $query->result_array();
        return $rows;
    }
    function getHomeFeedList($is_publish = false, $user_id=false, $available_only = false){

        if($user_id){
            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
            $is_draft = 0;

            if($is_publish && $is_draft){
                $where = " and (t1.is_publish='1' or t1.is_publish='2')";
            }elseif($is_publish){
                $where = " and t1.is_publish='1' ";
            }elseif($is_draft){
                $where = " and t1.is_publish='2' ";
            }else{
                $where = " and 0 ";
            }

        }else{
            if($is_publish){
                $where = " and t1.is_publish=1";
            }else{
                $where = "";
            }
        }
        $sql = "select t1.*, '' fan_id
            from 
                (
                    (
                        select concat('text_',id) id,'Text Post' post_type, '' as 'duration', date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, '' image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_text
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
                    ) 
                    union
                    (
                        select concat('photo_',id) id,'Photo Post' post_type, '' as 'duration', date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_photo
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, video_duration as 'duration', date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
                    )
                    union
                    (
                        select concat('poll_',id) id,'Poll Post' post_type, '' as 'duration', date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_poll
                        where
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
                    )
                    union
                    (
                        select concat('quiz_',id) id,'Quiz Post' post_type, '' as 'duration', date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_quiz
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
                    )  
                ) t1 
            where
                t1.fan_id=-1 {$where}
            order by t1.is_sticky desc, t1.time_stamp desc
        ";
        $now = date('Y-n-j H:i:s');
        $query = $this->db->query($sql, array($now, $now,$now, $now,$now, $now,$now, $now, $now, $now));

        $rows = $query->result_array();

        return $rows;
    }


    function update_post_status($id, $post_type, $is_publish){

    
        $sql =  

        array(

        
         'is_publish' => $is_publish,
         'start_date' => date('Y-m-d H:i:s'),

   
        
        );


        $this->db->where('id', $id);
        $result= $this->db->update('enr_'.$post_type, $sql);

     return $result;
    }



   function filterofloslider($is_publish = false, $user_id=false, $available_only = false){

      if($user_id){
            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
          
            $is_draft = 0;

            if($is_publish &&  $is_draft){
                $where = " and (t1.is_publish='1' or t1.is_publish='2')";
            }elseif($is_publish){
                $where = " and t1.is_publish='1' ";
            }elseif($is_draft){
                $where = " and t1.is_publish='2' ";
            }else{
                $where = " and 0 ";
            }

        }else{
            if($is_publish){
                $where = " and t1.is_publish=1";
            }else{
                $where = "";
            }
        }
        $sql = "select t1.*, '' fan_id
            from 
                (
                    (
                       
                        select concat('photo_',id) id,'Photo Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, is_oflo_slider, destination image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_photo
                        where 
                            /* is_publish=1  and */
                           
                            cid='".$this->chanel['id']."'".
                           
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") .   " AND is_oflo_slider='y'
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, is_oflo_slider,image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                            /* is_publish=1 and */
                          
                            cid='".$this->chanel['id']."'".
                             
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . " AND is_oflo_slider='y'
                    )
                    
                ) t1 
            where
                t1.fan_id=-1 {$where}
            order by t1.is_sticky desc, t1.time_stamp desc
        ";
        $now = date('Y-n-j H:i:s');
        $query = $this->db->query($sql, array($now, $now,$now, $now,$now, $now,$now, $now, $now, $now));

        $rows = $query->result_array();
        return $rows;
     
        



   }


    function getSliderFeedList($is_publish = false, $is_oflo_slider= false, $user_id=false, $available_only = false){

        if($user_id){
            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
            $is_oflo_slider=y;
            $is_draft = 0;

            
            if($is_publish && $is_oflo_slider){
                $where = " and (t1.is_publish='1' or t1.is_oflo_slider='y')";
            }elseif($is_publish){
                $where = " and t1.is_publish='1' ";
            }elseif($is_oflo_slider){
                $where = " and t1.is_oflo_slider='y' ";
            }
            

        }else{
            if($is_publish){
                $where = " and t1.is_publish=1";
            }else{
                if($is_oflo_slider){
                $where = "and t1.is_oflo_slider=y";
            }
               }
        }
        
                $sql = "select t1.*, '' fan_id
            from 
                (
                    
                    (
                        select concat('photo_',id) id,'Photo Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, is_oflo_slider,destination image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id 
                        from enr_photo
                        where 
                            /* is_publish=1 and */
                           cid='".$this->chanel['id']."'".
            ($available_only ? "  and start_date <= ? AND end_date >= ?" : "") . "
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, is_oflo_slider, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."'".
            ($available_only ? " and start_date <= ? AND end_date >= ?" : "") . "
            
                    )  
                ) t1 
            where
                t1.fan_id=-1 {$where}
            order by t1.is_sticky desc, t1.time_stamp desc
        ";
        $now = date('Y-n-j H:i:s');
        $query = $this->db->query($sql, array($now, $now,$now, $now,$now, $now,$now, $now, $now, $now));

        $rows = $query->result_array();
        return $rows;
    }

    function update_is_oflo_slider($id, $post_type, $is_oflo_slider){
    
        $sql = array(

        
         'is_oflo_slider' => $is_oflo_slider,
        
        );

        $this->db->where('id', $id);
       $result= $this->db->update('enr_'.$post_type, $sql);

     return $result;
    }


    function getFuturePosts(){



        $sql = "select t1.*, '' fan_id
            from 
                (
                    (
                        select concat('text_',id) id,'Text Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, '' image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_text
                        where 
                            cid='".$this->chanel['id']."'

                    ) 
                    union
                    (
                        select concat('photo_',id) id,'Photo Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_photo
                        where 
                            cid='".$this->chanel['id']."'
                            
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                            cid='".$this->chanel['id']."'
                            
                    )  
                    union
                    (
                        select concat('poll_',id) id,'Poll Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_poll
                        where 
                            cid='".$this->chanel['id']."'
                            
                    )  
                    union
                    (
                        select concat('quiz_',id) id,'Quiz Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, tags, description caption, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_quiz
                        where 
                            cid='".$this->chanel['id']."'
                            
                    )  
                ) t1
            where
                t1.fan_id=-1 and t1.is_publish=0 or t1.is_publish=2
            order by t1.start_date asc
        ";

        $query = $this->db->query($sql);

        $rows = $query->result_array();
        return $rows;

    }




    /**
     * Get list of Viral Partners.
     *
     * @return array
     */
    function get_viral_partners(){

        $sql = "SELECT *
                FROM viral_media_partner";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;
    }

    /**
     * Get Viral Partner.
     *
     * @param $partner_name
     * @return mixed
     */
    function get_viral_partner($partner_name){

        $sql = "SELECT *
                FROM viral_media_partner
                WHERE partner_name LIKE '$partner_name'
                LIMIT 1";
        $query = $this->db->query($sql);
        $rows = $query->row_array();

        return $rows;
    }

    /**
     * Get Viral Media XML Feed for ALL Viral Partners.
     *
     * @return mixed
     */
    function get_viral_media_xml_all(){

        $sql = "SELECT *
                FROM viral_media_xml";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;
    }

    /**
     * Get Viral Medial XML.
     *
     * @param $partner
     * @param $category
     * @param $search
     * @return mixed
     */
    function get_viral_media_xml($partner, $category, $search){

        $partner  = $this->db->escape_like_str($partner);
        $category = $this->db->escape_str($category);
        $search   = $this->db->escape_like_str($search);

        $sql = "SELECT vml.*
                FROM viral_media_xml vml";

        // Partner
        if ($partner != "all"){
            $sql .= " LEFT JOIN viral_media_partner vmp ON vmp.id = vml.partner_id
                     WHERE partner_name LIKE '".$partner."'";
        }

        // Category
        if ($partner == "all" && $category != NULL){
            $sql .= " WHERE vml.category = '".$category."'";
        } elseif ($partner != "all" && $category != NULL) {
            $sql .= " AND vml.category = '".$category."'";
        }

        // Search
        if ($partner != "all" && $category != NULL && !empty($search)){
            $sql .= " AND vml.title LIKE '%".$search."%' OR
                          vml.keywords LIKE '%".$search."%' OR
                          vml.category LIKE '%".$search."%'";
        } elseif (($partner == "all" || $category != NULL ) && !empty($search)) {
            $sql .= " WHERE vml.title LIKE '%".$search."%' OR
                          vml.keywords LIKE '%".$search."%' OR
                          vml.category LIKE '%".$search."%'";
        }

        // Non-blacklisted
        if ($partner != "all" || $category != NULL || !empty($search)){
            $sql .= " AND blacklist = '0'
            ORDER BY pubDate DESC";
        } elseif ($partner == "all" && $category == NULL  && empty($search)) {
            $sql .= " WHERE blacklist = '0'
            ORDER BY pubDate DESC";
        }
       // var_dump($sql);
       // exit;

        $query = $this->db->query($sql);
        $rows = $query->result_array();
      
//      
        return $rows;
    }

    /**
     * Get Viral Media XML Feed for Viral Partner.
     *
     * @param $partner_id
     * @return mixed
     */
    function get_viral_partner_media_xml($partner_id){

        $sql = "SELECT *
                FROM viral_media_xml

                WHERE partner_id = $partner_id";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;
    }

    /**
     * Get Viral Media XML Feed for enr_video insertion.
     * 
     * @param $viral_media_id
     * @return mixed
     */
    function get_viral_media_xml_by_id($viral_media_id){

        $sql = "SELECT *
                FROM viral_media_xml
                WHERE id = $viral_media_id";
        $query = $this->db->query($sql);
        $rows = $query->row_array();

        return $rows;
    }

    /**
     * Get all viral media categories.
     *
     * @return mixed
     */
    function get_viral_media_category_all(){

        $sql = "SELECT *
                FROM viral_media_category";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;

    }

    /**
     * Get all oflo categories.
     * 
     * @return mixed
     */
    function get_all_oflo_categories(){

        $sql = "SELECT *
                FROM viral_media_category vmc
                WHERE vmc.is_oflo ='1' ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;

    }

     function get_viral_media_cat_post(){

        $sql = "SELECT *
                FROM viral_media_cat_post";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;
    }


    
    function add_viral_media_category($post){


        
    $this->load->database();
     $designinfo = $this->getDesignInfo();
    $l_bCLOSE_BUFFER = 1;


  
    $cat_name = $this->input->post('cat_name');
    $hex_color =$this->input->post('hex_color');


     $filepath = $this->_uploadCropedPhotoCat('image');

        list($width,$height)=getimagesize(UPLOAD_DIR.$filepath);
        $aspect_ratio = $height/$width;
        @unlink(UPLOAD_DIR.$filepath);
       if($this->input->post('oflo')=='on'){
                $oflo = 1;

            }else{
                $oflo = 0;
            }
            if($this->input->post('oflo_menu')=='on'){
                $oflo_menu = 1;
            }else{
                $oflo_menu = 0; }

                 
   $insert_query = "INSERT IGNORE INTO viral_media_category (cat_name, cat_image, hex_color, oflo_menu, is_oflo)
                            VALUES('$cat_name', '$filepath', '$hex_color' , $oflo_menu, $oflo)";


    $this->db->query($insert_query);
    $query = $this->db->get('viral_media_category');
    $query_result = $query->result();


    foreach ($query_result as $category){
        if ($cat_name == $category->cat_name){
            $category->id;
            $id= $category->id;
        }
    }

                if($this->input->post('oflo')=='on'){
                        $oflo = 1;
                    if($this->input->post('oflo_menu')=='on'){
                        $oflo_menu =1;
                    }else{$oflo_menu = 0;}

                   $insert = "INSERT IGNORE INTO oflo_category (id, cat_name, oflo_menu)
                            VALUES($id, '$cat_name'  , $oflo_menu)";

                    $this->db->query($insert);

                }

           $sql_time = array(
            'last_updated_timestamp' => time()
        );
         $insert_query = $this->db->insert_string('category_update', $sql_time);
         $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
         $this->db->query($insert_query);

        @unlink(UPLOAD_DIR.$filepath);
        @unlink(UPLOAD_DIR."big_thumb/".$filepath);
        @unlink(UPLOAD_DIR."thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_square_thumb/".$filepath);
               
    }

     function update_viral_media_category($id, $cat_name, $hex_color, $cat_image, $oflo, $oflo_menu){

        if($_FILES['image']['size']){
            $cat_image = UPLOAD_DIR.$id['cat_image'];
            if(is_file($cat_image)){
                unlink($cat_image);
            }
           $filepath = $this->_uploadCropedPhotoCat('image');

            list($width,$height)=getimagesize(UPLOAD_DIR.$filepath);
            $aspect_ratio = $height / $width;


            if(!$filepath){
                return false;
            }
        }else{
            $filepath = $id['cat_image'];
        }
    
        $sql = array(
            // "id" => $cid,
            'cat_name' => $cat_name,
            'is_oflo' => $oflo,
            'cat_image' => $filepath,
            'hex_color' =>$hex_color,
            'oflo_menu' => $oflo_menu,
           
            
        );
           $sql2 = array(
            // "id" => $cid,
            'cat_name' => $cat_name,
            'oflo_menu' => $oflo_menu
        );

        $this->db->where("id", $id); 
        $this->db->update("oflo_category", $sql2);

              $sql_time = array(
            'last_updated_timestamp' => time()
        );
         $insert_query = $this->db->insert_string('category_update', $sql_time);
         $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
         $this->db->query($insert_query);

        // $this->db->where("cat_name", $post['cat_name']);
        if ($oflo ==1){
              $insert = "INSERT IGNORE INTO oflo_category (id, cat_name, oflo_menu)
                            VALUES($id, '$cat_name', $oflo_menu)";

                    $this->db->query($insert);
        }else{
            $this->db->where('id', $id);
            $this->db->delete("oflo_category");
        }

        $this->db->where("id", $id);       
        return $this->db->update("viral_media_category", $sql);

        @unlink(UPLOAD_DIR.$filepath);
        @unlink(UPLOAD_DIR."big_thumb/".$filepath);
        @unlink(UPLOAD_DIR."thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_square_thumb/".$filepath);
  
        // return true;
    }

    function fetch_viral_media_category(){
    $query = $this->db->get('viral_media_category');
    $query_result = $query->result();
    return $query_result;
    }
    // Function To Get Category
     function get_category_id($data){
        // $sql = SELECT * FROM viral_media_category WHERE id = '.$id.';
     $this->db->select('*');
     $this->db->from('viral_media_category');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result();
     // var_dump(query);    
     return $result;
    }
    // Update Query For Selected Categories
     function update_viral_media_category_id($id,$data){
     $this->db->where('id', $id);
     $this->db->update('viral_media_category', $data);
}
    
    function edit_category(){
    
      $data = array(
                       'id' => $id,
                       'cat_name' => $cat_name,
                       'cat_image' => $cat_image,
                       'hex_color' =>$hex_color,
                      
                    );

        $this->db->where('id', $post['id']);
        $this->db->update('viral_media_category', $data, array('id' =>$id));


        return true;

 }

function confirm_show_del($data){
    $this->db->select('*');
    $this->db->from('viral_media_category');
    $this->db->where('id',$data);
    $query = $this->db->get();
    $result=$query->result();
    return $result;
}


 function get_viral_media_catnames ($post){
       
        $sql = "SELECT cat_name
                FROM viral_media_category
                ";
        $query = $this->db->query($sql);
        
}



  /**
     * Get all push notifications
     *
     * @return mixed
     */
    function get_platform_notification(){

        $sql = "SELECT *
                FROM platform_notification
                ORDER BY date DESC";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;

    }

    function add_feed_notification($post){


               $description = $this->input->post('description');
               $cid = $this->chanel['id'];
               $date = strtotime($this->input->post('start_date')) == 0 ? date('Y-n-j H:i:s') : $this->input->post('start_date');


             $notification= "INSERT IGNORE INTO platform_notification (platform_id, recipients, followers, feed_notif, message, date, status)
                        VALUES(2,'Followers', '$cid', 'y', '$description', '$date', 0)";
  
              $this->db->query($notification);
     


    }


    function add_platform_notification($post){

        
    $this->load->database();
    $platform_id= $this->input->post('platform_id');
    $recipients = $this->input->post('recipients');
    $channel_recipients =$this->input->post('channel_recipients');
    $followers =$this->input->post('followers');
    $user_recipients =$this->input->post('user_recipients');
    $subject = $this->input->post('title');
        $title=$this->db->escape($subject);
    $message = $this->input->post('message');
        $notification =$this->db->escape($message);
    $date = $this->input->post('date');
    $status = $this->input->post('status');


      $post = array (
            'platform_id' => $platform_id, 
            'recipients' => $recipients,
            'channel_recipients' => $channel_recipients,
            'followers' => $followers,
            'user_recipients' => $user_recipients,
            'title' => $subject,
            'message' => $message,
            'date' => $date,
            'status' => $status
        );

      //oflo pro

     if ($platform_id == '3' && $recipients == 'All') {



    
     foreach($channel_recipients as $channel_recipient)


    {

     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipients', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                   
                                                       }

     
      if ($platform_id == '3' && $recipients == 'All') {


     
    
     foreach($user_recipients as $user_recipient)


    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipients','$user_recipient', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

     } 


                   
                                                        }

     
      elseif ($platform_id == '3' && $recipients == 'User') {



    
     foreach($user_recipients as $user_recipient)


   {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipients','$user_recipient', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                   
                                                              }

       elseif ($platform_id == '3' && $recipients == 'Channel') {

    
     foreach($channel_recipients as $channel_recipient)


    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipient,title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipients', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                                                         }


     // pro flo

     if ($platform_id == '4' && $recipients == 'All') {



    
     foreach($channel_recipients as $channel_recipient)


    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipients', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                   
                                                       }

     
      if ($platform_id == '4' && $recipients == 'All') {



    
     foreach($user_recipients as $user_recipient)


    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipients','$user_recipient', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

     } 


                   
                                                        }

     
      elseif ($platform_id == '4' && $recipients == 'User') {



    
     foreach($user_recipients as $user_recipient)


   {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipients','$user_recipient', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                   
                                                              }

       elseif ($platform_id == '4' && $recipients == 'Channel') {

    
     foreach($channel_recipients as $channel_recipient)


    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipient,title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipients', $title, $notification, '$date', '$status')";

                         $this->db->query($insert_query); 

    } 


                                                         }

     
     // oflo
     


      elseif ($platform_id == '2' && $recipients == 'All') {

    
   
     {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipient', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    }

                                                         }




  

  

     elseif ($platform_id == "2" && $recipients == 'User') {
        {

     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$user_recipient', $title, $notification, '$date', '$status')";
    $this->db->query($insert_query); 
       }
     
                                                          }




     elseif ($platform_id == "2" && $recipients == 'Channel') {
        

     foreach($channel_recipients as $channel_recipient)
    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipient', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    }
                                                            }
    // oflo followers
     


    elseif ($platform_id == '2' && $recipients =='Followers') {

    

     
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, followers, feed_notif, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipients', '$followers', 'n', '$user_recipients', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    

                                                         }


    
      // syndicator


      elseif ($platform_id == '1' && $recipients == 'All') {

    
   
    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipient', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    }

                                                          }
  
  
     elseif ($platform_id == '1' && $recipients == 'Channel') {

    
     foreach($channel_recipients as $channel_recipient)
    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, channel_recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$channel_recipient', '$user_recipient', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    }
          
                                                          }



    elseif ($platform_id == '1' && $recipients == 'User') {

    
     foreach($user_recipients as $user_recipient)
    {
     $insert_query = "INSERT IGNORE INTO platform_notification (platform_id, recipients, user_recipients, title, message, date, status) 
                     VALUES('$platform_id', '$recipients', '$user_recipient', $title, $notification, '$date', '$status')";

                   
                      
                     
    $this->db->query($insert_query); 
    
    }
          
                                                          }



    }

    //push recipients 

    function get_push_recipients(){

        $sql = $this->db->query('SELECT * FROM push_recipients');
        
        return $sql->result_array();

    }

    // get oflo channels

    function get_oflo_channels(){

        $sql = " SELECT *
                    FROM enr_chanel ec
                    LEFT JOIN users uo ON uo.cid = ec.id
                    WHERE ec.is_oflo ='y'";
  
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows; }

            // get oflo channels

    function get_oflo_users(){

        $sql = " SELECT *
                    FROM enr_chanel ec
                    LEFT JOIN users uo ON uo.cid = ec.id
                    WHERE ec.is_oflo ='y'";
  
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows; }

    



  // platform notification deletion 
   function confirm_deletion($data){
    $this->db->select('*');
    $this->db->from('platform_notification');
    $this->db->where('id',$data);
    $query = $this->db->get();
    $result=$query->result();
    return $result;
    }

    function get_platform_names(){

        $sql = $this->db->query('SELECT * FROM platform_form');
        
        return $sql->result_array();

    }
    

     function get_notifications_all(){

        $sql = "SELECT *
                FROM platform_notification";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;

    }

      function get_followers_id($data){
     
     $this->db->select('*');
     $this->db->from('enr_follows');
     $this->db->where('channel_id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
      return $result;
    }


     function get_notification_id($data){
        // $sql = SELECT * FROM viral_media_category WHERE id = '.$id.';
     $this->db->select('*');
     $this->db->from('platform_notification');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     // var_dump(query);    
     return $result;
    }



     function update_notification($id,$data){

     $this->db->where('id', $id);
     $result = $this->db->update('platform_notification', $data);

     return $result;
    }

    /**
     * Get enr_video Viral Posts.
     *
     * @param $category
     * @param $search
     * @return mixed
     */
    function getViralPosts( $category, $search){

        $sql = "SELECT * FROM (
                (
                    SELECT concat('video_', ev.id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( ev.time_stamp ) , '%Y-%m-%d') date, ev.credit, ev.is_publish, ev.destination, ev.image_path, ev.tags, ev.description, ev.time_stamp, ev.start_date, ev.fan_id, ev.is_sticky, ev.copy_origin_id, ev.copy_timestamp, ev.deep_link_app, ev.deep_link_web, ev.deep_link_web_tw, ev.brightcove_media_id
                    FROM enr_video ev ".

            ($category != NULL ?
                    " LEFT JOIN viral_media_cat_post vmcp ON vmcp.post_id = ev.id
                      LEFT JOIN viral_media_category vmc ON vmc.id = vmcp.cat_id " : "").

                    " WHERE ev.is_publish=1 AND
                            ev.cid='64' ".

            ($category != NULL ? " AND vmc.cat_name LIKE '%".$this->db->escape_like_str($category)."%' " : "").
            ($search != NULL ? " AND ev.description LIKE '%".$this->db->escape_like_str($search)."%' ": "").

            "   )
            ) t1
            WHERE t1.fan_id=-1
            ORDER BY t1.is_sticky DESC, t1.time_stamp DESC
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;

    }

    function getFeedText($id){
        $sql = "
            select *
            from enr_text
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }



    function get_text_from_feed()
    {
        $sql = $this->db->query('SELECT description FROM enr_text WHERE cid = 9');
        return $sql->result();
        /* you simply return the results as an object
         * also note you can use the ActiveRecord class for this...might make it easier
         */
    }

    function getHelpsContent($cid, $type='shop'){
        $sql = "select *
            from enr_helps
            where cid='{$cid}' and type='{$type}'
            limit 1
        ";
        $query = $this->db->query($sql);
        $helps = $query->row_array();
        return $helps;
    }
    function updateHelpsContent($cid, $type='shop', $content=''){
        $this->db->where('cid', $cid);
        $this->db->where('type', $type);
        $this->db->delete('enr_helps');

        $sql = array(
            'cid' => $cid,
            'type' => $type,
            'content' => $content
        );
        $this->db->insert('enr_helps', $sql);
    }
    /**
     * save from phpthumb
     *
     */
    function saveResizeImage($img_path, $width=320, $dest_path=false){
        $relativepath = str_replace(BASE_URL, '', UPLOAD_URL.$img_path);
        $img_url = BASE_URL."phpThumb.php?src=".$relativepath."&wp={$width}";

        $ch = curl_init($img_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $rawdata = curl_exec($ch);
        curl_close($ch);

        if($dest_path !== false){
            $year = date('Y');
            $month = date('m');
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;
            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }
            $filepath .= "/".$month;
            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }
            $ext = pathinfo($img_path, PATHINFO_EXTENSION);
            $filepath .= "/".$filename.".".$ext;
        }else{
            $filepath = UPLOAD_DIR.$img_path;
        }
        $fp = fopen($filepath, 'w');
        fwrite($fp, $rawdata);
        fclose($fp);
        return str_replace(UPLOAD_DIR, "", $filepath);

    }

    /**
     * Add photo to feed.
     *
     * @param $feed
     * @param $catid
     * @return bool
     */
    function addFeedPhoto($feed,$catid){

        $designinfo = $this->getDesignInfo();
        $l_bCLOSE_BUFFER = 1;

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }

        $filepath = $this->_uploadPhoto();

        list($width,$height)=getimagesize(UPLOAD_DIR.$filepath);
        $aspect_ratio = $height/$width;
        @unlink(UPLOAD_DIR.$filepath);

        $slider_thumb = $this->_uploadSliderphotofeed();
        @unlink(UPLOAD_DIR.$filepath);


        $post = array(
            'cid' => $feed['cid'],
            'fan_id' => -1,
            'cid' => $feed['cid'],
            'fan_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'destination' => $filepath,
            'image_mime' => $_FILES['image']['type'],
            'time_stamp' => time(),
            'slider_thumb'=>$slider_thumb,
            'url_link' => $feed['url_link'],
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            "feed_date" => $feed['feed_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0,
            'is_oflo' => $feed['is_oflo'],
            'is_oflo_slider' => $feed['is_oflo_slider'],
            'image_aspect_ratio' => $aspect_ratio,
            'is_premium' => $feed['is_premium'],
            'is_feed' => $feed['is_feed'],
            'product_widget' => $feed['product_widget'],
            


        );

        $res = $this->db->insert("enr_photo", $post);
        $id = $this->db->insert_id();

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        $viral_media_category = $this->data['viral_media_category'];

        $i=0;
        while($viral_media_category[$i]){
            
            $cat_id = $viral_media_category[$i]['id'];

            if($this->input->post($cat_id)){
              $insert = "INSERT IGNORE INTO viral_media_cat_post (cat_id, post_id, content_type_id)
                        VALUES($cat_id, $id, ".CONTENT_TYPE_ID_PHOTO.")";

                $this->db->query($insert);

            }
            $i++;
        }

                  if($this->input->post('priorityCheckBox')){
               
                    $prioritySelected = $this->input->post('prioritySelected');
                    $priorityDaySelected = $this->input->post('priorityDaySelected');
                    $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

                     $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                            VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')";

                
                 
                      $this->db->query($insert);
                    }


     
        /* if($l_bCLOSE_BUFFER){
                  Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

             window.location.href='/content/postfeed/';
             </SCRIPT>");
                 }     */

        if($filepath){

            $this->updateDeepLink('photo', $id);
            $this->insertFeed('photo', $id, $post['fan_id'], $feed['is_publish']);
            $this->updateLastPost($post['fan_id']);

            $l_nChannelId = $feed['cid'];
            $l_sDescription = $feed['description'];

            if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18 || $l_nChannelId == 24){
                            $SCAPI= new SCAPI();
                            $SCAPI->rq_sendToLegacyServer($l_nChannelId,'photo',UPLOAD_DIR.$filepath,$l_sDescription);
                        }

            $this->break_oflo_photo_memcache($feed);

            $this->load->helper('date');

            if($feed['post_to_facebook']=="true"){

                $post = array(
                    'post_type_id' => 2,
                    'channel_id' => $feed['cid'],
                    'content_id' => $id,
                    'social_property_type_id' => 1,
                    'channel_oauth_id' => NULL,
                    'message' => $feed['description'],
                    'has_sent' => 'N',
                    'created' => date("Y-m-d H:i:s")
                );

                $res = $this->db->insert("enr_content_post_to_social", $post);
                $postid = $this->db->insert_id();
                $l_bIsVideo = false;

                $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                        FROM enr_content_post_to_social cs
                        JOIN enr_channel_oauth co
                        ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                        JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                        ON (cs.content_id = a.id )

                        JOIN enr_chanel c
                        ON (cs.channel_id = c.id )

                        WHERE cs.has_sent = "N"
                        AND co.active = "Y"
                        AND cs.id = '.intval($postid);

                $query = $this->db->query($sql);


                foreach($query->result_array() as $row){

                    $data = array(
                        'page_id' => $row['page_id'],
                        'social_token' => $row['social_token'],
                        'link' => $row['deep_link_web_oflo_lp'],
                        'picture' => UPLOAD_CLOUD.$row['content_file'],
                        'message' => $row['message'],
                        'main_id' => $row['main_id']
                    );

                    $this->share_to_facebook($data);
                }

            }

            if($feed['post_to_twitter']=="true"){

                $post = array(
                    'post_type_id' => 2,
                    'channel_id' => $feed['cid'],
                    'content_id' => $id,
                    'social_property_type_id' => 2,
                    'channel_oauth_id' => NULL,
                    'message' => $feed['description'],
                    'has_sent' => 'N',
                    'created' => date("Y-m-d H:i:s")
                );

                $res = $this->db->insert("enr_content_post_to_social", $post);
                $postid = $this->db->insert_id();

                $l_bIsVideo = false;

                $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                        FROM enr_content_post_to_social cs
                        JOIN enr_channel_oauth co
                        ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                        JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                        ON (cs.content_id = a.id )

                        JOIN enr_chanel c
                        ON (cs.channel_id = c.id )

                        WHERE cs.has_sent = "N"
                        AND co.active = "Y"
                        AND cs.id = '.intval($postid);

                $query = $this->db->query($sql);

          
                foreach($query->result_array() as $row){

                    $oath_token = $row['social_token'];
                    $oath_secret_token = $row['social_token_secret'];
                    $page_id = $row['page_id'];

                    $twitter = new TwitterOAuth("292aZXJ21hJRD3TwaL4nhTSzg", 'E2TuUxUNCUpnBj59RzT08V0c30HrRT9owKociLyrxKQiHTJtR1', $oath_token, $oath_secret_token);

                    $twitter_response = $twitter->post('statuses/update', array('status' => $row['deep_link_web_oflo_lp'].' '.$row['message']));

                    

                     $posted_id = $twitter_response->id;

                    $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                    $querynew = $this->db->query($sql);
                }

            }

            if($feed['post_to_tumblr']=="true"){

                $post = array(
                    'post_type_id' => 2,
                    'channel_id' => $feed['cid'],
                    'content_id' => $id,
                    'social_property_type_id' => 4,
                    'channel_oauth_id' => NULL,
                    'message' => $feed['description'],
                    'has_sent' => 'N',
                    'created' => date("Y-m-d H:i:s")
                );

                $res = $this->db->insert("enr_content_post_to_social", $post);
                $postid = $this->db->insert_id();

                $l_bIsVideo = false;

                $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                                
                        FROM enr_content_post_to_social cs
                        JOIN enr_channel_oauth co
                        ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                        JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                        ON (cs.content_id = a.id )

                        JOIN enr_chanel c
                        ON (cs.channel_id = c.id )

                        WHERE cs.has_sent = "N"
                        AND co.active = "Y"
                        AND cs.id = '.intval($postid);

                $query = $this->db->query($sql);
                foreach($query->result_array() as $row){

                    $oath_token = $row['social_token'];
                    $oath_secret_token = $row['social_token_secret'];
                    echo "\n\n";

                    $tum_url = preg_replace("/https?:\/\//","",$row['page_name']);
                    $tum_url = preg_replace("/\//","",$tum_url);

                    $l_bSuppressOAuth = true;
                    require_once(LIB_PATH.'tumblr/tumblroauth.php');

                    $tumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$oath_token,$oath_secret_token);
                    $post_url = 'http://api.tumblr.com/v2/blog/'.$tum_url.'/post';

                    $params = array();

                    $params['type'] = "photo";
                    $params['link'] = $row['deep_link_web_oflo_lp'];
                    $params['source'] = UPLOAD_CLOUD.'watermark/'.$row['content_file'];
                    $params['caption'] = $row['message'];


                    $tumblr_response = $tumblr->post($post_url,$params);


                    if (201 == $tumblr_response->meta->status) {
                        $posted_id = $tumblr_response->response->id;
                        $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                        $querynew = $this->db->query($sql);
                    } else {
                        echo "error";
                        print_r($tumblr_response);
                    }
                }

            }

            /*
            if($feed['post_to_gp']=="true"){

                $post = array(
            'post_type_id' => 4,
            'channel_id' => $feed['cid'],
            'content_id' => $id,
            'social_property_type_id' => 2,
            'channel_oauth_id' => '',
            'message' => $feed['description'],
            'has_sent' => 'N',
            'created' => date("Y-m-d H:i:s")
            );

            } */

        @unlink(UPLOAD_DIR.$filepath);
        @unlink(UPLOAD_DIR."big_thumb/".$filepath);
        @unlink(UPLOAD_DIR."thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_square_thumb/".$filepath);
       
            return $id;
        }


        return false;
    }

    function getFeedPhoto($id){
        $sql = "
            select *
            from enr_photo
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    function updateFeedPhoto($feed){

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if($feed['time_stamp'] > time()) {
            $this->controller->data['show_errors'] = "Invalid post date and time.";
            return false;
        }

        $id = ltrim($feed['feed_id'], "photo_");
        $oFeed = $this->getFeedPhoto($id);

        if(($id!="0" || $id!="")&& $l_bCLOSE_BUFFER){
            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>
            
            window.location.href='/content/postfeed/';
            </SCRIPT>");
        }
         if($_FILES['image_slider']['size']){
            $slider_thumb= UPLOAD_DIR.$oFeed['slider_thumb'];
            if(is_file($slider_thumb)){
                unlink($slider_thumb);
            }
            $slider_thumb = $this->_uploadSliderphotofeed();

            if(!$slider_thumb){
                return false;
            }
        }else{
            $slider_thumb = $oFeed['slider_thumb'];
            $aspect_ratio = $oFeed['image_aspect_ratio'];

        }

        if($_FILES['image']['size']){
            $destination = UPLOAD_DIR.$oFeed['destination'];
            if(is_file($destination)){
                unlink($destination);
            }
            $filepath = $this->_uploadPhoto();

            list($width,$height)=getimagesize(UPLOAD_DIR.$filepath);
            $aspect_ratio = $height / $width;


            if(!$filepath){
                return false;
            }
        }else{
            $filepath = $oFeed['destination'];
            $aspect_ratio = $oFeed['image_aspect_ratio'];
        }

        $post = array(
            'cid' => $feed['cid'],
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'destination' => $filepath,
            'slider_thumb'=>$slider_thumb,
            'url_link' => $feed['url_link'],
            'image_mime' => $_FILES['image']['type'],
            'url_link' => $feed['url_link'],
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            'feed_date' => $feed['feed_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky'],
            'is_oflo' => $feed['is_oflo'],
            'is_oflo_slider' => $feed['is_oflo_slider'],
            'image_aspect_ratio' => $aspect_ratio,
            'is_premium' => $feed['is_premium'],
            'is_feed' => $feed['is_feed'],
            'is_feed' => $feed['product_widget'],

        );

        $l_nChannelId = $feed['cid'];
        $l_sDescription = $feed['description'];

        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18 || $l_nChannelId == 24){
            $SCAPI= new SCAPI();
            $SCAPI->rq_sendToLegacyServer($l_nChannelId,'photo',UPLOAD_DIR.$filepath,$l_sDescription);
        }

        if($feed['time_stamp'] != 0) {
            $post['time_stamp'] = $feed['time_stamp'];
        }

       
        $this->db->where("id", $id);
        $res = $this->db->update("enr_photo", $post);

        $feed_id = $this->get_feed_by_content_id('photo', $id);
        if(!empty($feed_id) && $feed['is_publish'] == 1) {
            $this->sendpush($feed_id);
        }

        $this->updateDeepLink('photo', $id);

        $this->break_oflo_photo_memcache($feed);


        if($feed['post_to_facebook']=="true"){

            $post = array(
                'post_type_id' => 2,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 1,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();
            $l_bIsVideo = false;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);


            foreach($query->result_array() as $row){

                $data = array(
                    'page_id' => $row['page_id'],
                    'social_token' => $row['social_token'],
                    'link' => $row['deep_link_web_oflo_lp'],
                    'picture' => UPLOAD_CLOUD.$row['content_file'],
                    'message' => $row['message'],
                    'main_id' => $row['main_id']
                );

                $this->share_to_facebook($data);
            }


        }

        if($feed['post_to_twitter']=="true"){

            $post = array(
                'post_type_id' => 2,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 2,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();

            $l_bIsVideo = false;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);

 
            foreach($query->result_array() as $row){

                $oath_token = $row['social_token'];
                $oath_secret_token = $row['social_token_secret'];
                $page_id = $row['page_id'];

                $twitter = new TwitterOAuth("292aZXJ21hJRD3TwaL4nhTSzg", 'E2TuUxUNCUpnBj59RzT08V0c30HrRT9owKociLyrxKQiHTJtR1', $oath_token, $oath_secret_token);

                $twitter_response = $twitter->post('statuses/update', array('status' => $row['deep_link_web_oflo_lp'].' '.$row['message']));
           
                $posted_id = $twitter_response->id;

                $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                $querynew = $this->db->query($sql);
            }

        }

        if($feed['post_to_tumblr']=="true"){

            $post = array(
                'post_type_id' => 2,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 4,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();

            $l_bIsVideo = false;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                                
                                FROM enr_content_post_to_social cs 
                                JOIN enr_channel_oauth co  
                                ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                                
                                JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                                ON (cs.content_id = a.id )
                                
                                JOIN enr_chanel c  
                                ON (cs.channel_id = c.id )

                                WHERE cs.has_sent = "N" 
                                AND co.active = "Y"
                                AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);
            foreach($query->result_array() as $row){

                $oath_token = $row['social_token'];
                $oath_secret_token = $row['social_token_secret'];
                echo "\n\n";

                $tum_url = preg_replace("/https?:\/\//","",$row['page_name']);
                $tum_url = preg_replace("/\//","",$tum_url);

                $l_bSuppressOAuth = true;
                require_once(LIB_PATH.'tumblr/tumblroauth.php');

                $tumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$oath_token,$oath_secret_token);
                $post_url = 'http://api.tumblr.com/v2/blog/'.$tum_url.'/post';

                $params = array();

                $params['type'] = "photo";
                $params['link'] = $row['deep_link_web_oflo_lp'];
                $params['source'] = UPLOAD_CLOUD.'watermark/'.$row['content_file'];
                $params['caption'] = $row['message'];


                $tumblr_response = $tumblr->post($post_url,$params);


                if (201 == $tumblr_response->meta->status) {
                    $posted_id = $tumblr_response->response->id;
                    $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                    $querynew = $this->db->query($sql);
                } else {
                    echo "error";
                    print_r($tumblr_response);
                }
            }

        }

        @unlink(UPLOAD_DIR.$filepath);
        @unlink(UPLOAD_DIR."big_thumb/".$filepath);
        @unlink(UPLOAD_DIR."thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_thumb/".$filepath);
        @unlink(UPLOAD_DIR."small_square_thumb/".$filepath);
      

        return $res;
    }

    /**
     * Break O-Flo video memcache.
     *
     * @param $feed
     */
    function break_oflo_video_memcache($feed){
        $url = $this->controller->chanel['url'];

        // Break O-Flo Content Slider memcache on O-Flo Homepage
        if ($feed['is_oflo_slider'] == 'y'){
            $oflo_homepage_slider_content_MemcacheKey = Memcaching::makeKey("oflo_homepage_slider_content");

            Memcaching::getInstance()->removeKey($oflo_homepage_slider_content_MemcacheKey);
        }

        // Break O-Flo & O-Flo Channel Flow Player videos, and O-Flo Homepage Content Grids' memcache
        if ($feed['is_oflo'] == 'y'){
            $oflo_homepage_content_MemcacheKey = Memcaching::makeKey("oflo_homepage_content");
            $oflo_fp_MemcacheKey = Memcaching::makeKey('oflo-fp-videos');
            $oflo_channel_fp_MemcacheKey = Memcaching::makeKey($url.'oflo-fp-videos');

            Memcaching::getInstance()->removeKey($oflo_homepage_content_MemcacheKey);
            Memcaching::getInstance()->removeKey($oflo_fp_MemcacheKey);
            Memcaching::getInstance()->removeKey($oflo_channel_fp_MemcacheKey);
        }
    }

    /**
     * Break O-Flo photo memcache.
     *
     * @param $feed
     */
    function break_oflo_photo_memcache($feed){
        $url = $this->controller->chanel['url'];

        // Break O-Flo Homepage Content Slider memcache
        if ($feed['is_oflo_slider'] == 'y'){
            $oflo_homepage_slider_content_MemcacheKey = Memcaching::makeKey("oflo_homepage_slider_content");

            Memcaching::getInstance()->removeKey($oflo_homepage_slider_content_MemcacheKey);
        }

        // Break O-Flo & O-Flo Channel photos
        if ($feed['is_oflo'] == 'y'){
//                $oflo_homepage_content_MemcacheKey = Memcaching::makeKey("oflo_homepage_content");
            $oflo_photos_MemcacheKey = Memcaching::makeKey('oflo-photos');
            $oflo_channel_photos_MemcacheKey = Memcaching::makeKey($url.'oflo-photos');

//                Memcaching::getInstance()->removeKey($oflo_homepage_content_MemcacheKey);
            Memcaching::getInstance()->removeKey($oflo_photos_MemcacheKey);
            Memcaching::getInstance()->removeKey($oflo_channel_photos_MemcacheKey);
        }
    }

    function deleteFeedText($id){
        $this->db->where('id', $id);
        $this->db->delete("enr_text");
    }


    /**
     * Delete feed photo.
     *
     * @param $id
     */
    function deleteFeedPhoto($id){
        $oFeed = $this->getFeedPhoto($id);
        $destination = UPLOAD_DIR.$oFeed['destination'];
        if(is_file($destination)){
            unlink($destination);
        }

        $this->db->where('id', $id);
        $this->db->delete("enr_photo");

        $this->deleteFeed_Like(CONTENT_TYPE_ID_PHOTO, $id);
    }

    /**
     * Delete feed Video.
     *
     * @param $id
     */
    function deleteFeedVideo($id){
        $oFeed = $this->getFeedVideo($id);
        $destination = UPLOAD_DIR.$oFeed['destination'];
        if(is_file($destination)){
            unlink($destination);
        }
        $image_path = UPLOAD_DIR.$oFeed['image_path'];
        if(is_file($image_path)){
            unlink($image_path);
        }

        $this->db->where('id', $id);
        $this->db->delete("enr_video");


        $this->deleteFeed_Like(CONTENT_TYPE_ID_VIDEO, $id);

    }

    /**
     * Delete feed livefyre like.
     * 
     * @param $post_type_id
     * @param $content_id
     */
    function deleteFeed_Like($post_type_id, $content_id){

        $this->db->where('post_type_id', $post_type_id);
        $this->db->where('content_id', $content_id);

        $this->db->delete("enr_like");
    }




    /**
     * Resize and Upload photo to S3.
     *
     * @param string $name
     * @return mixed
     */
    private function _uploadPhoto($name='image'){

        $year = date('Y');
        $month = date('m');
        $url = $this->controller->chanel['url'];
        $fileinfo = $_FILES[$name];

        if(!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a photo file.";
            return false;
        }

        $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));

        if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
            return false;
        }

        $filename = md5(time().rand(0.1, 99.9));

        $filepath             = UPLOAD_DIR."photo/".$year;
        $filepath_big_thumb   = UPLOAD_DIR."big_thumb/photo/".$year;
        $filepath_thumb       = UPLOAD_DIR."thumb/photo/".$year;
        $filepath_small_thumb = UPLOAD_DIR."small_thumb/photo/".$year;
        $filepath_small_square_thumb = UPLOAD_DIR."small_square_thumb/photo/".$year;

        $file_a3_path             = UPLOAD_AMZ."photo/".$year;
        $file_a3_path_big_thumb   = UPLOAD_AMZ."big_thumb/photo/".$year;
        $file_a3_path_thumb       = UPLOAD_AMZ."thumb/photo/".$year;
        $file_a3_path_small_thumb = UPLOAD_AMZ."small_thumb/photo/".$year;
        $file_a3_path_small_square_thumb = UPLOAD_AMZ."small_square_thumb/photo/".$year;

     

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_square_thumb)){
            mkdir($filepath_small_square_thumb, 0777, true);
        }

        $filepath             .= "/".$month;
        $filepath_big_thumb   .= "/".$month;
        $filepath_thumb       .= "/".$month;
        $filepath_small_thumb .= "/".$month;
        $filepath_small_square_thumb .= "/".$month;

        $file_a3_path             .= "/".$month;
        $file_a3_path_big_thumb   .= "/".$month;
        $file_a3_path_thumb       .= "/".$month;
        $file_a3_path_small_thumb .= "/".$month;
        $file_a3_path_small_square_thumb .= "/".$month;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_square_thumb)){
            mkdir($filepath_small_square_thumb, 0777, true);
        }

        $filepath             .= "/".$filename.".".$ext;
        $filepath_big_thumb   .= "/".$filename.".".$ext;
        $filepath_thumb       .= "/".$filename.".".$ext;
        $filepath_small_thumb .= "/".$filename.".".$ext;
        $filepath_small_square_thumb .= "/".$filename.".".$ext;

        $file_a3_path             .= "/".$filename.".".$ext;
        $file_a3_path_big_thumb   .= "/".$filename.".".$ext;
        $file_a3_path_thumb       .= "/".$filename.".".$ext;
        $file_a3_path_small_thumb .= "/".$filename.".".$ext;
        $file_a3_path_small_square_thumb .= "/".$filename.".".$ext;

        move_uploaded_file($fileinfo['tmp_name'], $filepath);

//        if(stripos("jpg, png, jpeg, gif", $ext) === false){
////            $this->compress_image($filepath, $filepath, 50);
//        }

        $file = $filepath;
        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);


        // Big Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

        // For our purposes, I have resized the image to be
        // 640 pixels wide, and maintain the original aspect
        // ratio. This prevents the image from being "stretched"
        // or "squashed". If you prefer some max width other than
        // 640, simply change the $newwidth variable
        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_big_thumb);

        // Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

        // For our purposes, I have resized the image to be
        // 320 pixels wide, and maintain the original aspect
        // ratio. This prevents the image from being "stretched"
        // or "squashed". If you prefer some max width other than
        // 320, simply change the $newwidth variable
        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);

        // Small Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

        // For our purposes, I have resized the image to be
        // 180 pixels wide, and maintain the original aspect
        // ratio. This prevents the image from being "stretched"
        // or "squashed". If you prefer some max width other than
        // 180, simply change the $newwidth variable
        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_thumb);

    

        // Small Square Thumb

        // We get the square side and the offsets
        list($width,$height)=getimagesize($file);

        // horizontal rectangle
        if ($width > $height) {
            $square = $height;                  // $square: square side length
            $offsetX = ($width - $height) / 2;  // x offset based on the rectangle
            $offsetY = 0;                       // y offset based on the rectangle
        }
        // vertical rectangle
        elseif ($height > $width) {
            $square = $width;
            $offsetX = 0;
            $offsetY = ($height - $width) / 2;
        }
        // it's already a square
        else {
            $square = $width;
            $offsetX = $offsetY = 0;
        }

        // Load original image
        $src = $this->loadImage($file);

        $newwidth  = 180;
        $newheight = 180;

        // Resize the image to be however pixels wide, and maintain the original aspect ratio.
        $tmp=imagecreatetruecolor($newwidth, $newheight);

        // Copy from the original image into the $tmp image
        imagecopyresampled($tmp, $src, 0, 0, $offsetX, $offsetY, $newwidth, $newheight, $square, $square);

        // Write the resized image jpeg to disk
        imagejpeg($tmp,$filepath_small_square_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_square_thumb;



        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_square_thumb);


        $filemain = str_replace(UPLOAD_DIR, "", $filepath);



        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        @unlink($filepath_big_thumb);
        @unlink($filepath_thumb);
        @unlink($filepath_small_thumb);
        @unlink($filepath_small_square_thumb);

        return $filemain;
    }


     private function _uploadCategoryPhoto($name='image'){

        $year = date('Y');
        $month = date('m');
        $url = $this->controller->chanel['url'];
        $fileinfo = $_FILES[$name];

        if(!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a photo file.";
            return false;
        }

        $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));

        if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
            return false;
        }

        $filename = md5(time().rand(0.1, 99.9));

        $filepath             = UPLOAD_DIR."photo/".$year;
        $filepath_big_thumb   = UPLOAD_DIR."big_thumb/photo/".$year;
        $filepath_thumb       = UPLOAD_DIR."thumb/photo/".$year;
        $filepath_small_thumb = UPLOAD_DIR."small_thumb/photo/".$year;
        $filepath_small_square_thumb = UPLOAD_DIR."small_square_thumb/photo/".$year;

        $file_a3_path             = UPLOAD_AMZ."photo/".$year;
        $file_a3_path_big_thumb   = UPLOAD_AMZ."big_thumb/photo/".$year;
        $file_a3_path_thumb       = UPLOAD_AMZ."thumb/photo/".$year;
        $file_a3_path_small_thumb = UPLOAD_AMZ."small_thumb/photo/".$year;
        $file_a3_path_small_square_thumb = UPLOAD_AMZ."small_square_thumb/photo/".$year;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_square_thumb)){
            mkdir($filepath_small_square_thumb, 0777, true);
        }

        $filepath             .= "/".$month;
        $filepath_big_thumb   .= "/".$month;
        $filepath_thumb       .= "/".$month;
        $filepath_small_thumb .= "/".$month;
        $filepath_small_square_thumb .= "/".$month;

        $file_a3_path             .= "/".$month;
        $file_a3_path_big_thumb   .= "/".$month;
        $file_a3_path_thumb       .= "/".$month;
        $file_a3_path_small_thumb .= "/".$month;
        $file_a3_path_small_square_thumb .= "/".$month;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_square_thumb)){
            mkdir($filepath_small_square_thumb, 0777, true);
        }

        $filepath             .= "/".$filename.".".$ext;
        $filepath_big_thumb   .= "/".$filename.".".$ext;
        $filepath_thumb       .= "/".$filename.".".$ext;
        $filepath_small_thumb .= "/".$filename.".".$ext;
        $filepath_small_square_thumb .= "/".$filename.".".$ext;

        $file_a3_path             .= "/".$filename.".".$ext;
        $file_a3_path_big_thumb   .= "/".$filename.".".$ext;
        $file_a3_path_thumb       .= "/".$filename.".".$ext;
        $file_a3_path_small_thumb .= "/".$filename.".".$ext;
        $file_a3_path_small_square_thumb .= "/".$filename.".".$ext;

        move_uploaded_file($fileinfo['tmp_name'], $filepath);

//        if(stripos("jpg, png, jpeg, gif", $ext) === false){
////            $this->compress_image($filepath, $filepath, 50);
//        }

        $file = $filepath;
        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);


        // Big Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

        // For our purposes, I have resized the image to be
        // 100 pixels wide, and maintain the original aspect
        // ratio. This prevents the image from being "stretched"
        // or "squashed". If you prefer some max width other than
        // 100, simply change the $newwidth variable
        $newwidth=100;
        $newheight=($height/$width)*100;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_big_thumb);

        // Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

      
        $newwidth=100;
        $newheight=($height/$width)*100;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);

        // Small Thumb

        // Create an Image from it so we can do the resize
        $src = imagecreatefromjpeg($file);

        // Capture the original size of the uploaded image
        list($width,$height)=getimagesize($file);

     
        $newwidth=100;
        $newheight=($height/$width)*100;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_thumb);

        // Small Square Thumb

        // We get the square side and the offsets
        list($width,$height)=getimagesize($file);

        // horizontal rectangle
        if ($width > $height) {
            $square = $height;                  // $square: square side length
            $offsetX = ($width - $height) / 2;  // x offset based on the rectangle
            $offsetY = 0;                       // y offset based on the rectangle
        }
        // vertical rectangle
        elseif ($height > $width) {
            $square = $width;
            $offsetX = 0;
            $offsetY = ($height - $width) / 2;
        }
        // it's already a square
        else {
            $square = $width;
            $offsetX = $offsetY = 0;
        }

        // Load original image
        $src = $this->loadImage($file);

        $newwidth  = 100;
        $newheight = 100;

        // Resize the image to be however pixels wide, and maintain the original aspect ratio.
        $tmp=imagecreatetruecolor($newwidth, $newheight);

        // Copy from the original image into the $tmp image
        imagecopyresampled($tmp, $src, 0, 0, $offsetX, $offsetY, $newwidth, $newheight, $square, $square);

        // Write the resized image jpeg to disk
        imagejpeg($tmp,$filepath_small_square_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_square_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_square_thumb);

        $filemain = str_replace(UPLOAD_DIR, "", $filepath);


        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        @unlink($filepath);
        @unlink($filepath_big_thumb);
        @unlink($filepath_thumb);
        @unlink($filepath_small_thumb);
        @unlink($filepath_small_square_thumb);

        return $filemain;
    }


    /**
     * Upload cropped photo.
     *
     * @param string $name
     * @return mixed
     */
    private function _uploadCropedPhoto($name='image'){

        $url = $this->controller->chanel['url'];

        $year = date('Y');
        $month = date('m');
        $filename = md5(time().rand(0.1, 99.9));

        $fileinfo = $_FILES[$name];

        if (!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a photo file.";
            return false;
        }

        $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));

        if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
            return false;
        }

        $filepath             = UPLOAD_DIR."photo/".$year."/".$month;
        $filepath_big_thumb   = UPLOAD_DIR."big_thumb/photo/".$year."/".$month;
        $filepath_thumb       = UPLOAD_DIR."thumb/photo/".$year."/".$month;
        $filepath_small_thumb = UPLOAD_DIR."small_thumb/photo/".$year."/".$month;

        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_big_thumb   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_thumb       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_small_thumb = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".".$ext;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        $filepath             .= "/".$filename.".".$ext;
        $filepath_big_thumb .= "/".$filename.".".$ext;
        $filepath_thumb       .= "/".$filename.".".$ext;
        $filepath_small_thumb .= "/".$filename.".".$ext;

        //Creating Photo
        $source = $this->loadImage($fileinfo['tmp_name']);

        list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
        
     
        // Design header image coords
        if (isset($_REQUEST['image1'])){
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }

        // Design logo image coords
        if (isset($_REQUEST['image4'])){
            $rw = isset($_REQUEST['photo2_width']) ? $_REQUEST['photo2_width'] : $ow;
            $rh = isset($_REQUEST['photo2_height']) ? $_REQUEST['photo2_height'] : $oh;
            $x1 = isset($_REQUEST['photo2_crop_x1']) ? $_REQUEST['photo2_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo2_crop_y1']) ? $_REQUEST['photo2_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo2_crop_x2']) ? $_REQUEST['photo2_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo2_crop_y2']) ? $_REQUEST['photo2_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo2_crop_w']) ? $_REQUEST['photo2_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo2_crop_h']) ? $_REQUEST['photo2_crop_h'] : $rh;

        }
        // Design chanel icon coords

        if (isset($_REQUEST['image7'])){
             $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }  


        // Design background image coords

        if (isset($_REQUEST['image3'])){
            $rw = isset($_REQUEST['photo3_width']) ? $_REQUEST['photo3_width'] : $ow;
            $rh = isset($_REQUEST['photo3_height']) ? $_REQUEST['photo3_height'] : $oh;
            $x1 = isset($_REQUEST['photo3_crop_x1']) ? $_REQUEST['photo3_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo3_crop_y1']) ? $_REQUEST['photo3_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo3_crop_x2']) ? $_REQUEST['photo3_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo3_crop_y2']) ? $_REQUEST['photo3_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo3_crop_w']) ? $_REQUEST['photo3_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo3_crop_h']) ? $_REQUEST['photo3_crop_h'] : $rh;

        } 

              // category photo

        if (isset($_REQUEST['image10'])){
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

        } 

         // category image coords

        if (isset($_REQUEST['image9'])){
            $rw = isset($_REQUEST['photo9_width']) ? $_REQUEST['photo9_width'] : $ow;
            $rh = isset($_REQUEST['photo9_height']) ? $_REQUEST['photo9_height'] : $oh;
            $x1 = isset($_REQUEST['photo9_crop_x1']) ? $_REQUEST['photo9_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo9_crop_y1']) ? $_REQUEST['photo9_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo9_crop_x2']) ? $_REQUEST['photo9_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo9_crop_y2']) ? $_REQUEST['photo9_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo9_crop_w']) ? $_REQUEST['photo9_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo9_crop_h']) ? $_REQUEST['photo9_crop_h'] : $rh;

         }else { // All other image coords

            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }

        $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
        $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
        $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

        if($cw == 0 || $ch == 0) {
            $cw = $ow;
            $ch = $oh;
        }


        $dest = imagecreatetruecolor($cw, $ch);

        imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

        // Create png for header and logo design elements and category images
        if (isset($_REQUEST['image1']) || isset($_REQUEST['image4']) || isset($_REQUEST['image10'])){

            imagepng($dest, $filepath);

        } else { // Create jpg for all other images

            imagejpeg($dest, $filepath);

        }

        imagedestroy($source);
        imagedestroy($dest);

        $file = $filepath;
        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);

        // Big Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_big_thumb);


        // Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);


        // Small Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_thumb);
        
        /* Watermarking of Images */

        list($sw, $sh) = getimagesize($filepath);

        // Calculate simulated size
        $width = $this->uri->segment(5);

        if (empty($width) || $width == NULL) { $width = 640; }

        $height = round($sh * $width / $sw);

//        $cid = $this->getChanelOptions();
//
//        $designinfo = $this->getDesignInfo($cid[id]);
        $watermark = THEME_CLOUD."watermark.png";
        
        list($sww, $swh) = getimagesize($watermark);
        // Resized watermark size
        $ww = round($width / 3);
        $wh = round($swh * $ww / $sww);

        // Load original image
        $ext = exif_imagetype ($file);
        $hSource = NULL;

        if($ext == IMAGETYPE_PNG) $hSource = imagecreatefrompng ($file);
        elseif($ext == IMAGETYPE_JPEG) $hSource = imagecreatefromjpeg ($file);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hSource = imagecreatefromwbmp ($file);
        elseif($ext == IMAGETYPE_GIF) $hSource = imagecreatefromgif ($file);

        // Load watermark image
        $ext = exif_imagetype($watermark);
        $hWatermark = NULL;

        if($ext == IMAGETYPE_PNG) $hWatermark = imagecreatefrompng ($watermark);
        elseif($ext == IMAGETYPE_JPEG) $hWatermark = imagecreatefromjpeg ($watermark);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hWatermark = imagecreatefromwbmp ($watermark);
        elseif($ext == IMAGETYPE_GIF) $hWatermark = imagecreatefromgif ($watermark);

        $hDest = imagecreatetruecolor($width, $height);
        imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);
        imagecopyresampled($hDest, $hWatermark, $width - $ww, $height - $wh, 0, 0, $ww, $wh, $sww, $swh);

        $filemain             = str_replace(UPLOAD_DIR, "", $filepath);
        $filemain_big_thumb   = str_replace(UPLOAD_DIR, "", $filepath_big_thumb);
        $filemain_thumb       = str_replace(UPLOAD_DIR, "", $filepath_thumb);
        $filemain_small_thumb = str_replace(UPLOAD_DIR, "", $filepath_small_thumb);

        imagejpeg($hDest,UPLOAD_DIR.$filemain);

        $watermarked_file                   = UPLOAD_DIR.$filemain;

        $watermarked_cloud_file             = UPLOAD_AMZ_WATER.$filemain;
        $watermarked_cloud_file_big_thumb   = UPLOAD_AMZ_WATER.$filemain_big_thumb;
        $watermarked_cloud_file_thumb       = UPLOAD_AMZ_WATER.$filemain_thumb;
        $watermarked_cloud_file_small_thumb = UPLOAD_AMZ_WATER.$filemain_small_thumb;

        $l_sNewAbsoluteFilePath = $watermarked_file;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file);


        // Big Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_big_thumb);


        // Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_thumb);


        // Watermarked Small Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_small_thumb);

        imagedestroy($hDest);

        $filemain = str_replace(UPLOAD_DIR, "", $filepath);
  
        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        @unlink($filepath);
        @unlink($filepath_thumb);
        @unlink($filepath_big_thumb);
        @unlink($filepath_small_thumb);

        return $filemain;
    }



    /**
     * Upload category cropped photo transparency
     *
     * @param string $name
     * @return mixed
     */
    private function _uploadCropedPhotoCat($name='image'){

       $url = $this->controller->chanel['url'];

        $year = date('Y');
        $month = date('m');
        $ratio = "@3x";

        $filename = md5(time().rand(0.1, 99.9)).$ratio;
   

        $fileinfo = $_FILES[$name];

        if (!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a photo file.";
            return false;
        }

        $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));

        if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
            return false;
        }

        $filepath             = UPLOAD_DIR."photo/".$year."/".$month;
        $filepath_big_thumb   = UPLOAD_DIR."big_thumb/photo/".$year."/".$month;
        $filepath_thumb       = UPLOAD_DIR."thumb/photo/".$year."/".$month;
        $filepath_small_thumb = UPLOAD_DIR."small_thumb/photo/".$year."/".$month;

        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_big_thumb   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_thumb       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_small_thumb = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".".$ext;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        $filepath             .= "/".$filename.".".$ext;
        $filepath_big_thumb .= "/".$filename.".".$ext;
        $filepath_thumb       .= "/".$filename.".".$ext;
        $filepath_small_thumb .= "/".$filename.".".$ext;

        //Creating Photo
        $source = $this->loadImage($fileinfo['tmp_name']);

        list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
        
     
        // Design header image coords
        if (isset($_REQUEST['image'])){
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }else { // All other image coords

            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }



        $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
        $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
        $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

        if($cw == 0 || $ch == 0) {
            $cw = $ow;
            $ch = $oh;
        }


        $dest = imagecreatetruecolor($cw, $ch);

        imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

       
             $dest = imagecreatetruecolor($cw, $ch);

             imagesavealpha($dest, true);
             imagealphablending($dest, false);
             $transparent = imagecolorallocatealpha($dest, 0, 0, 0, 127);
             imagefill($dest, 0, 0, $transparent);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

    
        imagedestroy($source);
        imagedestroy($dest);

        $file = $filepath;
        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);

        // Big Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_big_thumb);


        // Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);


        // Small Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_thumb);
        
        /* Watermarking of Images */

        list($sw, $sh) = getimagesize($filepath);

        // Calculate simulated size
        $width = $this->uri->segment(5);

        if (empty($width) || $width == NULL) { $width = 640; }

        $height = round($sh * $width / $sw);


        
        list($sww, $swh) = getimagesize($watermark);
        // Resized watermark size
        $ww = round($width / 3);
        $wh = round($swh * $ww / $sww);

        // Load original image
        $ext = exif_imagetype ($file);
        $hSource = NULL;

        if($ext == IMAGETYPE_PNG) $hSource = imagecreatefrompng ($file);
        elseif($ext == IMAGETYPE_JPEG) $hSource = imagecreatefromjpeg ($file);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hSource = imagecreatefromwbmp ($file);
        elseif($ext == IMAGETYPE_GIF) $hSource = imagecreatefromgif ($file);

        // Load watermark image
        $ext = exif_imagetype($watermark);
        $hWatermark = NULL;

        if($ext == IMAGETYPE_PNG) $hWatermark = imagecreatefrompng ($watermark);
        elseif($ext == IMAGETYPE_JPEG) $hWatermark = imagecreatefromjpeg ($watermark);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hWatermark = imagecreatefromwbmp ($watermark);
        elseif($ext == IMAGETYPE_GIF) $hWatermark = imagecreatefromgif ($watermark);

        $hDest = imagecreatetruecolor($width, $height);
        imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);
        imagecopyresampled($hDest, $hWatermark, $width - $ww, $height - $wh, 0, 0, $ww, $wh, $sww, $swh);

        $filemain             = str_replace(UPLOAD_DIR, "", $filepath);
        $filemain_big_thumb   = str_replace(UPLOAD_DIR, "", $filepath_big_thumb);
        $filemain_thumb       = str_replace(UPLOAD_DIR, "", $filepath_thumb);
        $filemain_small_thumb = str_replace(UPLOAD_DIR, "", $filepath_small_thumb);

        imagejpeg($hDest,UPLOAD_DIR.$filemain);

        $watermarked_file                   = UPLOAD_DIR.$filemain;

        $watermarked_cloud_file             = UPLOAD_AMZ_WATER.$filemain;
        $watermarked_cloud_file_big_thumb   = UPLOAD_AMZ_WATER.$filemain_big_thumb;
        $watermarked_cloud_file_thumb       = UPLOAD_AMZ_WATER.$filemain_thumb;
        $watermarked_cloud_file_small_thumb = UPLOAD_AMZ_WATER.$filemain_small_thumb;

        $l_sNewAbsoluteFilePath = $watermarked_file;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file);


        // Big Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_big_thumb);


        // Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_thumb);


        // Watermarked Small Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_small_thumb);

        imagedestroy($hDest);

        $filemain = str_replace(UPLOAD_DIR, "", $filepath);

  
        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        @unlink($filepath);
        @unlink($filepath_thumb);
        @unlink($filepath_big_thumb);
        @unlink($filepath_small_thumb);

        return $filemain;
    }
    private function _uploadSliderphotofeed($name='image_slider'){

        $url = $this->controller->chanel['url'];

        $year = date('Y');
        $month = date('m');
        $filename = md5(time().rand(0.1, 99.9));

        $fileinfo = $_FILES[$name];

        if (!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a photo file.";
            return false;
        }

        $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));

        if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
            return false;
        }

        $filepath             = UPLOAD_DIR."tmp/photo/".$year."/".$month;
        $filepath_big_thumb   = UPLOAD_DIR."tmp/big_thumb/photo/".$year."/".$month;
        $filepath_thumb       = UPLOAD_DIR."tmp/thumb/photo/".$year."/".$month;
        $filepath_small_thumb = UPLOAD_DIR."tmp/small_thumb/photo/".$year."/".$month;

        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_big_thumb   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_thumb       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".".$ext;
        $file_a3_path_small_thumb = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".".$ext;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir($filepath_big_thumb)){
            mkdir($filepath_big_thumb, 0777, true);
        }

        if(!is_dir($filepath_thumb)){
            mkdir($filepath_thumb, 0777, true);
        }

        if(!is_dir($filepath_small_thumb)){
            mkdir($filepath_small_thumb, 0777, true);
        }

        $filepath             .= "/".$filename.".".$ext;
        $filepath_big_thumb .= "/".$filename.".".$ext;
        $filepath_thumb       .= "/".$filename.".".$ext;
        $filepath_small_thumb .= "/".$filename.".".$ext;

        //Creating Photo
        $source = $this->loadImage($fileinfo['tmp_name']);

        list($ow, $oh) = getimagesize($fileinfo['tmp_name']);

        // Design header image coords
        if (isset($_REQUEST['image1'])){
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }

        // Design logo image coords
        if (isset($_REQUEST['image4'])){
            $rw = isset($_REQUEST['photo2_width']) ? $_REQUEST['photo2_width'] : $ow;
            $rh = isset($_REQUEST['photo2_height']) ? $_REQUEST['photo2_height'] : $oh;
            $x1 = isset($_REQUEST['photo2_crop_x1']) ? $_REQUEST['photo2_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo2_crop_y1']) ? $_REQUEST['photo2_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo2_crop_x2']) ? $_REQUEST['photo2_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo2_crop_y2']) ? $_REQUEST['photo2_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo2_crop_w']) ? $_REQUEST['photo2_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo2_crop_h']) ? $_REQUEST['photo2_crop_h'] : $rh;

        } else { // All other image coords

            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;
        }

        $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
        $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
        $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

        if($cw == 0 || $ch == 0) {
            $cw = $ow;
            $ch = $oh;
        }

        $dest = imagecreatetruecolor($cw, $ch);

        imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

        // Create png for header and logo design elements
        if (isset($_REQUEST['image1']) || isset($_REQUEST['image4'])){

            imagepng($dest, $filepath);

        } else { // Create jpg for all other images

            imagejpeg($dest, $filepath);

        }

        imagedestroy($source);
        imagedestroy($dest);

        $file = $filepath;
        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);

        // Big Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_big_thumb);


        // Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);


        // Small Thumb No Watermark
        $src = imagecreatefromjpeg($file);

        list($width,$height)=getimagesize($file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_small_thumb);
        
        /* Watermarking of Images */

        list($sw, $sh) = getimagesize($filepath);

        // Calculate simulated size
        $width = $this->uri->segment(5);

        if (empty($width) || $width == NULL) { $width = 640; }

        $height = round($sh * $width / $sw);

//        $cid = $this->getChanelOptions();
//
//        $designinfo = $this->getDesignInfo($cid[id]);
        $watermark = THEME_CLOUD."watermark.png";
        
        list($sww, $swh) = getimagesize($watermark);
        // Resized watermark size
        $ww = round($width / 3);
        $wh = round($swh * $ww / $sww);

        // Load original image
        $ext = exif_imagetype ($file);
        $hSource = NULL;

        if($ext == IMAGETYPE_PNG) $hSource = imagecreatefrompng ($file);
        elseif($ext == IMAGETYPE_JPEG) $hSource = imagecreatefromjpeg ($file);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hSource = imagecreatefromwbmp ($file);
        elseif($ext == IMAGETYPE_GIF) $hSource = imagecreatefromgif ($file);

        // Load watermark image
        $ext = exif_imagetype($watermark);
        $hWatermark = NULL;

        if($ext == IMAGETYPE_PNG) $hWatermark = imagecreatefrompng ($watermark);
        elseif($ext == IMAGETYPE_JPEG) $hWatermark = imagecreatefromjpeg ($watermark);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hWatermark = imagecreatefromwbmp ($watermark);
        elseif($ext == IMAGETYPE_GIF) $hWatermark = imagecreatefromgif ($watermark);

        $hDest = imagecreatetruecolor($width, $height);
        imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);
        imagecopyresampled($hDest, $hWatermark, $width - $ww, $height - $wh, 0, 0, $ww, $wh, $sww, $swh);

        $filemain             = str_replace(UPLOAD_DIR, "", $filepath);
        $filemain_big_thumb   = str_replace(UPLOAD_DIR, "", $filepath_big_thumb);
        $filemain_thumb       = str_replace(UPLOAD_DIR, "", $filepath_thumb);
        $filemain_small_thumb = str_replace(UPLOAD_DIR, "", $filepath_small_thumb);

        imagejpeg($hDest,UPLOAD_DIR.$filemain);

        $watermarked_file                   = UPLOAD_DIR.$filemain;

        $watermarked_cloud_file             = UPLOAD_AMZ_WATER.$filemain;
        $watermarked_cloud_file_big_thumb   = UPLOAD_AMZ_WATER.$filemain_big_thumb;
        $watermarked_cloud_file_thumb       = UPLOAD_AMZ_WATER.$filemain_thumb;
        $watermarked_cloud_file_small_thumb = UPLOAD_AMZ_WATER.$filemain_small_thumb;

        $l_sNewAbsoluteFilePath = $watermarked_file;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file);


        // Big Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=640;
        $newheight=($height/$width)*640;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_big_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_big_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_big_thumb);


        // Watermarked Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=320;
        $newheight=($height/$width)*320;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_thumb);


        // Watermarked Small Thumb
        $src = imagecreatefromjpeg($watermarked_file);

        list($width,$height)=getimagesize($watermarked_file);

        $newwidth=180;
        $newheight=($height/$width)*180;
        $tmp=imagecreatetruecolor($newwidth,$newheight);

        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

        imagejpeg($tmp,$filepath_small_thumb,100);

        imagedestroy($src);
        imagedestroy($tmp);

        $l_sNewAbsoluteFilePath = $filepath_small_thumb;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_small_thumb);

        imagedestroy($hDest);

        $filemain = str_replace(UPLOAD_DIR."tmp/", "", $filepath);

        @unlink($filepath);
        @unlink($filepath_thumb);
        @unlink($filepath_small_thumb);
        @unlink($filepath_big_thumb);

        $l_sMemcacheKey = Memcaching::makeKey($url);

        Memcaching::getInstance()->removeKey($l_sMemcacheKey);

      
      return str_replace(UPLOAD_DIR."tmp/" ,"", $filepath);
       return $filemain;

    }
    /**
     * video
     */
    function getFeedVideo($id){
        $sql = "
            select *
            from enr_video
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    /**
     * Get YouTube thumbnail url.
     *
     * @param mixed $video_url YouTube URL (ie. https://www.youtube.com/watch?v=video_id)
     * @return thumbnail's url
     */
    function getYouTubeThumbnail($video_url) {
        parse_str( parse_url( $video_url, PHP_URL_QUERY ), $end );
        $thumbnail = "http://img.youtube.com/vi/".$end['v']."/0.jpg";

        return $thumbnail;
    }
 

 // function getVevoThumbnail($video_url) {
 //        $parsed_url = parse_url( $video_url, PHP_URL_PATH);
 //        $parsed_url_arr = explode('/', $parsed_url);
 //        $count = count($parsed_url_arr);
 //        $data = file_get_contents("http://www.vevo.com/watch/".$parsed_url_arr[$count - 1]);
        
 //        $data = json_decode($data);
     
 //        return $data->thumbnail_loc;
 //    }
 

 
    /**
     * Get Instagram thumbnail url
     *
     * @param mixed $id Instagram URL (ie. )
     * @return thumbnail's url
     */

      function getInstagramThumbnail($video_url) {
        $parsed_url = parse_url( $video_url, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);
        $data = file_get_contents("https://api.instagram.com/oembed?url=http://instagr.am/p/".$parsed_url_arr[$count - 1]);
        
        $data = json_decode($data);
     
        return $data->thumbnail_url;

        
    }


    /**
     * Get Vimeo thumbnail url
     *
     * @param mixed $id Vimeo URL (ie. )
     * @return thumbnail's url
     */
    function getVimeoThumbnail($video_url) {

        $parsed_url = parse_url( $video_url, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);

        $data = file_get_contents("http://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/".$parsed_url_arr[$count - 1]);
        $data = json_decode($data);


        return $data->thumbnail_url;
    }

     /**
     * Get Vine thumbnail url
     *
     * @param mixed $id Vine URL (ie. )
     * @return thumbnail's url
     */

     function getVineThumbnail($video_url) {
        $parsed_url = parse_url( $video_url, PHP_URL_PATH);
        $parsed_url_arr = explode('/', $parsed_url);
        $count = count($parsed_url_arr);

        $data = file_get_contents("https://vine.co/oembed.json?url=http://vine.co/v/".$parsed_url_arr[$count - 1]);
        $data = json_decode($data);

        return $data->thumbnail_url;

    
     }

    /**
     * Grab thumbnail. Generate thumbnails and watermarks.
     *
     * @param $url
     * @return mixed
     */
    function grabThumbnail($thumbnail){

        $ch = curl_init($thumbnail);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $rawdata = curl_exec($ch);
        curl_close($ch);

        $year = date('Y');
        $month = date('m');

        $filename = md5(time().rand(0.1, 99.9));

        $filepath = UPLOAD_DIR."photo/".$year."/".$month."/";
        $file_a3_path = UPLOAD_AMZ."photo/".$year."/".$month."/";

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        if(!is_dir(UPLOAD_DIR."big_thumb/photo/".$year."/".$month."/")){
            mkdir(UPLOAD_DIR."big_thumb/photo/".$year."/".$month."/", 0777, true);
        }

        if(!is_dir(UPLOAD_DIR."thumb/photo/".$year."/".$month."/")){
            mkdir(UPLOAD_DIR."thumb/photo/".$year."/".$month."/", 0777, true);
        }

        if(!is_dir(UPLOAD_DIR."small_thumb/photo/".$year."/".$month."/")){
            mkdir(UPLOAD_DIR."small_thumb/photo/".$year."/".$month."/", 0777, true);
        }

        if(!is_dir(UPLOAD_DIR."small_square_thumb/photo/".$year."/".$month."/")){
            mkdir(UPLOAD_DIR."small_square_thumb/photo/".$year."/".$month."/", 0777, true);
        }

        $filepath .= $filename.".jpg";
        $file_a3_path .= $filename.".jpg";

        $fp = fopen($filepath, 'w');
        fwrite($fp, $rawdata);
        fclose($fp);

        // Get top-level directory
        $path = str_replace(UPLOAD_DIR, "", $filepath);

        $filepath_big_thumb   = UPLOAD_DIR."big_thumb/".$path;
        $filepath_thumb       = UPLOAD_DIR."thumb/".$path;
        $filepath_small_thumb = UPLOAD_DIR."small_thumb/".$path;
        $filepath_small_square_thumb = UPLOAD_DIR."small_square_thumb/".$path;

        $file_a3_big_thumb_path   = UPLOAD_AMZ."big_thumb/".$path;
        $file_a3_thumb_path       = UPLOAD_AMZ."thumb/".$path;
        $file_a3_small_thumb_path = UPLOAD_AMZ."small_thumb/".$path;
        $file_a3_small_square_thumb_path = UPLOAD_AMZ."small_square_thumb/".$path;

        $file_a3_water_path             = UPLOAD_AMZ_WATER.$path;
        $file_a3_water_big_thumb_path   = UPLOAD_AMZ_WATER."big_thumb/".$path;
        $file_a3_water_thumb_path       = UPLOAD_AMZ_WATER."thumb/".$path;
        $file_a3_water_small_thumb_path = UPLOAD_AMZ_WATER."small_thumb/".$path;
        $file_a3_water_small_square_thumb_path = UPLOAD_AMZ_WATER."small_square_thumb/".$path;

        // Check for original file to resize and watermark images
        if (file_exists($filepath)){

            // Create and push to S3:

            $thumb_array =
                array(
                    'original' => // Original
                        array(
                            "filepath" => $filepath,
                            "new_width" => NULL,
                            "s3_path" => $file_a3_path,
                            "s3_watermark_path" => $file_a3_water_path
                        ),
                    'big_thumb' => // Big Thumbnail (640)
                        array(
                            "filepath" => $filepath_big_thumb,
                            "new_width" => 640,
                            "s3_path" => $file_a3_big_thumb_path,
                            "s3_watermark_path" => $file_a3_water_big_thumb_path
                        ),
                    'thumb' => // Medium Thumbnail (320)
                        array(
                            "filepath" => $filepath_thumb,
                            "new_width" => 320,
                            "s3_path" => $file_a3_thumb_path,
                            "s3_watermark_path" => $file_a3_water_thumb_path
                        ),
                    'small_thumb' => // Small Thumbnail (180)
                        array(
                            "filepath" => $filepath_small_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_thumb_path
                        )
                );

            $square_thumb_array =
                array(
                    'small_square_thumb' => // Small Square Thumbnail (180)
                        array(
                            "filepath" => $filepath_small_square_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_square_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_square_thumb_path
                        )
                );

            $l_sNew_Path           = array();
            $l_sNew_Watermark_Path = array();

            // Create and push to S3:
            //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
            //      Small Thumbnail (180)

            //      Their watermark versions

            foreach ($thumb_array as $thumb){
                $new_file           = $this->resize_image_push_to_S3($thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;

            }

            //      Square Small Thumbnail (180) and watermark version

            foreach ($square_thumb_array as $square_thumb){
                $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }
        }

        return str_replace(UPLOAD_DIR, "", $filepath);

    }

    /**
     * Get thumbnails and watermarked thumbnails from video.
     *
     * @param $path
     * @return mixed
     */
    function getThumbnailFromVideo($path, $video_width, $video_height){

        $year = date('Y');
        $month = date('m');
        $filename = md5(time().rand(0.1, 99.9));

        // Arrays to build file paths for:
        $thumb_file_paths_to_build =
            array(
                'original' => // Original
                    array (
                        "media_type" => "photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'big' => // Big Thumbnail (640)
                    array (
                        "media_type" => "big_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array (
                        "media_type" => "thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'small' => // Small Thumbnail (180)
                    array (
                        "media_type" => "small_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        $square_thumb_file_paths_to_build =
            array(
                'small_square_thumb' => // Small Square Thumbnail (180)
                    array (
                        "media_type" => "small_square_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        // General dir
        $thumb_filepath_array        = array();
        $square_thumb_filepath_array = array();

        foreach ($thumb_file_paths_to_build as $thumb_file_path_to_build){
            $thumb_filepath_array[] = $this->build_file_path($thumb_file_path_to_build);
        }

        foreach ($square_thumb_file_paths_to_build as $square_thumb_file_path_to_build){
            $square_thumb_filepath_array[] = $this->build_file_path($square_thumb_file_path_to_build);
        }

        // Top-level directories
        $photo_filepath       = $thumb_filepath_array[0];
        $big_thumb_filepath   = $thumb_filepath_array[1];
        $thumb_filepath       = $thumb_filepath_array[2];
        $small_thumb_filepath = $thumb_filepath_array[3];

        $small_square_filepath = $square_thumb_filepath_array[0];

        // Local directories
        $filepath             = UPLOAD_DIR.$photo_filepath.$filename.".jpg";
        $filepath_big_thumb   = UPLOAD_DIR.$big_thumb_filepath.$filename.".jpg";
        $filepath_thumb       = UPLOAD_DIR.$thumb_filepath.$filename.".jpg";
        $filepath_small_thumb = UPLOAD_DIR.$small_thumb_filepath.$filename.".jpg";

        $filepath_small_square_thumb = UPLOAD_DIR.$small_square_filepath.$filename.".jpg";

        // Amazon S3 directories
        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_big_thumb_path   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_thumb_path       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_thumb_path = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_square_thumb_path = UPLOAD_AMZ."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Amazon S3 watermaked directories
        $file_a3_water_path             = UPLOAD_AMZ_WATER."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_big_thumb_path   = UPLOAD_AMZ_WATER."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_thumb_path       = UPLOAD_AMZ_WATER."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_thumb_path = UPLOAD_AMZ_WATER."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_square_thumb_path = UPLOAD_AMZ_WATER."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        /*
      move_uploaded_file($fileinfo['tmp_name'], $filepath_thumb);
//        $cmd = "avconv -i {$path} -ss 1 -s 120x90 -f image2  -frames:v 100 {$filepath}";
      $cmd = shell_exec("ffmpeg -i {$path} -ss 1 -s 501x360 -f image2  -vframes 1 {$filepath_thumb}");
      //$cmd = shell_exec("C:\wamp/bin/rtvidthumb.sh {$path} {$filepath_thumb}");
      $return = `$cmd`;

      move_uploaded_file($fileinfo['tmp_name'], $filepath);
      //$cmd = "avconv -i {$path} -ss 1 -s 120x90 -f image2  -frames:v 100 {$filepath}";
      //$cmd = shell_exec("ffmpeg -i {$path} -ss 1 -s 501x360 -f image2  -vframes 1 {$filepath}");
      $cmd = shell_exec("C:\wamp/bin/rtvidthumb.sh {$path} {$filepath}");
      //$return = `$cmd`;
      */

        ///move_uploaded_file($fileinfo['tmp_name'], $filepath_thumb);
//     //   $cmd = "avconv -i {$path} -ss 1 -s 120x90 -f image2  -frames:v 100 {$filepath}";
        $cmd = "/usr/bin/ffmpeg -y -i {$path} -ss 1 -s 640x360 -f image2  -vframes 1 {$filepath}";
//        $cmd = "/usr/bin/ffmpeg -y -i {$path} -ss 1 -s {$video_width}x{$video_height} -f image2  -vframes 1 {$filepath}";
        // $cmd = "/var/www/html/starsite/cms/assets/scripts/rtvidthumb.sh {$path} {$filepath_thumb}";
        $return = `$cmd`;

        // Check for original file to resize and watermark images
        if (file_exists($filepath)){

            // Create and push to S3:
            $thumb_array =
                array(
                    'original' => // Original
                        array(
                            "filepath" => $filepath,
                            "new_width" => NULL,
                            "s3_path" => $file_a3_path,
                            "s3_watermark_path" => $file_a3_water_path
                        ),
                    'big_thumb' => // Big Thumbnail (640)
                        array(
                            "filepath" => $filepath_big_thumb,
                            "new_width" => 640,
                            "s3_path" => $file_a3_big_thumb_path,
                            "s3_watermark_path" => $file_a3_water_big_thumb_path
                        ),
                    'thumb' => // Medium Thumbnail (320)
                        array(
                            "filepath" => $filepath_thumb,
                            "new_width" => 320,
                            "s3_path" => $file_a3_thumb_path,
                            "s3_watermark_path" => $file_a3_water_thumb_path
                        ),
                    'small_thumb' => // Small Thumbnail (180)
                        array(
                            "filepath" => $filepath_small_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_thumb_path
                        )

                );

            // Small Square Thumbnail (180)
            $square_thumb_array =
                array(
                    'small_square_thumb' =>
                        array(
                            "filepath" => $filepath_small_square_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_square_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_square_thumb_path
                        )
                );


            $l_sNew_Path           = array();
            $l_sNew_Watermark_Path = array();

            // Create and push to S3:
            //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
            //      Small Thumbnail (180)

            //      Their watermark versions
    
            foreach ($thumb_array as $thumb){
                $new_file           = $this->resize_image_push_to_S3($thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;

            }

            //      Square Small Thumbnail (180) and watermark version

            foreach ($square_thumb_array as $square_thumb){
                $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }

        }

        return str_replace(UPLOAD_DIR, '', $filepath );
    }

    function addFeedVideoFromApi($feed){
        $video_path = $this->_uploadVideo();

        // - convert iOS to Android video format (/usr/bin/cnvt.sh)
        $filepath = UPLOAD_DIR.$video_path;
        $file_a3_path = UPLOAD_AMZ.$video_path;

        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);
        $l_nSuccessId = 1;
        $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$l_sNewAbsoluteFilePath.' '.$l_nSuccessId;
        exec($l_sCommand . " > /dev/null &");


        if($feed['fan_id'] && $feed['fan_id'] != -1) {
            $convertpath = UPLOAD_DIR.$video_path;

            shell_exec ( "/usr/bin/cnvt.sh {$convertpath}");
        }

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if($image_path = $this->grabThumbnail($feed['video_url'])){
        }else{
            $image_path = $this->getThumbnailFromVideo(UPLOAD_DIR.$video_path);
        }
        if($video_path){
            $post = array(
                'cid' => $feed['cid'],
                'fan_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
                'description' => $feed['description'],
                'tags' => $feed['tags'],
//                'credit' => $feed['credit'],
                'video_url' => $feed['video_url'] ? $feed['video_url'] : '',
                'image_path' => $image_path,
                'slider_thumb'=>$feed['slider_thumb'],
                'destination' => $video_path,
                'video_size' => $_FILES['video']['size'] ? $_FILES['video']['size'] : 0,
                'video_mime' => $_FILES['video']['type'] ? $_FILES['video']['type'] : '',
                'time_stamp' => time(),
                "start_date" => $feed['start_date'],
          
                "end_date" => $feed['end_date'],
                'is_publish' => $feed['is_publish']
            );
            $res = $this->db->insert("enr_video", $post);
            $id = $this->db->insert_id();

            $this->updateLastPost($post['fan_id']);

            $url = $this->controller->chanel['url'];
            $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);


            $this->updateDeepLink('video', $id);

            //Added 10-28 insert for enr_feed   
            if($feed['fan_id'] == -1) {

                $this->insertFeed('video', $id, $post['fan_id'], $feed['is_publish']);
            }

            return $id;
        }
        return false;
    }
    function create_brightcove_media_id($video_id) {



        $query = 'select * from enr_video where id = ?';

        $result = $this->db->query($query, array($video_id))->result_array();

        if(count($result) > 0) {
            // Set the data for the new video DTO using the form values
            $chanel = $this->get_chanel($result[0]['cid']);
            $tag_url[] = $chanel['url'];
            $metaData = array(
                'name' => $result[0]['description'],
                'shortDescription' => $result[0]['description'],
                'tags' => $tag_url
            );

            // Rename the file to its original file name (instead of temp names like "a445ertd3")
            $file = UPLOAD_DIR . '/' . $result[0]['destination'];
            // Send the file to Brightcove

            $response = $this->controller->brightcove->createVideo($file, $metaData);
            $json = json_decode($response);

            //add overlay logo
            $channel = $this->get_chanel($result[0]['cid']);
            if(!empty($channel['brightcove_overlay_logo'])) {
                $this->controller->brightcove->addOverlayLogo($json->result, $channel['brightcove_overlay_logo']);
            }

            $query = 'update enr_video set brightcove_media_id = ?, is_publish= 0 where id = ?';
            $this->db->query($query, array($json->result, $video_id));


            return $json->result;
        }

        return '';
    }

    /**
     * Add feed video.
     *
     * @param $feed
     * @return bool
     */
    function addFeedVideo($feed){
        $l_bCLOSE_BUFFER = 1;
        $user_id = $this->ion_auth->get_user_id();
        $user = $this->main_m->getUser($user_id); // Get User
        $video_path = $this->input->post('destination');

        if($feed['slider_thumb'] !=""){
            if(!$slider_thumb =$this->_uploadSlider_photo($feed['slider_thumb'])) {return false;}
        }
              
        if($feed['custom_image']!=""){
            if (!$image_path = $this->_uploadPhoto_video($feed['custom_image'])) { return false; }

        }else{
            $image_path = $this->getThumbnailFromVideo(UPLOAD_DIR.$video_path, $feed['video_width'], $feed['video_height']);
        }

        list($width,$height)=getimagesize(UPLOAD_DIR.$image_path);
        $aspect_ratio = $height / $width;


        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }

     

        $post = array(
            'cid' => $feed['cid'],
            'fan_id' => -1,
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'video_url' => $feed['video_url'],
            'image_path' => $image_path,
            'image_mime' => $_FILES['image']['type'],
            'destination' => $feed['destination'],
            'slider_thumb' => $slider_thumb,
            'video_mime' => $feed['video_mime'],
            'video_size' => $feed['video_size'],
            'video_width' => $feed['video_width'],
            'video_height' => $feed['video_height'],
            'video_duration' => $feed['video_duration'],
            'edit_duration' => $feed['video_duration'],
            'time_stamp' => time(),
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            'feed_date' => $feed['feed_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0,
            'is_oflo' => $feed['is_oflo'],
            'is_oflo_slider' => $feed['is_oflo_slider'],
            'image_aspect_ratio' => $aspect_ratio,
            'is_premium' => $feed['is_premium'],
             'is_feed' => $feed['is_feed'],
             'product_widget' => $feed['product_widget'],
        );
     
          



        $res = $this->db->insert("enr_video", $post);

    

        $id = $this->db->insert_id();

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        $viral_media_category = $this->data['viral_media_category'];

        $i=0;
        while($viral_media_category[$i]){
            
            $cat_id = $viral_media_category[$i]['id'];

                if($this->input->post($cat_id)){
                  $insert = "INSERT IGNORE INTO viral_media_cat_post (cat_id, post_id, content_type_id)
                            VALUES($cat_id, $id, ".CONTENT_TYPE_ID_VIDEO.")";

                    $this->db->query($insert);

            }
            $i++;
        }

       if($this->input->post('priorityCheckBox')){

            $prioritySelected = $this->input->post('prioritySelected');
            $priorityDaySelected = $this->input->post('priorityDaySelected');
            $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

             $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                        VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')";

              $this->db->query($insert);
      }
          


    

        /* if($l_bCLOSE_BUFFER){
            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

             window.location.href='/content/postfeed/';
             </SCRIPT>");
         }     */

        if($feed['destination']){

            $this->updateDeepLink('video', $id);
            $this->insertFeed('video', $id, $post['fan_id'], $feed['is_publish']);
            $this->updateLastPost($post['fan_id']);

            $url = $this->controller->chanel['url'];

            $l_nChannelId = $feed['cid'];
            $l_sDescription = $feed['description'];

            $filepath = $video_path;

            $video_file = UPLOAD_DIR.$video_path;
            $video_cloud_file = UPLOAD_AMZ.$filepath;


            if($video_path!=""){
                $l_sNewAbsoluteFilePath = $video_file;

                if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18 || $l_nChannelId == 24){
                    $SCAPI= new SCAPI();
                    $SCAPI->rq_sendToLegacyServer($l_nChannelId,'video',$l_sNewAbsoluteFilePath,$l_sDescription);
                }

                $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath, $video_cloud_file);
//                $l_nSuccessId = 1;
                $l_nSuccessId = $id;
                $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$video_cloud_file.' '.$l_nSuccessId.' '.$post['video_height'];

//                exec($l_sCommand . " > /dev/null &");

                exec($l_sCommand , $output, $return_var);
//
//                echo "<BR><BR>\$output: ";
//                var_dump($output);
//
//                echo "<BR><BR>\$return_var: ";
//                var_dump($return_var);

                if ($return_var !== 0){
                    error_log("\$l_sCommand: ".$l_sCommand);
                    error_log("\$output: ".print_r($output, TRUE));
                    error_log("\$return_var: ".$return_var);
                }

                /**
                 * Break videos' memcache
                */



                $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos'); // Latest Starsite video memcache
                $oflo_home_MemcacheKey = Memcaching::makeKey('oflo_home'.'oflo_homepage_content'); // oflo homepage videogrid
                $channel_l_sMemcacheKey = Memcaching::makeKey($url.'oflo-fp-videos'); // Latest videos
                $most_recent_oflo_video_l_sMemcacheKey =
                Memcaching::makeKey('MOST-RECENT-OFLO-'.$feed['cid'].'-'.CONTENT_TYPE_ID_VIDEO); // Latest oflo video memcache

                Memcaching::getInstance()->removeKey($l_sMemcacheKey);
                Memcaching::getInstance()->removeKey($oflo_home_MemcacheKey);
                Memcaching::getInstance()->removeKey($channel_l_sMemcacheKey);
                Memcaching::getInstance()->removeKey($most_recent_oflo_video_l_sMemcacheKey);

                $this->break_oflo_video_memcache($feed);

                if($feed['post_to_facebook']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 1,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();
                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);


                    foreach($query->result_array() as $row){

                        $data = array(
                            'page_id' => $row['page_id'],
                            'social_token' => $row['social_token'],
                            'link' => $row['deep_link_web_oflo_lp'],
                            'picture' => UPLOAD_CLOUD.'watermark/'.$row['content_file'],
                            'message' => $row['message'],
                            'main_id' => $row['main_id']
                        );

                        $this->share_to_facebook($data);
                    }

                }

                if($feed['post_to_twitter']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 2,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();

                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);
                    foreach($query->result_array() as $row){

                        $oath_token = $row['social_token'];
                        $oath_secret_token = $row['social_token_secret'];
                        $page_id = $row['page_id'];

//                        $twitter = new TwitterOAuth("292aZXJ21hJRD3TwaL4nhTSzg", 'E2TuUxUNCUpnBj59RzT08V0c30HrRT9owKociLyrxKQiHTJtR1', $oath_token, $oath_secret_token);
                        $twitter = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $oath_token, $oath_secret_token);

                        $twitter_response = $twitter->post('statuses/update', array('status' => $row['deep_link_web_oflo_lp'].' '.$row['message']));

                        $posted_id = $twitter_response->id;

                        $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                        $querynew = $this->db->query($sql);
                    }

                }

                if($feed['post_to_tumblr']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 4,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();

                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                                
                                FROM enr_content_post_to_social cs 
                                JOIN enr_channel_oauth co  
                                ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                                
                                JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                                ON (cs.content_id = a.id )
                                
                                JOIN enr_chanel c  
                                ON (cs.channel_id = c.id )

                                WHERE cs.has_sent = "N" 
                                AND co.active = "Y"
                                AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);
                    foreach($query->result_array() as $row){

                        $l_sEmbedCodeInfo = Utils::encryptItemId($row['channel_id'].','.(($l_bIsVideo) ? 'v' : 'p').','.$row['content_id']);
                        $l_sEmbedCodeInfo = 'http://'.$row['channel_url'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                        $oath_token = $row['social_token'];
                        $oath_secret_token = $row['social_token_secret'];
                        echo "\n\n";

                        $tum_url = preg_replace("/https?:\/\//","",$row['page_name']);
                        $tum_url = preg_replace("/\//","",$tum_url);

                        $l_bSuppressOAuth = true;
                        require_once(LIB_PATH.'tumblr/tumblroauth.php');

                        $tumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$oath_token,$oath_secret_token);
                        $post_url = 'http://api.tumblr.com/v2/blog/'.$tum_url.'/post';

                        $params = array();

                        $params['type'] = "video";
                        $params['caption'] = $row['message'];
                        $params['embed'] = '<iframe scrolling="no" frameborder="0" width="680" height="383" src="'.$l_sEmbedCodeInfo.'"></iframe>';


                        $tumblr_response = $tumblr->post($post_url,$params);


                        if (201 == $tumblr_response->meta->status) {
                            $posted_id = $tumblr_response->response->id;
                            $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                            $querynew = $this->db->query($sql);
                        } else {
                            echo "error";
                            print_r($tumblr_response);
                        }
                    }

                }


            }


                  //send automatic email when new content is posted by a channel after zencorder is finished
  
                 if($this->session->userdata('username')=="administrator"){
                  // do nothing

                   }else{
                   $subject = $this->chanel['name'];
                   $chanel_name = $this->chanel['name'];
                   $description = $this->input->post('description');
                   $date = $this->input->post('start_date');

                   $this->email->from('info@starclubltd.com', 'Oflo'); // email address to send from
                   $this->email->to('feedalert@starclubltd.com');  // email address to send to
                   $this->email->subject("Oflo - new video posted in\r\n".$subject. "\r\ntitled\r\n".$description); // subject
                   $this->email->message("A new video has been posted on the date ".$date.".\r\n" .$chanel_name." just posted a new video titled ".$description."\r\nA link to video can be found here: ".$chanel_name.".on.oflo.co/ocontent/video/".$id); //message

                   $this->email->send();
                 }
                   //email sent and processed

            @unlink($video_file);

            @unlink(UPLOAD_DIR.$image_path);
            @unlink(UPLOAD_DIR."big_thumb/".$image_path);
            @unlink(UPLOAD_DIR."thumb/".$image_path);
            @unlink(UPLOAD_DIR."small_thumb/".$image_path);
            @unlink(UPLOAD_DIR."small_square_thumb/".$image_path);

            @unlink(UPLOAD_DIR.$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."big_thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."small_thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."small_square_thumb/".$image_path."_watermark.jpeg");

            return $id;
        }
        return false;
    }

    /**
     * Add feed video embed.
     *
     * @param $feed
     * @return bool
     */
    function addFeedVideoEmbed($feed){
        $l_bCLOSE_BUFFER = 1;
        $user_id = $this->ion_auth->get_user_id();
        $user = $this->main_m->getUser($user_id); // Get User
//        $video_path = $this->input->post('destination');
//
//        if($feed['custom_image']!=""){
//            if (!$image_path = $this->_uploadPhoto_video($feed['custom_image'])) { return false; }
//
//        }else{
//            $image_path = $this->getThumbnailFromVideo(UPLOAD_DIR.$video_path);
//        }

        $video_url = $this->input->post('video_url');

        if($feed['custom_image']!=""){
            if (!$image_path = $this->_uploadPhoto_video($feed['custom_image'])) { return false; }

        } else {

            switch ($feed['video_type']) {
                case EMBED_VIDEO_TYPE_ID_YOUTUBE:
                    $thumbnail = $this->getYouTubeThumbnail($feed['video_url']);
                    break;
                case EMBED_VIDEO_TYPE_ID_VIMEO:
                    $thumbnail = $this->getVimeoThumbnail($feed['video_url']);
                    break;
                case EMBED_VIDEO_TYPE_ID_VEVO:
                    $thumbnail = $this->getVevoThumbnail($feed['video_url']);
                    break;
                case EMBED_VIDEO_TYPE_ID_INSTAGRAM:
                    $thumbnail = $this->getInstagramThumbnail($feed['video_url']);
                    break;
                case EMBED_VIDEO_TYPE_ID_VINE:
                    $thumbnail = $this->getVineThumbnail($feed['video_url']);
                    break;
            }

            $image_path = $this->grabThumbnail($thumbnail);
        }

        list($width,$height)=getimagesize(UPLOAD_DIR.$image_path);
        $aspect_ratio = $height / $width;

        $slider_thumb = $this->_uploadSliderphotofeed();

        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }

        $post = array(
            'cid' => $feed['cid'],
            'fan_id' => -1,
            'cid' => $feed['cid'],
            'fan_id' => -1,
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'slider_thumb' =>$slider_thumb,
            'video_url' => $feed['video_url'],
            'video_type' => $feed['video_type'],
            'image_path' => $image_path,
//                'image_mime' => $_FILES['image']['type'],

            'image_mime' => 'jpg',
            'destination' => $feed['destination'],
            'video_mime' => 0,
            'video_size' => 0,
            'time_stamp' => time(),
            'is_publish' => $feed['is_publish'],
            "start_date" => $feed['start_date'],
            'feed_date' => $feed['feed_date'],
            "end_date" => $feed['end_date'],
            'is_sticky' => $feed['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0,
            'is_oflo' => $feed['is_oflo'],
            'is_oflo_slider' => $feed['is_oflo_slider'],
            'image_aspect_ratio' => $aspect_ratio,
            'is_premium' => $feed['is_premium'],
            'is_feed' => $feed['is_feed'],
             'product_widget' => $feed['product_widget'],
        );

        $res = $this->db->insert("enr_video", $post);

        $id = $this->db->insert_id();

        $this->data['viral_media_category'] = $this->main_m->get_viral_media_category_all();
        $viral_media_category = $this->data['viral_media_category'];

        $i=0;
        while($viral_media_category[$i]){

            $cat_id = $viral_media_category[$i]['id'];

            if($this->input->post($cat_id)){
                $insert = "INSERT IGNORE INTO viral_media_cat_post (cat_id, post_id, content_type_id)
                            VALUES($cat_id, $id, ".CONTENT_TYPE_ID_VIDEO.")";

                $this->db->query($insert);


            }
            $i++;
        }
              if($this->input->post('priorityCheckBox')){
               
                    $prioritySelected = $this->input->post('prioritySelected');
                    $priorityDaySelected = $this->input->post('priorityDaySelected');
                    $priorityCheckBox =  ($this->input->post('priorityCheckBox') ? 'y' : 'n');

                     $insert = "INSERT IGNORE INTO priority (priority_number, content_id, duration, is_priority)
                            VALUES('$prioritySelected', '$id', '$priorityDaySelected','$priorityCheckBox')";

                
                 
                      $this->db->query($insert);
                  }



        /* if($l_bCLOSE_BUFFER){
            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>

             window.location.href='/content/postfeed/';
             </SCRIPT>");
         }     */

//        if($feed['destination']){
        if($feed['video_url']){

            $this->updateDeepLink('video', $id);
            $this->insertFeed('video', $id, $post['fan_id'], $feed['is_publish']);
            $this->updateLastPost($post['fan_id']);

            $url = $this->controller->chanel['url'];

            // Break Channel Flow Player videos memcache
            $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');
            Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $this->break_oflo_video_memcache($feed);

//            $filepath = $video_path;
//
//            $video_file = UPLOAD_DIR.$video_path;
//            $video_cloud_file = UPLOAD_AMZ.$filepath;


//            if($video_path!=""){
            if($video_url!=""){
//                $l_sNewAbsoluteFilePath = $video_file;
//
//                $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath, $video_cloud_file);
////                $l_nSuccessId = 1;
//                $l_nSuccessId = $id;
//                $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$video_cloud_file.' '.$l_nSuccessId;
//
//                exec($l_sCommand . " > /dev/null &");

                if($feed['post_to_facebook']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 1,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();
                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp

                            FROM enr_content_post_to_social cs
                            JOIN enr_channel_oauth co
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                            ON (cs.content_id = a.id )

                            JOIN enr_chanel c
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N"
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);


                    foreach($query->result_array() as $row){

                        $data = array(
                            'page_id' => $row['page_id'],
                            'social_token' => $row['social_token'],
                            'link' => $row['deep_link_web_oflo_lp'],
                            'picture' => UPLOAD_CLOUD.'watermark/'.$row['content_file'],
                            'message' => $row['message'],
                            'main_id' => $row['main_id']
                        );

                        $this->share_to_facebook($data);
                    }

                }

                if($feed['post_to_twitter']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 2,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();

                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp

                            FROM enr_content_post_to_social cs
                            JOIN enr_channel_oauth co
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                            ON (cs.content_id = a.id )

                            JOIN enr_chanel c
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N"
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);
                    foreach($query->result_array() as $row){

                        $oath_token = $row['social_token'];
                        $oath_secret_token = $row['social_token_secret'];
                        $page_id = $row['page_id'];

                        $twitter = new TwitterOAuth("292aZXJ21hJRD3TwaL4nhTSzg", 'E2TuUxUNCUpnBj59RzT08V0c30HrRT9owKociLyrxKQiHTJtR1', $oath_token, $oath_secret_token);

                        $twitter_response = $twitter->post('statuses/update', array('status' => $row['deep_link_web_oflo_lp'].' '.$row['message']));

                        $posted_id = $twitter_response->id;

                        $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                        $querynew = $this->db->query($sql);
                    }

                }

                if($feed['post_to_tumblr']=="true"){

                    $post = array(
                        'post_type_id' => 1,
                        'channel_id' => $feed['cid'],
                        'content_id' => $id,
                        'social_property_type_id' => 4,
                        'channel_oauth_id' => NULL,
                        'message' => $feed['description'],
                        'has_sent' => 'N',
                        'created' => date("Y-m-d H:i:s")
                    );

                    $res = $this->db->insert("enr_content_post_to_social", $post);
                    $postid = $this->db->insert_id();

                    $l_bIsVideo = true;

                    $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp

                                FROM enr_content_post_to_social cs
                                JOIN enr_channel_oauth co
                                ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )

                                JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a
                                ON (cs.content_id = a.id )

                                JOIN enr_chanel c
                                ON (cs.channel_id = c.id )

                                WHERE cs.has_sent = "N"
                                AND co.active = "Y"
                                AND cs.id = '.intval($postid);

                    $query = $this->db->query($sql);
                    foreach($query->result_array() as $row){

                        $l_sEmbedCodeInfo = Utils::encryptItemId($row['channel_id'].','.(($l_bIsVideo) ? 'v' : 'p').','.$row['content_id']);
                        $l_sEmbedCodeInfo = 'http://'.$row['channel_url'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                        $oath_token = $row['social_token'];
                        $oath_secret_token = $row['social_token_secret'];
                        echo "\n\n";

                        $tum_url = preg_replace("/https?:\/\//","",$row['page_name']);
                        $tum_url = preg_replace("/\//","",$tum_url);

                        $l_bSuppressOAuth = true;
                        require_once(LIB_PATH.'tumblr/tumblroauth.php');

                        $tumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$oath_token,$oath_secret_token);
                        $post_url = 'http://api.tumblr.com/v2/blog/'.$tum_url.'/post';

                        $params = array();

                        $params['type'] = "video";
                        $params['caption'] = $row['message'];
                        $params['embed'] = '<iframe scrolling="no" frameborder="0" width="680" height="383" src="'.$l_sEmbedCodeInfo.'"></iframe>';


                        $tumblr_response = $tumblr->post($post_url,$params);


                        if (201 == $tumblr_response->meta->status) {
                            $posted_id = $tumblr_response->response->id;
                            $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                            $querynew = $this->db->query($sql);
                        } else {
                            echo "error";
                            print_r($tumblr_response);
                        }
                    }

                }


            }

                  //send automatic email when new content is posted by a channel after zencorder is finished
  
                 if($this->session->userdata('username')=="administrator"){
                  // do nothing

                   }else{
                   $subject = $this->chanel['name'];
                   $chanel_name = $this->chanel['name'];
                   $description = $this->input->post('description');
                   $date = $this->input->post('start_date');

                   $this->email->from('info@starclubltd.com', 'Oflo'); // email address to send from
                   $this->email->to('feedalert@starclubltd.com');  // email address to send to
                   $this->email->subject("Oflo - new video posted in \r\n".$subject. "\r\ntitled\r\n".$description); // subject
                   $this->email->message("A new video has been posted on the date ".$date.".\r\n" .$chanel_name." just posted a new video titled ".$description."\r\nA link to video can be found here: ".$chanel_name.".on.oflo.co/ocontent/video/".$id); //message

                   $this->email->send();
                 }
                   //email sent and processed


            @unlink(UPLOAD_DIR.$image_path);
            @unlink(UPLOAD_DIR."big_thumb/".$image_path);
            @unlink(UPLOAD_DIR."thumb/".$image_path);
            @unlink(UPLOAD_DIR."small_thumb/".$image_path);
            @unlink(UPLOAD_DIR."small_square_thumb/".$image_path);

            @unlink(UPLOAD_DIR.$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."big_thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."small_thumb/".$image_path."_watermark.jpeg");
            @unlink(UPLOAD_DIR."small_square_thumb/".$image_path."_watermark.jpeg");

            return $id;
        }
        return false;
    }

    function share_to_facebook($data){

        $fb = new Facebook\Facebook(
            array(
                'app_id'  => FACEBOOK_APP_ID,
                'app_secret' => FACEBOOK_APP_SECRET,
                'default_graph_version' => 'v2.6',
            )
        );

        $page_id = $data['page_id'];

        // setting default access token to be used in script
        $fb->setDefaultAccessToken($data['social_token']);

        $posting = array(
//                            'access_token'  => $row['social_token'],
            'link'    => $data['link'],
            'picture' => $data['picture'],
            'message' => $data['message'],
        );

        $posted = $fb->post("/$page_id/feed", $posting);

        $posted_arr = $posted->getGraphNode()->asArray();
        $posted_id = $posted_arr['id'];


        $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$data['posted_id'].'" WHERE id = '.$data['main_id'];
        $querynew = $this->db->query($sql);
    }

    /**
     * Check if Viral Video XML exists.
     *
     * @param $feed
     * @return bool
     */
    function viral_video_XML_exists($id)
    {
        $strSql = "SELECT * FROM viral_media_xml where id = " . $id;
        $query = $this->db->query($strSql);
        $rows = $query->result();

        if (count($rows) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add selected Viral Video XML to Viral Channel.
     *
     * @param $feed
     * @return bool
     */
    function addViralVideoXML($feed){

        // Video
        $video_path = file_get_contents($feed['content_url']);
        $video_parsed_headers = $this->parseHeaders($http_response_header);
        $video_content_type = $video_parsed_headers['Content-Type'];

        $video_extension = $this->video_content_type( $video_content_type );

        $filename = md5(time().rand(0.1, 99.9));
        $video_filename = $filename.$video_extension;

        // Original Thumbnail
        $thumbnail_path = file_get_contents($feed['thumbnail_url']);
        $parsed_headers = $this->parseHeaders($http_response_header);
        $l_sContentType = $parsed_headers['Content-Type'];

        $image_extension = $this->image_content_type( $l_sContentType );

        $thumbnail_filename = $filename.$image_extension;
        $thumbnail_extension = str_replace(".", "", $image_extension);

        // Video
        if($feed['content_url']){

//            $url = $this->controller->chanel['url'];
//            $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');
//
//            Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $year = date('Y');
            $month = date('m');

            $video_file_path_to_build =
                array (
                    "media_type" => "video",
                    "year" => $year,
                    "month" => $month
                );

            $video_filepath = $this->build_file_path($video_file_path_to_build);
            $video_fpath = $video_filepath;
            $video_filepath = UPLOAD_DIR.$video_filepath;

            if (!file_put_contents($video_filepath.$video_filename, $video_path)){
                error_log("Could not put video file to ".$video_filepath.$thumbnail_filename);
            } else {
                $video_file = UPLOAD_DIR.$video_fpath.$video_filename; // UPLOAD_DIR: /Applications/MAMP/htdocs/starsite/cms/assets/ufile/����2
                $video_cloud_file = "viral/assets/ufile/".$video_fpath.$video_filename; // UPLOAD_AMZ.$video_filepath; //

//                error_log("\$video_cloud_file");
//                error_log($video_cloud_file);

                if($video_path!=""){
                    $l_sNewAbsoluteFilePath = $video_file;
                    $l_sNewVideoPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath, $video_cloud_file);

                    // ffprobe
                    $cmd = "/usr/bin/ffprobe -v quiet -of flat -select_streams v:0 -show_entries stream=width,height,duration ".$video_file." | sed -n 's/streams.stream.0.//p'";
                    exec($cmd, $output, $cvh_return_var);

                    $output_implode = implode("&", $output);
                    $output_implode = str_replace('"', '', $output_implode);
                    parse_str($output_implode, $output_parsed);

                    $file['video_width']    = $output_parsed['width'];
                    $file['video_height']   = $output_parsed['height'];
                    $file['video_duration'] = $output_parsed['duration'];
                }
            }

//            @unlink($video_path);
//            @unlink($video_file);

        }

        // Create Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180), and Small Square Thumbnail (180 x 180)
        if($feed['thumbnail_url'] || (isset($_FILES['image']) && $_FILES['image']['size'])){

            // Arrays to build file paths

            $thumb_file_paths_to_build =
                array(
                    'original' => // Original
                        array (
                            "media_type" => "photo",
                            "year" => $year,
                            "month" => $month
                        ),
                    'big' => // Big Thumbnail (640)
                        array (
                            "media_type" => "big_thumb/photo",
                            "year" => $year,
                            "month" => $month
                        ),
                    'thumb' => // Medium Thumbnail (320)
                        array (
                            "media_type" => "thumb/photo",
                            "year" => $year,
                            "month" => $month
                        ),
                    'small' => // Small Thumbnail (180)
                        array (
                            "media_type" => "small_thumb/photo",
                            "year" => $year,
                            "month" => $month
                        )
                );

            $square_thumb_file_paths_to_build =
                array(
                    'small_square_thumb' => // Small Square Thumbnail (180)
                        array (
                            "media_type" => "small_square_thumb/photo",
                            "year" => $year,
                            "month" => $month
                        )
                );


            // General dir

            $thumb_filepath_array        = array();
            $square_thumb_filepath_array = array();

            foreach ($thumb_file_paths_to_build as $thumb_file_path_to_build){
                $thumb_filepath_array[] = $this->build_file_path($thumb_file_path_to_build);
            }

            foreach ($square_thumb_file_paths_to_build as $square_thumb_file_path_to_build){
                $square_thumb_filepath_array[] = $this->build_file_path($square_thumb_file_path_to_build);
            }

            $photo_filepath        = $thumb_filepath_array[0];
            $big_thumb_filepath    = $thumb_filepath_array[1];
            $thumb_filepath        = $thumb_filepath_array[2];
            $small_thumb_filepath  = $thumb_filepath_array[3];

            $small_square_filepath = $square_thumb_filepath_array[0];

            // Local dir
            $thumbnail_filepath              = UPLOAD_DIR.$photo_filepath;
            $big_thumbnail_filepath          = UPLOAD_DIR.$big_thumb_filepath;
            $medium_thumbnail_filepath       = UPLOAD_DIR.$thumb_filepath;
            $small_thumbnail_filepath        = UPLOAD_DIR.$small_thumb_filepath;

            $small_square_thumbnail_filepath = UPLOAD_DIR.$small_square_filepath;

            if (isset($_FILES['image']) && $_FILES['image']['size']){
                move_uploaded_file($_FILES['image']['tmp_name'], $thumbnail_filepath.$thumbnail_filename);
            } else {
                file_put_contents($thumbnail_filepath.$thumbnail_filename, $thumbnail_path);
            }

            if (!file_exists($thumbnail_filepath.$thumbnail_filename)){
                error_log("Could not put thumbnail file to ".$thumbnail_filepath.$thumbnail_filename);
            } else {

                // Push images to S3
                if($thumbnail_path!=""){

                    // Create and push to S3:
                    $thumb_array =
                        array(
                            'original' => // Original
                                array(
                                    "filepath" => $thumbnail_filepath.$filename.$image_extension,
                                    "new_width" => NULL,
                                    "s3_path" => "viral/assets/ufile/".$photo_filepath.$thumbnail_filename,
                                    "s3_watermark_path" => "viral/assets/ufile/watermark/".$photo_filepath.$thumbnail_filename
                                ),
                            'big_thumb' => // Big Thumbnail (640)
                                array(
                                    "filepath" => $big_thumbnail_filepath.$filename.$image_extension,
                                    "new_width" => 640,
                                    "s3_path" => "viral/assets/ufile/big_thumb/".$photo_filepath.$thumbnail_filename,
                                    "s3_watermark_path" => "viral/assets/ufile/watermark/big_thumb/".$photo_filepath.$thumbnail_filename

                                ),
                            'thumb' => // Medium Thumbnail (320)
                                array(
                                    "filepath" => $medium_thumbnail_filepath.$filename.$image_extension,
                                    "new_width" => 320,
                                    "s3_path" => "viral/assets/ufile/thumb/".$photo_filepath.$thumbnail_filename,
                                    "s3_watermark_path" => "viral/assets/ufile/watermark/thumb/".$photo_filepath.$thumbnail_filename

                                ),
                            'small_thumb' => // Small Thumbnail (180)
                                array(
                                    "filepath" => $small_thumbnail_filepath.$filename.$image_extension,
                                    "new_width" => 180,
                                    "s3_path" => "viral/assets/ufile/small_thumb/".$photo_filepath.$thumbnail_filename,
                                    "s3_watermark_path" => "viral/assets/ufile/watermark/small_thumb/".$photo_filepath.$thumbnail_filename
                                ),

                        );

//                    var_dump($thumb_array);
//                    exit;

                    // Small Square Thumbnail (180)
                    $square_thumb_array =
                        array(
                            'small_square_thumb' =>
                                array(
                                    "filepath" => $small_square_thumbnail_filepath.$filename.$image_extension,
                                    "new_width" => 180,
                                    "s3_path" => "viral/assets/ufile/small_square_thumb/".$photo_filepath.$thumbnail_filename,
                                    "s3_watermark_path" => "viral/assets/ufile/watermark/small_square_thumb/".$photo_filepath.$thumbnail_filename
                                )
                        );

                    $l_sNew_Path           = array();
                    $l_sNew_Watermark_Path = array();

                    // Create and push to S3:
                    //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
                    //      Small Thumbnail (180)

                    //      Their watermark versions

                    foreach ($thumb_array as $thumb){
                        $new_file           = $this->resize_image_push_to_S3($thumb, $thumbnail_filepath.$filename.$image_extension);
                        $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                        $l_sNew_Path[]           = $new_file;
                        $l_sNew_Watermark_Path[] = $new_watermark_file;
                    }

                    //      Square Small Thumbnail (180) and watermark version

                    foreach ($square_thumb_array as $square_thumb){
                        $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $thumbnail_filepath.$filename.$image_extension);
                        $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                        $l_sNew_Path[]           = $new_file;
                        $l_sNew_Watermark_Path[] = $new_watermark_file;
                    }
                }
            }

//            @unlink($thumbnail_path);

        }

        list($width,$height)=getimagesize($thumbnail_filepath.$thumbnail_filename);
        $aspect_ratio = $height/ $width;

        $post = array(
            'cid' => 64,
            'fan_id' => -1,
            'description' => !empty($feed['description']) ? $feed['description'] : "NULL",
            'video_url' => "NULL",
            'destination' => "video/".$year."/".$month."/".$video_filename,
            'video_mime' => "video",
            'video_size' => $feed['asset_length'],
            'video_width' => $output_parsed['width'],
            'video_height' => $output_parsed['height'],
            'video_duration' => $output_parsed['duration'],
            'tags' => !empty($feed['tags']) ? substr($feed['tags'], 0, 32) : "NULL",
            'credit' => "NULL",

            'image_mime' => $thumbnail_extension,
            'image_path'  => $photo_filepath.$thumbnail_filename,
            'time_stamp' => time(),
            "start_date" => date('Y-n-j H:i:s'),
            "end_date" => date('Y-n-j H:i:s', time() + 3600 * 24 * 30),
            'is_publish' => 1,
            'copy_origin_id' => isset($feed['id']) ? $feed['id'] : 0,
            'copy_timestamp' => time(),
            'deep_link_app' => "NULL",
            'deep_link_web' => "NULL",
            'deep_link_web_tw' => "NULL",
            'deep_link_web_lp' => "NULL",
            'brightcove_media_id' => "NULL",
            'image_aspect_ratio'  => $aspect_ratio
        );

        $res = $this->db->insert("enr_video", $post);

        $id = $this->db->insert_id();

        if($video_path!="") {

            $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$video_cloud_file.' '.$id.' '.$output_parsed['height'];

//            error_log("\$video_path != '");
//            error_log("\$l_sCommand: ");
//            error_log($l_sCommand);

            exec($l_sCommand . " > /dev/null &", $output, $return_var);

//            error_log("\$output: ");
//            error_log($output);
//            error_log("\$return_var: ");
//            error_log($return_var);
        }

        $rev = array(
            'viral_id' => $id,
            'viral_original_id' => $feed['id']
        );

        $test = $this->main_m->db->insert('viral_pair', $rev);

        $this->updateDeepLink('video', $id);

        @unlink($l_sNewAbsoluteFilePath); // Original Video

        @unlink($thumbnail_filepath.$filename.$image_extension); // Original Thumbnail
        @unlink($big_thumbnail_filepath.$filename.$image_extension); // Big Thumbnail
        @unlink($medium_thumbnail_filepath.$filename.$image_extension); // Medium Thumbnail
        @unlink($small_thumbnail_filepath.$filename.$image_extension); // Small Thumbnail
        @unlink($small_square_thumbnail_filepath.$filename.$image_extension); // Small Square Thumbnail

        @unlink($thumbnail_filepath.$filename.$image_extension."_watermark.jpeg"); // Original Watermark Thumbnail
        @unlink($big_thumbnail_filepath.$filename.$image_extension."_watermark.jpeg"); // Big Watermark Thumbnail
        @unlink($medium_thumbnail_filepath.$filename.$image_extension."_watermark.jpeg"); // Medium Watermark Thumbnail
        @unlink($small_thumbnail_filepath.$filename.$image_extension."_watermark.jpeg"); // Small Watermark Thumbnail
        @unlink($small_square_thumbnail_filepath.$filename.$image_extension."_watermark.jpeg"); // Small Square Watermark Thumbnail

        return $id;
    }
    


    /**
     * Parse $http_response_header from file_get_contents.
     *      Parser function to get formatted headers (with response code)
     *
     * @param $headers
     * @return array
     */
    
    function parseHeaders( $headers )
    {
        $head = array();
        foreach( $headers as $k=>$v )
        {
            $t = explode( ':', $v, 2 );
            if( isset( $t[1] ) )
                $head[ trim($t[0]) ] = trim( $t[1] );
            else
            {
                $head[] = $v;
                if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                    $head['reponse_code'] = intval($out[1]);
            }
        }
        return $head;
    }

    /**
     * Get video content type.
     *
     * @param $video_content_type
     * @return string
     */
    function video_content_type($video_content_type){
        switch ( $video_content_type ){
            case 'video/x-flv':             $extension = '.flv';    break;
            case 'video/mp4':               $extension = '.mp4';    break;
            case 'application/x-mpegURL':   $extension = '.m3u8';   break;
            case 'video/MP2T':              $extension = '.ts';     break;
            case 'video/3gpp':              $extension = '.3gp';    break;
            case 'video/quicktime':         $extension = '.mov';    break;
            case 'video/x-msvideo':         $extension = '.avi';    break;
            case 'video/x-ms-wmv':          $extension = '.wmv';    break;
            default:                        $extension = '.mp4';    break;
        }

        return $extension;
    }

    /**
     * Get image content type.
     *
     * @param $image_content_type
     * @return string
     */
    function image_content_type($image_content_type){
        switch ( $image_content_type ){
            case 'image/bmp':               $extension = '.bmp';    break;
            case 'image/cis-cod':           $extension = '.cod';    break;
            case 'image/gif':               $extension = '.gif';    break;
            case 'image/ief':               $extension = '.ief';    break;
            case 'image/jpeg':              $extension = '.jpeg';   break;
            case 'image/pipeg':             $extension = '.jfif';   break;
            case 'image/svg+xml':           $extension = '.svg';    break;
            case 'image/tiff':              $extension = '.tiff';   break;
            case 'image/x-cmu-raster':      $extension = '.ras';    break;
            case 'image/x-cmx':             $extension = '.cmx';    break;
            case 'image/x-icon':            $extension = '.ico';    break;
            case 'image/x-portable-anymap': $extension = '.pnm';    break;
            case 'image/x-portable-bitmap': $extension = '.pbm';    break;
            case 'image/x-portable-graymap':$extension = '.pgm';    break;
            case 'image/x-portable-pixmap': $extension = '.ppm';    break;
            case 'image/x-rgb':             $extension = '.rgb';    break;
            case 'image/x-xbitmap':         $extension = '.xbm';    break;
            case 'image/x-xpixmap':         $extension = '.xpm';    break;
            case 'image/x-xwindowdump':     $extension = '.xwd';    break;
            default:                        $extension = '.jpeg';   break;
        }

        return $extension;
    }

    /**
     * Return the high-level directory.
     *
     * @param $file_path_to_build
     * @return mixed
     */
    function build_file_path($file_path_to_build){
        $media_type = $file_path_to_build['media_type'];
        $year = $file_path_to_build['year'];
        $month = $file_path_to_build['month'];

        $filepath = UPLOAD_DIR.$media_type."/".$year;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        $filepath .= "/".$month."/";

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }

        $filemain = str_replace(UPLOAD_DIR, "", $filepath);

        return $filemain;
    }

    /**
     * Load image.
     *
     * @param $source_file
     * @return null|resource
     */
    function loadImage($source_file){

        // Load image
        $ext = exif_imagetype ($source_file);
        $destination_file = NULL;

        if($ext == IMAGETYPE_PNG) $destination_file = imagecreatefrompng ($source_file);
        elseif($ext == IMAGETYPE_JPEG) $destination_file = imagecreatefromjpeg ($source_file);
        elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $destination_file = imagecreatefromwbmp ($source_file);
        elseif($ext == IMAGETYPE_GIF) $destination_file = imagecreatefromgif ($source_file);

        return $destination_file;
    }

    /**
     * Resize image and push to S3.
     *
     * @param $thumb_array
     * @return bool|mixed|null
     */
    function resize_image_push_to_S3($thumb_array, $original_file){

        $filepath_thumb     = $thumb_array['filepath'];
        $newwidth           = $thumb_array['new_width'];
        $file_a3_path_thumb = $thumb_array['s3_path'];

        if (file_exists($original_file)){
            // Thumb No Watermark
            list($width,$height)=getimagesize($original_file);

            $newheight=($height/$width) * $newwidth;

            // Load original image
            $src = $this->loadImage($original_file);

            // Resize the image to be however pixels wide, and maintain the original aspect ratio.
            $tmp=imagecreatetruecolor($newwidth,$newheight);

            // Copy from the original image into the $tmp image
            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

            // Write the resized image jpeg to disk
            imagejpeg($tmp,$filepath_thumb,100);

            imagedestroy($src);
            imagedestroy($tmp);

            $l_sNewAbsoluteFilePath = $filepath_thumb;

            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);

            return $l_sNewPath;

        } else {
            error_log("Unresized file ".$filepath_thumb." does not exist");

            return false;
        }
    }

    /**
     * Resize to square image and push to S3.
     *
     * @param $thumb_array
     * @return bool|mixed|null
     */
    function resize_to_square_image_push_to_S3($thumb_array, $original_file){
        
        $filepath_thumb     = $thumb_array['filepath'];
        $newwidth           = $thumb_array['new_width'];
        $newheight          = $thumb_array['new_width'];
        $file_a3_path_thumb = $thumb_array['s3_path'];

        if (file_exists($original_file)){

            // Thumb No Watermark

            // We get the square side and the offsets
            list($width,$height)=getimagesize($original_file);

            // horizontal rectangle
            if ($width > $height) {
                $square = $height;                  // $square: square side length
                $offsetX = ($width - $height) / 2;  // x offset based on the rectangle
                $offsetY = 0;                       // y offset based on the rectangle
            }
            // vertical rectangle
            elseif ($height > $width) {
                $square = $width;
                $offsetX = 0;
                $offsetY = ($height - $width) / 2;
            }
            // it's already a square
            else {
                $square = $width;
                $offsetX = $offsetY = 0;
            }

            // Load original image
            $src = $this->loadImage($original_file);

            // Resize the image to be however pixels wide, and maintain the original aspect ratio.
            $tmp=imagecreatetruecolor($newwidth, $newheight);

            // Copy from the original image into the $tmp image
            imagecopyresampled($tmp, $src, 0, 0, $offsetX, $offsetY, $newwidth, $newheight, $square, $square);

            // Write the resized image jpeg to disk
            imagejpeg($tmp,$filepath_thumb,100);

            imagedestroy($src);
            imagedestroy($tmp);

            $l_sNewAbsoluteFilePath = $filepath_thumb;

            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path_thumb);

            return $l_sNewPath;

        } else {
            error_log("Unresized file ".$filepath_thumb." does not exist");

            return false;
        }
    }

    /**
     * Watermark Play Button and push to S3.
     * 
     * @param $thumb_array
     * @return bool|mixed|null
     */
    function watermark_play_button_push_to_s3($thumb_array){

        $watermarked_local_file = $thumb_array['filepath'];
        $width = $thumb_array['new_width'];
        $watermarked_cloud_file = $thumb_array['s3_watermark_path'];

        if (file_exists($watermarked_local_file)){

            // Watermark Play Button onto Thumbnail

            // Get Original Thumbnail width & height
            list($sw, $sh) = getimagesize($watermarked_local_file);

            // Get Watermark Play Button width & height
            list($sww, $swh) = getimagesize(WATERMARK_PLAY_BUTTON);

            // Calculate simulated size
            if (empty($width)){ $width = $sw; }

            $height = round($sh * $width / $sw);

            // Resized watermark size
            $ww = round($width);
            $wh = round($swh * $ww / $sww);

            // Load original image
            $hSource = $this->loadImage($watermarked_local_file);

            // Load watermark image
            $hWatermark = $this->loadImage(WATERMARK_PLAY_BUTTON);

            // Resize the image to be however pixels wide, and maintain the original aspect ratio.
            $hDest = imagecreatetruecolor($width, $height);

            // Copy from the original image into the $tmp image
            imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);

            // Watermark
            imagecopyresampled($hDest, $hWatermark, $width - $ww, ($height - $wh)/2, 0, 0, $ww, $wh, $sww, $swh);

            // Write the resized image jpeg to disk
            imagejpeg($hDest,$watermarked_local_file."_watermark.jpeg");

            $l_sNewAbsoluteFilePath = $watermarked_local_file."_watermark.jpeg";

            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath, $watermarked_cloud_file);

//            @unlink($watermarked_local_file);

            return $l_sNewPath;

        } else {

            error_log("Unwatermarked file ".$watermarked_local_file." does not exist");

            return false;
        }
    }

    /**
     * Black list Viral Video XML.
     *
     * @param $feed
     * @return mixed
     */
    function blacklist_viral_video_xml($feed){

        $post = array(
            'blacklist' => 1
        );

        $this->db->where("id", $feed['id']);
        $res = $this->db->update("viral_media_xml", $post);

        return $res;
    }

    /**
     * Update enr_video id description and tag.
     * 
     * @param $feed
     * @return mixed
     */
    function updateEnrVideoViralVideoXML($feed){

        $id = $feed['feed_id'];

        $post = array(
            'description' => $feed['description'],
            'tags' => $feed['tags']//,
        );

        $this->db->where("id", $id);
        $res = $this->db->update("enr_video", $post);

        return $res;
    }

    /**
     * Update enr_video S3 Thumbnail.
     *
     * @param $feed_id
     * @param $local_path
     * @param $S3_path
     * @return mixed|null
     */
    function updateEnrVideoS3Thumbnail($feed_id, $local_path, $S3_path){

        if ($local_path != ""){
            $l_sNewVideoPath = Utils::pushFileToS3(UPLOAD_DIR.$local_path, $S3_path);

            $post = array(
                'image_path' => $local_path
            );

            $this->db->where("id", $feed_id);
            $res = $this->db->update("enr_video", $post);

            return $l_sNewVideoPath;
        }
    }

    private function _uploadVideo($name='video'){
        $year = date('Y');
        $month = date('m');
        $fileinfo = $_FILES[$name];

//        if ($fileinfo['error'] != 0){
            $upload_tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();

            error_log('main_m->_uploadVideo asynchronous upload to: '.$upload_tmp_dir.' with file error: '
                .$fileinfo['error']);
//        }

        if(!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a correct file.";
            return false;
        }
        $filenames = explode(".", $fileinfo['name']);
        $ext = $filenames[count($filenames)-1];
        if(stripos("flv,mpg,mp4,mov,mpeg,m4v", $ext) === false){
            $this->controller->data['show_errors'][] = "Only flv,mpg,mp4,mov,mpeg,m4v files can be uploaded.";
            return false;
        }
        $filename = md5(time().rand(0.1, 99.9));
        $filepath = UPLOAD_DIR."video/".$year;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$month;

        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$filename.".".$ext;

        move_uploaded_file($fileinfo['tmp_name'], $filepath);

        $convertpath = UPLOAD_DIR.$filepath;

        //shell_exec ( "/usr/bin/cnvt.sh {$convertpath}");

        //@unlink($filepath . '.tmp');
        return str_replace(UPLOAD_DIR, "", $filepath);
    }
    private function _uploadMusic($name='music'){
        $year = date('Y');
        $month = date('m');
        $fileinfo = $_FILES[$name];
        if(!$fileinfo['size']){
            $this->controller->data['show_errors'][] = "Please select a correct file.";
            return false;
        }
        $filenames = explode(".", $fileinfo['name']);
        $ext = $filenames[count($filenames)-1];
        if(stripos("mp3", $ext) === false){
            $this->controller->data['show_errors'][] = "Only mp3 files can be uploaded.";
            return false;
        }
        $filename = md5(time().rand(0.1, 99.9));
        $filepath = UPLOAD_DIR."music/".$year;
        $file_a3_path = UPLOAD_AMZ."music/".$year;
        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$month;
        $file_a3_path .= "/".$month;
        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$filename.".".$ext;
        $file_a3_path .= "/".$filename.".".$ext;
        move_uploaded_file($fileinfo['tmp_name'], $filepath);


        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewVideoPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);

        return str_replace(UPLOAD_DIR, "", $filepath);
    }

    /**
     * Upload custom video thumbnail.
     *
     * @return mixed
     */
    function _uploadPhoto_video(){

        $img_values = explode(",", $this->input->post('video_img_value'));
        $header = $img_values[0];
        $data = base64_decode($img_values[1]);
        $ext = str_replace(array("data:image/", ";base64"), "", $header);

        if (!empty($img_values) && stripos("jpeg,png,gif,jpg,bmp", $ext) === false){

            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded for video thumbnails.";

            return false;
        }

        $filename = md5(time().rand(0.1, 99.9));
        $year = date('Y');
        $month = date('m');

        // Arrays to build file paths for: Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
        $thumb_file_paths_to_build =
            array(
                'original' => // Original
                    array (
                        "media_type" => "photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'big' => // Big Thumbnail (640)
                    array (
                        "media_type" => "big_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array (
                        "media_type" => "thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'small' => // Small Thumbnail (180)
                    array (
                        "media_type" => "small_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
                );

        $square_thumb_file_paths_to_build =
            array(
                'small_square_thumb' => // Small Square Thumbnail (180)
                    array (
                        "media_type" => "small_square_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        // General dir

        $thumb_filepath_array        = array();
        $square_thumb_filepath_array = array();

        foreach ($thumb_file_paths_to_build as $thumb_file_path_to_build){
            $thumb_filepath_array[] = $this->build_file_path($thumb_file_path_to_build);
        }

        foreach ($square_thumb_file_paths_to_build as $square_thumb_file_path_to_build){
            $square_thumb_filepath_array[] = $this->build_file_path($square_thumb_file_path_to_build);
        }

        // Top-level directories
        $photo_filepath       = $thumb_filepath_array[0];
        $big_thumb_filepath   = $thumb_filepath_array[1];
        $thumb_filepath       = $thumb_filepath_array[2];
        $small_thumb_filepath = $thumb_filepath_array[3];
        $small_square_thumb_filepath = $square_thumb_filepath_array[0];

        // Local directories
        $filepath             = UPLOAD_DIR.$photo_filepath.$filename.".jpg";
        $filepath_big_thumb   = UPLOAD_DIR.$big_thumb_filepath.$filename.".jpg";
        $filepath_thumb       = UPLOAD_DIR.$thumb_filepath.$filename.".jpg";
        $filepath_small_thumb = UPLOAD_DIR.$small_thumb_filepath.$filename.".jpg";
        $filepath_small_square_thumb = UPLOAD_DIR.$small_square_thumb_filepath.$filename.".jpg";

        // Amazon S3 directories
        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_big_thumb_path   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_thumb_path       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_thumb_path = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_square_thumb_path = UPLOAD_AMZ."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Amazon S3 watermaked directories
        $file_a3_water_path             = UPLOAD_AMZ_WATER."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_big_thumb_path   = UPLOAD_AMZ_WATER."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_thumb_path       = UPLOAD_AMZ_WATER."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_thumb_path = UPLOAD_AMZ_WATER."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_square_thumb_path = UPLOAD_AMZ_WATER."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Write original image data to file
        $fp = fopen($filepath, 'w');
        fwrite($fp, $data);
        fclose($fp);

        // Check for original file to resize and watermark images
        if (file_exists($filepath)){

            // Create and push to S3:

            $thumb_array =
                array(
                    'original' => // Original
                        array(
                            "filepath" => $filepath,
                            "new_width" => NULL,
                            "s3_path" => $file_a3_path,
                            "s3_watermark_path" => $file_a3_water_path
                        ),
                    'big_thumb' => // Big Thumbnail (640)
                        array(
                            "filepath" => $filepath_big_thumb,
                            "new_width" => 640,
                            "s3_path" => $file_a3_big_thumb_path,
                            "s3_watermark_path" => $file_a3_water_big_thumb_path
                        ),
                    'thumb' => // Medium Thumbnail (320)
                        array(
                            "filepath" => $filepath_thumb,
                            "new_width" => 320,
                            "s3_path" => $file_a3_thumb_path,
                            "s3_watermark_path" => $file_a3_water_thumb_path
                        ),
                    'small_thumb' => // Small Thumbnail (180)
                        array(
                            "filepath" => $filepath_small_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_thumb_path
                        )
                );

            $square_thumb_array =
                array(
                    'small_square_thumb' => // Small Square Thumbnail (180)
                        array(
                            "filepath" => $filepath_small_square_thumb,
                            "new_width" => 180,
                            "s3_path" => $file_a3_small_square_thumb_path,
                            "s3_watermark_path" => $file_a3_water_small_square_thumb_path
                        )
                );

            $l_sNew_Path           = array();
            $l_sNew_Watermark_Path = array();

            // Create and push to S3:
            //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
            //      Small Thumbnail (180)

            //      Their watermark versions

            foreach ($thumb_array as $thumb){
                $new_file           = $this->resize_image_push_to_S3($thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;

            }

            //      Square Small Thumbnail (180) and watermark version

            foreach ($square_thumb_array as $square_thumb){
                $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }
        }

        $url = $this->controller->chanel['url'];

        $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');
        Memcaching::getInstance()->removeKey($l_sMemcacheKey);


       

        return str_replace(UPLOAD_DIR, "", $filepath);

        // return $filepath;
    }

function _uploadSlider_photo(){

        $img_values = explode (",", $this->input->post('slider_thumb'));
        $header = $img_values[0];
        $data = base64_decode($img_values[1]);
        $ext = str_replace(array("data:image/", ";base64"), "", $header);


        if (!empty($img_values) && stripos("jpeg,png,gif,jpg,bmp", $ext) === false){

            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded for video thumbnails.";

            return false;
        }

        $filename = md5(time().rand(0.1, 99.9));
        $year = date('Y');
        $month = date('m');

        // Arrays to build file paths for: Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
        $thumb_file_paths_to_build =
            array(
                'original' => // Original
                    array (
                        "media_type" => "tmp/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'big' => // Big Thumbnail (640)
                    array (
                        "media_type" => "tmp/big_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array (
                        "media_type" => "tmp/thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'small' => // Small Thumbnail (180)
                    array (
                        "media_type" => "tmp/small_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        $square_thumb_file_paths_to_build =
            array (
                'small_square' => // Small Square Thumbnail (180)
                    array(
                        "media_type" => "tmp/small_square_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        // General dir

        $thumb_filepath_array = array();
        $square_thumb_filepath_array = array();

        foreach ($thumb_file_paths_to_build as $thumb_file_path_to_build){
            $thumb_filepath_array[] = $this->build_file_path($thumb_file_path_to_build);
        }

        foreach ($square_thumb_file_paths_to_build as $square_thumb_file_path_to_build){
            $square_thumb_filepath_array[] = $this->build_file_path($square_thumb_file_path_to_build);
        }

        // Top-level directories
        $photo_filepath       = $thumb_filepath_array[0];
        $big_thumb_filepath   = $thumb_filepath_array[1];
        $thumb_filepath       = $thumb_filepath_array[2];
        $small_thumb_filepath = $thumb_filepath_array[3];

        $small_square_filepath = $square_thumb_filepath_array[0];

        // Local directories
        $filepath             = UPLOAD_DIR.$photo_filepath.$filename.".jpg";
        $filepath_big_thumb   = UPLOAD_DIR.$big_thumb_filepath.$filename.".jpg";
        $filepath_thumb       = UPLOAD_DIR.$thumb_filepath.$filename.".jpg";
        $filepath_small_thumb = UPLOAD_DIR.$small_thumb_filepath.$filename.".jpg";

        $filepath_small_square_thumb = UPLOAD_DIR.$small_square_filepath.$filename.".jpg";


        // Amazon S3 directories
        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_big_thumb_path   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_thumb_path       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_thumb_path = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        $file_a3_small_square_thumb_path = UPLOAD_AMZ."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Amazon S3 watermaked directories
        $file_a3_water_path             = UPLOAD_AMZ_WATER."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_big_thumb_path   = UPLOAD_AMZ_WATER."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_thumb_path       = UPLOAD_AMZ_WATER."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_thumb_path = UPLOAD_AMZ_WATER."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        $file_a3_water_small_square_thumb_path = UPLOAD_AMZ_WATER."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Write original image data to file
        $fp = fopen($filepath, 'w');
        fwrite($fp, $data);
        fclose($fp);

        // Check for original file to resize and watermark images
        if (file_exists($filepath)){

            // Create and push to S3: Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
            $thumb_array = array(
                'original' => // Original
                    array(
                        "filepath" => $filepath,
                        "new_width" => NULL,
                        "s3_path" => $file_a3_path,
                        "s3_watermark_path" => $file_a3_water_path
                    ),
                'big_thumb' => // Big Thumbnail (640)
                    array(
                        "filepath" => $filepath_big_thumb,
                        "new_width" => 640,
                        "s3_path" => $file_a3_big_thumb_path,
                        "s3_watermark_path" => $file_a3_water_big_thumb_path
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array(
                        "filepath" => $filepath_thumb,
                        "new_width" => 320,
                        "s3_path" => $file_a3_thumb_path,
                        "s3_watermark_path" => $file_a3_water_thumb_path
                    ),
                'small_thumb' => // Small Thumbnail (180)
                    array(
                        "filepath" => $filepath_small_thumb,
                        "new_width" => 180,
                        "s3_path" => $file_a3_small_thumb_path,
                        "s3_watermark_path" => $file_a3_water_small_thumb_path
                    )
            );

            $square_thumb_array = array(
                'small_square_thumb' =>
                    array(
                        "filepath" => $filepath_small_square_thumb,
                        "new_width" => 180,
                        "s3_path" => $file_a3_small_square_thumb_path,
                        "s3_watermark_path" => $file_a3_water_small_square_thumb_path
                    )
            );

            $l_sNew_Path           = array();
            $l_sNew_Watermark_Path = array();

            // Create and push to S3:
            //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
            //      Small Thumbnail (180)

            //      Their watermark versions

            foreach ($thumb_array as $thumb){
                $new_file           = $this->resize_image_push_to_S3($thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }

            //      Square Small Thumbnail (180) and watermark version

            foreach ($square_thumb_array as $square_thumb){
                $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }
        }

         $url = $this->controller->chanel['url'];

         $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');
         Memcaching::getInstance()->removeKey($l_sMemcacheKey);

         @unlink($filepath);
         @unlink($filepath_big_thumb);
         @unlink($filepath_thumb);
         @unlink($filepath_small_thumb);
         @unlink($filepath_small_square_thumb);

         @unlink($filepath."_watermark.jpeg");
         @unlink($filepath_big_thumb."_watermark.jpeg");
         @unlink($filepath_thumb."_watermark.jpeg");
         @unlink($filepath_small_thumb."_watermark.jpeg");
         @unlink($filepath_small_square_thumb."_watermark.jpeg");

        return str_replace(UPLOAD_DIR."tmp/","", $filepath);

        return $filepath;
        
    }


function _uploadCategory_photo(){

        $img_values = explode (",", $this->input->post('cat_image'));
        $header = $img_values[0];
        $data = base64_decode($img_values[1]);
        $ext = str_replace(array("data:image/", ";base64"), "", $header);


        if (!empty($img_values) && stripos("jpeg,png,gif,jpg,bmp", $ext) === false){

            $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded for video thumbnails.";

            return false;
        }

        $filename = md5(time().rand(0.1, 99.9));
        $year = date('Y');
        $month = date('m');

        // Arrays to build file paths for: Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
        $thumb_file_paths_to_build =
            array(
                'original' => // Original
                    array (
                        "media_type" => "tmp/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'big' => // Big Thumbnail (640)
                    array (
                        "media_type" => "tmp/big_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array (
                        "media_type" => "tmp/thumb/photo",
                        "year" => $year,
                        "month" => $month
                    ),
                'small' => // Small Thumbnail (180)
                    array (
                        "media_type" => "tmp/small_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        $square_thumb_file_paths_to_build =
            array (
                'small_square' => // Small Square Thumbnail (180)
                    array(
                        "media_type" => "tmp/small_square_thumb/photo",
                        "year" => $year,
                        "month" => $month
                    )
            );

        // General dir

        $thumb_filepath_array = array();
        $square_thumb_filepath_array = array();

        foreach ($thumb_file_paths_to_build as $thumb_file_path_to_build){
            $thumb_filepath_array[] = $this->build_file_path($thumb_file_path_to_build);
        }

        foreach ($square_thumb_file_paths_to_build as $square_thumb_file_path_to_build){
            $square_thumb_filepath_array[] = $this->build_file_path($square_thumb_file_path_to_build);
        }

        // Top-level directories
        $photo_filepath       = $thumb_filepath_array[0];
        $big_thumb_filepath   = $thumb_filepath_array[1];
        $thumb_filepath       = $thumb_filepath_array[2];
        $small_thumb_filepath = $thumb_filepath_array[3];

        $small_square_filepath = $square_thumb_filepath_array[0];

        // Local directories
        $filepath             = UPLOAD_DIR.$photo_filepath.$filename.".jpg";
        $filepath_big_thumb   = UPLOAD_DIR.$big_thumb_filepath.$filename.".jpg";
        $filepath_thumb       = UPLOAD_DIR.$thumb_filepath.$filename.".jpg";
        $filepath_small_thumb = UPLOAD_DIR.$small_thumb_filepath.$filename.".jpg";

        $filepath_small_square_thumb = UPLOAD_DIR.$small_square_filepath.$filename.".jpg";


        // Amazon S3 directories
        $file_a3_path             = UPLOAD_AMZ."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_big_thumb_path   = UPLOAD_AMZ."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_thumb_path       = UPLOAD_AMZ."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_small_thumb_path = UPLOAD_AMZ."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        $file_a3_small_square_thumb_path = UPLOAD_AMZ."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Amazon S3 watermaked directories
        $file_a3_water_path             = UPLOAD_AMZ_WATER."photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_big_thumb_path   = UPLOAD_AMZ_WATER."big_thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_thumb_path       = UPLOAD_AMZ_WATER."thumb/photo/".$year."/".$month."/".$filename.".jpg";
        $file_a3_water_small_thumb_path = UPLOAD_AMZ_WATER."small_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        $file_a3_water_small_square_thumb_path = UPLOAD_AMZ_WATER."small_square_thumb/photo/".$year."/".$month."/".$filename.".jpg";

        // Write original image data to file
        $fp = fopen($filepath, 'w');
        fwrite($fp, $data);
        fclose($fp);

        // Check for original file to resize and watermark images
        if (file_exists($filepath)){

            // Create and push to S3: Big Thumbnail (640), Medium Thumbnail (320), Small Thumbnail (180)
            $thumb_array = array(
                'original' => // Original
                    array(
                        "filepath" => $filepath,
                        "new_width" => NULL,
                        "s3_path" => $file_a3_path,
                        "s3_watermark_path" => $file_a3_water_path
                    ),
                'big_thumb' => // Big Thumbnail (640)
                    array(
                        "filepath" => $filepath_big_thumb,
                        "new_width" => 640,
                        "s3_path" => $file_a3_big_thumb_path,
                        "s3_watermark_path" => $file_a3_water_big_thumb_path
                    ),
                'thumb' => // Medium Thumbnail (320)
                    array(
                        "filepath" => $filepath_thumb,
                        "new_width" => 320,
                        "s3_path" => $file_a3_thumb_path,
                        "s3_watermark_path" => $file_a3_water_thumb_path
                    ),
                'small_thumb' => // Small Thumbnail (180)
                    array(
                        "filepath" => $filepath_small_thumb,
                        "new_width" => 180,
                        "s3_path" => $file_a3_small_thumb_path,
                        "s3_watermark_path" => $file_a3_water_small_thumb_path
                    )
            );

            $square_thumb_array = array(
                'small_square_thumb' =>
                    array(
                        "filepath" => $filepath_small_square_thumb,
                        "new_width" => 180,
                        "s3_path" => $file_a3_small_square_thumb_path,
                        "s3_watermark_path" => $file_a3_water_small_square_thumb_path
                    )
            );

            $l_sNew_Path           = array();
            $l_sNew_Watermark_Path = array();

            // Create and push to S3:
            //      Original Thumbnail, Big Thumbnail (640), Medium Thumbnail (320),
            //      Small Thumbnail (180)

            //      Their watermark versions

            foreach ($thumb_array as $thumb){
                $new_file           = $this->resize_image_push_to_S3($thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }

            //      Square Small Thumbnail (180) and watermark version

            foreach ($square_thumb_array as $square_thumb){
                $new_file           = $this->resize_to_square_image_push_to_S3($square_thumb, $filepath);
                $new_watermark_file = $this->watermark_play_button_push_to_s3($square_thumb);

                $l_sNew_Path[]           = $new_file;
                $l_sNew_Watermark_Path[] = $new_watermark_file;
            }
        }

         $url = $this->controller->chanel['url'];

         $l_sMemcacheKey = Memcaching::makeKey($url.'fp-videos');
         Memcaching::getInstance()->removeKey($l_sMemcacheKey);

         @unlink($filepath);
         @unlink($filepath_big_thumb);
         @unlink($filepath_thumb);
         @unlink($filepath_small_thumb);
         @unlink($filepath_small_square_thumb);

         @unlink($filepath."_watermark.jpeg");
         @unlink($filepath_big_thumb."_watermark.jpeg");
         @unlink($filepath_thumb."_watermark.jpeg");
         @unlink($filepath_small_thumb."_watermark.jpeg");
         @unlink($filepath_small_square_thumb."_watermark.jpeg");

        return str_replace(UPLOAD_DIR."tmp/","", $filepath);

        return $filepath;
        
    }

   


    

    function updateFeedVideo($feed){
//        $l_bCLOSE_BUFFER = 1;
        
        if(strtotime($feed['end_date']) < strtotime($feed['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }

        if($feed['time_stamp'] > time()) {
            $this->controller->data['show_errors'] = "Invalid post date and time.";
            return false;
        }

        $id = ltrim($feed['feed_id'], "video_");
        $oFeed = $this->getFeedVideo($id);
        if(($id!="0" || $id!="")&& $l_bCLOSE_BUFFER){
            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>
            
            window.location.href='/content/postfeed/';
            </SCRIPT>");
        }
      
        $slider_thumb='';
        if(isset($_REQUEST['slider_thumb']) && !empty($_REQUEST['slider_thumb'])){

            $oslider_thumb= UPLOAD_DIR.$oFeed['slider_thumb'];

            $slider_thumb = $this->_uploadSlider_photo();

            @unlink($oslider_thumb);
        }


        $image_path = '';
        if(isset($_REQUEST['video_img_value']) && !empty($_REQUEST['video_img_value'])){

            $oimage_path = UPLOAD_CLOUD.$oFeed['image_path'];

            $image_path = $this->_uploadPhoto_video();

            list($width,$height)=getimagesize(UPLOAD_DIR.$image_path);
            $aspect_ratio = $height/$width;

            @unlink($oimage_path);

        }
//        elseif(is_numeric($oFeed['video_type'])){
//
//            switch ($oFeed['video_type']) {
//                case EMBED_VIDEO_TYPE_ID_YOUTUBE:
//                    $thumbnail = $this->getYouTubeThumbnail($feed['video_url']);
//                    break;
//                case EMBED_VIDEO_TYPE_ID_VIMEO:
//                    $thumbnail = $this->getVimeoThumbnail($feed['video_url']);
//                    break;
//                case EMBED_VIDEO_TYPE_ID_VEVO:
//                    $thumbnail = $this->getVevoThumbnail($feed['video_url']);
//                    break;
//                case EMBED_VIDEO_TYPE_ID_INSTAGRAM:
//                    $thumbnail = $this->getInstagramThumbnail($feed['video_url']);
//                    break;
//                case EMBED_VIDEO_TYPE_ID_VINE:
//                    $thumbnail = $this->getVineThumbnail($feed['video_url']);
//                    break;
//            }
//
//            $image_path = $this->grabThumbnail($thumbnail);
//            $oimage_path = UPLOAD_DIR.$image_path;
//
//            if(is_file($oimage_path)){
//
//                list($width,$height)=getimagesize($oimage_path);
//                $aspect_ratio = $height/$width;
//
//                unlink($oimage_path);
//            }
//        }
        else{
            $video_path = $this->input->post('destination');


            if($video_path == $oFeed['destination']) {
                $oimage_path = $oFeed['image_path'];
                $image_path = $oimage_path;

                $aspect_ratio = $oFeed['image_aspect_ratio'];
            }else{
                $oimage_path = UPLOAD_CLOUD.$oFeed['image_path'];
                if(is_file($oimage_path)){
                    unlink($oimage_path);
                }

                $image_path = $this->getThumbnailFromVideo(UPLOAD_CLOUD.$video_path);

                list($width,$height)=getimagesize(UPLOAD_DIR.$image_path);
                $aspect_ratio = $height/$width;
            
            }
        }

        $clfile = UPLOAD_CLOUD.$oFeed['destination'];

        $tmplfile = UPLOAD_DIR.$oFeed['destination'];

        copy($clfile, $tmplfile);

        if($this->input->post('destination') != $oFeed['destination']){
            $destination = UPLOAD_CLOUD.$oFeed['destination'];
            if(is_file($destination)){
                unlink($destination);
            }
        }
        
        $post = array(
            'cid' => $feed['cid'],
            'description' => $feed['description'],
            'tags' => $feed['tags'],
            'credit' => $feed['credit'],
            'video_url' => $feed['video_url'],
            'video_type' => $feed['video_type'],
            'destination' => $this->input->post('destination'),
            'video_mime' => $this->input->post('video_mime'),
            'video_size' => $this->input->post('video_size'),
            "start_date" => $feed['start_date'],
            "end_date" => $feed['end_date'],
             "feed_date" => $feed['feed_date'],
            'is_publish' => $feed['is_publish'],
            'is_sticky' => $feed['is_sticky'],
            'is_oflo' => $feed['is_oflo'],
            'is_oflo_slider' => $feed['is_oflo_slider'],
            'image_aspect_ratio'=>$aspect_ratio,
            'is_premium'=>$feed['is_premium'],
            'is_feed'=>$feed['is_feed'],
            'product_widget'=>$feed['product_widget'],
        );

        $l_nChannelId = $feed['cid'];
        $l_sDescription = $feed['description'];

         // if(empty($slider_thumb) {
         //    $post['slider_thumb'] = $slider_thumb;
         //    $post['slider_thumb'] = $_FILES['image_slider']['type'];
         // }
        
      
        if($feed['time_stamp'] != 0) {
            $post['time_stamp'] = $feed['time_stamp'];
        }
       
        if (!empty($image_path)) {
            $post['image_path'] = $image_path;
            $post['image_mime'] = $_FILES['image']['type'];
            
        }

        if (!empty($slider_thumb)) {
            $post['slider_thumb'] = $slider_thumb;
            $post['image_mime'] = $_FILES['image_slider']['type'];
            
        }
        
        $this->db->where("id", $id);
        $res = $this->db->update("enr_video", $post);

   
                  


        $feed_id = $this->get_feed_by_content_id('video', $id);
        if(!empty($feed_id) && $feed['is_publish'] == 1) {
            $this->sendpush($feed_id);
        }

        $this->updateDeepLink('video', $id);
        $this->break_oflo_video_memcache($feed);

        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18 || $l_nChannelId == 24){
            $SCAPI= new SCAPI();
            $SCAPI->rq_sendToLegacyServer($l_nChannelId,'video',$tmplfile,$l_sDescription);

            unlink($tmplfile);
        }

        if($feed['post_to_facebook']=="true"){

            $post = array(
                'post_type_id' => 1,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 1,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();
            $l_bIsVideo = true;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);


            foreach($query->result_array() as $row){

                $data = array(
                    'page_id' => $row['page_id'],
                    'social_token' => $row['social_token'],
                    'link' => $row['deep_link_web_oflo_lp'],
                    'picture' => UPLOAD_CLOUD.'watermark/'.$row['content_file'],
                    'message' => $row['message'],
                    'main_id' => $row['main_id']
                );

                $this->share_to_facebook($data);
            }


        }

        if($feed['post_to_twitter']=="true"){

            $post = array(
                'post_type_id' => 1,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 2,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();

            $l_bIsVideo = true;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);
            foreach($query->result_array() as $row){

                $oath_token = $row['social_token'];
                $oath_secret_token = $row['social_token_secret'];
                $page_id = $row['page_id'];

                $twitter = new TwitterOAuth("292aZXJ21hJRD3TwaL4nhTSzg", 'E2TuUxUNCUpnBj59RzT08V0c30HrRT9owKociLyrxKQiHTJtR1', $oath_token, $oath_secret_token);

                $twitter_response = $twitter->post('statuses/update', array('status' => $row['deep_link_web_oflo_lp'].' '.$row['message']));



                $posted_id = $twitter_response->id;

                $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                $querynew = $this->db->query($sql);
            }

        }

        if($feed['post_to_tumblr']=="true"){

            $post = array(
                'post_type_id' => 1,
                'channel_id' => $feed['cid'],
                'content_id' => $id,
                'social_property_type_id' => 4,
                'channel_oauth_id' => NULL,
                'message' => $feed['description'],
                'has_sent' => 'N',
                'created' => date("Y-m-d H:i:s")
            );

            $res = $this->db->insert("enr_content_post_to_social", $post);
            $postid = $this->db->insert_id();

            $l_bIsVideo = true;

            $sql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp, a.deep_link_web_oflo_lp
                                
                                FROM enr_content_post_to_social cs 
                                JOIN enr_channel_oauth co  
                                ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                                
                                JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                                ON (cs.content_id = a.id )
                                
                                JOIN enr_chanel c  
                                ON (cs.channel_id = c.id )

                                WHERE cs.has_sent = "N" 
                                AND co.active = "Y"
                                AND cs.id = '.intval($postid);

            $query = $this->db->query($sql);
            foreach($query->result_array() as $row){

                $l_sEmbedCodeInfo = Utils::encryptItemId($row['channel_id'].','.(($l_bIsVideo) ? 'v' : 'p').','.$row['content_id']);
                $l_sEmbedCodeInfo = 'http://'.$row['channel_url'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $oath_token = $row['social_token'];
                $oath_secret_token = $row['social_token_secret'];
                echo "\n\n";

                $tum_url = preg_replace("/https?:\/\//","",$row['page_name']);
                $tum_url = preg_replace("/\//","",$tum_url);

                $l_bSuppressOAuth = true;
                require_once(LIB_PATH.'tumblr/tumblroauth.php');

                $tumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$oath_token,$oath_secret_token);
                $post_url = 'http://api.tumblr.com/v2/blog/'.$tum_url.'/post';

                $params = array();


                $params['type'] = "video";
                $params['caption'] = $row['message'];
                $params['embed'] = '<iframe scrolling="no" frameborder="0" width="680" height="383" src="'.$l_sEmbedCodeInfo.'"></iframe>';


                $tumblr_response = $tumblr->post($post_url,$params);


                if (201 == $tumblr_response->meta->status) {
                    $posted_id = $tumblr_response->response->id;
                    $sql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$posted_id.'" WHERE id = '.$row['main_id'];
                    $querynew = $this->db->query($sql);
                } else {
                    echo "error";
                    print_r($tumblr_response);
                }
            }

        }

        @unlink(UPLOAD_DIR.$image_path);
        @unlink(UPLOAD_DIR."big_thumb/".$image_path);
        @unlink(UPLOAD_DIR."thumb/".$image_path);
        @unlink(UPLOAD_DIR."small_thumb/".$image_path);
        @unlink(UPLOAD_DIR."small_square_thumb/".$image_path);

        @unlink(UPLOAD_DIR.$image_path."_watermark.jpeg");
        @unlink(UPLOAD_DIR."big_thumb/".$image_path."_watermark.jpeg");
        @unlink(UPLOAD_DIR."thumb/".$image_path."_watermark.jpeg");
        @unlink(UPLOAD_DIR."small_thumb/".$image_path."_watermark.jpeg");
        @unlink(UPLOAD_DIR."small_square_thumb/".$image_path."_watermark.jpeg");

        @unlink(UPLOAD_DIR.$feed['destination']);

        return $res;
    }

    /** Update deep link **/
    function updateDeepLink($post_type, $content_id) {

        $row = $this->db->query('select * from enr_' . $post_type . ' where id = ' . $content_id)->result_array();

        if(count($row) == 0) return;
        $row= $row[0];

        //      if(!empty($row['deep_link_web']) && $row['deep_link_web'] != "INVALID_URI") return;

        $channel = $this->get_chanel($row['cid']);

        $web_url = get_web_app_link($channel, $post_type, $content_id);
        $app_url = get_mobile_app_link($channel, $post_type, $content_id);
        $of_url = get_web_app_link_of($channel, $post_type, $content_id);

        if(!MODE_DEV) {

            $star_url = PRODUCTION_LANDING ."viewpost_auth.php". $web_url."&page_type=inline";
            $oflo_url = OFLO_LANDING ."ocontent/". $of_url;

            if($post_type=="photo"){
                $star_url = PRODUCTION_LANDING ."viewpost_auth.php". $web_url;
                $star_url_tw = PRODUCTION_LANDING ."viewpost_auth.php". $web_url;
                $star_url_lp = PRODUCTION_LANDING ."viewpost_auth.php". $web_url;

                $oflo_url = OFLO_LANDING ."ocontent/". $of_url;
                $oflo_url_lp = OFLO_LANDING ."ocontent/". $of_url;
            }

            else if($post_type=="video"){
                $star_url_tw = PRODUCTION_LANDING ."viewpost_auth.php". $web_url."&page_type=twitter";
                $star_url_lp = PRODUCTION_LANDING ."viewpost_auth.php". $web_url."&page_type=landingpage";
                $oflo_url_lp = OFLO_LANDING ."ocontent/". $of_url."?page_type=landingpage";
            }else{
                $star_url_tw    = PRODUCTION_LANDING ."viewpost_auth.php". $web_url."&page_type=twitter";
                $star_url_lp = PRODUCTION_LANDING ."viewpost_auth.php". $web_url."&page_type=landingpage";
                $oflo_url_lp = OFLO_LANDING ."ocontent/". $of_url."?page_type=landingpage";
            }
            $web_url = $star_url;
            $web_url_tw = $star_url_tw;
            $web_url_lp = $star_url_lp;
            $oflo_web_url = $oflo_url;
            $oflo_web_url_lp = $oflo_url_lp;



        }

        $update = array();

        $update['deep_link_web'] =  get_bitly_url($web_url);

        $update['deep_link_web_tw'] = get_bitly_url($web_url_tw);
        $update['deep_link_web_lp'] = get_bitly_url($web_url_lp);
        $update['deep_link_web_oflo'] = get_bitly_url($oflo_web_url);
        $update['deep_link_web_oflo_lp'] = get_bitly_url($oflo_web_url_lp);
        $update['deep_link_app'] = $app_url;

        $this->db->where("id", $content_id);
        $res = $this->db->update("enr_".$post_type, $update);
    }
    /**
     * feed poll
     */
    function addFeedPoll(){
        $post = $this->input->post();
        if(!$post['question'][0]){
            $this->controller->data['show_errors'] = "At least one question is required.";
            return false;
        }
        if(strtotime($post['end_date']) < strtotime($post['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        $pollSql = array(
            "cid" => $this->controller->chanel['id'],
            "fan_id" => "-1",
            "description" => $post['description'],
            "text" => $post['text'],
            "tags" => $post['tags'],
            "credit" => $post['credit'],
            "end_description" => $post['end_description'],
            "is_publish" => $post['is_publish'],
            "start_date" => strtotime($post['start_date']) == 0 ? date('Y-n-j') : $post['start_date'],
            "end_date" => strtotime($post['end_date']) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $post['end_date'],
            "time_stamp" => time(),
            'is_sticky' => $post['is_sticky'] == null ? 0 : $post['is_sticky'] == null ? 0 : $post['is_sticky'] == null ? 0 : $post['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0
        );
        if($_FILES['background_image']['size']){
            $image_path = $this->_uploadPhoto('background_image');
            $pollSql['background_image_path'] = $image_path;
        }
        if($_FILES['image1']['size']){
            $image_path = $this->_uploadPhoto('image1');
            $pollSql['image_path'] = $image_path;
        }
        if($_FILES['image2']['size']){
            $end_image_path = $this->_uploadPhoto('image2');
            $pollSql['end_image_path'] = $end_image_path;
        }
//        print_r($post);exit;
        $this->db->insert('enr_poll', $pollSql);
        $poll_id = $this->db->insert_id();
        foreach($post['question'] as $key => $question){
            if(!$question)
                continue;
            $pollQuestionSql = array(
                'poll_id' => $poll_id,
                'name' => $question
            );
            $this->db->insert('enr_poll_question', $pollQuestionSql);
            $question_id = $this->db->insert_id();
            foreach($post['answer'][$key] as $akey => $answer){
                if(!$answer)
                    continue;
                $pollAnswerSql = array(
                    'question_id' => $question_id,
                    'name' => $answer,
                    'is_correct' => (isset($post["is_correct_".$key."_".$akey]) && $post["is_correct_".$key."_".$akey]=='on') ? 1 : 0
                );
                $this->db->insert('enr_poll_answer', $pollAnswerSql);
            }
        }

        $this->updateDeepLink('poll', $poll_id);
        $this->insertFeed('poll', $poll_id, $post['fan_id'], $post['is_publish']);
        return true;
    }
    function getFeedPoll($id){
        $sql = "
            select *
            from enr_poll
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
    function getFeedPollQuestion($id){
        $sql = "
            select *
            from enr_poll_question
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    function deleteFeedPoll($id){
        $oFeed = $this->getFeedPoll($id);
//        print_r($oFeed);exit;
        $image_path = UPLOAD_DIR.$oFeed['image_path'];
        if(is_file($image_path)){
            unlink($image_path);
        }
        $end_image_path = UPLOAD_DIR.$oFeed['end_image_path'];
        if(is_file($end_image_path)){
            unlink($end_image_path);
        }
        $this->db->where('id', $id);
        $this->db->delete("enr_poll");

        $this->_deleteQuestionAndAnswer($id);
    }

    private function _deleteQuestionAndAnswer($poll_id){
        $sql = "select * from enr_poll_question where poll_id='{$poll_id}'";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        foreach($rows as $row){
            $this->db->where('question_id', $row['id']);
            $this->db->delete("enr_poll_answer");
        }
        $this->db->where('poll_id', $poll_id);
        $this->db->delete("enr_poll_question");
    }

    function updateFeedPoll($id){
        $oFeed = $this->getFeedPoll($id);

        $post = $this->input->post();
        if(!$post['question'][0]){
            $this->controller->data['show_errors'] = "At least one question is required.";
            return false;
        }

        if(strtotime($post['end_date']) < strtotime($post['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if($this->input->post('manual_time') != '' && strtotime($this->input->post('time_stamp')) > time()) {
            $this->controller->data['show_errors'] = "Invalid post date and time.";
            return false;
        }
        $pollSql = array(
            "cid" => $this->controller->chanel['id'],
            "description" => $post['description'],
            "text" => $post['text'],
            "tags" => $post['tags'],
            "credit" => $post['credit'],
            "end_description" => $post['end_description'],
            "is_publish" => $post['is_publish'],
            "start_date" => strtotime($post['start_date']) == 0 ? date('Y-n-j') : $post['start_date'],
            "end_date" => strtotime($post['end_date']) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $post['end_date'],
            'is_sticky' => $post['is_sticky'] == null ? 0 : $post['is_sticky']
        );
        if($this->input->post('manual_time') != '') {
            $pollSql['time_stamp'] = strtotime($this->input->post('time_stamp'));
        }

        $feed_id = $this->get_feed_by_content_id('poll', $id);
        if(!empty($feed_id) && $post['is_publish'] == 1) {
            $this->sendpush($feed_id);
        }

        if($_FILES['background_image']['size']){
            $background_image_path = UPLOAD_DIR.$oFeed['background_image_path'];
            if(is_file($background_image_path)){
                unlink($background_image_path);
            }

            $background_image_path = $this->_uploadPhoto('background_image');
            $pollSql['background_image_path'] = $background_image_path;
        }
        if($_FILES['image1']['size']){
            $image_path = UPLOAD_DIR.$oFeed['image_path'];
            if(is_file($image_path)){
                unlink($image_path);
            }

            $image_path = $this->_uploadPhoto('image1');
            $pollSql['image_path'] = $image_path;
        }
        if($_FILES['image2']['size']){
            $end_image_path = UPLOAD_DIR.$oFeed['end_image_path'];
            if(is_file($end_image_path)){
                unlink($end_image_path);
            }

            $end_image_path = $this->_uploadPhoto('image2');
            $pollSql['end_image_path'] = $end_image_path;
        }
//        print_r($post);exit;
        $this->db->where('id', $id);
        $this->db->update('enr_poll', $pollSql);

        $this->_deleteQuestionAndAnswer($id);

        foreach($post['question'] as $key => $question){
            if(!$question)
                continue;
            $pollQuestionSql = array(
                'poll_id' => $id,
                'name' => $question
            );
            $this->db->insert('enr_poll_question', $pollQuestionSql);
            $question_id = $this->db->insert_id();
            foreach($post['answer'][$key] as $akey => $answer){
                if(!$answer)
                    continue;
                $pollAnswerSql = array(
                    'question_id' => $question_id,
                    'name' => $answer,
                    'is_correct' => (isset($post["is_correct_".$key."_".$akey]) && $post["is_correct_".$key."_".$akey]=='on') ? 1 : 0
                );
                $this->db->insert('enr_poll_answer', $pollAnswerSql);
            }
        }


        $this->updateDeepLink('poll', $id);
        return true;
    }

    function getFeedPoll_question_answer($id){
        $sql = "
            select t1.* , t2.id question_id, t2.name question_name, t3.id answer_id, t3.name answer_name, t3.is_correct
            from 
                enr_poll t1 inner join enr_poll_question t2 on t1.id=t2.poll_id
                left join enr_poll_answer t3 on t2.id=t3.question_id
            where
                t1.id='{$id}'
            order by
                t2.id
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        $results = array();
        $result = array();
        $qid = 0;
        foreach($rows as $row){
            if($qid != $row['question_id']){
                if($result){
                    $results[] = $result;
                }
                $result = array();
                $result = $row;
//                $result['question_id'] = $row['question_id'];
//                $result['question_name'] = $row['question_name'];
                $result['answer'] = array();
            }
            $arr = array();
            $arr['name'] = $row['answer_name'];
            $arr['is_correct'] = $row['is_correct'];
            $arr['answer_id'] = $row['answer_id'];
            $result['answer'][] = $arr;
            $qid = $row['question_id'];
        }
        if($result){
            $results[] = $result;
        }
        return $results;
    }

    /**
     * feed quiz
     */
    function addFeedQuiz(){
        $post = $this->input->post();
        if(!$post['question'][0]){
            $this->controller->data['show_errors'] = "At least one question is required.";
            return false;
        }
        if(strtotime($post['end_date']) < strtotime($post['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        $pollSql = array(
            "cid" => $this->controller->chanel['id'],
            "fan_id" => "-1",
            "description" => $post['description'],
            "text" => $post['text'],
            "tags" => $post['tags'],
            "credit" => $post['credit'],
            "end_description" => $post['end_description'],
            "is_publish" => isset($post['is_publish']) ,
            "time_stamp" => time(),
            "start_date" => strtotime($post['start_date']) == 0 ? date('Y-n-j') : $post['start_date'],
            "end_date" => strtotime($post['end_date']) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $post['end_date'],
            'is_sticky' => $post['is_sticky'] == null ? 0 : $post['is_sticky'],
            'copy_origin_id' => isset($feed['copy_origin_id']) ? $feed['copy_origin_id'] : 0,
            'copy_timestamp' => isset($feed['copy_timestamp']) ? $feed['copy_timestamp'] : 0
        );
        if($_FILES['background_image']['size']){
            $image_path = $this->_uploadPhoto('background_image');
            $pollSql['background_image_path'] = $image_path;
        }
        if($_FILES['image1']['size']){
            $image_path = $this->_uploadPhoto('image1');
            $pollSql['image_path'] = $image_path;
        }
        if($_FILES['image2']['size']){
            $end_image_path = $this->_uploadPhoto('image2');
            $pollSql['end_image_path'] = $end_image_path;
        }
//        print_r($post);exit;
        $this->db->insert('enr_quiz', $pollSql);
        $poll_id = $this->db->insert_id();
        foreach($post['question'] as $key => $question){
            if(!$question)
                continue;
            $pollQuestionSql = array(
                'quiz_id' => $poll_id,
                'name' => $question
            );
            $this->db->insert('enr_quiz_question', $pollQuestionSql);
            $question_id = $this->db->insert_id();
            foreach($post['answer'][$key] as $akey => $answer){
                if(!$answer)
                    continue;
                $pollAnswerSql = array(
                    'question_id' => $question_id,
                    'name' => $answer,
                    'is_correct' => ($post["is_correct_".$key]  == $akey) ? 1 : 0
                );
                $this->db->insert('enr_quiz_answer', $pollAnswerSql);
            }
        }

        $this->updateDeepLink('quiz', $poll_id);
        $this->insertFeed('quiz', $poll_id, $post['fan_id'], isset($post['is_publish']));
        return true;
    }
    function getFeedQuiz($id){
        $sql = "
            select *
            from enr_quiz
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
    function getFeedQuizQuestion($id){
        $sql = "
            select *
            from enr_quiz_question
            where
                id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    function deleteFeedQuiz($id){
        $oFeed = $this->getFeedQuiz($id);
//        print_r($oFeed);exit;
        $image_path = UPLOAD_DIR.$oFeed['image_path'];
        if(is_file($image_path)){
            unlink($image_path);
        }
        $end_image_path = UPLOAD_DIR.$oFeed['end_image_path'];
        if(is_file($end_image_path)){
            unlink($end_image_path);
        }
        $this->db->where('id', $id);
        $this->db->delete("enr_quiz");

        $this->_deleteQuizQuestionAndAnswer($id);
    }

    private function _deleteQuizQuestionAndAnswer($poll_id){
        $sql = "select * from enr_quiz_question where quiz_id='{$poll_id}'";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        foreach($rows as $row){
            $this->db->where('question_id', $row['id']);
            $this->db->delete("enr_quiz_answer");
        }
        $this->db->where('quiz_id', $poll_id);
        $this->db->delete("enr_quiz_question");
    }

    function updateFeedQuiz($id){
        $oFeed = $this->getFeedQuiz($id);

        $post = $this->input->post();
        if(!$post['question'][0]){
            $this->controller->data['show_errors'] = "At least one question is required.";
            return false;
        }
        if(strtotime($post['end_date']) < strtotime($post['start_date'])) {
            $this->controller->data['show_errors'] = "Invalid start or end date.";
            return false;
        }
        if($this->input->post('manual_time') != '' && strtotime($this->input->post('time_stamp')) > time()) {
            $this->controller->data['show_errors'] = "Invalid post date and time.";
            return false;
        }
        $pollSql = array(
            "cid" => $this->controller->chanel['id'],
            "description" => $post['description'],
            "text" => $post['text'],
            "tags" => $post['tags'],
            "credit" => $post['credit'],
            "end_description" => $post['end_description'],
            "is_publish" => $post['is_publish'],
            "start_date" => strtotime($post['start_date']) == 0 ? date('Y-n-j') : $post['start_date'],
            "end_date" => strtotime($post['end_date']) == 0 ? date('Y-n-j', time() + 3600 * 24 * 30) : $post['end_date'],
            'is_sticky' => $post['is_sticky'] == null ? 0 : $post['is_sticky']
        );

        if($this->input->post('manual_time') != '') {
            $pollSql['time_stamp'] = strtotime($this->input->post('time_stamp'));
        }

        $feed_id = $this->get_feed_by_content_id('quiz', $id);
        if(!empty($feed_id) && $post['is_publish'] == 1) {
            $this->sendpush($feed_id);
        }

        if($_FILES['background_image']['size']){
            $image_path = UPLOAD_DIR.$oFeed['background_image_path'];
            if(is_file($image_path)){
                unlink($image_path);
            }

            $image_path = $this->_uploadPhoto('background_image');
            $pollSql['background_image_path'] = $image_path;
        }
        if($_FILES['image1']['size']){
            $image_path = UPLOAD_DIR.$oFeed['image_path'];
            if(is_file($image_path)){
                unlink($image_path);
            }

            $image_path = $this->_uploadPhoto('image1');
            $pollSql['image_path'] = $image_path;
        }
        if($_FILES['image2']['size']){
            $end_image_path = UPLOAD_DIR.$oFeed['end_image_path'];
            if(is_file($end_image_path)){
                unlink($end_image_path);
            }

            $end_image_path = $this->_uploadPhoto('image2');
            $pollSql['end_image_path'] = $end_image_path;
        }
//        print_r($post);exit;
        $this->db->where('id', $id);
        $this->db->update('enr_quiz', $pollSql);

        $this->_deleteQuizQuestionAndAnswer($id);

        foreach($post['question'] as $key => $question){
            if(!$question)
                continue;
            $pollQuestionSql = array(
                'quiz_id' => $id,
                'name' => $question
            );
            $this->db->insert('enr_quiz_question', $pollQuestionSql);
            $question_id = $this->db->insert_id();
            foreach($post['answer'][$key] as $akey => $answer){
                if(!$answer)
                    continue;
                $pollAnswerSql = array(
                    'question_id' => $question_id,
                    'name' => $answer,
                    'is_correct' => ($post["is_correct_".$key]  == $akey) ? 1 : 0
                );
                $this->db->insert('enr_quiz_answer', $pollAnswerSql);
            }
        }

        $this->updateDeepLink('quiz', $id);
        return true;
    }

    function getFeedQuiz_question_answer($id){
        $sql = "
            select t1.* , t2.id question_id, t2.name question_name, t3.id answer_id, t3.name answer_name, t3.is_correct
            from 
                enr_quiz t1 inner join enr_quiz_question t2 on t1.id=t2.quiz_id
                left join enr_quiz_answer t3 on t2.id=t3.question_id
            where
                t1.id='{$id}'
            order by
                t2.id
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        $results = array();
        $result = array();
        $qid = 0;
        foreach($rows as $row){
            if($qid != $row['question_id']){
                if($result){
                    $results[] = $result;
                }
                $result = array();
                $result = $row;
                $result['answer'] = array();
            }
            $arr = array();
            $arr['name'] = $row['answer_name'];
            $arr['is_correct'] = $row['is_correct'];
            $arr['answer_id'] = $row['answer_id'];
            $result['answer'][] = $arr;
            $qid = $row['question_id'];
        }
        if($result){
            $results[] = $result;
        }
        return $results;
    }
    /**
     * photo gallery
     */
    public function getPhotos($is_publish, $user_id=false, $count = -1, $page = 0){


        if($user_id){
            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
            $is_draft = 0;

            if($is_publish && $is_draft){
                $where = " and (is_publish='1' or is_publish='2')";
            }elseif($is_publish){
                $where = " and is_publish='1' ";
            }elseif($is_draft){
                $where = " and is_publish='2' ";
            }else{
                $where = " and 0 ";
            }

        }else{
            if($is_publish){
                $where = " and is_publish=1";
            }else{
                $where = "";
            }
        }
        $cid = $this->controller->chanel['id'];
        $sql = "select *, CONCAT('" . UPLOAD_CLOUD . "',destination) as destination
            from enr_photo
            where cid='{$cid}' and fan_id='-1' {$where}
            order by
                time_stamp desc
       ";
        if($count != -1) {
            $sql .= '
            limit ' . $page * $count . ', ' . $count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        return $rows;
    }
    public function getPhoto($photo_id){
        $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_photo
            where cid='{$cid}' and id='{$photo_id}'
       ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $row['destination'] = UPLOAD_CLOUD.$row['destination'];
        return $row;
    }
    /**
     * video gallery
     */
    public function getVideos($is_publish=false, $user_id=false, $count = -1, $page = 0){
        if($user_id){

            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
            $is_draft = 0;

            if($is_publish && $is_draft){
                $where = " and (is_publish='1' or is_publish='2')";
            }elseif($is_publish){
                $where = " and is_publish='1' ";
            }elseif($is_draft){
                $where = " and is_publish='2' ";
            }else{
                $where = " and 0 ";
            }

        }else{
            if($is_publish){
                $where = " and is_publish=1";
            }else{
                $where = "";
            }
        }
        $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_video
            where cid='{$cid}' and fan_id='-1' {$where}
            order by
                time_stamp desc
       ";

        if($count != -1) {
            $sql .= '
            limit ' . $page * $count . ', ' . $count;
        }

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getVideosByTags($id=false){
        if(!$id){
            $where = ' 1 ';
        }else{
            $where = " t1.tags in (select tags from enr_video where id='{$id}') ";
        }
        $cid = $this->controller->chanel['id'];
        $sql = "select t1.*
            from enr_video t1 
            where t1.cid='{$cid}' and fan_id='-1' and {$where}
            order by
                t1.time_stamp desc
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getPhotosByTags($id=false){
        if(!$id){
            $where = ' 1 ';
        }else{
            $where = " t1.tags in (select tags from enr_photo where id='{$id}') ";
        }
        $cid = $this->controller->chanel['id'];
        $sql = "select t1.*
            from enr_photo t1 
            where t1.cid='{$cid}' and t1.fan_id='-1' and {$where}
            order by
                t1.time_stamp desc
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getVideosTagOtions($cid = false){
        $rows = $this->getVideos();
        $results = array(''=>'All');
        foreach($rows as $row){
            if(!$row['tags'] || in_array($row['tags'], $results) ){
                continue;
            }
            $results[$row['id']] = $row['tags'];
        }
        return $results;
    }
    public function getPhotoTagOtions($cid = false, $is_visible=false){
        $rows = $this->getPhotos($is_visible);
        $results = array(''=>'All');
        foreach($rows as $row){
            if(!$row['tags'] || in_array($row['tags'], $results) ){
                continue;
            }
            $results[$row['id']] = $row['tags'];
        }
        return $results;
    }
    public function getShpTagOtions($cid = false){
        $rows = $this->getProductList();
        $results = array(''=>'All');
        foreach($rows as $row){
            if(!$row['tags'] || in_array($row['tags'], $results) ){
                continue;
            }
            $results[$row['id']] = $row['tags'];
        }
        return $results;
    }
    /**
     * community
     */
    public function getCommunityFeedList($is_publish=false, $user_id=false, $follow_only = false, $page = 0, $page_count = -1, $search = ''){
        if($user_id){

            $fan = $this->getFan($user_id);
            $is_publish = $fan['is_publish'];
            $is_draft = $fan['is_draft'];

            if($is_publish && $is_draft){
                $where = "  (t1.is_publish='1' or t1.is_publish='2')";
            }elseif($is_publish){
                $where = "  t1.is_publish='1' ";
            }elseif($is_draft){
                $where = "  t1.is_publish='2' ";
            }else{
                $where = "  0 ";
            }

            if($follow_only) {
                $where = " (select count(*) from enr_follow where user_id = " . $user_id . " and followed_user_id = t1.fan_id) > 0";
            }

        }else{
            if($is_publish){
                $where = " t1.is_publish=1";
            }else{
                $where = " 1 ";
            }
        }


        if(!empty($search)) {
            $where .= ' and t1.caption LIKE "%' . $search . '%"';
        }

        $sql = "select t1.*, t2.name username
            from 
                (
                    (
                        select concat('text_',id) id,'Text Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, '' image_path, fan_id, tags, description caption, time_stamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_text
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."' and fan_id>0
                    ) 
                    union
                    (
                        select concat('photo_',id) id,'Photo Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination image_path, fan_id, tags, description caption, time_stamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_photo
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."' and fan_id>0
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, fan_id, tags, description caption, time_stamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."' and fan_id>0
                    )  
                    union
                    (
                        select concat('poll_',id) id,'Poll Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, fan_id, tags, description caption, time_stamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_poll
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."' and fan_id>0
                    )  
                    union
                    (
                        select concat('quiz_',id) id,'Quiz Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, image_path image_path, fan_id, tags, description caption, time_stamp, deep_link_app, deep_link_web, deep_link_web_tw, '' as brightcove_media_id
                        from enr_quiz
                        where 
                            /* is_publish=1 and */
                            cid='".$this->chanel['id']."' and fan_id>0
                    )  
                ) t1 inner join enr_fan t2 on t1.fan_id = t2.id
            where
                {$where}
            order by t1.time_stamp desc
        ";
        if($page_count != -1) {
            $sql .= '
                    limit ' . ($page_count * $page) . ',' . $page_count;
        }

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    public function getTourdateList(){
        $cid = $this->controller->chanel['id'];
        $sql = "select *,date_format(FROM_UNIXTIME( start_time ) , '%Y-%m-%d') start_date ,date_format(FROM_UNIXTIME( end_time ) , '%Y-%m-%d') end_date
            from enr_event
            where cid='{$cid}'
            order by start_time desc, end_time desc
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getTourdateListByDate($date=false, $searchvalue=''){
        if(!$date){
            $where = ' 1 ';
        }else{
            $where = " start_time ='{$date}' ";
        }
        if($searchvalue){
            $where .= " and title like '%{$searchvalue}%' ";
        }
        $cid = $this->controller->chanel['id'];
        $sql = "select *,date_format(FROM_UNIXTIME( start_time ) , '%Y-%m-%d') start_date ,date_format(FROM_UNIXTIME( end_time ) , '%Y-%m-%d') end_date
            from enr_event  
            where cid='{$cid}' and {$where}
            order by start_time desc, end_time desc
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;


    }
    public function getEventdateOptions($cid = false){
        $rows = $this->getTourdateList();
        $results = array(''=>'All');
        foreach($rows as $row){
            if(!$row['start_time'] || in_array($row['start_time'], $results) ){
                continue;
            }
            $results[$row['start_time']] = $row['start_date'];
        }
        return $results;
    }

    public function getTourdate($id){
        $sql = "select *,date_format(FROM_UNIXTIME( start_time ) , '%Y-%m-%d') start_date ,date_format(FROM_UNIXTIME( end_time ) , '%Y-%m-%d') end_date, date_format(FROM_UNIXTIME( concert_time ) , '%Y-%m-%d') concert_date
            from enr_event
            where id='{$id}'
       ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    public function addTourdate($post){
        $concert_time = strtotime($post['concert_date']);
        $start_time = strtotime($post['start_date']);
        $end_time = strtotime($post['end_date']);
        if($start_time > $end_time){
            $this->controller->data['show_errors'][] = "End date can't be before the start date.";
            return false;
        }
        $sql = array(
            "cid" => $post['cid'],
            "title" => $post['title'],
            "description" => $post['description'],
            "position" => $post['position'],
            "concert_time" => $concert_time,
            "start_time" => $start_time,
            "end_time" => $end_time,
            "price" => $post['price'],
            "url" => $post['url'],
        );
        $this->db->insert("enr_event", $sql);
        return true;
    }

    public function updateTourdate($post){
        $concert_time = strtotime($post['concert_date']);
        $start_time = strtotime($post['start_date']);
        $end_time = strtotime($post['end_date']);
        if($start_time > $end_time){
            $this->controller->data['show_errors'][] = "End date can't be before the start date.";
            return false;
        }
        $sql = array(
            "cid" => $post['cid'],
            "title" => $post['title'],
            "position" => $post['position'],
            "description" => $post['description'],
            "concert_time" => $concert_time,
            "start_time" => $start_time,
            "end_time" => $end_time,
            "price" => $post['price'],
            "url" => $post['url'],
        );
        $this->db->where("id", $post['id']);
        $this->db->update("enr_event", $sql);
        return true;
    }

    public function getMusicList(){
        $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_music
            where cid='{$cid}' and is_publish = 1
            order by time_stamp desc
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    public function getMusic($id){
        $sql = "select *
            from enr_music
            where id='{$id}'
       ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }

    public function addMusic($post){
        if($filepath = $this->_uploadPhoto()){
            $post = array(
                'cid' => $post['cid'],
                'fan_id' => "-1",
                'title' => $post['title'],
                'album' => $post['album'],
                'destination' => $_POST['destination'],
                'music_size' => $_POST['music_size'],
                'url' => $post['url'],
                'url1' => $post['url1'],
                'image_path' => $filepath,
                'time_stamp' => time(),
                'is_publish' => $post['is_publish']
            );
            $res = $this->db->insert("enr_music", $post);
            return $res;
        }
        return false;
    }

    public function updateMusic($post){
        $id = $post['id'];
        $music = $this->getMusic($id);
        if($_FILES['image']['size']){
            $destination = UPLOAD_DIR.$music['image_path'];
            if(is_file($destination)){
                unlink($destination);
            }
            $filepath = $this->_uploadPhoto();
            if(!$filepath){
                return false;
            }
        }else{
            $filepath = $music['image_path'];
        }
        $post = array(
            'cid' => $post['cid'],
            'title' => $post['title'],
            'album' => $post['album'],
            'destination' => $_POST['destination'],
            'music_size' => $_POST['music_size'],
            'url' => $post['url'],
            'url1' => $post['url1'],
            'image_path' => $filepath,
            'time_stamp' => time(),
            'is_publish' => $post['is_publish']
        );
        $this->db->where("id", $id);
        $res = $this->db->update("enr_music", $post);
        return $res;
    }
    /**
     * category
     */
    public function getCategoryList($id = false){
//         $id = $this->controller->chanel['id'];
        if ($id === false){
      
            $sql = "select *
                from viral_media_category
               
            ";
            $query = $this->db->query($sql);
            $rows = $query->result_array();
            return $rows;
        }else{
            $sql = "select *
                from viral_media_category
               
            ";
            $query = $this->db->query($sql);

            $row = $query->row_array();
            return $row;
        }
    }
    public function addCategory($post){
       // $id = $this->controller->chanel['id'];
        $sql = array(
            "id" => $id,
            "cat_name" => $post['cat_name']
        );
        $this->db->insert("viral_media_category", $sql);
        return true;
    }

    public function updateCategory($post){
        //$id = $this->controller->chanel['id'];
        $sql = array(
            "id" => $id,
            "cat_name" => $post['cat_name']
        );
        $this->db->where("id", $post['id']);
        $this->db->update("viral_media_category", $sql);
        return true;
    }

    public function deleteCategory($id){
        $sql = "select * from viral_media_category where id='{$id}'";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        foreach($rows as $row){
            $this->db->where('id', $row['id']);
            $this->db->delete("cat_name");}
                }
    
    
    /**
     * color
     */
    public function getColorList($id = false){
        $cid = $this->controller->chanel['id'];
        if ($id === false){
            $sql = "select *
                from enr_color
                where cid='{$cid}'
            ";
            $query = $this->db->query($sql);
            $rows = $query->result_array();
            return $rows;
        }else{
            $sql = "select *
                from enr_color
                where id='{$id}'
            ";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row;
        }
    }
    public function addColor($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            "name" => $post['name']
        );
        $this->db->insert("enr_color", $sql);
        return true;
    }

    public function updateColor($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            "name" => $post['name']
        );
        $this->db->where("id", $post['id']);
        $this->db->update("enr_color", $sql);
        return true;
    }
    /**
     * Size
     */
    public function getSizeList($id = false){
        $cid = $this->controller->chanel['id'];
        if ($id === false){
            $sql = "select *
                from enr_size
                where cid='{$cid}'
            ";
            $query = $this->db->query($sql);
            $rows = $query->result_array();
            return $rows;
        }else{
            $sql = "select *
                from enr_size
                where id='{$id}'
            ";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row;
        }
    }
    public function addSize($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            "name" => $post['name']
        );
        $this->db->insert("enr_size", $sql);
        return true;
    }

    public function updateSize($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            "name" => $post['name']
        );
        $this->db->where("id", $post['id']);
        $this->db->update("enr_size", $sql);
        return true;
    }
    /**
     * attribute
     */
    public function getCategoryOptions($cid = false){
        $cid===false && $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_category
            where cid='{$cid}'
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $results = array();
        foreach($rows as $row){
            $results[$row['name']] = $row['name'];
        }
        return $results;
    }
    public function getColorOptions($cid = false){
        $cid===false && $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_color
            where cid='{$cid}'
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $results = array();
        foreach($rows as $row){
            $results[$row['name']] = $row['name'];
        }
        return $results;
    }
    public function getSizeOptions($cid = false){
        $cid===false && $cid = $this->controller->chanel['id'];
        $sql = "select *
            from enr_size
            where cid='{$cid}'
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $results = array();
        foreach($rows as $row){
            $results[$row['name']] = $row['name'];
        }
        return $results;
    }
    /**
     * Product
     */
    public function getProductList($id = false){
        $cid = $this->controller->chanel['id'];
        if ($id === false){
            $sql = "select *
                from enr_product
                where cid='{$cid}'
            ";
            $query = $this->db->query($sql);
            $rows = $query->result_array();
            return $rows;
        }else{
            $sql = "select *
                from enr_product
                where id='{$id}'
            ";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row;
        }
    }
    public function addProduct($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            'title' => $post['title'],
            'category' => $post['category'],
            'color' => $post['color'],
            'size' => $post['size'],
            'tags' => $post['tags'],
            'price' => $post['price'],
            'is_publish' => $post['is_publish'],
            'images' => serialize($post['images'])
        );
        $this->db->insert("enr_product", $sql);
        return true;
    }

    public function updateProduct($post){
        $cid = $this->controller->chanel['id'];
        $sql = array(
            "cid" => $cid,
            'title' => $post['title'],
            'category' => $post['category'],
            'color' => $post['color'],
            'size' => $post['size'],
            'tags' => $post['tags'],
            'price' => $post['price'],
            'is_publish' => $post['is_publish'],
            'images' => serialize($post['images'])
        );
        $this->db->where("id", $post['id']);
        $this->db->update("enr_product", $sql);
        return true;
    }
    public function uploadTempVideo(){
        $path = $this->_uploadVideo('video');
        return $path;
    }
    public function uploadTempMusic(){
        $path = $this->_uploadMusic('music');
        return $path;
    }
    public function uploadTempPhoto(){
        $path = $this->_uploadPhoto('image');
        return $path;
    }
    public function getFans($conds=false, $orders=false, $page = 0, $page_count = -1, $name = ''){
        $orderby = array();
        if($orders !== false){
            if($orders['lastlogin'] !== false){
                $orderby[] = " last_login {$orders['lastlogin']} ";
            }
            if($orders['joindate'] !== false){
                $orderby[] = " joined {$orders['joindate']} DESC";
            }
            if($orders['joindate_desc'] !== false){
                $orderby[] = " joined {$orders['joindate']} DESC ";
            }
            if($orders['lastpost'] !== false){
                $orderby[] = " lastpost {$orders['lastpost']} ";
            }
            if($orderby){
                $orderby = implode(",", $orderby);
                $orderby = " order by {$orderby} ";
            }else{
                $orderby = "";
            }
        }else{
            $orderby = "";
        }
        if(!$conds){
            $where = "";
        }else{
            $where = "";
            foreach($conds as $key=>$value){
                $where .= " and {$key}='{$value}'";
            }
        }
        if(!empty($name)) {
            $where .= ' and name like "%' . $name . '%"';
        }
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *,date_format(FROM_UNIXTIME( last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( joined ) , '%Y-%m-%d') join_date
            from enr_fan
            where cid='{$cid}' {$where}
            {$orderby}
        ";
        if($page_count != -1) {
            $sql .= '
                    limit ' . ($page_count * $page) . ',' . $page_count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }



    public function getFansNew($conds=false, $orders=false, $page = 0, $page_count = -1, $name = ''){
        $orderby = array();
        if($orders !== false){
            if($orders['lastlogin'] !== false){
                $orderby[] = " last_login {$orders['lastlogin']} ";
            }
            if($orders['joindate'] !== false){
                $orderby[] = " joined {$orders['joindate']} ";
            }
            if($orders['lastpost'] !== false){
                $orderby[] = " lastpost {$orders['lastpost']} ";
            }
            if($orderby){
                $orderby = implode(",", $orderby);
                $orderby = " order by {$orderby} ";
            }else{
                $orderby = "";
            }
        }else{
            $orderby = "";
        }
        if(!$conds){
            $where = "";
        }else{
            $where = "";
            foreach($conds as $key=>$value){
                $where .= " and {$key}='{$value}'";
            }
        }
        if(!empty($name)) {
            $where .= ' and name like "%' . $name . '%"';
        }
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *, date_format(FROM_UNIXTIME( joined ) , '%Y-%m-%d') join_date
            from enr_fan
            where cid='{$cid}' {$where}
            {$orderby}
        ";
        if($page_count != -1) {
            $sql .= '
                    limit ' . ($page_count * $page) . ',' . $page_count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $sql;
    }



    public function getFan($id){
        $sql = "
            select *,date_format(FROM_UNIXTIME( last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( joined ) , '%Y-%m-%d') join_date
            from enr_fan
            where id='{$id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
    public function getDeactiveFans(){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *,date_format(FROM_UNIXTIME( last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( joined ) , '%Y-%m-%d') join_date
            from enr_fan
            where cid='{$cid}' and (is_block=1 or is_suspend=1)
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getActiveFans(){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *,date_format(FROM_UNIXTIME( last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( joined ) , '%Y-%m-%d') join_date
            from enr_fan
            where cid='{$cid}' and (is_block=0 or is_suspend=0)
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    /**
     * Get Channel Users.
     * 
     * @param bool $conds
     * @return mixed
     */
    public function getUsers($conds=false){
        if(!$conds){
            $where = "";
        }else{
            $where = "";
            foreach($conds as $key=>$value){
                $where .= " and {$key}='{$value}'";
            }
        }
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *
            from users
            where cid='{$cid}' {$where}
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    public function getDeactiveUsers(){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *
            from users
            where cid='{$cid}' and (active=0 or suspend=1) and fan=1
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    public function checkFanUser($user_id){
        $sql = "
            select *
            from enr_fan
            where id='{$user_id}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        if($row && $row['is_block'] == 0 && $row['is_suspend'] == 0){
            $res = true;
        }else{
            $res = false;
        }
        return $res;
    }

    public function getChanelOptions(){
        $chanels = $this->getChanels();
        $result = array();
        foreach($chanels as $chanel){
            $result[$chanel->id] = $chanel->name;
            $result[id] = $chanel->id;
        }
        return $result;
    }

    public function getDefaultSections(){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *
            from enr_section
            where 
                id not in (select section_id from enr_submenu where cid='{$cid}')
            order by 
                ordering
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }

    public function getSubmenuInfo($id){
        $id = $this->db->escape_str($id);
        $cid = $this->controller->chanel['id'];
        $strSql = "
            SELECT t1.*, if(t1.alter_name<>'',t1.alter_name,t2.name) alter_name
            FROM enr_submenu t1 inner join enr_section t2 on t1.section_id=t2.id
            where 
                t1.id={$id} 
        ";
        $query = $this->db->query($strSql);
        $row = $query->row_array();
        return $row;
    }
    public function _reOrderingSubmenu($id, $ordering){
        if(!$id)
            return false;
        $cid = $this->controller->chanel['id'];
        $sql = "
            select *
            from enr_submenu
            where 
                cid='{$cid}'
            order by 
                ordering
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $i = 1;
        foreach($rows as $key=>$row){
            if($id == $row['id'])
                continue;
            if($ordering == $i)
                $i++;
            $this->db->where('id', $row['id']);
            $this->db->update('enr_submenu', array('ordering'=>$i));
            $i++;
        }

        $this->db->where('id', $id);
        !$ordering && $ordering=count($rows);
        $this->db->update('enr_submenu', array('ordering'=>$ordering));
        return true;

    }

    public function getDesignInfo($cid = false){
        if($cid === false)
            $cid = $this->controller->chanel['id'];
        $strSql = "
            SELECT *
            FROM enr_design 
            where 
                cid='{$cid}'
        ";
        $query = $this->db->query($strSql);
        $row = $query->row_array();

        $url = $this->controller->chanel['url//'];
//        echo "<BR><BR><BR>\$url: ".$url;
//
//        $l_sMemcacheKey = Memcaching::getInstance()->getKey($url);
//        echo "<BR><BR><BR>MEMCACHE_KEY_PREPEND.\$l_sMemcacheKey.MEMCACHE_KEY_CACHE_RELEASE: ";
//        var_dump(MEMCACHE_KEY_PREPEND.$l_sMemcacheKey.MEMCACHE_KEY_CACHE_RELEASE);

        return $row;
    }

    public function saveDesignInfo($infos){
        $cid = $this->controller->chanel['id'];
        $url = $this->controller->chanel['url'];

        $sql = array(
            'cid' => $cid,
            'link_color' => ($infos['link_color']) ? $infos['link_color'] : '000000',
            'button_color' => ($infos['button_color']) ? $infos['button_color'] : '000000',
            'back_color' => ($infos['back_color']) ? $infos['back_color'] : 'FFFFFF',
            'text_color' => ($infos['text_color']) ? $infos['text_color'] : '000000',
            'bio_link' => ($infos['bio_link']) ? $infos['bio_link'] : '',
            'site_link' => ($infos['site_link']) ? $infos['site_link'] : '',
            'twitter_link' => ($infos['twitter_link']) ? $infos['twitter_link'] : '',
            'facebook_link' => ($infos['facebook_link']) ? $infos['facebook_link'] : '',
            'instagram_link' => ($infos['instagram_link']) ? $infos['instagram_link'] : '',
            'channel_description' => ($infos['channel_description']) ? $infos['channel_description'] : '',
            'channel_type' => ($infos['channel_type']) ? $infos['channel_type'] : ''
        );
        $row = $this->getDesignInfo($cid);
        if($row){
            $this->db->where('cid', $cid);
            $this->db->update('enr_design', $sql);
            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);


        }else{
            $this->db->insert('enr_design', $sql);
            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        }

        // Logo for title bar - header_img
        if(isset($_FILES['image1']) && $_FILES['image1']['size'] && $path = $this->_uploadCropedPhoto('image1')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image1'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."ef-logo.png";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreate($cw, $ch);

            $colourBlack = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $colourBlack);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

            $filepath = str_replace(UPLOAD_DIR,"",$filepath);
            $header_image = UPLOAD_DIR.$filepath;
            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('header_image'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        }

        // Watermark - for photos
        if(isset($_FILES['image2']) && $_FILES['image2']['size'] && $path = $this->_uploadCropedPhoto('image2')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image2'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."watermark.png";
            $watermark = $file;

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreate($cw, $ch);

            $colourBlack = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $colourBlack);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

            $filepath = str_replace(UPLOAD_DIR,"",$filepath);

            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('watermark'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }

        // Background image
        if(isset($_FILES['image3']) && $_FILES['image3']['size'] && $path = $this->_uploadCropedPhoto('image3')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image3'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."bg-sn.jpg";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreatetruecolor($cw, $ch);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagejpeg($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

//            $filepath = str_replace(UPLOAD_DIR,"",$filepath);
            $background_image = UPLOAD_DIR.$filepath;
            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('background_image'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }

        // Logo Image
        if(isset($_FILES['image4']) && $_FILES['image4']['size'] && $path = $this->_uploadCropedPhoto('image4')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image4'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."logo.png";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo2_width']) ? $_REQUEST['photo2_width'] : $ow;
            $rh = isset($_REQUEST['photo2_height']) ? $_REQUEST['photo2_height'] : $oh;
            $x1 = isset($_REQUEST['photo2_crop_x1']) ? $_REQUEST['photo2_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo2_crop_y1']) ? $_REQUEST['photo2_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo2_crop_x2']) ? $_REQUEST['photo2_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo2_crop_y2']) ? $_REQUEST['photo2_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo2_crop_w']) ? $_REQUEST['photo2_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo2_crop_h']) ? $_REQUEST['photo2_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreate($cw, $ch);

            $colourBlack = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $colourBlack);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

            $filepath = str_replace(UPLOAD_DIR,"",$filepath);
            $logo_image = UPLOAD_DIR.$filepath;
            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('logo_image'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }

        // Banner Image
        if(isset($_FILES['image5']) && $_FILES['image5']['size'] && $path = $this->_uploadCropedPhoto('image5')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image5'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."banner.png";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreatetruecolor($cw, $ch);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);



            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

//            $filepath = str_replace(UPLOAD_DIR,"",$filepath);
            $background_image = UPLOAD_DIR.$filepath;
            $path = UPLOAD_DIR.$filepath;


            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('banner_image'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);


            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }

        // Brightcove
        if(isset($_FILES['image6']) && $_FILES['image6']['size'] && $path = $this->_uploadCropedPhoto('image6')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image6'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."brightcove.png";
            $brightcove = "sdhad";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreate($cw, $ch);

            $colourBlack = imagecolorallocate($dest, 0, 0, 0);
            imagecolortransparent($dest, $colourBlack);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagepng($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

            $filepath = str_replace(UPLOAD_DIR,"",$filepath);
            $watermark = UPLOAD_CLOUD.'watermark.png';
            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('brightcove'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }

        // Channel Icon
        if(isset($_FILES['image7']) && $_FILES['image7']['size'] && $path = $this->_uploadCropedPhoto('image7')){

            $year = date('Y');
            $month = date('m');
            $fileinfo = $_FILES['image7'];
            if(!$fileinfo['size']){
                $this->controller->data['show_errors'][] = "Please select a photo file.";
                return false;
            }
            $ext = strtolower(pathinfo($fileinfo['name'], PATHINFO_EXTENSION));
            if(stripos("jpg, png, bmp, jpeg, gif", $ext) === false){
                $this->controller->data['show_errors'][] = "Only jpg, png, gif, bmp files can be uploaded.";
                return false;
            }
            $filename = md5(time().rand(0.1, 99.9));
            $filepath = UPLOAD_DIR."photo/".$year;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$month;

            if(!is_dir($filepath)){
                mkdir($filepath, 0777, true);
            }

            $filepath .= "/".$filename.".".$ext;

            $file = THEME_AMZ."chanel.jpg";

            $source = null;
            switch($ext) {
                case 'jpg' : case 'jpeg': $source = imagecreatefromjpeg($fileinfo['tmp_name']); break;
                case 'png' : $source = imagecreatefrompng($fileinfo['tmp_name']); break;
                case 'bmp' : $source = imagecreatefromwbmp($fileinfo['tmp_name']); break;
                case 'gif' : $source = imagecreatefromgif($fileinfo['tmp_name']); break;
            }

            list($ow, $oh) = getimagesize($fileinfo['tmp_name']);
            $rw = isset($_REQUEST['photo_width']) ? $_REQUEST['photo_width'] : $ow;
            $rh = isset($_REQUEST['photo_height']) ? $_REQUEST['photo_height'] : $oh;
            $x1 = isset($_REQUEST['photo_crop_x1']) ? $_REQUEST['photo_crop_x1'] : 0;
            $y1 = isset($_REQUEST['photo_crop_y1']) ? $_REQUEST['photo_crop_y1'] : 0;
            $x2 = isset($_REQUEST['photo_crop_x2']) ? $_REQUEST['photo_crop_x2'] : $rw;
            $y2 = isset($_REQUEST['photo_crop_y2']) ? $_REQUEST['photo_crop_y2'] : $rh;
            $cw = isset($_REQUEST['photo_crop_w']) ? $_REQUEST['photo_crop_w'] : $rw;
            $ch = isset($_REQUEST['photo_crop_h']) ? $_REQUEST['photo_crop_h'] : $rh;

            $x1 = $x1 * $ow / $rw; $y1 = $y1 * $oh / $rh;
            $x2 = $x2 * $ow / $rw; $y2 = $y2 * $oh / $rh;
            $cw = $cw * $ow / $rw; $ch = $ch * $oh / $rh;

            if($cw == 0 || $ch == 0) {
                $cw = $ow;
                $ch = $oh;
            }

            $dest = imagecreatetruecolor($cw, $ch);

            imagecopyresampled($dest, $source, 0, 0, $x1, $y1, $cw, $ch, $cw, $ch);

            imagejpeg($dest, $filepath);

            imagedestroy($source);
            imagedestroy($dest);

            $l_sNewAbsoluteFilePath = $filepath;
            $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file);

            $chanel_icon = UPLOAD_DIR.$filepath;
            $path = UPLOAD_DIR.$filepath;

            $this->db->where('cid', $cid);
            $this->db->update('enr_design', array('chanel_icon'=>$path));

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);
        }


        if($watermark!=""){

            $sql = "select destination, id from enr_photo where fan_id = '-1'";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            $rows = $query->result_array();

            $results = array();
            $i=0;

            $paths="";
            foreach($rows as $row){
                $results[$row['destination']] = UPLOAD_CLOUD."watermark/".$row['destination'];
                $results[$row['destination_thumb']] = UPLOAD_CLOUD."watermark/thumb/".$row['destination'];
                $results[$row['destination_small_thumb']] = UPLOAD_CLOUD."watermark/small_thumb/".$row['destination'];
                $results[$row['og_destination']] = UPLOAD_CLOUD.$row['destination'];
                $results[$row['og_destination_thumb']] = UPLOAD_CLOUD."thumb/".$row['destination'];
                $results[$row['og_destination_small_thumb']] = UPLOAD_CLOUD."small_thumb/".$row['destination'];
                $id = $row['id'];

                $file = UPLOAD_CLOUD.$row['destination'];
                $filepath = UPLOAD_DIR.$row['destination'];
                if(file_exists($filepath)){
                    chmod($filepath,0777);
                    unlink($filepath);
                }

                $content = file_get_contents($file);

                $fp = fopen($filepath, "w");
                fwrite($fp, $content);
                fclose($fp);

                $file_thumb = UPLOAD_CLOUD."thumb/".$row['destination'];
                $filepath_thumb = UPLOAD_DIR."thumb/".$row['destination'];

                if(file_exists($filepath_thumb)){
                    chmod($filepath_thumb,0777);
                    unlink($filepath_thumb);
                }
                $test = explode('/', $filepath_thumb);
                $year_folder = UPLOAD_DIR."thumb/photo/".$test[9];

                if(!is_dir($year_folder)){
                    mkdir($year_folder, 0777, true);
                }

                $month_folder = UPLOAD_DIR."thumb/photo/".$test[9]."/".$test[10];

                if(!is_dir($month_folder)){
                    mkdir($month_folder, 0777, true);
                }

                $content = file_get_contents($file_thumb);

                $fp = fopen($filepath_thumb, "w");
                fwrite($fp, $content);
                fclose($fp);

                $file_small_thumb = UPLOAD_CLOUD."small_thumb/".$row['destination'];
                $filepath_small_thumb = UPLOAD_DIR."small_thumb/".$row['destination'];

                if(file_exists($filepath_small_thumb)){
                    chmod($filepath_small_thumb,0777);
                    unlink($filepath_small_thumb);
                }

                $test = explode('/', $filepath_small_thumb);
                $year_folder = UPLOAD_DIR."small_thumb/photo/".$test[9];

                if(!is_dir($year_folder)){
                    mkdir($year_folder, 0777, true);
                }

                $month_folder = UPLOAD_DIR."small_thumb/photo/".$test[9]."/".$test[10];

                if(!is_dir($month_folder)){
                    mkdir($month_folder, 0777, true);
                }

                $content = file_get_contents($file_small_thumb);

                $fp = fopen($filepath_small_thumb, "w");
                fwrite($fp, $content);
                fclose($fp);

                list($sw, $sh) = getimagesize($filepath);

                //calculate simulated size
                $width = $this->uri->segment(5);
                if(empty($width) || $width == NULL) { $width = 640; }
                {
                    $height = round($sh * $width / $sw);
                }

                $cid = $this->getChanelOptions();

                $designinfo = $this->getDesignInfo($cid[id]);
                $local_watermark = THEME_CLOUD."watermark.png?v=".md5(rand());
                $watermark = UPLOAD_DIR."watermark.png";
                $content = file_get_contents($local_watermark);

                $fp = fopen($watermark, "w");
                fwrite($fp, $content);
                fclose($fp);

                //Add new watermark to all photos
                list($sww, $swh) = getimagesize($watermark);
                //resized watermark size
                $ww = round($width / 3);
                $wh = round($swh * $ww / $sww);

                //load original image
                $ext = exif_imagetype ($filepath);
                $hSource = NULL;
                if($ext == IMAGETYPE_PNG) $hSource = imagecreatefrompng ($filepath);
                elseif($ext == IMAGETYPE_JPEG) $hSource = imagecreatefromjpeg ($filepath);
                elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hSource = imagecreatefromwbmp ($filepath);
                elseif($ext == IMAGETYPE_GIF) $hSource = imagecreatefromgif ($filepath);
                //load watermark image
                $ext = exif_imagetype($watermark);
                $hWatermark = NULL;
                if($ext == IMAGETYPE_PNG) $hWatermark = imagecreatefrompng ($watermark);
                elseif($ext == IMAGETYPE_JPEG) $hWatermark = imagecreatefromjpeg ($watermark);
                elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hWatermark = imagecreatefromwbmp ($watermark);
                elseif($ext == IMAGETYPE_GIF) $hWatermark = imagecreatefromgif ($watermark);


                $hDest = imagecreatetruecolor($width, $height);
                imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $width, $height, $sw, $sh);
                imagecopyresampled($hDest, $hWatermark, $width - $ww, $height - $wh, 0, 0, $ww, $wh, $sww, $swh);

                $filemain = str_replace(UPLOAD_DIR,"",$filepath);


                imagejpeg($hDest,UPLOAD_DIR.$filemain, 100);

                $watermarked_file = UPLOAD_DIR.$filemain;
                $watermarked_cloud_file = UPLOAD_AMZ."watermark/".$filemain;
                $watermarked_cloud_file_thumb = UPLOAD_AMZ."watermark/thumb/".$filemain;
                $watermarked_cloud_file_small_thumb = UPLOAD_AMZ."watermark/small_thumb/".$filemain;


                $l_sNewAbsoluteFilePath = $watermarked_file;

                $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file);


                //Add new watermark to all thumbs
                $src = imagecreatefromjpeg($watermarked_file);

                list($width,$height)=getimagesize($watermarked_file);

                $newwidth=360;
                $newheight=($height/$width)*360;
                $tmp=imagecreatetruecolor($newwidth,$newheight);

                imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                imagejpeg($tmp,$filepath_thumb,100);

                imagedestroy($src);
                imagedestroy($tmp);

                $l_sNewAbsoluteFilePath = $filepath_thumb;

                $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_thumb);


                //Add new watermark to all small thumbs
                $src = imagecreatefromjpeg($watermarked_file);

                list($width,$height)=getimagesize($watermarked_file);

                $newwidth=180;
                $newheight=($height/$width)*180;
                $tmp=imagecreatetruecolor($newwidth,$newheight);

                imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                imagejpeg($tmp,$filepath_small_thumb,100);

                imagedestroy($src);
                imagedestroy($tmp);

                $l_sNewAbsoluteFilePath = $filepath_small_thumb;

                $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$watermarked_cloud_file_small_thumb);


                imagedestroy($hDest);

                $i++;

            }

            $l_sMemcacheKey = Memcaching::makeKey($url);

            Memcaching::getInstance()->removeKey($l_sMemcacheKey);

        }

        @unlink($filepath);
        @unlink($filepath_thumb);
        @unlink($filepath_small_thumb);
//        @unlink($filepath_big_thumb);

        return true;
    }

    /**
     * Add Facebook Group Pages.
     *
     * @param $channel_id
     * @param $facebook_accts
     * @return mixed
     */
    public function add_Facebook_Accounts($channel_id, $facebook_accts){
        $sspfb_id = SOCIAL_SHARING_PROPERTY_FACEBOOK_ID;
        $count = count($facebook_accts);

        // User/Admin page
        $sql = "INSERT INTO enr_channel_oauth (channel_id, social_property_type_id, social_token, page_category, page_name, page_id, created, active)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE social_token = ?, modified = ?";

        $facebook_accts[0]['result'] = $this->db->query($sql, array($channel_id, $sspfb_id, $facebook_accts[0]['access_token'], "User", $facebook_accts[0]['name'], $facebook_accts[0]['id'], date('Y-n-j H:i:s'), 'N', $facebook_accts[0]['access_token'], date('Y-n-j H:i:s')));
        $facebook_accts[0]['insert_id'] = $this->db->insert_id();


        // Loop through admin's/user's group pages
        for ($i = 1; $i < $count; $i++){

            $sql = "INSERT INTO enr_channel_oauth (channel_id, social_property_type_id, social_token, page_category, page_name, page_id, created, active)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE social_token = ?, modified = ?";

            $facebook_accts[$i]['result'] = $this->db->query($sql, array($channel_id, $sspfb_id, $facebook_accts[$i]['access_token'], $facebook_accts[$i]['category'], $facebook_accts[$i]['name'], $facebook_accts[$i]['id'], date('Y-n-j H:i:s'), 'N', $facebook_accts[$i]['access_token'], date('Y-n-j H:i:s')));
            $facebook_accts[$i]['insert_id'] = $this->db->insert_id();

        }

        return $facebook_accts;

    }


    /**
     * Delete Facebook account (oauth).
     *
     * @param $facebook_id
     * @return mixed
     */
    public function delete_facebook_account($channel_id, $facebook_id){
        $this->db->where('channel_id', $channel_id);
        $this->db->where('id', $facebook_id);
        $result = $this->db->delete('enr_channel_oauth');

        return $result;
    }

// public function connect_to_twitter ($channel_id, $connect_to_twitter){
// 
//        
//         foreach($facebook_group_pages as $group_page){
// 
//             $sql = "INSERT INTO enr_channel_oauth (channel_id, social_property_type_id, social_token, page_category, page_name, page_id, created, active)
//                     VALUES (?, ?, ?, ?, ?, ?, ?, ?)
//                     ON DUPLICATE KEY UPDATE social_token = ?, modified = ?";
// 
//             $res = $this->db->query($sql, array($channel_id, 1, $group_page['access_token'], $group_page['category'], $group_page['name'], $group_page['id'], date('Y-n-j H:i:s'), 'N', $group_page['access_token'], date('Y-n-j H:i:s')));
// 
//         }
// 
//         return $res;
// 
//     }

    public function add_twitter_accounts($channel_id, $connect_to_twitter){

        $res = array();

         $l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,social_token,social_token_secret,created,active) 
                        VALUES ('.$channel_id.',
                           "'.SOCIAL_SHARING_PROPERTY_TWITTER_ID.'",
                           "'.$connect_to_twitter['user_id'].'",
                           "'.$connect_to_twitter['screen_name'].'",
                            "'.$connect_to_twitter['oauth_token'].'",
                           "'.$connect_to_twitter['oauth_token_secret'].'",
                            NOW(),
                            "Y"
                            )
                            ON DUPLICATE KEY UPDATE page_name = VALUES(page_name), social_token = VALUES(social_token), social_token_secret = VALUES(social_token_secret), active = VALUES(active), modified = NOW() ';   

              
             
             $res[] = $this->db->query($l_sSql);
         return $res;
            }


             
                
            
    

     
    public function list_all_social_media($cid){
        $chid = $cid;
        

    }

    public function list_facebook_accounts($cid){
        $chid = $cid;

        $SCAPI = new SCAPI();

        $pairingID = $SCAPI->returnCurrentPairingForChannelId($chid);


        return $pairingID;
    }

    public function is_connected_to_facebook($pairingID){

    }

     public function cat_post_add($cat_id, $id, $content_type_id){
        $cid = $this->controller->chanel['id'];
        $insert = "INSERT IGNORE INTO viral_media_cat_post (cat_id, post_id, content_type_id)
                VALUES($cat_id, $id, $content_type_id)";
        $this->db->query($insert);
    }

    public function cat_post_delete($cat_id, $id){
        $cid = $this->controller->chanel['id'];
        $delete = "DELETE FROM viral_media_cat_post 
                    WHERE cat_id=$cat_id && post_id=$id";
        $this->db->query($delete);
    }


   // ** PRIORITY ** //

     public function get_priority(){

         $sql = "SELECT *
                FROM priority";

         $query = $this->db->query($sql);
         $rows = $query->result_array();

          return $rows;

      }


     public function get_priority_id($data){

     $this->db->select('*');
     $this->db->from('priority');
     $this->db->where('content_id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
     
     }

     public function update_priority($id, $data){
     $this->db->where('content_id', $id);
     $result = $this->db->update('priority', $data);

     return $result;
    }

 

    public function facebook_pair_update($id,$access){
        $cid = $this->controller->chanel['id'];
        $this->db->where('id', $id);
        $this->db->update('enr_channel_oauth', array('active'=>$access));
    }

    public function twitter_pair_update($id,$access){
        $cid = $this->controller->chanel['id'];
        $this->db->where('id', $id);
        $this->db->update('enr_channel_oauth', array('active'=>$access));
    }

    public function tumblr_pair_update($id,$access){
        $cid = $this->controller->chanel['id'];
        $this->db->where('id', $id);
        $this->db->update('enr_channel_oauth', array('active'=>$access));
    }

    public function delete_header_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['header_image'])){
            unlink(UPLOAD_DIR.$row['header_image']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('header_image'=>''));
    }

    public function delete_background_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['background_image'])){
            unlink(UPLOAD_DIR.$row['background_image']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('background_image'=>''));
    }

    public function delete_logo_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['logo_image'])){
            unlink(UPLOAD_DIR.$row['logo_image']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('logo_image'=>''));
    }

    public function delete_banner_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['banner_image'])){
            unlink(UPLOAD_DIR.$row['banner_image']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('logo_image'=>''));
    }

    public function delete_watermark_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['watermark'])){
            unlink(UPLOAD_DIR.$row['watermark']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('watermark'=>''));
    }

    public function delete_brightcove_image(){
        $cid = $this->controller->chanel['id'];
        $row = $this->getDesignInfo();
        if(is_file(UPLOAD_DIR.$row['brightcove'])){
            unlink(UPLOAD_DIR.$row['brightcove']);
        }
        $this->db->where('cid', $cid);
        $this->db->update('enr_design', array('brightcove'=>''));
    }

    public function getCurrencyInfo(){
        $cid = $this->controller->chanel['id'];
        $strSql = "
            SELECT *
            FROM enr_currency 
            where 
                cid='{$cid}'
            order by
                id
        ";
        $query = $this->db->query($strSql);
        $rows = $query->result_array();
        return $rows;
    }
    public function saveCurrencyInfo($post){
        $cid = $this->controller->chanel['id'];
        $this->db->where('cid', $cid);
        $this->db->delete('enr_currency');
        foreach($post['package'] as $key=>$package){
            $sql = array(
                'cid' => $cid,
                'package' => $package,
                'cost' => $post['cost'][$key],
                'credit' => $post['credit'][$key]
            );
            $this->db->insert('enr_currency', $sql);
        }
        return true;
    }

    public function getSocial($cid){
        $sql = "
            select *
            from  enr_social
            where 
                cid='{$cid}'
        ";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        if(count($row) == null) return null;
        return $row[0];
    }
    public function saveSocial($cid, $social) {
        $cur = $this->getSocial($cid);
        if($cur == null) {
            $social['cid'] = $cid;
            $this->db->insert('enr_social', $social);
        }
        else {
            $this->db->where('id', $cur['id']);
            $this->db->update('enr_social', $social);
        }
    }

    public function getSocialAccount($social_type, $cid){
        $sql = "
            select *
            from enr_social_account
            where
                social_type = '{$social_type}' and cid='{$cid}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
    public function updateSocialAccount($post){

        $this->db->where('cid', $post['cid']);
        $this->db->where('social_type', $post['social_type']);
        $this->db->delete('enr_social_account');
        $sql = array(
            'cid' => $post['cid'],
            'social_type' => $post['social_type'],
            'username' => $post['username'],
            'password' => $post['password']
        );
        $this->db->insert('enr_social_account', $sql);
        return true;
    }

    public function getStatsPost($start_time, $end_time, $cid){
        if(!$start_time || !$end_time){
            return false;
        }
        $where = " time_stamp>{$start_time} and time_stamp<{$end_time} ";
        $sql = "
            select count(t1.id) stat_cnt, date_format(FROM_UNIXTIME( t1.time_stamp ) , '%Y-%m-%d') stat_date
            from 
                (
                    (
                        select concat('text_',id) id,'Text Post' post_type, time_stamp 
                        from enr_text
                        where 
                            /* is_publish=1 and */
                            cid='".$cid."' and {$where}
                    ) 
                    union
                    ( 
                        select concat('photo_',id) id,'Photo Post' post_type, time_stamp 
                        from enr_photo
                        where 
                            /* is_publish=1 and */
                            cid='".$cid."' and {$where}
                    )  
                    union
                    (
                        select concat('video_',id) id,'Video Post' post_type, time_stamp
                        from enr_video
                        where 
                            /* is_publish=1 and */
                            cid='".$cid."' and {$where}
                    )  
                    union
                    (
                        select concat('poll_',id) id,'Poll Post' post_type, time_stamp 
                        from enr_poll 
                        where 
                            /* is_publish=1 and */
                            cid='".$cid."' and {$where}
                    )  
                    union
                    (
                        select concat('quiz_',id) id,'Quiz Post' post_type, time_stamp 
                        from enr_quiz
                        where 
                            /* is_publish=1 and */
                            cid='".$cid."' and {$where}
                    )  
                ) t1
            group by
                date_format(FROM_UNIXTIME( t1.time_stamp ) , '%Y-%m-%d') 
            order by
                date_format(FROM_UNIXTIME( t1.time_stamp ) , '%Y-%m-%d') 
        ";
        $query = $this->db->query($sql);
        $rows = $query->result();
        return $rows;
    }
    public function getStatsGender($start_time, $end_time, $cid){
        if($start_time && $end_time){
            $where = " and joined>{$start_time} and joined<{$end_time} ";
        }
        $sql = "
            select count(id) stat_cnt, if(gender, 'Female', 'Male') stat_label
            from  enr_fan
            where
                cid='{$cid}' {$where}
            group by
                gender
        ";
        $query = $this->db->query($sql);
        $rows = $query->result();
        return $rows;
    }
    public function getStatsAge($start_time, $end_time, $cid){
        if($start_time && $end_time){
            $where = " and joined>{$start_time} and joined<{$end_time} ";
        }
        $year = date('Y');
        $sql = "
            select count(id) stat_cnt, ({$year}-YEAR(birthday)) stat_label
            from  enr_fan
            where 
                cid='{$cid}' {$where}
            group by
                YEAR(birthday)
        ";
        $query = $this->db->query($sql);
        $rows = $query->result();
        return $rows;
    }
    public function getStatsPurchase($start_time, $end_time, $cid){
        if(!$start_time || !$end_time){
            $where = " and time_stamp>{$start_time} and time_stamp<{$end_time} ";
        }
        $sql = "
            select sum(value) stat_value, date_format(FROM_UNIXTIME( time_stamp ) , '%m/%d') stat_label
            from  enr_purchase
            where 
                cid='{$cid}' {$where}
            group by
                date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') 
            order by
                date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d')
        ";
        $query = $this->db->query($sql);
        $rows = $query->result();
        return $rows;
    }
    public function getFanByEmail($email, $cid){
        $sql = "
            select *
            from enr_fan
            where
                 UPPER(email)=?
                AND cid = ?
        ";
        $query = $this->db->query($sql, array(strtoupper($email), $cid));
        $row = $query->row_array();
        return $row;
    }
    public function getFanByVerificationCode($v){
        $sql = "
            select *
            from enr_fan
            where
                 verification_code='{$v}'
        ";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
    public function updateProfilePhoto($fan_id){
        if(!$_FILES['picture'] || !$_FILES['picture']['size'])
            return;
        $img_path = $this->_uploadPhoto('picture');
        $sql = array(
            'img_url' => $img_path
        );

        $this->db->where('id', $fan_id);
        $this->db->update('enr_fan',$sql);
        return true;
    }
    public function getConfigurations(){
        $sql = "
            select *
            from enr_configuration
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function setComment($options){
        $options['time_stamp'] = time();
        $this->db->insert('enr_comment', $options);
    }
    public function setLike($options, $like=true){
        $options['time_stamp'] = time();
        $this->db->where('user_id', $options['user_id']);
        $this->db->where('post_type', $options['post_type']);
        $this->db->where('content_id', $options['content_id']);
        $this->db->delete('enr_like');

        if($like){
            $this->db->insert('enr_like', $options);
        }
    }

    public function getLike($options, $like=true){

        $this->db->where('content_id', $options['content_id']);

        if($like){
            $this->db->get_where('enr_like', $options);
        }
    }


    public function setApproval($options, $approve=true){
        $options['time_stamp'] = time();
        $this->db->where('user_id', $options['user_id']);
        $this->db->where('post_type', $options['post_type']);
        $this->db->where('content_id', $options['content_id']);
        $this->db->delete('enr_approval');

        if($approve){
            $this->db->insert('enr_approval', $options);
        }
    }
    public function setPushNotification($value, $user_id){
        $sql['time_stamp'] = time();
        $sql['user_id'] = $user_id;
        $this->db->where('user_id', $user_id);
        $this->db->delete('enr_push_notification');

        if($value){
            $this->db->insert('enr_push_notification', $sql);
        }
    }
    public function followUser($user_id, $followed_user_id, $is_unfollow=false){
        $this->db->where('user_id', $user_id);
        $this->db->where('followed_user_id', $followed_user_id);
        $this->db->delete('enr_follow');
        if($is_unfollow){
            return;
        }

        $sql = array(
            'user_id' => $user_id,
            'followed_user_id' => $followed_user_id
        );
        $this->db->insert('enr_follow', $sql);
    }
    /**
     * user_id : the user id that follow
     *
     */
    public function getFollowings($user_id, $UPLOAD_URL, $count = -1, $page = 0){
        //$rows = $this->db->get_where('enr_follow', array('user_id'=>$user_id))->result_array();
        $query = 'select t1.id, t1.name, '. (empty($UPLOAD_URL) ? 'img_url' : 'IF(img_url = "" or img_url is NULL, "", CONCAT("' . $UPLOAD_URL . '", img_url)) as img_url') . ' from enr_fan as t1, enr_follow as t2
                where t1.id = t2.followed_user_id and t2.user_id = ?';

        if($count != -1) {
            $query .= '
                    limit ' . ($count * $page) . ',' . $count;
        }
        $rows = $this->db->query($query, array($user_id))->result_array();

        return $rows;
    }
    public function checkFollow($user_id, $followed_user_id ){
        $row = $this->db->get_where('enr_follow', array('user_id'=>$user_id, 'followed_user_id'=>$followed_user_id))->row();
        if($row)
            return true;
        else
            return false;

    }
    /**
     * followed_id : the user id that be followed
     *
     */
    public function getFolloweds($followed_id, $UPLOAD_URL, $count = -1, $page = 0){
        //$rows = $this->db->get_where('enr_follow', array('followed_user_id'=>$followed_id))->result_array();
        $query = 'select t1.id, t1.name, '. (empty($UPLOAD_URL) ? 'img_url' : 'IF(img_url = "" or img_url is NULL, "", CONCAT("' . $UPLOAD_URL . '", img_url)) as img_url') . ' from enr_fan as t1, enr_follow as t2
                where t1.id = t2.user_id and t2.followed_user_id = ?';
        if($count != -1) {
            $query .= '
                    limit ' . ($count * $page) . ',' . $count;
        }
        $rows = $this->db->query($query, array($followed_id))->result_array();

        return $rows;
    }

    public function getPoint($user_id){
        $cnts[] = count($this->db->get_where('enr_text', array('fan_id'=>$user_id))->result_array());
        $cnts[] = count($this->db->get_where('enr_photo', array('fan_id'=>$user_id))->result_array());
        $cnts[] = count($this->db->get_where('enr_video', array('fan_id'=>$user_id))->result_array());
        $cnts[] = count($this->db->get_where('enr_poll', array('fan_id'=>$user_id))->result_array());
        $cnts[] = count($this->db->get_where('enr_quiz', array('fan_id'=>$user_id))->result_array());
        $count = 0;
        foreach($cnts as $cnt){
            $count += $cnt;
        }

        $follows = count($this->db->get_where('enr_follow', array('user_id'=>$user_id))->result_array());
        $point = $count*5 + $follows*2;
        return $point;
    }
    public function getFansByRanking($UPLOAD_DIR, $cid, $search, $page = 0, $count = 100) {
        $query = '
            select id as user_id, name as userName, point, IF(img_url="", "", concat("' . $UPLOAD_DIR . '", img_url)) as img_url from 
            (
                select *, 
                (
                    (
                        (select count(*) from enr_text where fan_id = enr_fan.id) +
                        (select count(*) from enr_photo where fan_id = enr_fan.id) +
                        (select count(*) from enr_video where fan_id = enr_fan.id) +
                        (select count(*) from enr_poll where fan_id = enr_fan.id) +
                        (select count(*) from enr_quiz where fan_id = enr_fan.id)
                    ) * 5 
                    +
                    (
                        select count(*) from enr_follow where user_id = enr_fan.id
                    ) * 2
                ) as `point`
                
                from enr_fan 
                where cid = ' . $cid .
            (empty($search) ? '' : '
                and upper(concat(enr_fan.firstname, " ", enr_fan.lastname)) LIKE "%'.strtoupper($search).'%"').'
            ) as view_fan
            where admin_type is NULL
            order by `point` DESC '.
            ($count == 0 ? '' : '
            limit ' . $page * $count . ', ' . $count);
        //echo $query;
        return $this->db->query($query)->result_array();

    }
    public function getComments($post_type, $content_id, $UPLOAD_URL = '', $count = -1, $page = 0){
        $sql = "
            select t1.*, t2.name, " . (empty($UPLOAD_URL) ? 't2.img_url' : 'IF(img_url = "" or img_url is NULL, "", CONCAT("' . $UPLOAD_URL . '", t2.img_url)) as img_url') . ", t2.admin_type
            from enr_comment t1 inner join enr_fan t2 on t1.user_id=t2.id
            where
                t1.post_type='{$post_type}' and t1.content_id='{$content_id}'
            order by
                t1.time_stamp desc
        ";
        if($count != -1) {
            $sql .= '
                    limit ' . ($count * $page) . ',' . $count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getAllComments($user_id = '', $UPLOAD_URL = '', $count = -1, $page= 0) {
        $sql = "
            select t1.*, t2.name, " . (empty($UPLOAD_URL) ? 't2.img_url' : 'IF(img_url = "" or img_url is NULL, "", CONCAT("' . $UPLOAD_URL . '", t2.img_url)) as img_url') . ", t2.admin_type
                    from enr_comment t1 inner join enr_fan t2 on t1.user_id=t2.id
                    where 1" .
            (empty($user_id) ? '' : " and t1.user_id = ".$user_id)."
                    order by
                    t1.time_stamp desc
                    ";
        if($count != -1) {
            $sql .= '
                    limit ' . ($count * $page) . ',' . $count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function checkPushinfo($user_id){
        $row = $this->db->get_where('enr_push_notification', array('user_id'=>$user_id))->row();
        if(!$row){
            return;
        }

        $sql = "
            select *
            from enr_feed 
            where
                id not in (select feed_id from enr_feed_pushnotification where user_id='{$user_id}') 
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();

        foreach($rows as &$row){
            $sql = array(
                'feed_id' => $row['id'],
                'user_id' => $user_id
            );
            $this->db->insert('enr_feed_pushnotification', $sql);

            $this->db->where('id', $row['content_id']);
            $row['content'] = $this->db->get_where('enr_'.$row['post_type']);
            $row['base_path'] = UPLOAD_URL;
        }
        return $rows;
    }

    public function setBlockFan($user_id, $blocked_id){
        $this->db->where('user_id', $user_id);
        $this->db->where('blocked_id', $blocked_id);
        $this->db->delete('enr_block');

        $sql = array(
            'user_id' => $user_id,
            'blocked_id' => $blocked_id,
            'time_stamp' => time()
        );
        $this->db->insert('enr_block', $sql);
    }

    public function setUnblockFan($user_id, $blocked_id){
        $this->db->where('user_id', $user_id);
        $this->db->where('blocked_id', $blocked_id);
        $this->db->delete('enr_block');
    }

    public function checkBlockFan($user_id, $blocked_id){
        $row = $this->db->get_where('enr_block', array('user_id'=>$user_id, 'blocked_id'=>$blocked_id))->row_array();
        if($row)
            return true;
        else
            return false;
    }
    public function insertMail($data, $current_time=false){
//        if(!$current_time)
//            $current_time = time();

        if(isset($_FILES['song']) && $_FILES['song']['size'])
            $path = $this->_uploadSong('song');
        else
            $path = "";

        $sql = array(
            "sender_id" => $data['sender'],
            "receiver_id" => $data['receiver'],
            "message" => $data['message'] ? $data['message'] : "",
            "time_stamp" => time()
        );
        $this->db->insert("enr_mail", $sql);
        $inserted_id = $this->db->insert_id();
        return $inserted_id;
    }
    public function getUnreadMessages($user_id){
        $sql = "
            select t1.*, date_format(FROM_UNIXTIME( t1.time_stamp ) , '%m/%d/%y') mail_date, t2.*, t1.id id
            from enr_mail t1 inner join enr_fan t2 on t1.sender_id=t2.id
            where t1.receiver_id='{$user_id}' and t1.is_seen=0
            order by t1.time_stamp desc
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
//        print_r($rows);exit;
        return $rows;
    }
    public function getMailList($user_id, $utc=false, $current_time=false, $count = -1, $page = 0){
        $result = array();

        if($user_id){
            $where1 = "t1.sender_id='{$user_id}'";
            $where2 = "t1.receiver_id='{$user_id}'";
        }else
            return false;
        $sql = "
            select t1.*, date_format(FROM_UNIXTIME( t1.time_stamp ) , '%m/%d/%y') mail_date, t2.*, t1.id id
            from enr_mail t1 inner join enr_fan t2 on t1.receiver_id=t2.id
            where {$where1}
            order by t1.time_stamp desc
        ";

        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $temps = array();

        $sql = "
            select t1.*, date_format(FROM_UNIXTIME( t1.time_stamp ) , '%m/%d/%y') mail_date, t2.*, t1.id id
            from enr_mail t1 inner join enr_fan t2 on t1.sender_id=t2.id
            where {$where2}
            order by t1.time_stamp desc
        ";
        if($count != -1) {
            $sql .= '
                    limit ' . ($count * $page) . ',' . $count;
        }
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        $temps = array();

        $result['receiver_messages'] = $rows;
        return $result;
    }
    public function getContent($post_type, $content_id){
        $row = $this->db->get_where('enr_'.$post_type, array('id' => $content_id))->row_array();
        return $row;
    }
    public function buyContent($data){
        $fan = $this->getFan($data['user_id']);
        $content = $this->getContent($data['post_type'], $data['content_id']);
        if(!$content){
            $this->controller->result['message'] = "Incorrect content.";
            $this->controller->result['status'] = false;
            return false;
        }
        if(!$content['credit']){
            $this->controller->result['message'] = "The content is free.";
            $this->controller->result['status'] = false;
            return false;
        }
        if($fan['credit'] < $content['credit']){
            $this->controller->result['message'] = "The fan's credit is smaller than credit of this content.";
            $this->controller->result['status'] = false;
            return false;
        }
        $this->db->where('id', $data['user_id']);
        $remain_credit = (float)$fan['credit'] - (float)$content['credit'];
        $this->db->update('enr_fan', array('credit'=>$remain_credit ));

        $sql = array(
            'user_id' => $data['user_id'],
            'post_type' => $data['post_type'],
            'content_id' => $data['content_id'],
            'credit' => $content['credit'] ? $content['credit'] : 0,
            'time_stamp' => time()
        );
        $this->db->insert('enr_buy', $sql);
        return true;
    }
    public function checkBuyContent($data){
        $user_id = $data['user_id'];
        $post_type = $data['post_type'];
        $content_id = $data['content_id'];

        $where = array(
            'user_id'=>$user_id,
            'post_type'=> $post_type,
            'content_id'=> $content_id
        );
        $row = $this->db->get_where('enr_buy', $where)->row_array();
        $content = $this->getContent($post_type, $content_id);
        if($row || !$content['credit']){
            return true;
        }else{
            return false;
        }
    }
    public function insertAnswerHistory($answer_id, $user_id){
        $answer = $this->db->get_where('enr_poll_answer', array('id'=>$answer_id))->row_array();
        $sql = array(
            'answer_id' => $answer_id,
            'user_id' => $user_id,
            'question_id' => $answer['question_id'],
            'time_stamp' => time()
        );
        $this->db->insert('enr_answer_history', $sql);
    }
    public function insertQuizAnswerHistory($user_id, $data){
//       $this->db->where('user_id', $user_id);
//       $this->db->delete('enr_answer_history');
        $data['time_stamp'] = time();
        $this->db->insert('enr_answer_history_quiz', $data);
    }
    public function getFansAnsweredQuiz($quiz_id){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select t1.*,date_format(FROM_UNIXTIME(t1.last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( t1.joined ) , '%Y-%m-%d') join_date, date_format(FROM_UNIXTIME( t2.time_stamp ) , '%Y-%m-%d') anwerdate
            from enr_fan t1 inner join enr_answer_history_quiz t2 on t1.id=t2.user_id
            where t1.cid='{$cid}' and t2.content_id='{$quiz_id}'
            order by t2.time_stamp desc
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getFansAnsweredPoll($poll_id){
        $cid = $this->controller->chanel['id'];
        $sql = "
            select t1.*,date_format(FROM_UNIXTIME(t1.last_login  ) , '%Y-%m-%d') last_login, date_format(FROM_UNIXTIME( t1.joined ) , '%Y-%m-%d') join_date, date_format(FROM_UNIXTIME( t2.time_stamp ) , '%Y-%m-%d') anwerdate, t4.name qName, t5.name aName
            from enr_fan t1 inner join enr_answer_history t2 on t1.id=t2.user_id
                inner join enr_poll_question t4 on t2.question_id=t4.id
                inner join enr_poll_answer t5 on t2.answer_id = t5.id
            where t1.cid='{$cid}' and t2.question_id in (select id from enr_poll_question where poll_id='{$poll_id}')
            order by t2.time_stamp desc
        ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        return $rows;
    }
    public function getAnswerPercent($question_id){
        $sql = "
            select count(t1.id) cnt
            from enr_answer_history  t1
            where
                t1.question_id = '{$question_id}'
       ";
//       echo $sql;exit;
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalCnt = $row['cnt'];

        $sql = "
            select count(t1.id) cnt, t1.answer_id, t2.*
            from enr_answer_history t1 right join enr_poll_answer t2 on t1.answer_id=t2.id
            where
                t2.question_id = '{$question_id}'
            group by
                t2.id 
       ";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        foreach($rows as &$row){
            $row['percent'] = round($row['cnt'] / $totalCnt, 2) * 100;
        }

        return $rows;
    }

    /**
     * Delete feed content.
     *
     * @param $user_id
     * @param $post_type
     * @param $content_id
     * @return bool
     */
    public function deleteFeed($user_id, $post_type, $content_id){
        $l_bCLOSE_BUFFER =1;
        if($l_bCLOSE_BUFFER){
            Utils::closeBufferAndContinueProcess("<SCRIPT LANGUAGE='JavaScript'>
            
            window.location.href='/content/postfeed/';
            </SCRIPT>");
        }
        $fan = $this->db->get_where('enr_fan', array('id'=>$user_id))->row_array();
        if($fan['admin_type']){
            $this->db->where('id', $content_id);
        }else{
            $this->db->where('fan_id', $user_id);
            $this->db->where('id', $content_id);
        }
        $this->db->delete('enr_'.$post_type);

        return true;
    }


  




     

        

      
  

    /**
     * Delete feed content from viral_media_cat_post.
     *
     * @param $post_type
     * @param $content_id
     * @return mixed
     */
    public function deleteFeed_Categories($post_type, $content_id){
        $content_type_id = ($post_type == CONTENT_TYPE_VIDEO) ? CONTENT_TYPE_ID_VIDEO : CONTENT_TYPE_ID_PHOTO;

        $this->db->where('post_id', $content_id);
        $this->db->where('content_type_id', $content_type_id);
        $result = $this->db->delete('viral_media_cat_post');

        return $result;
    }

       /**
     * Delete feed priority from priority table 
     *
     * @param $content_id
     * @return mixed
     */
    public function deleteFeed_priority($id){
        $this->db->where('content_id', $id);
        $result = $this->db->delete('priority');

        return $result;
    }

    public function addUdToken($user_id, $ud_type, $ud_token){

        if($ud_token == ""){
            return true;
        }

        $this->db->where('ud_type', $ud_type);
        $this->db->where('user_id', $user_id);
        $this->db->delete('enr_ud_token');

        $this->db->insert('enr_ud_token', array('user_id'=>$user_id, 'ud_type' => $ud_type, 'ud_token'=>$ud_token));
        return true;
    }
    public function deleteComment($user_id, $content_id, $post_type, $comment_id){
        $this->db->where('post_type',$post_type);
        $this->db->where('content_id',$content_id);

        $fans = $this->db->query('select IFNULL(admin_type, -1) as admin_type from enr_fan where id= ?', array($user_id))->result_array();
        if(count($fans) == 0 || $fans[0]['admin_type'] == -1) {
            $this->db->where('user_id',$user_id);
        }
        $this->db->where('id',$comment_id);
        $this->db->delete('enr_comment');
        return $this->db->affected_rows();
    }
    public function deleteFan($fan_id){
        $this->db->where('id', $fan_id);
        $this->db->delete('enr_fan');

        $this->db->where('blocked_id', $fan_id);
        $this->db->delete('enr_block');

        $this->db->where('followed_user_id', $fan_id);
        $this->db->delete('enr_follow');

        $this->db->where('sender_id', $fan_id);
        $this->db->delete('enr_mail');

        $this->db->where('receiver_id', $fan_id);
        $this->db->delete('enr_mail');

        $tables = array(
            'enr_answer_history',
            'enr_block',
            'enr_buy',
            'enr_block',
            'enr_comment',
            'enr_feed_pushnotification',
            'enr_follow',
            'enr_like',
            'enr_push_notification',
            'enr_ud_token',
        );
        foreach($tables as $table){
            $this->db->where('user_id', $fan_id);
            $this->db->delete($table);
        }

        $sql = "delete from t3
                using enr_poll_question t1, enr_poll t2, enr_poll_answer t3 where t1.poll_id=t2.id and t2.fan_id='{$fan_id}' and t3.question_id=t1.id";
        $this->db->query($sql);

        $sql = "delete from t1
                using enr_poll_question t1, enr_poll t2 where t1.poll_id=t2.id and t2.fan_id='{$fan_id}' ";
        $this->db->query($sql);

        $sql = "delete from t3
                using enr_quiz_question t1, enr_quiz t2, enr_quiz_answer t3 where t1.quiz_id=t2.id and t2.fan_id='{$fan_id}' and t3.question_id=t1.id";
        $this->db->query($sql);

        $sql = "delete from t1
                using enr_quiz_question t1, enr_quiz t2 where t1.quiz_id=t2.id and t2.fan_id='{$fan_id}' ";
        $this->db->query($sql);

        $tables = array(
            'enr_music',
            'enr_photo',
            'enr_poll',
            'enr_quiz',
            'enr_text',
            'enr_video',
        );
        foreach($tables as $table){
            $this->db->where('fan_id', $fan_id);
            $this->db->delete($table);
        }
    }

    /**
     * update admins
     * 2014 02 28
     */
    function loginToCms($data){


        $status =  $this->ion_auth->login_ext($data['identity'], $data['password'], 0);

        return $status;
    }
    function updateAdmin($data, $admin_type){
        $login_user_id = $this->ion_auth->get_user_id();
        if($login_user_id!=""){
            $login_user = $this->db->get_where('users', array('id'=>$login_user_id))->row_array();

            $cid = $login_user['cid'];
        }else{
            $login_user = $this->db->get_where('users', array('email'=>$data['email']))->row_array();
            $cid = $login_user['cid'];
        }
        if($login_user['superadmin'] && $cid){
            return false;
        }

        $fan = $this->db->get_where('enr_fan', array('cid'=>$cid, 'admin_type'=>$admin_type))->row_array();
        $sql = array(
            'email' => $data['email'],
            'cid' => $cid,
            'admin_type' => $admin_type,
            'joined' => time(),
        );
        if($data['password']){
            $sql['password'] = $data['password'];
        }
        if(!$fan){
            $this->db->insert('enr_fan', $sql);
        }else{
            $this->db->where('cid', $cid);
            $this->db->where('admin_type', $admin_type);
            $this->db->update('enr_fan', $sql);
        }

        $user = $this->db->get_where('users', array('cid'=>$cid, 'admin_type'=>$admin_type))->row_array();
        $sql = array(
            'email' => $data['email'],
            'admin_type' => $admin_type,
            'created_on' => time(),
            'active' => 1,
            'phone' => $data['phone'],
            'admin_cid' => $cid,
        );
        if($data['password']){
            $sql['password'] = $this->ion_auth->hash_password($data['password']);
        }


        if(!$user){
            $this->db->insert('users', $sql);
        }else{
            $this->db->where('cid', $cid);
            $this->db->where('admin_type', $admin_type);
            $this->db->update('users', $sql);
        }
        $this->db->where('cid', $cid);
        $this->db->update('users', array('admin_cid'=>$cid));
    }

    function getAdminInfos(){
        $login_user_id = $this->ion_auth->get_user_id();
        $login_user = $this->db->get_where('users', array('id'=>$login_user_id))->row_array();
        $cid = $login_user['cid'];
        $sql = "
        select *
        from users
        where
            cid='{$cid}' or admin_cid='{$cid}' 
       ";
        $query = $this->db->query($sql);
        $admins = $query->result_array();
        return $admins;
    }
    function compress_image($source_url, $destination_url, $quality) {

        $info = getimagesize($source_url);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);

        imagejpeg($image, $destination_url, $quality);
        return $destination_url;
    }



    public function _copy_file($type, $from) {
        $year = date('Y');
        $month = date('m');

        $ext = strtolower(pathinfo($from, PATHINFO_EXTENSION));
        $filename = md5(time().rand(0.1, 99.9));
        $filepath = UPLOAD_DIR.$type."/".$year;
        $ret_path = $type."/".$year;
        $file_a3_path = UPLOAD_AMZ."photo/".$year;
        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$month;
        $file_a3_path  .= "/".$month;
        $ret_path .= "/".$month;
        if(!is_dir($filepath)){
            mkdir($filepath, 0777, true);
        }
        $filepath .= "/".$filename.".".$ext;
        $file_a3_path .= "/".$filename.".".$ext;
        $ret_path .= "/".$filename.".".$ext;

        copy($from, $filepath);

        $l_sNewAbsoluteFilePath = $filepath;

        $l_sNewPath = Utils::pushFileToS3($l_sNewAbsoluteFilePath,$file_a3_path);

        return $ret_path;
    }




    /*
*
* Wrapper functions 25-5-2014
*
*/

    public function InsertUserDetails($user_id, $ip, $os, $user_browser, $html_headers) {



        $escaped_values = array_map('mysql_real_escape_string', array_values($html_headers));

        $html_headers = implode(", ", $escaped_values);

//echo $user_id;echo $ip;echo $os;echo $user_browser;echo $html_headers;  exit;
        $res = $this->db->insert('ss_os_details', array('user_id' => $user_id, 'user_ip' => $ip, 'user_os' => $os, 'user_browser' => $user_browser, 'html_headers' => $html_headers, 'inserted_time' => date('Y-m-d H:i:s')));
//echo $user_id;echo $ip;echo $os;echo $user_browser;echo $html_headers;  exit;
    }

    public function RegisterdUserDetails($fan_id, $ip, $user_os, $user_browser, $gender, $birthday) {

        $res = $this->db->insert('ss_registerd_users', array('user_id' => $fan_id, 'user_ip' => $ip, 'user_os' => $user_os, 'user_browser' => $user_browser, 'gender' => $gender, 'birthday' => $birthday, 'inserted_time' => date('Y-m-d H:i:s')));
    }

    /*
     * Credits spent by users
     */

    public function Wrp_buyContent($data) {
        $fan = $this->getFan($data['user_id']);
        $content = $this->getContent($data['post_type'], $data['content_id']);

        $this->db->where('id', $data['user_id']);
        $remain_credit = (float) $fan['credit'] - (float) $content['credit'];

        $sql = array(
            'user_id' => $data['user_id'],
            'post_type' => $data['post_type'],
            'content_id' => $data['content_id'],
            'credit' => $content['credit'] ? $content['credit'] : 0,
            'remaining_credit' => $remain_credit,
            'time_stamp' => time()
        );
        $this->db->insert('ss_buycontent', $sql);
    }

    public function wrp_setComment($options) {
        $options['time_stamp'] = time();
        $this->db->insert('ss_comment', $options);
    }

    public function wrp_followUser($user_id, $followed_user_id, $is_unfollow = false) {

        $sql = array(
            'user_id' => $user_id,
            'followed_user_id' => $followed_user_id,
            'time_stamp' => time()
        );
        $this->db->insert('ss_follow', $sql);
    }

    function wrp_addFeed($feed,$feed_id, $type = 'NUll') {
        $designinfo = $this->getDesignInfo();

        if ($type == 'photo') {
            $post = array(
                'user_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
                'content_id' => $feed_id,
                'content_des' => $feed['description'],
                'post_type' => 'photo',
                'credit' => $feed['credit'],
                'time_stamp' => time(),
            );
        } elseif ($type == 'video') {
            $post = array(
                'user_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
                'content_id' => $feed_id,
                'content_des' => $feed['description'],
                'post_type' => 'video',
                'credit' => $feed['credit'],
                'time_stamp' => time(),
            );
        } elseif ($type == 'text') {
            $post = array(
                'user_id' => ($feed['fan_id'] ? $feed['fan_id'] : -1),
                'content_id' => $feed_id,
                'content_des' => $feed['description'],
                'post_type' => 'text',
                'credit' => $feed['credit'],
                'time_stamp' => time(),
            );
        }

        $res = $this->db->insert("ss_addfeed", $post);
    }

    /*  Tracking Details start from */


    public function menu_tracking_details($user_id, $menu_name) {
        $res = $this->db->insert('ss_menu_tracking_details', array('user_id' => $user_id, 'menu_name' => $menu_name, 'inserted_time' => date('Y-m-d H:i:s')));
    }

    public function shop_link_details($linkid, $userid) {
        $res = $this->db->insert('ss_shop_link_details', array('user_id' => $userid, 'link_id' => $linkid, 'inserted_time' => date('Y-m-d H:i:s')));
    }

    public function track_video($userid, $contentid, $type) {
        $sql = "insert into ss_video_image_tracking_details(user_id,image_id,video_id,type,inserted_time)values(?,?,?,?,?)";
        $query = $this->db->query($sql, array($userid, '', $contentid, $type, date('Y-m-d H:i:s')));
        return $this->db->last_query();
    }

    public function track_image($userid, $contentid, $type) {
        $sql = "insert into ss_video_image_tracking_details(user_id,image_id,video_id,type,inserted_time)values(?,?,?,?,?)";
        $query = $this->db->query($sql, array($userid, $contentid, '', $type, date('Y-m-d H:i:s')));
    }

    public function track_like_event($userid, $contentid, $type) {
        $sql = "insert into ss_likeuser_tracking_details(user_id,content_id,type,inserted_time)values(?,?,?,?)";
        $query = $this->db->query($sql, array($userid, $contentid, $type, date('Y-m-d H:i:s')));
    }

    public function track_share_event($userid, $contentid, $type, $sharetype) {
        $sql = "insert into ss_share_tracking_details(user_id,content_id,type, share_type, inserted_time)values(?,?,?,?,?)";
        $query = $this->db->query($sql, array($userid, $contentid, $type, $sharetype, date('Y-m-d H:i:s')));
    }

    /* Tracking Details End */

    //newly added function on 28_7_2014 by vinod

    public function TrackFBPostId($fbpostid,$content_id,$content_type,$posted_on,$userid){

        $sql = "insert into ss_social_tracking_details(post_id,content_id,type, share_type,user_id, inserted_time)values(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($fbpostid,$content_id,$content_type,$posted_on,$userid, date('Y-m-d H:i:s')));
    }

}