<?
 error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

 	 set_time_limit(0);

	ini_set('memory_limit','1280M');
    ini_set('upload_max_filesize','100M');
    ini_set('max_execution_time',1000000);



  require(__DIR__.'/config/settings.php');

  $accountId = GA_ACCOUNT_ID;

  $analytics = Gastar::getService();

  Gastar::refreshGAProperties($analytics);

  //get_daily_totals($analytics);
  //get_daily_photo_totals($analytics);  
  echo "RONNIE COLEMAN OVERALL<br><br>";

  $video_page_views = get_overall_individual_totals($analytics,37); 
  $photo_page_views = get_overall_individual_photo_totals($analytics,37);

  $total_page_views = $video_page_views + $photo_page_views;

  echo "Total Combined Page Views: ".$total_page_views."<br><br>";	

  echo "RONNIE COLEMAN 07-05-15 through 08-05-15<br><br>";	

  $video_page_views = get_overall_individual_totals_month($analytics,37); 
  $photo_page_views = get_overall_individual_photo_totals_month($analytics,37);

  $total_page_views = $video_page_views + $photo_page_views;

  echo "Total Combined Page Views: ".$total_page_views."<br><br>";	

  echo "LOUIS SASHA OVERALL<br><br>";

  $video_page_views = get_overall_individual_totals($analytics,75); 
  $photo_page_views = get_overall_individual_photo_totals($analytics,75);

  $total_page_views = $video_page_views + $photo_page_views;

  echo "Total Combined Page Views: ".$total_page_views."<br><br>";	

  echo "LOUIS SASHA 07-05-15 through 08-05-15<br><br>";	

  $video_page_views = get_overall_individual_totals_month($analytics,75); 
  $photo_page_views = get_overall_individual_photo_totals_month($analytics,75);

  $total_page_views = $video_page_views + $photo_page_views;

  echo "Total Combined Page Views: ".$total_page_views."<br><br>";	

  function get_overall_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE c.is_publish=1 AND v.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-03-01";

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view = Gastar::calcTotals($data);
		    	}else{
					$video_view = 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	$video_impression = Gastar::calcTotals($data);
		    	}else{
					$video_impression = 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_played = Gastar::calcTotals($data);
		    	}else{
					$video_played = 0;
				}

		    	$video_impression += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_finished = Gastar::calcTotals($data);
		    	}else{
					$video_finished = 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_half = Gastar::calcTotals($data);
		    	}else{
					$video_view_half = 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_three_fourths = Gastar::calcTotals($data);
		    	}else{
					$video_view_three_fourths = 0;
				}
		    	
		    	$engagement1 = ($video_view/$video_impression)*100;

		        if($video_view_half>0){
		        $engagement2 = ($video_view_half/$video_impression)*100;
		        }
		        if($video_view_three_fourths>0){
		        $engagement3 = ($video_view_three_fourths/$video_impression)*100;
		     
		        }
		        $engagement4 = ($video_finished/$video_impression)*100;
		        if($video_view_half>0 && $video_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		     

				$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}

		    }

		    $count++;
		}

	 }
	 echo $count;
  }

  function get_daily_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE c.is_publish=1 AND v.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-08-03";

  	$end_date = "2015-08-03";

   	$sql_end_date = time();

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		   	if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){

		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view = Gastar::calcTotals($data);
		    	}else{
					$video_view = 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	$video_impression = Gastar::calcTotals($data);
		    	}else{
					$video_impression = 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_played = Gastar::calcTotals($data);
		    	}else{
					$video_played = 0;
				}

		    	$video_impression += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_finished = Gastar::calcTotals($data);
		    	}else{
					$video_finished = 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_half = Gastar::calcTotals($data);
		    	}else{
					$video_view_half = 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_three_fourths = Gastar::calcTotals($data);
		    	}else{
					$video_view_three_fourths = 0;
				}
		    	
		    	$engagement1 = ($video_view/$video_impression)*100;

		        if($video_view_half>0){
		        $engagement2 = ($video_view_half/$video_impression)*100;
		        }
		        if($video_view_three_fourths>0){
		        $engagement3 = ($video_view_three_fourths/$video_impression)*100;
		     
		        }
		        $engagement4 = ($video_finished/$video_impression)*100;
		        if($video_view_half>0 && $video_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		     

				$l_sSql = "INSERT INTO sc_reports_daily (time_stamp,channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ('2015-08-03',$channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement)"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted:".$l_sSql;
				}else{
					echo "Not Insereted".$l_sSql;
				}
			
		    }
		
		    $count++;
		}

	 }
	 echo $count;
  }


    function get_hourly_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE c.is_publish=1 AND v.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = date('Y-m-d');

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view = Gastar::calcTotals($data);
		    	}else{
					$video_view = 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	$video_impression = Gastar::calcTotals($data);
		    	}else{
					$video_impression = 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_played = Gastar::calcTotals($data);
		    	}else{
					$video_played = 0;
				}

		    	$video_impression += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_finished = Gastar::calcTotals($data);
		    	}else{
					$video_finished = 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_half = Gastar::calcTotals($data);
		    	}else{
					$video_view_half = 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_three_fourths = Gastar::calcTotals($data);
		    	}else{
					$video_view_three_fourths = 0;
				}
		    	
		    	$engagement1 = ($video_view/$video_impression)*100;

		        if($video_view_half>0){
		        $engagement2 = ($video_view_half/$video_impression)*100;
		        }
		        if($video_view_three_fourths>0){
		        $engagement3 = ($video_view_three_fourths/$video_impression)*100;
		     
		        }
		        $engagement4 = ($video_finished/$video_impression)*100;
		        if($video_view_half>0 && $video_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		     

				$l_sSql = "INSERT INTO sc_reports_hourly (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}

		    }

		    $count++;
		}

	 }
	 echo $count;
  }


   function get_overall_photo_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT p.id,p.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS p INNER JOIN enr_chanel AS c ON p.cid = c.id WHERE c.is_publish=1 AND p.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-03-01";

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;

		    	
		     

				$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga) 
							VALUES ($channel_id,$content_id,'photo',$page_views) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}

		    }

		    $count++;
		}

	 }
	 echo $count;
  }

  function get_daily_photo_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT p.id,p.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS p INNER JOIN enr_chanel AS c ON p.cid = c.id WHERE c.is_publish=1 AND p.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-08-03";

  	$end_date = "2015-08-03";

   	$sql_end_date = time();

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;

				
				$l_sSql = "INSERT INTO sc_reports_daily (time_stamp,channel_id,content_id,content_type,page_views_ga) 
							VALUES ('2015-08-03',$channel_id,$content_id,'photo',$page_views)"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted:".$l_sSql;
				}else{
					echo "Not Insereted".$l_sSql;
				}
			
		    }

		    $count++;
		}

	 }
	 echo $count;
  }

      function get_hourly_photo_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT p.id,p.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS p INNER JOIN enr_chanel AS c ON p.cid = c.id WHERE c.is_publish=1 AND p.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = date('Y-m-d');

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	 echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest;


				$l_sSql = "INSERT INTO sc_reports_hourly (channel_id,content_id,content_type,page_views_ga) 
							VALUES ($channel_id,$content_id,'photo',$page_views) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}

		    }

		    $count++;
		}

	 }
	 echo $count;
  }


   function get_overall_individual_totals($analytics, $cid){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE v.cid='.$cid.' AND c.id='.$cid.' AND c.is_publish=1';
  
  	

  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-03-01";

  	$end_date = date('Y-m-d');

  	


  	$count=0;

	 foreach($l_aData as $value){
	 //echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views += Gastar::calcPageViewTotals($data);
				}else{
					$page_views += 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp += 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest += 0;
				}

			    

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		
		    		$ind_view = Gastar::calcTotals($data);
		    		$video_view += $ind_view;
		    	}else{
					$video_view += 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	 $ind_impression = Gastar::calcTotals($data);
		    	 $video_impression += $ind_impression;
		    	}else{
					$video_impression += 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_played = Gastar::calcTotals($data);
		    		$video_played += $ind_played;
		    	}else{
					$video_played += 0;
				}

		    	$video_played += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_finished =  Gastar::calcTotals($data);
		    		$video_finished += $ind_finished;
		    	}else{
					$video_finished += 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	    $ind_view_half = Gastar::calcTotals($data);
		    	    $video_view_half += $ind_view_half;
		    	}else{
					$video_view_half += 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_three_fourths = Gastar::calcTotals($data);
		    		$video_view_three_fourths += $ind_three_fourths;
		    	}else{
					$video_view_three_fourths += 0;
				}
				$ind_impression+=$ind_played;
			
		    	$engagement1 = ($ind_view/$ind_impression)*100;

		        if($ind_view_half>0){
		        $engagement2 = ($ind_view_half/$ind_impression)*100;
		        }
		        if($ind_view_three_fourths>0){
		        $engagement3 = ($ind_view_three_fourths/$ind_impression)*100;
		     
		        }
		        $engagement4 = ($ind_finished/$ind_impression)*100;
		        if($ind_view_half>0 && $ind_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		    	$engagement += $engagement;
		     

				/*$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}*/

		    }

		    $count++;
		}

	 }
	 
	 $engagement = $engagement/$count;
	 $engagement = number_format($engagement, 2, '.', '');
	 $page_views = $page_views + $page_views_lp + $page_views_mtest;
	 $video_impression += $video_played;

	 if($page_views==$video_view){ $engagement=100;}



	 echo "Video Stats<br>";
	 echo "Number of Videos: ".$count."<br>";
	 echo "Video Views: ".$video_view."<br>";
	 echo "Page Views: ".$page_views."<br>";
	 echo "Engagement: ".$engagement."%<br><br>";

	 return $page_views;

  }
 

 function get_overall_individual_photo_totals($analytics, $cid){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE v.cid ='.$cid.' AND c.id='.$cid.' AND c.is_publish=1 AND v.is_publish=1';
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-03-01";

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	// echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views += Gastar::calcPageViewTotals($data);
				}else{
					$page_views += 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp += 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest += 0;
				}

			    


			
		    	
		    	
		     

				/*$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}*/

		    }

		    $count++;
		}

	 }

	$page_views += $page_views_lp; 
	$page_views += $page_views_mtest;

	 echo "Photo Stats<br>";
	 
	 echo "Page Views: ".$page_views."<br><br>";

	 return $page_views;

  }

     function get_overall_individual_totals_month($analytics, $cid){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid,v.start_date, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE v.cid='.$cid.' AND c.id='.$cid.' AND c.is_publish=1';
  
  	

  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-07-05";

  	$end_date = date('Y-m-d');

  	


  	$count=0;

	 foreach($l_aData as $value){
	 //echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views += Gastar::calcPageViewTotals($data);
				}else{
					$page_views += 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp += 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest += 0;
				}

			    

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		
		    		$ind_view = Gastar::calcTotals($data);
		    		$video_view += $ind_view;
		    	}else{
					$video_view += 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	 $ind_impression = Gastar::calcTotals($data);
		    	 $video_impression += $ind_impression;
		    	}else{
					$video_impression += 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_played = Gastar::calcTotals($data);
		    		$video_played += $ind_played;
		    	}else{
					$video_played += 0;
				}

		    	$video_played += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_finished =  Gastar::calcTotals($data);
		    		$video_finished += $ind_finished;
		    	}else{
					$video_finished += 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	    $ind_view_half = Gastar::calcTotals($data);
		    	    $video_view_half += $ind_view_half;
		    	}else{
					$video_view_half += 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$ind_three_fourths = Gastar::calcTotals($data);
		    		$video_view_three_fourths += $ind_three_fourths;
		    	}else{
					$video_view_three_fourths += 0;
				}
				$ind_impression+=$ind_played;
			
		    	$engagement1 = ($ind_view/$ind_impression)*100;

		        if($ind_view_half>0){
		        $engagement2 = ($ind_view_half/$ind_impression)*100;
		        }
		        if($ind_view_three_fourths>0){
		        $engagement3 = ($ind_view_three_fourths/$ind_impression)*100;
		     
		        }
		        $engagement4 = ($ind_finished/$ind_impression)*100;
		        if($ind_view_half>0 && $ind_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		    	$engagement += $engagement;
		     

				/*$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}*/

		    }
            if($value['start_date']>"2015-07-04"){
		    $count++;
		}
		}

	 }
	 
	 $engagement = $engagement/$count;
	 $engagement = number_format($engagement, 2, '.', '');
	 $page_views = $page_views + $page_views_lp + $page_views_mtest;
	 $video_impression += $video_played;

	 
	 if($page_views==$video_view){ $engagement=100;}


	 echo "Video Stats<br>";
	 echo "Number of Videos: ".$count."<br>";
	 echo "Video Views: ".$video_view."<br>";
	 echo "Page Views: ".$page_views."<br>";
	 echo "Engagement: ".$engagement."%<br><br>";

	 return $page_views;

  }
 

 function get_overall_individual_photo_totals_month($analytics, $cid){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE v.cid ='.$cid.' AND c.id='.$cid.' AND c.is_publish=1 AND v.is_publish=1';
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-07-05";

  	$end_date = date('Y-m-d');

  	$count=0;

	 foreach($l_aData as $value){
	// echo "Channel ID: ".$value['cid']." Content ID: ".$value['id']." Profile ID: ".$value['google_analytics_internal_id']."<br>";

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views += Gastar::calcPageViewTotals($data);
				}else{
					$page_views += 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp += 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest += Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest += 0;
				}

			    


			
		    	
		    	
		     

				/*$l_sSql = "INSERT INTO sc_reports (channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ($channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement) 
							ON DUPLICATE KEY
							UPDATE page_views_ga=$page_views,video_views=$video_view,video_view_half=$video_view_half,video_view_three_fourths=$video_view_three_fourths,video_finished=$video_finished,video_impressions=$video_impression,engagement=$engagement"; 	
				if($l_xSql->runQuery($l_sSql)){
					echo "Inserted";
				}else{
					echo "Not Insereted";
				}*/

		    }

		    $count++;
		}

	 }

	$page_views += $page_views_lp; 
	$page_views += $page_views_mtest;

	 echo "Photo Stats<br>";
	 
	 echo "Page Views: ".$page_views."<br><br>";

	 return $page_views;

  }
?>