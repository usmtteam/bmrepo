<?php

    require(__DIR__.'/settings.php');

    $l_bIsIE10 = preg_match("/MSIE (10|9|8|7)/",$_SERVER['HTTP_USER_AGENT']);

    if($_SERVER['SC_CHANNEL']){
        $l_sSlug = strtolower($_SERVER['SC_CHANNEL']);
    }else{
        preg_match("/(.+\.)?(.+)\.(starsite|starclub)/",$_SERVER['HTTP_HOST'],$l_aMatches);
        $l_sSlug = strtolower($l_aMatches[2]);
        
    }
   
    
    if($l_sSlug){

        //==================================================================
        //              GET CACHE BASED ON CHANNEL
        //==================================================================
        
        $l_sMemcacheKey = Memcaching::makeKey($l_sSlug);
       
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);
        //print_r($l_aCachedContent); exit();
        if(!$l_aCachedContent){
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT * FROM enr_chanel WHERE lower(url) = "'.$l_sSlug.'"';
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            $cid = $l_aData['id'];
            $l_dSql = 'SELECT * FROM enr_design WHERE cid = "'.$cid.'"';
            $l_dData = $l_xSql->getRow($l_xSql->runQuery($l_dSql));
            
            if(!$l_aData) Utils::show404();

            $l_xChannel = new stdClass();
            $l_xChannel->cid = $l_aData['id'];
            $l_xChannel->site_title = ($l_aData['title']) ? $l_aData['title'] : $l_aData['name'];
            $l_xChannel->site_header_title = $l_aData['name'];
            $l_xChannel->google_analytics_key = ($l_aData['google_analytics_key']) ? $l_aData['google_analytics_key'] : 'UA-46624794-23';
            $l_xChannel->url = $l_aData['url'];
            $l_xChannel->domain = ($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->slug = ($l_aData['slug']) ? $l_aData['slug'] : $l_aData['url'];
            $l_xChannel->cms = 'local.cms.'.($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->domain_cookie = '*.'.($l_aData['domain']) ? $l_aData['domain']: $l_aData['url'];
            $l_xChannel->starstats_site_id = $l_aData['starstats_site_id'];
            $l_xChannel->quantserve_key = ($l_aData['quantserve_key']) ? $l_aData['quantserve_key'] : QUANTSERVE_KEY;
            $l_xChannel->bc_playerid_web = ($l_aData['bc_playerid_web']) ? $l_aData['bc_playerid_web'] : BRIGHTCOVE_WEB;
            $l_xChannel->brightcove_key = ($l_aData['brightcove_key']) ? $l_aData['brightcove_key'] : BRIGHTCOVE_KEY;
            // $l_xChannel->adURL = 'http://ad4.liverail.com/?LR_PUBLISHER_ID=63753&LR_SCHEMA=vast2-vpaid&LR_AUTOPLAY=1&LR_TITLE=starclub&LR_VIDEO_ID=starclub&LR_URL=starclub test.starsite.com';
            $l_xChannel->adURL = ($l_aData['ad_server_url_web']) ? $l_aData['ad_server_url_web'] : AD_SERVER_URL;
            $l_xChannel->topAdUnit = GLOBAL_UNIT_LEADERBOARD;
            $l_xChannel->rightTopAdUnit = GLOBAL_UNIT_300x200;
            $l_xChannel->rightBottomAdUnit = GLOBAL_UNIT_SKYSCRAPER;
            $l_xChannel->mobileAdUnit = GLOBAL_UNIT_320x50;
            $l_xChannel->linkColor = ($l_dData['link_color']) ? $l_dData['link_color'] : '000000';
            $l_xChannel->buttonColor = ($l_dData['button_color']) ? $l_dData['button_color'] : '000000';
            $l_xChannel->backColor = ($l_dData['back_color']) ? $l_dData['back_color'] : 'FFFFFF';
            $l_xChannel->textColor = ($l_dData['text_color']) ? $l_dData['text_color'] : '000000';
            $l_xChannel->headerImage = $l_dData['header_image'];
            $l_xChannel->backgroundImage = $l_dData['background_image'];
            $l_xChannel->raw = $l_aData;
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_xChannel,MEMCACHE_DURATION_CHANNEL);
        }else{
            $l_xChannel = $l_aCachedContent['value'];
        }
        AppController::getInstance()->setChannel($l_xChannel);

     
    }

    //======================================================================
    //                  CACHE BRIGHTCOVE LOOK UP
    //======================================================================

    $l_sMemcacheKey = Memcaching::makeKey($l_sSlug.'bc-videos');
    $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    if(!$l_aCachedContent){

        require LIB_PATH.'brightcove/bc-mapi-cache.php';
        require LIB_PATH.'brightcove/MAPI.php';

        $bc = new BCMAPI('v2_tp1lbll_k9JjauR8-RcCMzw26Tq-KD87rxfhK7KdRmlkBsrGsKQ..');
        $params = array(
            'video_fields' => 'id,name,shortDescription,videoStillURL,thumbnailURL,playsTotal,publishedDate',
            'sortByOrder' => 'playsTotal:ASC'
        );

        $terms = array(
             'all' => 'tag:'.$l_xChannel->slug.''
        );

        try {
            $l_aBCVideos = $bc->search('video', $terms, $params);
            usort($l_aBCVideos, "publishVideoDateCompare");
        } catch (Exception $error) {
            
        }
        Memcaching::getInstance()->store($l_sMemcacheKey,$l_aBCVideos,MEMCACHE_DURATION_BC_VIDEOS);
    }else{
        $l_aBCVideos = $l_aCachedContent['value'];
    }

    //======================================================================
    //                  CACHE VIDEO THUMB LIST LOOK UP
    //======================================================================

    $l_sMemcacheKey = Memcaching::makeKey($l_sSlug.'fp-videos');
    $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

    if(!$l_aCachedContent){

        $l_xSql = AppController::getInstance()->getConnection();
        $l_sSql = 'SELECT * FROM enr_video WHERE cid = "'.$l_xChannel->cid.'" AND is_publish = 1';
       
        $l_aFPVideos =  $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
        
        Memcaching::getInstance()->store($l_sMemcacheKey,$l_aFPVideos,MEMCACHE_DURATION_FP_VIDEOS);
       
       
    }else{
        
        $l_aFPVideos = $l_aCachedContent['value'];

        $l_xSql = AppController::getInstance()->getConnection();
        $l_sSql = 'SELECT * FROM enr_video WHERE cid = "'.$l_xChannel->cid.'" AND is_publish = 1 ORDER BY cid ASC';
       
        $l_aFPVideos =  $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
        
    }

    


?>