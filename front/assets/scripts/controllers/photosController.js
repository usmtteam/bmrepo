enriqueApp.controller('photosController', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
	$scope.page = 0;
    $scope.feeds = [];
    $scope.star = {};
    $scope.filter = 'All';
    $scope.filterList = [];
    $scope.show_comment_box = false;
    $scope.is_loading = false;
  
    
    $scope.view_type = 'grid';
    
    $scope.loadData = function () {
    	if($scope.is_loading) return;
    	CommonLib.show_loading_bubble();
    	$scope.is_loading= true;
    	var url = $rootScope.get_api_url() + '&option=get_photos&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&count=9&page=' + $scope.page;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        	if(data.status == true) {
        		for(var i =0 ; i < data.photos.length; i++) {
        			data.photos[i].content_id = data.photos[i].id;
        			data.photos[i].post_type = 'photo';
        		}
        		
        		$scope.star = data.star;
        		$scope.feeds = $scope.feeds.concat( data.photos );
        		$scope.filterList = data.filters;
        		if(data.photos.length > 0) $scope.page ++;        		
        	}
        	else {
        		$rootScope.showConfirmDialog({
    				type: 'ALERT',
    				title: 'Photos',
    				description: 'You need to login to see photos',
    				onOk : function() {
    					document.location = $rootScope.WEBSITE_URL;
    				}
    			});
        	}      
        	$scope.is_loading = false;  	
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        	$scope.is_loading = false;
        });
    };
    $scope.onChangeToList = function(post) {
    	$scope.view_type = 'list';
    };
    $scope.onChangeToGrid = function(post) {
        $scope.view_type = 'grid';
    };
    //$scope.loadData();
    
    $scope.onMyComment_enter = function(e) {
    	if(e.which == 13) {
    		$rootScope.addComment(e, $scope.feeds);
    	}
	};
	
	$scope.onViewAllComments_click = function(e) {
		$rootScope.viewAllComments(e, $scope.feeds);
	};
	
	$scope.onButtonStar_click = function(e) {
		$rootScope.addLike(e, $scope.feeds);
	};
	$scope.onChangeFilter = function(e) {
		var target = $(e.target);
		$scope.filter = target.attr('data-filter');
	}
    $scope.loadPostData = function(post_type, post_id) {
        $http({method: 'GET', url: $rootScope.get_api_url() + '&option=get_post_detail&post_type=' + post_type + '&content_id=' + post_id}).
        success(function(data, status, headers, config) {
            
            if(data.status == true) {
                $rootScope.onViewPostDetail(null, true, data.post[0], data.star, true, true);
                
                //$rootScope.putMetaTag(data.post[0], data.star);
            }
        }).
        error(function(data, status, headers, config) {
        });
    };
}]);