 <?php

 $user_id = $this->session->userdata('user_id');
 $selfuser = $this->ion_auth->user($user_id)->row();

?>
  <!-- BEGIN PAGE -->  
<div class="page-content">
     <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
           <div class="span12">
              <h3 class="page-title">
                 Edit Channel
              </h3>
              <!--<ul class="breadcrumb">
                <li><a href="<?php echo site_url("question/question_list"); ?>">Question</a></li>
                <li>&raquo;</li>
                <li><a href="<?php echo site_url("question/question_edit/".$question[0]->qid); ?>">Edit Question</a></li>
              </ul>-->
           </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
           <div class="span12">
<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Edit Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    
                    <?php 
                        echo form_open_multipart("dashboard/chanel_edit/".$chanel['id'], 'id="videofileupload" class="form-horizontal"');
                            echo form_error("chanel_name");
                            echo form_error("chanel_admin");
                            echo form_error("chanel_password");
                            echo form_error("chanel_confirm_password");
                            if(isset($show_errors)) {
                                if (is_array($show_errors)) {
                                    foreach($show_errors as $error) {
                                        echo "<div class='alert alert-error'>".$error."</div>";
                                    }
                                }
                                else{
                                    echo "<div class='alert alert-error'>".$show_errors."</div>";
                                }
                            }
                    ?>
                    <?php if(isset($success_message)): ?> 
                        <div class="alert alert-block alert-success fade in"><?php echo $success_message; ?></div>
                    <?php endif; ?>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Name</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_name",$chanel['name'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Title</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_title",$chanel['title'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Url</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_url",$chanel['url'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Channel Slug</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_slug",$chanel['slug'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Admin email</label>
                                <div class="controls">
                                <?php  echo form_input("chanel_admin",$chanel['chanel_admin'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Password</label>
                                <div class="controls">
                                <?php  echo form_input(array("name"=>"chanel_password", 'type'=>'password'),'','class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Confirm Password</label>
                                <div class="controls">
                                <?php  echo form_input(array('name'=>"chanel_confirm_password", 'type'=>'password'),'','class="large m-wrap"');?>
                                </div>
                            </div>
                            
                              <div class="control-group">
                                    <label class="control-label" for="url">BrightCove Overlay Logo</label>
                                    <div class="controls">
                                    <?php  echo form_input("brightcove_overlay_logo",$chanel['brightcove_overlay_logo'],'class="large m-wrap"');?>
                                    </div>
                              </div>
                              <div class="control-group">
                                    <label class="control-label" for="url">Feedback email</label>
                                    <div class="controls">
                                    <?php  echo form_input("feedback_email",$chanel['feedback_email'],'class="large m-wrap"');?>
                                    </div>
                              </div>
                            <div class="control-group">
                                <div class="controls" style="margin-left:200px;">
                                <label class="checkbox line" >
                                    <input type="checkbox" name="is_publish" <?php if($chanel['is_publish']) echo "checked='true'"; ?>   >Published
                                </label>
                                </div>
                                 <div class="controls" style="margin-left:200px;">
                                <label class="checkbox line" >
                                    <input type="checkbox" name="is_oflo"  <?php if ($chanel['is_oflo']==y){   echo "checked='true'";} ?> >Breaking Movement Channel
                                </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Breaking Movement Channel User:</label>
                                <?php
                                foreach ($channel_users as $channel_user) : ?>
                                    <div class="controls" style="margin-left:200px;">
                                        <label class="checkbox line" >
                                            <input type="checkbox" name="channel_user[][<?=$channel_user['id']?>]" <?php if($channel_user['is_oflo'] != 'n') echo "checked='true'"; ?>   ><?=$channel_user['username']?>
                                        </label>
                                    </div>
                                <? endforeach; ?>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Enable Banner AD's:</label>
                                <div class="controls" style="margin-left:200px;">
									<div class="row-fluid">
				                        <div class="span4">
			                                <label class="checkbox line" >
			                                    <input type="checkbox" name="enable_banner_ads_ios" <?php if($chanel['enable_banner_ads_ios']) echo "checked='true'"; ?>   >iOS
			                                </label>
				                        </div>
				                        <div class="span4">
			                                <label class="checkbox line" >
			                                    <input type="checkbox" name="enable_banner_ads_android" <?php if($chanel['enable_banner_ads_android']) echo "checked='true'"; ?>   >Android
			                                </label>
			                            </div>
				                        <div class="span4">
			                                <label class="checkbox line" >
			                                    <input type="checkbox" name="enable_banner_ads_web" <?php if($chanel['enable_banner_ads_web']) echo "checked='true'"; ?>   >Web app
			                                </label>
			                            </div>
			                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">

                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?php echo ((isset($chanel['image_path']) and $chanel['image_path']) ? "http://d3hl6zmfu9utfu.cloudfront.net/".$chanel['slug']."/assets/ufile/".$chanel['image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 400px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image" class="default"/></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
					              <div class="row-fluid">
                        <div class="span6">
                          <div class="control-group">
                          	<center>Brightcove Player ID</center>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">iOS</label>
                                <div class="controls">
                                <?php  echo form_input("bc_playerid_ios",$chanel['bc_playerid_ios'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Android</label>
                                <div class="controls">
                                <?php  echo form_input("bc_playerid_android",$chanel['bc_playerid_android'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Web</label>
                                <div class="controls">
                                <?php  echo form_input("bc_playerid_web",$chanel['bc_playerid_web'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Minimum Inline Video Length(Seconds)</label>
                                <div class="controls">
                                <?php  echo form_input("min_inline_video_len",$chanel['min_inline_video_len'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                        <div class="span6"><div class="control-group">
                          	<center>Ad Server URL</center>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">iOS</label>
                                <div class="controls">
                                <?php  echo form_input("ad_server_url_ios",$chanel['ad_server_url_ios'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Android</label>
                                <div class="controls">
                                <?php  echo form_input("ad_server_url_android",$chanel['ad_server_url_android'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Web</label>
                                <div class="controls">
                                <?php  echo form_input("ad_server_url_web",$chanel['ad_server_url_web'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Mobile</label>
                                <div class="controls">
                                <?php  echo form_input("ad_server_url_mobile",$chanel['ad_server_url_mobile'],'class="large m-wrap"');?>
                                </div>
                          </div>
                          <div class="control-group">
                                <label class="control-label" for="url">Desktop</label>
                                <div class="controls">
                                <?php  echo form_input("ad_server_url_desktop",$chanel['ad_server_url_desktop'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                     </div>
                     <div class="row-fluid">
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Google Analytics Key</label>
                                <div class="controls">
                                <?php  echo form_input("google_analytics_key",$chanel['google_analytics_key'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                        <? /*
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">CPM</label>
                                <div class="controls">
                                <?php  echo form_input("current_cpm",$chanel['current_cpm'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                        */ ?>
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Starstats ID</label>
                                <div class="controls">
                                <?php  echo form_input("starstats_site_id",$chanel['starstats_site_id'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                      </div>
                    <div class="row-fluid">
                      <div class="span6">
                        <div class="control-group">
                              <label class="control-label" for="url">Quantserve Key</label>
                              <div class="controls">
                              <?php  echo form_input("quantserve_key",$chanel['quantserve_key'],'class="large m-wrap"');?>
                              </div>
                        </div>
                      </div>
                      <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Brightcve Key</label>
                                <div class="controls">
                                <?php  echo form_input("brightcove_key",$chanel['brightcove_key'],'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Google Ad Top - 728px x 90px</label>
                                <div class="controls">
                                <?php  echo form_textarea("top_ad_unit",base64_decode($chanel['top_ad_unit']),'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Google Ad Right Top - 300px x 250px</label>
                                <div class="controls">
                                <?php  echo form_textarea("right_top_ad_unit",base64_decode($chanel['right_top_ad_unit']),'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                      </div>
                      <div class="row-fluid">
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Google Ad Right Bottom - 300px x 600px</label>
                                <div class="controls">
                                <?php  echo form_textarea("right_bottom_ad_unit",base64_decode($chanel['right_bottom_ad_unit']),'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                        <div class="span6">
                          <div class="control-group">
                                <label class="control-label" for="url">Mobile Ad - 320px x 50px</label>
                                <div class="controls">
                                <?php  echo form_textarea("mobile_ad_unit",base64_decode($chanel['mobile_ad_unit']),'class="large m-wrap"');?>
                                </div>
                          </div>
                        </div>
                      </div>
                    <div class="form-actions">
                        <?php echo form_submit('submit', "Save",' class="btn blue"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo form_submit('delimg', "Delete image",' class="btn blue"');?>
                    </div>

                    <?php echo form_close();?>

                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    <!-- END PAGE CONTENT--> 
    
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 
                  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>
 <script src="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.js"></script>
<link href="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.css" rel="stylesheet" />

<!-- END PAGE --> 

