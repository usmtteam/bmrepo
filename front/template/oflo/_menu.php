<body>
    <div class="mobile-width" style="margin-top:10px;">
        <div class="header clearfix fixed-width">
            <nav class="navbar-default box-menu menu">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand menu-brand" href="http://<?=OFLO_HOME?>/"><img src="<?= CDN_FRONT ?>/assets/images/bmc.png" class="oflo-img"></a>
                          <ul class="nav navbar-nav navbar-right social-nav hidden-sm hidden-md hidden-lg" style="padding-top: 7px;">
                            <li class="inline-social"><a class="inline-social" href="https://www.facebook.com/OfloApp"><i class="fa fa-lg fa-facebook"></i></a></li>
                            <li class="inline-social"><a class="inline-social" href="https://twitter.com/OfloApp"><i class="fa fa-lg fa-twitter"></i></a></li>
                            <li class="inline-social"><a class="inline-social" href="https://instagram.com/ofloapp/"><i class="fa fa-lg fa-instagram"></i></a></li>
                        </ul>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        
                        <form class="navbar-form navbar-right hidden-sm hidden-md hidden-lg" action="http://<?=OFLO_HOME?>/template/oflo/search.php" style="display:inline-block; padding-left:10px; padding-right:0px; margin-right:0px;">
                            <div class="form-group search" style="padding-right: 0px; margin-right:0px;">
                                <input type="text" name="search" placeholder="Search" value="<?=$search?>" class="form-control search-box" style="width:18em; padding-right:0px; margin-right: 0px;">
                                   <span class="fa fa-search hidden-xs"></span>
                               </input>
                            </div>
                          </form>
                        <strong><ul class="nav navbar-nav navbar-left">
                        <li><a href="#">CATEGORIES</a><ul>   
                            <?php
                  
                            for($i=0; $i<=5; $i++){ ?>
                      <li><a href="http://<?=OFLO_HOME?>/category/<?php echo strtolower($l_xOfloCategories[$i]['cat_name']); ?>">
                      <?php if ($l_xOfloCategories[$i]['oflo_menu'] ==1){echo $l_xOfloCategories[$i]['cat_name'];} ?>
                    </a></li>

                       <?php } ?></ul></li>
                        <li><a href="/creators/">COACHES</a></li>
                        <li><a href="http://breakingmuscle.com">BREAKING MUSCLE</a></li>
                        <? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
                          <li><a href="https://www.facebook.com/BreakingMuscle"><i class="fa fa-lg fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/BreakingMuscle"><i class="fa fa-lg fa-twitter"></i></a></li>
                            <li><a href="https://instagram.com/breakingmuscle/"><i class="fa fa-lg fa-instagram"></i></a></li>
                        <? } ?>

                          
                        </ul></strong>

                         <!-- social media icons -->
                        <ul class="nav navbar-nav navbar-right social-nav hidden-xs">
                            <li><a href="https://www.facebook.com/BreakingMuscle"><i class="fa fa-lg fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/BreakingMuscle"><i class="fa fa-lg fa-twitter"></i></a></li>
                            <li><a href="https://instagram.com/breakingmuscle/"><i class="fa fa-lg fa-instagram"></i></a></li>
                        </ul>
                        <form class="navbar-form navbar-right hidden-xs"  id="search_form"  action="http://<?=OFLO_HOME?>/template/oflo/search.php"  style="display:inline-block; padding-left:10px; padding-right:0px; margin-right:7px;">
                            <div class="form-group search" style="padding-right: 0px; margin-right:0px">
                                <input type="text" id="search" name="search" placeholder="Search" value="<?=$search?>" class="form-control search-box" style="width:12em; padding-right:0px; margin-left: -10px;">
                                   <span class="fa fa-search hidden-xs"></span>
                               </input>
                            </div>
                        </form>




                    </div><!-- /.navbar-collapse -->
               </div><!--/.container-fluid -->
            </nav>
        </div>

        <script>
            ;(function (document) {

              var minSearch = 1,

              filter = /[^0-9a-z]/ig;

              document.getElementById('search_form').onsubmit = function () {
                var search = document.getElementById('search').value,
                terms = search.replace(filter,'');
                if (terms.length >= minSearch) {
                  return true;
                }

                return false;
              }
            }(window.document));
        </script>