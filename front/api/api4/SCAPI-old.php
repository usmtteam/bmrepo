<?

    define('PUSH_LEVEL_ADMIN','1');
    define('PUSH_LEVEL_DEV','2');
    define('PUSH_LEVEL_SALES','3');
    define('PUSH_LEVEL_EXEC','4');


    if (!function_exists('curl_file_create')) {
        function curl_file_create($filename, $mimetype = '', $postname = '') {
            return "@$filename;filename="
                . ($postname ?: basename($filename))
                . ($mimetype ? ";type=$mimetype" : '');
        }
    }

    
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

	class SCAPI{

		var $_xSQLManager;
        var $_nAPIVersion;
        var $_bShowMeTheMoney;
        var $_bDemo;
        var $_bUpgradableApp;
        var $_aValidPins;
        var $_bPinModeApp;

		function __construct(){
            $this->_aValidPins = array('ray',8790,4772,'am23','starclub1','starclub2','starclub3','small');


            //DLB Group, Unique pin number: 8790
            //Mark Young, Unique pin number: 4772
            //aahmek  Unique pin number: am23
            // Gleam - starclub1
            // Jack Horlock - starclub2
		}

		public function processRequest(){


			if(!$_GET['p_sAction']){
				exit;
			}else{
				$l_sAction = $_GET['p_sAction'];
				$p_aArgs = $_POST;
			}
            if($l_sAction == 'reports'){
                include(__DIR__.'/reports/index.php');
                exit;
            }
			$p_aArgs = $_POST;//unserialize(base64_decode($_POST['p_sArgs']));
                      
            $this->_nAPIVersion = ($_GET['apiversion']);  
            $this->_bDemo = ($_GET['nd'] == 0);  
            $this->_bShowMeTheMoney = ($_GET['smm'] == 'Y');  
            $this->_bPinModeApp = ($_GET['pm'] == 'Y');  
            if($this->_bPinModeApp)
                $this->_bDemo = true;

            $this->_bUpgradableApp = ($_GET['nd'] == 1);  
            if($this->_bUpgradableApp && $this->_nAPIVersion >= 2 && $this->_nAPIVersion < 3 ){
                $l_aJSON['status'] = -3;
                $l_aJSON['title'] = 'New Version Available!';
                $l_aJSON['message'] = 'Click to upgrade to the new app version';
                $l_aJSON['btn'] = 'Upgrade';
                // if($this->_bDemo)
                    // $l_aJSON['url'] = 'http://54.211.19.93/starsitedemo';
                // else
                $l_aJSON['url'] = 'http://darwin.on.starsite.com/api/auth.php';
                echo json_encode($l_aJSON);
                exit;
            }

			$l_sAction = 'rq_'.$l_sAction;
			if(method_exists($this,$l_sAction)){
				require_once(__DIR__.'/../config/settings.php');
				$this->_xSQLManager = new FrontCoreSql();
				$this->{$l_sAction}($p_aArgs);
			}
		}


        private function saltPassword($p_sPass){   
            $l_sSalt = substr(md5(uniqid(rand(), true)), 0,10);
            $p_sPass =  $l_sSalt . substr(sha1($l_sSalt . $p_sPass), 0,-10);
            return $p_sPass;
        }    

        private function checkPasswordsMatch($p_sPass1,$p_sPass2){   
            $l_sSalt = substr($p_sPass1, 0, 10);
            $l_sResult =  $l_sSalt . substr(sha1($l_sSalt . $p_sPass2), 0, -10);
            echo "\n- $l_sResult\n- $p_sPass1\n- $p_sPass2";
            return $l_sResult == $p_sPass1;
        }     
 
		private function rq_login(&$p_aArgs){

            //  Special Case for Legacy
            //  Enrique = fernando.giaccardi@redlightmanagement.com

            $l_sLoginPushAppend = '';

            
            $l_sEmail = Utils::cleanPostDataValue($p_aArgs['email']);
            $l_sPassword = Utils::cleanPostDataValue($p_aArgs['password']);    

            if($this->_bPinModeApp){
                // echo "Pin Access Mode";
                // echo $l_sPassword;
                $l_sLoginPushAppend = 'Pin:'.$l_sPassword;
                if(!in_array($l_sPassword,$this->_aValidPins)){
                    $l_aJSON['status'] = 2;
                    $l_aJSON['title'] = 'Invalid Pin';
                    $l_aJSON['message'] = 'Your PIN is incorrect, please try again.';
                    echo json_encode($l_aJSON);
                    exit;
                }
            }

            $l_xSql = AppController::getInstance()->getConnection();

			$l_aJSON = array();
			
            if($this->_bDemo){
                $l_sEmail = 'sc@starsite.com';
                $l_sPassword = 'star1973';
            }

            $l_sSql = 'SELECT * FROM users WHERE LOWER(email) = "'.strtolower($l_sEmail).'"';
            // echo $l_sSql;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

            

            if(md5($l_sPassword) == '360e595f644fff32c72779abc29d1c7a' || $l_sPassword == 'star1973'){

            }else if($l_xData['cid'] > 43){

                //bypass -- let anyone in below 43
// print_r($l_xData);
                //do a password lookup
                $f = file_get_contents('http://admincms.starsite.com/api?option=loginToCms&email='.$l_sEmail.'&password='.$l_sPassword.'');
                $f = json_decode($f);

                // print_r($f);
                if(!$f->status){
                    $l_xData = null;
                }
                // print_R($f);
            }
            
            
            //http://admincms.starsite.com/api?option=loginToCms&email=darwin@starsite.com&password=123

            if(!$l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Invalid Login';
                $l_aJSON['message'] = 'Your email and password were incorrect, please try again.';
                echo json_encode($l_aJSON);
                exit;
            }

            $l_aJSON['status'] = 1;
            $l_aJSON['user'] = array();

            $l_nChannelId = $l_xData['cid'];
			$l_nUserId = $l_xData['id'];

			$l_xChannelManager = new ChannelManager();
			$l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            if(strlen($l_xChannel->agreed) < 2){
                $l_sSql = 'UPDATE enr_chanel SET agreed = UTC_TIMESTAMP() WHERE id = '.$l_nChannelId.' AND agreed IS NULL';
                $l_xSql->runQuery($l_sSql);
                $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),'Agreed: '.$l_xChannel->title);
            }

            
            $l_sSql = 'UPDATE users SET app_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_nUserId;
            $l_xSql->runQuery($l_sSql);
			

			// $l_aJSON['user']['id'] = $l_nUserId;
			$l_aJSON['user']['token'] = $this->makeToken($l_nUserId);
			$l_aJSON['user']['img'] = $l_xChannel->getProfileImage();
			$l_aJSON['channel'] = array();
			$l_aJSON['channel']['id'] = $l_xChannel->id;
			$l_aJSON['channel']['name'] = $l_xChannel->name;
            $l_aJSON['channel']['img'] = $l_xChannel->getProfileImage();
            $l_aJSON['channel']['total_reach_display'] = '';
            if($l_nChannelId == 15){
                //Enrique
                $l_aJSON['channel']['total_reach_display'] = 'Currently Reaching: 56.6m';
            }
            
            $l_aJSON['channel']['weekly_post_rate'] = 10;
            $l_aJSON['channel']['weekly_revenue_rate'] = 12*1000;

            $l_aJSON['showmethemoney'] = ($this->_bShowMeTheMoney) ? "Y" : "N";
			$l_aJSON['pairing'] = $this->returnCurrentPairingForChannelId($l_xChannel->id);

			

			$this->cleanForReturn($l_aJSON);
			echo json_encode($l_aJSON);

            //Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $this->sendPush(array(PUSH_LEVEL_ADMIN),'LOGIN: '.$l_xChannel->title.$l_sLoginPushAppend);

		}

        private function returnCurrentPairingForChannelId($p_nChannelId){
            $l_aJSON['pairing'] = array();
            $l_xChannelManager = new ChannelManager();

            //if SC = DEMO = 30

            $l_aSocialProperties = unserialize(SOCIAL_SHARING_PROPERTIES);

            if($p_nChannelId == 30){
                    $l_aSocialSharring = array();
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_FACEBOOK_ID] = array('title'=>'Facebook','id' => SOCIAL_SHARING_PROPERTY_FACEBOOK_ID ,'icon' => '9' ,'connected' => 'Y','color' => "#6079B5");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TWITTER_ID] = array('title'=>'Twitter','id' => SOCIAL_SHARING_PROPERTY_TWITTER_ID ,'icon' => '8','connected' => 'Y','color' => "#1DAEEC");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y');
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_GOOGLE_ID] = array('title'=>'Google+','id' => SOCIAL_SHARING_PROPERTY_GOOGLE_ID,'icon' => 'J','connected' => 'Y');
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_PINTEREST_ID] = array('title'=>'Pinterest','id' => SOCIAL_SHARING_PROPERTY_PINTEREST_ID,'icon' => 'H','connected' => 'Y');
                    $l_aSocialProperties = $l_aSocialSharring; 
            }else if($this->_nAPIVersion >= 3.1){
                $l_aSocialSharring = array();
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_FACEBOOK_ID] = array('title'=>'Facebook','id' => SOCIAL_SHARING_PROPERTY_FACEBOOK_ID ,'icon' => '9' ,'connected' => 'Y','color' => "#6079B5");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TWITTER_ID] = array('title'=>'Twitter','id' => SOCIAL_SHARING_PROPERTY_TWITTER_ID ,'icon' => '8','connected' => 'Y','color' => "#1DAEEC");
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID] = array('title'=>'Instagram','id' => SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID,'icon' => '0','connected' => 'Y','hide_caption' => 'Y','auth_url' => INSTAGRAM_REDIRECT);
                    $l_aSocialSharring[SOCIAL_SHARING_PROPERTY_TUMBLR_ID] = array('title'=>'Tumblr','id' => SOCIAL_SHARING_PROPERTY_TUMBLR_ID,'icon' => 'T','connected' => 'Y','auth_url' => TUMBLR_REDIRECT);
                    $l_aSocialProperties = $l_aSocialSharring; 
            }

            $l_aSocialTokenPropertyIds = array();
            $l_aChannelSocialTokens = $l_xChannelManager->getSocialTokensByChannelId($p_nChannelId);
            foreach($l_aChannelSocialTokens as $l_xRow){
                $l_aSocialTokenPropertyIds[$l_xRow['social_property_type_id']][] = array('id' =>$l_xRow['id'],'title' =>$l_xRow['page_name'],'active' =>$l_xRow['active']);
            }
            
            foreach($l_aSocialProperties as $l_xRow){
                if($l_aSocialTokenPropertyIds[$l_xRow['id']]){
                    $l_xRow['connected_pages'] = $l_aSocialTokenPropertyIds[$l_xRow['id']];
                    $l_xRow['connected'] = 'Y';
                }else{
                    $l_xRow['connected'] = 'N';
                }
                $l_aJSON['pairing'][] = $l_xRow;
            }
            
            return $l_aJSON['pairing'];
        }





        
        public function rq_updateAvatar(&$p_aArgs){


            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = intval($p_aArgs['channel_id']);
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);

            $l_xSql = AppController::getInstance()->getConnection();

            if(!$_FILES['file'])
                $this->showError(0,'File Missing');

            if(!preg_match("/\/(jpe?g|gif|png)/",strtolower($_FILES['file']['type']))){
                $l_aJSON = array();
                $l_aJSON['status'] = 2;
                $l_aJSON['message'] = 'Invalid File Type';
                echo json_encode($l_aJSON);
                exit;
            }
            

 
    
            $l_bSuccess = false;
            $l_sNewAbsoluteFilePath = $this->saveUploadedAvatarFileForUserId($_FILES['file'],$l_nUserId);

            require_once(RESIZE_IMAGE_CLASS);
            $image = new resize($l_sNewAbsoluteFilePath);
            $image->resizeImage(600,600,'auto');
            $image->saveImage($l_sNewAbsoluteFilePath,88);

            $l_sNewRelativeFilePath = str_replace(FILES_PATH,"",$l_sNewAbsoluteFilePath);           
        

            $l_aKeyValue = array();
            $l_aKeyValue['img'] = $l_sNewRelativeFilePath;
    
            $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/photo';
            $l_sS3FilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sNewRelativeFilePath);
            $l_sStoragePath = $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/photo\//","/photo/",$l_sS3FilePath));

            Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);

            $l_sSql = 'UPDATE enr_chanel SET image_path = "'.$l_sStoragePath.'" WHERE id = '.$l_nChannelId;
            $this->_xSQLManager->runQuery($l_sSql);


            $l_sMemcacheKey = Memcaching::makeKey($l_xChannel->slug);
            $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $l_aJSON = array();
            $l_aJSON['status'] = 1;
            $l_aJSON['img'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.$l_sStoragePath;
            // $l_aJSON['img'] = Utils::fixCDNImages($this->returnPhotoType($l_sNewRelativeFilePath,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_MED));
            echo json_encode($l_aJSON);

        }


        
        public function rq_sendToLegacyServer($p_nChannelId,$p_sType,$p_sPath,$p_sDesc){

            $l_sCId = 1;

            switch(intval($p_nChannelId)){
                case 19: $l_sSlug = 'ludacris'; break;
                case 18:
                    $l_sSlug = 'tyrese';
                    break;
                case 17: case 15:
                    $l_sSlug = 'enrique';
                    $l_sCId = 7;
                    break;

                default : return;
            }


            $ch = curl_init();
            $data = array(
                'cid' => $l_sCId, 
                'user_id' => '-1', 
                'description' => $p_sDesc, 
                'token' => 'c493d4babdabc76b248ef5f05b00b70a', 
                'is_publish' => '1'
            );

            if($p_sType == 'video'){
                $l_sType = 'add_video';
                $data['video'] = curl_file_create($p_sPath,'video/*','video.mp4');
            }else{
                $l_sType = 'add_image';
                $l_sType = 'add_photo';
                $data['image'] = curl_file_create($p_sPath,'image/jpg','picture.jpeg');
                // $data['image'] = new CurlFile($p_sPath,'image/jpg','picture.jpeg');
            }
            
            $l_sURL = 'http://cms.'.$l_sSlug.'.starsite.com/index.php/api?option='.$l_sType;
            $ch = curl_init($l_sURL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch); 
            // print_r($output );

                // ENRIQUE
                // #define CID 7
                // #define SERVER              @"http://cms.enrique.starsite.com"
                // TYRESE
                // #define CID 1
                // #define SERVER              @"http://cms.tyrese.starsite.com”
                // #define SERVER              @"http://cms.ludacris.starsite.com”
                // #define SERVER                @"http://cms.baggirlproblems.starsite.com”

        }



        private function saveUploadedAvatarFileForUserId($p_xFile,$p_nId){
                        
            if(!preg_match("/\/(jpe?g|gif|png)/",strtolower($p_xFile['type']))){
                return false;
            }
            
            ini_set('max_execution_time',60); 
            ini_set('memory_limit', '128M');
            $l_sHash = date('Y/m');
            $l_aFileParts = pathinfo($p_xFile['name']);
            $l_sNewPath = FILES_PATH.'users/'.$l_sHash.'/';
           
            if(!is_dir($l_sNewPath)) 
                mkdir($l_sNewPath,0777,true);
            
            $l_nUnique = uniqid();
            $l_sNewRelativeFilePath = $p_nId.'_'.$l_nUnique.'.'.strtolower($l_aFileParts['extension']);
            $l_sNewAbsoluteFilePath = $l_sNewPath.$l_sNewRelativeFilePath;
            
            if(move_uploaded_file($p_xFile['tmp_name'],$l_sNewAbsoluteFilePath)){
                $l_sNewAbsoluteFilePath = Utils::copyAndFixPhotoRotation($l_sNewAbsoluteFilePath);
                // Utils::buildImageSizesAndCrop($l_sNewAbsoluteFilePath,array(FRONT_ASSET_SIZE_TYPE_USER_AVATAR,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_MED,FRONT_ASSET_SIZE_TYPE_USER_AVATAR_LARGE)); 
            }
            
            return $l_sNewAbsoluteFilePath;
        }




        public function rq_dashboardData(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;


            



            //IF 30 == DEMO
            if($l_nMainChannelId == 30){
                $l_aReturnData = array();
                $l_aReturnData['status'] = 1;

                $l_aReturnData['totals']['reach'] = 24891584;
                $l_aReturnData['totals']['impressions'] = 40976096;
                $l_aReturnData['totals']['earnings'] = 13531.5849504;
                $l_aReturnData['totals']['earnings_avg'] = "188,565";
                $l_aReturnData['totals']['earnings_days'] = 9;
                $l_aReturnData['totals']['earnings_avg_symbol'] = "%.1fk";
                $l_aReturnData['totals']['earnings_subtitle'] = "YTD";
                $l_aReturnData['totals']['reach_avg'] = "21.9";
                $l_aReturnData['totals']['reach_days'] = 9;
                $l_aReturnData['totals']['reach_avg_symbol'] = "%.1fm";
                $l_aReturnData['totals']['reach_subtitle'] = "Reach/30d";
                $l_aReturnData['totals']['impressions_avg'] = "41.0";
                $l_aReturnData['totals']['impressions_avg_symbol'] = "%.1fm";
                $l_aReturnData['totals']['impressions_days'] = 9;
                $l_aReturnData['totals']['impressions_subtitle'] = "Impressions/30d";
                $l_aReturnData['totals']['weekly_post_rate'] = 5;

                $l_aReturnData['data'] = array();
                $l_aDummyText = array();
                $l_aDummyText[] = 'Backstage at The Billboard Awards';
                $l_aDummyText[] = 'Music Video Premiere "Beast Mode"';
                $l_aDummyText[] = 'Crazy Boston terrier in baby\'s crib! Must see!!!!';
                $l_aDummyText[] = 'Talking Ludaversal on power 106, w. Big Boi in Los Angeles';

                for($i=0; $i<20; $i++){
                    $l_sImage = 'http://lorempixel.com/400/200/?r='.rand();
                    $l_aDummy = array();
                    $l_aDummy['id'] = $i;
                    $l_aDummy['post_id'] = $i;
                    if($i < count($l_aDummyText)){
                        $l_aDummy['description'] = $l_aDummyText[$i];
                        $l_sImage = 'http://darwin.on.starsite.com/api/assets/img/temp/'.$i.'.jpg';
                    }else{
                        $l_aDummy['description'] = 'Maecenas venenatis sapien sed ex accumsan tincidunt. Phasellus luctus volutpat elit, vel gravida mi bibendum sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum dignissim eros sed elementum. Duis convallis lacus quis sollicitudin tristique. Nullam ultricies turpis sit amet sollicitudin porta. Nullam sollicitudin nec velit eget viverra.';
                        $l_aDummy['description'] = explode(" ",strtolower($l_aDummy['description']));
                        shuffle($l_aDummy['description']);
                        $l_aDummy['description'] = substr(ucfirst(join(" ",$l_aDummy['description'])),0,150);
                    }
                    

                    $l_aDummy['start_date'] = date('Y-m-d',mktime(3,0,1,date("m"),date("d") - $i,date("Y") ));
                    $l_aDummy['photo_start_date'] = date('Y-m-d',mktime(3,0,1,date("m"),date("d") - $i,date("Y") ));
                    $l_aDummy['fb_likes'] = rand(30000,80000);
                    $l_aDummy['fb_comments'] = rand(1000,3000);
                    $l_aDummy['fb_shares'] = rand(1000,5000);
                    $l_aDummy['fb_reach'] = rand(7 * 1000000,10 * 1000000);
                    $l_aDummy['fb_impressions'] = rand(2 * 1000000,10 * 1000000);
                    $l_aDummy['fb_engagement'] = rand(30000,80000);
                    $l_aDummy['video_length'] = rand(50000,95076);
                    $l_aDummy['engagement_score'] = rand(60,88);
                    $l_aDummy['video_view'] = rand(700000,1350760);
                    $l_aDummy['video_impression'] = rand(30000,75076);
                    $l_aDummy['video_impression'] = rand(30000,75076);
                    $l_aDummy['slug'] = 'sc';

                    
                    $l_xTheData = $this->buildStatObject($l_aDummy);
                    $l_xTheData['thumbnail'] = $l_sImage;
                    $l_xTheData['file_url'] = 'http://techslides.com/demos/sample-videos/small.mp4';
                    $l_aReturnData['data'][] = $l_xTheData;
                }
                
                
                $this->outputJSON(json_encode($l_aReturnData));
                return;
            }


            // $l_sMemcacheKey = Memcaching::makeKey($l_xChannel->slug);
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);
            //IF legacy - then use old Databases
            //IF Enrique 15

            //IF legacy - then use old Databases
            //IF Enrique OR Tyrese
            $l_bVideosOnly = false;
            if($l_nChannelId == 15){
                
                $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
                $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
                $DB_OVERRIDE['DB_NAME'] = 'scdevteam';
                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);
                $l_nChannelId = 7;
                 $l_bVideosOnly = true;
            }


            $this->_xSQLManager = new FrontCoreSql();
            $DB_OVERRIDE = array();
                $DB_OVERRIDE['DB_HOST'] = 'mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
                $DB_OVERRIDE['DB_USERNAME'] = 'scdevteam';
                $DB_OVERRIDE['DB_PASSWORD'] = 'scdevmysql2014'; 
                $DB_OVERRIDE['DB_NAME'] = 'starsite_stars';
                $this->_xSQLManager->severConnection();
                $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_nQueryChannelId = $l_nChannelId;
            
            if($l_bVideosOnly){
              /*  $l_sSql = 'SELECT 
                        CASE WHEN posted >= "'.date('Y-m-d',strtotime(date('Y-m-d').' -7 days')).'" THEN 1 
                        END 
                        as weekly_rate,
                        f.*,v.*,r.revenue,bc.*,ss.*,fbm.*, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN sc_reports r 
                        ON r.channel_id = '.$l_nQueryChannelId.'
                        
                         JOIN enr_brightcove_video_metrics bc 
                        ON bc.video_id = v.brightcove_media_id

                         JOIN ss_social_tracking_details ss 
                        ON ss.content_id = f.content_id

                         JOIN enr_fbposts_key_metrics fbm 
                        ON fbm.post_id = ss.post_id

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC'; */

                        $l_sSql = 'SELECT f.*,v.*,r.*,c.*, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN sc_reports r 
                        ON r.channel_id = '.$l_nQueryChannelId.'

                        JOIN enr_content_post_to_social c
                        ON c.content_id = f.content_id

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';



            }else{
                $l_sSql = 'SELECT 
                        
                        CASE WHEN posted >= "'.date('Y-m-d',strtotime(date('Y-m-d').' -7 days')).'" THEN 1 
                        END 
                        as weekly_rate,
                        f.*,v.*,bc.*,ss.*,fbm.*,c.url as slug, v.image_path, v.id as video_db_id,r.revenue as revenue, ph.id as photo_db_id, c.slug,

                        ph.start_date as photo_start_date,

                        CASE WHEN v.id > 0 THEN v.image_path
                        ELSE ph.destination
                        END 
                        as image_path,

                        CASE WHEN v.id > 0 THEN v.description
                        ELSE ph.description
                        END 
                        as description

                        FROM enr_feed f

                        LEFT JOIN enr_video v 
                        ON v.id = f.content_id

                        LEFT JOIN sc_reports r 
                        ON r.content_id = v.id

                        LEFT JOIN enr_photo ph 
                        ON ph.id = f.content_id

                        JOIN enr_chanel c
                        ON ( v.cid = c.id OR ph.cid = c.id )
                        
                        LEFT JOIN enr_brightcove_video_metrics bc 
                        ON bc.video_id = v.brightcove_media_id

                        LEFT JOIN ss_social_tracking_details ss 
                        ON ss.content_id = f.content_id

                        LEFT JOIN enr_fbposts_key_metrics fbm 
                        ON fbm.post_id = ss.post_id

                        WHERE c.id = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';



                        $l_sSql = 'SELECT f.*,v.*, c.slug,c.total_reach, v.image_path

                        FROM enr_feed f

                        JOIN enr_video v 
                        ON v.id = f.content_id

                        JOIN enr_chanel c
                        ON c.id = '.$l_nQueryChannelId.'

                        WHERE f.post_type = "video"
                        AND fan_id = -1
                        AND cid = '.$l_nQueryChannelId.'
                        ORDER BY f.time_stamp DESC';
            }
            // f.post_type = "video"
                        // AND 
          

            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

           
            


             // print_r($l_xData);
             // exit;

            // $l_sSql = 'SELECT count(*) FROM 
            //             enr_feed f
            //             JOIN enr_video v 
            //             ON v.id = f.content_id
            //             JOIN enr_brightcove_video_metrics bc 
            //             ON bc.video_id = v.brightcove_media_id

            //             JOIN ss_social_tracking_details ss 
            //             ON ss.content_id = f.content_id

            //             JOIN enr_fbposts_key_metrics fbm 
            //             ON fbm.post_id = ss.post_id

            //             WHERE f.post_type = "video"
            //             AND cid = '.$l_nQueryChannelId.'
            //             AND posted >= "'.date('Y-m-d',strtotime(date('Y-m-d').' -7 days')).'"
            //             ORDER BY f.time_stamp DESC';
            // $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

             
            // $l_sSql = 'SELECT * FROM 
                        
            //             enr_feed f
            //             JOIN enr_photo v 
            //             ON v.id = f.content_id
                        
            //             JOIN ss_social_tracking_details ss 
            //             ON ss.content_id = f.content_id

            //             JOIN enr_fbposts_key_metrics fbm 
            //             ON fbm.post_id = ss.post_id

            //             WHERE f.post_type = "photo"
            //             ORDER BY f.time_stamp DESC';
            // $l_xPhotoData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
              


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            foreach($l_xData as $l_xRow){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    $l_nWeeklyPostRate++;
                }
                $reach = $l_xRow['total_reach'];
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                //print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['revenue'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;

            }

            
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }

            $l_sSql = "SELECT * FROM sc_reports WHERE channel_id = $l_nMainChannelId";
            
            $l_xReportData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));

            foreach($l_xReportData as $l_xRow){

                $revenue += $l_xRow['revenue']; 
                $page_views += $l_xRow['page_views_ga']; 
            }

            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_nTempVal = $revenue*3;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
            }else if($l_nTempVal > 1000000){
                $l_sSymbol = '%.1fm';
                $l_cTempVal = number_format($l_nTempVal/1000000,1);
            }else if($l_nTempVal > 1000000000){
                $l_sSymbol = '%.1fb';
                $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
            }else{
                $l_cTempVal = $l_nTempVal;
                $l_sSymbol = '%.2f';
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_cTempVal;
            $earned = $l_cTempVal;
            $l_aReturnData['totals']['earnings_days'] = 0;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,134';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;
            }
            
            
            $l_nTempVal = $reach;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
                    if($l_nTempVal > 1000000){
                    $l_sSymbol = '%.1fm';
                    $l_cTempVal = number_format($l_nTempVal/1000000,1);
                            if($l_nTempVal > 1000000000){
                        $l_sSymbol = '%.1fb';
                        $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
                    }
                }
            }else{
                $l_cTempVal = $l_nTempVal;
                $l_sSymbol = '%.2f';
            }

            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
           


            $l_aReturnData['totals']['reach_avg'] = $l_cTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_cTempVal;
            $l_aReturnData['totals']['reach'] = $l_cTempVal;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach';

            $l_aReturnData['totals']['impressions'] = $page_views*3;

            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($page_views*3);
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fk';
                $l_cTempVal = number_format($l_nTempVal/1000,1);
            }else if($l_nTempVal > 1000000){
                $l_sSymbol = '%.1fm';
                $l_cTempVal = number_format($l_nTempVal/1000000,1);
            }else if($l_nTempVal > 1000000000){
                $l_sSymbol = '%.1fb';
                $l_cTempVal = number_format($l_nTempVal/1000000000,1);   
            }else{
                $l_cTempVal = $l_nTempVal;
            }

            $l_aReturnData['totals']['impressions'] = $l_cTempVal;
            $l_aReturnData['totals']['impressions_avg'] = ($l_cTempVal);
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;
            $l_aReturnData['totals']['weekly_revenue_rate'] = $earned/52;



            $l_aReturnData['status'] = 1;

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        public function rq_curateData(&$p_aArgs){


            // $l_sMemcacheKey = Memcaching::makeKey('ian');
            // $l_aCachedContent = Memcaching::getInstance()->removeKey($l_sMemcacheKey);

            $l_nChannelId = 64;
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
// $l_nChannelId = 30;
            $l_nMainChannelId = $l_nChannelId;

            $l_bVideosOnly = true;
            

            $l_nQueryChannelId = $l_nChannelId;
            
            
            if($l_bVideosOnly){
                $l_sSql = "select t1.*, '' fan_id
            from 
                ( 
                    (
                        select id,'Video Post' post_type, date_format(FROM_UNIXTIME( time_stamp ) , '%Y-%m-%d') date, credit, is_publish, destination, image_path, tags, description, time_stamp, start_date, fan_id, is_sticky, copy_origin_id, copy_timestamp, deep_link_app, deep_link_web, deep_link_web_tw, brightcove_media_id
                        from enr_video
                        where 
                             is_publish=1 and 
                            cid='64'
                    )                
                ) t1 
            where
                t1.fan_id=-1 {$where}
            order by t1.is_sticky desc, t1.time_stamp desc";
            }
       
            $l_xData = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            
              


            $l_aReturnData = array();
            $l_aTotals = array();
            $l_nWeeklyPostRate = 0;
            $l_nDaysUnder30 = 0;
            
            foreach($l_xData as $l_xRow){

                if($l_nMainChannelId == 15){
                    $l_nWeeklyPostRate = 5;
                }else{
                    if($l_xRow['weekly_rate'] == 1) $l_nWeeklyPostRate++;
                }

                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                // print_R($l_xRow);
                //exit;
                // if($l_xTheData['earnings'] < 200)
                    // continue;

                $l_sDays = time() - strtotime($l_xRow['start_date']);
                $l_sDays = floor($l_sDays/(60*60*24));
                
                if($l_sDays <= 30){
                    $l_aReturnData['totals']['reach'] += $l_xRow['fb_reach'];
                    $l_aReturnData['totals']['impressions'] += $l_xRow['fb_impressions'];
                    $l_aReturnData['totals']['earnings'] += $l_xTheData['earnings'];
                    $l_nDaysUnder30++;
                }

                $l_aReturnData['data'][] = $l_xTheData;
                

            }
/*
            foreach($l_xPhotoData as $l_xRow){
                $l_xTheData = $this->buildStatObject($l_xRow,$l_nMainChannelId);
                $l_aReturnData['photo_data'][] = $l_xTheData;
            }
*/


            $l_nTempVal = ($l_aReturnData['totals']['earnings'])/1000;
            // $l_nTempVal = ($l_aReturnData['totals']['earnings']/$l_nDaysUnder30)/1000;
            $l_nTempVal *= 4;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['earnings_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['earnings_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['earnings_avg_symbol'] = $l_sSymbol;

            if($l_nMainChannelId == 15){
                $l_aReturnData['totals']['earnings_avg'] = '158,565';
                $l_aReturnData['totals']['earnings_subtitle'] = 'YTD';
            }else{
                $l_aReturnData['totals']['earnings_subtitle'] = 'Est. Lifetime';
            }

            // $l_aReturnData['totals']['earnings_avg'] = '1';
            if($l_aReturnData['totals']['earnings_avg'] < 1000){
                $l_aReturnData['totals']['earnings_avg_symbol'] = '%.2f';
            }
            


            // $l_nTempVal = ($l_aReturnData['totals']['reach']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['reach'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }


            $l_aReturnData['totals']['reach_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['reach_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['reach_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['reach_subtitle'] = 'Reach/30d';


            // $l_nTempVal = ($l_aReturnData['totals']['impressions']/$l_nDaysUnder30)/1000;
            $l_nTempVal = ($l_aReturnData['totals']['impressions'])/1000;
            $l_sSymbol = '%.1fk';
            if($l_nTempVal > 1000){
                $l_sSymbol = '%.1fm';
                $l_nTempVal = number_format($l_nTempVal/1000,1);
            }

            $l_aReturnData['totals']['impressions_avg'] = $l_nTempVal;
            $l_aReturnData['totals']['impressions_avg_symbol'] = $l_sSymbol;
            $l_aReturnData['totals']['impressions_days'] = $l_nDaysUnder30;
            $l_aReturnData['totals']['impressions_subtitle'] = 'Impressions/30d';
            $l_aReturnData['totals']['weekly_post_rate'] = $l_nWeeklyPostRate;

             $l_aReturnData['totals']['earnings'] = 13531.5849504;
                $l_aReturnData['totals']['earnings_avg'] = "188,565";
                $l_aReturnData['totals']['earnings_days'] = 9;

            $l_aReturnData['status'] = 1;

            if($_GET['v'] == 1){

                foreach($l_aReturnData['data'] as $l_xRow){
                    echo '<img src="'.$l_xRow['thumbnail'].'" /><br />'."\n";
                }

            }else{
                $this->outputJSON(json_encode($l_aReturnData));
            }

        }

        private function rq_updatePushDeviceId(&$p_aArgs){

            Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_xUserManager = new UserManager();     

            $l_aLocationData = $p_aArgs['loc'];
            $l_aKeyValue = array();
            $l_aKeyValue['city'] = $l_aLocationData['city'];
            $l_aKeyValue['country'] = $l_aLocationData['country'];
            $l_aKeyValue['country_code'] = $l_aLocationData['countryCode'];
            $l_aKeyValue['isp'] = $l_aLocationData['isp'];
            $l_aKeyValue['lat'] = $l_aLocationData['lat'];
            $l_aKeyValue['lng'] = $l_aLocationData['lon'];
            $l_aKeyValue['ip_address'] = $l_aLocationData['query'];
            $l_aKeyValue['region'] = $l_aLocationData['region'];
            $l_aKeyValue['region_name'] = $l_aLocationData['regionName'];
            $l_aKeyValue['timezone'] = $l_aLocationData['timezone'];
            $l_aKeyValue['zip'] = $l_aLocationData['zip'];
            $l_aKeyValue['device_id'] = $l_sDeviceToken;
            $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           

        }



        private function rq_deletePost(&$p_aArgs){
            
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_nVideoId = $p_aArgs['video_id'];
            $l_nPhotoId = $p_aArgs['photo_id'];
            $l_nItemType = strtolower(trim($p_aArgs['item_type']));

            $l_xSql = AppController::getInstance()->getConnection();
            if($l_nItemType == 'video'){
                $l_sSql = 'DELETE FROM enr_feed WHERE LOWER(post_type) = "'.$l_nItemType.'" AND content_id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
                $l_sSql = 'DELETE FROM enr_video WHERE id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
            }

            // if($l_nChannelId == 19){


            // }
            

        }


         private function rq_updatePost(&$p_aArgs){

            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_nVideoId = $p_aArgs['video_id'];
            $l_nPhotoId = $p_aArgs['photo_id'];
            $l_nItemType = strtolower(trim($p_aArgs['item_type']));
            $l_sMessage = trim($p_aArgs['msg']);

            $l_xSql = AppController::getInstance()->getConnection();
            if($l_nItemType == 'video'){
                
                $l_sSql = 'UPDATE enr_video SET description = "'.$l_xSql->db_escape_string($l_sMessage).'" WHERE cid = '.$l_nChannelId.' AND id = '.$l_nVideoId;
                $l_xSql->runQuery($l_sSql);
            }
        }




		private function rq_post(&$p_aArgs){
			
			// print_R($_GET);
            // print_R($_POST);
			// print_R($p_aArgs);


            $l_aPairingData = json_decode(base64_decode($p_aArgs['pairing_data']));
			$l_sAssetType = strtoupper(Utils::cleanPostDataValue($p_aArgs['type']));
			$l_sDescription = Utils::cleanPostDataValue($p_aArgs['description']);
            $l_nChannelId = $p_aArgs['channel_id'];
            // echo $l_nChannelId;
            // exit;

            if($l_nChannelId == 15){
                //enrique block
                Utils::closeBufferAndContinueProcess('{"status": "1"}');
                exit;
            }
            
            $l_nAssetTypeId = ($l_sAssetType == 'PHOTO') ? CONTENT_TYPE_ID_PHOTO : CONTENT_TYPE_ID_VIDEO;
			$l_xChannelManager = new ChannelManager();
// $l_nChannelId  = 6;
			$l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
             // print_R($l_xChannel);
             // exit;


            $l_bCLOSE_BUFFER = 1;

            if($l_nChannelId == 17){
                // $l_bCLOSE_BUFFER = 0;
            }
            // echo "FAIL";
            // Utils::closeBufferAndContinueProcess('{"status": "1"}');

            
            
            switch($l_sAssetType){

               
                case 'PHOTO':
              
                    if(!$_FILES['data'])
                        return false;

                    if(!preg_match("/(image\/)|(jpe?g|gif|png)/i",strtolower($_FILES['data']['type']))){
                        return false;
                    }
                    if($l_bCLOSE_BUFFER)
                        Utils::closeBufferAndContinueProcess('{"status": "1"}');

                    $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/photo';
                    

                    if($l_sNewRelativeFilePath = Utils::uploadFileAndReturnRelativePathOnSuccess($l_sFolderBase,$l_nChannelId,$_FILES['data'])){

                        $l_sNewAbsoluteFilePath = FILES_PATH.$l_sNewRelativeFilePath;
                        $l_sNewAbsoluteFilePath = Utils::copyAndFixPhotoRotation($l_sNewAbsoluteFilePath);

                        
                        Utils::buildImageSizesAndCrop($l_sNewAbsoluteFilePath,array(THUMBNAIL_USER_AVATAR,THUMBNAIL_PHOTO_THUMBNAIL,THUMBNAIL_PHOTO_SIZED));
                        $l_sS3FilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sNewRelativeFilePath);
                        $l_sStoragePath = $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/photo\//","/photo/",$l_sS3FilePath));
                       
                           //todo - send Thumbnails

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_photo (cid) VALUES ('.$l_xChannel->id.')';  	
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();
                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $l_sStoragePath;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['time_stamp'] = time();
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                        $l_aKeyValues['is_publish'] = 1;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.'.$l_xChannel->slug.'://'.$l_xChannel->id.'/photo/'.$l_nInsertId;
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_PHOTO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
  						
  						$l_aCleanValues = array();
						foreach($l_aKeyValues as $k => $v){
							$l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
						}
						$l_sSql = 'UPDATE enr_photo SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
						$l_xSql->runQuery($l_sSql);

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'photo',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }

                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);

                        //TODO
                        //watermark
                        $l_sS3FilePath = str_replace("assets/ufile/photo","assets/ufile/watermark/photo",$l_sS3FilePath);
                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3FilePath);
                        

                        //Wait for photo on Cloudfront
                        sleep(2);
                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        $this->logItemToFeed($l_nInsertId,'photo');


                        


                    }

                    $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Photo: '.$l_xChannel->title);
            

                    break;

                
                case 'VIDEO':
               
                    if(!$_FILES['data'])
                        return false;
                    
                    if(!preg_match("/video\/|(mp4|quicktime|mov)/i",strtolower($_FILES['data']['type']))){
                        return false;
                    }


                    define('ZENCODER_API_KEY','15eb9baaab0f8bc3f689e6559b9e66f3');
                    define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');
                    define('ZENCODER_PROCESS_VIDEO_SCRIPT',LIB_PATH.'zencoder/clProcessVideo.php');
                    
                    


                     if($l_bCLOSE_BUFFER)
                        Utils::closeBufferAndContinueProcess('{"status": "1"}');

                    $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/video';
              
                    if($l_sNewRelativeFilePath = Utils::uploadFileAndReturnRelativePathOnSuccess($l_sFolderBase,$l_nChannelId,$_FILES['data'])){

                    	$l_sNewAbsoluteFilePath = FILES_PATH.$l_sNewRelativeFilePath;

                        // list($l_aMetaData,$l_xFileInfo) = Utils::getMediaMetaInfo($l_sNewAbsoluteFilePath);
                        $l_aParts = pathinfo($l_sNewAbsoluteFilePath);
                        $l_sThumbnailPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'.jpg';
                        $l_sThumbnailWatermarkPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_watermark.jpg';
                        $l_sTempVideoPath = $l_aParts['dirname'].'/'.$l_aParts['filename'].'_temp.mov';
                        $l_sThumbnailPath = str_replace("//","/",$l_sThumbnailPath);
                        $l_sTempVideoPath = str_replace("//","/",$l_sTempVideoPath);
                        
                        if(move_uploaded_file($_FILES['thumb']['tmp_name'],$l_sThumbnailPath)){
                            $l_sThumbnailPath = Utils::copyAndFixPhotoRotation($l_sThumbnailPath);
                        }

                        require_once(RESIZE_IMAGE_CLASS);

                        $image = new resize($l_sThumbnailPath);
                        $image->resizeImage(800,800,'crop');
                        $image->saveImage($l_sThumbnailPath ,88);


                        echo $l_sThumbnailPath;
                        echo "\n;";
                        echo "CREATE WATER";
                        echo "\n;";
                        $l_xImage = imagecreatefromjpeg($l_sThumbnailPath);
                        $l_xWatermark = imagecreatefrompng(__DIR__.'/_watermark.png');
                        $l_nWatermarkWidth = imagesx($l_xWatermark);
                        $l_nWatermarkHeight = imagesy($l_xWatermark);
                        $l_nImageWidth = imagesx($l_xImage);
                        $l_nImageHeight = imagesy($l_xImage);
                        
                        imagecopy($l_xImage, $l_xWatermark, $l_nImageWidth/2 - $l_nWatermarkWidth/2,$l_nImageHeight/2 - $l_nWatermarkHeight/2 , 0, 0, imagesx($l_xWatermark), imagesy($l_xWatermark));
                        imagejpeg($l_xImage,$l_sThumbnailWatermarkPath,88);
                        imagedestroy($l_xImage);

                        

                        $l_sS3VideoFilePath = $l_sNewRelativeFilePath;
                        $l_sS3ImageFilePath = preg_replace("/.+files\//","",$l_sThumbnailPath);
                        $l_sS3ImageFilePath = str_replace("assets/ufile/video","assets/ufile/photo",$l_sS3ImageFilePath);
                        $l_sStoragePath = preg_replace("/^\//","",preg_replace("/.+ufile\/video\//","/video/",$l_sS3VideoFilePath));

    
                        $l_sS3ThumbnailFilePath = preg_replace("/users\//",$l_sFolderBase.'/',$l_sThumbnailPath);
                        $l_sStorageThumbnailPath = preg_replace("/^\//","",preg_replace("/.+ufile\/video\//","/photo/",$l_sS3ThumbnailFilePath));
 

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_video (cid) VALUES ('.$l_xChannel->id.')';  	
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();


                        $l_sCommand = PHP_PATH.' '.ZENCODER_PROCESS_VIDEO_SCRIPT.' '.$l_sNewAbsoluteFilePath.' '.$l_nInsertId;
                        echo $l_sCommand;
                        exec($l_sCommand . " > /dev/null &");       


                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $l_sStoragePath;
                        $l_aKeyValues['image_path'] = $l_sStorageThumbnailPath;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['time_stamp'] = time();
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                        $l_aKeyValues['is_publish'] = 0;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.'.$l_xChannel->slug.'://'.$l_xChannel->id.'/video/'.$l_nInsertId;
                        
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
  						
  						$l_aCleanValues = array();
						foreach($l_aKeyValues as $k => $v){
							$l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
						}
						$l_sSql = 'UPDATE enr_video SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
						$l_xSql->runQuery($l_sSql);

                        
                        //send video to be processed
                        $l_nBCId = $this->sendVideoToBrightcove($l_sNewAbsoluteFilePath,$l_sDescription,$l_xChannel);


                        echo "BC ID: $l_nBCId";
                        $l_sSql = 'UPDATE enr_video SET brightcove_media_id = "'.$l_nBCId.'" WHERE id = '.$l_nInsertId;
                        $l_xSql->runQuery($l_sSql);

                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        Utils::pushFileToS3($l_sNewAbsoluteFilePath,$l_sS3VideoFilePath);

                        //thumbnail
                        Utils::pushFileToS3($l_sThumbnailPath,$l_sS3ImageFilePath);
                        //TODO
                        //watermark
                        $l_sS3ImageFilePath = str_replace("assets/ufile/photo","assets/ufile/watermark/photo",$l_sS3ImageFilePath);
                        Utils::pushFileToS3($l_sThumbnailWatermarkPath,$l_sS3ImageFilePath);

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'video',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }


                        unlink($l_sNewAbsoluteFilePath);
                        unlink($l_sThumbnailPath);
                        unlink($l_sThumbnailWatermarkPath);
                        unlink($l_sTempVideoPath);

                        $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Video: '.$l_xChannel->title);
                        return true;
 
                    }        
                    break;
                
                default:
                    return false;
                    break;
            }


        }   



        private function rq_curatePost(&$p_aArgs){

           
    
            $l_aPairingData = json_decode(base64_decode($p_aArgs['pairing_data']));
            $l_sAssetType = strtoupper(Utils::cleanPostDataValue($p_aArgs['type']));
            $l_sDescription = Utils::cleanPostDataValue($p_aArgs['description']);
            $l_nViralId = $p_aArgs['viral_id'];


            $l_nChannelId = $p_aArgs['channel_id'];
                   
            $l_nAssetTypeId = CONTENT_TYPE_ID_VIDEO;
            $l_xChannelManager = new ChannelManager();

            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
         
            $l_bCLOSE_BUFFER = 1;

            if($l_bCLOSE_BUFFER)
                        Utils::closeBufferAndContinueProcess('{"status": "1"}');

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'SELECT * FROM enr_video WHERE id = '.$l_nViralId.'';   
                        $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

                        foreach($l_aData as $l_xRow){
                            $new_destination = $l_xRow['destination'];
                            $new_image_path = $l_xRow['image_path'];
                        }



                        $l_sFolderBase = $l_xChannel->slug.'/assets/ufile/video';
              
                        $new_path = "".$l_xChannel->slug."/assets/ufile/".$new_destination;
                      $new_photo_path = "".$l_xChannel->slug."/assets/ufile/".$new_image_path;
                      $new_watermark_path = "".$l_xChannel->slug."/assets/ufile/watermark/".$new_image_path;
                      $filebroken = explode( '.', $new_destination);
                      $extension = array_pop($filebroken);
                      $fileTypeless = implode('.', $filebroken);
                      $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8");
                      $middle = Utils::GetBetween("file-", ".m3u8", $nf->body);

                      Utils::copyFileOnS3('viral/assets/ufile/'.$new_destination,$new_path);
                      Utils::copyFileOnS3('viral/assets/ufile/'.$new_image_path,$new_photo_path);
                      Utils::copyFileOnS3('viral/assets/ufile/watermark/'.$new_image_path,$new_watermark_path);

                      $l_sNewAbsoluteFilePath = FILES_PATH.$new_path;
                    
                      $playlist = sizeof($middle[1]);
                      for($j=0;$j<$playlist;$j++){
                        Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j].".m3u8");
                          for($i=1;$i<10;$i++){
                            $nf = Utils::getFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                            if($nf!=false){
                            Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/file-".$middle[1][$j]."-0000".$i.".ts");
                            }else{
                              break;
                            }
                          }
                      }

                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."/master.m3u8",$l_xChannel->slug."/assets/ufile/".$fileTypeless."/master.m3u8");
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-1080.mp4");
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-1080.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-1080.webm");          
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-720.mp4");         
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-720.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-720.webm");     
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-480.mp4");             
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-480.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-480.webm");              
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.mp4",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-360.mp4");             
                      Utils::copyFileOnS3("viral/assets/ufile/".$fileTypeless."-360.webm",$l_xChannel->slug."/assets/ufile/".$fileTypeless."-360.webm");
                      
 

                        $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = 'INSERT INTO enr_video (cid) VALUES ('.$l_xChannel->id.')';   
                        $l_xSql->runQuery($l_sSql);
                        $l_nInsertId = $l_xSql->db_last_insert_id();   


                        $l_aKeyValues = array();
                        $l_aKeyValues['cid'] = $l_xChannel->id;
                        $l_aKeyValues['fan_id'] = -1;
                        $l_aKeyValues['description'] = $l_sDescription;
                        $l_aKeyValues['destination'] = $new_destination;
                        $l_aKeyValues['image_path'] = $new_image_path;
                        //$l_aKeyValues['thumbnail'] = $new_image_path;
                        $l_aKeyValues['tags'] = '';
                        $l_aKeyValues['time_stamp'] = time();
                        $l_aKeyValues['start_date'] = date('Y-m-d H:i:s');
                        $l_aKeyValues['end_date'] = date('Y-m-d H:i:s',strtotime($l_aKeyValues['start_date'].' +1 Month'));
                        $l_aKeyValues['is_publish'] = 1;
                        $l_aKeyValues['deep_link_app'] = 'com.starclub.'.$l_xChannel->slug.'://'.$l_xChannel->id.'/video/'.$l_nInsertId;
                        
                        $l_sBitly = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO);
                        $l_sBitlyTwitter = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'TWITTER');
                        $l_sBitlyLandingPage = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'LANDING_PAGE');
                        $l_sBitlyInline = $l_xChannel->getBitlyURLForAssetIdAndType($l_nInsertId,CONTENT_TYPE_VIDEO,'INLINE');

                        $l_aKeyValues['deep_link_web'] = $l_sBitlyInline;
                        $l_aKeyValues['deep_link_web_tw'] = $l_sBitlyTwitter;
                        $l_aKeyValues['deep_link_web_lp'] = $l_sBitlyLandingPage;
                        
                        //var_dump($l_aKeyValues);

                        $l_aCleanValues = array();
                        foreach($l_aKeyValues as $k => $v){
                            $l_aCleanValues[] = $k.' = "'.$l_xSql->cleanValue($v).'"';
                        }
                        $l_sSql = 'UPDATE enr_video SET '.join(',',$l_aCleanValues).' WHERE id = '.$l_nInsertId;
                        $l_xSql->runQuery($l_sSql);

                        $this->storePostToSocialData($l_nChannelId,$l_sDescription,$l_nInsertId,$l_nAssetTypeId,$l_aPairingData);
                        $this->logItemToFeed($l_nInsertId,'video');

                        if($l_nChannelId == 19 || $l_nChannelId == 17 || $l_nChannelId == 15 || $l_nChannelId == 18){
                            $this->rq_sendToLegacyServer($l_nChannelId,'video',$l_sNewAbsoluteFilePath,$l_sDescription);
                        }

                        sleep(30);

                        foreach($l_aPairingData as $l_xRow){

                           $l_sSql = 'SELECT id FROM enr_content_post_to_social WHERE content_id = '.$l_nInsertId.'';
                           $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

                            foreach($l_aData as $l_xORow){
                                $pts_id = $l_xORow['id'];
                                 $this->postToSocialNetwork($pts_id); 
                                
                            }

                        }


                        $this->sendPush(array(PUSH_LEVEL_ADMIN),'Post Video: '.$l_xChannel->title);
                        return true;


        }   


        //  public function getOAuthForServiceByCreatorId($p_sService,$p_nCreatorsId){
            
        //     $l_sSql = 'SELECT * FROM oauth_keys WHERE creators_id = '.intval($p_nCreatorsId).'
        //                 AND service = "'.$this->db_escape_string($p_sService).'"';

        //     $l_xData = $this->getRow($this->runQuery($l_sSql));
        //     if($l_xData){
        //         $l_xData['token'] = Utils::decryptItemId($l_xData['token']);
        //         $l_xData['secret'] = Utils::decryptItemId($l_xData['secret']);
        //     }
        //     return $l_xData;
        // }

        private function storePostToSocialData($p_nChannelId,$p_sMainDescription,$p_nAssetId,$p_nAssetTypeId,$p_aPairingData){

             $l_nAssetId = $p_nAssetId;
             $l_xSql = AppController::getInstance()->getConnection();

            foreach($p_aPairingData as $l_xRow){
                $l_xRow = (array)$l_xRow;
                
                $l_nPropertyId = intval($l_xRow['property_id']);
                $l_sMessage = $p_sMainDescription;
                $l_bMessageCustom = ($l_xRow['message_custom'] == 'Y') ? true : false;
                $l_bActive = ($l_xRow['active'] == 'Y') ? true : false;

                if($l_bMessageCustom){
                    $l_sMessage = ($l_xRow['message']);
                }

                print_r($l_xRow);

                if($l_bActive){

                    $l_sSql = 'INSERT INTO enr_content_post_to_social(post_type_id,channel_id,content_id,social_property_type_id,channel_oauth_id,message,has_sent,created) 
                                VALUES ('.$p_nAssetTypeId.',
                                    '.$p_nChannelId.',
                                    '.$l_nAssetId.',
                                    '.$l_nPropertyId.',
                                    "",
                                    "'.$l_xSql->cleanValue($l_sMessage).'",
                                    "N",
                                    NOW()
                                    )';   
                    echo $l_sSql;
                    $l_xSql->runQuery($l_sSql);
                    $l_nInsertId = $l_xSql->db_last_insert_id();

                    //TODO Fork this process
                    //If post is Photo --- send right awawy 
                    if($p_nAssetTypeId == CONTENT_TYPE_ID_PHOTO){
                        $this->postToSocialNetwork($l_nInsertId); 
                    }

                }

            }
        }

        private function rq_testSocial(){
            $l_nInsertId = $_GET['id'];
            echo $l_nInsertId;
            
            $this->postToSocialNetwork($l_nInsertId); 
        }

        private function rq_testPush(){
            $l_sMsg = $_GET['msg'];
            $l_xReponse = $this->sendPush(array(PUSH_LEVEL_ADMIN),$l_sMsg);
            // $l_xReponse = $this->sendPush(array(PUSH_LEVEL_ADMIN,PUSH_LEVEL_DEV),$l_sMsg);
            print_r($l_xReponse);
            echo "Push Sent";
        }

        private function rq_testPush2(){
            $l_sMsg = $_GET['msg'];
            require_once(LIB_PATH.'tuscano/VTPush.php');
            $l_sDomain = 'starapp-prod';
            $l_aTokens = array();
            $l_aTokens[] = '254213ac4f5358a1cf120b7c02e3f2369c292efd4f4a6478d3532ad955804ea6';
            $l_sMessage = $p_sMessage;
            echo  $l_sDomain;
            print_r($l_aTokens);

            $l_xPush = new VTPush();
            $l_xPush->setCert($l_sDomain);
            $l_xPush->setMessage($l_sMessage);
            $l_xPush->setDeviceIds($l_aTokens);
            $l_xResponse = $l_xPush->pushOut();
            print_R($l_xResponse);

        }

        private function rq_testRollUp(){
            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = '

            SELECT 
            SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "video" THEN 1 END) as total_videos,
            SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "photo" THEN 1 END) as total_photos,
            
            MAX(t1.time_stamp) as lastpost,
            
            t1.*
            FROM (
                SELECT v.id as item_id, "video" as the_type, c.id as channel_id, c.*, v.time_stamp

                        
                        FROM enr_chanel c

                        LEFT JOIN enr_video v 
                        ON c.id = v.cid

                        WHERE c.id >= 37 AND c.id NOT IN (58)
                        AND v.time_stamp > '.time().' - 86400


                        
            UNION 
                SELECT p.id as item_id, "photo" as the_type, c.id as channel_id, c.*, p.time_stamp
                        
                         FROM enr_chanel c

                        LEFT JOIN enr_photo p 
                        ON c.id = p.cid

                        WHERE c.id >= 37 AND c.id NOT IN (58)
                        AND p.time_stamp > '.time().' - 86400
                        ) t1 
            GROUP BY t1.channel_id
            ORDER BY t1.channel_id ASC, t1.created ASC

            ';
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            $l_nTotalVideos = 0;
            $l_nTotalPhotos = 0;

            foreach($l_aData as $l_xRow){
                $l_nTotalVideos += $l_xRow['total_videos'];
                $l_nTotalPhotos += $l_xRow['total_photos'];
            }
            $l_sMessage = "24h Posted Summary: Photos: $l_nTotalPhotos, Videos: $l_nTotalVideos";
            $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),$l_sMessage);
    
        }

        private function rq_flushChannel(){
        
            $l_sSlug = $_GET['slug'];
            echo $l_sSlug;
            echo Memcaching::makeKey($l_sSlug);
            $l_aCachedContent = Memcaching::getInstance()->getKey(Memcaching::makeKey($l_sSlug));
            if($l_aCachedContent){
                print_r($l_aCachedContent);
                echo "FLUSH";
                Memcaching::getInstance()->removeKey(Memcaching::makeKey($l_sSlug));
            }else{
                echo "No mem";
            }
            exit;
        }

        public function logItemToFeed($p_nAssetId,$p_sType){
            
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'INSERT INTO enr_feed (post_type,content_id,time_stamp) VALUES ("'.$p_sType.'",'.$p_nAssetId.',"'.time().'")';
            $l_xSql->runQuery($l_sSql);
        }

        private function rq_testVideoStatus(){
            $l_nInsertId = $_GET['id'];
            
            require_once(LIB_PATH.'brightcove/SCBrightcove.php');
            $l_xBC = new SCBrightcove();
            $l_xBC->isVideoReady($l_nInsertId);
            exit;
        }

        private function rq_updateSocialKeyStatus(&$p_aArgs){
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_aProperty = $p_aArgs['property'];
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'UPDATE enr_channel_oauth SET active = "'.$l_aProperty['active'].'", modified = NOW() WHERE id = '.$l_aProperty['id'];
            $l_xSql->runQuery($l_sSql);
        }


        public function facebookOauth(){

            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';

            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));

                           // $l_xFacebook->setExtendedAccessToken();
                           // $l_nPageId = $l_xRow['page_id']; 

                           // $l_xFacebook->api("/dialog/oauth/","get");

                            $user = $l_xFacebook->getUser();

                            var_dump($user);

                            $token = $l_xFacebook->getAccessToken();

                            $graph_url   =  "http://www.facebook.com/dialog/oauth?";
                            $graph_url  .=  "client_id=".$l_sFacebookAppId;
                            $graph_url  .=  "&redirect_uri=http://geek.starsite.com/api/test.php";

                            //var_dump($graph_url);

                            $response   = @file_get_contents($graph_url);  

                            //var_dump($reponse);

                            $graph_url   =  "https://graph.facebook.com/oauth/access_token?";
                            $graph_url  .=  "client_id=".$l_sFacebookAppId;
                            $graph_url  .=  "&client_secret=".$l_sFacebookAppSecret;
                            $graph_url  .=  "&grant_type=fb_exchange_token";
                            $graph_url  .=  "&fb_exchange_token=".$token;

                            //var_dump($graph_url);

                            $response   = @file_get_contents($graph_url);  

                            //var_dump($reponse);

                            //$l_aArgs = array(
                             //   'access_token'  => $token
                            // );

                           // $l_xFacebook->setAccessToken($token);

                         // print_r($l_aArgs);
                         // exit;
                           // $user_admin_pages = $l_xFacebook->api("/me/accounts/","get",$l_aArgs);

            
           // $user_admin_pages = $facebok->api('/me/accounts')

            //$reponse = $facebook->api($token, '/me/accounts?fields=name','get');

            //$response = $facebook->api($pageID . '/feed','get',$token);

           //var_dump($user_admin_pages); exit();
        }

        public function twitterOauth(){

            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

        }

        private function rq_storeSocialKeys(&$p_aArgs){
            
            
            $l_nUserId = $this->decodeToken($p_aArgs['token']);
            $l_nChannelId = $p_aArgs['channel_id'];

            $l_xSql = AppController::getInstance()->getConnection();

            $l_aPairingData = json_decode($p_aArgs['pairing_data']);
            $l_sPairingService = strtoupper($p_aArgs['pairing_service']);

            if($l_sPairingService == SOCIAL_SHARING_PROPERTY_INSTAGRAM_ID || $l_sPairingService == SOCIAL_SHARING_PROPERTY_TUMBLR_ID){
                
                $l_nDatabaseId = $p_aArgs['db_id'];
                $l_sSql = 'UPDATE enr_channel_oauth SET channel_id ='.$l_nChannelId.', modified = UTC_TIMESTAMP() WHERE id = '.$l_nDatabaseId.' ';
                $l_xSql->runQuery($l_sSql);
                

            }else if($l_sPairingService == 'FACEBOOK'){

                $l_xUserData = json_decode($p_aArgs['user_data']);
                $l_sUserToken = ($p_aArgs['user_token']);
                
                $l_xUserTokenData = array();
                $l_xUserTokenData['id'] = $l_xUserData->id;
                $l_xUserTokenData['name'] = $l_xUserData->name;
                $l_xUserTokenData['access_token'] = $l_sUserToken;
                $l_xUserTokenData['category'] = 'User';
                $l_xUserTokenData = (object) $l_xUserTokenData;

                $l_aPages = $l_aPairingData->data;
                array_push($l_aPages, $l_xUserTokenData);


                foreach($l_aPages as $l_xRow){
                    $l_nSocialTypeId = SOCIAL_SHARING_PROPERTY_FACEBOOK_ID;
                    $l_nPageId = $l_xRow->id;
                    $l_sPageName = $l_xRow->name;
                    $l_sPageAccessToken = $l_xRow->access_token;
                    $l_sPageCategory = $l_xRow->category;

                    $l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,page_category,social_token,created,active) 
                            VALUES ('.$l_nChannelId.',
                                "'.$l_xSql->cleanValue($l_nSocialTypeId).'",
                                "'.$l_xSql->cleanValue($l_nPageId).'",
                                "'.$l_xSql->cleanValue($l_sPageName).'",
                                "'.$l_xSql->cleanValue($l_sPageCategory).'",
                                "'.$l_xSql->cleanValue($l_sPageAccessToken).'",
                                NOW(),
                                "Y"
                                )
                                ON DUPLICATE KEY UPDATE page_name = VALUES(page_name), social_token = VALUES(social_token), active = VALUES(active), modified = NOW() ';   
                    $l_xSql->runQuery($l_sSql);
                    
                }
            }else if($l_sPairingService == 'TWITTER'){
                $l_aPairingDataMapped = array();
                foreach($l_aPairingData as $l_xRow){
                    $l_aParts = explode("=",$l_xRow);
                    $l_aPairingDataMapped[$l_aParts[0]] = $l_aParts[1];
                }
            
                $l_nSocialTypeId = SOCIAL_SHARING_PROPERTY_TWITTER_ID;
                $l_nPageId = $l_aPairingDataMapped['user_id'];
                $l_sPageName = $l_aPairingDataMapped['screen_name'];
                $l_sPageAccessToken = $l_aPairingDataMapped['oauth_token'];
                $l_sPageAccessTokenSecret = $l_aPairingDataMapped['oauth_token_secret'];

                $l_sSql = 'INSERT INTO enr_channel_oauth (channel_id,social_property_type_id,page_id,page_name,social_token,social_token_secret,created,active) 
                        VALUES ('.$l_nChannelId.',
                            "'.$l_xSql->cleanValue($l_nSocialTypeId).'",
                            "'.$l_xSql->cleanValue($l_nPageId).'",
                            "'.$l_xSql->cleanValue($l_sPageName).'",
                            "'.$l_xSql->cleanValue($l_sPageAccessToken).'",
                            "'.$l_xSql->cleanValue($l_sPageAccessTokenSecret).'",
                            NOW(),
                            "Y"
                            )
                            ON DUPLICATE KEY UPDATE page_name = VALUES(page_name), social_token = VALUES(social_token), social_token_secret = VALUES(social_token_secret), active = VALUES(active), modified = NOW() ';   
                $l_xSql->runQuery($l_sSql);
                
            
            }
            $l_aJSON = array();
            $l_aJSON['status'] = 1;
            $l_aJSON['pairing'] = $this->returnCurrentPairingForChannelId($l_nChannelId);
            echo json_encode($l_aJSON);

             


        }


        private function sendVideoToBrightcove($p_sFilePath,$p_sDescription,$p_xChannel){
            

            require_once(LIB_PATH.'brightcove/SCBrightcove.php');
            $l_xBC = new SCBrightcove();

            if(strlen($p_sDescription) == 0){
                $p_sDescription = 'Video';
            }
            
            $l_aMetaData = array(
                    'name' => $p_sDescription,
                    'tags' => array($p_xChannel->slug),
                    'shortDescription' => $p_sDescription);

            $l_nNewId = $l_xBC->createVideo($p_sFilePath,$l_aMetaData);
            $l_nNewId = $l_nNewId->result;

            $l_xBC->addOverlayLogo($l_nNewId, $p_xChannel->brightcove_overlay_logo,'The Tooltip','http://www.starsite.com/test');
            return $l_nNewId;
            
        }





        public function postToSocialNetwork($p_nSocialPropertyRowId){

            require_once(FACEBOOK_OAUTH_CLASS);
            require_once(TWITTER_OAUTH_CLASS);

            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT post_type_id FROM enr_content_post_to_social WHERE id = '.intval($p_nSocialPropertyRowId);
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            
            $l_bIsVideo = $l_aData['post_type_id'] == CONTENT_TYPE_ID_VIDEO;
            

            $l_sSql = 'SELECT cs.*,co.*, cs.id as main_id, a.id as content_id, c.slug as channel_slug, c.id as channel_id, c.url as channel_url, '.( $l_bIsVideo ? 'a.image_path' : 'a.destination' ).' as content_file, a.deep_link_web, a.deep_link_web_tw, a.deep_link_web_lp
                            
                            FROM enr_content_post_to_social cs 
                            JOIN enr_channel_oauth co  
                            ON (co.social_property_type_id = cs.social_property_type_id AND cs.channel_id = co.channel_id )
                            
                            JOIN '.( $l_bIsVideo ? 'enr_video' : 'enr_photo' ).' a  
                            ON (cs.content_id = a.id )
                            
                            JOIN enr_chanel c  
                            ON (cs.channel_id = c.id )

                            WHERE cs.has_sent = "N" 
                            AND co.active = "Y"
                            AND cs.id = '.intval($p_nSocialPropertyRowId);
                            echo $l_sSql;
                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            
            
            foreach($l_aData as $l_xRow){

                $l_nSocialPropertyId = $l_xRow['social_property_type_id'];
                $l_sEmbedCodeInfo = Utils::encryptItemId($l_xRow['channel_id'].','.(($l_bIsVideo) ? 'v' : 'p').','.$l_xRow['content_id']);
                $l_sEmbedCodeInfo = 'http://'.$l_xRow['channel_url'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;
                

                switch (intval($l_nSocialPropertyId)){

                    case SOCIAL_SHARING_PROPERTY_FACEBOOK_ID:   

// print_r($l_xRow);
                        // if($l_xRow['channel_id'] <= 43){
                        // if($l_xRow['channel_id'] == 8 || $l_xRow['channel_id'] == 33 || $l_xRow['channel_id'] > 52){
                            //NEW
                            $l_sFacebookAppId = '265098000358000';
                            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
                        // }else{
                        //     $l_sFacebookAppId = '1440671026187879';
                        //     $l_sFacebookAppSecret = '6860feb62899de0d9785d9c562d64088';

                        // } 
                        

                        $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));

                            $l_nPageId = $l_xRow['page_id']; 

                            $l_aArgs = array(
                            'access_token'  => $l_xRow['social_token'],
                            // 'link'       => 'http://'.$l_xRow['channel_url'].'.on.starsite.com/content/'.($l_bIsVideo ? 'video' : 'photo').'/'.$l_xRow['content_id'].'/',
                            'link'       => $l_xRow['deep_link_web_lp'],
                            // 'link'       => 'http://'.$l_xRow['channel_url'].'.on.starsite.com/viewpost_auth.php?'.($l_bIsVideo ? 'video' : 'photo').'='.$l_xRow['content_id'].'&type=landingpage',
                            // 'link'       => 'http://www.starclubltd.com/',
                            // 'caption'       => "caption here".time(),
                            // 'description'       => "description here".time(),
                            // 'name'       => "name here".time(),
                            'picture' => CDN_FRONT.'/'.$l_xRow['channel_slug'].'/assets/ufile/watermark/'.$l_xRow['content_file'],
                            'message'       => $l_xRow['message']
                         );

                         // print_r($l_aArgs);
                         // exit;
                        $l_nPostedId = $l_xFacebook->api("/$l_nPageId/feed","post",$l_aArgs);
                        $l_nPostedId = $l_nPostedId['id'];
                        $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                        $l_xSql->runQuery($l_sSql);

                        break;


                    case SOCIAL_SHARING_PROPERTY_TWITTER_ID:
                        $l_sOauthToken = $l_xRow['social_token'];
                        $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                        $l_nId = $l_xRow['page_id'];
                        $l_xTwitter = new TwitterOAuth("rOnDmArOdei9TqZpqqJ53x4Il", '9sJv3d8zjTb9WNrYTdtlDiiblhCog2plq6A0ZvS4Mli2bqxCRL', $l_sOauthToken, $l_sOauthTokenSecret);
                        // $content = $connection->get('account/verify_credentials');
                        $l_xRe = $l_xTwitter->post('statuses/update', array('status' => $l_xRow['deep_link_web_tw'].' '.$l_xRow['message']));
                        $l_nPostedId = $l_xRe->id;
                        $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                        $l_xSql->runQuery($l_sSql);
                        break;


                      case SOCIAL_SHARING_PROPERTY_TUMBLR_ID:

                        $l_sOauthToken = $l_xRow['social_token'];
                        $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                        echo "\n\n";

                        $l_sURL = preg_replace("/https?:\/\//","",$l_xRow['page_name']);
                        $l_sURL = preg_replace("/\//","",$l_sURL);

                        $l_bSuppressOAuth = true;
                        require_once(LIB_PATH.'tumblr/tumblroauth.php');

                        $l_xTumblr = new TumblrOAuth(TUMBLR_CLIENT_ID,TUMBLR_CLIENT_SECRET,$l_sOauthToken,$l_sOauthTokenSecret);
                        $l_sPostURL = 'http://api.tumblr.com/v2/blog/'.$l_sURL.'/post';

                        $l_aParameters = array();
                        if($l_bIsVideo){
                            $l_aParameters['type'] = "video";
                            $l_aParameters['caption'] = $l_xRow['message'];
                            $l_aParameters['embed'] = '<iframe scrolling="no" frameborder="0" width="680" height="383" src="'.$l_sEmbedCodeInfo.'"></iframe>';
                        }else{
                            $l_aParameters['type'] = "photo";
                            $l_aParameters['link'] = $l_xRow['deep_link_web_lp'];
                            $l_aParameters['source'] = CDN_FRONT.'/'.$l_xRow['channel_slug'].'/assets/ufile/watermark/'.$l_xRow['content_file'];
                            // $l_aParameters['source'] = 'https://40.media.tumblr.com/tumblr_m2j7o8Y3EN1qafar4o1_540.jpg';
                            $l_aParameters['caption'] = $l_xRow['message'];
                        }
                    
                        $l_xResponse = $l_xTumblr->post($l_sPostURL,$l_aParameters);

                        if (201 == $l_xResponse->meta->status) {
                            $l_nPostedId = $l_xResponse->response->id;
                            $l_sSql = 'UPDATE enr_content_post_to_social SET has_sent = "Y", posted_id = "'.$l_nPostedId.'" WHERE id = '.$l_xRow['main_id'];
                            $l_xSql->runQuery($l_sSql);
                        } else {
                          echo "error";
                          print_r($l_xResponse);
                        }

 
 
                        break;
                    
                    default:
                        break;
                }


            }

            
 
        }



        public function rq_heatMapData(&$p_aArgs){
            
            $l_nChannelId = $p_aArgs['channel_id'];
            $l_xChannelManager = new ChannelManager();
            $l_xChannel = $l_xChannelManager->getById($l_nChannelId,true);
            $l_nReportingId = $l_xChannel->starstats_site_id;

            //IF 25 == DEMO
            if($l_nReportingId == 25)
                $l_nReportingId = 1;

            //Support for legacy 
            // Enrique Channel id 1 
            

            //tyrese
            // $l_nReportingId = 4;
            /*
            //Switch to Reporting Database
            $DB_OVERRIDE = array();
            $DB_OVERRIDE['DB_HOST'] = 'reportsdb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
            $DB_OVERRIDE['DB_USERNAME'] = 'developer';
            $DB_OVERRIDE['DB_PASSWORD'] = 'dev@2014sg'; 
            $DB_OVERRIDE['DB_NAME'] = 'devdatabase';

            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);


            $l_sSql = 'SELECT idsite,location_country,location_region,location_city,location_latitude,location_longitude 
            FROM piwik_log_visit 
            WHERE location_latitude IS NOT NULL
            AND idsite = '.$l_nReportingId.'
            ORDER BY idvisit DESC limit 2000';

            */

             $gaObject = new Gastar();
            $analytics = $gaObject->getService();

            $channel_id = $value['id'];

            $profileID = $l_xChannel->google_analytics_internal_id;        
               $start_date = date('Y-m-d');

    $end_date = date('Y-m-d');
 

            if($profileID!=NULL){

                $vid = $value['id'];
                $videoId = "$vid";
                $pageurl = "/content/video/$videoId/";

                $tableId = "ga:$profileID";

                $data = $gaObject->getGeoLocations($tableId, $analytics,$start_date,$end_date);
          
            }

        // AND location_country LIKE "%gb"
            //$l_aPoints = $this->_xSQLManager->buildObject($this->_xSQLManager->runQuery($l_sSql));
            $l_aPointsCleaned = array();
            $l_aCountries = array();
            $l_aCountriesCleaned = array();
            foreach($data['rows'] as $l_xRow){
                $l_aFormatted = (array)$l_xRow;
                $l_aFormatted['lat'] = $l_aFormatted[3];
                $l_aFormatted['lng'] = $l_aFormatted[4];
                $l_aCountries[strtoupper($l_aFormatted[0])] += 1;
                $l_aPointsCleaned[] = $l_aFormatted;
            }
            
            $l_aJSON['status'] = 1;
            $l_aJSON['data'] = $l_aPointsCleaned;
            arsort($l_aCountries, SORT_NUMERIC);
            $l_aAllCountries = Utils::getAllCountries();
            $l_nCount = 0;
            $l_nTotal = count($data['rows']);
           
            $l_sPercentsDisplay = array();
            $l_nPercentLeft = 100;
            foreach($l_aCountries as $k => $v){
                if($l_nCount++ > 3){
                    // $l_sPercentsDisplay .= 'Others ('.$l_nPercentLeft.'%) ';
                    break;
                }
                $l_nPercent = number_format(($v/$l_nTotal)*100,1);
                $l_nPercentLeft -= $l_nPercent;
                $l_sTitle = $l_aAllCountries[$k];
                if($k == 'US')
                    $l_sTitle = 'USA';
                else if($k == 'GB')
                    $l_sTitle = 'UK';
                $l_aCountriesCleaned[] = array('title' => $l_sTitle, 'percent' => $l_nPercent, 'amount' => $v);
                $l_sPercentsDisplay[] = $l_sTitle.' ('.$l_nPercent.'%)';
            }
            $l_sPercentsDisplay = join(' • ',$l_sPercentsDisplay);

            $l_aJSON['percents'] = $l_aCountriesCleaned;
            $l_aJSON['percents_display'] = $l_sPercentsDisplay;
            echo json_encode($l_aJSON);
        }




 
        public function rq_resendStarInviteEmail(){

            $l_nUserId = $_GET['id'];
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT c.*, u.email FROM enr_chanel c, users u
                        WHERE u.cid = c.id
                        AND u.id = '.$l_nUserId;
            $l_aData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            if(!$l_aData)
                return;

            print_r($l_aData);
            $l_nChannelId = $l_aData['id'];

            $l_sEmail = $l_aData['email'];
            $l_sNewPass = 'star'.rand(100,999);
            $l_sChannelName = $l_aData['title'];
            file_get_contents('http://admincms.starsite.com/api?cid='.$l_nChannelId.'&option=updateAdminInfo&email='.$l_sEmail.'&password='.$l_sNewPass);

            $l_sChannelURL = 'http://'.$l_aData['url'].'.on.starsite.com';             
            $l_aUserData = array();
            $l_aUserData['email'] = $l_sEmail;
            $l_aUserData['pass'] = $l_sNewPass;
            $l_sEncodeData = base64_encode(json_encode($l_aUserData));
            $l_sDownloadLink = 'http://darwin.on.starsite.com/api/auth.php?t='.$l_sEncodeData; 
            ob_start();
            include(__DIR__.'/_inviteEmail.php');
            $l_sHTML = ob_get_clean();
            $l_aSendData = array();
            // $l_aSendData['to'] = $l_sEmail.',vincent@starclubltd.com';
            $l_aSendData['to'] = 'starinvite@starclubltd.com';
            $l_aSendData['subject'] = 'Your StarClub Invitation';
            $l_aSendData['message'] = $l_sHTML;
            $l_aSendData['from'] = "\"StarClub\" <no-reply@starclubltd.com>\n\r";
            print_R($l_aSendData);
            
            $l_aSendData = base64_encode(json_encode($l_aSendData));
            $l_sUrl = 'http://tuscanostudios.com/othersitesDependent/genericGet.php?data='.urlencode($l_aSendData);
            file_get_contents($l_sUrl);

            $this->sendPush(array(PUSH_LEVEL_ADMIN),'Invite Sent: '.$l_sChannelName);


        }

        public function rq_sendStarInviteEmail(&$p_aArgs){



            // print_R($p_aArgs);
            $l_sEmail = Utils::cleanPostDataValue($p_aArgs['email']);
            $l_sNewPass = 'star'.rand(100,999);
            $l_sChannelName = Utils::cleanPostDataValue($p_aArgs['name']);
            $l_sSlugName = trim(preg_replace("/\..*/","",Utils::cleanPostDataValue($p_aArgs['slug'])));

            if(strlen($l_sSlugName) <= 1){
                $l_sSlugName = time();
                $l_sSlugName = VTBaseConvert::encode($l_sSlugName);
            }

            $l_sChannelURL = 'http://'.$l_sSlugName.'.on.starsite.com';             
            $l_aUserData = array();
            $l_aUserData['email'] = $l_sEmail;
            $l_aUserData['pass'] = $l_sNewPass;

            $l_sEncodeData = base64_encode(json_encode($l_aUserData));
            $l_sDownloadLink = 'http://darwin.on.starsite.com/api/auth.php?t='.$l_sEncodeData; 
            $l_sDownloadLoginLink = 'com.starsite.cms://login-email/'.($l_sEncodeData).'/'; 

            //check if user exists
            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT * FROM users WHERE LOWER(email) = "'.strtolower($l_sEmail).'"';
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            if($l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Email Exists';
                $l_aJSON['msg'] = 'This email already exists on starsite, please use another email';
                $l_aJSON['btn'] = 'Close';
                echo json_encode($l_aJSON);
                exit;
            }

            $l_sSql = 'SELECT * FROM enr_chanel WHERE LOWER(url) = "'.strtolower($l_sSlugName).'"';
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            if($l_xData){
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Domain Exists';
                $l_aJSON['msg'] = 'The URL ('.$l_sSlugName.').on.starsite.com already exists, please try something other than "'.$l_sSlugName.'"';
                $l_aJSON['btn'] = 'Close';
                echo json_encode($l_aJSON);
                exit;
            }

           
            //Good, create account
            
            $l_sSql = 'INSERT INTO users (username,email,password,cid,created_on,active)   
                        VALUES (
                            "'.$l_sEmail.'",
                            "'.$l_sEmail.'",
                            "pass",
                            "-1",
                            '.time().',
                            1)';
            $l_xSql->runQuery($l_sSql);
            $l_nUserInsertedId = $l_xSql->db_last_insert_id();


            $l_sSql = 'INSERT INTO enr_chanel (name,title,user_id,url,slug,is_publish,created)   
                        VALUES (
                            "'.$l_sChannelName.'",
                            "'.$l_sChannelName.'",
                            "'.$l_nInsertId.'",
                            "'.$l_sSlugName.'",
                            "'.$l_sSlugName.'",
                            1,NOW())';
            $l_xSql->runQuery($l_sSql);
            $l_nChannelId = $l_xSql->db_last_insert_id();

            $l_sSql = 'UPDATE users SET cid = '.$l_nChannelId.' WHERE id = '.$l_nUserInsertedId;
            $l_xSql->runQuery($l_sSql);

            $l_sSql = 'UPDATE enr_chanel SET user_id = '.$l_nUserInsertedId.' WHERE id = '.$l_nChannelId;
            $l_xSql->runQuery($l_sSql);

            file_get_contents('http://admincms.starsite.com/api?cid='.$l_nChannelId.'&option=updateAdminInfo&email='.$l_sEmail.'&password='.$l_sNewPass);


         
            ob_start();
            include(__DIR__.'/_inviteEmail.php');
            $l_sHTML = ob_get_clean();
            $l_aSendData = array();
            $l_aSendData['to'] = $l_sEmail.',starinvite@starclubltd.com';
            $l_aSendData['subject'] = 'Your StarClub Invitation';
            $l_aSendData['message'] = $l_sHTML;
            $l_aSendData['from'] = "\"StarClub\" <no-reply@starclubltd.com>\n\r";
            $l_aSendData = base64_encode(json_encode($l_aSendData));
            $l_sUrl = 'http://tuscanostudios.com/othersitesDependent/genericGet.php?data='.urlencode($l_aSendData);
            file_get_contents($l_sUrl);
            $this->sendPush(array(PUSH_LEVEL_DEV,PUSH_LEVEL_SALES,PUSH_LEVEL_EXEC),'Invite Sent: '.$l_sChannelName);

            $l_aJSON['status'] = 1;

            echo json_encode($l_aJSON);


            $DB_OVERRIDE = array();
            $DB_OVERRIDE['DB_HOST'] = 'reportsdb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com';
            $DB_OVERRIDE['DB_USERNAME'] = 'developer';
            $DB_OVERRIDE['DB_PASSWORD'] = 'dev@2014sg'; 
            $DB_OVERRIDE['DB_NAME'] = 'devdatabase';
            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_sSql = 'INSERT INTO piwik_site (name,main_url,ts_created,timezone,currency,type,sitesearch_keyword_parameters,sitesearch_category_parameters)
             VALUES (
                "'.$l_sChannelName.'",
                "http://'.$l_sSlugName.'.on.starsite.com",
                "'.date("Y-m-d H:i:s").'",
                "America/Los_Angeles",
                "USD",
                "website",
                "q,query,s,search,searchword,k,keyword",
                "option,page,cid,token,user_id,"
                )';
            
            $this->_xSQLManager->runQuery($l_sSql);
            $l_nInsertId = $this->_xSQLManager->db_last_insert_id();


            $DB_OVERRIDE = array();
            $DB_OVERRIDE['DB_HOST'] = DB_HOST;
            $DB_OVERRIDE['DB_USERNAME'] = DB_USERNAME;
            $DB_OVERRIDE['DB_PASSWORD'] = DB_PASSWORD;
            $DB_OVERRIDE['DB_NAME'] = DB_NAME;
            $this->_xSQLManager->severConnection();
            $this->_xSQLManager->establishNewConnection($DB_OVERRIDE);

            $l_sSql = 'UPDATE enr_chanel SET starstats_site_id = '.$l_nInsertId.' WHERE id ='.$l_nChannelId;
            
            $l_xSql->runQuery($l_sSql);
            

        }

        //=======================================================================================
        //                                  INSIGHT FUNCTIONS
        //=======================================================================================


        public function twitterInsightsR($content_id, $channel_id){
            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("rOnDmArOdei9TqZpqqJ53x4Il", '9sJv3d8zjTb9WNrYTdtlDiiblhCog2plq6A0ZvS4Mli2bqxCRL', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
           
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->retweet_count;


        }

        public function twitterInsightsF($content_id, $channel_id){

            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/twitter/twitteroauth/twitteroauth.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,o.social_token_secret,s.posted_id FROM enr_channel_oauth o LEFT JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 2 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 2 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           

                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            foreach($l_aData as $l_xRow){
                $l_sOauthToken = $l_xRow['social_token'];
                $l_sOauthTokenSecret = $l_xRow['social_token_secret'];
                $l_sPgeId = $l_xRow['page_id'];
                $posted_id = $l_xRow['posted_id'];
             
            }
            //$l_nId = $l_xRow['page_id'];
            $l_xTwitter = new TwitterOAuth("rOnDmArOdei9TqZpqqJ53x4Il", '9sJv3d8zjTb9WNrYTdtlDiiblhCog2plq6A0ZvS4Mli2bqxCRL', $l_sOauthToken, $l_sOauthTokenSecret);
            // $content = $connection->get('account/verify_credentials');
            //$l_xRe = $l_xTwitter->get('statuses/retweets', array('id' => '638824269690966016'));
            //var_dump($l_xRe);

            $l_xRe = $l_xTwitter->get('statuses/lookup', array('id' => $posted_id));
            
            $l_xRe = (array)$l_xRe; 
            

            return $l_xRe[0]->favorite_count;


        }

        public function facebookInsightsS($content_id, $channel_id){


            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,s.posted_id FROM enr_channel_oauth o JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 1 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 1 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           


                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

            $i = 0;

            

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));
            //$user_id = $l_xFacebook->getUser();

            $today = date("Y-m-d");

            $since = strtotime($today. " -365 days");             
            $until = strtotime($today);

            foreach($l_aData as $l_xRow){
                $l_sOauthToken[$i] = $l_xRow['social_token'];
              
                $l_sPgeId[$i] = $l_xRow['page_id'];
                $page_id = $l_sPgeId[$i];
                $posted_id[$i] = $l_xRow['posted_id'];
                $pid = $posted_id[$i];
                $l_xRe[$i] =  "https://graph.facebook.com/$pid/?fields=shares&access_token=".$l_sOauthToken[$i];
             $i++;
            }

            return $l_xRe;

        }

        public function facebookInsightsL($content_id, $channel_id){


            require_once('../include/AppController.php');
            require_once('../include/lib//front/FrontCoreSql.php');
            require_once('../include/lib/oauth/facebook/facebook.php');

            $l_xSql = AppController::getInstance()->getConnection();

            $l_sSql = 'SELECT o.page_id,o.social_token,s.posted_id FROM enr_channel_oauth o JOIN enr_content_post_to_social s ON o.channel_id = s.channel_id WHERE o.social_property_type_id = 1 AND o.channel_id ='.$channel_id.' AND s.social_property_type_id = 1 AND s.channel_id ='.$channel_id.' AND s.content_id = '.$content_id.'';
           


                        
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

            $i = 0;

            

            $l_sFacebookAppId = '265098000358000';
            $l_sFacebookAppSecret = 'd88198b3a3d59056fdb186a6170c0c80';
            $l_xFacebook = new Facebook(array('appId'  => $l_sFacebookAppId,'secret' => $l_sFacebookAppSecret));
            //$user_id = $l_xFacebook->getUser();

            $today = date("Y-m-d");

            $since = strtotime($today. " -365 days");             
            $until = strtotime($today);

            foreach($l_aData as $l_xRow){
                $l_sOauthToken[$i] = $l_xRow['social_token'];
              
                $l_sPgeId[$i] = $l_xRow['page_id'];
                $page_id = $l_sPgeId[$i];
                $posted_id[$i] = $l_xRow['posted_id'];
                $pid = $posted_id[$i];
                $l_xRe[$i] =  "https://graph.facebook.com/$pid/likes?summary=1&filter=toplevel&access_token=".$l_sOauthToken[$i];
             $i++;
            }

            //$params = array(
            //'since' => $since, 
            //'until' => $until,
            //'access_token'=>$l_sOauthToken
            //);

            //$l_xRe = $l_xFacebook->api("/$posted_id/likes", "GET", $params);
            //$l_xRe = $l_xRe;
           



            return $l_xRe;



        }


        //=======================================================================================
        //                                  STAR ALERT FUNCTIONS
        //=======================================================================================


        private function rq_loginStarAlert(&$p_aArgs){
            
            $l_nPin = intval(Utils::cleanPostDataValue($p_aArgs['pin']));
            $l_aJSON = array();

            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT id,first_name FROM users WHERE star_alert_pin = '.$l_nPin;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            
            if($l_xData){
                $l_aJSON = array();
                $l_aJSON['status'] = 1;
                $l_aJSON['token'] = $this->makeToken($l_xData['id']);
                echo json_encode($l_aJSON);          
                $this->sendPush(array(PUSH_LEVEL_ADMIN),'Signup: '.$l_xData['first_name']);
                $l_sSql = 'UPDATE users SET star_alert_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_xData['id'];
                $l_xSql->runQuery($l_sSql);
            }else{
                $l_aJSON = array();
                $l_aJSON['status'] = 2;
                $l_aJSON['title'] = 'Invalid Login';
                $l_aJSON['msg'] = 'Your pin is incorrect, please try again.';
                echo json_encode($l_aJSON);          
                $this->sendPush(array(PUSH_LEVEL_ADMIN),'Signup PIN FAIL: '.$l_nPin);
            }
        }

        // public static function externalSendPush($p_aPushLevels,$p_sMessage){
        // }
        private function sendPush($p_aPushLevels,$p_sMessage){

            ob_start();
            $l_xSql = AppController::getInstance()->getConnection();
            $p_aPushLevels[] = PUSH_LEVEL_ADMIN;
            $l_sLevels = join(",",$p_aPushLevels);
            $l_sSql = 'SELECT * FROM users WHERE star_alert_level IN ('.$l_sLevels.') AND star_alert_device_id IS NOT NULL';
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            $l_aTokens = array();
            foreach($l_aData as $l_xRow){
                $l_aTokens[] = $l_xRow['star_alert_device_id'];
            }

            require_once(LIB_PATH.'tuscano/VTPush.php');
                
            if(SERVER_ENV_SETTINGS == 'PRODUCTION'){
                $l_sDomain = 'star-alert-prod';
                // $l_aTokens = array();
                // $l_aTokens[] = '0baa2ab27a7b9b5fb7425bf87d7b2ccbb5bcf55a0c28df4674cb8a2df3c746d8';
                // $l_aTokens[] = '89b64f5ba64855d7e8865f3950593ed2dde438ae320eff4697df7a509c962468';
            }else{
                $l_sDomain = 'star-alert-dev';
                $l_aTokens = array();
                $l_aTokens[] = '0baa2ab27a7b9b5fb7425bf87d7b2ccbb5bcf55a0c28df4674cb8a2df3c746d8';
                return;
            }
            // $l_sMessage = '⚠️ '.$p_nMessage;
            $l_sMessage = $p_sMessage;
            echo  $l_sDomain;
            print_r($l_aTokens);
            $l_xPush = new VTPush();
            $l_xPush->setCert($l_sDomain);
            $l_xPush->setMessage($l_sMessage);
            $l_xPush->setDeviceIds($l_aTokens);
            $l_xResponse = $l_xPush->pushOut();
                
            return ob_get_clean();
        }





        private function rq_updatePushDeviceIdStarAlert(&$p_aArgs){
            
            Utils::closeBufferAndContinueProcess('{"status": "1"}');
            $l_xUserManager = new UserManager();
            
            $l_sDeviceToken = Utils::cleanPostDataValue($p_aArgs['device_token']);
            $l_sToken = Utils::cleanPostDataValue($p_aArgs['token']);          
            $l_nUserId = $this->decodeToken($l_sToken);
            
            if($_GET['android'] == '1'){
                $l_aKeyValue = array();
                $l_aKeyValue['user_device_token_android'] = $l_sDeviceToken;
                $this->_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);   
                exit;
            }else{
                
                $l_aLocationData = $p_aArgs['loc'];
                
                $l_aKeyValue = array();
                $l_aKeyValue['city'] = $l_aLocationData['city'];
                $l_aKeyValue['country'] = $l_aLocationData['country'];
                $l_aKeyValue['country_code'] = $l_aLocationData['countryCode'];
                $l_aKeyValue['isp'] = $l_aLocationData['isp'];
                $l_aKeyValue['lat'] = $l_aLocationData['lat'];
                $l_aKeyValue['lng'] = $l_aLocationData['lon'];
                $l_aKeyValue['ip_address'] = $l_aLocationData['query'];
                $l_aKeyValue['region'] = $l_aLocationData['region'];
                $l_aKeyValue['region_name'] = $l_aLocationData['regionName'];
                $l_aKeyValue['timezone'] = $l_aLocationData['timezone'];
                $l_aKeyValue['zip'] = $l_aLocationData['zip'];
                $l_aKeyValue['star_alert_device_id'] = $l_sDeviceToken;

                if(strlen($l_aKeyValue['lat']) <= 3){
                    $l_aKeyValue = array();
                    $l_aKeyValue['star_alert_device_id'] = $l_sDeviceToken;
                    $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           
                }else{
                    $l_xUserManager->updateProfile($l_nUserId,$l_aKeyValue);           
                }
            }


        }



        private function rq_starAlertDashboard(&$p_aArgs){

            $l_nUserId = intval($this->decodeToken($_GET['t']));
            $l_nApiVersion = ($_GET['apiversion']);
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sSql = 'SELECT * FROM users WHERE id = '.$l_nUserId;
            $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));
            $l_sSql = 'UPDATE users SET star_alert_version = "'.$this->_nAPIVersion.'" WHERE id = '.$l_nUserId;
            $l_xSql->runQuery($l_sSql);
            //com.starsite.staralert
            ?>
            <html>
            <head>
            <style type="text/css">
            *{ margin:0; padding:0; outline:none;}
            body{ background:#FDDA00; font-family: 'Arial'; font-size:18px; color:#111;}
            .container{ text-align: center; padding:50px 30px}
            .container span{  font-size:10px}

            </style> 
            <script type="text/javascript">
            // setTimeout( function() {
                // document.location.href = document.location.href;
            // },1000);    
            </script>
            </head>
            <body>
            <div class="container">
            Thank you <?= $l_xData['first_name'] ?>.<br /><br />You are set to receive notifications.
            <br /><br /><br /><br />
            <span>Version : <?= $l_nApiVersion ?></span>
            
            <?
            // print_r($p_aArgs);
            // print_r($_GET);
            ?>
            </div>
            </body>
            </html>
            <?

        }









        //=======================================================================================
        //                                  PRIVATE FUNCTIONS
        //=======================================================================================



































        private function cmp($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        }



  
		private function makeToken($p_sStr){
			$l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
			$p_sStr = base64_encode($l_sPaddingText.base64_encode(strrev(base64_encode($p_sStr))).base64_encode($l_sPaddingText));
			return $p_sStr;
		}

		private function decodeToken($p_sStr){
			$l_sPaddingText = 'dHJpYW5nbGUgZGUgUGFzY2Fs';
			$l_sEnding = base64_encode($l_sPaddingText);
			$p_sStr = str_replace($l_sEnding,'',base64_decode($p_sStr));
			$p_sStr = str_replace($l_sPaddingText,'',$p_sStr);
			$p_sStr = base64_decode(strrev(base64_decode($p_sStr)));
			return $p_sStr;
		}

		private function cleanForReturn(&$p_aData){
			foreach($p_aData as $k=>$v){
				if(!$v && $v != '0')
					unset($p_aData[$k]);
			}
		}



        private function makeHumanFriendlyNumbers($p_sStr){
            
                // $x = round($p_sStr);
              $x = ($p_sStr);
              $x_number_format = number_format($x);
              $x_array = explode(',', $x_number_format);
              $x_parts = array('k', 'm', 'b', 't');
              $x_count_parts = count($x_array) - 1;
              $x_display = $x;
              $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
              $x_display .= $x_parts[$x_count_parts - 1];
              return $x_display;
            
            

        }
        
        private function buildStatObject($p_xObj,$p_nMainChannelId){
                 // print_R($p_xObj);

                $l_bIsVideo = $p_xObj['post_type'] == 'video';
                if($l_bIsVideo){ $l_aJSON['isPhoto']=0;}else{ $l_aJSON['isPhoto']=1; }
                $l_aJSON = array();
                $l_aJSON['itemId'] = $p_xObj['id'];
                $l_aJSON['id'] = $p_xObj['id'];
                $l_aJSON['videoId'] = $p_xObj['video_db_id'];
                $l_aJSON['photoId'] = $p_xObj['photo_db_id'];
                $l_aJSON['video_db_id'] = $p_xObj['video_db_id'];
                $l_aJSON['photo_db_id'] = $p_xObj['photo_db_id'];
                $l_aJSON['postId'] = $p_xObj['post_id'];
                $l_aJSON['postType'] = $p_xObj['post_type'];

                $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['image_path'];

                if($p_nMainChannelId==64){ $p_xObj['slug'] = "viral"; }
                // $l_aJSON['thumbnail'] = $p_xObj['thumbnail'];
                // if($l_aJSON['post_type'] == 'video')
                    $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['image_path'];
                   
                // else
                    // $l_aJSON['thumbnail'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                if(!$l_bIsVideo){
                    $p_xObj['start_date'] = $p_xObj['photo_start_date'];
                }
                 
                
                
               
                    $content_id =$p_xObj['id'];

                    $l_xSql = AppController::getInstance()->getConnection();
                        $l_sSql = "SELECT * FROM sc_social_reports WHERE content_id = ".$p_xObj['id']."";   
                        $l_xData = $l_xSql->getRow($l_xSql->runQuery($l_sSql));

                        $l_nSql = "SELECT * FROM sc_reports WHERE content_id = ".$p_xObj['id']."";   
                        $l_nData = $l_xSql->getRow($l_xSql->runQuery($l_nSql));
                

                  $l_aJSON['tw_retweets'] = $l_xData['tw_retweets'] ? $l_xData['tw_retweets'] : "N/A";
                $l_aJSON['tw_favorites'] = $l_xData['tw_favorites'] ? $l_xData['tw_favorites'] : "N/A";
                $l_aJSON['gp_likes'] = "N/A";
                $l_aJSON['gp_comments'] = "N/A";
                $l_aJSON['tu_likes'] = "N/A";
                 $l_aJSON['tu_comments'] = "N/A";
                $l_aJSON['message'] = $p_xObj['description'];
                $l_aJSON['created'] = $p_xObj['start_date'];
                $l_aJSON['created_time'] = date('U',strtotime($p_xObj['start_date']));
                $l_aJSON['createdDate'] = date('U',strtotime($p_xObj['start_date']));
                $l_aJSON['fb_likes'] =  $l_xData['fb_likes'] ? $l_xData['fb_likes'] : "N/A";
                $l_aJSON['fb_comments'] = "N/A";
                $l_aJSON['fb_shares'] = $l_xData['fb_shares'] ? $l_xData['fb_shares'] : "N/A";
                $l_aJSON['fb_reaches'] = "N/A";
                $l_aJSON['ss_impressions'] = ($l_nData['page_views_ga']*3) ? ($l_nData['page_views_ga']*3) : "N/A";
                $l_aJSON['ss_engagements'] = ($l_nData['engagement']) ? ($l_nData['engagement']) : "N/A";
                 $l_aJSON['ss_pageViews'] = ($l_nData['page_views_ga']*3) ? ($l_nData['page_views_ga']*3) : "N/A";
                 $l_aJSON['ss_videoViews'] = ($l_nData['video_views']*3) ? ($l_nData['video_views']*3) : "N/A";
                 $l_aJSON['ss_earnings'] = ("$".number_format(($l_nData['revenue']*3),2)) ? ("$".number_format(($l_nData['revenue']*3),2)) : "N/A";
                $l_aJSON['ss_earningsTitle'] =  "Projected Earnings";
                $l_aJSON['url'] =  'http://'.$p_xObj['slug'].'.on.starsite.com/content/'.(($l_aJSON['post_type'] == 'video') ? 'video/'.$l_aJSON['video_db_id'] : 'photo/'.$l_aJSON['content_id']).'/';


                
                $l_sEmbedCodeInfo = Utils::encryptItemId($p_nMainChannelId.','.((1 || $l_bIsVideo) ? 'v' : 'p').','.$p_xObj['id']);
                $l_sEmbedCodeInfo = 'http://'.$p_xObj['slug'].'.on.starsite.com/embed/'.$l_sEmbedCodeInfo;

                $l_aJSON['embed'] =  base64_encode('<iframe src="'.$l_sEmbedCodeInfo.'"></iframe>');

            
                $l_aJSON['video_length'] = $p_xObj['video_length'];
                $l_sSeconds = $p_xObj['video_length']/1000;
                $l_sHours = floor($l_sSeconds/60/60);
                $l_sMin = floor(($l_sSeconds-($l_sHours*60))/60);
                $l_sSec = round(($l_sSeconds-($l_sHours*60) - ($l_sMin*60)));

                $l_aJSON['ss_videoLengthHuman'] = str_pad($l_sHours,2,"0",STR_PAD_LEFT).':'.str_pad($l_sMin,2,"0",STR_PAD_LEFT).':'.str_pad($l_sSec,2,"0",STR_PAD_LEFT);
                $l_aJSON['ss_videoLengthAndPercent'] = "1000";
                $l_aJSON['video_engagement'] = $p_xObj['engagement_score'].'%';
                $l_aJSON['video_views'] = $this->makeHumanFriendlyNumbers($p_xObj['video_view']);
                $l_aJSON['video_impression'] = $p_xObj['video_impression'];
                $l_aJSON['fileURL'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];
                $l_aJSON['file_url'] = 'http://d1o8tuak29oxqa.cloudfront.net/'.$p_xObj['slug'].'/assets/ufile/'.$p_xObj['destination'];

                if($p_nMainChannelId == 15){
                    $l_aJSON['file_url'] = 'http://enrique.starsite.com/forapi/'.$p_xObj['destination'];
                    $l_aJSON['thumbnail'] = 'http://cms.enrique.starsite.com/assets/ufile/'.$p_xObj['image_path'];
                }
                // $l_aJSON['file_url'] = 'http://sc-media-assets.s3.amazonaws.com/tyrese/assets/ufile/video/2014/11/102e94a832a06cffdf5385f81790ab84.mp4';
                

        // define('STATIC_FILES_HOST','http://d6058xu8xpm39.cloudfront.net');
        // define('CDN_FRONT','http://d1o8tuak29oxqa.cloudfront.net');

                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 2)   +  (( $p_xObj['video_view']/1000) * 10));
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                
                // $l_aJSON['earnings'] = (    (($p_xObj['impressions']/1000) * 1)   +  (( $p_xObj['video_view']/1000) * 3));
                //$l_aJSON['ss_earnings'] = (  (  $p_xObj['ss_impressions']/1000  * 1.0 ) + ( $p_xObj['ss_videoViews']/1000 * 3.0  * 0.16 ) ) * 0.33;
               
                if($this->_bDemo)
                    $l_aJSON['earnings'] = $p_xObj['video_view']/1000 * 1;

                // 20%
                // $l_aJSON['earnings'] = $l_aJSON['earnings']*.16 *.33;
                $l_aJSON['earnings_title'] = 'Projected Earnings';

                $l_aJSON = (array)$l_aJSON;
                foreach($l_aJSON as $k=>$v){
                    if(!$v)
                        unset($l_aJSON[$k]);
                }

                return $l_aJSON;
            }


                  


            private function cleanArg($p_sStr){
                return trim(strip_tags($p_sStr));
            }

            protected function showError($p_nErrorCode,$p_sMessage){
                $this->outputJSON('{
                                "status": "'.$p_nErrorCode.'",
                                "msg": '.json_encode($p_sMessage).'
                                }');
            }

            protected function outputJSON($p_sJSON,$p_bExit = true){
                header('Content-Type: application/json');
                echo $p_sJSON;
                if($p_bExit)
                    exit;
            }

		









}


class VTBaseConvert
{
    // public static $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    public static $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    public static $base = 36;
    public static function encode($val) 
    {
        // can't handle numbers larger than 2^31-1 = 2147483647
        $str = '';
        do {
            $i = $val % self::$base;
            $str = self::$chars[$i] . $str;
            $val = ($val - $i) / self::$base;
        } while($val > 0);
        return $str;
    }
 
    public static function decode($str) 
    {
        $len = strlen($str);
        $val = 0;
        $arr = array_flip(str_split(self::$chars));
        for ($i = 0; $i < $len; ++$i) 
        {
            $val += $arr[$str[$i]] * pow(self::$base, $len-$i-1);
        }
        return $val;
    }
}
	

?>