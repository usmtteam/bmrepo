<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->chanel = $this->session->userdata('chanel');
        $this->data['chanel'] = $this->chanel;
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
    }
    
    function _remap($method) {
        $this->load->view('header_v');
        $this->load->view('sidebar_v');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){
        $cid = $this->chanel['id'];
		
		$cmd = isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : '';
		
		if($cmd == 'save') {
			$push = isset($_REQUEST['push']) ? 1 : 0;
			$facebook_page = isset($_REQUEST['facebook_page']) ? $_REQUEST['facebook_page'] : '';
			
			$update = array(
				'push' => $push,
				'facebook_page' => $facebook_page
			);
			
			$this->main_m->saveSocial($cid, $update);
			
			
			
		}

        $this->data['social'] = $this->main_m->getSocial($cid);

        $this->load->view('social', $this->data);
    }

}

