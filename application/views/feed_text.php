<!-- BEGIN PAGE -->
<div class="page-content">
    <?php 
        $this->load->view('header_feed');
    ?>
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                    <div id="form_head_back">Add Text</div>
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("content/feed/text", 'class="form-horizontal"');
                    //    
                        echo form_error("description");
                        echo form_error("credit");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                        <div class="control-group">
                            <label class="control-label" for="url">Text Post</label>
                            <div class="controls">
                            <?php  echo form_textarea("description",$post['description'],'class="span6 m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Tags</label>
                            <div class="controls">
                            <?php  echo form_input("tags",$post['tags'],'class="span6 m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Credits</label>
                            <div class="controls">
                            <?php  echo form_input("credit",$post['credit'],'class="span6 m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Permission</label>
                            <div class="controls">
								<?php echo form_dropdown("is_publish", array('Not Published', 'Publish', 'Draft'), $post['is_publish'], 'class="large m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Sticky Flag</label>
                            <div class="controls">
                            <?php  echo form_checkbox("is_sticky", 1, $post['is_sticky'],'class=""');?>
                            </div>
                        </div>
                            
                            
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Start date</label>
                                <div class="controls">
                                	<div id="start_date_div" class="input-append">
		                            	<input type="text" id="start_date" name="start_date" value="<?php echo $post['start_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="url">End date</label>
                                <div class="controls">
                                	<div id="end_date_div" class="input-append">
		                            	<input type="text" id="end_date" name="end_date" value="<?php echo $post['end_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            <script language="javascript">
								$(document).ready(function() {
									$('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
									$('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
								});
							</script>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Post",' class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
