<?php
 error_reporting(0);

 	 set_time_limit(0);

	ini_set('memory_limit','1280M');
    ini_set('upload_max_filesize','100M');
    ini_set('max_execution_time',1000000);



  require(__DIR__.'/config/settings.php');

  $accountId = GA_ACCOUNT_ID;

  $analytics = Gastar::getService();

  
  get_daily_totals($analytics);
  get_daily_photo_totals($analytics);
   			

  function get_daily_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT v.id,v.cid, c.slug, c.google_analytics_internal_id FROM enr_video AS v INNER JOIN enr_chanel AS c ON v.cid = c.id WHERE c.is_publish=1 AND v.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-07-13";

  	$end_date = "2015-07-13";

   	$sql_end_date = time();

  	$count=0;

	 foreach($l_aData as $value){
	 

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		   	if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){

		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/video/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/video/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/video/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $pageurl = "/content/video/$videoId/?page_type=twitter";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_twitter = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_twitter = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest + $page_views_twitter;

				$fields = "Video-View";
	           
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view = Gastar::calcTotals($data);
		    	}else{
					$video_view = 0;
				}
		    	$fields = "Video-Impression";
	         
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    	$video_impression = Gastar::calcTotals($data);
		    	}else{
					$video_impression = 0;
				}

		    	$fields = "Video-Played";
	      

		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_played = Gastar::calcTotals($data);
		    	}else{
					$video_played = 0;
				}

		    	$video_impression += $video_played;

		    	$fields = "Video-Finished";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_finished = Gastar::calcTotals($data);
		    	}else{
					$video_finished = 0;
				}

		    	$fields = "Video-View-Half";
	        
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_half = Gastar::calcTotals($data);
		    	}else{
					$video_view_half = 0;
				}

		    	$fields = "Video-View-Three-Fourths";
		   
		    	$data = Gastar::queryCoreReportingApi($tableId, $analytics,$start_date,$end_date,$videoId, $fields);

		    	if($data!=""){
		    		$video_view_three_fourths = Gastar::calcTotals($data);
		    	}else{
					$video_view_three_fourths = 0;
				}
		    	
		    	$engagement1 = ($video_view/$video_impression)*100;

		        if($video_view_half>0){
		        $engagement2 = ($video_view_half/$video_impression)*100;
		        }
		        if($video_view_three_fourths>0){
		        $engagement3 = ($video_view_three_fourths/$video_impression)*100;
		     
		        }
		        $engagement4 = ($video_finished/$video_impression)*100;
		        if($video_view_half>0 && $video_view_three_fourths>0){
		        $engagement = ($engagement1+$engagement2+$engagement3+$engagement4)/4;
		        }else{
		        $engagement = ($engagement1+$engagement4)/2;  
		        }
		        $engagement = number_format($engagement, 2, '.', '');
		     

				$l_sSql = "INSERT INTO sc_reports_daily (time_stamp,channel_id,content_id,content_type,page_views_ga,video_views,video_view_half,video_view_three_fourths,video_finished,video_impressions,engagement) 
							VALUES ('2015-07-13',$channel_id,$content_id,'video',$page_views,$video_view,$video_view_half,$video_view_three_fourths,$video_finished,$video_impression,$engagement)"; 	
				
				$l_xSql->runQuery($l_sSql);

		    }
		
		    $count++;
		}

	 }
	 echo $count;
  } 

  function get_daily_photo_totals($analytics){

  	$l_xSql = AppController::getInstance()->getConnection();

  	$l_sSql = 'SELECT p.id,p.cid, c.slug, c.google_analytics_internal_id FROM enr_photo AS p INNER JOIN enr_chanel AS c ON p.cid = c.id WHERE c.is_publish=1 AND p.is_publish=1' AND c.google_analytics_internal_id!=NULL;
  
  	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

   	$start_date = "2015-07-13";

  	$end_date = "2015-07-13";

   	$sql_end_date = time();

  	$count=0;

	 foreach($l_aData as $value){
	 

	  	$channel_id = $value['cid'];
	  	$content_id = $value['id'];
	  	
	  	$fullurl = "http://".$value['slug'].".on.starsite.com";

		   	$profileID = $value['google_analytics_internal_id']; 	    
		if($channel_id!=24 && $channel_id!=8 && $channel_id!=12){
		    if($profileID!=NULL || $profileID!=""){

		    	$vid = $value['id'];
		    	$videoId = "$vid";
		        $pageurl = "/content/photo/$videoId/";

			    $tableId = "ga:$profileID";

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views = Gastar::calcPageViewTotals($data);
				}else{
					$page_views = 0;
				}
			    $pageurl = "/content/photo/$videoId/?page_type=landingpage";
	            

			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_lp = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_lp = 0;
				}
			    $pageurl = "/content/photo/$videoId/?mtest=1";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_mtest = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_mtest = 0;
				}

			    $pageurl = "/content/photo/$videoId/?page_type=twitter";
			  
			    $data = Gastar::queryCorePageViewReportingApi($tableId, $analytics,$start_date,$end_date,$videoId,$pageurl);

			    if($data!=""){
			    	$page_views_twitter = Gastar::calcPageViewTotals($data);
			    }else{
					$page_views_twitter = 0;
				}

			    $page_views = $page_views + $page_views_lp + $page_views_mtest + $page_views_twitter;

				
				$l_sSql = "INSERT INTO sc_reports_daily (time_stamp,channel_id,content_id,content_type,page_views_ga) 
							VALUES ('2015-07-13',$channel_id,$content_id,'photo',$page_views)"; 	

				$l_xSql->runQuery($l_sSql);
				
			
		    }

		    $count++;
		}

	 }
	 echo $count;
  }
 
?>