
<!-- BEGIN PAGE -->
<?php  setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>

<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Payments Manager
                </div>
         
              <?php

                  $months= array(
                    'All',
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July ',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December',
                    'By_Months'
                ); 

                 ?>
           
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class='duration'>Content</th>
                            <th class='duration'>Duration/Time</th>
                            <th class='duration'>Views</th>
                            <th class='duration'>Revenue</th>
                            <th class='duration'>Payment</th>
                            
                        </tr>
                    </thead>
                    <tbody id="reportBody">  
                        
                         <?php 

                                  foreach ($report_by_channel as $report_channel){
                               ?>
                        
                                <tr>
                                   

                                    <td>
                                   
                                     <?php 


                                        echo strtoupper($report_channel['month']); 

                                     ?>
                                   
                                    </td>
                          
                                    <td>
                                   <?php echo gmdate("H:i:s", $report_channel['total_duration']); ?>
                                  
                                   
                                    </td>
                                   

                                    <td>
                                   <?php echo number_format($report_channel['total_views']); ?>

                           
                                  
                                   
                                    </td>
                                   

                                    <td>

                                   <?php echo money_format('%.2n',$report_channel['total_revenue']) . "\n"; ?>
                                  
                                   
                                    </td>

                                    
                                    
                                    <td class="center" style="text-align: center;">
                          
                                      <?php
                                          echo 
                                          '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="#">'
                                          .
                                          '<i class="icon-edit" ></i>Cash Out'.
                                          '</a>';
                                      ?>
                                  
                                    </td>
                                </tr>
                                
                               <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
