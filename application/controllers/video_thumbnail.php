<?php

class Video_Thumbnail extends CI_Controller {

	public function index() {
		
		$f = isset($_REQUEST['f']) ? $_REQUEST['f'] : '';

		$f = UPLOAD_DIR . $f;
		if(empty($f) || !file_exists($f) ) exit();
		
		$w = isset($_REQUEST['w']) ? $_REQUEST['w'] : '';
		$h = isset($_REQUEST['h']) ? $_REQUEST['h'] : '';
		$icon = isset($_REQUEST['icon']) ? $_REQUEST['icon'] : 0;
		
		$ext = strtolower(pathinfo($f, PATHINFO_EXTENSION));


		$l_sRequestImage = md5($w.$h.$icon).basename($_REQUEST['f']);
		$l_sRequestImageFullPath = UPLOAD_DIR.'tmp/'.$l_sRequestImage;
		// echo $l_sRequestImageFullPath;
		if(file_exists($l_sRequestImageFullPath)){
			header('Content-Type: image/png');
			$contents = file_get_contents($l_sRequestImageFullPath);
			header("Content-length: " . strlen($contents));	
			echo $contents;
			exit;
		}
		
		
		$hSource = NULL;
		
		if($ext == 'png') {
			$hSource = imagecreatefrompng ($f);
		}
		elseif($ext == 'jpg' || $ext == 'jpeg') {
			$hSource = imagecreatefromjpeg ($f);
		}
		elseif($ext == 'bmp') {
			$hSource = imagecreatefromwbmp ($f);
		}
		elseif($ext == 'gif') {
			$hSource = imagecreatefromgif ($f);
		}
		
		if($hSource == NULL) exit();
	
		list($s_width, $s_height) = getimagesize($f);
		
		if(empty($w) && empty($h)) {
			$w = $s_width;
			$h = $s_height;
		}
		else if(empty($h)) {
			$h = $s_height * $w / $s_width;
		}
		else if(empty($w)) {
			$w = $s_width * $h / $s_height;
		}
		

		$hDest = imagecreatetruecolor($w, $h);
		
		imagecopyresampled($hDest, $hSource, 0, 0, 0, 0, $w, $h, $s_width, $s_height);
		if($icon) {
		
			$icon = RT_PATH.'/assets/img/play.png';
			$ext = exif_imagetype($icon);
			$hIcon = NULL;
			if($ext == IMAGETYPE_PNG) $hIcon = imagecreatefrompng ($icon);
			elseif($ext == IMAGETYPE_JPEG) $hIcon = imagecreatefromjpeg ($icon);
			elseif($ext == IMAGETYPE_BMP || $ext == IMAGETYPE_WBMP) $hIcon = imagecreatefromwbmp ($icon);
			elseif($ext == IMAGETYPE_GIF) $hIcon = imagecreatefromgif ($icon);
				

			list($isw, $ish) = getimagesize($icon);
			
			$iw = $w / 5;
			$ih = $iw / $isw * $ish;
		
			imagecopyresampled($hDest, $hIcon, $w / 2 - $iw / 2, $h / 2 - $ih / 2, 0, 0, $iw, $ih, $isw, $ish);
		}
		
		
		imagepng($hDest,$l_sRequestImageFullPath,9);
		ob_start();
		
		imagepng($hDest);		
		
		$contents = ob_get_clean();
		header('Content-Type: image/png');
		header("Content-length: " . strlen($contents));
		
		echo $contents;
		
		imagedestroy($hDest);
		imagedestroy($hSource);
		
	}
}