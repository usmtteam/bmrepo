<?php


class Cron_Push extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('language');
        $this->load->helper('cronhelper');
        
        $this->load->model('main_m');
	}

	public function index() {


	if(($pid = cronHelper::lock()) !== FALSE) {

		/*
		 * Cron job code goes here
		*/

	//	sleep(50); // Cron job code for demonstration


		
		$sql = 'select count(*) as `count`, channel_id from enr_feed_thread group by channel_id';
		$result = $this->main_m->db->query($sql)->result_array();

		foreach($result as $row) {

			if($row['count'] == 0) continue;

			$sql = 'select post_type, content_id from enr_feed_thread join enr_feed on enr_feed_thread.feed_id = enr_feed.id where channel_id = ' . $row['channel_id'];
			$rows = $this->main_m->db->query($sql)->result_array();
			$feeds = array();
			foreach($rows as $row2) {
				$feeds[] = array(
					'post_type' => $row2['post_type'],
					'content_id' => $row2['content_id']	
				);

			}
			
			
			$sql = "
			select *
			from enr_ud_token

			where
			user_id in (select user_id from enr_push_notification)
			and user_id in (select id from enr_fan where cid='{$row['channel_id']}')
			";

			$query = $this->main_m->db->query($sql);


			$rows = $query->result_array();

			$ud_tokens = array('IPHONE' => array(), 'ANDROID' => array());


			foreach($rows as $row1){
				$ud_tokens[$row1['ud_type']][] = $row1['ud_token'];
			}

			$post_type = $feeds[0]['post_type'];

			$data = array();
			$data['push_type'] = "main_feed";
			$data['aps'] = array('alert'=>"New " . $post_type ." posted!", 'sound'=>"default");
			$data['aps']['badge'] = 1;
			$data['feeds'] = $feeds;

			
			if($row['count'] > 1) $data['aps']['alert'] = $row['count'] . ' New Posts!';
			if($row['count'] > 1) $data['aps']['badge'] = intval ($row['count']);


	//		print_r($data);
			
			print_r($ud_tokens);

	//		$this->main_m->db->query('delete from enr_feed_thread where channel_id = ?', array($row['channel_id']));

	//		sendpush($ud_tokens['IPHONE'], $data);

			sendpush_android($ud_tokens['ANDROID'], $data);
			

		}
	cronHelper::unlock();
	}
	

}


	public function brightcove() {

	
		$query = 'select * from enr_video where is_publish = 0';
	
		$videos = $this->db->query($query)->result_array();
		
		$this->load->library('BrightCove');
		foreach($videos as $video) {
			$status = $this->brightcove->getVideoStatus($video['brightcove_media_id']);
			echo 'checking video id : ' . $video['id'] . ' with bc id : ' . $video['brightcove_media_id'] . '...' . $status . '\n';


			if($status == 'ACTIVE') {
				$query = 'update enr_video set is_publish = 2 where id = ?';
				$this->db->query($query, $video['id']);
				echo '- status has been updated.\n';
			}
		}
		exit();
	}
}