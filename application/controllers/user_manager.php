<?php
class User_Manager extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('user_manager_m');
        $this->load->model('main_m');
	
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

	}
	
  function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }

	   function index(){
          $this->data['user_manager'] = $this->user_manager_m->get_user_manager();
          
            $this->load->view('user_manager', $this->data);

      }


  public function user_manager_edit(){
        
        $id = $this->uri->segment(3);
        $this->data['user_manager'] = $this->user_manager_m->get_user_manager_id($id);
        
        $this->data['id'] = $this->data['user_manager'][0]['id'];
        $this->data['username'] = $this->data['user_manager'][0]['username'];
        $this->data['email'] = $this->data['user_manager'][0]['email'];
        $this->data['user_group_id'] = $this->data['user_manager'][0]['user_group_id'];
       
         if ($this->input->post('submit')){  
         $this->update_user_manager_id($id);
          }

         $this->load->view('user_manager_edit', $this->data);
   

    }


     public function update_user_manager_id($id) {
     
   
 
      $username = $this->input->post('username');
      $email =$this->input->post('email');
      $user_group_id = $this->input->post('user_group_id');

      

      $data = array (
            'username' => $username,
             'email' => $email,
            'user_group_id' => $user_group_id 
            
            );


            if  ($this->user_manager_m->update_user_manager($id, $data))
            {
                redirect(site_url('user_manager/index'));
            }
        
            return;
    
      
    }


     public function user_manager_add($post){

      

          $this->form_validation->set_rules('name', 'name', 'required|max_length[512]|xss_clean');


           if ($this->form_validation->run()==false){}
            else
           {
           if ($this->form_validation->run() == true) 

          { 

        
             if($this->input->post('name',false) == true)

                {
                
             
                {
            $this->data['user_manager'] = $this->user_manager_m->add_user_manager();


             redirect(site_url('user_manager/index'));


                }
                }
           
              
                {
                  
                    if(empty($name)){
            show_error("Enter a name!");
              return;
                
                                     }
                }

                }
           
            
          
        }
                  

    
        
        $this->load->view('user_manager_add', $this->data);
    }



      
  


     public function confirm_deletion(){
     
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("users");
        redirect(site_url('user_manager/'));
     }

    
     

     

   


 

 
}