<?php



class Verify extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
		
        $this->load->library('email');
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );
        date_default_timezone_set('UTC');
        $this->email->initialize($config);
		
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('language');
	}
	
	public function index() {
		$v = isset($_REQUEST['v']) ? $_REQUEST['v'] : '';
		
        $this->load->model('main_m');
		
		$fan = $this->main_m->getFanByVerificationCode($v);
		
		$data = array();
		if($fan == NULL || empty($v)) {
			$data['text'] = "Sorry, we can't process your request.";
		}
		else {
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			$password = implode($pass);
			
			$hash_pass = $this->ion_auth->hash_password($password);
			//update password
			$update = array(
				'password' => $hash_pass,
				'verification_code' => ''
			);
			$this->main_m->db->where('id', $fan['id']);
			$this->main_m->db->update('enr_fan', $update);
			
			$this->email->clear();
			$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
			$this->email->to($fan['email']);
			$this->email->subject($this->config->item('site_title', 'ion_auth') . ' - New password has been set.');
			
			
			$message = 'You have reset your account password.<BR><BR>
New password : ' . $password;
			$this->email->message($message);
			
			$this->email->send();
				
				
			$data['text'] = 'We have sent a new password to your email address.';
			
		}
		$this->load->view('verify', $data);
	}
}