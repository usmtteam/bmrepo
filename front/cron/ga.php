<?

	ini_set('memory_limit','1280M');
    ini_set('upload_max_filesize','100M');
    ini_set('max_execution_time',100000);
    require_once (__DIR__.'/../include/lib/google/Google/autoload.php');

	require(__DIR__.'/../config/settings.php');	


	class StarsiteGA {

		var $_xAnalytics;

		
		private function startService(){
  
  			if($this->_xAnalytics)
  				return;

  			$service_account_email = '812317047113-200e6drtn0eu4g6m5f2oba0oi8bu3ljl@developer.gserviceaccount.com';
  			$key_file_location = __DIR__.'/../include/lib/google/client_secrets.p12';

  
  			$client = new Google_Client();
  			$client->setApplicationName("HelloAnalytics");
  			$this->_xAnalytics = new Google_Service_Analytics($client);

			
  			$key = file_get_contents($key_file_location);
  			$cred = new Google_Auth_AssertionCredentials($service_account_email,array(Google_Service_Analytics::ANALYTICS_READONLY),$key);
  			
  			$client->setAssertionCredentials($cred);
  			if($client->getAuth()->isAccessTokenExpired()) {
    			$client->getAuth()->refreshTokenWithAssertion($cred);
  			}
		}

		public function refreshGAProperties(){

			$this->startService();

			try {
				$accounts = $this->_xAnalytics->management_accountSummaries->listManagementAccountSummaries();
			} catch (apiServiceException $e) {
				print 'There was an Analytics API service error '. $e->getCode() . ':' . $e->getMessage();
				exit;

			} catch (apiException $e) {
				print 'There was a general API error '. $e->getCode() . ':' . $e->getMessage();
				exit;
			}

			$l_aFoundItems = array();
			$l_xSql = AppController::getInstance()->getConnection();

			foreach ($accounts->getItems() as $account) {
				foreach ($account->getWebProperties() as $property) {
					foreach ($property->getProfiles() as $profile) {
						$l_aFoundItems[$property->getId()] =  $profile->getId();
					}
				}
			}
						

			// print_r($l_aFoundItems);
			foreach($l_aFoundItems as $k => $v){
				$l_sSql = 'UPDATE enr_chanel SET google_analytics_internal_id = "'.$v.'" WHERE google_analytics_key = "'.$k.'"';
				$l_xSql->runQuery($l_sSql);
			}
		}

		public function getReports(){

			$this->startService();
			$l_xSql = AppController::getInstance()->getConnection();
			$l_sSql = 'SELECT * FROM enr_chanel WHERE google_analytics_internal_id IS NOT NULL';
			$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

			foreach($l_aData as $l_xRow){
				print_R($l_xRow);
				$l_sCurrentMonthStart = date("Y-m-d", mktime(0,0,0,date('m'),1,date('Y')));
				$l_sCurrentMonthStart = '2015-06-01';
				$l_sCurrentMonthEnd = date("Y-m-d");
				$l_sKey = 'ga:users';
				$l_sKey = 'ga:sessions';
				$l_sKey = 'ga:pageviews';
				$optParams = array();

			 	$l_aReturnData =  $this->_xAnalytics->data_ga->get(
			      'ga:'.$l_xRow['google_analytics_internal_id'],
			      $l_sCurrentMonthStart,
			      $l_sCurrentMonthEnd,
			      $l_sKey,
			      $optParams);
				// print_R($l_aReturnData);
				$l_aReturnData = (Object) $l_aReturnData;
				$l_sValue = $l_aReturnData->totalsForAllResults[$l_sKey];
				

				$l_sSql = 'UPDATE enr_chanel SET google_analytics_current_total = "'.$l_sValue.'" WHERE id = '.$l_xRow['id'];
				$l_xSql->runQuery($l_sSql);
				// exit;
			}
		}

 }


// function getFirstprofileId(&$analytics) {
//   // Get the user's first view (profile) ID.
// //
// //https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A102648882&start-date=30daysAgo&end-date=yesterday&metrics=ga%3AtotalEvents&dimensions=ga%3AeventAction&segment=gaid%3A%3A-1
// //

// //https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A95680195&start-date=30daysAgo&end-date=yesterday&metrics=ga%3Ausers&segment=gaid%3A%3A-1
//   $optParams = array();

//  $l_aData =  $analytics->data_ga->get(
//       'ga:'.'95680195',
//       '2015-01-01',
//       '2015-05-30',
//       'ga:users',
//       $optParams);
// print_R($l_aData);


// 	}


	$l_xStarsiteGA = new StarsiteGA();
	$l_xStarsiteGA->refreshGAProperties();
	$l_xStarsiteGA->getReports();
	

	 


?>