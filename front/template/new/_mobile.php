<!DOCTYPE html>
<html lang="en">
<head>
<?
	$l_bIsMobile = false;
	if($l_nDeviceType == DEVICE_TYPE_MOBILE){
		$l_bIsMobile = true;
		$l_aZoneUnits = array('ZONE_1'=>11,'ZONE_2'=>12,'ZONE_3'=>13);
		$l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=14';
	}else{
		$l_aZoneUnits = array('ZONE_1'=>3,'ZONE_2'=>1,'ZONE_3'=>2);
		$l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=4';
		
	}

	// $l_sAdServerURL = 'http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=229&LR_SCHEMA=vast2';
	$l_sMetaTitle = preg_replace('/^"|"$/',"",json_encode('WATCH VIDEO: '.$l_xAssetResult['description']));
	$l_sMetaDescription = preg_replace('/^"|"$/',"",json_encode($l_xAssetResult['description']));
?>
	<meta charset="utf-8" />
    <title><?= $l_xChannel->site_title ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	<style type="text/css">
		@font-face {
		font-family: 'starsitefont';
		src:url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.eot?e5v9gc');
		src:url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.eot?#iefixe5v9gc') format('embedded-opentype'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.woff2?e5v9gc') format('woff2'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.ttf?e5v9gc') format('truetype'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.woff?e5v9gc') format('woff'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.svg?e5v9gc#starsitefont') format('svg');
		font-weight: normal;
		font-style: normal;
		}
		.ssfont{font-family:'starsitefont';}
		*{ margin:0; padding:0; outline: none }
		body{ font-family:'Arial'; font-size: 12px; color:#222; background:#EDEDED; font-weight: normal;}
		.container{ margin: 0 auto; text-align: center; padding:0px; max-width:980px}
		.header{ display: block; position: relative; border:1px solid #E2E2E2; margin:10px 0 0 0; min-height:100px}
		.header .nav{ float:left}
		.header .nav a{ text-decoration: none; color:#222; margin-top:10px; }
		.header .nav .as{ background:#fff; padding:10px 15px;float:left; margin-left:2px; border-radius:6px; border:1px solid #E2E2E2; box-shadow:0 0 5px #E2E2E2; font-weight:bold;}
		.header-over{ position: absolute; top: 0; width:100%}
		.header .login{ float:right}

		.logo{ font-size:32px; margin:10px; float: left;}
		h1{ font-size:22px; margin:10px 0; min-height:100px}
		h3{ font-size: 16px; font-weight: normal; line-height:20px; }
		.content{ display: block; width:100%; max-width:960px; margin:0 auto }
		.footer{ display: block; margin:10px 0 100px 0}

		.flowplayer-holder{ background:#000; margin:10px; box-shadow:0 0 5px #E2E2E2}
		.clr{ clear: both; height:0; overflow: hidden;}
		
		.col1{ float:left; width:70%; <?= (!$l_bIsMobile) ? 'max-width:660px;' : ''; ?>}
		.col2{ float:left; width:30%; <?= (!$l_bIsMobile) ? 'min-width:300px;' : ''; ?>}

		.assets-list{ max-height:500px; border:1px solid #E2E2E2; overflow-y: scroll; margin:10px 0 10px 0; box-shadow:0 0 5px #E2E2E2 }
		.assets-list ul{ margin: 0 auto; list-style: none;}
		.assets-list ul li{ text-align:left; font-size:11px }
		.assets-list ul li a{ outline: none; display: block; background: #fff; margin-bottom:1px; text-decoration: none; color:#222; }
		.assets-list ul li p{ padding:10px; margin-bottom:5px}
		.assets-list ul li p span{ font-size:10px; color:#999;}
		.assets-list ul li .thumb{ float: left; max-height:30%; max-width:80px; max-height:50px; overflow:hidden; margin:3px 6px 3px 3px}
		.assets-list ul li img{ }

		.page-unit-1{ margin-bottom: 10px}
		.page-unit-2{ margin-bottom: 10px}
		.desc-area{ padding:20px; background: #fff; text-align: left; margin:10px; border:1px solid #E2E2E2; box-shadow:0 0 5px #E2E2E2}
		.desc{ }
		.social{ border-top:1px solid #E2E2E2; margin:15px 0; padding:15px 0 }
		.social ul{  }
		.social ul li{ display: inline-block; }
		.social ul li a{ font-size:36px; color:#222; text-decoration:none  }

		.flowplayer > div.fp-ui > div.adContainer > div > div > video { position:relative !important; left:0px !important;top:0px !important; float:left !important; width:100%; height:100%;}

		@media screen and (max-width: 500px) {
    
      		*{/* border:1px solid #f00;*/}
    		body{ font-size: 9px;}
    		h3{ font-size: 11px;  line-height:15px; }
    		.assets-list ul li{ font-size:10px; line-height:12px }
    		.assets-list ul li p span{ font-size:9px }
    		.header .nav a{ position: absolute;}
    		.header{ padding-bottom: 0px}
    		.header .nav .as{ padding:6px 8px; font-weight: normal; border-radius:3px; width:50px}
    		.col1{ float:left; width:74%;}
    		.col2{ float:left; width:23.5%;}


			.col1{ float:left; width:100%;}
    		.col2{ float:none; width:92.5%; margin:0 auto; clear: both; }

			.assets-list{  max-height:none; overflow-y: visible; width:100%;}
    		.assets-list ul li a{ margin:2px 0}
    		.assets-list ul li p{ padding:3px;}
    		.header .nav .as:nth-child(2) {
    			margin-top: 70px;
			}
			.header .nav .as:nth-child(3) {
    			margin-top: 70px;
    			margin-left: 80px;
			}
			.logo{ font-size:22px;}
		}

	</style>
	<link rel="stylesheet" type="text/css" href="//d3hl6zmfu9utfu.cloudfront.net/fp/html5/skin/functional.css" />
	<link rel="stylesheet" type="text/css" href="//d3hl6zmfu9utfu.cloudfront.net/fp/html5/fp-ima/fp-ima.css" />
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="//s0.2mdn.net/instream/html5/ima3.js"></script>
	<script type="text/javascript" src="//d3hl6zmfu9utfu.cloudfront.net/fp/html5/flowplayer.min.js"></script>
	<script type="text/javascript" src="//d3hl6zmfu9utfu.cloudfront.net/fp/html5/fp-ima/fp-ima.min.js"></script>
	<script type='text/javascript' src='//scads.starsite.com/www/delivery/spcjs.php?id=1'></script>
	<script type="text/javascript">
		flowplayer.conf = {
  		autoplay : true,
	   	wmode : 'transparent',
	   	tooltip: false,
   		key: "$846027373127464"
		};
	//analytics: "UA-27182341-1",
	</script>
    <?

        //if($l_bIsVideo){ ?>



		<meta property="og:title" content="<?= $l_sMetaTitle ?>" />
		<meta property="og:description" content="<?= $l_sMetaDescription ?>"/>

		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
		<meta property="og:image" content="<?= $l_sImageUrl ?>" />
		<meta property="og:image:type" content="image/png" />
		<meta property="og:image:width" content="1200" />


		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Starsitenetwork" />
		<meta name="twitter:title" content="Starsite" />
		<meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
		<meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
		<meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


        <? 
        /*

          if($p_sPageType=="inline"){ ?>
                
                <meta property="og:type" content="movie" />
                <meta property="og:video:height" content="270"/>
                <meta property="og:video:width" content="480"/>
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:video" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
                <meta property="og:video:secure_url" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
                <meta property="og:image" content="<?= $l_sImageUrl ?>"/>
                <meta property="og:video:type" content="application/x-shockwave-flash">
            <? } else if($p_sPageType == "landingpage"){ ?>
               
                <meta property="og:type" content="website" />
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:image" content="<?= $l_sImageUrl ?>" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:width" content="1200" />

                
        <? } else if($p_sPageType=="twitter_player"){ ?>
       
               <meta property="twitter:card" content="player">
               <meta property="twitter:title" content="<?= $l_xChannel->site_title ?>">
               <meta property="twitter:description" content="<?= $l_sMetaDescription ?>">
               <meta property="twitter:image" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/watermark/<?= $l_xAssetResult['image_path']; ?>">
               <meta property="twitter:image:width" content="480">
               <meta property="twitter:image:height" content="270">
               <meta property="twitter:site" content="@starsitenetwork">
               <meta property="twitter:player:width" content="480">
               <meta property="twitter:player:height" content="270">
               <meta property="twitter:player" content="<?= $l_sCurrentAbsoluteSecureURL; ?>">
       
        <? }else if($p_sPageType=="twitter"){ ?>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


        <? } else { ?>
              <meta property="og:type" content="website" />
                <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
                <meta property="og:image" content="<?= $l_sImageUrl ?>" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:width" content="1200" />
                
        <? }
    }else{  

        if($p_sPageType=="twitter_player"){ ?>

        <? //<meta name="twitter:card" content="photo" /> ?>
         <meta name="twitter:card" content="player" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_sMetaDescription; ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
        
        <? }else if($p_sPageType=="twitter"){ ?>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@Starsitenetwork" />
          <meta name="twitter:title" content="Starsite" />
          <meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
          <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
          <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


        <? }else{} ?>

        <meta property="og:title" content="<?= $l_sMetaDescription ?>" />
        <meta property="og:description" content="<?= $l_sMetaDescription ?>"/>
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
        <meta property="og:image" content="<?= $l_sImageUrl ?>" />
        <meta property="og:image:type" content="image/png" />        
    <? } 

    	*/ 

	
	$l_sBackgroundImage = strlen($l_xChannel->backgroundImage) > 2 ? '<img src="'.CDN_FRONT.'/'.$l_xChannel->url."/images/bg-sn.jpg?v=".RELEASE_VERSION.'" width="100%" />' : '';
	// $l_sBackgroundImage = '';

   ?>
</head>
<body>
	<div class="container">
	<?
		
				/*  LEADER BOARD  */
		
			?>
			<div class="page-unit-1">
				<script type='text/javascript'>
				<!--// <![CDATA[
    				OA_show(<?= $l_aZoneUnits['ZONE_1'] ?>);
    			// ]]> -->
    			</script>
			</div>

		<div class="header"><?= $l_sBackgroundImage ?>
			<div class="header-over">
				<div class="nav">
					<a href="http://www.starclubltd.com" class="logo ssfont" target="_blank">S</a>
					<a href="/content/photo/recent/" class="as">PHOTOS</a>
					<a href="/content/video/recent/" class="as">VIDEOS</a>
				</div>
				<div class="login">
					<!-- <a href="#">LOG IN</a> -->
				</div>
					<h1<?= (strlen($l_sBackgroundImage) > 5) ? ' style="display:none" ' : '' ?>><?= $l_xChannel->site_header_title ?></h1>
				</div>
				<div class="clr"></div>
			</div>
		<div class="clr"></div>
		<div class="content">
			
			<?
		
				/*  Video  */
		
			?>
			<? $l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mp4/",".jpg",$l_xAssetResult['destination'])) ?>


			<div class="col1">

				<div class="flowplayer-holder">
				<? if (1) { ?>
					<div class="flowplayer" style="background-image:url(<?= $l_sImage ?>)">
					<script type="text/javascript">
				    flowplayer_ima.conf({ 
					    ads: [
					      { time : 0, adTagUrl : "<?= $l_sAdServerURL ?>" }
					    ]
					 });
					</script>
					   <video>
					      <source type="video/webm" src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mp4/","-480.webm",$l_xAssetResult['destination']) ?>">
					      <source type="video/mp4"  src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mp4/","-480.mp4",$l_xAssetResult['destination']) ?>">
					   </video>
					</div>
				</div>
				<? } ?>
				<script>

				flowplayer(function(api, root) {
				 
				  // when a new video is about to be loaded
				  api.bind("load", function() {
				 	console.log('++++++++ load');
				 	
				  // when a video is loaded and ready to play
				  }).bind("ready", function() {
				 	console.log('++++++++ ready');
				 	var dur = parseInt(api.video.duration);
				 	
				 	var pause = Math.round((.25)*dur)+1;
				 	pause = pause*1000;

				 	var pausehalf = Math.round((.5)*dur)+1;
				 	pausehalf = pausehalf*1000;

				 	var pausethreefourths = Math.round((.75)*dur)+1;
				 	pausethreefourths = pausethreefourths*1000;

        
        			ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Impression', dur);
        			setTimeout(function(){
        				ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
        			},pause);
        			setTimeout(function(){
        				ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Half', dur);
        			},pausehalf);
        			setTimeout(function(){
        				ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Three-Fourths', dur);
        			},pausethreefourths);
          			
				  });

				  api.bind("finish", function() {
				  	console.log('++++++++ finish');
				  	var dur = parseInt(api.video.duration);
        
        			ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);

				  });
				 
				  api.bind("error", function() {
				  	console.log('++++++++ error');
				  	var dur = parseInt(api.video.duration);
        
        			ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);

				  });

				});

				</script>

				<div class="desc-area">
					<div class="desc">
	                  <h3><?= $l_xAssetResult['description'] ?></h3>
	                  <br /><br />
	                  <span><? echo Utils::time_passed($l_xAssetResult['time_stamp']); ?></span>
	                </div>

	                <div class="social">
		                <ul>
		                	<li><a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont fb">f</a></li>
		                	<li><a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>?page_type=twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tw">t</a></li>
		                	<li><a href="http://tumblr.com/widgets/share/tool?canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tb">u</a></li>
		                	<li><a href="https://plus.google.com/share?url=canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont gp">g</a></li>
		                </ul>
	                </div>

	                <div class="fb-comments" data-href="<?= $l_sCanonicalLink ?>" data-width="100%" data-numposts="5"></div>
	                

				</div>




			</div>


			<div class="col2">
			<?
			if($l_bIsVideo) {
				?>
				<div class="assets-list">
					<ul><? AppController::getInstance()->outputVideoListItemsFlowPlayerMobile($l_aFPVideos,$l_xChannel);  ?></ul>
				</div>
				<?
	        }
	        ?>


	        <? if($l_bIsMobile){ ?>

	        </div>
	        <div class="clr"></div>
	        <div>
	        <? } ?>

	        <div class="page-unit-2">
				<script type='text/javascript'>
				<!--// <![CDATA[
    				OA_show(<?= $l_aZoneUnits['ZONE_2'] ?>);
				// ]]> -->
				</script>
			</div>


			<div class="page-unit-3">
				<script type='text/javascript'>
				<!--// <![CDATA[
			    	OA_show(<?= $l_aZoneUnits['ZONE_3'] ?>);
				// ]]> -->
				</script>
			</div>


	        </div>
	        <div class="clr"></div>
		</div>
		<div class="clr"></div>
		<div class="footer">
			<p>Copyright Starsite 2015</p>
		</div>
	</div>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
</script>
<? /*<iframe src="/template/tracking_master.php?t=<?= $l_xChannel->domain ?>-mtest=1" width="0" height="0" frameborder="none"></iframe> */ ?>

</body>
</html>