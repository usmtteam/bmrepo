enriqueApp.controller('loginEmailController', ['$scope', '$rootScope', '$http', '$modal', function($scope, $rootScope, $http, $modal) {
	$scope.error = '';
	
	$scope.onLoginButton_click = function() {
		var email = $('#text_email').val();
		var password = $('#text_password').val();
		
		if(email == '') {
			$scope.error = 'Please enter your email.';
		}
		else if(password == '') {
			$scope.error = 'Please enter your password.';
		}
		else {
			$http({method: 'GET', url: $rootScope.get_api_url() + '&option=login&email=' + email + '&password=' + password + '&ud_token=WEB'}).
	        success(function(data, status, headers, config) {
	        	if(data.status == true) {
	        		$rootScope.user_token = data.token;
	        		$rootScope.current_user = data.info;
	        		
	        		$rootScope.dialogLoginNow.close();
	        		
	        		$rootScope.saveCookie();
	        	}
	        	else {
	        		$scope.error = data.message;
	        	}
	        }).
	        error(function(data, status, headers, config) {
	        	
	        	$scope.error = 'Please try again.';
	        });
		}
	};

	
	$scope.onCloseButton_click = function() {
		$rootScope.dialogLoginNow.close();
		$rootScope.onShowLoginButton_click();
	};
}]);