<!-- BEGIN PAGE -->
<form id="frm" method="post">
<input type="hidden" name="cmd" id="cmd" value="" />
<input type="hidden" name="fan_id" id="fan_id" value="" />
<input type="Hidden" id="page" value="<?php echo $page; ?>" />
<input type="hidden" name="admin_type" id="admin_type" value="" />
<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div class="container-fluid">

			<!-- BEGIN PAGE HEADER-->
			<div class="row-fluid">
				<div class="span12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
					<h3 class="page-title">
						Fans
<?php if($selfuser->admin_type!=2): ?>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('fan/add_user'); ?>" class="btn green">
                            New Fan
                        </a>
                    </div>
                    <?php endif; ?>
					</h3>

					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
		
			<!-- BEGIN PAGE CONTENT-->
			<div class="row-fluid">
				<div class="span12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box">
							<div id="form_head_back_fan">Active Fans</div>
						<div class="portlet-body pb-fan">
							<center>
							Search name : <input type="text" name="search" id="search" value="<?php echo $search;?>" />
							</center>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
                                        <th ></th>
                                        <th>Username</th>
                                        <th>E-Mail</th>
                                        <th>Admin Types</th>
                                        <th>Credit</th>
                                        <th>Published</th>
										<th>Draft</th>
                                        <th class=""><a href="<?php echo site_url("fan/active/lastlogin_{$lastlogin}"); ?>">Last login</a></th>
                                        <th class=""><a href="<?php echo site_url("fan/active/joindate_{$joindate}"); ?>">Join Date</a></th>
										<th class=""><a href="<?php echo site_url("fan/active/lastpost_{$lastpost}"); ?>">Last Post</a></th>
                                        <th class="hidden-480">Block</th>
                                        <th class="hidden-480">Suspend</th>
										<th class="hidden-480">Account</th>
										<th class="hidden-480">Delete</th>
									</tr>
								</thead>
								<tbody>
	                            <?php foreach ($users as $user):?>

									<tr class="odd gradeX">
                                        <td class="photo-thumbnail-td ">
                                            <img height="50" width="50" class='photo-thumbnail pull-right' src="<?php if(is_file(UPLOAD_DIR.$user['img_url'])){ echo UPLOAD_URL.$user['img_url']; }else{ echo UPLOAD_URL."photo/no-image-50.png"; } ?>">
                                        </td>
                                        <!--<td ><a href='<?php echo site_url('fan/view')."/".$user['id']; ?>'><?php echo $user['name'];?></a></td>-->
                                        <td ><?php echo $user['name'];?></td>
                                        <td ><?php echo $user['email'];?></td>
                                        <td>
                                        	<select class="user_admin_type_s" name="user_admin_type" data-id="<?php echo $user['id']; ?>">
                                        		<option value="">User</option>
                                        		<!--<option value="0"<?php echo $user['admin_type'] === '0' ? ' selected="selected"' : ''; ?>>CMS Admin</option>-->
                                        		<option value="1"<?php echo $user['admin_type'] === '1' ? ' selected="selected"' : ''; ?>>Admin(Post/Delete)</option>
                                        		<option value="2"<?php echo $user['admin_type'] === '2' ? ' selected="selected"' : ''; ?>>Admin(Delete)</option>
                                        	</select>
                                        </td>
                                        <td ><?php echo $user['credit'];?></td>
                                        <td class="center" >
                                            <input class="fan_published" data-fanid="<?php echo $user['id']; ?>" id="publish_<?php echo $user['id']; ?>" type="checkbox" name="publish_<?php echo $user['id']; ?>" <?php if($user['is_publish']) echo " checked "; ?> >
                                            
                                        </td>
                                        <td class=" center" align="center">
                                            <input class="fan_drafed" data-fanid="<?php echo $user['id']; ?>" id="draft_<?php echo $user['id']; ?>" type="checkbox" name="publish_<?php echo $user['id']; ?>" <?php if($user['is_draft']) echo " checked "; ?> >                                        
                                        <td class=""><?php echo $user['last_login'] == '1970-01-01' || strtotime($user['last_login']) == 0 ? '' : $user['last_login'];?></td>
                                        <td class=""><?php echo $user['join_date'];?></td>
                                        <td class=""><?php echo $user['lastpost'] == '0000-00-00' || $user['lastpost'] == '1970-01-01' || strtotime($user['lastpost']) == 0 ? '' : $user['lastpost'];?></td>
                                        <td class="center"><input class="fan_blocked" data-fanid="<?php echo $user['id']; ?>" type="checkbox" <?php if($user['is_block']) echo " checked "; ?> ></td>
                                        <td class="center"><input class="fan_suspended" data-fanid="<?php echo $user['id']; ?>" type="checkbox" <?php if($user['is_suspend']) echo " checked "; ?> ></td>
                                        <td class=""><a class="btn mini black" href="<?php echo site_url('fan/edit_user/'.$user['id']); ?>">EDIT</a></td>
										<td class="span1 hidden-480 center">
                                            <a class="btn mini black" href="javascript:confirm_del('<?php echo $user['id']; ?>')">Delete</a>
                                        </td>
									</tr>
	                            <?php endforeach;?>

								</tbody>
							</table>
							
			                <center>
                            
			                <div style="float:left;">Total count : <?php echo $total_count; ?></div>
			                <div class="dataTables_paginate paging_bootstrap pagination"><ul>
			                	<li class="prev<?php echo $page == 0 ? ' disabled' : ''?>"><a data-page="<?php echo max(0, $page - 1);?>">← Previous</a></li>
			                	<?php $s = max(0, $page - 5); for($i = $s; $i < min($total_page_count, $s + $page_count); $i++) :?>
			                	<li class="<?php echo $page == $i ? ' active' : ''; ?>"><a data-page="<?php echo $i;?>"><?php echo $i+1; ?></a></li>
			                	<?php endfor; ?>
			                	<li class="next<?php echo $page == $total_page_count-1 ? ' disabled' : ''?>""><a data-page="<?php echo min($total_page_count-1, $page + 1);?>">Next →</a></li>
							</ul></div>
			                </center>
			                <script language="javascript">
			                $(document).ready(function() {
			                    $('.pagination a').click(function(e) {
				                    $('#page').val($(e.target).attr('data-page'));
				                    refresh();
			                    });
			                });
			                </script>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
		</div>
		<!-- END PAGE CONTAINER-->
</div>
</form>
		<!-- END PAGE -->
<script>
    function confirm_del(id) {
        if(confirm("Do you want to delete this user?")) {
            document.location.href = "<?php echo site_url('fan/delete_user'); ?>/" + id;
        }
    }
$(document).ready(function() {
	$('#search').keydown(function(e) {
		if(e.keyCode == 13) {
			e.preventDefault();
            $('#page').val(0);
			refresh();
		}
	});
	$('select[name="user_admin_type"]').change(function(e) {
		var target = $(e.target);
		
		$('#cmd').val('change_admin_type');
		$('#fan_id').val(target.attr('data-id'));
		$('#admin_type').val(target.val());
		$('#frm').submit();
	});
});
function refresh() {

    var base_url = '<?php echo site_url('fan/active'); ?>';
    var search_val = $('#search').val();
    var url = base_url + '/' + $('#page').val() + '/' + search_val;
    document.location.href = url;
    
}
</script>

