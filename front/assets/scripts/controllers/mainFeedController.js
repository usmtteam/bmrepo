enriqueApp.controller('mainFeedController', ['$scope', '$rootScope', '$http', '$routeParams', function($scope,$rootScope, $http, $routeParams) {
	$scope.page = 1;
    $scope.feeds = [];
    $scope.star = {};
    $scope.is_loading = false;
    $scope.show_comment_box = true;
    //$scope.loadData();
    $scope.view_m_comments = false;
    
    $scope.loadData = function () {
    	if($scope.is_loading) return;
    	CommonLib.show_loading_bubble();
    	$scope.is_loading= true;
    	$http({method: 'GET', url: $rootScope.get_api_url() + '&option=get_main_feed&page='+$scope.page+'&comment_count=3'}).
        success(function(data, status, headers, config) {

        	$rootScope.updateBrightCoveInfo(data);
        	if(data.status == true) {
        		$scope.star = data.star;
        		$scope.feeds = $scope.feeds.concat( data.feeds );
        		
        		if(data.feeds.length > 0) $scope.page ++;
        	}
        	CommonLib.hide_loading_bubble();
        	
        	$scope.is_loading = false;
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();

        	$scope.is_loading = false;
        });
    };

    $scope.Limit = 5;
   
    $scope.getMore = function (e, feeds) {

       
        $scope.Limit = $scope.Limit + 5;
      
       
        
        // Call service to get more data
        var target = $(e.target);
        var content_id = target.attr('data-content-id');
        var comment_count = target.attr('data-comment-count');
        
        var show_all = false;
        var i;
        for(i = 0; i < feeds.length; i++) {
            if(feeds[i].content_id == content_id) {
                CommonLib.show_loading_bubble();
                var url = $rootScope.get_api_url() + '&option=get_comment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&post_type=' + feeds[i].post_type + '&content_id=' + feeds[i].content_id;
                $http({method: 'GET', url: url}).
                success(function(data, status, headers, config) {
                    CommonLib.hide_loading_bubble();
                    if(data.status == true) {
                        feeds[i].comments = data.comments;
                        
                        
                        show_all = feeds[i].show_all_comment;
                        if(show_all == null || typeof show_all == 'undefined') show_all = true;
                        
                        
                        feeds[i].show_all_comment = show_all;
                        

                        if(show_all) {
                            target.html('View more comments');
                        }
                        else {
                            target.html('View more comments');
                        }
                        var comm_diff = comment_count - $scope.Limit;

                        if(comm_diff<5)
                        {
                            target.html('Hide comments');
                        }

                        if($scope.Limit>comment_count){
                            show_all = !show_all;
                            feeds[i].show_all_comment = show_all;
                            target.html('View more comments');
                        }
                    }           
                }).
                error(function(data, status, headers, config) {
                    CommonLib.hide_loading_bubble();
                });
              
                break;
            }
        }
    };

    $scope.loadCommentData = function (post) {
        var post = post;
        
        CommonLib.show_loading_bubble();
        //$scope.is_loading= true;
        var url = $rootScope.get_api_url() + '&option=get_comment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&post_type=' + post.post_type + '&content_id=' + post.content_id;
                $http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {

            if(data.status == true) {
                
               $scope.post.comments =  data.comments;
                
                
            }
            CommonLib.hide_loading_bubble();
            
            $scope.is_loading = false;
        }).
        error(function(data, status, headers, config) {
            CommonLib.hide_loading_bubble();

            $scope.is_loading = false;
        });
    };
    
    $scope.loadPostData = function(post_type, post_id) {
    	$http({method: 'GET', url: $rootScope.get_api_url() + '&option=get_post_detail&post_type=' + post_type + '&content_id=' + post_id}).
        success(function(data, status, headers, config) {
        	
        	if(data.status == true) {
        		$rootScope.onViewPostDetail(null, true, data.post[0], data.star, true, true);
        		
        		//$rootScope.putMetaTag(data.post[0], data.star);
        	}
        }).
        error(function(data, status, headers, config) {
        });
    };
    
    if(typeof $routeParams.post_type != undefined && $routeParams.post_type != null) {
    	$scope.loadPostData($routeParams.post_type, $routeParams.post_id);
    }
}]);