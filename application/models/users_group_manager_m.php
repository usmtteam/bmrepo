<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_Group_Manager_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

 // get list of users group 
    
    function get_users_group_manager(){

     $sql = "SELECT *
                FROM users_groups";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }

    // add users group manager

    function add_users_group_manager($post){


     $this->load->database();
     $hub_id= $this->input->post('hub_id');
     $group_name = $this->input->post('name');
    
            $post = array (
            'hub_id' => $hub_id, 
            'group_name' => $group_name,
            );


         $insert_query = "INSERT IGNORE INTO users_groups (hub_id, group_name) 
                     VALUES('$hub_id', '$group_name')";


                   
          $this->db->query($insert_query); 
    


     }



     //  delete users group manager

    function confirm_deletion($data){
     $this->db->select('*');
     $this->db->from('users_groups');
     $this->db->where('id',$data);
     $query = $this->db->get();
     $result=$query->result();
     return $result;

     }


     // update users group manager


     function update_users_group_manager($id,$data){

     $this->db->where('id', $id);
     $result = $this->db->update('users_groups', $data);
      return $result;

     }


     // get users group manager id

    function get_users_group_manager_id($data){
     $this->db->select('*');
     $this->db->from('users_groups');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
     
     }
 




   
}