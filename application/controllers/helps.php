<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helps extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->chanel = $this->session->userdata('chanel');
        $this->data['chanel'] = $this->chanel;
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
    }
    
    function _remap($method) {
        $this->load->view('header_v');
        $this->load->view('sidebar_v');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){
        $cid = $this->chanel['id'];
        
        $this->load->view('auth/admins', $this->data);
    }
    
    function managehelps(){
		$this->data['title'] = 'Help';
		$this->manage('help');
    }
    function manageterms(){
		$this->data['title'] = 'Terms';
		$this->manage('terms');
    }
    function managepravacy(){
		$this->data['title'] = 'Privacy';
		$this->manage('pravacy');
    }
    function manageshop(){
		$this->data['title'] = 'Shop';
		$this->manage('shop');
    }
    function managetourdate(){
		$this->data['title'] = 'Events';
		$this->manage('tourdate');
    }
	
	function manage($type) {
		$cid = $this->chanel['id'];

        $helps = $this->main_m->getHelpsContent($cid, $type);
        $this->data['post']  = $helps;
        $this->data['post']['type']  = $type;
        $this->_update_helps($type);

        $this->load->view('managehelps', $this->data);
	}
    
    private function _update_helps($type){
        if (isset($_REQUEST['content']))
        {
            $content = isset($_REQUEST['content']) ? $_REQUEST['content'] : '';
            $cid = $this->chanel['id'];
            $this->main_m->updateHelpsContent($cid, $type, $content);
            
			
			$helps = $this->main_m->getHelpsContent($cid, $type);
			$this->data['post']  = $helps;
			$this->data['post']['type']  = $type;
        }
		
    }
    
}

