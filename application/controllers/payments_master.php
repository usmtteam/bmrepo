
<?php
class Payments_Master extends CI_Controller 

{
  function __construct()
  {
    parent::__construct();
    $this->load->library('ion_auth');
    $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


    $this->load->helper('url');
    $this->load->helper('csv');
    $this->load->helper('download');
  
    $this->load->helper('language');
    $this->load->model('payments_master_m');
        $this->load->model('main_m');
  
  
    $this->form_validation->set_error_delimiters(
        $this->config->item('error_start_delimiter'),
        $this->config->item('error_end_delimiter')
           
    );
    $this->chanel = $this->session->userdata('chanel');
    $this->data['chanel'] = $this->chanel;
    $this->data['show_errors'] = array();

  }
  
  function _remap($method) {
        
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }

  function index(){

      $user_id = $this->session->userdata('user_id');

      $id = $user_id;


     $report_by_channel = $this->payments_master_m->get_reports_by_id($id, $month_input = 'by_months');

         $this->data['report_by_channel'] = $report_by_channel;


           $this->load->view('payments_list', $this->data);
      
  
      
  }


}