<!-- BEGIN PAGE -->

<? 
/*
   $accountId = GA_ACCOUNT_ID;
   $fullurl = "http://".$chanel['url'].".on.starsite.com";

   $analytics = Gastar::getService(); 
   $propertyID = Gastar::getIDfromlistProperties($accountId, $fullurl, $analytics);
   $profileID = Gastar::getIDfromlistProfiles($accountId,$propertyID, $analytics);
*/
   $total_rev = $post['total_revenue'];

   $total_views = $post['total_views'];
?>
<style>
#map_canvas path:nth-child(57) {
fill:#cccccc;
}
</style>
<div class="page-content">
	<div class="container-fluid" id="dashboard">
           <h1>Dashboard</h1>
        <div id="top_data">

<!--            <div class="span4" style="text-align:center"><font style="color:#69b459; font-size:45px;text-align:center;font-weight:300;">$--><?//=number_format(round(($total_rev),0), 0);?><!--</font></div>-->
<!--            <div class="span4" style="text-align:center"><font style="color:#6ca79f; font-size:45px; text-align:center;font-weight:300;">--><?//=number_format($total_views);?><!--</font></div>-->
<!--            <div class="span4" style="text-align:center"><font style="color:#e29e6d; font-size:45px; text-align:center;font-weight:300;">--><?//=$post['engagement'];?><!--%</font></div>-->

            <div class="span6" style="text-align:center"><font style="color:#69b459; font-size:45px;text-align:center;font-weight:300;">$<?=number_format(round(($total_rev),0), 0);?></font></div>
            <div class="span6" style="text-align:center"><font style="color:#6ca79f; font-size:45px; text-align:center;font-weight:300;"><?=number_format($total_views);?></font></div>

            <div style="width:100%;margin-top:40px;">

<!--                <div class="span4" style="text-align:center"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Revenue</font></div>-->
<!--                <div class="span4" style="text-align:center"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Views</font></div>-->
<!--                <div class="span4" style="text-align:center"><font style="color:#95999b; font-size:23px; text-align:center;font-weight:300;">Engagement</font></div>-->

                <div class="span6" style="text-align:center"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Revenue</font></div>
                <div class="span6" style="text-align:center"><font style="color:#95999b; font-size:20px; text-align:center;font-weight:300">Views</font></div>

            </div>
    </div>


        <div id="map" style="width: 95%; height: 187px; margin-bottom:60px;"></div>

    </div>

		<div id="bottom_dashboar_wrapper">
            <h1>Recent Posts</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Thumbnail</th>
                        <th>Caption</th>
                        <th>Duration in seconds</th>
                        <th>Revenue</th>
                        <th>Views</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i=0;
                foreach($feeds as $feed):
                    $ids = explode("_", $feed['id']);
                    if ($ids[0] == "video" && ($feed['video_row']['video_url'] != '0' && $feed['video_row']['video_url'] != "NULL" && $feed['video_row']['video_url'] != NULL)) {
                        $post_controller_name = "videoEmbed";
                    } else {
                        $post_controller_name = $ids[0];
                    }
                ?>
                    <tr <? if($i%2==1){ echo "class='odd'"; }else{} ?>>
                        <td class="feed-thumbnail-td ">
                        <?php if($feed['image_path']!=""){

                            echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="'.$feed['image_path'].'"></a><br/>';
                        } else {  echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="/assets/img/click-to-view.jpg"></a><br/>'; } ?>
                        </td>
                        <td class="span2"><div style='word-wrap:break-word; width: 200px'><?php if($feed['caption']){echo substr($feed['caption'], 0, 200);}else{ echo "Processing - Content will be available shortly"; } ?>
                            <?php if($feed['is_sticky']) :?>
                            <span style="color:red;font-weight:bold;"><sup>STICKY</sup></span>
                            <?php endif;?>
                        </div></td>
                              <td>
                                  <?php if($feed['duration'] != "0" && $feed['duration'] != ""){

                                      echo floor($feed['duration']);

                                  }else{ echo "N/A"; } ?>
                              </td>
                        <td class="green">$<?=number_format(floor($feed['revenue']), 0);?></td>
<!--                        <td class="blue">--><?//=number_format($feed['page_views_ga']);?><!--</td>-->
                        <td class="blue"><?=number_format($feed['total_views']);?></td>

                        <?php if ($feed['is_publish']==0){
                            $permission = "Transcoding";
                        } elseif ($feed['is_publish']==1) {
                            $permission = "Published";
                        } elseif ($feed['is_publish']==2) {
                            $permission = "Draft";
                        } elseif ($feed['is_publish']==3) {
                            $permission = "Removed";
                        } ?>
                        <td><?php echo $permission ?></td>
                        <td><?php echo $feed['start_date']; ?></td>

                        <td>
                        <?php
                            $ids = explode("_", $feed['id']);
                            $post_controller_name = $ids[0];

                            if ($feed['video_row']['video_url'] != '0' && $feed['video_row']['video_url'] != 'NULL' && $feed['video_row']['video_url'] != NULL){
                                $feed_copy_id = str_replace('video', 'videoEmbed', strtolower($feed['id']));
                            } else {
                                $feed_copy_id = $feed['id'];
                            }
                            if ($feed['is_publish'] =='1'){

                            echo
                            ' <a class="btn" href="javascript:post_unpublish(\''.$feed['id'].'\')">'.
                              'Unpublish'.
                             '</a>'; } if ($feed['is_publish'] =='3') {echo' <a class="btn" href="javascript:post_republish(\''.$feed['id'].'\')">'.
                              'Re-Publish'.
                             '</a>'; }


                            echo
                            '<a class="btn" href="javascript:confirm_del(\''.$feed['id'].'\')">'.
                            'Delete'.
                            '</a>';
                        ?>
                        </td>
                    </tr>
                 <? if($i>5){ break; }   ?>
                 <? $i++; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
		</div>
	</div>
</div>
    
  
<script>
    function confirm_del(id) {
        if(confirm("Do you want to delete this feed?")) {
            document.location.href = "<?php echo site_url('content/delete_home_feed'); ?>/" + id;
        }
    }

    <?php // Geo Location Count
        $geosCount = count($geos);
    ?>

    // Google Maps with Heatmap Layer
    var map, heatmap;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: {lat: 37.775, lng: -122.434},
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        heatmap = new google.maps.visualization.HeatmapLayer({
            data: getPoints(),
            map: map
        });
    }

    // Heatmap data
    function getPoints() {
        return [

            <?php for ($i = 0; $i < $geosCount; $i++): ?>

                new google.maps.LatLng(<?=$geos[$i][3]?>, <?=$geos[$i][4]?>)

                <?php if ($i !== ($geosCount - 1)): ?>
                    ,
                <?php endif; ?>

            <?php endfor; ?>

        ];
    }

    function post_unpublish(id) {
        if(confirm("Do you want to unpublish this post?")) {
            document.location.href = "<?php echo site_url('content/post_unpublish_homefeed'); ?>/" + id;
        }
    }  

      function post_republish(id) {
        if(confirm("Do you want to re-publish this post?")) {
            document.location.href = "<?php echo site_url('content/post_republish_homefeed'); ?>/" + id;
        }
    }    
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?libraries=visualization&callback=initMap">
</script>