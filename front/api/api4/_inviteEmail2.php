<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
    <td><table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="font-family:Arial; font-size:15px; color:#fff; text-align:center; line-height:18px; background:url(/assets/images/email/invite-bg.jpg) 0 0 repeat-x #000">
        <tr>
          <td height="25">&nbsp;</td>
        </tr>
        <tr>
          <td><img src="http://darwin.on.starsite.com/assets/images/email/sc-logo.png" width="70%" /></td>
        </tr>
        <tr>
          <td height="25">&nbsp;</td>
        </tr>
        <tr>
          <td><p style="font-sze:22px">You've been invited to join<br />
              the <strong>StarClub Curated Beta</strong> network.</p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><p>Get started by downloading<br />
              the app and logging in.</p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
              <a href="<?= $l_sDownloadLink2 ?>" style="background:#9C2DC3; padding:18px 0; color:#fff; text-decoration:none; border-radius:19px; min-width:60%; display:inline-block; font-size:22px;  margin:10px 0 0 0 ">Download App</a></p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td bgcolor="#EEEEEE" style="padding:10px"><table cellpadding="0" cellspacing="0" border="0" align="center" style="font-family:Arial;  font-size:16px; text-align:center; line-height:18px; color:#222;">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Your Channel name:<br />
            <strong><?= $l_sChannelName ?></strong></td>
        </tr>
        <tr>
          <td><p>&nbsp;</p></td>
        </tr>
        <tr>
          <td style="font-size:0.7em">CHANNEL DETAILS</td>
        </tr>
        <tr>
          <td style="font-size:0.7em">&nbsp;</td>
        </tr>
      </table>
      <table cellpadding="0" cellspacing="0" border="0" width="80%" align="center" bgcolor="#ffffff" style="font-family:Arial;  font-size:16px; text-align:left; line-height:20px; color:#222; background-color:#fff">
        <tr>
          <td style="padding:10px"><p>Username: <br />
              <strong><?= $l_sEmail ?></strong></p></td>
        </tr>
        <tr>
          <td style="padding:10px"><p>Password: <br />
              <strong><?= $l_sNewPass ?></strong></p></td>
        </tr>
        <tr>
          <td style="padding:10px"><p>Channel URL: <br />
              <strong><?= $l_sChannelURL ?></strong></p></td>
        </tr>


        <tr>
          <td align="center"><p>&nbsp;</p></td>
        </tr>
      </table>
      <table cellpadding="0" cellspacing="0" border="0" align="center" style="font-family:Arial;  font-size:12px; text-align:center; line-height:15px; color:#999;">
        <tr>
          <td><p>&nbsp;</p>
            <p>Questions? Email us <a href="#" style="color:#111; text-decoration:none"><strong>here</strong></a>.<br />
              <br />
              <br />
            </p></td>
        </tr>
        <tr>
          <td><br /><p style="font-size:9px; line-height:14px">STARCLUB Inc. 228 SANTA MONICA BLVD, SANTA MONICA, CA 90401<br />
              <strong><a href="http://www.starsite.com" style="color:#111; text-decoration:none">STARSITE.COM</a> </strong></p>
            <p></p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
