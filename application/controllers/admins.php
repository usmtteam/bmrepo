<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->chanel = $this->session->userdata('chanel');
        $this->data['chanel'] = $this->chanel;
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
    }
    
    function _remap($method) {
        $this->load->view('header_v');
        $this->load->view('sidebar_v');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){
        $cid = $this->chanel['id'];
        $admin_data = $this->getAdminInfos();
        $this->data['admin_data'] = $admin_data;
        $this->load->view('auth/admins', $this->data);
    }
    
    function getAdminInfos(){
        $admins = $this->main_m->getAdminInfos();
        $result = array();
        foreach($admins as $admin){
            switch($admin['admin_type']){
                case "0":
                	$result['id0'] = $admin['id'];
                    $result['email0'] = $admin['email'];
                    $result['phone0'] = $admin['phone'];
                break;
                case "1":
                	$result['id1'] = $admin['id'];
                    $result['email1'] = $admin['email'];
                    $result['phone1'] = $admin['phone'];
                break;
                case "2":
                	$result['id2'] = $admin['id'];
                    $result['email2'] = $admin['email'];
                    $result['phone2'] = $admin['phone'];
                break;
            }
        }
        return $result;
    }
    
    function admin0(){
        if(!$this->input->post('submit')){
            $this->index();
        }
        $admin_data = $this->getAdminInfos();
        $id = $this->input->post('id');
        $admin_data['email0'] = $this->input->post('email');
        $admin_data['phone0'] = $this->input->post('phone');
        $this->data['admin_data'] = $admin_data;
        
        $this->form_validation->set_rules('email', "email", 'required');
        $this->form_validation->set_rules('phone', "phone", 'required');
        if ($this->input->post('password'))
        {
            $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
        }
        if ($this->form_validation->run() === TRUE){
            $sql = array(
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
            );
            if ($this->input->post('password'))
            {
                $sql['password'] = $this->ion_auth->hash_password($this->input->post('password'));
            }
            $cid = $this->chanel['id'];
            $this->db->where('id', $id);
            $this->db->update('users', $sql);
            redirect("admins", '');
        }else{
            $this->data['admin_message0'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->load->view('auth/admins', $this->data);
        }
    }
    
    function admin1(){
        if(!$this->input->post('submit')){
            $this->index();
        }
        $admin_data = $this->getAdminInfos();
        $id = $this->input->post('id');
        $admin_data['email1'] = $this->input->post('email');
        $admin_data['phone1'] = $this->input->post('phone');
        $this->data['admin_data'] = $admin_data;

        $this->form_validation->set_rules('email', "email", 'required');
        $this->form_validation->set_rules('phone', "phone", 'required');
        if ($this->input->post('password'))
        {
            $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
        }

        if ($this->form_validation->run() === TRUE){
            $sql = array(
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
            );
            if ($this->input->post('password'))
            {
                $sql['password'] = $this->ion_auth->hash_password($this->input->post('password'));
            }
            $sql['cid'] = $this->chanel['id'];

            $this->main_m->updateAdmin($sql, 1);
            redirect("admins", '');
        }else{
            $this->data['admin_message1'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->load->view('auth/admins', $this->data);
        }
    }
    
    function admin2(){
        if(!$this->input->post('submit')){
            $this->index();
        }
        $admin_data = $this->getAdminInfos();
        $id = $this->input->post('id');
        $admin_data['email2'] = $this->input->post('email');
        $admin_data['phone2'] = $this->input->post('phone');
        $this->data['admin_data'] = $admin_data;

        $this->form_validation->set_rules('email', "email", 'required');
        $this->form_validation->set_rules('phone', "phone", 'required');
        if ($this->input->post('password'))
        {
            $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
        }

        if ($this->form_validation->run() === TRUE){
            $sql = array(
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
            );
            if ($this->input->post('password'))
            {
                $sql['password'] = $this->ion_auth->hash_password($this->input->post('password'));
            }
            $sql['cid'] = $this->chanel['id'];

            $this->main_m->updateAdmin($sql, 2);
            redirect("admins", '');
        }else{
            $this->data['admin_message2'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->load->view('auth/admins', $this->data);
        }
    }
    
    function helps(){
        $this->load->view('managehelps', $this->data);
    }

}

