<style>

</style>
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Notifications
                    <small></small>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('push_notifications/notifications_add'); ?>" class="btn blue">
                        Add New 
                        </a>
                    </div>
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="span2">Platform</th>
                            <th>Recipient</th>
                            <th>Title</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th>Sent</th>
                            <th class="span2">Edit</th>
                            <th class="span2">Delete</th>
                        </tr>
                    </thead>
                    <tbody>  
           
                


                      
                
                        <tr>
                            <!--<td class="feed-thumbnail-td ">
                            <?php if(is_file(UPLOAD_DIR.$event['image_path'])){  ?>
                                <img class='feed-thumbnail pull-right' src="<?php echo UPLOAD_URL.$feed['image_path']; ?>">
                            <?php } ?>
                            </td>-->
 

                       
                        <?php 
                    foreach($platform_notification as $notifications): ?>
                       

                    
                        <td>
                                                <?php                                                               
                                                        if ($notifications['platform_id']==1){
                                                                        echo "Syndicator";
                                                        }
                                                    
                                                ?>


                                                <?php                                                               
                                                        if ($notifications['platform_id']==2){
                                                                        echo "Breaking Movement";
                                                        }
                                                    
                                                ?>
                                            

                                                <?php                                                               
                                                        if ($notifications['platform_id']==3){
                                                                        echo "Breaking Movement Pro";
                                                        }
                                                    
                                                ?>


                                                <?php                                                               
                                                        if ($notifications['platform_id']==4){
                                                                        echo "Proflo";
                                                        }
                                                    
                                                ?>
                                            
                                            
                            </td>


                             
                         <td><?php echo $notifications['recipients']; ?></td>
                         <td><?php echo $notifications['title']; ?></td>
                         <td><?php echo $notifications['message']; ?></td>
                         <td><?php echo $notifications['date']; ?></td>
                        <td>
                                                <?php                                                               
                                                        if ($notifications['status']==0){
                                                                        echo "No";
                                                        }
                                                    
                                                ?>


                                                <?php                                                               
                                                        if ($notifications['status']==1){
                                                                        echo "Yes";
                                                        }
                                                    
                                                ?>
                                            

                                              
                                            
                            </td>

          
                            </td>
                            <td class="center" style="text-align: center;">
                          
                            <?php
                                echo 
                                '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="'.site_url('push_notifications/notifications_edit/'.$notifications['id']).'">'.
                                '<i class="icon-edit" ></i>Edit'.
                                '</a>';
                            ?>
                        
                            </td>
                            <td class="center" style="text-align: center;">
                                
                            <?php
                                echo 
                                '<a class="btn" style="padding-left: 1px; padding-right: 17px;" href="javascript:confirm_deletion(\''.$notifications['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                       
                            </td>
                        </tr>

                             
            
           
                               <?php endforeach; ?> 
                 

               
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    function confirm_deletion(id) {
        if(confirm("Do you want to delete this notification?")) {
            document.location.href = "<?php echo site_url('push_notifications/confirm_deletion'); ?>/" +id;
        }
    }    
</script>