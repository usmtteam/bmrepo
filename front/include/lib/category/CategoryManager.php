<?php


class CategoryManager extends FrontCoreManager{

    private $_xSql;
    private $_nCacheObjectDuration = MEMCACHE_DURATION_OFLO;


    public function init(){

        $this->_xSql = AppController::getInstance()->getConnection();

    }

     // CATEGORY MENU AND LISTINGS 
        
    public function  getCategoryMenu($p_nId){
      $l_xCached = $this->getCategoryMenuFromMemcacheAndStoreIfFail($p_nId);
 
        return $l_xCached;
    }
    
    public function  getCatList($p_catlist){
      $l_xCached = $this->getCatListFromMemcacheAndStoreIfFail($p_catlist);
 
        return $l_xCached;
    }

   public function  getCatListByCatName($cat){
      $l_xCached = $this->getCatListByCatNameFromMemcacheAndStoreIfFail($cat);
 
        return $l_xCached;
    }

   public function  savecat($cat_name){
      $l_xCached = $this->saveCatToMemcache($cat_name);
 

        return $l_xCached;
    }

    // MEMCACHING


    public function getCategoryMenuFromMemcacheAndStoreIfFail($p_nId,$p_bBypassMemcache = false){

        $l_sMemcacheKey = Memcaching::makeKey(CategoryManager::returnItemKey($p_nId));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);


        if($p_bBypassMemcache || !$l_aCachedContent){
            $l_sSql = "SELECT * FROM viral_media_category
                       WHERE oflo_menu = '1' ";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);
            return $l_aData;
        }else{
            return $l_aCachedContent['value'];
        }
    }

    public function getCatListFromMemcacheAndStoreIfFail($p_catlist, $p_bBypassMemcache = false){
      // date_default_timezone_set('UTC');
      //    $utc_time = date("Y-m-d h:i:sa"); 

      //   date_default_timezone_set('America/Los_Angeles');
      //    $pacific_time = date("Y-m-d h:i:sa"); 

      //    $hours_diff = $utc_time - $pacific_time; 


        $l_sMemcacheKey = Memcaching::makeKey(CategoryManager::returnItemKey($p_catlist));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);
   
      if ($p_bBypassMemcache || !$l_aCachedContent){

         $l_sSql = "SELECT ec.url, ev.id, ev.description, ev.video_type, ev.image_path
                           FROM enr_video ev  
                           LEFT JOIN enr_chanel ec ON ec.id = ev.cid
                           WHERE ev.is_oflo = 'y' AND ev.is_publish= 1 
                           ORDER BY ev.id DESC
                           LIMIT 150
                       ";
                       
            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);
          return $l_aData;


          } else {

             return $l_aCachedContent['value'];
         }
    }

    public function getCatListByCatNameFromMemcacheAndStoreIfFail($cat, $p_bBypassMemcache = false){
        // date_default_timezone_set('UTC');
        //  $utc_time = date("Y-m-d h:i:sa"); 

        // date_default_timezone_set('America/Los_Angeles');
        //  $pacific_time = date("Y-m-d h:i:sa"); 

        //  $hours_diff = $utc_time - $pacific_time; 

        $l_sMemcacheKey = Memcaching::makeKey(CategoryManager::returnItemKey($cat));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);
   
       if ($p_bBypassMemcache || !$l_aCachedContent) {

            $l_sSql = "SELECT ec.url, ev.id, ev.description, ev.video_type, ev.image_path
                               
                            FROM enr_video ev  
                          
                            LEFT JOIN enr_chanel ec ON ec.id = ev.cid

                              LEFT JOIN viral_media_cat_post vmcp ON vmcp.post_id = ev.id
                              LEFT JOIN viral_media_category vmc ON vmc.id = vmcp.cat_id
                              WHERE vmcp.content_type_id = 1  AND  ev.is_oflo = 'y' AND ev.is_publish= 1 
                          
                              AND vmc.cat_name LIKE '%$cat%'
         
                        ORDER BY ev.id DESC
                        
                        LIMIT 100";

            $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));

            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;

         } else {

            return $l_aCachedContent['value'];
         }
    }

    public function getCategoryfromAssetId($p_nAssetId,$p_bBypassMemcache = false){

        $l_sMemcacheKey = Memcaching::makeKey(CategoryManager::returnItemKey($p_nAssetId));
        $l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

        if($p_bBypassMemcache || !$l_aCachedContent){
               $l_sSql = "SELECT oc.cat_name
                        FROM oflo_category oc
                        LEFT JOIN viral_media_category vmc ON oc.id = vmc.id
                        LEFT JOIN viral_media_cat_post vmcp ON vmc.id = vmcp.cat_id
                        WHERE vmcp.post_id = $p_nAssetId";
                        
             $l_aData = $this->_xSql->buildObject($this->_xSql->runQuery($l_sSql));
  
            Memcaching::getInstance()->store($l_sMemcacheKey,$l_aData,$this->_nCacheObjectDuration);

            return $l_aData;

        } else {
            return $l_aCachedContent['value'];
        }
    }

    // STATIC

    public static function returnItemKey($p_sSlug){
        return 'oflo-category-object-'.$p_sSlug;
    }

}

?>