<!-- BEGIN PAGE -->
<?php
//    phpinfo();
?>
<style>

</style>

<div class="page-content">
    <?php 
        $this->load->view('header_feed');
    ?>
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
        
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("", 'id="videofileupload" class="form-horizontal"');
                    echo form_error("description");
                    echo form_error("tags");
                    echo form_error("video_type");
                    echo form_error("video_url");

                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>

                    <div class="row-fluid">
                        <div class="span6">
                        
                            <?php include(__DIR__.'/feed_template_description.php'); ?>

                            Embed Video Type:
                            <select class="gray" name="video_type">
                                    <option value=""></option>
                                    <option value="<?=EMBED_VIDEO_TYPE_ID_YOUTUBE?>" <?php if($post['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE){echo "selected";}?> >YouTube</option>
                                    <option value="<?=EMBED_VIDEO_TYPE_ID_VIMEO?>" <?php if($post['video_type'] == EMBED_VIDEO_TYPE_ID_VIMEO){echo "selected";}?> >Vimeo</option>
<!--                                     <option value="<?=EMBED_VIDEO_TYPE_ID_VEVO?>" <?php if($post['video_type'] == EMBED_VIDEO_TYPE_ID_VEVO){echo "selected";}?> >Vevo</option>
 -->                                    <option value="<?=EMBED_VIDEO_TYPE_ID_VINE?>" <?php if($post['video_type'] == EMBED_VIDEO_TYPE_ID_VINE){echo "selected";}?> >Vine</option>
                                    <option value="<?=EMBED_VIDEO_TYPE_ID_INSTAGRAM?>"<?php if($post['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){echo "selected";}?> >Instagram</option> </select>

                            <div class="control-group">
                                <font size="1" face="arial, helvetica, sans-serif">
                                </font><br>
                                <?php  echo form_input("video_url",$post['video_url'],'class="large m-wrap" placeholder="Embed Video URL"');?>
                            </div>


                    <?php include(__DIR__.'/feed_template.php'); ?>
                        <div class="span6" >
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="border-style:none !important; width: 400px; height: 300px;">
                                    <input type="hidden" id="video_img_value" name="video_img_value" value="">

                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" id="video_custom_img" style="max-width: 400px; max-height: 300px; line-height: 20px;"></div>
                                <div>
                                <span class="btn btn-file">
                                    <span class="fileupload-new">Select image</span>
                                    <span class="fileupload-exists">Change</span>

                                    <input style="float:right; width:200px;" type="file" name="image" class="default" id="videoPhotoUpload" />
                                </span>
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    <br>
                                    <font size="1" face="arial, helvetica, sans-serif"> Thumbnail automatically created if left blank!
                                    </font><br>
                                </div>
                            </div>
                            <div class="control-group">
                                
                                <div class="customarea">
                                <?php  echo form_textarea("product_widget",$post['product_widget'],'class="large m-wrap" id="product_widget" maxlength="5000" placeholder="Product_wiget" style="height:400px; margin-left:40px;"');?>
                                </div>
                            </div>
                        </div>
                    </div><!-- end of row-fluid -->
                    <div class="form-actions">
                        <?php echo form_submit('submit', "Share",' id="submit_now", class="btn blue"');?>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->

                     <div id="spinner" class="spinner" style="display:none;">
                        <img id="img-spinner" src="<?php echo ASSETS_DIR; ?>/img/loader2.gif" alt="Loading"/>
                    </div>

                        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
                        <!-- The Templates plugin is included to render the upload/download listings -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
                        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
                        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
                        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
                        <!-- The basic File Upload plugin -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
                        <!-- The File Upload file processing plugin -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
                        <!-- The File Upload user interface plugin -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
                        <!-- The main application script -->
                        <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>

                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>


