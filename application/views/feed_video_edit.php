<!-- BEGIN PAGE -->
<div class="page-content">
    
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">
                <div class="page-title">
                 Video Editor
                </div>
<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                   
                 <div class="portlet-body form pb-feed">
                    <?php 
//                    echo form_open_multipart("content/feed/video_edit/".$post['feed_id'], 'id="videofileupload" class="form-horizontal"');
                    echo form_open_multipart("", 'id="videofileupload" class="form-horizontal"');
                   

                        echo form_error("description");
                        echo form_error("credit");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <input type="hidden" id="ajaxuploadvideo" value="<?php echo site_url("content/upload_video"); ?>">
                     <div class="row-fluid" >
                        <div class="span6">
                           <div class="control-group">
                                
                                <div class="customarea">  
                                <?php  echo form_textarea("description",$post['description'],'class="large m-wrap" placeholder="Caption" style="height:80px;"');?>
                                </div>
                            </div>
                            <div class="control-group">
                               
                                 <font size="1" face="arial, helvetica, sans-serif"> Only 512 characters allowed!
                                    </font><br>
                                <?php  echo form_input("tags",$post['tags'],'class="large m-wrap" placeholder="Tags"');?>
                
                            </div>
                            <div class="control-group">
                                
                                <div class="">
                                   
                                    <table role="presentation" class="table table-striped span8" style='margin-bottom:0px; margin-left:0px;'>
                                        <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
                                            <?php if($post['destination']): ?>
                                                <tr class="template-download fade in">
                                                    <td class="gray name">
                                                        <a href="<?PHP if(UPLOAD_CLOUD.$post['destination']!="") echo UPLOAD_CLOUD.$post['destination']; ?>" title="" data-gallery="gallery" download="" style="color: #eeeeee !important; padding-left: 3px;">Video</a>
                                                    </td>
                                                    <td class="gray size"><span><?php echo $post['format_size']; ?></span></td>
                                                    <td class="gray" colspan="2"></td>
                                                    <td class="delete" style="position:absolute; border:none">
                                                        <input name="video" value="<?php echo $post['destination']; ?>" type="hidden">
                                                        <input type="hidden" name='destination' value='<?php echo $post['destination']; ?>'>
                                                        <input type="hidden" name='video_mime' value='<?php echo $post['video_mime']; ?>'>
                                                        <input type="hidden" name='video_size' value='<?php echo $post['video_size']; ?>'>
                                                        <button style="background:none; border:none !important; color:#aeaeae" class="btn red" data-type="" data-url="">
                                                            <i class="icon-trash icon-white"></i>
                                                            <span>Replace</span>
                                                        </button>
                                                    </td>
                                                </tr>                                    
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                        <!-- The loading indicator is shown during file processing -->
                                        <div class="fileupload-loading"></div>
                                      

                                      
                                        <br>
                                        <!-- The table listing the files available for upload/download -->
                                </div>
                            </div>
                                <div id="infos"> Video Duration: <?php echo $post['video_duration']; ?></div> 
                           

                            <div class="row-fluid" style="visibility:hidden">
                                <div class="span12">
                                    <script id="template-upload" type="text/x-tmpl">
                                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                                            <tr class="template-upload fade">
                                                <td class="name"><span>{%=file.name%}</span></td>
                                                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                {% if (file.error) { %}
                                                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                                                {% } else if (o.files.valid && !i) { %}
                                                    <td>
                                                        <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
                                                    </td>
                                                    <td class="start" >{% if (!o.options.autoUpload) { %}
                                                        <button class="btn">
                                                            <i class="icon-upload icon-white"></i>
                                                            <span>Start</span>
                                                        </button>
                                                    {% } %}</td>
                                                {% }  %}
                                                    
                                                <td class="cancel"></td>
                                            </tr>
                                        {% } %}
                                    </script>
                                    <!-- The template to display files available for download -->
                                    <script id="template-download" type="text/x-tmpl">
                                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                                            <tr class="template-download fade">
                                                {% if (file.error) { %}
                                                    <td class="name"><span>{%=file.name%}</span></td>
                                                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                                                {% } else { %}
                                                    
                                                    <td class="name">
                                                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                                                    </td>
                                                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                                                    <td colspan="2"></td>
                                                {% } %}
                                                <td class="delete">
                                                    <input type="hidden" name='destination' value='{%=file.path%}'>
                                                    <input type="hidden" name='video_mime' value='{%=file.video_mime%}'>
                                                    <input type="hidden" name='video_size' value='{%=file.video_size%}'>
                                                    <button class="btn red" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                        <i class="icon-trash icon-white"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </td>
                                            </tr>
                                        {% } %}
                                    </script>
                                </div>
                            </div>
    
                        <?php include(__DIR__.'/feed_template_edit.php'); ?> 
                        <div class="span6" style="">
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="width: 400px; height: 300px; border-style: none !important;">
                                    <input type="hidden" id="video_img_value" name="video_img_value" value="">
                                   <a href="<?php echo UPLOAD_CLOUD.$post['image_path']; ?>"><img id='video_img_value' style="max-height: 100%" src="<?php echo ((isset($post['image_path']) and $post['image_path']) ? UPLOAD_CLOUD.$post['image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" /></a>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" id="video_custom_img" style="max-width: 400px; max-height: 300px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input style="float:right; width:200px;" type="file" name="image" class="default" id="videoPhotoUpload" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                             </div>

                             <div class="control-group" style="margin-top:60px;">
                                
                                <div class="customarea">
                                <?php  echo form_textarea("product_widget",$post['product_widget'],'class="large m-wrap" id="product_widget" maxlength="5000" placeholder="Product Widget" style="height:80px; margin-left:40px;"');?>
                                </div>
                                <div style="max-width: 415px; margin: 0 auto; float:left; margin-left:40px; margin-top:20px"><? echo $post['product_widget']; ?></div>

                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                       <!--      <?php echo form_submit('refresh', "Refresh",' id="refresh_now", class="btn blue"');?>
                            <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <?php // echo form_submit('delimg', "Delete image",' class="btn blue"');?> -->
                        
                            <?php echo form_submit('submit', "Save",' id="submit_now", class="btn blue"');?>
                            <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <?php // echo form_submit('delimg', "Delete image",' class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                     <div id="spinner" class="spinner" style="display:none;">
                            <img id="img-spinner" src="<?php echo ASSETS_DIR; ?>/img/loader2.gif" alt="Loading"/>
                    </div>
                    <!-- END FORM-->
                 </div>
                                
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>


<style type="">
    .btn.fileupload-exists{
        display: none;
    }
</style>



    
<script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>
    <script>

</script>