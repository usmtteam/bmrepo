enriqueApp.controller('helpController', ['$scope', '$rootScope', '$http', function($scope,$rootScope, $http) {;
	$scope.contents = '';
    
    $scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	$http({method: 'GET', url: $rootScope.SERVER_URL + '/viewhelp/help/' + $rootScope.CHANNEL}).
        success(function(data, status, headers, config) {
        	$('#page #help_contents').append($(data));
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    }

    $scope.loadData();
}]);