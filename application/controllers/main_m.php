<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Main_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	//	$this->load->database();
	}
	
	
	function get_next_insert_idx($tbl_name) {

		$next_increment = 0;
		$strSql = "SHOW TABLE STATUS WHERE Name='$tbl_name'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		$next_increment = $row['Auto_increment'];
		
		return $next_increment;
	}
	
	function get_upload_path($path_type,$media_type='',$media_idx) {
		// local path
		if($path_type==0) {
			return $this->config->item("upload_path")."/".$media_type."/".$media_idx;
		}
		//http path
		if($path_type==1) { 
			return RT_PATH.substr($this->config->item("upload_path"),1)."/".$media_type."/".$media_idx;
		}
		return "http://".$_SERVER['HTTP_HOST'].RT_PATH.substr($this->config->item("upload_path"),1)."/".$media_type."/".$media_idx;
	}
	
	
	
	function delTree($dirPath) {

		if (! is_dir($dirPath)) {
	    //    throw new InvalidArgumentException("$dirPath must be a directory");
	    	return;
	    }
	    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
	        $dirPath .= '/';
	    }
	    $files = glob($dirPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            self::delTree($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($dirPath);
	}
	

	
}