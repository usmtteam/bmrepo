enriqueApp.controller('selectFacebookPageController', function($scope, $rootScope, $http, $sce) {
	
	$scope.fb_pages = [{id: '', name: 'My Timeline'}];
	$scope.page_id = $rootScope.current_user.facebook_page_id;
	$scope.updating = false;
	
	$scope.load = function() {
		
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				$scope.loadFacebookPages();
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						$scope.loadFacebookPages();
					}
					else {
						
					}
				}, {scope: 'email, manage_pages'});
			}
		});
	};
	
	$scope.loadFacebookPages = function() {


		CommonLib.show_loading_bubble();
		
		FB.api("/me/accounts", function(response) {
        	var i;
        	$scope.fb_pages = [{id: '', name: 'My Timeline'}];
        	for(i = 0; i < response.data.length; i++) {
        		$scope.fb_pages[i+1] = {
        			id: response.data[i].id,
        			name: response.data[i].name
        		};
        	}
        	$scope.$apply();
        	CommonLib.hide_loading_bubble();
		});
	};
	
	$scope.selectPage = function (page_id) {
		$scope.page_id = page_id;
	};
	
	$scope.updateFacebookPageSettings = function() {
		if($scope.updating) return;
		$scope.updating = true;
		var target = $('.select_facebook_page_dialog button');
		CommonLib.show_waiting_circle(target, 1);
    	var url = $rootScope.get_api_url() + '&option=update_user&facebook_page_id=' + $scope.page_id;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.user = data.info;
        		$rootScope.current_user = $scope.user;
        		$rootScope.saveCookie();
        	}
        	else {
        		$scope.error = data.message;
        	}
        	CommonLib.hide_waiting_circle();
        	$rootScope.selectFacebookPageDialog.close();
        }).
        error(function(data, status, headers, config) {
        	$scope.updating = false;
        	CommonLib.hide_waiting_circle();
        	$scope.error = 'Please try again.';
        });
	};
	
	$scope.load();
});