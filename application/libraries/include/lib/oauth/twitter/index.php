<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */

session_start();
require(dirname(__FILE__).'/../../../../config/settings.php');
require_once(TWITTER_OAUTH_CLASS);

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY,TWITTER_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/verify_credentials');


//Array ( [access_token] => Array ( [oauth_token] => 1328079619-3Ke3Ocuh43TX9UosqOgrCQGXcSD1s0alsqN5kLd [oauth_token_secret] => ItRRNegj28et7RSzPqokpZqCoSblIcc6gxPKSyicRU [user_id] => 1328079619 [screen_name] => Vtest8 ) [status] => verified )


/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham'));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992));
//$connection->post('friendships/destroy', array('id' => 9436992));

/* Include HTML to display on the page */
include(dirname(__FILE__).'/html.inc');
?>
