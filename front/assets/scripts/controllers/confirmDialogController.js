enriqueApp.controller('confirmDialogController', function($scope, $rootScope, $http, options) {
	
	$scope.options = options;
	$scope.button_text = (options.button_text == null || typeof options.button_text == undefined) ? {} : options.button_text;
	if($scope.button_text.ok == null || typeof $scope.button_text.ok == undefined) $scope.button_text.ok = 'OK';
	if($scope.button_text.cancel == null || typeof $scope.button_text.cancel == undefined) $scope.button_text.cancel = 'Cancel';
	$scope.onOk = function() {
		$scope.options.onOk();
		
		$rootScope.unlockConfirmDialog.close();
		$rootScope.unlockConfirmDialog = null;
	};

	$scope.onCancel = function() {
		
		$scope.options.onCancel();
		
		$rootScope.unlockConfirmDialog.close();
		$rootScope.unlockConfirmDialog = null;
	};
});