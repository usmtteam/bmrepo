 <?php

 $user_id = $this->session->userdata('user_id');
 $selfuser = $this->ion_auth->user($user_id)->row();

?>
<style>
#block,#block1,#block2,#block3,#block4,#block5,#block6,#block7,#block8,#block9,#block10{display:none;padding:0px;text-align:left;}
</style>
  <!-- BEGIN PAGE -->  
<div class="page-content">
     <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
           <div class="span12">
              <h3 class="page-title">
                 CPM Headquarters
              </h3>
              <!--<ul class="breadcrumb">
                <li><a href="<?php echo site_url("question/question_list"); ?>">Question</a></li>
                <li>&raquo;</li>
                <li><a href="<?php echo site_url("question/question_edit/".$question[0]->qid); ?>">Edit Question</a></li>
              </ul>-->
           </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
           <div class="span12">
<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box blue">
                 <div class="portlet-title">
                    <h4><i class="icon-reorder"></i>Edit Form</h4>
                    <div class="tools">
                    </div>
                 </div>
                 <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    
                    <?php 
                        echo form_open_multipart("dashboard/cpm_control/".$chanel['id'], 'class="form-horizontal" name="frmMain"');
                            echo form_error("chanel_name");
                            echo form_error("chanel_admin");
                            echo form_error("chanel_password");
                            echo form_error("chanel_confirm_password");
                            if(isset($show_errors)) {
                                if (is_array($show_errors)) {
                                    foreach($show_errors as $error) {
                                        echo "<div class='alert alert-error'>".$error."</div>";
                                    }
                                }
                                else{
                                    echo "<div class='alert alert-error'>".$show_errors."</div>";
                                }
                            }
                    ?>
                    <?php if(isset($success_message)): ?> 
                        <div class="alert alert-block alert-success fade in"><?php echo $success_message; ?></div>
                    <?php endif; ?>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <h3>Channel Name:<?php  echo $chanel['name'];?></h3>
                            </div>
                        </div>
                    </div>
                    <script>
                    var count = 0;
                    </script>
                    <div id="cpm_content">
                    <h4>Set CPM for certain date range</h4>
                           <div class="row-fluid  control-group">
                               <div class="span4">
                                        <div id="start_date_div_report" class="input-append">
                                            <input type="text" id="start_date_report" name="start_date_report" value="<?php echo $post['start_date_report']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report" class="input-append">
                                    <input type="text" id="end_date_report" name="end_date_report" value="<?php echo $post['end_date_report']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm",$post['cpm'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>

                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide"/><label for="cbxShowHide">Add Another CPM/Date Range</label>
                            </div>
                      </div>



                            <div class="row-fluid  control-group" id="block">
                            <div class="span4">
                                        <div id="start_date_div_report1" class="input-append">
                                            <input type="text" id="start_date_report1" name="start_date_report1" value="<?php echo $post['start_date_report1']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report1" class="input-append">
                                    <input type="text" id="end_date_report1" name="end_date_report1" value="<?php echo $post['end_date_report1']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm1",$post['cpm1'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide1"/><label for="cbxShowHide1">Add Another CPM/Date Range</label>
                            </div>

                            </div>




                            <div class="row-fluid  control-group" id="block1">
                            <div class="span4">
                                        <div id="start_date_div_report2" class="input-append">
                                            <input type="text" id="start_date_report2" name="start_date_report2" value="<?php echo $post['start_date_report2']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report2" class="input-append">
                                    <input type="text" id="end_date_report2" name="end_date_report2" value="<?php echo $post['end_date_report2']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm2",$post['cpm2'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide2"/><label for="cbxShowHide2">Add Another CPM/Date Range</label>
                            </div>

                            </div>

                            <div class="row-fluid  control-group" id="block2">
                            <div class="span4">
                                        <div id="start_date_div_report3" class="input-append">
                                            <input type="text" id="start_date_report3" name="start_date_report3" value="<?php echo $post['start_date_report3']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report3" class="input-append">
                                    <input type="text" id="end_date_report3" name="end_date_report3" value="<?php echo $post['end_date_report3']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm3",$post['cpm3'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide3"/><label for="cbxShowHide3">Add Another CPM/Date Range</label>
                            </div>

                            </div>

                            <div class="row-fluid  control-group" id="block3">
                            <div class="span4">
                                        <div id="start_date_div_report4" class="input-append">
                                            <input type="text" id="start_date_report4" name="start_date_report4" value="<?php echo $post['start_date_report4']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report4" class="input-append">
                                    <input type="text" id="end_date_report4" name="end_date_report4" value="<?php echo $post['end_date_report4']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm4",$post['cpm4'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide4"/><label for="cbxShowHide4">Add Another CPM/Date Range</label>
                            </div>

                            </div>



                            <div class="row-fluid  control-group" id="block4">
                            <div class="span4">
                                        <div id="start_date_div_report5" class="input-append">
                                            <input type="text" id="start_date_report5" name="start_date_report5" value="<?php echo $post['start_date_report5']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report5" class="input-append">
                                    <input type="text" id="end_date_report5" name="end_date_report5" value="<?php echo $post['end_date_report5']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm5",$post['cpm5'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide5"/><label for="cbxShowHide5">Add Another CPM/Date Range</label>
                            </div>

                            </div>

                            <div class="row-fluid  control-group" id="block5">
                            <div class="span4">
                                        <div id="start_date_div_report6" class="input-append">
                                            <input type="text" id="start_date_report6" name="start_date_report6" value="<?php echo $post['start_date_report6']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report6" class="input-append">
                                    <input type="text" id="end_date_report6" name="end_date_report6" value="<?php echo $post['end_date_report6']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm6",$post['cpm6'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide6"/><label for="cbxShowHide6">Add Another CPM/Date Range</label>
                            </div>

                            </div>

                            <div class="row-fluid  control-group" id="block6">
                            <div class="span4">
                                        <div id="start_date_div_report7" class="input-append">
                                            <input type="text" id="start_date_report7" name="start_date_report7" value="<?php echo $post['start_date_report7']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report7" class="input-append">
                                    <input type="text" id="end_date_report7" name="end_date_report7" value="<?php echo $post['end_date_report7']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm7",$post['cpm7'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide7"/><label for="cbxShowHide7">Add Another CPM/Date Range</label>
                            </div>

                            </div>


                            <div class="row-fluid  control-group" id="block7">
                            <div class="span4">
                                        <div id="start_date_div_report8" class="input-append">
                                            <input type="text" id="start_date_report8" name="start_date_report8" value="<?php echo $post['start_date_report8']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report8" class="input-append">
                                    <input type="text" id="end_date_report8" name="end_date_report8" value="<?php echo $post['end_date_report8']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm8",$post['cpm8'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide8"/><label for="cbxShowHide8">Add Another CPM/Date Range</label>
                            </div>

                            </div>


                            <div class="row-fluid  control-group" id="block8">
                            <div class="span4">
                                        <div id="start_date_div_report9" class="input-append">
                                            <input type="text" id="start_date_report9" name="start_date_report9" value="<?php echo $post['start_date_report9']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report9" class="input-append">
                                    <input type="text" id="end_date_report9" name="end_date_report9" value="<?php echo $post['end_date_report9']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm9",$post['cpm9'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>
                            <div class="span12">
                              <input type="checkbox" id="cbxShowHide9"/><label for="cbxShowHide9">Add Another CPM/Date Range</label>
                            </div>

                            </div>


                            <div class="row-fluid  control-group" id="block9">
                            <div class="span4">
                                        <div id="start_date_div_report10" class="input-append">
                                            <input type="text" id="start_date_report10" name="start_date_report10" value="<?php echo $post['start_date_report10']; ?>" readonly />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                            <div class="span4">
                                <div id="end_date_div_report10" class="input-append">
                                    <input type="text" id="end_date_report10" name="end_date_report10" value="<?php echo $post['end_date_report10']; ?>" readonly />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <?php  echo form_input("cpm10",$post['cpm10'],'class="large m-wrap" placeholder="CPM"');?>
                            </div>

                            </div>
                            <br><br>
                        </div>

                    <? if($post['preview']==true || $post['preview_more']==true){ ?>

                    <h3>Your reports will look something like this</h3>
                           <div class="row-fluid  control-group">
                               <div class="span4">
                                            <h4><? echo $post['preview_phrase_total']; ?></h4>
                                            <h5><? echo $post['preview_phrase']; ?></h5>
                                            <? if($post['preview_more']==true){ 
                                                  foreach($post['preview_phrase_more'] as $value){ ?>
                                                     <h5><? echo $value; ?></h5>
                                                  <? } ?>
                                            <? } ?>
                                        </div>
                                </div>


                    <? } ?>
                    
                    <script language="javascript">
                                $(document).ready(function() {
                                    $('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
                                    $('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});


                                    $('#start_date_div_report').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report').datetimepicker({format: 'yyyy-MM-dd'});

                                    $('#start_date_div_report1').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report1').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report2').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report2').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report3').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report3').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report4').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report4').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report5').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report5').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report6').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report6').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report7').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report7').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report8').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report8').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#start_date_div_report9').datetimepicker({format: 'yyyy-MM-dd'});
                                    $('#end_date_div_report9').datetimepicker({format: 'yyyy-MM-dd'});

                                    $('#cbxShowHide').click(function(){
                                      this.checked?$('#block').show(1000):$('#block').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide1').click(function(){
                                      this.checked?$('#block1').show(1000):$('#block1').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide2').click(function(){
                                      this.checked?$('#block2').show(1000):$('#block2').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide3').click(function(){
                                      this.checked?$('#block3').show(1000):$('#block3').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide4').click(function(){
                                      this.checked?$('#block4').show(1000):$('#block4').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide5').click(function(){
                                      this.checked?$('#block5').show(1000):$('#block5').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide6').click(function(){
                                      this.checked?$('#block6').show(1000):$('#block6').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide7').click(function(){
                                      this.checked?$('#block7').show(1000):$('#block7').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide8').click(function(){
                                      this.checked?$('#block8').show(1000):$('#block8').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide9').click(function(){
                                      this.checked?$('#block9').show(1000):$('#block9').hide(1000); //time for show
                                    });

                                    $('#cbxShowHide10').click(function(){
                                      this.checked?$('#block10').show(1000):$('#block10').hide(1000); //time for show
                                    });

                                });
                            </script>   
					
                      
                    <div class="form-actions">
                        <?php echo form_submit('submit', "Genrate Report",' class="btn blue"');?>
                        <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <? if($post['preview']==true){ ?>
                        <?php echo form_submit('refresh', "Apply CPM",' class="btn red"');?>
                        <? } ?>
                       
                    </div>

                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    <!-- END PAGE CONTENT--> 
    
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 

