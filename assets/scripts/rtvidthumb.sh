#!/bin/bash

# Script to test iOS video orientation and if need be rotate video thumbnnail is correct 



if test $# -lt 2 ; then 
	echo "Usage: rtvidthumb.sh input file (video.mp4)  imagepath(.jpg)!"
	exit 1
fi

declare ROTATEVALUE='Image roatated 0'
declare VF180="thumbnail,scale=480:360"  # supposed to be Facebook ideal for mobile etc.. '560x292'  or maybe 640:360
declare VF90="thumbnail,scale=480:360"  # supposed to be Facebook ideal for mobile etc.. '560x292'  or maybe 640:360



ffmpeg -i $1 2> /tmp/rtvidthumb.txt

if grep -q "rotate          : 270" /tmp/rtvidthumb.txt
then
        cp -rf /tmp/rtvidthumb.txt /tmp/last-270-rtvidthumb.txt
	ROTATEVALUE='Image rotated 270'
        echo $ROTATEVALUE
	ffmpeg -y -i $1  -vf $VF90 -frames:v 1 $2
	convert $2 -rotate 270 $2
	cp -rf $2 /tmp/last-270.jpg
	exit 0


elif grep -q "rotate          : 180" /tmp/rtvidthumb.txt
then
    	cp -rf /tmp/rtvidthumb.txt /tmp/last-180-rtvidthumb.txt
	ROTATEVALUE='Image rotated 180'
    	ROTATECMD='-vf transpose=1,transpose=1 rotate=0'
    	echo $ROTATEVALUE
	ffmpeg -y -i $1  -vf $VF180 -frames:v 1 $2
	convert $2 -rotate 180 $2
	cp -rf $2 /tmp/last-180.jpg
	exit 0

elif grep -q "rotate          : 90" /tmp/rtvidthumb.txt

then
	cp -rf /tmp/rtvidthumb.txt /tmp/last-90-rtvidthumb.txt
	ROTATEVALUE='Image rotated 90'
    	echo $ROTATEVALUE
	ffmpeg -y -i $1  -vf $VF90 -frames:v 1 $2
	convert $2 -rotate 90 $2
	cp -rf $2 /tmp/last-90.jpg
	exit 0
 
fi

echo $ROTATEVALUE
cp -rf /tmp/rtvidthumb.txt /tmp/last-0-rtvidthumb.txt
ffmpeg -y -i $1  -vf $VF180 -frames:v 1 $2
cp -rf $2 /tmp/last-0.jpg


