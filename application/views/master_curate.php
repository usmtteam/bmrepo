<!-- BEGIN PAGE -->
<?php
//    phpinfo();
?>
<style>

</style>
<div class="page-content">

    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">
 <div class="page-title">
  Master Curator Editor
                </div>

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
        
                 <div class="portlet-body form pb-feed">
                    <?php
//                    echo form_open_multipart("content/feed/video_edit/".$post['feed_id'], 'id="videofileupload" class="form-horizontal"');
                    echo form_open_multipart("", 'id="videofileupload" class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("tags");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    <input type="hidden" id="ajaxuploadvideo" value="<?php echo site_url("master_curater/upload_video"); ?>">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                
                                <div class="customarea">
                                <?php  echo form_textarea("description",$post['title'],'class="large m-wrap" placeholder="Caption" style="height:80px;"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                
                                 <font size="1" face="arial, helvetica, sans-serif"> Only 32 characters allowed!
                                    </font><br>
                                <div class="">
                                <?php echo form_input("tags",substr($post['keywords'], 0, 32),'class="large m-wrap" placeholder="Tags"');?>
                                </div>
                            </div>
                            <div class="control-group">

            <div class="span12" style="margin-bottom:15px;">
                <label>Category:</label>
            </div>
            
             <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Update Categories</button>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog" style="background-color:#000020">
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Choose Video Categories</h4>
                          </div>
                          <div class="modal-body">
                            
                            <table>
                                <tr>
                                  

                                   <?php 

                                        $i=0;
                                        foreach($viral_media_category as $category){
                                        $i++;

                                    ?>
                                    <td>
                                        <div style="display: inline-block; vertical-align: middel; text-align:center;">
                                            <?php echo $category['cat_name']; ?>
                                            <input style="text-align: center; vertical-align: middle; width: 35px;" type='checkbox' name='<?php echo $category['id'];?>'
                                                <?php     
                                                      
//                                                  $i=0;
//                                                      while($viral_media_xml[$i]){
//
//                                                        if ($viral_media_xml[$i]['id']==$post['copy_origin_id']){
//
//                                                          if($category['cat_name']==$viral_media_xml[$i]['category'])
                                                          if($category['cat_name'] == $post['category'])
                                                                        echo checked;

//                                                        }
//                                                        $i++;
//
//                                                      }
                                                                          
                                                            
                                                        
                                                ?>
                                            >
                                            
                                        </div>
                                    </td>
                                    <?php if($i==6){echo'</tr><tr>'; $i=0;}} ?>
                    
                                </tr>
                            </table>
                          </div>
                           <div class="modal-footer" style="position:left; background-color:#000020">
                            <?php echo form_submit('', "Save",' class="btn blue" data-dismiss="modal"');?>
                        </div>
                        </div>

                      </div>
                                               

                    </div>

        </div>

                          
                            <div class="control-group" style="margin:0px; width:220px; height:24px;">
                                <div class="">
                                   
                             </div>
                             </div> 
                             </div> 

                              <div class="span6" >
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 400px; height: 300px;">
                                    <input type="hidden" id="video_img_value" name="video_img_value" value="">
                                   <a href="<?php echo $post['thumbnail_url']; ?>"><img id='video_img' style="max-height: 100%" src="<?php echo (!empty($post['thumbnail_url']) ? $post['thumbnail_url'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" /></a>

                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 400px; max-height: 300px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file">
                                        <span class="fileupload-new">Select image</span>
                                        <span class="fileupload-exists">Change</span>

                                        <input style="float:right; width:200px;" type="file" name="image" class="default" id="videoPhotoUpload" /> 
                                    </span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                  
                  
                                </div>
                             </div>
                        </div>
                    </div>
                      
                     

                      
                
                
                      <div class="form-actions">
                          <?php echo form_submit('refresh', "Refresh",' id="refresh_now", class="btn blue"');?>
                          <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                            <?php echo form_submit('submit', "Save",' id="submit_now", class="btn blue"');?>
                            <span class="space7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <?php // echo form_submit('delimg', "Delete image",' class="btn blue"');?>
                        </div>
                      
                    <?php echo form_close();?>
                     <div id="spinner" class="spinner" style="display:none;">
                         <img id="img-spinner" src="<?php echo ASSETS_DIR; ?>/img/loader2.gif" alt="Loading"/>
                     </div>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#submit_now').click(function() {
            $('#spinner').show();
            $('#submit_now').hide();
            $('#refresh_now').hide();

        });
    });
</script>

<script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?php echo ASSETS_DIR; ?>/jquery-file-upload/js/main.js"></script>
<script>

</script>