

     <div class="new_upload_top">
        <div class="new_upload_top_title">Upload</div>
        <ul class="new_upload_menu">

            <li class="<?php if(strpos($this->uri->segment(2), 'video') !== false AND strpos($this->uri->segment(2), 'videoEmbed') === false){echo 'active';} ?>"><a href="<?php echo site_url('content/feed_video'); ?>">Video</a></li>
            <li class="<?php if(strpos($this->uri->segment(2), 'videoEmbed') !== false){echo 'active';} ?>"><a href="<?php echo site_url('content/feed_videoEmbed'); ?>">Embed Video</a></li>
            <li class="<?php if(strpos($this->uri->segment(2), 'photo') !== false){echo 'active';}?>"><a href="<?php echo site_url('content/feed_photo'); ?>">Photo</a></li>
            <li class="<?php if(strpos($this->uri->segment(2), 'curate') !== false){echo 'active';}?>"><a href="<?php echo site_url('content/curate'); ?>">Curate</a></li>
       
        </ul>
     </div>
