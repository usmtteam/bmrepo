/*
 * jQuery File Upload Plugin JS Example 7.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';
    
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
//             Uncomment the following to send cross-domain cookies:
//            xhrFields: {withCredentials: true},
        url: $("#ajaxuploadproductphoto").val(),
        autoUpload : true
        
    })



    $('#fileupload').bind('fileuploadchange', function (e) {
        if($('[name="photos[]"]').length >= 3 ){
            alert("Unable to upload 3 more images.");
            return false;
        }
    })


    var is_photo = false;

    $('#videoPhotoUpload').click(function(){
        is_photo = true;
    })

    $('#videofileupload').submit(function(){
        if ( $('#video_custom_img img').attr('src') !== undefined ){
            console.log( "$('#video_img_value').val($('#video_custom_img img').attr('src')): " +
                          $('#video_img_value').val($('#video_custom_img img').attr('src')) );
                       
        } else {
            console.log( "$('#video_img_value').val($('#video_custom_img').text()): " +
                          $('#video_img_value').val($('#video_custom_img').text()) );
        }
        $('#slider_thumb').val($('#slider_thumb_preview img').attr('src'));
        return true;
    })
     

    $('#videofileupload').fileupload({
//             Uncomment the following to send cross-domain cookies:
//            xhrFields: {withCredentials: true},
        url: $("#ajaxuploadvideo").val(),
        autoUpload : true
        
    })

    $('#videofileupload')
        .bind('fileuploadchange', function (e) {
            if(is_photo){
                is_photo = false;
                return false;
            }

            if($('[name="destination"]').length >= 1 ){
                return false;
            }
        })

   

    $('#musicfileupload').fileupload({
        url: $("#ajaxuploadmusic").val(),
        autoUpload : true
        
    })
    $('#musicfileupload').bind('fileuploadchange', function (e) {
        if(is_photo){
            is_photo = false;
            return false;
        }
        if($('[name="destination"]').length >= 1 ){
            return false;
        }
    })
     
    $('#some-file-input-field'), function toggle_visibility(id) { 
       var e = document.getElementById(id); 
     
       if(e != null) { if(e.style.display == 'none') 
        { e.style.display = 'block'; } 
      else 
        { e.style.display = 'none'; } } }

 
     
});
