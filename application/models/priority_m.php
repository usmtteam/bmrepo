<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Priority_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

    


      // ** GET PRIORITY ** //
    
     function get_priority(){

     $sql = "SELECT *
                FROM priority";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }


   
     // ** ADD A PRIORITY ** //
   
 
     function add_priority($post){


     $this->load->database();
     $content_id= $this->input->post('content_id');
     $priority_number= $this->input->post('priority_number');
     $start_date =$this->input->post('start_date');
     $end_date= $this->input->post('end_date');
    
          $post = array (
            'content_id' => $content_id, 
            'priority_number' => $priority_number,
            'start_date' => $start_date,
            'end_date' => $end_date,
          );


     $insert_query = "INSERT IGNORE INTO priority (content_id, priority_number, start_date, end_date) 
                     VALUES('$content_id', '$priority_number', '$start_date', '$end_date')";


                   
     $this->db->query($insert_query); 
    


     }



     // ** UPDATE A PRIORITY ** // 


     function update_priority ($id,$data){

     $this->db->where('id', $id);
     $result = $this->db->update('priority', $data);
      return $result;

     }


     // ** GET THE ID OF A PRIORITY ** // 

     function get_priority_id($data){
     $this->db->select('*');
     $this->db->from('priority');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
     
     }



     // ** DELETE A PRIORITY ** //

    function confirm_deletion($data){
     $this->db->select('*');
     $this->db->from('priority');
     $this->db->where('id',$data);
     $query = $this->db->get();
     $result=$query->result();
     return $result;

     }




   







   
}