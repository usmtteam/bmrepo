enriqueApp.directive('brightcoveplayer', function() {
	return {
		link: function($scope, $element, $attrs) {
			var player,
		    APIModules,
		    playerData = { "playerID" :  $scope.BRIGHTCOVE_PLAYER_ID,
		                   "playerKey" : $scope.BRIGHTCOVE_READ_API_KEY,
		                   "width" : "568",
		                   "height" : "320",
		                   "videoID" : $scope.post.brightcove_media_id,
		                   "autoStart": $scope.auto_play},
		    playerTemplate = "<div style=\"display:none\"></div><object id=\"myExperience\" class=\"BrightcoveExperience\"><param name=\"bgcolor\" value=\"#FFFFFF\" /><param name=\"width\" value=\"{{width}}\" /><param name=\"height\" value=\"{{height}}\" /><param name=\"playerID\" value=\"{{playerID}}\" /><param name=\"playerKey\" value=\"{{playerKey}}\" /><param name=\"isVid\" value=\"true\" /><param name=\"isUI\" value=\"true\" /><param name=\"dynamicStreaming\" value=\"true\" /><param name=\"@videoPlayer\" value=\"{{videoID}}\"; /><param name=\"includeAPI\" value=\"true\" /><param name=\"templateLoadHandler\" value=\"onTemplateLoad\" /><param name=\"autoStart\" value=\"{{autoStart}}\" /><param name=\"templateReadyHandler\" value=\"onTemplateReady\" /><param name=\"showNoContentMessage\" value=\"true\" /></object>";
		 
		    var addPlayer = function(){
		      var playerHTML,
		      template;
		      // dynamically build player code using handlebars
		      template = Handlebars.compile(playerTemplate);
		      playerHTML = template(playerData);
		      // inject the player code into the DOM
		      $element.html(playerHTML);
		      // instantiate the player

		      brightcove.createExperiences();
		    }
		 
		    var onTemplateLoad = function(experienceID){
		      player = brightcove.api.getExperience(experienceID);
		      APIModules = brightcove.api.modules.APIModules;
		    };
		 
		    var onTemplateReady = function(evt){
		      videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
		      videoPlayer.play();
		    };
		    
		    addPlayer();
		}
	};
});