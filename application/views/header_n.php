<!DOCTYPE html><head>
	<meta charset="utf-8" />
	<title><?php echo "Breaking Movement"; ?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="<?php echo ASSETS_DIR; ?>/bootstrap/new-css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo ASSETS_DIR; ?>/css/updated/metro.css" rel="stylesheet" />
	<link href="<?php echo ASSETS_DIR; ?>/bootstrap/new-css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo ASSETS_DIR; ?>/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
	<?php

        $is_oflo =$this->data['is_oflo'];
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url,'bmcms') !== false) { ?>
            <title><?php echo "Breaking Movement"; ?></title>
            <link href="<?php echo ASSETS_DIR; ?>/css/updated/ifoflo.css" rel="stylesheet" />
        <?php } else { ?>
            <title><?php echo "Breaking Movement"; ?></title>
            <link href="<?php echo ASSETS_DIR; ?>/css/updated/style.css" rel="stylesheet" />
        <?php }

    ?>
	<link href="<?php echo ASSETS_DIR; ?>/css/updated/style_responsive.css" rel="stylesheet" />
	<link href="<?php echo ASSETS_DIR; ?>/css/updated/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_DIR; ?>/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_DIR; ?>/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_DIR; ?>/cleditor/jquery.cleditor.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_DIR; ?>/bootstrap-datepicker/css/datepicker.css" />
	<link href="<?php echo ASSETS_DIR; ?>/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	 <link rel="shortcut icon" href="<?php echo ASSETS_DIR; ?>/img/favicon.png" />
    <link rel="stylesheet" href="<?php echo ASSETS_DIR; ?>/jquery-file-upload/css/jquery.fileupload-ui.css">
    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    
<noscript>
        <link rel="stylesheet" href="<?php echo ASSETS_DIR; ?>/jquery-file-upload/css/jquery.fileupload-ui-noscript.css">
    </noscript>
    <!-- BEGIN JAVASCRIPTS -->
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <script src="<?php echo ASSETS_DIR; ?>/js/jquery-1.8.3.min.js"></script>    
    <script src="<?php echo ASSETS_DIR; ?>/bootstrap-fileupload/bootstrap-fileupload.js"></script>    
    
    <!--[if lt IE 9]>
    <script src="<?php echo ASSETS_DIR; ?>/js/excanvas.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/js/respond.js"></script>    
    <![endif]-->    
    <script src="<?php echo ASSETS_DIR; ?>/breakpoints/breakpoints.js"></script>        
    <script src="<?php echo ASSETS_DIR; ?>/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="<?php echo ASSETS_DIR; ?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/js/jquery.blockui.js"></script>    
    <script src="<?php echo ASSETS_DIR; ?>/js/jquery.cookie.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/js/nicEdit.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>    
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="<?php echo ASSETS_DIR; ?>/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>    
    <script src="<?php echo ASSETS_DIR; ?>/flot/jquery.flot.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/flot/jquery.flot.stack.js"></script>    
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/js/jquery.pulsate.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/jquery-contentchange-event/jquery.contentchange-event.js"></script>
    <script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <!--<script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/cleditor/jquery.cleditor.js"></script>-->    
    <script src="<?php echo ASSETS_DIR; ?>/js/app.js"></script>  

     <script type="text/javascript" src="<?php echo ASSETS_DIR; ?>/js/jquery-ui-1.10.4.custom.min.js"></script>
     
    <script>
        jQuery(document).ready(function() {        
            App.setPage("<?php echo $curPage = $this->router->fetch_class().'/'.$this->router->fetch_method(); ?>");  // set current page
            App.init(); // init the rest of plugins and elements
        });
    </script>


	<!--jcrop-->
	<link rel="stylesheet" href="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.css">
    <script src="<?php echo ASSETS_DIR; ?>/jcrop/jquery.Jcrop.min.js"></script>
    <script src="<?php echo ASSETS_DIR; ?>/js/social_sharing.js"></script>   
    <script language="javascript">
    	SocialShareEngine.site_url = "<?php echo rtrim(site_url(''), '/index.php'); ?>";
    	SocialShareEngine.asset_url = "<?php echo ASSETS_DIR; ?>";
    	SocialShareEngine.cid = "<?php echo $this->main_m->chanel['id']; ?>";
    	SocialShareEngine.bitly_login = "<?php echo BITLY_LOGIN; ?>";
    	SocialShareEngine.bitly_apikey = "<?php echo BITLY_APIKEY;?>";
    </script>
    
    
</head>
<input type='hidden' value="<?php echo site_url('ajax'); ?>" id="ajax_control_url">
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php 
     $designinfo = $this->ion_auth->getColors();
     $channel =  $this->main_m->get_chanel(24);
     
?>
<style >
    .backcolor, .backcolor .backcolor{
        background-color: <?php echo '#000000' ?> !important;
    }
    
    span.title, ul.sub li a{
        color : <?php echo '#636769' ?>;
    }
    span.title, ul.sub li:active a{
        color : <?php echo '#fff' ?>;
    }
    a:hover span.title{
       color:#ccc !important;
    }
	span.title:hover,.page-sidebar > ul > li > ul.sub > li > a:hover{
		color:#ccc !important;
	}
</style>
<body class="fixed-top backcolor">
	<div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo FACEBOOK_ID; ?>',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->

                <?php $is_oflo =$this->data['is_oflo']; 
                $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
				<a class="brand" href="/content/feed/">
				<img src="<?php if(isset($designinfo['header_image']) && $designinfo['header_image']){
                    $head_img = str_replace('/var/www/html/starsite/cms/assets/ufile/', '', $designinfo['header_image']);

                    echo UPLOAD_URL.$head_img;
                    }else{ 
                        if(strpos($url,'oflo') !== false){
                            echo ASSETS_DIR."/img/oflo.png";
                        }else{
                            echo ASSETS_DIR."/img/sc-logo.png";
                        }
                    }
                    ?>" alt="logo" style="height:45px; background:none" />
				</a><span class="channel_name" style="font-size:22px; line-height:62px; margin-left:-10px; font-weight:300;">>&nbsp;&nbsp;<?=$this->data['chanel']['name'] ?></span>
				<!-- END LOGO -->
<?php
	 if ($this->ion_auth->logged_in()):
?>
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				        <a style="float:right;margin:0 10px; color:#adafb0; font-size:16px; line-height:62px; font-weight:300" class="right" href="<?php echo site_url("auth/logout"); ?>">Log Out</a>
						<span style="float:right;margin:0 10px; color:#adafb0; font-size:16px; line-height:62px; font-weight:300" class="username right">
							<?php 
								echo "Hi, ".$this->session->userdata('username'); 
							?>
						</span>
                        <? if($this->session->userdata('username')=="administrator"){ ?>
                        <span style="float:right;margin:0 10px; color:#adafb0; font-size:16px; line-height:62px; font-weight:300" class="username right">
                            <?php 
                                echo "<a href='/'>Admin Home</a>"; 
                            ?>
                        </span>
                        <? } ?>
						<?php
	endif; 
?>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">

	