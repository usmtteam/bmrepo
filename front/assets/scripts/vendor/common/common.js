window.CommonLib = {
	formatDate: function(time_stamp) {
		var d = new Date(time_stamp * 1000);
		var week_names = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var month_names = ['Jan', 'Feb', 'Mar',' Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		console.log(time_stamp);
		console.log(d);
		return week_names[d.getDay()] + ', ' + month_names[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear() + ' ' + (d.getHours() % 12) + ':' + d.getMinutes() + ':' + d.getSeconds() + ' ' + (d.getHours() >= 12 ? 'PM' : 'AM');
	},
	get_time_ago: function (time_stamp) {
		
		var time = Math.round(+new Date()/1000) - time_stamp;
		if(time <= 0) time = 1;
		
		var divisor = 0;
		var time_unit = '';
		if (time >= 60 * 60 * 24 * 365.242199)	{
			divisor = 60 * 60 * 24 * 365.242199;
			time_unit = 'year';
		}
		else if (time >= 60 * 60 * 24 * 30.4368499)	{
			divisor = 60 * 60 * 24 * 30.4368499;
			time_unit = 'month';
		}
		else if (time >= 60 * 60 * 24 * 7){
			divisor = 60 * 60 * 24 * 7;
			time_unit = 'week';
		}
		else if (time >= 60 * 60 * 24)  {
			divisor = 60 * 60 * 24;
			time_unit = 'day';
		}
		else if (time >= 60 * 60)  {
			divisor = 60 * 60;
			time_unit = 'hour';
		}
		else  {
			divisor = 60;
			time_unit = 'minute';
		}
		var time_units = Math.floor(time / divisor);
	 
		if (time_units === 0)  {
			return time_unit == 'hour' ? 'less than an hour ago' : 'less than a ' + time_unit + ' ago';
		}
		else if (time_units === 1)	{
			return (time_unit == 'hour' ? 'an ' : 'a ') + time_unit + ' ago';
		}
		else  {
			return time_units + ' ' + time_unit + 's ago';
		}
	},
	
	show_loading_bubble: function() {
		var page =$('#page');
		
		this.hide_loading_bubble();
		
		var bubble = $('<div id="loading_bubble_container"><div class="overlay"></div><div id="loading_bubble" class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></div>');
		
		page.append(bubble);
		bubble = bubble.find('#loading_bubble');
		var wi = $(window);
		bubble.css('left', wi.width()/2 - bubble.width()/2);
		bubble.css('top', wi.height()/2 - bubble.height()/2);
	},
	
	hide_loading_bubble: function() {
		$('#loading_bubble_container').remove();
	},
	
	show_waiting_circle: function(target, center) {
		this.hide_waiting_circle();
		center = typeof center == 'undefined' ? false : center;
		var circle = $('<div id="waitingCircleFrame"><div id="floatingCirclesG"><div class="f_circleG" id="frotateG_01"></div><div class="f_circleG" id="frotateG_02"></div><div class="f_circleG" id="frotateG_03"></div><div class="f_circleG" id="frotateG_04"></div><div class="f_circleG" id="frotateG_05"></div><div class="f_circleG" id="frotateG_06"></div><div class="f_circleG" id="frotateG_07"></div><div class="f_circleG" id="frotateG_08"></div></div></div>');
		
		circle.insertAfter(target);
		var left = target.position().left + target.width() - circle.width();
		var top = target.position().top + 4;
		if(center) {
			left = target.position().left + target.outerWidth(true)/2 - circle.outerWidth(true)/2;
			top = target.position().top + target.outerHeight(true) /2 - circle.outerHeight(true) / 2;
		}
		circle.css('left', left);
		circle.css('top', top);
	},
	
	hide_waiting_circle: function() {
		$('#waitingCircleFrame').remove();
	},
	serializeData: function ( data ) {

        // If this is not an object, defer to native stringification.
        if ( ! angular.isObject( data ) ) {

            return( ( data == null ) ? "" : data.toString() );

        }

        var buffer = [];

        // Serialize each key in the object.
        for ( var name in data ) {

            if ( ! data.hasOwnProperty( name ) ) {

                continue;

            }

            var value = data[ name ];

            buffer.push(
                encodeURIComponent( name ) +
                "=" +
                encodeURIComponent( ( value == null ) ? "" : value )
            );

        }

        // Serialize the buffer and clean it up for transportation.
        var source = buffer
            .join( "&" )
            .replace( /%20/g, "+" )
        ;

        return( source );

    }
};