enriqueApp.controller('musicPlayerController', ['$scope', '$rootScope', '$http', '$sce', function($scope, $rootScope, $http, $sce) {
	
	$scope.musics = [];
	$scope.current_status = $rootScope.music_play ? 'Play' : 'Pause';
	
	$scope.initialize = function() {
		CommonLib.show_loading_bubble();
    	var url = $rootScope.get_api_url() + '&option=get_musics';
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        	if(data.status == true) {
        		$scope.musics = data.musics;
        	}	
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
	};
	
	$scope.onMusicPrev_click = function() {
		if($rootScope.music_id <= 0) return;
		$rootScope.music_id--;

		$rootScope.music_file = $sce.trustAsResourceUrl($scope.musics[$rootScope.music_id].destination);
		
		console.log($rootScope.music_file.toString());
	};
	$scope.onMusicNext_click = function() {
		if($rootScope.music_id >= $scope.musics.length - 1) return;
		$rootScope.music_id++;
		$rootScope.music_file = $sce.trustAsResourceUrl($scope.musics[$rootScope.music_id].destination);
	};
	$scope.onMusicPlay_click = function() {
		$rootScope.music_play = !$rootScope.music_play;
		$rootScope.music_file = $sce.trustAsResourceUrl($scope.musics[$rootScope.music_id].destination);
	};
	$scope.onMusicItem_click = function(index) {
		$rootScope.music_id = index;
		$rootScope.music_file = $sce.trustAsResourceUrl($scope.musics[$rootScope.music_id].destination);
	};
	
	$scope.initialize();
}]);