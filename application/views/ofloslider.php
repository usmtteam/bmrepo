<!-- BEGIN PAGE -->
<div class="page-content">
     <div class="container-fluid" id="dashboard">
    
    
		
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Breaking Movement Slider Posts
                    
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Thumbnail</th>
                            <th>Caption</th>
                            <th>Type</th>
                        
                            <th>Status</th>
                            <th>Date</th>
                            <th>Edit</th> 
                            <th>Disable</th>
                                                    
                        </tr>
                    </thead>
                    <tbody>     
                    <?php 
                    foreach($sliderfeeds as $feed): 
                    $ids = explode("_", $feed['id']);
                    $post_controller_name = $ids[0];   
                    ?> 
                        <tr>
                            <td class="feed-thumbnail-td grid">
                            <?php if($feed['image_path']!=""){ 
							
                                echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="'.$feed['image_path'].'"></a><br/>';
                            } else {  echo '<a href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">    <img class="feed-thumbnail-large pull-left" src="/assets/img/click-to-view.jpg"></a><br/>'; } ?>
                            </td>
                            <td class="span2 grid"><div style='word-wrap:break-word; width: 200px'><?php if($feed['caption']){echo substr($feed['caption'], 0, 200);}else{ echo "Processing - Content will be available shortly"; } ?>
                            	<?php if($feed['is_sticky']) :?>
                            	<span style="color:red;font-weight:bold;"><sup>STICKY</sup></span>
                            	<?php endif;?>
                            </div></td>
                            <td class="grid"><?=$feed['post_type'];?></td>
                     

                       
                            <?php $permission = ($feed['is_publish']==2 ? "Draft" : ($feed['is_publish']==1 ? "Published" : "Transcoding")); ?>
                            <td class="grid"><?php echo $permission ?></td>
                             <td class="grid"><?php echo $feed['date']; ?></td>
                            
                            <td class="grid">
                            <?php
                                $ids = explode("_", $feed['id']);
                                $post_controller_name = $ids[0];
                                
                                echo 
                                '<a class="btn" href="'.site_url('content/feed/'.$post_controller_name.'_edit/'.$feed['id']).'">'.
                                'Edit'.
                                '</a>';
                                

                                      
                            ?>
                        </td>
                 
                        <td class="grid">
                        
                             <?php

                                $ids = explode("_", $feed['id']);
                                $post_controller_name = $ids[0];
                                
                              
                                echo 
                                '<a class="btn" href="'.site_url('content/ofloslider_del/'.$feed['id']).'">'.
                                'Disable'.
                                '</a>';
                                
                            ?>
                                      
                      


                        </td>

                          
                          
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
		
    </div>
</div>
<script>
    function update_slider(id) {
        if(confirm("Do you want to disable this slider?")) {
            document.location.href = "<?php echo site_url('content/ofloslider_del'); ?>/" + id;
        }
    }    
</script>