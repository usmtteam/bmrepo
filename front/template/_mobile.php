<?php

require_once("../include/lib/livefyre-php-utils/src/Livefyre.php");
require_once("../include/lib/php-jwt/src/JWT.php");
require_once("../include/lib/requests/library/Requests.php");

use Livefyre\Livefyre;
use Livefyre\Exceptions\ApiException;
Requests::register_autoloader();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?
	$siteurl = "http://".$l_xChannel->slug.".on.starsite.com";
	$end_of_url = str_replace($siteurl,"",$l_sCurrentAbsoluteURLNoVars);

	$l_sMetaTitle = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
	$l_sMetaDescription = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
	?>
	<script type="text/javascript">

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?= $l_xChannel->google_analytics_key ?>','<?= $l_xChannel->domain ?>');
		ga('require', 'displayfeatures');
		ga('send', 'pageview');
		ga('set', 'page', '<?= $end_of_url ?>');
	</script>
	<?

	$l_bIsMobile = false;
	if($l_nDeviceType == DEVICE_TYPE_MOBILE){
		$l_bIsMobile = true;
		$l_aZoneUnits = array('ZONE_1'=>11,'ZONE_2'=>12,'ZONE_3'=>13);
		$l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=14';
	}else{
		$l_aZoneUnits = array('ZONE_1'=>16,'ZONE_2'=>1,'ZONE_3'=>2);
		$l_sAdServerURL = 'http://scads.starsite.com/www/delivery/fc.php?script=apVideo:vast2&zoneid=4';

	}

	$network = Livefyre::getNetwork("starclub.fyre.co", "9GlDgawpgrNRGprDbwi82+RYm5s=");


	//update a Network's name and key
	$network->getData()->setName("starclub.fyre.co");
	$network->getData()->setKey("9GlDgawpgrNRGprDbwi82+RYm5s=");

	//set SSL off
	$network->setSsl(false);

	//get system and user tokens
	$systemToken = $network->buildLivefyreToken();





	//make sure a system token is still valid
	$isValid = $network->validateLivefyreToken($systemToken);

	//get the $network URN
	$networkUrn = $network->getUrn();

	$site = $network->getSite("379531", "MX0QS7E0rdA7iRP4lF2KlqIMTsg=");

	$newsite = $site->getData()->getId();

	$siteUrn = $site->getUrn();



	$commentsCollection = $site->buildCommentsCollection($l_sMetaTitle, "beta-starsite-".$l_nAssetId, $end_of_url);




	$articleId = $commentsCollection->getData();

	$commentCollectionMetaToken = $commentsCollection->buildCollectionMetaToken();

	$commentCollectionCheckSum = $commentsCollection->buildChecksum();

	$commentsCollection->createOrUpdate();


	try {
		$test = $commentsCollection->getCollectionContent();



	} catch (ApiException $e) {
		print("LOG: can't retrieve content of a collection that has not been created!");


	}
	// $l_sAdServerURL = 'http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=229&LR_SCHEMA=vast2';
	/*$l_sMetaTitle = preg_replace('/^"|"$/',"",$l_xAssetResult['description']);
    $l_sMetaTitle = preg_replace('/[\r\n\t]+/',"",$l_sMetaTitle);
    $l_sMetaTitle = preg_replace("/[^!'#a-zA-Z0-9_.-\s]/","",$l_sMetaTitle);
    $l_sMetaDescription = $l_sMetaTitle; */


	?>
	<meta charset="utf-8" />
	<title><?= $l_xChannel->site_title ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

	<!-- Load videojs player dependent stylesheets. -->
	<link href="//vjs.zencdn.net/5.4.6/video-js.css" rel="stylesheet">
	<!--    <link href="//vjs.zencdn.net/4.12.15/video-js.css" rel="stylesheet">-->
	<!--    <link rel="stylesheet" href="--><?//=STATIC_FILES_HOST?><!--/video-js-5.4.6/video-js.css" />-->
	<!--    <link rel="stylesheet" href="--><?//=STATIC_FILES_HOST?><!--/video-js-4.12.15/video-js.css" />-->
	<!--    <link href="--><?//=STATIC_FILES_HOST?><!--/videojs-ga-master-4.6.1/libs/video-js-4.6.1/video-js.css" rel="stylesheet" type="text/css">-->

	<link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/videojs-contrib-ads-master-3.1.2/src/videojs.ads.css" />
	<!--    <link rel="stylesheet" href="--><?//=STATIC_FILES_HOST?><!--/videojs-contrib-ads-1.1.0/src/videojs.ads.css" />-->
	<link rel="stylesheet" href="<?=STATIC_FILES_HOST?>/videojs-ima-master-5.3/src/videojs.ima.css" />
	<!-- If you'd like to support IE8 -->
	<script src="http://vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
	<script src="http://vjs.zencdn.net/5.4.6/video.js"></script>
	<!--    <script src="http://vjs.zencdn.net/4.12.15/video.js"></script>-->
	<!--    <script src="--><?//=STATIC_FILES_HOST?><!--/video-js-5.4.6/video.js"></script>-->
	<!--    <script src="--><?//=STATIC_FILES_HOST?><!--/video-js-4.12.15/video.js"></script>-->

	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<!--    <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-ga-master-4.6.1/libs/video-js-4.6.1/video.dev.js"></script>-->
	<script src="<?=STATIC_FILES_HOST?>/videojs-ga-master-4.6.1/dist/videojs.ga.min.js"></script>

	<style type="text/css">
		@font-face {
			font-family: 'starsitefont';
			src:url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.eot?e5v9gc');
			src:url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.eot?#iefixe5v9gc') format('embedded-opentype'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.woff2?e5v9gc') format('woff2'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.ttf?e5v9gc') format('truetype'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.woff?e5v9gc') format('woff'),
			url('<?=STATIC_FILES_HOST ?>/fonts/starsitefont.svg?e5v9gc#starsitefont') format('svg');
			font-weight: normal;
			font-style: normal;
		}
		.fyre .fyre-help{ display:none;}
		.ssfont{font-family:'starsitefont';}
		*{ margin:0; padding:0; outline: none }
		body{ font-family:'Avenir','Arial'; font-size: 12px; color:#222; background:#fff; font-weight: normal;}
		.container{ margin: 0 auto; text-align: center; padding:0px; max-width:1140px}
		.header{ display: block; position: relative; border:0px solid #E2E2E2; margin:0px 0 0 10px; min-height:100px; font-size: 20px}
		.header .nav{ float:left; width:99.8%; border: 1px solid #e2e2e2; padding-bottom: 7px; margin-top: 5px; box-shadow:0 0 5px #E2E2E2; background-color: #fff}
		.header .nav a{ text-decoration: none; color:#222; margin-top:10px; }
		.header .nav .as{ background:#fff; padding:10px 30px;float:left; margin-left:0px; border-radius:0px; border-right:1px solid #E2E2E2; box-shadow:0 0 0px #E2E2E2; font-weight:normal;}
		.header .nav .as1{ background:#fff; padding:10px 30px;float:left; margin-left:0px; border-radius:0px; border-right:1px solid #E2E2E2; box-shadow:0 0 0px #E2E2E2; font-weight:bold;}
		.header .nav .as2{ background:#fff; padding:10px 30px;float:left; margin-left:0px; border-radius:0px; border-right:1px solid #E2E2E2; box-shadow:0 0 0px #E2E2E2; font-weight:normal;}

		.header .nav .as3{ background:#fff; padding:10px 30px;float:left; margin-left:0px; border-radius:0px; border-right:1px solid #E2E2E2; box-shadow:0 0 0px #E2E2E2; font-weight:bold;}

		.header .nav .as4{ background:#fff; padding:10px 30px;float:left; margin-left:0px; border-radius:0px; border-right:1px solid #e2e2e2; box-shadow:0 0 0px #E2E2E2; font-weight:normal;}
		.header-over{ position: relative; top: 0; width:100%}
		.header .login{ float:right}
		.logo{ font-size:32px; margin:10px 0px 10px 20px; float: left; padding-top: 5px; padding-right: 20px;; border-right:1px solid #E2E2E2}
		h1{ font-size:60px; margin:20px 0; min-height:20px; text-transform: uppercase}
		h3{ font-size: 20px; font-weight: normal; }
		.content{ display: block; width:100%; max-width:1140px; margin:0 auto }
		.footer{ display: block; margin:10px 0 100px 0; padding-top: 20px; border-top: 1px solid #e2e2e2}

		.flowplayer-holder{ background:#000; margin:10px; box-shadow:0 0 5px #E2E2E2; }
		.flowplayer-holder { overflow: hidden; position: relative; } /* Fix for static videojs-ima */

		.clr{ clear: both; height:0; overflow: hidden;}

		.col1{ float:left; width:70%; <?= (!$l_bIsMobile) ? 'max-width:800px;' : ''; ?>}
		.col2{ float:left; width:30%; <?= (!$l_bIsMobile) ? 'min-width:300px;' : ''; ?>}

		.latest {border-bottom: 1px solid #e2e2e2; padding-bottom: 5px; margin: 10px 10px 10px 10px; text-align: left; font-size: 14px}
		.assets-list{ max-height:400px; border:1px solid #E2E2E2; margin:10px 0 10px 0; box-shadow:0 0 5px #E2E2E2; padding: 10px; overflow: scroll; overflow-x:hidden; }
		.assets-list ul{ margin: 0 auto; list-style: none;}

		.assets-list ul li a, .assets-list ul a li a, .assets-list ul a:visited, .assets-list ul a{ outline: none; display: block; background: #fff; margin-bottom:1px; text-decoration: none; color:#222; }
		.assets-list ul li p, .assets-list ul a li p{ padding:10px; margin-bottom:5px}
		.assets-list ul li p span, .assets-list ul a li span.dates{font-size:10px; color:#999; font-weight: normal}
		.assets-list ul li, .assets-list ul a { text-align:left; font-size:14px; font-weight: normal }
		.assets-list ul li .thumb, .assets-list ul a li span .vid_thumb{ float: left; max-height:30%; max-width:80px; max-height:50px; overflow:hidden; margin:10px}
		.assets-list ul li img{ }

		.page-unit-1{ margin-left: 10px; background-color: #eee; padding: 10px}
		.page-unit-2{ margin-bottom: 10px; border:1px solid #e2e2e2; box-shadow:0 0 5px #E2E2E2; margin-top: 10px; padding:10px}
		.page-unit-3{ margin-bottom: 10px; border:1px solid #e2e2e2; box-shadow:0 0 5px #E2E2E2; margin-top: 10px; padding:10px}
		.desc-area{ padding:20px; background: #fff; text-align: left; margin:10px; border:1px solid #E2E2E2; box-shadow:0 0 5px #E2E2E2}
		.desc-area span{color: #999}
		.social{ border-top:1px solid #E2E2E2; margin:15px 0; padding:15px 0 }
		.social ul{  }
		.social ul li{ display: inline-block; }
		.social ul li a{ font-size:36px; color:#222; text-decoration:none  }

		.social-links{ border-top:0px solid #E2E2E2; margin:15px 20px 0px 0px; padding:0px 0; float: right; display: none}
		.social-links ul{  }
		.social-links ul li{ display: inline-block; }
		.social-links ul li a{ font-size:36px; color:#222; text-decoration:none  }
		.view-all{border: 1px solid #e2e2e2; padding: 10px; margin: 10px; font-size: 15px}


		.flowplayer > div.fp-ui > div.adContainer > div > div > video { position:relative !important; left:0px !important;top:0px !important; float:left !important; width:100%; height:100%;}

		.video-js { color: #b20000; }
		.video-js, #ima-ad-container + div { width: 100% !important; }
		.vjs-loading-spinner { opacity: 0 }
		.vjs-default-skin.vjs-ended .vjs-play-control:before { content: ''; }

		.next_button {padding: 20px 20px; border: 1px solid #e2e2e2; border-right: 0px; margin-top: 30px; position: absolute; top: 100px; right: 0px; text-transform: uppercase; font-size: 32px; box-shadow:0 0 5px #E2E2E2;}
		a.next_button{text-decoration: none; color: #6D6D6D}
		a.next_button:hover{text-decoration: none; color: #000; background-color: #ededed}
		a.back_button:hover{text-decoration: none; color: #000; background-color: #ededed}

		a.back_button{text-decoration: none; color: #6D6D6D}
		.back_button {padding: 20px 20px; border: 1px solid #e2e2e2; border-right: 0px; margin-top: 30px; position: absolute; top: 100px; left: -1px; text-transform: uppercase; font-size: 32px; box-shadow:0 0 5px #E2E2E2;}
		#gallery_back{margin:10px; border: 1px solid #e2e2e2; position: relative; box-shadow:0 0 5px #E2E2E2}
		#image-back{width:450px;height:auto; overflow:hidden; margin:10px auto 10px auto;}
		#image-back img{margin:0 auto;  max-width:450px; vertical-align:middle; text-align:center; border:1px solid #E2E2E2; box-shadow:0 0 5px #E2E2E2}
		img.powered-by{width: 200px}
		.social-links img{width:25px}

		@media screen and (max-width: 767px) {

			*{/* border:1px solid #f00;*/}
			body{ font-size: 9px;}
			h1{ font-size:40px; }
			h3{ font-size: 16px;  line-height:18px; }
			.assets-list ul li{ font-size:10px; line-height:12px }
			.assets-list ul li p span{ font-size:9px }
			.header-over{ position: relative; top: 0; margin: 0px auto}
			.header .nav{height: 35px; padding-top: 5px; background-color: #fff; border-right: 0px; border-left: 0px}
			.header .nav a{ position: absolute;}
			.header{padding:0px; margin:0px auto; border:0px; width: 100%}
			.header .nav .as{ padding:0px; font-weight: normal; border-radius:0px; width: 49%; float: left; position: relative; border-right: 0px; font-size: 15px}
			.header .nav .as1{ padding:0px; font-weight: bold; border-radius:0px; width: 49%; float: left; position: relative; font-size: 15px}
			.header .nav .as2{ padding:0px; font-weight: normal; border-radius:0px; width: 49%; float: left; position: relative; font-size: 15px}
			.header .nav .as3{ padding:0px; font-weight: bold; border-radius:0px; width: 49%; float: left; position: relative; font-size: 15px; border-left: 1px solid #e2e2e2; border-right:0px}
			.header .nav .as4{ padding:0px; font-weight: normal; border-radius:0px; width: 49%; float: left; position: relative; font-size: 15px; border:0px}
			.desc-area span{color: #999; font-size: 12px}
			.col1{ float:left; width:74%;}
			.col2{ float:left; width:23.5%;}


			.col1{ float:left; width:100%; margin:0 auto; clear: both;}
			.col2{ float:none; width:100%; margin:0 auto; clear: both;}

			.latest {border-bottom: 1px solid #e2e2e2; padding-bottom: 5px; margin: 10px 10px 10px 10px; text-align: left; font-size: 14px}
			.assets-list{  max-height:none; overflow-y: visible; width:92.5%; padding:0px; margin:0px auto}
			.assets-list ul li{ text-align:left; font-size:15px; font-weight: normal; line-height: 125%}
			.assets-list ul li a{ margin:2px 0}
			.assets-list ul li p{ padding:10px 10px 5px 3px;}
			.header .nav .as:nth-child(2) {
				margin-top: 10px;
			}
			.header .nav .as:nth-child(3) {
				margin-top: 10px;
			}
			.logo{ font-size:22px;}

			.social{margin-bottom:0px}

			.social-links{display: none }

			.page-unit-1{ margin-left: 0px; background-color: #eee; padding: 10px 0px; overflow: hidden;}
			.page-unit-2{ margin-bottom: 10px; border:1px solid #e2e2e2; box-shadow:0 0 5px #E2E2E2; margin-top: 10px; padding:10px 0px; margin-left:0px; overflow: hidden;}
			.page-unit-3{ margin-bottom: 10px; border:1px solid #e2e2e2; box-shadow:0 0 5px #E2E2E2; margin-top: 10px; padding:10px 0px; margin-left:0px; overflow: hidden;}

			.next_button {padding: 10px 20px; border: 1px solid #e2e2e2; margin-top: 0px; position: relative; top: 20px; right: 0px; text-transform: uppercase; font-size: 17px; box-shadow:0 0 5px #E2E2E2}
			a.next_button{text-decoration: none; color: #6D6D6D}
			a.next_button:hover{text-decoration: none; color: #000; background-color: #ededed}
			a.back_button:hover{text-decoration: none; color: #000; background-color: #ededed}

			a.back_button{text-decoration: none; color: #6D6D6D}
			.back_button {padding: 10px 20px; border: 1px solid #e2e2e2; margin-top: 0px; position: relative; top: 20px; left: 0px; text-transform: uppercase; font-size: 17px; box-shadow:0 0 5px #E2E2E2; margin-right: 30px}
			#gallery_back{margin:10px; border: 1px solid #e2e2e2; position: relative; box-shadow:0 0 5px #E2E2E2; padding-bottom: 50px}
			#image-back{width:98%;height:auto; overflow:hidden; margin:10px auto 10px auto;}
			#image-back img{margin:0 auto;  width:92.5%; vertical-align:middle; text-align:center; border:1px solid #E2E2E2; box-shadow:0 0 5px #E2E2E2}
			img.powered-by{width: 150px}



		}
		.inactive{ pointer-events: none; color:#eee !important;}


	</style>

	<style type="text/css">
		.endscreen {
			top: 10%;
			left: 10%;
			position: absolute;
			width: 80%;
			/*height: 70%;*/
			height: 80%;
			-webkit-border-radius: 20px;
			-moz-border-radius: 20px;
			border-radius: 20px;
			font-size: 14px;
			background-color: #777;
			text-align: center;

			/* do not show endscreen by default - a simpler, non-animated alternative would be: */
			display: none;
			/*opacity: 0;*/
			/*filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);*/
			-webkit-transition: opacity .5s;
			-moz-transition: opacity .5s;
			transition: opacity .5s;

			/* overlay player endscreen when video is finished */
			z-index: 2;

			/* show endscreen - a simpler alternative would be:
            * display: block; */
			opacity: 1;
			filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
			overflow-wrap: break-word;

		}
		.endscreen h3 {
			font-size: 160%;
			font-weight: bold;
			color: #ccc;
			margin-top: 10%;
		}
		.endscreen h4 {
			font-size: 120%;
			color: #eee;
		}
		.endscreen .button {
			background-color: #00a6bc;
			padding: 20px 30px;
			margin: 15px 0;
			-webkit-border-radius: 60px;
			-moz-border-radius: 60px;
			border-radius: 60px;
			font-size: 110%;
			color: #eee;
			cursor: pointer;
			display: inline-block;
			-webkit-transition: background-color .1s;
			-moz-transition: background-color .1s;
			transition: background-color .1s;

		}
		.endscreen .button:hover {
			background-color: #00afc6;
		}
		.endscreen .button:active {
			background-color: #009db2;
		}
		.endscreen .vjs-toggle {
			display: block;
			color: #eee;
			cursor: pointer;
		}
		.endscreen .vjs-toggle:hover {
			color: #444;
		}

		.endscreen b {
			color: #eee;
		}

		.endscreen textarea {
			width: 80%;
		}
	</style>

	<? if($l_xAssetResult['cid'] != 86){ ?>
		<script type='text/javascript' src='//scads.starsite.com/www/delivery/spcjs.php?id=1'></script>
	<? } ?>

	<?
	//if($l_bIsVideo){ ?>

	<meta property="og:title" content="<?= $l_sMetaTitle ?>" />
	<meta property="og:description" content="<?= $l_sMetaDescription ?>"/>

	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
	<meta property="og:image" content="<?= $l_sImageUrl ?>" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="1200" />


	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@Starsitenetwork" />
	<meta name="twitter:title" content="Starsite" />
	<meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
	<meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
	<meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />

	<script>  // Create a clone of the menu, right next to original.
		$(document).ready(function(){
			$('.nav').addClass('original').clone().insertAfter('.nav').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

			scrollIntervalID = setInterval(stickIt, 10);


			function stickIt() {

				var orgElementPos = $('.original').offset();
				orgElementTop = orgElementPos.top;

				if ($(window).scrollTop() >= (orgElementTop)) {
					// scrolled past the original position; now only show the cloned, sticky element.

					// Cloned element should always have same left position and width as original element.
					orgElement = $('.original');
					coordsOrgElement = orgElement.offset();
					leftOrgElement = coordsOrgElement.left;
					widthOrgElement = orgElement.css('width');
					$('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
					$('.original').css('visibility','hidden');
				} else {
					// not scrolled past the menu; only show the original menu.
					$('.cloned').hide();
					$('.original').css('visibility','visible');
				}
			}
		});
	</script>


	<?
	/*

      if($p_sPageType=="inline"){ ?>

            <meta property="og:type" content="movie" />
            <meta property="og:video:height" content="270"/>
            <meta property="og:video:width" content="480"/>
            <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
            <meta property="og:video" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
            <meta property="og:video:secure_url" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/<?= $video_destination; ?>.mp4">
            <meta property="og:image" content="<?= $l_sImageUrl ?>"/>
            <meta property="og:video:type" content="application/x-shockwave-flash">
        <? } else if($p_sPageType == "landingpage"){ ?>

            <meta property="og:type" content="website" />
            <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
            <meta property="og:image" content="<?= $l_sImageUrl ?>" />
            <meta property="og:image:type" content="image/png" />
            <meta property="og:image:width" content="1200" />


    <? } else if($p_sPageType=="twitter_player"){ ?>

           <meta property="twitter:card" content="player">
           <meta property="twitter:title" content="<?= $l_xChannel->site_title ?>">
           <meta property="twitter:description" content="<?= $l_sMetaDescription ?>">
           <meta property="twitter:image" content="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->url; ?>/assets/ufile/watermark/<?= $l_xAssetResult['image_path']; ?>">
           <meta property="twitter:image:width" content="480">
           <meta property="twitter:image:height" content="270">
           <meta property="twitter:site" content="@starsitenetwork">
           <meta property="twitter:player:width" content="480">
           <meta property="twitter:player:height" content="270">
           <meta property="twitter:player" content="<?= $l_sCurrentAbsoluteSecureURL; ?>">

    <? }else if($p_sPageType=="twitter"){ ?>
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@Starsitenetwork" />
      <meta name="twitter:title" content="Starsite" />
      <meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
      <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
      <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


    <? } else { ?>
          <meta property="og:type" content="website" />
            <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
            <meta property="og:image" content="<?= $l_sImageUrl ?>" />
            <meta property="og:image:type" content="image/png" />
            <meta property="og:image:width" content="1200" />

    <? }
}else{

    if($p_sPageType=="twitter_player"){ ?>

    <? //<meta name="twitter:card" content="photo" /> ?>
     <meta name="twitter:card" content="player" />
      <meta name="twitter:site" content="@Starsitenetwork" />
      <meta name="twitter:title" content="Starsite" />
      <meta name="twitter:description" content="<?= $l_sMetaDescription; ?>" />
      <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
      <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURL ?>" />

    <? }else if($p_sPageType=="twitter"){ ?>
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@Starsitenetwork" />
      <meta name="twitter:title" content="Starsite" />
      <meta name="twitter:description" content="<?= $l_sMetaDescription ?>" />
      <meta name="twitter:image" content="<?= $l_sImageUrl ?>" />
      <meta name="twitter:url" content="<?= $l_sCurrentAbsoluteURLNoVars ?>" />


    <? }else{} ?>

    <meta property="og:title" content="<?= $l_sMetaDescription ?>" />
    <meta property="og:description" content="<?= $l_sMetaDescription ?>"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?= $l_sCurrentAbsoluteURL ?>" />
    <meta property="og:image" content="<?= $l_sImageUrl ?>" />
    <meta property="og:image:type" content="image/png" />
<? }

    */


	$l_sBackgroundImage = strlen($l_xChannel->backgroundImage) > 2 ? '<img src="'.CDN_FRONT.'/'.$l_xChannel->url."/images/bg-sn.jpg?v=".RELEASE_VERSION.'" width="100%" />' : '';
	// $l_sBackgroundImage = '';

	?>
	<script src="https://starclub.ep.livefyre.com/media/c3RhcmNsdWIuZXAubGl2ZWZ5cmUuY29t/javascripts/customprofiles.js"></script>
	<script src="//cdn.livefyre.com/Livefyre.js"></script>
</head>
<body>
<div class="container">
	<?

	/*  LEADER BOARD  */

	?>
	<div class="page-unit-1">
		<? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- GlobalUnit-320x50 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:320px;height:50px"
				 data-ad-client="ca-pub-7963321383486508"
				 data-ad-slot="6866262471"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		<? }else{ ?>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- Large Leaderboard -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:970px;height:90px"
				 data-ad-client="ca-pub-7963321383486508"
				 data-ad-slot="6322799279"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		<? } ?>
	</div>

	<div class="header"><?= $l_sBackgroundImage ?>
		<h1<?= (strlen($l_sBackgroundImage) > 5) ? ' style="display:none" ' : '' ?>><?= $l_xChannel->site_header_title ?></h1>
		<div class="header-over">
			<div class="nav">
				<? /*<a href="http://www.starclubltd.com" class="logo ssfont" target="_blank">S</a>*/?>
				<a href="/content/video/recent/" class="<? if($l_bIsVideo) { ?>as1<? }else{ ?>as <? } ?>">VIDEOS</a>
				<a href="/content/photo/recent/" class="<? if($l_bIsVideo) { ?>as4<? }else{ ?>as3 <? } ?>">PHOTOS</a>
				<div class="social-links">
					<a href="http://starsite.com" target="_blank"> <img src="/assets/images/star-site.png"></a>
				</div>

			</div>


			<div class="login">
				<!-- <a href="#">LOG IN</a> -->
			</div>

		</div>
		<div class="clr"></div>
	</div>
	<div class="clr"></div>
	<div class="content">

		<?

		/*  Video  */

		?>
		<?
		if (strpos($l_xAssetResult['destination'], '.mov') !== FALSE){
			$l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mov/",".jpg",$l_xAssetResult['destination']));
		}else{
			$l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.preg_replace("/video/","photo",preg_replace("/\.mp4/",".jpg",$l_xAssetResult['destination']));
		}
		$l_sImage = 'http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.$l_xAssetResult['image_path'];


		?>
		<div class="col1">
			<? if($l_bIsVideo){ ?>

				<div class="flowplayer-holder">

					<? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>

					<video id="my-video" class="video-js vjs-big-play-centered" controls preload="auto" width="632" height="353.92"
						   poster="<?= $l_sImage ?>" data-setup="{}">

					<? } else { ?>

					<video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="778" height="438"
						   poster="<?= $l_sImage ?>" data-setup="{}">

					<? } ?>

						<? if (strpos($l_xAssetResult['destination'], '.mov') !== FALSE){ ?>
<!--                            <source type="application/x-mpegurl"  src="http://d3hl6zmfu9utfu.cloudfront.net/--><?//= $l_xChannel->slug ?><!--/assets/ufile/--><?//= preg_replace("/\.mov/","/master.m3u8",$l_xAssetResult['destination']) ?><!--">-->
							<source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mov/","-480.webm",$l_xAssetResult['destination']) ?>">
							<source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mov/","-480.mp4",$l_xAssetResult['destination']) ?>">
							<source type="video/flash" src="mp4:http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mov/","-480.mp4",$l_xAssetResult['destination']) ?>">
						<? }else{ ?>
<!--                            <source type="application/x-mpegurl"  src="http://d3hl6zmfu9utfu.cloudfront.net/--><?//= $l_xChannel->slug ?><!--/assets/ufile/--><?//= preg_replace("/\.mp4/","/master.m3u8",$l_xAssetResult['destination']) ?><!--">-->
							<source type='video/webm' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mp4/","-480.webm",$l_xAssetResult['destination']) ?>">
							<source type='video/mp4' src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mp4/","-480.mp4",$l_xAssetResult['destination']) ?>">
							<source type="video/flash"  src="mp4:http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?= preg_replace("/\.mp4/","-480.mp4",$l_xAssetResult['destination']) ?>">
						<? } ?>

                    <iframe width="778" height="438" src="http://d3hl6zmfu9utfu.cloudfront.net/<?=$l_xChannel->slug?>/assets/ufile/<?=preg_replace("/\.mov/","/-480.mp4",$l_xAssetResult['destination'])?>"></iframe>

						<p class="vjs-no-js">
							To view this video please enable JavaScript, and consider upgrading to a web browser that
							<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
						</p>
					</video>
					<div class="endscreen">
						<span>
							<a class="button"
							   href="http://<?=$_SERVER['HTTP_HOST']?>/content/video/<?=$l_xPreviousVideoResult['id']?>">Next video</a>
							<a class="vjs-toggle">Play it again!</a>
							<BR><b>Embed code</b>
							<BR><textarea id="embed"><?=htmlspecialchars('<iframe width="778" height="438" src="http://d3hl6zmfu9utfu.cloudfront.net/'.$l_xChannel->slug.'/assets/ufile/'.preg_replace("/\.(mov|mp4)/","-480.mp4",$l_xAssetResult['destination']).'"></iframe>'); ?>
							</textarea>
						</span>
					</div>

					<script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
					<script src="<?=STATIC_FILES_HOST?>/videojs-contrib-ads-master-3.1.2/src/videojs.ads.js"></script>
					<!--                    <script src="--><?//=STATIC_FILES_HOST?><!--/videojs-contrib-ads-1.1.0/src/videojs.ads.js"></script>-->
					<script src="<?=STATIC_FILES_HOST?>/videojs-ima-master-5.3/src/videojs.ima.js"></script>

				</div>
			<? } else if(!$l_bIsVideo){ ?>

				<div id="gallery_back">
					<div id="image-back"><img id="main_img" src="http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/<?php echo $l_xAssetResult['destination'] ?>" /></div>
					<a class="back_button <?= ($l_xPreviousPhotoResult) ? 'full' : 'inactive' ?>" href="/content/photo/<?= $l_xPreviousPhotoResult['id'] ?>/"><</a><a class="next_button <?= ($l_xNextPhotoResult) ? 'full' : 'inactive' ?>" href="/content/photo/<?= $l_xNextPhotoResult['id'] ?>/">></a></div>
			<? } else{} ?>
			<script>

				var player = videojs('my-video');

				//                player.ga({
				////                    'percentsPlayedInterval': 25,
				////                    'eventsToTrack': ['loaded', 'percentsPlayed', 'start', 'end', 'error'],
				//                    'debug': true
				//                });

				player.ready( function(){
					console.log('++++++++++++++++ready');
					var dur = Math.round(this.duration());
					var pause = Math.round(dur * .25);
					var pausehalf = Math.round(dur * .5);
					var pausethreefourths = Math.round(dur * .75);

					ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Impression', dur);
					setTimeout(function(){
						ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View', dur);
					},pause);
					setTimeout(function(){
						ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Half', dur);
					},pausehalf);
					setTimeout(function(){
						ga('send', 'event', '<?=$l_nAssetId?>', 'Video-View-Three-Fourths', dur);
					},pausethreefourths);

					this.on('play', function(){
						console.log('++++++++++++++++play');

					}).on('ended', function(){
						console.log('++++++++++++++++ended');
						var dur = Math.round(this.duration());
						ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Finished', dur);

						$('.endscreen').show();

					}).on('error', function(){
						console.log('++++++++++++++++error');
						var dur = Math.round(this.duration());
						ga('send', 'event', '<?=$l_nAssetId?>', 'Video-Error', dur);
					});

				});

				var options = {
					id: 'my-video',
//                    debug: true,
//                    adTagUrl: 'http://adsite.starsite.com/simple/'
					adTagUrl: '<?=$l_sAdServerURL?>'
				};

				player.ima(options);

				// On mobile devices, you must call initializeAdDisplayContainer as the result
				// of a user action (e.g. button click). If you do not make this call, the SDK
				// will make it for you, but not as the result of a user action. For more info
				// see our examples, all of which are set up to work on mobile devices.
				player.ima.initializeAdDisplayContainer();
				player.ima.requestAds();
				//                var adsMan = player.ima.getAdsManager;
				//                    .resize(77, 43, 'normal');

				player.play();

				//                console.log($('video').width());

				//                $('#ima-ad-container.div').ready(function(){
				//                    console.log('#ima-ad-container.div first ready');
				//                    console.log($("#ima-ad-container div").first()[0].style.width);
				////                    console.log($("#ima-ad-container div").first()[0].style.width = 100 + "%");
				//                    console.log($("#ima-ad-container div").first()[0].style.width = ($('.flowplayer-holder').width() + 'px'));
				//                    console.log($("#ima-ad-container div").first()[0].style.width);
				//
				//                });
			</script>

			<div class="desc-area">
				<div class="desc">
					<h3><?= $l_xAssetResult['description'] ?></h3>
					<br />
					<span><? echo Utils::time_passed($l_xAssetResult['time_stamp']); ?></span>
				</div>

				<div class="social">
					<ul>
						<li><a href="http://www.facebook.com/share.php?u=<?= $l_sCanonicalLink ?>&title=<?php echo $_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont fb">f</a></li>
						<li><a href="http://twitter.com/home?status=<?= $l_sCanonicalLink ?>?page_type=twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tw">t</a></li>
						<li><a href="http://tumblr.com/widgets/share/tool?canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont tb">u</a></li>
						<li><a href="https://plus.google.com/share?url=canonicalUrl=<?= $l_sCanonicalLink ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;" class="ssfont gp">g</a></li>
					</ul>
				</div>

				<div id="livefyre-app-beta-starsite-<?= $l_nAssetId ?>"></div>



				<!-- Scripts -->
				<script>
					Livefyre.require(["fyre.conv#3","auth","lfep-auth-delegate#0"],

						function (Conv, auth, LFEPAuthDelegate) {
							var authDelegate = new LFEPAuthDelegate({
								engageOpts: {
									app: "starclub.auth.fyre.co"
								}
							});



							auth.delegate(authDelegate);
						});
				</script>
				<script>
					(function() {
						Livefyre.require(['fyre.conv#3'], function(Conv) {
							new Conv({
								network: 'starclub.fyre.co'

							}, [{
								app: 'main',
								siteId: "<?php echo $newsite ?>",
								articleId: 'beta-starsite-<?= $l_nAssetId ?>',
								el: 'livefyre-app-beta-starsite-<?= $l_nAssetId ?>',

							}], function (widget) {
								// Initialize or Auth



							});

						});
					}());
				</script>
			</div>




		</div>


		<div class="col2">
			<div class="page-unit-2">
				<? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- GlobalUnit-320x50 -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:320px;height:50px"
						 data-ad-client="ca-pub-7963321383486508"
						 data-ad-slot="6866262471"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				<? }else{ ?>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- Medium Rectangle -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:300px;height:250px"
						 data-ad-client="ca-pub-7963321383486508"
						 data-ad-slot="8232028077"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				<? } ?>
			</div>

			<div class="assets-list">
				<div class="latest">LATEST <? if($l_bIsVideo) { ?>VIDEOS<? }else{ ?>PHOTOS<? } ?></div>
				<ul>
					<?
					if($l_bIsVideo) { ?>
						<? AppController::getInstance()->outputVideoListItemsFlowPlayerMobile($l_aFPVideos,$l_xChannel); ?><?
					}else{

						foreach($l_aAllFoundAssets as $photo_list_result){
							?>
							<a href="/content/photo/<?= $photo_list_result['id'] ?>/">
								<li><span class="thumb_video_list"><div class="vid_thumb" style="background:url(http://d3hl6zmfu9utfu.cloudfront.net/<?= $l_xChannel->slug ?>/assets/ufile/watermark/<?= $photo_list_result['destination']; ?>); height:40px; width:40px; float:left; background-size:40px 40px;"></div></span>
									<p><?= substr($photo_list_result['description'], 0, 45) . '...'; ?><br><span class="dates"><? echo Utils::time_passed($photo_list_result['time_stamp']); ?></span></p>

									<?php /*<span><?= Utils::get_time_ago($photo->publishedDate); ?></span></p> */ ?>
									<div class="clr"></div>
								</li>
							</a>
							<?

						}
					} ?>
				</ul>

			</div>



			<? if($l_bIsMobile){ ?>

		</div>
		<div class="clr"></div>
		<div>
			<? } ?>


			<div class="page-unit-3">
				<? if($l_nDeviceType == DEVICE_TYPE_MOBILE){ ?>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- Mobile Large 320x100 -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:320px;height:100px"
						 data-ad-client="ca-pub-7963321383486508"
						 data-ad-slot="2284614472"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				<? }else{ ?>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- Large Skyscraper -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:300px;height:600px"
						 data-ad-client="ca-pub-7963321383486508"
						 data-ad-slot="8908182473"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				<? } ?>
			</div>


		</div>
		<div class="clr"></div>
	</div>
	<div class="clr"></div>
	<div class="footer">
		<p><a target="_blank" href="http://starsite.com"><img class="powered-by" src="/assets/images/powered-by-starsite.png"></a></p>
	</div>
</div>


<script>
	$('.flowplayer-holder .video-js, .vjs-toggle').click(function(){
		$('.endscreen').hide();
	});

	$('.vjs-toggle').click(function(){
		var player = videojs('my-video');
		player.play();
	});
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<!-- Quantcast Tag -->
<script type="text/javascript">
	var _qevents = _qevents || [];

	(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
	})();

	_qevents.push({
		qacct:"p-F-MpJUp5wCJHc"
	});
</script>

<noscript>
	<div style="display:none;">
		<img src="//pixel.quantserve.com/pixel/p-F-MpJUp5wCJHc.gif" border="0" height="1" width="1" alt="Quantcast"/>
	</div>
</noscript>
<!-- End Quantcast tag -->

</body>
</html>