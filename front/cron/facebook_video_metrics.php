<?

	require(__DIR__.'/../config/settings.php');	

	$l_xReportsConnection = mysqli_connect($l_aDatabase_Reports['host'],$l_aDatabase_Reports['user'],$l_aDatabase_Reports['pass'],$l_aDatabase_Reports['db']);
	$l_xDevTeamConnection = mysqli_connect($l_aDatabase_DevTeam['host'],$l_aDatabase_DevTeam['user'],$l_aDatabase_DevTeam['pass'],$l_aDatabase_DevTeam['db']);
	
	

	$l_sSql = 'SELECT * FROM enr_channel_oauth WHERE social_property_type_id = 1 '; // AND channel_id = 8 //
	
	$l_xResult = mysqli_query($l_xDevTeamConnection,$l_sSql);

	$l_aResultsMain = array();
	while ($l_xObj = mysqli_fetch_array($l_xResult,MYSQLI_ASSOC)) {
		$l_aResultsMain[] = $l_xObj;
	} 
	

	foreach($l_aResultsMain as $l_xResultRow){

		$l_nSiteId = intval($l_xResultRow['channel_id']);

		$l_nPageId = $l_xResultRow['page_id'];
		$l_sPageName = $l_xResultRow['page_name'];
		$l_sAccessToken = $l_xResultRow['social_token'];


		$l_sSql = 'SELECT * FROM enr_facebook_post WHERE site_id = '.$l_nSiteId.' AND type = "video" ORDER BY created_time DESC LIMIT 15';
		// echo $l_sSql;
		$l_xResult = mysqli_query($l_xDevTeamConnection,$l_sSql);
		$l_sResults = array();
		while ($l_xObj = mysqli_fetch_array($l_xResult,MYSQLI_ASSOC)) {
    		$l_sResults[] = $l_xObj;
		}

		// print_r($l_sResults);
		// exit;
		
		foreach($l_sResults as $l_xRow){
			
			$l_nPostId = $l_xRow['post_id'];
			$l_sFBAPIEndpoint = 'https://graph.facebook.com/v2.0/'.$l_nPostId.'/insights/?access_token='.$l_sAccessToken;
			$l_sData = file_get_contents($l_sFBAPIEndpoint);
			
			

			$l_aJSON = json_decode($l_sData); 
			$l_FBData = $l_aJSON->data;
			$l_FBPaging = $l_aJSON->paging;

			//Map data to Keys
			$l_aMappedData = array();
			foreach($l_FBData as $l_xSubRow){
				$l_aMappedData[$l_xSubRow->name.'::'.$l_xSubRow->period] = $l_xSubRow;
			}

			// print_R($l_aJSON);
			// print_R($l_aMappedData);
			// exit;

			$l_aColumnsToRecord = array();
			$l_aColumnsToRecord['post_video_avg_time_watched'] = 'post_video_avg_time_watched::lifetime';
			$l_aColumnsToRecord['post_video_length'] = 'post_video_length::lifetime';
			$l_aColumnsToRecord['post_video_views_organic'] = 'post_video_views_organic::lifetime';
			$l_aColumnsToRecord['post_video_views_organic_unique'] = 'post_video_views_organic_unique::lifetime';
		
			$l_sRetentionGraph = implode(',',$l_aMappedData['post_video_retention_graph::lifetime']->values[0]->value);
			
			$l_sSql = 'SELECT id FROM enr_fbpost_video_metrics WHERE site_id = '.$l_nSiteId.' AND post_id = "'.$l_nPostId.'" LIMIT 1';
			
			
			$l_xResult = mysqli_query($l_xDevTeamConnection,$l_sSql);
			$l_xResult = mysqli_fetch_assoc($l_xResult);

			$l_nCountBlock = 0;	
			if($l_xResult){
				$l_sSql = 'UPDATE enr_fbpost_video_metrics SET ';
				$l_aUpdates = array();
				foreach($l_aColumnsToRecord as $k=>$v){
					$l_sValue = $l_aMappedData[$v]->values[$l_nCountBlock]->value;
					$l_aUpdates[] = $k.' = "'.$l_sValue.'"';
				}
				$l_aUpdates[] = 'post_video_retention_graph = "'.$l_sRetentionGraph.'"';
				
				$l_sSql .= join(',',$l_aUpdates);
				$l_sSql .= ' WHERE site_id = '.$l_nSiteId.' AND post_id = "'.$l_nPostId.'"';
			}else{
				$l_sSql = 'INSERT INTO enr_fbpost_video_metrics(site_id,post_id,post_video_retention_graph,';
				$l_aCols = array_keys($l_aColumnsToRecord);
				$l_sSql .= join(',',$l_aCols).') VALUES ('.$l_nSiteId.',"'.$l_nPostId.'","'.$l_sRetentionGraph.'",';
				$l_aVals = array();
				foreach($l_aColumnsToRecord as $k=>$v){
					$l_sValue = $l_aMappedData[$v]->values[$l_nCountBlock]->value;
					$l_aVals[] = '"'.$l_sValue.'"';
				}
				$l_sSql .= join(',',$l_aVals).')';
			}
			// echo $l_sSql;
			// exit;
			mysqli_query($l_xDevTeamConnection,$l_sSql);
		}
 	}