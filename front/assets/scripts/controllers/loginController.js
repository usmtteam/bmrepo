enriqueApp.controller('loginController', ['$scope', '$rootScope', '$http', '$modal', function($scope, $rootScope, $http, $modal) {
	$scope.error = '';
	

	$scope.onSignInWithFacebook_click = function() {
		if(!$('#checkbox_accept_terms').is(':checked')) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Accept terms and conditions',
				description: 'Please accept terms and conditions to sign in.',
				onOk : function() {
				}
			});
			return;
		}
		
		
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				do_login_by_facebook();
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						do_login_by_facebook();
					}
					else {
						
					}
				}, {scope: 'email, manage_pages'});
			}
		});
	};
	
	$scope.onPrivacyAndPolicy_click = function() {
		$rootScope.dialogViewPost = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'view_privacy_dialog',
		    resolve: {
		    	url: function() { return "privacy"; }
		    },
			templateUrl: '/views/view_help.html',
			controller: 'viewHelpController'
		});
	};
	$scope.onTermsOfService_click = function() {
		$rootScope.dialogViewPost = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'view_privacy_dialog',
		    resolve: {
		    	url: function() { return "terms"; }
		    },
			templateUrl: '/views/view_help.html',
			controller: 'viewHelpController'
		});
	};
	
	function do_login_by_facebook() {
		FB.api('/me', {fields: 'id,first_name,last_name,email' }, function(response) {
			var facebook_page_id = response.id; 
			var username = response.first_name + ' ' + response.last_name;
			var first_name = response.first_name;
			var last_name = response.last_name;
			var email = response.email;
			
			
			
			$http({method: 'GET', url: $rootScope.get_api_url() + '&option=register&username=' + username + '&firstname=' + first_name + '&lastname=' + last_name+ '&email=' + email + '&facebook_page_id=' + facebook_page_id + '&facebook=1'}).
	        success(function(data, status, headers, config) {
	        	if(data.status == true) {
	        		$rootScope.user_token = data.token;
	        		$rootScope.current_user = data.info;
	        		
	        		$rootScope.dialogLogin.close();
	        		
	        		$rootScope.saveCookie();
	        	}
	        	else {
	        		$scope.error = data.message;
	        	}
	        }).
	        error(function(data, status, headers, config) {
	        	$scope.error = 'Please try again.';
	        });
		});
	}
}]);