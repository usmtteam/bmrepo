<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Facebook Class.
 */

//require(__DIR__.'/../config/settings.php');
//require_once(FACEBOOK_OAUTH_CLASS);

define('FACEBOOK_APP_ID','265098000358000');
define('FACEBOOK_APP_SECRET','d88198b3a3d59056fdb186a6170c0c80');

class Facebook_Star{

//    // We'll use a constructor, as you can't directly call a function
//    // from a property definition.

    function get_Facebook_Accounts(){

        // Create our Application instance
        $facebook = new Facebook(array('appId'  => FACEBOOK_APP_ID,'secret' => FACEBOOK_APP_SECRET));

        // Facebook logout to release user access_token
//        $logoutUrl = $facebook->getLogoutUrl();
//        echo $logoutUrl;
//        exit;

        $user = $facebook->getUser(); // Get User ID

        // We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.

        if ($user) {

//            echo "<br><br>\$user: ".$user;
//
//            $l_sReturnedCode = $_GET['code'];
//            echo "<br><br>\$l_sReturnedCode: ".$l_sReturnedCode;

            // Get long-lived access token
            $facebook->setExtendedAccessToken(); // Extend token this one time
            $access_token = $facebook->getAccessToken();

//            echo "<br><br>\$access_token: ";
//            var_dump($access_token);

            $me = $facebook->api("/me");
            $me['access_token'] = $access_token;

//            echo "<br><br>\$me: ";
//            var_dump($me);
//            exit;

            $user_data = array();
            $user_data[] = $me;

            try {
                $permissions = $facebook->api("/me/permissions");
//                echo "<br><br>\$permissions: ";
//                var_dump($permissions);

                $publish_actions_permission = FALSE;
                $manage_pages_permission    = FALSE;

                foreach($permissions['data'] as $permission){

//                    echo "<br><br>\$permission: ";
//                    var_dump($permission);

                    if(in_array('publish_actions', $permission)) {

                        $publish_actions_permission = TRUE;
//                        echo "<br><br>\$publish_actions_permission ";


                    } elseif (in_array('manage_pages', $permission)){

                        $manage_pages_permission   = TRUE;
//                        echo "<br><br>\$manage_pages_permission ";

                    }

                }

                // Check if we don't have one of the permissions
                if ($publish_actions_permission == FALSE || $manage_pages_permission == FALSE){

//                    echo "<br><br>FALSE";

                    // We don't have one of the permissions
                    // Alert the admin or ask for the permission!
                    // header( "Location: " . $facebook->getLoginUrl(array("scope" => "publish_actions, manage_pages")) );
                    $loginUrl = $facebook->getLoginUrl(array("scope" => "publish_actions, manage_pages"));
//                    echo "<BR>".$loginUrl;
//                    exit;

                    ?>
                    Redirecting you to Facebook ...
                    <script type="text/javascript"> document.location.href ='<?= $loginUrl  ?>'; </script>
                    <?
//                    exit;
                }

                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me/accounts');
//                echo "<br>\$user_profile: ";
//                var_dump($user_profile);

                // Build array of user profile and group pages
                foreach ($user_profile['data'] as $data){
                    $user_data[] = $data;
                }

//                exit;

                return $user_data;

            } catch (FacebookApiException $e) {

//                echo "<br><br>ERROR<br>";
//                print_r($e);
                error_log($e);
                $user = null;

                return $show_errors = $e->getMessage();
            }

        } elseif (empty($_GET['code'])) {

            // Login url will be needed
            $params = array('scope' => 'public_profile,email,manage_pages,publish_actions,read_insights,publish_pages');
            $loginUrl = $facebook->getLoginUrl($params);
//            echo "<BR>".$loginUrl;
//            exit;

            ?>
            Redirecting you to Facebook ...
            <script type="text/javascript"> document.location.href ='<?= $loginUrl  ?>'; </script>
            <?
//            exit;

        } elseif (!empty($_GET['code'])){

            return $show_errors = "Cannot connect to Facebook at this time. Please try again later.";
        }

    }

}

?>