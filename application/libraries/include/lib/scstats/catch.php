<?
  
  require(__DIR__.'/../../../config/init.php');
  $l_xChannel = AppController::getInstance()->getChannel();

?>

function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname+"="+cvalue+"; "+expires;
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


window.CommonStats = {
    get_piwik_stats: function() {
    },
    get_quantcast_stats: function() {
    }
}

var _paq = _paq || [];
_paq.push(["setDocumentTitle", document.location.href]);
_paq.push(["setCookieDomain", "<?= $l_xChannel->domain_cookie ?>"]);
_paq.push(["setCustomVariable", 1, "userid", getCookie("ss_userid"), "visit"]);
_paq.push(["trackPageView"]);
_paq.push(["enableLinkTracking"]);

(function() {
    var u = (("https:" == document.location.protocol) ? "https" : "http") + "://reports.starsite.com/";
    _paq.push(["setTrackerUrl", u + "starstats.php"]);
    _paq.push(["setSiteId", "<?= $l_xChannel->starstats_site_id ?>"]);
    var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
    g.type = "text/javascript";
    g.defer = true;
    g.async = true;
    g.src = u + "piwik.js";
    s.parentNode.insertBefore(g, s);
})();

// End Starstats Code
var _qevents = _qevents || [];

(function() {
    var elem = document.createElement('script');
    elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
    elem.async = true;
    elem.type = "text/javascript";
    var scpt = document.getElementsByTagName('script')[0];
    scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
    qacct: "<?= $l_xChannel->quantserve_key ?>"
});