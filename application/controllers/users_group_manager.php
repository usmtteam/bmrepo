<?php
class Users_Group_Manager extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('users_group_manager_m');
        $this->load->model('main_m');
	
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

	}
	
  function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }

 function index(){

      $this->data['users_group_manager'] = $this->users_group_manager_m->get_users_group_manager();
      
        $this->load->view('users_group_manager', $this->data);


  }


  public function users_group_manager_edit(){
        
        $id = $this->uri->segment(3);
        $this->data['users_group_manager'] = $this->users_group_manager_m->get_users_group_manager_id($id);
        
        $this->data['id'] = $this->data['users_group_manager'][0]['id'];
        $this->data['hub_id'] = $this->data['users_group_manager'][0]['hub_id'];
        $this->data['group_name'] = $this->data['users_group_manager'][0]['group_name'];

       
         if ($this->input->post('submit')){  
         $this->update_users_group_manager_id($id);
          }

         $this->load->view('users_group_manager_edit', $this->data);
   

    }


     public function update_users_group_manager_id($id) {
     
   
 
      $hub_id = $this->input->post('hub_id');
      $group_name =$this->input->post('group_name');
    

      

      $data = array (
            'hub_id' => $hub_id,
             'group_name' => $group_name,
          
            
            );


            if  ($this->users_group_manager_m->update_users_group_manager($id, $data))
            {
                redirect(site_url('users_group_manager/index'));
            }
        
            return;
    
      
    }


     public function users_group_manager_add($post){

      

          $this->form_validation->set_rules('name', 'name', 'required|max_length[512]|xss_clean');


           if ($this->form_validation->run()==false){}
            else
           {
           if ($this->form_validation->run() == true) 

          { 

        
             if($this->input->post('name',false) == true)

                {
                
             
                {
            $this->data['users_group_manager'] = $this->users_group_manager_m->add_users_group_manager();


             redirect(site_url('users_group_manager/index'));


                }
                }
           
              
                {
                  
                    if(empty($name)){
            show_error("Enter a name!");
              return;
                
                                     }
                }

                }
           
            
          
        }
                  

    
        
        $this->load->view('users_group_manager_add', $this->data);
    }



      
  


     public function confirm_deletion(){
     
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("users_groups");
        redirect(site_url('users_group_manager/'));
     }

    
     

     

   


 

 
}