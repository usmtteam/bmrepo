<?php

require(__DIR__.'/settings.php');


$l_bIsIE10 = preg_match("/MSIE (10|9|8|7)/",$_SERVER['HTTP_USER_AGENT']);



if($_SERVER['SC_CHANNEL']){
    $l_sSlug = strtolower($_SERVER['SC_CHANNEL']);
}else{
    preg_match("/(.+\.)?(.+)\.(oflo)/",$_SERVER['HTTP_HOST'],$l_aMatches);
    $l_sSlug = strtolower($l_aMatches[2]);
}

if($l_sSlug){
    //==================================================================
    //              GET CACHE BASED ON CHANNEL
    //==================================================================
    $l_aCachedContent = Memcaching::getInstance()->getKey(Memcaching::makeKey($l_sSlug));
    if(!$l_aCachedContent){
        $l_xChannelManager = new ChannelManager();
        $l_xChannel = $l_xChannelManager->setChannelInMemcacheBySlugAndReturn($l_sSlug);
        if(!$l_xChannel) Utils::show404();
    }else{
        $l_xChannel = $l_aCachedContent['value'];
    }
    AppController::getInstance()->setChannel($l_xChannel);
}

//======================================================================
//                  CACHE BRIGHTCOVE LOOK UP
//======================================================================



$l_sMemcacheKey = Memcaching::makeKey($l_sSlug.'fp-videos');
$l_aCachedContent = Memcaching::getInstance()->getKey($l_sMemcacheKey);

if(!$l_aCachedContent){

    $l_xSql = AppController::getInstance()->getConnection();
    $l_sSql = 'SELECT * FROM enr_video WHERE cid = "'.$l_xChannel->cid.'" AND is_publish = 1 ORDER BY id DESC';
    $l_aFPVideos =  $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
    Memcaching::getInstance()->store($l_sMemcacheKey,$l_aFPVideos,MEMCACHE_DURATION_FP_VIDEOS);
}else{
    $l_aFPVideos = $l_aCachedContent['value'];
}


?>