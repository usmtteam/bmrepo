enriqueApp.controller('pollQuizController', function($scope, $rootScope, $http, post, star) {
    $scope.post = post;
    $scope.feeds = [];
    $scope.questions = [];
    $scope.answers = [];
    $scope.star = star;
    $scope.result_page = false;
    $scope.answer_result = {};
    $scope.questionIndex = 0;
    
    $scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	var url = $rootScope.get_api_url() + '&option=get_poll_quiz_content&post_type=' + post.post_type + '&content_id=' + post.content_id;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.feeds = data.feed;
        		$scope.questions = data.questions;
        	}
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    };
    $scope.closeDialog = function() {
    	$rootScope.dialogViewPost.close();
    };
    $scope.submitAnswer = function() {
    	var answer_radios = $('input[id="radio_answer"]:checked');
    	
    	if(answer_radios.length == 0) {
    		$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Poll and Quizz',
				description: 'Please select your answer.',
				onOk : function() {
				}
			});
    	}
    	else {
	    	var answer = $('input[id="radio_answer"]:checked').val();
	    	
	    	$scope.answers.push(answer);
	    	
	    	if($scope.post.post_type == 'poll') {
	    		$scope.postAnswer($scope.questions[$scope.questionIndex].question_id, answer);
	    		$scope.result_page = true;
	    	}
	    	else {
	    		$scope.nextQuestion();
	    	}
    	}
    };
    $scope.nextQuestion = function() {
    	$scope.result_page = false;
    	if($scope.questionIndex >= $scope.questions.length - 1) {
    		if($scope.post.post_type == 'poll') {
    			$scope.closeDialog();
    		}
    		else {
    			
    			$scope.answer_result.total_answers_count = $scope.answers.length;
    			$scope.answer_result.correct_count = 0;
    			for(var i = 0; i < $scope.answers.length; i++) {
    				for(var j = 0; j < $scope.questions[i].answer.length; j++) {
    					if($scope.questions[i].answer[j].answer_id == $scope.answers[i] && $scope.questions[i].answer[j].is_correct == "1") {
    						$scope.answer_result.correct_count++;
    						break;
    					}
    				}
    			}
    			
    			$scope.post.answered = true;
    			
    			$scope.postQuizAnswer($scope.answer_result.correct_count, 
    					$scope.answer_result.total_answers_count,
    					$scope.post.content_id,
    					function() {
		    				$scope.answer_result.description = '';
		        			
		        			$http({method: 'GET', url: $scope.feeds.end_description}).
		        	        success(function(data, status, headers, config) {
		        	        	
		    	        		$scope.answer_result.description = data;
		    	        		
		    	        		$scope.result_page = true;
		        	        }).
		        	        error(function(data, status, headers, config) {
		        	        	$scope.result_page = true;
		        	        });
		        			
		        			$scope.quiz_result = {
		    	    			post_type : 'quiz',
		    	    			content_id : $scope.post.content_id,
		    	    			unlock:true,
		    	    			caption: '',
		    	    			image_path: $scope.post.image_path
		    	    		}
		            		$scope.quiz_result.caption = $scope.post.caption + ' - ' + $scope.questions[$scope.questionIndex].text + '\n' + 'Correctly answered ' + $scope.answer_result.correct_count + '/' + $scope.answer_result.total_answers_count;
    			});
    		}
    	}
    	else {
    		$scope.questionIndex ++;
    	}
    };
    $scope.postAnswer = function(question_id, answer_id) {
		CommonLib.show_waiting_circle($('#button_submit'), 1);
    	var url = $rootScope.get_api_url() + '&option=poll_answer&question_id=' + question_id + '&answer_id=' + answer_id;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.answer_result = data.ratings;
        		
        		$scope.poll_result = {
	    			post_type : 'poll',
	    			content_id : $scope.post.content_id,
	    			unlock:true,
	    			caption: '',
	    			image_path: $rootScope.SERVER_URL + '/assets/ufile/' + $scope.questions[$scope.questionIndex].image_path
	    		};
        		$scope.poll_result.caption = $scope.questions[$scope.questionIndex].description + ' - ' + $scope.questions[$scope.questionIndex].text;
        		for(var i = 0; i<$scope.answer_result.length; i++) {
        			$scope.poll_result.caption += '\n' + $scope.answer_result[i].name + ' : ' + $scope.answer_result[i].percent + '%';
        		}
        	}
        	CommonLib.hide_waiting_circle();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_waiting_circle();
        });
    }
    $scope.postQuizAnswer = function(correct_count, question_count, content_id, callback) {
		CommonLib.show_waiting_circle($('#button_submit'), 1);
    	var url = $rootScope.get_api_url() + '&option=quiz_answer&correct_count=' + correct_count + '&question_count=' + question_count + '&content_id=' + content_id;
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	CommonLib.hide_waiting_circle();
        	callback();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_waiting_circle();
        	callback();
        });
    };
    
    $scope.loadData();
});