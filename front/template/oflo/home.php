<?php

//    require(__DIR__.'/../../config/init.php');
require(__DIR__.'/../../config/settings.php');
require(__DIR__.'/init.php');
    require(__DIR__.'/db_home.php');
include '_head.php';
include '_menu.php';

// var_dump($l_xOfloHomepageVideos); exit;
?>

      <div class="fixed-width">

                <div class="carousel-holder big-slider">

                    <div class="col-md-12" style="padding:0">
                        <div id="big-slider" class="carousel slide carousel-fade box-menu" data-ride="carousel">
                            <div class="carousel-inner">

                              <?php
                              $homepageSliderVideo_count = count($l_xOfloHomepageSlider);

                              for ($i = 0; $i < $homepageSliderVideo_count ; $i++) : ?>
                                  <div class="item <?php if ($i == 0) { echo ' active'; } ?>">  
                                      <a href="http://<?=$l_xOfloHomepageSlider[$i]['url']?>.<?=OFLO_LANDING?>/ocontent/<?php echo $l_xOfloHomepageSlider[$i]['content_type'].'/'.$l_xOfloHomepageSlider[$i]['id'] ?>/">
                                          <div class="home-slider-wrap"><img src="<?php echo CDN_FRONT.'/'.$l_xOfloHomepageSlider[$i]['url'].'/assets/ufile/'.$l_xOfloHomepageSlider[$i]['slider_thumb'];?>" class="carousel-img1 backup_picture">
                                          </div>
                                      </a>
                                  </div>
                              <?php endfor; ?>

                            </div>

                            <a class="left carousel-control" href="#big-slider" data-slide="prev" style="background-image: none; color:#e7e7e7;">
                                <span class="glyphicon glyphicon-chevron-left glyphicon1"></span>
                            </a>
                            <a class="right carousel-control" href="#big-slider" data-slide="next" style="background-image: none; color:#e7e7e7;">
                                <span class="glyphicon glyphicon-chevron-right glyphicon1"></span>
                            </a>
                        </div>
                    </div>
               </div>

               <div class="clr"></div>
<? /*
        <div class="col-md-12 hidden-xs" style="padding:0">
 
  
          <div class="well box-menu" style="border: 0px;">

              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <div class="channelAll">
                  <h3>CREATORS</h3>
                    <a href="/creators"><strong style="font-size: 14px;">View All ></strong></a>
                  
                  </div>
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                      <div class="item active">
                          <div class="row row-carousel home-second-carousel">

                              <?php $channel_count = count($l_xChannels);
                              for ($i = 0; $i < $channel_count && $i <= 5; $i++) : ?>

                              <div class="channel_image">
                                      <a href="<?='http://'.$l_xChannels[$i]['url'].'.'.OFLO_LANDING.'/'?>">
                                          <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                          <strong><?=$l_xChannels[$i]['name']?></strong>
                                      </a>
                                  </div>
                              <?php endfor; ?>

                          </div>
                          <!--/row-->
                      </div>
                      <!--/item active-->

                      <?php
                        for ($i; $i < $channel_count && $i >= 5; $i++) : ?>

                            <!--/item-->
                            <div class="item">
                                <div class="row row-carousel home-second-carousel">
                                    <div class="channel_image">
                                        <a href="<?='http://'.$l_xChannels[$i]['url'].".".OFLO_LANDING?>/">
                                            <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                            <strong><?=$l_xChannels[$i]['name']?></strong>
                                        </a>
                                    </div>

                                    <?php while($i < $channel_count - 1 && $i >= 5 && (($i + 1) % 6 != 0)): ?>

                                    <div class="channel_image">
                                                <?php $i++ ?>
                                        <a href="<?='http://'.$l_xChannels[$i]['url'].".".OFLO_LANDING?>/">
                                            <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                                <strong><?=$l_xChannels[$i]['name']?></strong>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>

                                </div>
                                <!--/row-->
                            </div>
                            <!--/item-->

                        <?php endfor; ?>

                  </div>

                  <!--/carousel-inner-->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="margin-top: -30px; width:0%; color:#e7e7e7; background-image: none; outline: none;" >
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next" style="margin-top: -30px; width:0%; color:#e7e7e7; background-image: none; outline: none;">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
              </div>
              
              <!--/myCarousel-->
          </div>
          <!--/well-->
  </div>
<? /*
  <div class="col-md-12 hidden-sm hidden-md hidden-lg" style="padding:0"  >
            <div class="well" style=" background-color: white; border: 0px;">
                <div id="myCarousela" class="carousel slide">
             <!-- Carousel items -->
              <div class="channelAll">
                  <h3>CREATORS</h3>
                    <a href="/creators"><strong style="font-size: 14px;">View All ></strong></a>
                  
                  </div>
            


                    <div class="carousel-inner">
                        <div class="item active home-second-carousel">
                            <div class="row row-carousel">

                                <?php $channel_count = count($l_xChannels);
                                for ($i = 0; $i < $channel_count && $i <= 1; $i++) : ?>
                                
                                    <div class="channel_image_mobile">
                                      <a href="<?='http://'.$l_xChannels[$i]['url'].'.'.OFLO_LANDING.'/'?>">
                                          <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                          <strong><?=$l_xChannels[$i]['name']?></strong>
                                      </a>
                                  </div>
                                <?php endfor; ?>

                            </div>
                            <!--/row-->
                        </div>
                        <!--/item active-->

                        <?php
                          for ($i; $i < $channel_count && $i > 1; $i++) : ?>

                              <!--/item-->
                              <div class="item home-second-carousel">
                                  <div class="row row-carousel">
                                      <div class="channel_image_mobile">
                                      <a href="<?='http://'.$l_xChannels[$i]['url'].'.'.OFLO_LANDING.'/'?>">
                                          <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                          <strong><?=$l_xChannels[$i]['name']?></strong>
                                      </a>
                                  </div>

                                      <?php while($i < $channel_count-1 && $i > 1 && (($i + 1) % 2 != 0)): ?>

                                      <div class="channel_image_mobile">
                                                  <?php $i++ ?>
                                          <a href="<?='http://'.$l_xChannels[$i]['url'].'.'.OFLO_LANDING.'/'?>">
                                              <img src="<?=CDN_FRONT."/".$l_xChannels[$i]['url']."/images/chanel.jpg?v=".RELEASE_VERSION ?>" class="carousel-img margin-bottom margin backup_picture">
                                          <strong><?=$l_xChannels[$i]['name']?></strong>
                                      </a>
                                          </div>
                                      <?php endwhile; ?>

                                  </div>
                                  <!--/row-->
                              </div>
                              <!--/item-->

                          <?php endfor; ?>

                    </div>
                    <!--/carousel-inner-->
                    <a class="left carousel-control" href="#myCarousela" data-slide="prev" style="margin-top: -30px; width:2%; color:black; background-image: none; outline: none;" >
                                  <span class="glyphicon glyphicon-chevron-left"></span>
                              </a>
                              <a class="right carousel-control" href="#myCarousela" data-slide="next" style="margin-top: -30px; width:2%; color:black; background-image: none; outline: none;">
                                  <span class="glyphicon glyphicon-chevron-right"></span>
                              </a>
                </div>
                <!--/myCarousel-->
            </div>
            <!--/well-->
        </div>
      <?php  
         
   */

      ?>
               
       <!-- row of thumnails 3x4 -->
      <div class="col-md-12 box-menu pic-vid-div">

        <div class="viewAll">
        <h3>LATEST VIDEOS</h3> 
              <a href="/category/<?php echo strtolower($category['all_categories']); ?>"><strong style="font-size: 14px;">View All ></strong></a>
           <br>
          </div>

         <?php $homepage_video_count = count($l_xOfloHomepageVideos);
// var_dump($l_xOfloHomepageVideos); exit;

            for ($j = 0; $j < $homepage_video_count && $j < 16; $j++) : ?>


                <div class="col-sm-3 col-lg-3 col-md-3 home_thumb">
                        <a href="http://<?=$l_xOfloHomepageVideos[$j]['url'].'.'.OFLO_LANDING ?>/ocontent/video/<?=$l_xOfloHomepageVideos[$j]['id'] ?>/">

                            <div <?php if ($l_xOfloHomepageVideos[$j]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloHomepageVideos[$j]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo "class='home-img-wrap-embeded'";}else{echo "class='home-img-wrap'";} ?>>

                                <img class="backup_picture <?php if ($l_xOfloHomepageVideos[$j]['video_type'] == EMBED_VIDEO_TYPE_ID_YOUTUBE || $l_xOfloHomepageVideos[$j]['video_type'] == EMBED_VIDEO_TYPE_ID_INSTAGRAM){ echo 'home-page-pics-embeded';}else{echo 'home-page-pics';} ?>" src="<?=CDN_FRONT.'/'.$l_xOfloHomepageVideos[$j]['url'].'/assets/ufile/thumb/'.$l_xOfloHomepageVideos[$j]['image_path']?>" alt="">

                            </div>

                            <p class="home_video_description hidden-xs"><span class="video_desc"><?=substr($l_xOfloHomepageVideos[$j]['description'],0,65); ?><? $strlen =strlen($l_xOfloHomepageVideos[$j]['description']); if($strlen>65){echo "...";} ?></span><br>
                            <span>Posted By <span class="by_channel"><?php echo $l_xOfloHomepageVideos[$j]['url'] ?></span>

                            <br>

                            <?php 
                              foreach($homeviews as $views){
                                if($views['content_id'] == $l_xOfloHomepageVideos[$j]['id']){
                            echo number_format($views['total_views']).' Views<span class="dot_between"></span> '.Utils::time_passed($l_xOfloHomepageVideos[$j]['feed_date']+25200);}}?></span></p>

                            <p class="home_video_description hidden-lg hidden-md hidden-sm"><strong><?=substr($l_xOfloHomepageVideos[$j]['description'],0,180); ?><? $strlen =strlen($l_xOfloHomepageVideos[$j]['description']); if($strlen>180){echo "...";} ?></strong><br>
                             <span>Posted By <span class="by_channel"><?php echo $l_xOfloHomepageVideos[$j]['url'] ?></span>

                            <br>

                             <?php 
                              foreach($homeviews as $views){
                                if($views['content_id'] == $l_xOfloHomepageVideos[$j]['id']){
                            echo number_format($views['total_views']).' Views<span class="dot_between"></span> '.Utils::time_passed($l_xOfloHomepageVideos[$j]['feed_date']+25200);}}?></span></p>
                        </a>

                  
                </div>

                <?php endfor; ?>

          

           <?php include '_home_footer.php' ?>  

      </div>
  </div>

    <script type="text/javascript">

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?=$oflo_global_ga_key ?>','breakingmuscle.com');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
        ga('set', 'page', '/');
    </script>

<?php include '_footer.php' ?>