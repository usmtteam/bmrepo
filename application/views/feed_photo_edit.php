<!-- BEGIN PAGE -->
<div class="page-content">

    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">
               <div class="page-title">
                   Photo Editor
               </div>
<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                 
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("tags");

                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                     <div class="row-fluid">
                        <div class="span6">
                           <div class="control-group">
                                <div class="customarea">
                                <?php  echo form_textarea("description",$post['description'],'class="large m-wrap" placeholder="Caption" style="height:80px;"');?>
                                </div>
                            </div>

                            <div class="control-group">
                                 <font size="1" face="arial, helvetica, sans-serif"> Only 512 characters allowed!
                                    </font><br>
                                <?php  echo form_input("tags",$post['tags'],'class="large m-wrap" placeholder="Tags"');?>
                               
                            </div>
                           
                       <?php include(__DIR__.'/feed_template_edit.php'); ?>
	                       
                        <div class="span6">
                        	<input type="hidden" id="photo_width" name="photo_width" val="" />
                        	<input type="hidden" id="photo_height" name="photo_height" val="" />
                        	<input type="hidden" id="photo_crop_x1" name="photo_crop_x1" val="" />
                        	<input type="hidden" id="photo_crop_y1" name="photo_crop_y1" val="" />
                        	<input type="hidden" id="photo_crop_x2" name="photo_crop_x2" val="" />
                        	<input type="hidden" id="photo_crop_y2" name="photo_crop_y2" val="" />
                        	<input type="hidden" id="photo_crop_w" name="photo_crop_w" val="" />
                        	<input type="hidden" id="photo_crop_h" name="photo_crop_h" val="" />
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="gray fileupload-new thumbnail" style="border-style: none !important; max-width: 400px; height: 300px;">
                                   <img src="<?php echo ((isset($post['destination']) and $post['destination']) ? UPLOAD_CLOUD.$post['destination'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 400px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input style="float:right; width:200px;" type="file" name="image" class="default"/></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                             </div>
                             <div class="control-group">
                                
                                <div class="customarea">
                                <?php  echo form_textarea("product_widget",$post['product_widget'],'class="large m-wrap" id="product_widget" maxlength="5000" placeholder="Product_wiget" style="height:400px; margin-left:40px;"');?>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Save",' id="submit_now", class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                    
                      <div id="spinner" class="spinner" style="display:none;">
                            <img id="img-spinner" src="<?php echo ASSETS_DIR; ?>/img/loader2.gif" alt="Loading"/>
                    </div>

                      <script type="text/javascript">
                        $(document).ready(function(){
                            $('#submit_now').click(function() {
                                $('#spinner').show();
                                $('#submit_now').hide();
                            });
                        });
                    </script>

                    <script language="javascript">
                    	$(document).ready(function() {
                    		$('div.fileupload[data-provides=fileupload] div.fileupload-preview').trigger('contentchange');
                    	});
						$('div.fileupload[data-provides=fileupload] div.fileupload-preview').bind('contentchange', function(e) {
							var img = $(e.target).find('img');
							if(img.length > 0) {
								img.load(function(e) {
									var w = 600;
									var h = img[0].clientHeight* w/img[0].clientWidth;
									$('#photo_width').val(w);
									$('#photo_height').val(h);
									img.attr('width', w + 'px');
									img.attr('height', h + 'px');
									img.Jcrop({
										onSelect: showCoords,
							            onChange: showCoords,
							            aspectRatio : 2 / 3,
							            maxSize : [1000, 1000]
									});
								});
							}
						});
						function showCoords(c) {
							$('#photo_crop_x1').val(c.x);
							$('#photo_crop_y1').val(c.y);
							$('#photo_crop_x2').val(c.x2);
							$('#photo_crop_y2').val(c.y2);
							$('#photo_crop_w').val(c.w);
							$('#photo_crop_h').val(c.h);
						}
					</script>
                    <!-- END FORM-->
                 </div>
                 
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
<style type="">
    .btn.fileupload-exists{
        display: none;
    }
</style>