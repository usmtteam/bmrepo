<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxController extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('reports_master_m');
        $this->load->model('main_m');
	
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

	}
	


    function report($id){

	    $this->data['report_by_id'] = $this->reports_master_m->get_reports_by_id($id);

	    $result = $this->data['report_by_id'];
	    
        $this->load->view('reports_list', $this->data);
    }

 
}
	