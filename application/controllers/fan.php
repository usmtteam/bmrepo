<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fan extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', '');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');
        $this->lang->load('auth');
        
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );
        $this->chanel = $this->session->userdata('chanel');
        if(is_array($this->chanel)){ $cid = $this->chanel['id'];  }else{ $cid = $this->chanel; }
        $this->chanel = $this->main_m->get_chanel($cid);
        $this->data['chanel'] = $this->chanel;
        $this->data['community_feeds'] = $this->main_m->getCommunityFeedList();
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);
        
    }
    
    function _remap($method) {
        $this->load->view('header_v');
        $this->load->view('sidebar_v');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){
        redirect('fan/active');
    }
    
    public function active()
    {
    	$cmd = isset($_REQUEST['cmd']) ? $_REQUEST['cmd']: '';
    	
    	if($cmd == 'change_admin_type') {
    		$fan_id = isset($_REQUEST['fan_id']) ? $_REQUEST['fan_id']: '';
    		$admin_type = isset($_REQUEST['admin_type']) ? $_REQUEST['admin_type']: '';
    		
    		$query = 'update enr_fan set admin_type = ? where id = ?';
    		
    		$this->main_m->db->query($query, array($admin_type === '' ? null : $admin_type, $fan_id));
    	}
    	
        $lastlogin = $this->getOption('lastlogin', false);
        if($lastlogin === false){
            $data['lastlogin'] = "asc";
        }else{
            if($lastlogin == 'asc'){
                $data['lastlogin'] = "desc";
            }else{
                $data['lastlogin'] = "asc";
            }
        }
        $joindate = $this->getOption('joindate', false);
        if($joindate === false){
            $data['joindate'] = "asc";
        }else{
            if($joindate == 'asc'){
                $data['joindate'] = "desc";
            }else{
                $data['joindate'] = "asc";
            }
        }
        $lastpost = $this->getOption('lastpost', false);
        if($lastpost === false){
            $data['lastpost'] = "asc";
        }else{
            if($lastpost == 'asc'){
                $data['lastpost'] = "desc";
            }else{
                $data['lastpost'] = "asc";
            }
        }
        $orderby = array(
            'lastlogin' => $lastlogin,
            'joindate' => $joindate,
            'lastpost' => $lastpost,
        );
        
        $page = $this->uri->segment(3);
        $search = $this->uri->segment(4);
        if(empty($page)) $page = 0;
        $data['search'] = $search;
        $data['page'] = $page;
        $data['page_count'] = 10;
        $total_count = count($this->main_m->getFans(false, $orderby, 0, -1, $search));
        $data['total_count'] = $total_count;
        $data['total_page_count'] = ceil($total_count / $data['page_count']);
        
        $fans = $this->main_m->getFans(false, $orderby, $page, $data['page_count'], $search);
        
        $data['users'] = $fans;
        $this->load->view('fans_active', $data);
    } 
    public function getOption($key, $default=''){
        $i = 2;
        $value = "";
        $flag = false;
        while($val = $this->uri->segment($i)){
            if(stripos($val, $key."_") !== false){
                $value = str_replace($key."_", "", $val);
                $flag = true;
                break;
            }
            $i++;
        }
        if(!$flag){
            $value = $default;
        }
        return $value;
    }
    
    public function block()
    {
        $data['users'] = $this->main_m->getDeactiveFans();
        $this->load->view('fans_block', $data);
        
    } 
    
    function suspend($id=null, $code=false){
        if(empty($id)) {
            $id    = $this->uri->segment(3);
        }
        $this->db->where("id", $id);
        $this->db->update("enr_fan", array('is_suspend'=>1));
        if($this->isActiveFan($id)){
            redirect("fan/active", '');
        }else{
            redirect("fan/block", '');
        }
    }
    
    function isActiveFan($id){
        $res = $this->main_m->checkFanUser($id);
        return $res;
    }

    function unsuspend($id=null, $code=false){
        if(empty($id)) {
            $id    = $this->uri->segment(3);
        }
        $this->db->where("id", $id);
        $this->db->update("enr_fan", array('is_suspend'=>0));
        if($this->isActiveFan($id)){
            redirect("fan/active", '');
        }else{
            redirect("fan/block", '');
        }
    }

    //activate the user
    function activate($id=null, $code=false)
    {
        if(empty($id)) {
            $id    = $this->uri->segment(3);
        }
        $this->db->where("id", $id);
        $this->db->update("enr_fan", array('is_block'=>0));
        if($this->isActiveFan($id)){
            redirect("fan/active", '');
        }else{
            redirect("fan/block", '');
        }

    }
    
    function deactivate($id = NULL)
    {

        if(empty($id)) {
            $id    = $this->uri->segment(3);
        }
        $this->db->where("id", $id);
        $this->db->update("enr_fan", array('is_block'=>1));
        if($this->isActiveFan($id)){
            redirect("fan/active", '');
        }else{
            redirect("fan/block", '');
        }
    }
	function add_user() {
		
		
		$data = array(
				'name'  => $this->input->post('name'),
				'email'  => $this->input->post('email'),
				'credit'  => $this->input->post('credit'),
		);
		if (isset($_POST) && !empty($_POST)) {

			$this->form_validation->set_rules('name', "name", 'required');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
			$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
			$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			$data['password'] = $this->ion_auth->hash_password($this->input->post('password'));
			
			if ($this->form_validation->run() === TRUE)
			{
				$data['cid'] = $this->data['chanel']['id'];
				$data['joined'] = time();
				$this->db->insert('enr_fan', $data);
				$id = $this->db->insert_id();
				$result = $this->main_m->updateProfilePhoto($id);
			
				//check to see if we are creating the user
				//redirect them back to the admin page
				$this->session->set_flashdata('message', "User Saved");
				redirect("fan/active", '');
			}
		}

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		
		$this->data['name'] = array(
				'name'  => 'name',
				'id'    => 'name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('name', $data['name']),
		);
		$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email', $data['email']),
		);
		$this->data['credit'] = array(
				'name'  => 'credit',
				'id'    => 'credit',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('credit', $data['credit']),
		);
		$this->data['password'] = array(
				'name' => 'password',
				'id'   => 'password',
				'type' => 'password'
		);
		$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password'
		);
		
		$this->_render_page('fan/add_user', $this->data);
	}
    //edit a user
    function edit_user($id=NULL)
    {
        if(empty($id)) {
            $id    = $this->uri->segment(3);
        }
        //echo "id=".$id;
        
        $user = $this->main_m->getFan($id);

        //validate form input
        $this->form_validation->set_rules('name', "name", 'required');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');

        if (isset($_POST) && !empty($_POST))
        {
            // do we have a valid request?

            $data = array(
                'name'  => $this->input->post('name'),
                'email'  => $this->input->post('email'),
                'credit'  => $this->input->post('credit'),
            );

            //update the password if it was posted
            if ($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');

                $data['password'] = $this->ion_auth->hash_password($this->input->post('password'));
            }

            if ($this->form_validation->run() === TRUE)
            {
                $this->db->where('id', $id);
                $this->db->update('enr_fan', $data);
                $result = $this->main_m->updateProfilePhoto($id);
                
                //check to see if we are creating the user
                //redirect them back to the admin page
                $this->session->set_flashdata('message', "User Saved");
                redirect("fan/active", '');
            }
        }

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['user'] = $user;
        $this->data['comments'] = $this->main_m->getAllComments($id);
        $feeds = $this->main_m->getCommunityFeedList(false, $this->$id);
    	$this->data['posts'] = array();
        foreach($feeds as $feed){
            if($id == $feed['fan_id']){
                $this->data['posts'][] = $feed;
            }
        }
        $this->data['ref_url'] = isset($_REQUEST['ref']) ? $_REQUEST['ref'] : site_url('fan/active');
        


        $this->data['username'] = array(
            'name'  => 'username',
            'id'    => 'username',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('username', $user->username),
        );
        $this->data['name'] = array(
            'name'  => 'name',
            'id'    => 'name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('name', $user['name']),
        );
        $this->data['first_name'] = array(
            'name'  => 'first_name',
            'id'    => 'first_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        );
        $this->data['last_name'] = array(
            'name'  => 'last_name',
            'id'    => 'last_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );
        $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email', $user['email']),
            );
        $this->data['credit'] = array(
                'name'  => 'credit',
                'id'    => 'credit',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('credit', $user['credit']),
            );
        
        $this->data['phone'] = array(
            'name'  => 'phone',
            'id'    => 'phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
        );
        
        $this->data['password'] = array(
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'type' => 'password'
        );

        $this->_render_page('fan/edit_user', $this->data);
    }
    function delete_comment() {
    	$user_id = $this->uri->segment(3);
    	$comment_id = $this->uri->segment(4);
    	$ref_url = isset($_REQUEST['ref']) ? $_REQUEST['ref'] : '';
    	
    	
    	$this->main_m->db->query('delete from enr_comment where id = ' . $comment_id);

    	redirect(site_url('fan/edit_user/' . $user_id) . (empty($ref_url) ? '' : '?ref=' . $ref_url));
    	
    	exit();
    }
    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }
    function _valid_csrf_nonce()
    {
        /*
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        */
        return true;    // ADDED BY PGR
    }
    function _render_page($view, $data=null, $render=false)
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $render);

        if (!$render) return $view_html;
    }
    function delete_user(){
        $id    = $this->uri->segment(3);
        if(empty($id)) {
            show_error("Select a fan to delete!");
        }
        if($this->isActiveFan($id)){
            $is_active = true;
        }else{
            $is_active = false;
        }
        $this->main_m->deleteFan($id);
        if($is_active){
            redirect('fan/active');
        }else{
            redirect('fan/block');
        }
    }
    function view(){
        $id    = $this->uri->segment(3);
        if(empty($id)) {
            show_error("Select a fan to preview!");
        }
        $users = $this->main_m->getUsers(array('id'=>$id));
        $users[0]['last_login'] = date('m/d/Y', $users[0]['last_login']);
        $users[0]['join_date'] = date('m/d/Y', $users[0]['created_on']);
        $this->data['user'] = $users[0];
        $feeds = array();
        foreach($this->data['community_feeds'] as &$feed){
            if($feed['fan_id'] == $id){
                $feeds[] = $feed;
            }
        }
        $this->data['community_feeds'] = $feeds;
        if($this->isActiveFan($id)){
            $this->data['is_active'] = true;
        }else{
            $this->data['is_active'] = false;
        }
        $this->_render_page('fan/view', $this->data);
    }

}

