<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Group Manager
                    <small></small>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('users_group_manager/users_group_manager_add'); ?>" class="btn blue">
                        Add Groups
                        </a>
                    </div>
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Hub</th>
                            <th class="span2">Edit</th>
                            <th class="span2">Delete</th>
                        </tr>
                    </thead>
                    <tbody>  
                           
                        <tr>
                         
                       
                            <?php foreach($users_group_manager as $users_group ): ?>
                       
                            <td>
                                <?php echo $users_group['group_name']; ?>
                                                          
                            </td>

                             <td>
                                <?php echo $users_group['hub_id']; ?>
                                                          
                            </td>

                            <td class="center" style="text-align: center;">
                          
                            <?php
                                echo 
                                '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="'.site_url('users_group_manager/users_group_manager_edit/'.$users_group['id']).'">'.
                                '<i class="icon-edit" ></i>Edit'.
                                '</a>';
                            ?>
                        
                            </td>
                            <td class="center" style="text-align: center;">
                                
                            <?php
                                echo 
                                '<a class="btn" style="padding-left: 1px; padding-right: 17px;" href="javascript:confirm_deletion(\''.$users_group['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                       
                            </td>
                        </tr>

                             
            
           
                               <?php endforeach; ?> 
                 

               
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    function confirm_deletion(id) {
        if(confirm("Do you want to delete this user?")) {
            document.location.href = "<?php echo site_url('users_group_manager/confirm_deletion'); ?>/" +id;
        }
    }    
</script>