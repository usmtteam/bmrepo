<?php
//    phpinfo();
?>
<style>

</style>
<!-- BEGIN PAGE -->
<div class="page-content">
    <div class="container-fluid" id="dashboard">

        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->            
                <div class="page-title">
                    Category List
                    <small></small>
                    <div class="btn-group pull-right">
                        <a id="sample_editable_1_new" href="<?php echo site_url('category_list/add_category'); ?>" class="btn blue">
                        Add New 
                        </a>
                    </div>
                </div>
                <?php // echo $message;?>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="span2">ID</th>
                            <th>Name</th>
                            <th>Is this an Breaking Movement <br> category?</th>
                            <th>Will this be on <br>the Breaking Movement menu?</th>
                            <th class="span2">Edit</th>
                            <th class="span2">Delete</th>
                        </tr>
                    </thead>
                    <tbody>     
                    <?php 
                    foreach($viral_media_category as $category): ?>
                        <tr>
                            <!--<td class="feed-thumbnail-td ">
                            <?php if(is_file(UPLOAD_DIR.$event['image_path'])){  ?>
                                <img class='feed-thumbnail pull-right' src="<?php echo UPLOAD_URL.$feed['image_path']; ?>">
                            <?php } ?>
                            </td>-->

                            <td><?php echo $category['id']; ?></td>
                            <td><?php echo $category['cat_name']; ?></td>
                            <td>
                                                <?php                                                               
                                                        if ($category['is_oflo']==1){
                                                                        echo "Yes";
                                                        }
                                                    
                                                ?>
                                            
                            </td>
                            <td>
                                                <?php                                                               
                                                        if ($category['oflo_menu']==1){
                                                                        echo "Yes";
                                                        }
                                                    
                                                ?>
                            </td>
                            <td class="center" style="text-align: center;">
                          
                            <?php
                                echo 
                                '<a class="btn" style="padding-right: 17px; padding-left:1px;" href="'.site_url('category_list/edit_category/'.$category['id']).'">'.
                                '<i class="icon-edit" ></i>Edit'.
                                '</a>';
                            ?>
                        
                            </td>
                            <td class="center" style="text-align: center;">
                                
                            <?php
                                echo 
                                '<a class="btn" style="padding-left: 1px; padding-right: 17px;" href="javascript:confirm_del(\''.$category['id'].'\')">'.
                                '<i class="icon-trash"></i>Delete'.
                                '</a>';
                            ?>
                       
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function confirm_del(id) {
        if(confirm("Do you want to delete this category?")) {
            document.location.href = "<?php echo site_url('category_list/confirm_del'); ?>/" +id;
        }
    }    
</script>