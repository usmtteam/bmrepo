<!-- BEGIN PAGE -->
<div class="page-content">
    <?php 
        $this->load->view('header_feed');
    ?>
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                    <div id="form_head_back">Add Poll</div>
                 <div class="portlet-body form pb-feed">
                    <?php 
                    echo form_open_multipart("content/feed/poll", 'class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("credit");
                        echo form_error("question0[0]");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                    
                    <h4 class="alert-heading">Thumbnail</h4>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Caption</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("description",$post['description'],'class="large m-wrap"');?>
                                </div>
                            </div>
                        </div>    
                        <div class="control-group">
                                <label class="control-label" >Open</label>
                                <div class="controls fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img src="<?php echo ((isset($post['image_path']) and $post['image_path']) ? UPLOAD_URL.$post['image_path'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image') ?>" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="image1" class="default" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Opening Page</h4>
                    <div class="row-fluid">
                        <div class="span6">
                        
                            <div class="control-group">
                                <label class="control-label" for="url">Text</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("text",$post['text'],'id="text" class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Tags</label>
                                <div class="controls">
                                <?php  echo form_input("tags",$post['tags'],'class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Credits</label>
                                <div class="controls">
                                <?php  echo form_input("credit",'','class="large m-wrap"');?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Permission</label>
                                <div class="controls">
                                    <?php echo form_dropdown("is_publish", array('Not Published', 'Publish', 'Draft'), $post['is_publish'], 'class="large m-wrap"');?>
                                </div>
                            </div>
	                        <div class="control-group">
	                            <label class="control-label" for="url">Sticky Flag</label>
	                            <div class="controls">
	                            <?php  echo form_checkbox("is_sticky", 1, $post['is_sticky'],'class=""');?>
	                            </div>
	                        </div> 
                            
                            
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Start date</label>
                                <div class="controls">
                                	<div id="start_date_div" class="input-append">
		                            	<input type="text" id="start_date" name="start_date" value="<?php echo $post['start_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="url">End date</label>
                                <div class="controls">
                                	<div id="end_date_div" class="input-append">
		                            	<input type="text" id="end_date" name="end_date" value="<?php echo $post['end_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            <script language="javascript">
								$(document).ready(function() {
									$('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
									$('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
								});
							</script>
                        </div>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Background</label>
                                <div class="controls fileupload fileupload-new center" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                       <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" name="background_image" class="default" /></span>
                                       <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Questions</h4>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                               <div class="controls customarea">
                                 <h5 style="font-weight: bold;">Question</h5>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <table class="table answer">
                                <tr>
                                    <th>Answers</th>
                                    <!--<th width='90px'>True / False</th>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id='question_container'>
                        <input type="hidden" id='qcount' value="1">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                   <div class="controls customarea">
                                    <?php  echo form_textarea("question[0]",'','class="large m-wrap"');?>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <table class="table answer">
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center" width="90px">
                                            <span><input type="checkbox" name="is_correct_0_0" value="1"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_1"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_2"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_3"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_4"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_5"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_6"></span>
                                        </td>-->
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php  echo form_input("answer[0][]",'','class="large m-wrap"');?>
                                        </td>
                                        <!--<td class="center">
                                            <span><input type="checkbox" name="is_correct_0_7"></span>
                                        </td>-->
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">                    
                        <span class="add-more" onclick="return addMoreQuestion();">Add More</span>
                    </div>
                    <div style="border: 1px solid #e5e5e5;margin-bottom: 15px;"></div>
                    <h4 class="alert-heading">Closing Page</h4>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="url">Caption</label>
                                <div class="controls customarea">
                                <?php  echo form_textarea("end_description",$post['end_description'],'id = "end_description" class="large m-wrap"');?>
                                
                                <script language="javascript">
									bkLib.onDomLoaded(function() {
										new nicEditor({fullPanel : true, iconsPath : '<?php echo base_url(); ?>nicEditorIcons.gif'}).panelInstance('end_description');
									});
								</script>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="fileupload fileupload-new center" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                   <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                   <span class="fileupload-exists">Change</span>
                                   <input type="file" name="image2" class="default" /></span>
                                   <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                             </div>
                        </div>
                    </div>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Post",' class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
