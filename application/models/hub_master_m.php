<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hub_Master_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

     // get list of hub managers
    
    function get_hub_manager(){

     $sql = "SELECT *
                FROM hub_manager";

     $query = $this->db->query($sql);
     $rows = $query->result_array();

      return $rows;

    }

    // add hub managers

    function add_hub_manager($post){


     $this->load->database();
     $name= $this->input->post('name');
     $template_path= $this->input->post('template_path');
     $api_path =$this->input->post('api_path');
     $front_style_path= $this->input->post('front_style_path');
     $back_style_path = $this->input->post('back_style_path');
     $push_cert_path = $this->input->post('push_cert_path');
     $logo_backend = $this->input->post('logo_backend');


          $post = array (
            'name' => $name, 
            'template_path' => $template_path,
            'api_path' => $api_path,
            'front_style_path' => $front_style_path,
            'back_style_path' => $back_style_path,
            'push_cert_path' => $push_cert_path,
            'logo_backend' => $logo_backend

        );


         $insert_query = "INSERT IGNORE INTO hub_manager (name, template_path, api_path, front_style_path, back_style_path, push_cert_path, logo_backend) 
                     VALUES('$name', '$template_path', '$api_path', '$front_style_path', '$back_style_path', '$push_cert_path', '$logo_backend')";


                   
          $this->db->query($insert_query); 
    


     }



     //  delete hub managers

    function confirm_deletion($data){
     $this->db->select('*');
     $this->db->from('hub_manager');
     $this->db->where('id',$data);
     $query = $this->db->get();
     $result=$query->result();
     return $result;

     }


     // update hub managers


     function update_hub_manager($id,$data){

     $this->db->where('id', $id);
     $result = $this->db->update('hub_manager', $data);
      return $result;

     }


     // get hub manager id

    function get_hub_manager_id($data){
     $this->db->select('*');
     $this->db->from('hub_manager');
     $this->db->where('id', $data);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
     
     }







   
}