<?php

// Grabs page 1 with 50 items

require_once(__DIR__.'/../../../config/settings.php');
require_once(__DIR__.'/../front/FrontCoreSql.php');
require_once(__DIR__.'/../front/FrontCoreManager.php');
define('ZENCODER_API_KEY','b34cc8e8b92ae58bf3f1a151608a0ef6');
define('ZENCODER_CLASS',LIB_PATH.'zencoder/Zencoder.php');

try {

    $l_xFrontCoreSql = new FrontCoreSql();

    spl_autoload_unregister('coreAutoload');
    require(ZENCODER_CLASS);
    $l_xZencoder = new Services_Zencoder(ZENCODER_API_KEY);

//    for ($page_number = 0; $page_number < 10000; $page_number++){
    for ($page_number = 1; $page_number <= 2; $page_number++){

        echo "<BR>\n##################################################################################################";
        echo "<BR>\n\$page_number: ".$page_number;

        $job_index = $l_xZencoder->jobs->index(array('page'=> $page_number), array()); // From oflo production
//        $job_index = file_get_contents("https://app.zencoder.com/api/v2/jobs?api_key=".ZENCODER_API_KEY."&page=".$page_number); // From oflo production

//    echo "<BR>\n\$job_index: ";
//    var_dump($job_index);

//    exit;

        foreach($job_index as $job){

            echo "<BR>\n----------------------------------------------------------------------------------------------------";

//        echo "<BR>\n\$job: ";
//        var_dump($job);

//        exit;
//        echo "<BR>\n\$job->id: ";
//        var_dump($job->id);

//        echo "<BR>\n\$job->raw_response->job->input_media_file->duration_in_ms: ";
//        var_dump($job->raw_response->job->input_media_file->duration_in_ms);

//        exit;

            $job_id = $job->id;
            $duration_in_ms = $job->raw_response->job->input_media_file->duration_in_ms;

            echo "<BR>\n\$job_id: ";
            var_dump($job_id);

            echo "<BR>\n\$duration_in_ms: ";
            var_dump($duration_in_ms);

//        exit;

            $l_sSql = 'SELECT * FROM enr_video
                       WHERE zencoder_id = '.$job_id.'
                       AND video_duration = 0';

            $select_video_result = $l_xFrontCoreSql->buildObject($l_xFrontCoreSql->runQuery($l_sSql));

            echo "<BR>\n\$select_video_result: ";
            var_dump($select_video_result);

            // Update video_duration
            if (!empty($select_video_result)){

                // Get Job/Output IDs to update their video duration.
                $l_sSql = 'UPDATE enr_video SET video_duration = "'.$duration_in_ms.'"
                           WHERE zencoder_id = '.$job_id;

                echo $l_sSql;
                $update_video_result = $l_xFrontCoreSql->runQuery($l_sSql);

                echo "<BR>\n\$update_video_result: ";
                var_dump($update_video_result);

            } else {
                echo "<BR>\nZencoder Job ID: ".$job_id." not in this database.";
            }

//        exit;

        }
    }

//    // Success if we got here
//    echo "w00t! \n\n";
//    echo "Job ID: ".$l_xEncodingJob->id."\n";
//    echo "Output ID: ".$l_xEncodingJob->outputs['web']->id."\n";
//    // Store Job/Output IDs to update their status when notified or to check their progress.
//    $l_sSql = 'UPDATE enr_video SET zencoder_id = "'.$l_xEncodingJob->id.'" WHERE id = '.$l_nItemsId;
//    echo $l_sSql;
//    $l_xFrontCoreSql->runQuery($l_sSql);

} catch (Services_Zencoder_Exception $e) {
    // If were here, an error occured

    echo "Fail :(<BR><BR>\n\n";
    echo "Errors:<BR>\n";
    foreach ($e->getErrors() as $error) echo $error."<BR>\n";
    echo "Full exception dump:<BR><BR>\n\n";
    var_dump($e);
}

spl_autoload_register('coreAutoload');

?>


