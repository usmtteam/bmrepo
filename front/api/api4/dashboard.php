<? 


	define('GROUP_ADMIN',1);
	define('GROUP_SALES',2);	

	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
	date_default_timezone_set('UTC');

	session_start();

	if(0){
		$token_data['payload']['email'] = 'vincent@starclubltd.com';
	}else{

		include_once __DIR__."/../include/lib/google/Google/autoload.php";
		$client_id = '405597610036-vpds0015blkqabqaderl10h6m3mq07bs.apps.googleusercontent.com';
		$client_secret = 'S3sFjqO7hNLm37cG33XKulyu';
		$redirect_uri = 'http://darwin.on.starsite.com/api/dashboard/';
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->setScopes('email');

		if (isset($_REQUEST['logout'])) {
			unset($_SESSION['doconnect']);
			unset($_SESSION['access_token']);
		}


		if (isset($_GET['code'])) {
			$client->authenticate($_GET['code']);
			$_SESSION['access_token'] = $client->getAccessToken();
			$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
			header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
			exit;
		}

		if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
			$client->setAccessToken($_SESSION['access_token']);
		} else {
			$authUrl = $client->createAuthUrl();
		}

		if ($client->getAccessToken()) {
			$_SESSION['access_token'] = $client->getAccessToken();
		try{
			$token_data = $client->verifyIdToken()->getAttributes();
		}catch(Exception $e){
			header('Location: /api/dashboard/?logout');
			exit;
		}
		
		if (strpos($client_id, "googleusercontent") == false) {
			echo "Cannot connect";
			exit;
		}
		}
	}
?>
<html>
<head>
	<style type="text/css">
	*{ margin:0; padding:0; outline:none;}
	@font-face { font-family: 'HelveticaLight'; src: url('/assets/fonts/HelveticaLight.eot'); /* IE9 Compat Modes */ src: url('/assets/fonts/HelveticaLight.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */  url('/assets/fonts/HelveticaLight.woff') format('woff'), /* Modern Browsers */  url('/assets/fonts/HelveticaLight.ttf') format('truetype'), /* Safari, Android, iOS */  url('/assets/fonts/HelveticaLight.svg#df2eaeb3aeecfb305b3632871e6dcb16') format('svg'); /* Legacy iOS */ font-style: normal; font-weight: 200; }
	@font-face { font-family: 'HelveticaUltraLight'; src: url('/assets/fonts/helveticaultralight-webfont.eot'); /* IE9 Compat Modes */ src: url('/assets/fonts/helveticaultralight-webfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */  url('/assets/fonts/helveticaultralight-webfont.woff') format('woff'), /* Modern Browsers */  url('/assets/fonts/helveticaultralight-webfont.ttf') format('truetype'), /* Safari, Android, iOS */  url('/assets/fonts/helveticaultralight-webfont.svg#df2eaeb3aeecfb305b3632871e6dcb16') format('svg'); /* Legacy iOS */ font-style: normal; font-weight: 200; }
	@font-face { font-family: 'HelveticaThin'; src: url('/assets/fonts/HelveticaThin.eot'); /* IE9 Compat Modes */ src: url('/assets/fonts/HelveticaThin.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */  url('/assets/fonts/HelveticaThin.woff') format('woff'), /* Modern Browsers */  url('/assets/fonts/HelveticaThin.ttf') format('truetype'), /* Safari, Android, iOS */  url('/assets/fonts/HelveticaThin.svg#df2eaeb3aeecfb305b3632871e6dcb16') format('svg'); /* Legacy iOS */ font-style: normal; font-weight: 200; }
	@font-face { font-family: 'helveticaneue-medium-webfont'; src: url('/assets/fonts/helveticaneue-medium-webfont.eot'); /* IE9 Compat Modes */ src: url('/assets/fonts/helveticaneue-medium-webfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */  url('/assets/fonts/helveticaneue-medium-webfont.woff') format('woff'), /* Modern Browsers */  url('/assets/fonts/helveticaneue-medium-webfont.ttf') format('truetype'), /* Safari, Android, iOS */  url('/assets/fonts/helveticaneue-medium-webfont.svg#df2eaeb3aeecfb305b3632871e6dcb16') format('svg'); /* Legacy iOS */ font-style: normal; font-weight: 100; }
	body{ font-family: 'HelveticaUltraLight';
	font-family: 'HelveticaThin', sans-serif;
	 font-size: 22px;  text-align: center; padding-top:100px;  background:#14191D; color:#fff; }
	a{  background:none; color: #fff; text-decoration: underline; }
	a:hover{ color:#66B553}
	a span{ font-size:15px}
	.upfront{ font-size:12px; padding:20px 0 0 0;}
	h2{ font-weight: normal; margin-bottom:10px}
	.theTable{ max-width:1400px; margin:40px auto; font-family: 'HelveticaLight', sans-serif; font-size: 14px; width:90% }
	.theTable .heading{ }
	.theTable td{ border-bottom:1px solid #333; padding:8px 10px; font-size: 12px; background:#111 }
	.theTable .heading td{   }
	.theTable .row-1 td{ background: #191919	}
	.theTable .heading td{ /*background: #3B99BB;*/ color: #fff; border-bottom:2px solid #fff}
	/*.theTable tr:hover td{ background: #424300}*/
	.dim{ color:#bbb;  font-size: 11px;	}
	.warn{ color:#ff0000; font-size: 11px;	}
	.info-box{ color:#ffffff;  font-size: 12px; text-align: center; display: inline-block; padding:0 10px	}
	.info-box b{ color:#C68D60; font-size:32px; font-weight: normal;	}
	.cgreen{ color:#66B553; font-weight: bold;	}
	.lgreen,b.lgreen{ color:#66B454; }
	.lblue,b.lblue{ color:#3B99BB; }
	.theTable .alert td{ background: #aa0000; padding:8px}
	.theTable .pending td{ background: #660000; padding:8px}
	.toplogout{ position: absolute; top: 20px; right:20px; font-size:12px;text-align: right; }
	.blink {
      animation: blink 1s steps(5, start) infinite;
      -webkit-animation: blink 1s steps(5, start) infinite;
    }
    @keyframes blink {
      to { visibility: hidden; }
    }
    @-webkit-keyframes blink {
      to { visibility: hidden; }
    }
	</style>
</head>
<body>
<div class="box">
  <div class="request">
	<?php
	if (isset($authUrl)) {
		$_SESSION['doconnect'] = 1;
	  	echo "<a class='login' href='" . $authUrl . "'>Connect</a>";
	  	exit;
	}
	?>
  </div>
  <div class="data">
	<?php 

	require(__DIR__.'/../config/settings.php');
	$l_xSSDash = new SSDash();
	if (isset($token_data)) {

		$l_sEmail = strtolower($token_data['payload']['email']);
		$l_xSSDash->setUser($l_sEmail);

		if(!$l_xSSDash->isAllowedAccess()){
			?>
				<p>You do not have permissions to view this content</p>
				<div class="toplogout"><?= $l_sEmail ?><br /><a href='?logout'>Logout </a></div>
				<?
				exit;
		}	
	}
	?>
  </div>
</div>
<?





if(!$l_xSSDash->isAllowedAccess())
	exit;

	
	date_default_timezone_set('UTC');	

	$l_xSql = AppController::getInstance()->getConnection();
	$l_sSql = 'SELECT GROUP_CONCAT(social_property_type_id) as props, o.* FROM enr_channel_oauth o GROUP BY channel_id';
	$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

	$l_sSql = 'SELECT * FROM users WHERE cid > 0';
	$l_aUsersData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
	$l_aUsersMapped = array();
	foreach($l_aUsersData as $l_xRow){
		$l_aUsersMapped[$l_xRow['cid']] = $l_xRow;
	}

	$l_aSocial = array();
	foreach($l_aData as $l_xRow){
		$l_aSocial[$l_xRow['channel_id']] = 
		array('fb' => preg_match("/1/",$l_xRow['props']),
			'tw' => preg_match("/2/",$l_xRow['props']),
			'ig' => preg_match("/3/",$l_xRow['props']),
			);
	}
	 
    $l_sSql = '

    SELECT 
	SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "video" THEN 1 END) as total_videos,
	SUM( CASE WHEN t1.item_id > 0 AND t1.the_type = "photo" THEN 1 END) as total_photos,
	
	MAX(t1.time_stamp) as lastpost,
	
	t1.*
	FROM (
		SELECT v.id as item_id, "video" as the_type, c.id as channel_id, c.*, v.time_stamp

    			
    			FROM enr_chanel c

    			LEFT JOIN enr_video v 
    			ON c.id = v.cid

    			WHERE (c.id >= 37 OR c.id = 19 OR c.id = 18 OR c.id = 15 ) AND c.id NOT IN (58)
    			AND c.is_publish = 1


    			
	UNION 
		SELECT p.id as item_id, "photo" as the_type, c.id as channel_id, c.*, p.time_stamp
    			
    			 FROM enr_chanel c

				LEFT JOIN enr_photo p 
    			ON c.id = p.cid

    			WHERE (c.id >= 37 OR c.id = 19 OR c.id = 18 OR c.id = 15 ) AND c.id NOT IN (58)
    			AND c.is_publish = 1
    			
    			) t1 
	GROUP BY t1.channel_id
	ORDER BY t1.channel_id ASC, t1.created ASC

    ';
    $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));

             
?>
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<?
	// print_R($l_aData);
	?>
	<div class="toplogout"><?= $l_sEmail ?><br /><a href='?logout'>Logout </a></div>
	<h2>StarClub Network</h2>


	
		<? 
			$l_nTotalCount = 0;
			$l_nTotalConfirmed = 0;
			$l_nTotalRev = 0;
			$l_nTotalVideos = 0;
			$l_nTotalPhotos = 0;
			$l_nTotalPageViews = 0;
			$l_nTotalReach = 0;
			$l_aRowsOuput = array();
			
			foreach($l_aData as $l_nCount => $l_xRow){

			if(in_array($l_xRow['channel_id'],array(43,44,45,46)))
				continue;
			ob_start();
			$l_nTotalCount++;


			$l_bAlert = false;
			$l_bAgreed = strlen($l_xRow['agreed']) > 4;

			if($l_xRow['lastpost'] > 0){
				$l_nAgo = (time()-$l_xRow['lastpost'])/60/60;
				if($l_nAgo > 24*2){
					//over 48 hours 
					$l_bAlert = true;
				}
			}

			
			
			?>
			

			<tr class="row-<?= $l_nCount % 2 ?><?= ($l_bAlert) ? ' alert ' : '' ?><?= (!$l_bAgreed) ? ' pending ' : '' ?>">
				<td><span class="dim"><?= $l_nTotalCount ?>.</span></td>
				<td>
				
				<!-- <img src="<?= CDN_FRONT ?>/<?= $l_xRow['slug'] ?>/assets/ufile/<?= $l_xRow['image_path'] ?>" width="30" height="30" /> -->
				<?= $l_xRow['name'] ?><br /><span class="dim"><?= ($l_aUsersMapped[$l_xRow['channel_id']]['username']) ?></span></td>
				<td><?= date('m-d-Y',strtotime($l_xRow['created'])) ?></td>
				<td><?

					if(!$l_bAgreed){
						echo '<i class="warn">--------</i>';
					}else{
						$l_nTotalConfirmed++;
						echo '<span class="cgreen">'.date('m-d-Y',strtotime($l_xRow['agreed'])).'</span>';
					}

				?></td>
				<td><a href="http://<?= $l_xRow['url'] ?>.on.starsite.com" target="_blank"><?= $l_xRow['url'] ?>.on.starsite.com</a></td>
				<td align="center"><?= SSDash::ssGetCleanNumberFormat($l_xRow['total_reach']) ?></td>
				<? if($l_xSSDash->isInGroup(GROUP_ADMIN)){?>
					<td align="center"><?= $l_xRow['google_analytics_current_total'] ?></td>
					<td align="center">$<?= number_format($l_xRow['google_analytics_current_total']/1000,2) ?></td>
				<? } ?>
				<td align="center"><?= number_format($l_xRow['total_videos']) ?> / <?= number_format($l_xRow['total_photos']) ?>&nbsp;</td>
				<td align="center"><?
						if($l_xRow['lastpost'] > 0){
							if($l_bAlert){
								echo '<span class="alert blink">'.SSDash::timeago($l_xRow['lastpost']).'</span>';
							}else{
								echo SSDash::timeago($l_xRow['lastpost']);
							}
						}
				 ?>&nbsp;</td>
				<td>
				<?= ($l_aSocial[$l_xRow['channel_id']]['fb'] == 1) ? 'Facebook' : "" ?>
				<?= ($l_aSocial[$l_xRow['channel_id']]['tw'] == 1) ? 'Twitter' : "" ?>
				<?= ($l_aSocial[$l_xRow['channel_id']]['ig'] == 1) ? 'Instagram' : "" ?>
				</td>
				<td><?= ($l_aUsersMapped[$l_xRow['channel_id']]['app_version']); ?></td>
			</tr>
			<?
			$l_nTotalPageViews += $l_xRow['google_analytics_current_total'];
			$l_nTotalVideos += $l_xRow['total_videos'];
			$l_nTotalPhotos += $l_xRow['total_photos'];
			$l_nTotalReach += $l_xRow['total_reach'];
			$l_nTotalRev += $l_xRow['google_analytics_current_total']/1000;
			$l_aRowsOuput[] = ob_get_clean();
		}
		?>

	<div> 	
		<div class="info-box"><b><?= number_format($l_nTotalConfirmed) ?>/<?= number_format($l_nTotalCount) ?> <span style="font-size:13px">( <?= number_format($l_nTotalConfirmed/$l_nTotalCount,2)*100 ?>% )</span></b><p>Confirmed Talent</p></div>
		<div class="info-box"><b><?= number_format($l_nTotalVideos) ?></b><p>Video Uploads</p></div>
		<div class="info-box"><b><?= number_format($l_nTotalPhotos) ?></b><p>Photo Uploads</p></div>
		<div class="info-box"><b class="lblue"><?= SSDash::ssGetCleanNumberFormat($l_nTotalReach) ?></b><p>FB Reach</p></div>
		<? if($l_xSSDash->isInGroup(GROUP_ADMIN)){?>
			<div class="info-box"><b class="lblue"><?= SSDash::ssGetCleanNumberFormat($l_nTotalPageViews) ?></b><p><?= date("M") ?> Page Views</p></div>
			<div class="info-box"><b class="lgreen">$<?= number_format($l_nTotalRev,2) ?></b><p><?= date("M") ?> Payout</p></div>
		<? } ?>
		
	</div>

	<table class="theTable" width="80%" cellpadding="0" cellspacing="0"> 
		<tr class="heading">
				<td>#</td>
				<td>Talent</td>
				<td>Invite Sent</td>
				<td>Talent Agreed</td>
				<td>URL</td>
				<td align="center">FB Reach</td>
				<? if($l_xSSDash->isInGroup(GROUP_ADMIN)){?>
					<td align="center">Est Mon. PageViews</td>
					<td align="center">Talent Payout <?= date('M') ?></td>
				<? } ?>
				<td align="center">Videos/Photos</td>
				<td>Last Post</td>
				<td>Paired</td>
				<td>App&nbsp;V. (Latest 3.3)</td>
			</tr>
			<?
				$l_aRowsOuput = array_reverse($l_aRowsOuput);
				foreach($l_aRowsOuput as $l_xRow){
					echo $l_xRow;
				}
			?>
	</table>
	<script type="text/javascript">
	$(document).ready(function(){
		// $('h2').append(' (<?= $l_nCount ?>)');
	});
	</script>


	<!-- <h2>Legacy Network</h2> -->
	<?	
		/*
		$l_aRowsOuput = array();
		$l_sSql = 'SELECT * FROM enr_chanel WHERE id >= 17 AND id <= 19 ORDER BY id DESC';
		$l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
		?>
		<table class="theTable" width="80%" cellpadding="0" cellspacing="0"> 
		<tr class="heading">
			<td>Talent</td>
			<td>May-> <?= date('M') ?> PageViews</td>
			<td>Current Reach</td>
		</tr>
		<?

		$l_nTotalPageViews = 0;
		$l_nTotalReach = 0;

		foreach($l_aData as $l_xRow){
			// print_r($l_xRow);
			$l_nTotalPageViews += $l_xRow['google_analytics_current_total'];
			$l_nTotalReach += $l_xRow['total_reach'];
			ob_start();
			?>
			<tr>
				<td><?= $l_xRow['name'] ?></td>
				<td><?= number_format($l_xRow['google_analytics_current_total']) ?></td>
				<td><?= ssGetCleanNumberFormat($l_xRow['total_reach']) ?></td>
			</tr>
			<?
			$l_aRowsOuput[] = ob_get_clean();
		}

		?>
		<div> 	
			<div class="info-box"><b class="lblue"><?= number_format($l_nTotalPageViews) ?></b><p>May -> <?= date("M") ?> Page Views</p></div>
			<div class="info-box"><b class="lblue"><?= ssGetCleanNumberFormat($l_nTotalReach) ?></b><p>Reach</p></div>
		</div>
		<?

		$l_aRowsOuput = array_reverse($l_aRowsOuput);
		foreach($l_aRowsOuput as $l_xRow){
			echo $l_xRow;
		}

		?></table><?

	*/
	?>
		<br /><br /><br /><br /><br /><br /><br />
</body>
</html>



<?

	class SSDash {

		var $_aGroups;
		var $_sEmail;
		var $_bAllowed;

		function __construct(){
			require_once(LIB_PATH.'tuscano/VTPush.php');
			$this->_aGroups = array();
			$this->_aGroups[GROUP_ADMIN] = array(
				'bernhard@starclubltd.com',
				'james@starclubltd.com',
				'ruben@starclubltd.com',
				'ray@starclubltd.com',
				'kim@starclubltd.com',
				'zach@starclubltd.com',
				'vincent@starclubltd.com',
				'benjamin@starclubltd.com',
				'mae@starclubltd.com',
				'ian@starclubltd.com'
				);

			$this->_aGroups[GROUP_SALES] = array(
				'leah@starclubltd.com',
				'dionne@starclubltd.com',
				'hazel@starclubltd.com',
				'aahmek@starclubltd.com'
				);
		}

		public function setUser($p_sEmail){
			$this->_sEmail = $p_sEmail;
		}

		public function isAllowedAccess(){
			
			$l_aGroupsAll = array_merge($this->_aGroups[GROUP_ADMIN],$this->_aGroups[GROUP_SALES]);

			if(in_array($this->_sEmail,$l_aGroupsAll)){
				if($_SESSION['doconnect'] == 1){
					$this->sendOutPush('Dash LOGIN: '.$this->_sEmail);
				}
				unset($_SESSION['doconnect']);
				$this->_bAllowed = true;
			}else{
				if($_SESSION['doconnect'] == 1){
					$this->sendOutPush('Dash FAIL: '.$this->_sEmail);
				}
				unset($_SESSION['doconnect']);
				$this->_bAllowed = false;
			}

			return $this->_bAllowed;
		}

		public function isInGroup($p_sGroup){
			return in_array($this->_sEmail,$this->_aGroups[$p_sGroup]);
		}

		public function sendOutPush($p_sMessage){
			
			ob_start();
            $l_xSql = AppController::getInstance()->getConnection();
            $l_sLevel = '1';
            $l_sSql = 'SELECT * FROM users WHERE star_alert_level IN ('.$l_sLevel.') AND star_alert_device_id IS NOT NULL';
            $l_aData = $l_xSql->buildObject($l_xSql->runQuery($l_sSql));
            $l_aTokens = array();
            foreach($l_aData as $l_xRow){
                $l_aTokens[] = $l_xRow['star_alert_device_id'];
            }
            $l_sDomain = 'star-alert-prod';
            $l_xPush = new VTPush();
            $l_xPush->setCert($l_sDomain);
            $l_xPush->setMessage($p_sMessage);
            $l_xPush->setDeviceIds($l_aTokens);
            $l_xResponse = $l_xPush->pushOut();
            return ob_get_clean();
		}


		static public function timeago($time){
			
			$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
			$lengths = array("60","60","24","7","4.35","12","10");

			$now = time();

		   	$difference     = $now - $time;
		   	$tense         = "ago";

			for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			   $difference /= $lengths[$j];
			}

			$difference = round($difference);

			if($difference != 1) {
			   $periods[$j].= "s";
			}
			$l_sDisplay = "$difference $periods[$j] ago";
			return preg_replace("/ /","&nbsp;",$l_sDisplay);
		}

		static public function ssGetCleanNumberFormat($l_nVal){
			if($l_nVal < 1000)
				return $l_nVal;
			if($l_nVal < 1000000)
				return number_format($l_nVal/1000,1).'K';
	        // return number_format($l_nVal);
	        return number_format($l_nVal/1000/1000,2).'m';
		}
 	}
 
?>