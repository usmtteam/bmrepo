function ofloGoToLogin() {
    Livefyre.auth.login();
    return 0;
}

function livefyreInit() {
    var theApi = 'fyre.conv#3';
    var theApp = 'main';
    var theNetwork = 'starclub.fyre.co';
    var theSiteId = "379527";
    var theArticleId = 'stg-oflo-placeHolderArticleId';
    var theEl = 'livefyre-app-stg-oflo-placeHolderArticleId';
    
    Livefyre.require([theApi], function(Conv) {
        new Conv(
        { network: theNetwork },
        [{
            app: theApp,
            siteId: theSiteId,
            articleId: theArticleId,
            el: theEl,
        }],
        
        function (widget) {
            widget.on('userLoggedIn', function (data) {
                webkit.messageHandlers.userLoggedIn.postMessage(data);
            });
                 
            widget.on('userLoggedOut', function (data) {
                webkit.messageHandlers.userLoggedOut.postMessage(data);
            });
        });
    });
}

livefyreInit();
