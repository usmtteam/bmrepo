<?php

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

// Bitly link
if(!$_GET['p_sContentType']){

    if($_GET['p_sType']){

        if($_GET['video']){
            $l_sId = $_GET['video'];
            $l_sType = 'video';
        }elseif($_GET['photo']){
            $l_sId = $_GET['photo'];
            $l_sType = 'photo';
        }
        header('Location: /ocontent/'.$l_sType.'/'.$l_sId.'/');
    }
    exit;
}


require(__DIR__.'/../../config/settings.php');
require(__DIR__.'/init.php');
require(__DIR__.'/_db.php');

header( 'Content-type: text/html; charset=utf-8' );
flush();
?>

<?php include(__DIR__.'/_head.php'); ?>

<?

flush();
ob_flush();

switch($p_sPageType){
    case 'twitter_player': include(__DIR__.'/_twitter_content.php'); break;
    case 'twitter': include(__DIR__.'/_content.php'); break;
    default :
        include(__DIR__.'/_content.php'); break;
}

include(__DIR__.'/_footer.php');
?>
