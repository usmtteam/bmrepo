enriqueApp.controller('draftsController', ['$scope', '$rootScope', '$http', '$routeParams', function($scope,$rootScope, $http, $routeParams) {
	$scope.page = 1;
    $scope.feeds = [];
    $scope.star = {};
    $scope.is_loading = false;
    $scope.show_comment_box = true;
    //$scope.loadData();
    
    
    $scope.loadData = function () {
    	if($scope.is_loading) return;
    	CommonLib.show_loading_bubble();
    	$scope.is_loading= true;
    	var url = $rootScope.get_api_url() + '&option=get_main_feed&page='+$scope.page+'&comment_count=3&draft=1';
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
            	$rootScope.updateBrightCoveInfo(data);
        		$scope.star = data.star;
        		$scope.feeds = $scope.feeds.concat( data.feeds );
        		if(data.feeds.length > 0) $scope.page ++;
        	}
        	CommonLib.hide_loading_bubble();
        	
        	$scope.is_loading = false;
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();

        	$scope.is_loading = false;
        });
    };
}]);