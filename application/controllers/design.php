<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Design extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');


        $this->load->library('image_lib');
        
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
            return;
        }
        $this->load->helper('language');
        $this->load->model('main_m');

//        include(realpath(dirname(__FILE__).'/../../../front/include/lib/oauth/facebook/autoload.php'));

        $this->load->library('Facebook_Star');

//        require(LIB_PATH.'fb_persistent_data_interface.php');
//        $this->load->library('fb_persistent_data_interface');
        $this->load->library('CIFacebookPersistentDataHandler');

        $this->load->library('Twitter_Star');


        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter'), 
            $this->config->item('error_end_delimiter')
        );

        $this->chanel = $this->session->userdata('chanel');
        $this->data['chanel'] = $this->chanel;
        $this->data['show_errors'] = array();
        $user = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->session->set_userdata('email', $user->email);

        $this->set_slug_dependent_constants($user->email);
         $this->data['is_oflo'] =  $this->main_m->isChanelOflo();

    }
    
    function _remap($method) {
      
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
    }
    
    function index(){
      
        $designInfo = $this->main_m->getDesignInfo();
        if($this->input->post('del_header')){
            $this->main_m->delete_header_image();
            redirect("design", '');
        }
        if($this->input->post('del_watermark')){
            $this->main_m->delete_watermark_image();
            redirect("design", '');
        }
//        if($this->input->post('del_background')){
//            $this->main_m->delete_background_image();
//            redirect("design", '');
//        }
        if($this->input->post('del_logo')){
            $this->main_m->delete_logo_image();
            redirect("design", '');
        }
        if($this->input->post('del_banner')){
            $this->main_m->delete_banner_image();
            redirect("design", '');
        }

         if($this->input->post('del_chanel')){
            $this->main_m->delete_chanel_icon();
            redirect("design", '');
        }
        if($this->input->post('del_brightcove')){
            $this->main_m->delete_brightcove_image();
            redirect("design", '');
        }

        if($this->input->post('connect_facebook') || $this->input->get('code', TRUE)){

//            $facebook_accts = Facebook_Star::get_Facebook_Accounts();
            $fb = new Facebook\Facebook(
                array(
                    'app_id'  => FACEBOOK_APP_ID,
                    'app_secret' => FACEBOOK_APP_SECRET,
                    'default_graph_version' => 'v2.6',
                    'persistent_data_handler' => new CIFacebookPersistentDataHandler(),
                )
            );

            $fb_star = new Facebook_Star();

//            $facebook_accts = Facebook_Star::get_Facebook_Accounts($fb);
            $facebook_accts = $fb_star->get_Facebook_Accounts($fb);

//            echo "<br><br>\$facebook_accts ";
//            var_dump($facebook_accts);
//            exit;

            // Check if Facebook Accts
            if (array_key_exists('show_errors', $facebook_accts) === FALSE){

//                echo "<br><br>\$facebook_accts ";
//                var_dump($facebook_accts);
//
//                echo "<br><br>\$this->data['chanel']: ";
//                var_dump($this->data['chanel']);
//                exit;

                $facebook_accts_w_results = $this->main_m->add_Facebook_Accounts($this->data['chanel']['id'], $facebook_accts);

//                var_dump($facebook_accts_w_results);
//                exit;

                if ($facebook_accts_w_results[0]['result'] === TRUE){
                    $fb_flashdata = 'Successfully paired Facebook connection: ';
                    $fb_res_count = count($facebook_accts_w_results);

                    for ($ctr = 0; $ctr < $fb_res_count ; $ctr++){

                        if ($facebook_accts_w_results[$ctr]['result'] === TRUE){
                            $fb_flashdata .= $facebook_accts_w_results[$ctr]['name'];

                            if ($ctr !== $fb_res_count - 1){
                                $fb_flashdata .= ", ";
                            }
                        }

                    }

                    $fb_flashdata .= ". <BR>You may now manage your Facebook connection.";

                    $this->session->set_flashdata('facebook', $fb_flashdata);

                    redirect("design", '');

                }
//                elseif (!empty($facebook_accts_w_results['show_errors'])){
//                    $this->data['show_errors'][] = $facebook_accts_w_results['show_errors'];
//                    error_log("Cannot pair Facebook accounts at this time through design controller->Facebook_Star.");
//                }

            } else {
                $this->data['show_errors'][] = "Did not pair Facebook accounts at this time.";
                error_log("Did not pair Facebook accounts at this time through design controller->Facebook_Star.");

//                if (!empty($facebook_accts['show_errors'])){
                    $this->data['show_errors'][] = $facebook_accts['show_errors'];
//                    error_log("Cannot pair Facebook accounts at this time through design controller->Facebook_Star.");
//                }
            }

        }
    
    
        if ($this->input->post('connect_twitter')){


            if ($connect_to_twitter = Twitter_Star::connect_to_twitter()){

                if ($connect_to_twitter){
                  // $result =  $this->main_m->add_twitter_accounts($this->data['chanel']['id'], $connect_to_twitter);
                    if($result!=""){ redirect("design", ''); }
                    
                     }
                   
                
    
 }
//                exit;

            }
        if($this->input->get('oauth_token', TRUE) && $this->input->get('oauth_verifier', TRUE)){
   
           if ($connect_to_twitter = Twitter_Star::connect_to_twitter_with_oauth($this->input->get('oauth_token', TRUE),$this->input->get('oauth_verifier', TRUE))){

               if ($connect_to_twitter){
                  $result =  $this->main_m->add_twitter_accounts($this->data['chanel']['id'], $connect_to_twitter);
                    if($result!=""){ redirect("design", ''); }
               }
           }

        }
           // redirect("design", '');
    
        if($this->input->post('submit', false) === false){
            $this->data['post'] = array(
                'link_color' => isset($designInfo['link_color']) ? $designInfo['link_color'] : '000000',
                'button_color' => isset($designInfo['button_color']) ? $designInfo['button_color'] :'000000',
                'back_color' => isset($designInfo['back_color']) ? $designInfo['back_color'] : 'FFFFFF',
                'text_color' => isset($designInfo['text_color']) ? $designInfo['text_color'] : '000000',
                'header_image' => isset($designInfo['header_image']) ? $designInfo['header_image'] : "",
                'background_image' => isset($designInfo['background_image']) ? $designInfo['background_image'] : "",
                'logo_image' => isset($designInfo['logo_image']) ? $designInfo['logo_image'] : "",
                'banner_image' => isset($designInfo['banner_image']) ? $designInfo['banner_image'] : "",
                'chanel_icon' =>isset($designInfo['chanel_icon'])? $designInfo['chanel_icon']:"",
                'watermark' => isset($designInfo['watermark']) ? $designInfo['watermark'] : "",
                'brightcove' => isset($designInfo['brightcove']) ? $designInfo['brightcove'] : "",
                'bio_link' => isset($designInfo['bio_link']) ? $designInfo['bio_link'] : "",
                'site_link' => isset($designInfo['site_link']) ? $designInfo['site_link'] : "",
                'twitter_link' => isset($designInfo['twitter_link']) ? $designInfo['twitter_link'] : "",
                'facebook_link' => isset($designInfo['facebook_link']) ? $designInfo['facebook_link'] : "",
                'instagram_link' => isset($designInfo['instagram_link']) ? $designInfo['instagram_link'] : "",
                'channel_description' => isset($designInfo['channel_description']) ? $designInfo['channel_description'] : "",
                'channel_type' => isset($designInfo['channel_type']) ? $designInfo['channel_type'] : ""
            );            
        }else{
             $this->data['post'] = $_POST;

             $this->data['post']['header_image'] = isset($_POST['header_image']) ? $_POST['header_image'] : "";
             $this->data['post']['background_image'] = isset($_POST['background_image']) ? $_POST['background_image'] : "";
             $this->data['post']['logo_image'] = isset($_POST['logo_image']) ? $_POST['logo_image'] : "";
             $this->data['post']['chanel_icon']= isset($designInfo['chanel_icon'])? $_POST['chanel_icon']:"";
             $this->data['post']['banner_image'] = isset($_POST['banner_image']) ? $_POST['banner_image'] : "";
             $this->data['post']['watermark'] = isset($_POST['watermark']) ? $_POST['watermark'] : "";
             $this->data['post']['brightcove'] = isset($_POST['brightcove']) ? $_POST['brightcove'] : "";
             $this->data['post']['bio_link'] = isset($_POST['bio_link']) ? $_POST['bio_link'] : "";
             $this->data['post']['site_link'] = isset($_POST['site_link']) ? $_POST['site_link'] : "";
             $this->data['post']['twitter_link'] = isset($_POST['twitter_link']) ? $_POST['twitter_link'] : "";
             $this->data['post']['facebook_link'] = isset($_POST['facebook_link']) ? $_POST['facebook_link'] : "";
             $this->data['post']['instagram_link'] = isset($_POST['instagram_link']) ? $_POST['instagram_link'] : "";
             $this->data['post']['channel_description'] = isset($_POST['channel_description']) ? $_POST['channel_description'] : "";
             $this->data['post']['channel_type'] = isset($_POST['channel_type']) ? $_POST['channel_type'] : "";
             $this->main_m->saveDesignInfo($this->data['post']);
        }


        $pairs = Array();
        $this->data['post']['pairing'] = $this->main_m->list_facebook_accounts($this->chanel['id']);
        $pairs[] = $this->data['post']['pairing'];
        $newpairs = Array();
        $newpairs[] = $pairs[0][0];
        $newnewpairs = Array();
        $newnewpairs[] = $newpairs[0];

        $i=0;
        while($newnewpairs[0]['connected_pages'][$i]!=""){
                $this->data['post']['facebook'][$i] = $newnewpairs[0]['connected_pages'][$i];
                $id= $this->data['post']['facebook'][$i]['id'];
                //when "PAIR" button is press
                $pair = $this->input->post('facebook_pair');

                if($pair){
                    if($this->input->post($id)){
                      $this->main_m->facebook_pair_update($id, "Y");
                    }else{
                      $this->main_m->facebook_pair_update($id, "N"); 
                    }
                }         
            $i++;

      }
      if($pair==true){
        redirect("design", '');
      }
     
  
       $twpairs = Array();
       $twpairs[] = $pairs[0][1];
       $newtwpairs = Array();
       $newtwpairs[] = $twpairs[0];
       

        $i=0;
        while($newtwpairs[0]['connected_pages'][$i]!=""){
                $this->data['post']['twitter'][$i] = $newtwpairs[0]['connected_pages'][$i];
                $id= $this->data['post']['twitter'][$i]['id'];
                $pair2 = $this->input->post('twitter_pair');
                
                if($pair2){
                    if($this->input->post($id)){
                      $this->main_m->twitter_pair_update($id, "Y");
                    }else{
                      $this->main_m->twitter_pair_update($id, "N"); 
                    }
                }         
            $i++;

      }
      if($pair2==true){
        redirect("design", '');
      }
        


       $tmpairs = Array();
       $tmpairs[] = $pairs[0][2];

       $newtmpairs = Array();
       $newtmpairs[] = $tmpairs[0];
    

       $i=0;
        while($newtmpairs[0]['connected_pages'][$i]!=""){
                $this->data['post']['tumblr'][$i] = $newtmpairs[0]['connected_pages'][$i];
                $id= $this->data['post']['tumblr'][$i]['id'];
                $pair3 = $this->input->post('tumblr_pair');
                
                if($pair3){
                    if($this->input->post($id)){
                      $this->main_m->tumblr_pair_update($id, "Y");
                    }else{
                      $this->main_m->tumblr_pair_update($id, "N"); 
                    }
                }         
            $i++;

      }
      if($pair3==true){
        redirect("design", '');
      }
       

        $this->main_m->is_connected_to_facebook($this->data['post']['pairing']);
        $this->data['submenus'] = $this->main_m->getSubmenus($this->chanel['id']);
        $defaultSections = $this->main_m->getDefaultSections();
        $defaultSectionOptions = array();
        foreach($defaultSections as $defaultSection){
            $defaultSectionOptions[$defaultSection['id']] = $defaultSection['name'];
        }
        $this->data['defaultSectionOptions'] = $defaultSectionOptions;
        $this->_proc_add_design();

        $this->load->view('design', $this->data);
    }

    public function set_slug_dependent_constants($email){

      $SQL = 'SELECT cid FROM users WHERE email = "'.$email.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result();
      $cid = $rows[0]->cid;

      $SQL = 'SELECT slug FROM enr_chanel WHERE id = "'.$cid.'"';
      $query = $this->db->query($SQL);
      $rows = $query->result(); 
      $slug = $rows[0]->slug;

      define('PRODUCTION_LANDING' , 'http://'.$slug.'.on.starsite.com/');
       define('OFLO_LANDING' , 'http://'.$slug.'.on.oflo.co/');
      define('UPLOAD_AMZ', $slug.'/assets/ufile/');
      define('THEME_AMZ', $slug.'/images/');
      define('UPLOAD_AMZ_WATER', $slug.'/assets/ufile/watermark/');
      define('UPLOAD_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/assets/ufile/');
      define('THEME_CLOUD', 'http://d3hl6zmfu9utfu.cloudfront.net/'.$slug.'/images/');

      return;

      }
    
    private function _proc_add_design(){
        
        $data = array();
        
        if ($this->input->post('submit', false) !== false)
        {
            if($path = $_FILES['image2']['name']){
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                if(strtolower($ext)!='png'){
                    $this->data['show_errors'] = "Only png file is possible for watermark.";
                    return $data;
                }
            }
            if($this->main_m->saveDesignInfo($this->data['post'])){
//                redirect("design", '');
            }
        }
        return $data;
    }
    
//        $feed_id = $this->uri->segment(4);
//        $id = ltrim($feed_id, "text_");
//        if(empty($id)){
//            show_error("Select a post to edit!");
//            return;
//        }
//        if($this->input->post('submit', false) === false){
//            $FeedText = $this->main_m->getFeedText($id);
//            $this->data['post'] = array(
//                'feed_id' => $feed_id,
//                'cid' => $this->chanel['id'],
//                'description' => $FeedText['description'],
//                'tags' => $FeedText['tags'],
//                'credit' => $FeedText['credit'],
//                'is_publish' => $FeedText['is_publish']
//            );
//        }else{
//            $this->data['post'] = array(
//                'feed_id' => $feed_id,
//                'cid' => $this->chanel['id'],
//                'description' => $this->input->post('description'),
//                'tags' => $this->input->post('tags'),
//                'credit' => $this->input->post('credit'),
//                'is_publish' => $this->input->post('is_publish') == "on" ? 1 : 0
//            );
//        }
//        $this->();
//        $this->load->view('feed_text_edit', $this->data);

    /**
     * Delete Facebook account
     */
    public function delete_fb(){
        $facebook_id = $this->uri->segment(3, false);

        $result = $this->main_m->delete_facebook_account($this->chanel['id'], $facebook_id);

        redirect("design", '');
    }

}

