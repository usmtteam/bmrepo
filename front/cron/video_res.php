<?php // Get video resolution and transcode

//ini_set('memory_limit','1280M');
//ini_set('upload_max_filesize','100M');
ini_set('max_execution_time',100000);

require(__DIR__.'/../config/settings.php');

/*
|--------------------------------------------------------------------------
| Custom Define
|--------------------------------------------------------------------------
 */

define('RT_PATH', dirname(dirname(__DIR__)));
define('UPLOAD_DIR', RT_PATH.'/cms/assets/ufile/');

// Prints the day, date, month, year, time, AM or PM
echo "<BR>\n".date("l jS \of F Y h:i:s A");

$time_start = microtime(true); // Start calculating time
echo "<BR><BR>\$time_start: ".$time_start;
//exit;

$l_xDevTeamConnection =
    mysqli_connect('mysqldb.c0zphzzxzfwy.us-west-2.rds.amazonaws.com','scdevteam','scdevmysql2014','oflo_pro');

//$l_xDevTeamConnection =
//    mysqli_connect('localhost','root','root','stars');

$sql_video = 'SELECT "enr_video", ec.slug, ev.*
              FROM enr_video ev
              LEFT JOIN enr_chanel ec ON ec.id = ev.cid
              WHERE ec.slug IS NOT NULL
              AND (ev.video_type = 0
              OR ev.video_type IS NULL)
              AND (ev.video_height IS NULL
              OR ev.video_height = 0)
              ORDER BY ev.id ASC';

echo "<BR><BR>\n\n\$sql_video: ".$sql_video;

$result_video = mysqli_query($l_xDevTeamConnection, $sql_video);

//var_dump($result_video);
//exit;

//$result_video_main = array();

while ($l_xObj = mysqli_fetch_assoc($result_video)) {
//    $result_video_main[] = $l_xObj;

    echo "<BR>\n----------------------------------------------------------------------------------------------------";

    echo "<BR><BR>\n\n\$l_xObj: ";
    var_dump($l_xObj);
//    exit;

    // Get video width & height
    get_video_width_and_height($l_xDevTeamConnection, $l_xObj);
}

// Script End time
$time_end = microtime(true);

// dividing with 60 will give the execution time in minutes other wise seconds
$execution_time = ($time_end - $time_start)/60;

//execution time of the script
echo "<BR><BR>\n\n<b>Total Execution Time:</b> ".$execution_time." Mins";

$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//execution time of the script
echo "<BR><BR>\n\n\$time: ".$time;

// Prints the day, date, month, year, time, AM or PM
echo "<BR>\n".date("l jS \of F Y h:i:s A");

function get_video_width_and_height($l_xDevTeamConnection, $l_xResultRow){

    $slug = $l_xResultRow['slug'];
    echo "<BR><BR>\n\n\$slug: " . $slug;

    $destination = $l_xResultRow['destination'];

    if($destination) {

        $slug_video_path = "/" . $slug . "/assets/ufile/" . $destination;
        echo "<br><br>\n\n\$slug_video_path: " . $slug_video_path;

        $video_CDN_path = CDN_FRONT . $slug_video_path;
        echo "<br><br>\n\n\$video_CDN_path: " . $video_CDN_path;

        $path_parts = pathinfo($video_CDN_path);

        echo "<BR>\n";

        echo "<BR>\n\$path_parts['dirname']: ", $path_parts['dirname'];
        echo "<BR>\n\$path_parts['basename']: ",$path_parts['basename'];
        echo "<BR>\n\$path_parts['extension']: ",$path_parts['extension'];
        echo "<BR>\n\$path_parts['filename']: ",$path_parts['filename']; // since PHP 5.2.0

        $m3u8_CDN_path = $path_parts['dirname']."/".$path_parts['filename']."/";
        echo "<br><br>\n\n\$m3u8_CDN_path: " . $m3u8_CDN_path;

//                $pos = strrpos($destination, "/"); // position of last forward slash
//                echo "<BR><BR>\n\n\$pos of last forward slash: " . $pos;

//                $video_dir = substr($destination, 0, $pos + 1); // include last forward slash
//                echo "<BR><BR>\n\n\$video_dir: " . $video_dir;

        $pos = strrpos($destination, $path_parts['extension']); // position of last period
        echo "<BR><BR>\n\n\$pos of last forward slash: " . $pos;

        $m3u8_dir = substr($destination, 0, $pos - 1); // include last period
        $m3u8_dir .= "/"; // Concat forward-slash
        echo "<BR><BR>\n\n\$m3u8_dir: " . $m3u8_dir;

        if (!is_dir(UPLOAD_DIR . $m3u8_dir)) {
            mkdir(UPLOAD_DIR . $m3u8_dir, 0777, true);

            echo "<BR><BR>\n\nMade \$m3u8_dir: " . $m3u8_dir;
        } else {
            echo "<BR><BR>\n\n\$m3u8_dir : " . $m3u8_dir ." already exists";
        }

        // S3 master.m3u8
        $m3u8_path = file_get_contents($m3u8_CDN_path."master.m3u8");

        // Local dir
        $m3u8_filepath = UPLOAD_DIR . $m3u8_dir."master.m3u8";
        echo "<BR>\n\$m3u8_filepath: ".$m3u8_filepath;

        // Save master.m3u8 locally
        if (!file_put_contents($m3u8_filepath, $m3u8_path)) {
            echo "<BR><BR>\n\n<b>Error: </b>Could not put master.m3u8 file to " . $m3u8_filepath;
        } else {
            echo "<BR>\n**************************************************************************************************************";
            echo "<BR><BR>\n\nPut master.m3u8 file to " . $m3u8_filepath;

            $master_line = NULL;
            $master_line_number = false;

            // Open master.m3u8 file
            if ($handle = fopen($m3u8_filepath, "r")) {
                $count = 0;

                // Read lines and find first master.m3u8
                while (($master_line = fgets($handle, 4096)) !== FALSE and !$master_line_number) {
                    $count++;
                    $master_line_number = (strpos($master_line, "file-") !== FALSE) ? $count : $master_line_number;
                    echo "<BR>\n\$master_line: ".$master_line;

//                  echo "<BR>\n\$master_line_number: ";
//                  var_dump($master_line_number);

                    // Found first file-#.m3u8
                    if ($master_line_number){

                        // Trim whitespaces
                        $master_line = trim($master_line);
                        echo "<BR>\n\$master_line: ";
                        var_dump($master_line);

                        echo "<BR>\nFound \$master_line: ".$master_line." on line #: ".$master_line_number;

                        // S3 file-#.m3u8
                        $file_num_m3u8_path = file_get_contents($m3u8_CDN_path.$master_line);

                        // Local dir
                        $file_num_m3u8_filepath = UPLOAD_DIR . $m3u8_dir.$master_line;
                        echo "<BR>\n\$file_num_m3u8_filepath: ".$file_num_m3u8_filepath;

                        // Save file-#.m3u8 locally
                        if (!file_put_contents($file_num_m3u8_filepath, $file_num_m3u8_path)) {
                            echo "<BR><BR>\n\n<b>Error: </b>Could not put ".$master_line." file to " . $file_num_m3u8_filepath;
                        } else {
                            echo "<BR>\n#################################################################################################################";
                            echo "<BR><BR>\n\nPut ".$master_line." file to " . $file_num_m3u8_filepath;

                            $file_line = NULL;
                            $file_line_number = false;

                            // Open file-#.m3u8
                            if ($handle = fopen($file_num_m3u8_filepath, "r")) {
                                $count = 0;

                                // Read lines and find "file-"
                                while (($file_line = fgets($handle, 4096)) !== FALSE and !$file_line_number) {
                                    $count++;
                                    $file_line_number = (strpos($file_line, "file-") !== FALSE) ? $count : $file_line_number;
                                    echo "<BR>\n\$file_line: ".$file_line;

//                                  echo "<BR>\n\$file_line_number: ";
//                                  var_dump($file_line_number);

                                    // Found "file-"
                                    if ($file_line_number){

                                        // Trim whitespaces
                                        $file_line = trim($file_line);
                                        echo "<BR><BR>\n\ntrim($file_line): ";
                                        var_dump($file_line);

                                        echo "Found \$file_line: ".$file_line." on line #: ".$file_line_number;

                                        // S3 file-#.ts
                                        $ts_path = file_get_contents($m3u8_CDN_path.$file_line);

                                        // Local dir
                                        $ts_filepath = UPLOAD_DIR . $m3u8_dir.$file_line;
                                        echo "<BR>\n\$ts_filepath: ".$ts_filepath;

                                        // Save file-#.ts locally
                                        if (!file_put_contents($ts_filepath, $ts_path)) {
                                            echo "<BR><BR>\n\n<b>Error: </b>Could not put ".$file_line." to " . $ts_path;
                                        } else {

                                            // ffprobe
                                            $cmd = "/usr/bin/ffprobe -v quiet -of flat -select_streams v:0 -show_entries stream=width,height,duration ".$ts_filepath." | sed -n 's/streams.stream.0.//p'";
                                            exec($cmd, $output, $cvh_return_var);

                                            echo "<BR>\n\$cmd: ".$cmd;
                                            echo "<BR>\n\$output: ";
                                            var_dump($output);
                                            echo "<BR>\n\$cvh_return_var: ".$cvh_return_var;

                                            $output_implode = implode("&", $output);
                                            $output_implode = str_replace('"', '', $output_implode);
                                            parse_str($output_implode, $output_parsed);

                                            $file['video_width']    = $output_parsed['width'];
                                            $file['video_height']   = $output_parsed['height'];
                                            $file['video_duration'] = $output_parsed['duration'];

                                            // Store video res of video ID.
                                            $l_sSql = "UPDATE enr_video
                                                       SET video_width = ".$output_parsed['width'].",
                                                       video_height = ".$output_parsed['height']."
                                                       WHERE id = ".$l_xResultRow['id'];

                                            echo "<BR><BR>\n\n\$l_sSql: ".$l_sSql;

                                            if (!empty($file_line) && !empty($output_parsed['width']) && !empty($output_parsed['height'])){
                                                echo "<BR>\nRun query: ";
                                                $result_video = mysqli_query($l_xDevTeamConnection, $l_sSql);
                                            } else {
                                                echo "<BR>\nDon't run query: ";
                                            }
                                        }

                                        echo "<BR><BR>\n\n\$result_video: ";
                                        var_dump($result_video);

                                        @unlink($ts_filepath);

                                        break;
                                    } // end of Save file-#.ts locally
                                } // end of Read lines and find "file-"
                                fclose($handle);
                            } // end of Open file-#.m3u8

                        } // end of Save file-#-m3u8 locally

                        @unlink($file_num_m3u8_filepath);

                        break;
                    } // end of Found first file-#.m3u8
                } // end of Read lines and find first file-#.m3u8
                fclose($handle);
            } // end of Open master.m3u8 file

        } // end of Save master.m3u8 locally

        @unlink($m3u8_filepath);

//                exit;

    } // end of if($destination) {
}

?>