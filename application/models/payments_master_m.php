<?php


//include('Endroid/Gcm/Gcm.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments_Master_M extends CI_Model
{
    public function __construct()
    {
        $this->errors = array();
        parent::__construct();
        $this->controller = get_instance();
        $homepage = "/index.php";
        if ($this->ion_auth->logged_in() && $_SERVER['REQUEST_URI']==$homepage)
        {
            $this->chanel = $this->controller->session->userdata('chanel');

            $this->chanel = $this->get_chanel($this->chanel);
        }


        $this->load->library('Utils');
        $this->load->library('Memcaching');
        $this->load->library('Gastar');
        $this->load->library('SCAPI');
        $this->load->library('Facebook_Star');
        $this->load->library('Twitter_Star');
        require_once(RT_PATH.'/application/config_f/constants.php');
        require_once(LIB_PATH.'/../AppController.php');
        require_once(LIB_PATH.'front/FrontCoreManager.php');
        require_once(LIB_PATH.'front/FrontCoreSql.php');
        require_once(LIB_PATH.'channel/Channel.php');
        require_once(LIB_PATH.'channel/ChannelManager.php');
        require_once(LIB_PATH.'oauth/facebook/facebook.php');
        require_once(TWITTER_OAUTH_CLASS);



    }

     

    function get_reports_by_id($id, $month_input){

     $sql = "SELECT *, SUM(views) as total_views, SUM(revenue) as total_revenue, SUM(video_duration) as total_duration FROM

            (SELECT orm.content_id, orm.video_views as views, orm.revenue, orm.month, ev.video_duration, ev.description, ev.is_premium
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             
             ";


        if($month_input == 'all' || $month_input == 'by_months'){
            $sql .= " WHERE channel_id = $id AND is_mobile = 1
             
             UNION
             
             SELECT orm.content_id, orm.page_views_ga as views, orm.revenue, orm.month, ev.video_duration, ev.description, ev.is_premium
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             WHERE channel_id = $id AND is_mobile = 0

             ) as tbl1";
        }else {
            $sql .=" WHERE channel_id = $id AND is_mobile = 1 AND month = '$month_input'
             
             UNION
             
             SELECT orm.content_id, orm.page_views_ga as views, orm.revenue, orm.month, ev.video_duration, ev.description, ev.is_premium
             FROM oflo_reports_monthly orm
             LEFT JOIN enr_video ev ON ev.id = orm.content_id
             WHERE channel_id = $id AND is_mobile = 0 AND month = '$month_input'

             ) as tbl1";
        }
    


        if (isset($_POST['premiumCheckbox'])){
            if($month_input == 'by_months'){
                $sql .= " WHERE tbl1.is_premium = 1
                            GROUP BY month";

                }else{
                $sql .= " WHERE tbl1.is_premium = 1
                            GROUP BY content_id";
            }
        }else {

            if($month_input == 'by_months'){
                $sql .=" GROUP BY month";
            }else{

                $sql .=" GROUP BY content_id";
            }
        }
    

     $query = $this->db->query($sql);
     $rows = $query->result_array();


      return $rows;    

    }



   
   
}