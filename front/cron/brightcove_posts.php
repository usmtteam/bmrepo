<?


    ini_set('memory_limit','1280M');
    ini_set('upload_max_filesize','100M');
    ini_set('max_execution_time',100000);
    require(__DIR__.'/../config/settings.php'); 

    
    $l_xDevTeamConnection = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);    
    
    $l_sSql = 'SELECT * FROM enr_video WHERE is_publish = 1 AND fan_id < 0 AND  LENGTH(brightcove_media_id) > 4';
    $l_xResult = mysqli_query($l_xDevTeamConnection,$l_sSql);

    $l_aResultsMain = array();
    while ($l_xObj = mysqli_fetch_array($l_xResult,MYSQLI_ASSOC)) {
        $l_aResultsMain[] = $l_xObj;
    } 
    
    foreach($l_aResultsMain as $l_xRow){

        $url = "https://data.brightcove.com/analytics-api/videocloud/accounts/2866239739001/report/?dimensions=video";
        $headers = array('Authorization: Bearer 153bd49b6e4b00b61fe97fba3');
        $start_time = 1000 * strtotime($l_xRow['start_date']);
        $now = date("Y-m-d h:s:i");
        $end_time = 1000 * strtotime($now);
        $query = "where=video==".$l_xRow['brightcove_media_id']."&from={$start_time}&to={$end_time}";

        $crl = curl_init();
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $query);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($crl);
        curl_close($crl);
        $val = json_decode($return);

    
            $api_url = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$l_xRow['brightcove_media_id'].'&video_fields=name,length&token=fordwEkkX_OySh6vDyBg0mfNwYAR8Vedac_c3xVsPI0aa375XbymHQ..';
            $json = file_get_contents($api_url);
            $video = json_decode($json);
            
            
            $l_sSql = 'INSERT INTO enr_brightcove_video_metrics 
                        (video_id,content_id,video_length,engagement_score,video_engagement_1, 
                            video_engagement_25,video_engagement_50,video_engagement_75, 
                            video_engagement_100,video_seconds_viewed,video_percent_viewed, 
                            video_impression,video_view,play_rate)

                    VALUES (
                        "'.$l_xRow['brightcove_media_id'].'",
                        "'.$l_xRow['id'].'",
                        "'.$video->length.'",
                        "'.$val->summary->engagement_score.'",
                        "'.$val->summary->video_engagement_1.'",
                        "'.$val->summary->video_engagement_25.'",
                        "'.$val->summary->video_engagement_50.'",
                        "'.$val->summary->video_engagement_75.'",
                        "'.$val->summary->video_engagement_100.'",
                        "'.$val->summary->video_seconds_viewed.'",
                        "'.$val->summary->video_percent_viewed.'",
                        "'.$val->summary->video_impression.'",
                        "'.$val->summary->video_view.'",
                        "'.$val->summary->play_rate.'"
                        )

                    ON DUPLICATE KEY UPDATE 
                        video_length = VALUES(video_length),
                        engagement_score = VALUES(engagement_score),
                        video_engagement_1 = VALUES(video_engagement_1),
                        video_engagement_25 = VALUES(video_engagement_25),
                        video_engagement_50 = VALUES(video_engagement_50),
                        video_engagement_75 = VALUES(video_engagement_75),
                        video_engagement_100 = VALUES(video_engagement_100),
                        video_seconds_viewed = VALUES(video_seconds_viewed),
                        video_percent_viewed = VALUES(video_percent_viewed),
                        video_impression = VALUES(video_impression),
                        video_view = VALUES(video_view),
                        play_rate = VALUES(play_rate)';

            mysqli_query($l_xDevTeamConnection,$l_sSql);
            
    }


?>