function ofloGoToLogin() {
    Livefyre.auth.login();
    return 0;
}

function livefyreInit() {
    var theApi = 'fyre.conv#3';
    var theApp = 'main';
    var theNetwork = 'starclub.fyre.co';
    var theSiteId = "379527";
    var theArticleId = 'stg-oflo-placeHolderArticleId';
    var theEl = 'livefyre-app-stg-oflo-placeHolderArticleId';
    
    Livefyre.require([theApi], function(Conv) {
        new Conv(
        { network: theNetwork },
        [{
            app: theApp,
            siteId: theSiteId,
            articleId: theArticleId,
            el: theEl,
        }],
        
        function (widget) {
            widget.on('commentPosted', function (data) {
                webkit.messageHandlers.commentPosted.postMessage(data);
            });
                 
            widget.on('commentFlagged', function (data) {
                webkit.messageHandlers.commentFlagged.postMessage(data);
            });
                 
            widget.on('commentLiked', function (data) {
                webkit.messageHandlers.commentLiked.postMessage(data);
            });
                 
            widget.on('commentShared', function (data) {
                webkit.messageHandlers.commentShared.postMessage(data);
            });
                 
            widget.on('commentCountUpdated', function (data) {
                jQuery.ajax({
                    type: "GET",
                    url: "http://api.oflo.co/commentcount.php",
                    data: {data:data, id:placeHolderArticleId},
                    success: function(data) {}
                });
                      
                webkit.messageHandlers.commentCountUpdated.postMessage(data);
            });
                 
            widget.on('userLoggedIn', function (data) {
                webkit.messageHandlers.userLoggedIn.postMessage(data);
            });
                 
            widget.on('userLoggedOut', function (data) {
                webkit.messageHandlers.userLoggedOut.postMessage(data);
            });
                 
            widget.on('socialMention', function (data) {
                webkit.messageHandlers.socialMention.postMessage(data);
            });
                 
            widget.on('commentFeatured', function (data) {
                webkit.messageHandlers.commentFeatured.postMessage(data);
            });
                 
            widget.on('initialRenderComplete', function (data) {
                webkit.messageHandlers.initialRenderCompleteComments.postMessage(data);
            });
                 
            widget.on('showMore', function (data) {
                webkit.messageHandlers.showMore.postMessage(data);
            });
        });
    });
}

livefyreInit();