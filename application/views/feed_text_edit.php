<!-- BEGIN PAGE -->
<div class="page-content">
    
    <div class="container-fluid">
        <div class="row-fluid">
           <div class="span12">
            <div class="page-title">
  Text Editor
                </div>

<!-- BEGIN SAMPLE FORM PORTLET-->   
              <div class="portlet box">
                    <div id="form_head_back">Edit Text</div>
                 <div class="portlet-body form pb-feed">
                    <?php 
//                    echo form_open_multipart("content/feed/text_edit/".$post['feed_id'], 'class="form-horizontal"');
                    echo form_open_multipart("", 'class="form-horizontal"');
                        echo form_error("description");
                        echo form_error("credit");
                        if(isset($show_errors)) {
                            if (is_array($show_errors)) {
                                foreach($show_errors as $error) {
                                    echo "<div class='alert alert-error'>".$error."</div>";
                                }
                            }
                            else{
                                echo "<div class='alert alert-error'>".$show_errors."</div>";
                            }
                        }
                    ?>
                        <div class="control-group">
                            <label class="control-label" for="url">Text Post</label>
                            <div class="controls">
                            <?php  echo form_textarea("description",$post['description'],'class="span6 m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Tags</label>
                            <div class="controls">
                            <?php  echo form_input("tags",$post['tags'],'class="span6 m-wrap"');?>
                            </div>
                        </div>
                        <?php if(!$is_community): ?>
                        <div class="control-group">
                            <label class="control-label" for="url">Credits</label>
                            <div class="controls">
                            <?php  echo form_input("credit",$post['credit'],'class="large m-wrap"');?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="control-group">
                            <label class="control-label" for="url">Permission</label>
                            <div class="controls">
								<?php echo form_dropdown("is_publish", array('Not Published', 'Publish', 'Draft'), $post['is_publish'], 'class="large m-wrap"');?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="url">Sticky Flag</label>
                            <div class="controls">
                            <?php  echo form_checkbox("is_sticky", 1, $post['is_sticky'],'class=""');?>
                            </div>
                        </div>
                        
	                        <div class="control-group">
	                            <label class="control-label" for="url">Post time</label>
	                            <div class="controls">
		                            <input type="checkbox" id="manual_time" name="manual_time" value="1" />
		                            Set new post time
		                            <div id="timestamp_div" style="display:none;" class="input-append">
		                            	<input type="text" id="time_stamp" name="time_stamp" value="<?php echo date('Y-m-d H:i:s'); ?>" />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
		                            <script language="javascript">
										$(document).ready(function() {
											$('#timestamp_div').datetimepicker({
										        format: 'yyyy-MM-dd hh:mm:ss'
										     });
											$('#manual_time').change(function(e) {
												$('#timestamp_div').toggle(this.checked);
											});
										});
		                            </script>
	                            </div>
	                        </div>
                            
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Start date</label>
                                <div class="controls">
                                	<div id="start_date_div" class="input-append">
		                            	<input type="text" id="start_date" name="start_date" value="<?php echo $post['start_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="url">End date</label>
                                <div class="controls">
                                	<div id="end_date_div" class="input-append">
		                            	<input type="text" id="end_date" name="end_date" value="<?php echo $post['end_date']; ?>" readonly />
		                            	<span class="add-on">
        									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      									</span>
		                            </div>
                                </div>
                            </div>
                            <script language="javascript">
								$(document).ready(function() {
									$('#start_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
									$('#end_date_div').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'});
								});
							</script>
							
                            
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(Web app)</label>
                                <div class="controls">
                                	<a href="<?php echo get_web_app_link($chanel_info, 'text', $post['id']); ?>" target="_blank">
                                	<?php echo get_web_app_link($chanel_info, 'text', $post['id']); ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(iOS, Android app)</label>
                                <div class="controls">
                                	<a href="<?php echo get_mobile_app_link($chanel_info, 'text', $post['id']); ?>" target="_blank">
                                	<?php echo get_mobile_app_link($chanel_info, 'text', $post['id']); ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL(bit.ly)</label>
                                <div class="controls">
                                	<a href="<?php echo $post['deep_link_web']; ?>" target="_blank">
                                	<?php echo $post['deep_link_web']; ?>
                                	</a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="url">Deep Link URL Twitter(bit.ly)</label>
                                <div class="controls">
                                    <a href="<?php echo $post['deep_link_web_tw']; ?>" target="_blank">
                                    <?php echo $post['deep_link_web_tw']; ?>
                                    </a>
                                </div>
                            </div>
                        <div class="form-actions">
                            <?php echo form_submit('submit', "Post",' class="btn blue"');?>
                        </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                 </div>
                 
                 
                 
                    <div id="form_head_back">Comments</div>
                 
                 <div class="portlet-body form pb-feed">
                 	<table class="table table-bordered table-hover bootstrap-datatable datatable">
		                    <thead>
		                        <tr>
		                            <th style="width:30px;">No</th>
		                            <th style="width:200px;">Username</th>
		                            <th>Comments</th>
		                            <th style="width:120px;">Date</th>
		                        </tr>
		                    </thead>
		                    <tbody> 
		                    <?php 
		                     $post_type = explode('_', $post['feed_id']);
		                     $post_type = $post_type[0];
		                     foreach($comments as $i => $comment) :?>
		                    	<tr>
		                    		<td><?php echo $i+1; ?></td>
		                    		<td><a href="<?php echo site_url('fan/edit_user/'.$comment['user_id']).'?ref='.site_url('content/feed/'.$post_type.'_edit/'.$post['feed_id']); ?>"><?php echo $comment['name']; ?></a></td>
		                    		<td><?php echo $comment['comment']; ?></td>
		                    		<td><?php echo date('Y-n-j', $comment['time_stamp']); ?></td>
		                    	</tr>
		                    <?php endforeach; ?>
		                    </tbody>
						</table>
                 </div>
              </div>
              <!-- END SAMPLE FORM PORTLET-->
              </div>
        </div>
    </div>
</div>
