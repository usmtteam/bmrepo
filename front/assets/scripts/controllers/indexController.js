var l_aArgs = jQuery.parseJSON(Base64.decode(g_sAngularSettings));

enriqueApp.controller('indexController',  ['$scope', '$rootScope', '$modal', '$cookieStore', '$http', '$upload', '$location', '$sce', function($scope, $rootScope, $modal, $cookieStore, $http, $fileUploader, $location, $sce) {
    
	
	
	$rootScope.WEBSITE_URL = l_aArgs.WEBSITE_URL;
	$rootScope.SERVER_URL = l_aArgs.SERVER_URL;
	$rootScope.CHANNEL = l_aArgs.CHANNEL;
	$rootScope.get_api_url = function() { return $rootScope.SERVER_URL + '/api?cid=' + $rootScope.CHANNEL + ($rootScope.user_token != null && $rootScope.user_token != '' ? '&token=' + $rootScope.user_token + '&user_id=' + $rootScope.current_user.id : ''); };
	$rootScope.current_user = null;
	$rootScope.user_token = '';
	
	
	//bit.ly info
	$rootScope.bitly_login = l_aArgs.BITLY_LOGIN;
	$rootScope.bitly_apikey = l_aArgs.BITLY_APIKEY;
	//brightcove info
	$rootScope.BRIGHTCOVE_PLAYER_ID = l_aArgs.BRIGHTCOVE_PLAYER_ID;
	$rootScope.AD_SERVER_URL = '';
	$rootScope.BRIGHTCOVE_READ_API_KEY = l_aArgs.BRIGHTCOVE_READ_API_KEY;
	$rootScope.BRIGHTCOVE_WRITE_API_KEY = l_aArgs.BRIGHTCOVE_WRITE_API_KEY;
	//Paypal
	$rootScope.ACCEPT_CREDITCARD = false;
	
	$rootScope.Math = window.Math;
	$rootScope.Date = window.Date;
	$rootScope.CommonLib = window.CommonLib;
	$rootScope.FB = window.FB;
	$rootScope.brightcove = window.brightcove;
	$rootScope.Handlebars = window.Handlebars;
	
	$rootScope.music_id = 0;
	$rootScope.music_file = '';
	$rootScope.music_play = false;
	
	
	
//	enriqueApp.config(function($sceDelegateProvider, apiUrl) {
//		$sceDelegateProvider.resourceUrlWhitelist(['self',
//		                                           $rootScope.SERVER_URL +   /
//		                                           ]);
//	});
	
	
	$rootScope.current_page = 'homefeed';
	
	$rootScope.dialogLogin = null;
	$rootScope.dialogSignup = null;
	$rootScope.dialogLoginNow = null;
	$rootScope.unlockConfirmDialog = null;
	
	$rootScope.isCDNData = function(url) {
		if(url.indexOf('youtu') == -1) {
			return true;
		}
		
		return false;
	};
	$rootScope.updateBrightCoveInfo = function(data) {
		
		if(typeof data != undefined && data.channel_info.bc_playerid_web != "") {
			$rootScope.BRIGHTCOVE_PLAYER_ID = data.channel_info.bc_playerid_web; 
		}
		if(data.channel_info.ad_server_url_web != "") {
			$rootScope.AD_SERVER_URL = data.channel_info.ad_server_url_web;
		}
	}
	$rootScope.getBitLyUrl = function (longUrl) {
		longUrl = encodeURIComponent(longUrl);
		return $.ajax({
	        type: "GET",
	        url: 'http://api.bit.ly/v3/shorten?login='+$rootScope.bitly_login + '&apikey='+$rootScope.bitly_apikey+'&longUrl='+longUrl+'&format=txt',
	        async: false
	    }).responseText;
	};
	
	$rootScope.$on("$locationChangeSuccess", function (angularEvent, newUrl, oldUrl) {
		$rootScope.current_page = $location.$$path.replace('/', '');
	});
	$rootScope.isDate = function (val) {
	    var d = new Date(val);
	    return !isNaN(d.valueOf());
	}
	$rootScope.onOpenMusicPlayer = function() {
		$rootScope.dialogMusicPlayer = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'login_music_player',
		    resolve: {
		    },
			templateUrl: '/views/music_player.html',
			controller: 'musicPlayerController'
		});
	};
	$rootScope.onShowLoginButton_click = function() {
		$rootScope.dialogLogin = $modal.open({
		    backdrop: 'static',
		    keyboard: false,
		    backdropClick: false,
		    windowClass: 'login_dialog',
		    resolve: {
		    },
			templateUrl: '/views/login.html',
			controller: 'loginController'
		});
	};
	$rootScope.onShowSignupButton_click = function() {
		$rootScope.dialogSignup = $modal.open({
		    backdrop: 'static',
		    keyboard: false,
		    backdropClick: false,
		    windowClass: 'signup_dialog',
		    resolve: {
		    },
			templateUrl: '/views/signup.html',
			controller: 'signupController'
		});
	};
	$rootScope.onShowSignupButton1_click = function() {
		if(!$('#checkbox_accept_terms').is(':checked')) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Accept terms and conditions',
				description: 'Please accept terms and conditions to sign up.',
				onOk : function() {
				}
			});
			return;
		}
		
		if($rootScope.dialogLogin != null) $rootScope.dialogLogin.close();
		
		$rootScope.onShowSignupButton_click();
	};

	$rootScope.onShowSignInNow_click = function() {

		if(!$('#checkbox_accept_terms').is(':checked')) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Accept terms and conditions',
				description: 'Please accept terms and conditions to sign in.',
				onOk : function() {
				}
			});
			return;
		}
		
		if($rootScope.dialogLogin != null) $rootScope.dialogLogin.close();

		$rootScope.dialogLoginNow = $modal.open({
		    backdrop: 'static',
		    keyboard: false,
		    backdropClick: false,
		    windowClass: 'login_email_dialog',
		    resolve: {
		    },
			templateUrl: '/views/login_email.html',
			controller: 'loginEmailController'
		});
	};
	
	$rootScope.openPaypalDialog = function(url) {
//		$rootScope.dialogPaypal = $modal.open({
//		    backdrop: 'static',
//		    keyboard: false,
//		    backdropClick: false,
//		    windowClass: 'paypal_dialog',
//		    resolve: {
//		    	url: function() { return url; }
//		    },
//			templateUrl: '/views/paypal_payment.html',
//			controller: 'paypalPaymentController'
//		});
		document.location = url;
	};
	
	$rootScope.onLogout = function() {
		$rootScope.current_user = null;
		$rootScope.user_token = '';
		
		$rootScope.saveCookie();
		
		$rootScope.onShowLoginButton_click();
	};
	
	$rootScope.saveCookie = function() {
		$cookieStore.put('current_user', $rootScope.current_user );
		$cookieStore.put('user_token', $rootScope.user_token );
	};
	$rootScope.loadCookie = function() {
		$rootScope.current_user = $cookieStore.get('current_user');
		$rootScope.user_token = $cookieStore.get('user_token');
		
		if(typeof $rootScope.current_user == undefined) { $rootScope.current_user = null; $rootScope.user_token = null; }
		if(typeof $rootScope.user_token == undefined) { $rootScope.current_user = null; $rootScope.user_token = null; }
	};
	
	
	$rootScope.addComment = function(e, feeds) {
		if(e.which != 13) return;
		
		var input = $(e.target);
		var content_id = input.attr("data-content-id");
		var post_type = input.attr('data-post-type');
		var comment = input.val();
		
		input.attr('disabled', 'disabled');
		$rootScope.CommonLib.show_waiting_circle(input);
		//post comment
		$http({method: 'GET', url: $rootScope.get_api_url() + '&option=set_comment&user_id=' + $rootScope.current_user.id + '&post_type=' + post_type + '&content_id=' + content_id + '&comment=' + comment+'&comment_count=3'}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		var i;
        		for(i = 0; i < feeds.length; i++) {
        			if(feeds[i].content_id == content_id && feeds[i].post_type == post_type) {
        				//reload comments
        		        feeds[i].comments = data.comments;

						feeds[i].comments_count = data.comments_count;
        		        break;
        			}
        		}
        		
        		input.val('');
        	}
        	else {
        	}
        	
        	input.removeAttr('disabled');
        	$rootScope.CommonLib.hide_waiting_circle();
        }).
        error(function(data, status, headers, config) {
        	$scope.error = 'Please try again.';
        });
	};
	
	$rootScope.onDeleteComment = function(e, feeds) {

		$rootScope.showConfirmDialog({
			type: 'CONFIRM',
			title: 'Delete Comment',
			description: 'Do you want to delete this Comment?',
			onOk : function() {
				var input = $(e.target);
				var content_id = input.attr("data-content-id");
				var post_type = input.attr('data-post-type');
				var comment_id = input.attr('data-comment-id');
				
				$http({method: 'GET', url: $rootScope.get_api_url() + '&option=delete_comment&post_type=' + post_type + '&content_id=' + content_id + '&comment_id=' + comment_id+'&comment_count=3'}).
		        success(function(data, status, headers, config) {
		        	if(data.status == true) {
		        		var i, j;
		        		for(i = 0; i < feeds.length; i++) {
		        			if(feeds[i].content_id == content_id && feeds[i].post_type == post_type) {
		        				//reload comments
		        		        feeds[i].comments = data.comments;

								feeds[i].comments_count = data.comments_count;
		        		        break;
		        			}
		        		}
		        	}
		        	else {
		        	}
		        }).
		        error(function(data, status, headers, config) {
		        	$scope.error = 'Please try again.';
		        });
			},
			onCancel : function (){
				
			}
		});
	};
	
	$rootScope.onDeletePost = function(content_id, post_type, feeds) {
		$rootScope.showConfirmDialog({
			type: 'CONFIRM',
			title: 'Delete Post',
			description: 'Do you want to delete this Post?',
			onOk : function() {
				$http({method: 'GET', url: $rootScope.get_api_url() + '&option=delete_feed&post_type=' + post_type + '&content_id=' + content_id}).
		        success(function(data, status, headers, config) {
		        	if(data.status == true) {
		        		var i, j = -1;
		        		for(i = 0; i < feeds.length; i++) {
		        			if(feeds[i].post_type == post_type && feeds[i].content_id == content_id) {
		        				j = i;
		        				break;
		        			}
		        		}
		        		if(j >= 0) {
		        			feeds = feeds.splice(j, 1);
		        		}
		        	}
		        	else {
		        	}
		        }).
		        error(function(data, status, headers, config) {
		        	$scope.error = 'Please try again.';
		        });
			},
			onCancel : function (){
				
			}
		});
	};
	

	$rootScope.viewAllComments = function(e, feeds) {
		var target = $(e.target);
		var content_id = target.attr('data-content-id');
		var comment_count = target.attr('data-comment-count');
		
		var show_all = false;
		var i;
		for(i = 0; i < feeds.length; i++) {
			if(feeds[i].content_id == content_id) {
				CommonLib.show_loading_bubble();
				var url = $rootScope.get_api_url() + '&option=get_comment&user_id='+($rootScope.current_user != null ? $rootScope.current_user.id : '') + '&post_type=' + feeds[i].post_type + '&content_id=' + feeds[i].content_id;
				$http({method: 'GET', url: url}).
			    success(function(data, status, headers, config) {
			    	CommonLib.hide_loading_bubble();
			    	if(data.status == true) {
			    		feeds[i].comments = data.comments;

						show_all = feeds[i].show_all_comment;
						if(show_all == null || typeof show_all == 'undefined') show_all = false;
						
						show_all = !show_all;
						feeds[i].show_all_comment = show_all;
						

						if(show_all) {
							target.html('Hide comments');
						}
						else {
							target.html('View all ' + comment_count + ' comments');
						}
			    	}        	
			    }).
			    error(function(data, status, headers, config) {
			    	CommonLib.hide_loading_bubble();
			    });
				
				break;
			}
		}
	};


	
	$rootScope.onViewPostDetail = function(e, show, post, star, detail_only, auto_play, view_m_comments) {
		if(show === false) return;
		
		//var target = $(e.target);
		
//		if($rootScope.dialogViewPost != null) $rootScope.dialogViewPost.close();
		if((post.post_type == 'poll' || post.post_type == 'quiz') && detail_only !== true) {
			if(post.answered == true) {
				$rootScope.showConfirmDialog({
					type: 'ALERT',
					title: 'Poll and Quiz',
					description: 'You have already answered to this ' + (post.post_type == 'poll' ? 'Poll' : 'Quiz') + '.',
					onOk : function() {
						
					}
				});
			}
			else {
				$rootScope.dialogViewPost = $modal.open({
				    backdrop: true,
				    keyboard: true,
				    backdropClick: false,
				    windowClass: 'poll_quiz_dialog',
				    resolve: {
				    	post: function() { return post; },
				    	star: function() { return star; }
				    },
					templateUrl: '/views/poll_quiz.html',
					controller: 'pollQuizController'
				});
			}
		}
		else {
			$rootScope.dialogViewPost = $modal.open({
			    backdrop: true,
			    keyboard: true,
			    backdropClick: true,
			    windowClass: 'view_post_detail_dialog',
			    resolve: {
			    	post: function() { return post; },
			    	star: function() { return star; },
			    	auto_play: function() { return auto_play; }
			    },
				templateUrl: '/views/view_post.html',
				controller: 'viewPostController'
			});
		}
		$scope.view_m_comments = true;
	};
	$rootScope.showConfirmDialog = function(options) {
		//if($rootScope.unlockConfirmDialog != null) $rootScope.unlockConfirmDialog.close();
		$rootScope.unlockConfirmDialog = $modal.open({
			backdrop:true,
			keyboard: true,
			backdropClick: true,
			windowClass: 'confirm_dialog',
			resolve: {
				options: function() { return options; }
			},
			templateUrl: '/views/confirm_dialog.html',
			controller: 'confirmDialogController'
		});
	};
	$rootScope.onUnlockContent = function(post,post_type, content_id) {
		if(post.unlock == true) return;
		
		if($rootScope.current_user == null) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Premium Content',
				description: 'You need to login to see this locked content.',
				onOk : function() {
					
				}
			});
		}
		else if($rootScope.current_user.credit == null || typeof $rootScope.current_user.credit == undefined || $rootScope.current_user.credit == "" || parseInt($rootScope.current_user.credit) < parseInt(post.credit)) {
			$rootScope.showConfirmDialog({
				type: 'CONFIRM',
				title: 'Unlock Confirmation',
				description: 'You do not have enough credits in your account.',
				button_text: {
					ok: 'Buy now',
					cancel: 'Close'
				},
				onOk : function() {
					$rootScope.onBuyMoreCredits();
				},
				onCancel: function() {
					
				}
			});
		}
		else {
			$rootScope.showConfirmDialog({
				type: 'CONFIRM',
				title: 'Unlock Confirmation',
				description: 'You have ' + $rootScope.current_user.credit + ' credits left. This content needs ' + post.credit + ' credits to unlock. Do you want to unlock this content?',
				onOk : function() {
					$http({method: 'GET', url: $rootScope.get_api_url() + '&option=buy_content&post_type=' + post_type+ '&content_id=' + content_id}).
			        success(function(data, status, headers, config) {
			        	if(data.status == true) {
			        		$rootScope.current_user.credit = parseInt($rootScope.current_user.credit) - parseInt(post.credit);
			        		
			        		post.unlock = true;
			        		
			        		$rootScope.saveCookie();
			        	}
			        	else {
			        	}
			        }).
			        error(function(data, status, headers, config) {
			        });
				},
				onCancel : function (){
					
				}
			});
		}
	};
	$rootScope.addLike = function(e, feeds, post) {
		if(!post.unlock) return;
		var target = $(e.target);
		var content_id = target.attr("data-content-id");
		var post_type = target.attr('data-post-type');
		var like = post.did_like ? 0 : 1;
		$http({method: 'GET', url: $rootScope.get_api_url() + '&option=set_like&user_id=' + $rootScope.current_user.id + '&post_type=' + post_type + '&content_id=' + content_id + '&like=' + like}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		var i;
        		for(i = 0; i < feeds.length; i++) {
        			if(( (typeof feeds[i].content_id != 'undefined' && feeds[i].content_id != null && feeds[i].content_id == content_id) ||
        					(typeof feeds[i].id != 'undefined' && feeds[i].id != null && feeds[i].id == content_id)	) 
        					&& ( feeds[i].post_type == null || typeof feeds[i].post_type == 'undefined' || feeds[i].post_type == post_type) ) {
        				//reload comments
        		        feeds[i].numberoflike = data.numbersoflike;
        		        feeds[i].did_like = like ? true : false;
        		        break;
        			}
        		}
        	}
        	else {
        	}
        }).
        error(function(data, status, headers, config) {
        });
	};
	$rootScope.showLikeUsers = function(post) {
		CommonLib.show_loading_bubble();
		var url = $rootScope.get_api_url() + '&option=get_like_users&post_type=' + post.post_type + '&content_id=' + post.content_id;
		$http({method: 'GET', url: url}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		$rootScope.dialogLikeUsers = $modal.open({
	    		    backdrop: true,
	    		    keyboard: true,
	    		    backdropClick: true,
	    		    windowClass: 'like_users_dialog',
	    		    resolve: {
	    		    	post: function() { return post; },
	    		    	users: function() { return data.users; }
	    		    },
	    			templateUrl: '/views/like_users.html',
	    			controller: 'likeUsersController'
	    		});
	    	}        	
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
	$rootScope.showApprovalUsers = function(post) {
		CommonLib.show_loading_bubble();
		var url = $rootScope.get_api_url() + '&option=get_approval_users&post_type=' + post.post_type + '&content_id=' + post.content_id;
		$http({method: 'GET', url: url}).
	    success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    	if(data.status == true) {
	    		$rootScope.dialogLikeUsers = $modal.open({
	    		    backdrop: true,
	    		    keyboard: true,
	    		    backdropClick: true,
	    		    windowClass: 'like_users_dialog',
	    		    resolve: {
	    		    	post: function() { return post; },
	    		    	users: function() { return data.users; }
	    		    },
	    			templateUrl: '/views/approval_users.html',
	    			controller: 'approvalUsersController'
	    		});
	    	}        	
	    }).
	    error(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
	    });
	};
    
	$rootScope.approveDraft = function(feeds, post) {
		var content_id = post.content_id;
		var post_type = post.post_type;
		CommonLib.show_loading_bubble();
		
		$http({method: 'GET', url: $rootScope.get_api_url() + '&option=set_approval&user_id=' + $rootScope.current_user.id + '&post_type=' + post_type + '&content_id=' + content_id + '&approval=1'}).
        success(function(data, status, headers, config) {
	    	CommonLib.hide_loading_bubble();
        	if(data.status == true) {
        		var i;
        		for(i = 0; i < feeds.length; i++) {
        			if(( (typeof feeds[i].content_id != 'undefined' && feeds[i].content_id != null && feeds[i].content_id == content_id) ||
        					(typeof feeds[i].id != 'undefined' && feeds[i].id != null && feeds[i].id == content_id)	) 
        					&& ( feeds[i].post_type == null || typeof feeds[i].post_type == 'undefined' || feeds[i].post_type == post_type) ) {
        				//reload comments
        		        feeds[i].numberofapproval = data.numbersofapproval;
        		        feeds[i].did_approve = true;
        		        break;
        			}
        		}
        	}
        	else {
        	}
        }).
        error(function(data, status, headers, config) {
        });
    };
    
    $rootScope.publishDraft = function(feeds, post) {
    	var content_id = post.content_id;
		var post_type = post.post_type;
    	$rootScope.showConfirmDialog({
			type: 'CONFIRM',
			title: 'Publish Feed',
			description: 'Do you want to publish this feed?',
			onOk : function() {
				CommonLib.show_loading_bubble();
				$http({method: 'GET', url: $rootScope.get_api_url() + '&option=set_publish&post_type=' + post_type+ '&content_id=' + content_id}).
		        success(function(data, status, headers, config) {
			    	CommonLib.hide_loading_bubble();
		        	if(data.status == true) {

						post.is_publish = 1;
						post.comments_count = 0;
						post.comments = [];
						
		        		$rootScope.dialogSyndicate = $modal.open({
		        		    backdrop: true,
		        		    keyboard: true,
		        		    backdropClick: true,
		        		    windowClass: 'syndicate_dialog',
		        		    resolve: {
		        		    	post: function() { return post; }
		        		    },
		        			templateUrl: '/views/syndicate.html',
		        			controller: 'syndicateController'
		        		});
		        	}
		        	else {
		        	}
		        }).
		        error(function(data, status, headers, config) {
		        });
			},
			onCancel : function (){
			}
		});
    };
	//message
	$rootScope.showSendMessageDialog = function(user_id, name, image, reply_message) {
		$rootScope.dialogSendMessage = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'send_message_dialog',
		    resolve: {
		    	user_id: function() { return user_id; },
		    	name: function() { return name; },
		    	image: function() { return image; },
		    	reply_message: function() { return reply_message; }
		    },
			templateUrl: '/views/sendmessage.html',
			controller: 'sendMessageController'
		});
	};
	
	//new post
	$rootScope.onNewPostFileSelect = function($files, post_type) {
		$rootScope.new_post_files = $files;
		
		if(post_type == 'photo') {
			if ($files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img_preview').attr('src', e.target.result);
		        }

		        reader.readAsDataURL($files[0]);
		    }
		}
	};
	$rootScope.onAddPost = function(new_post_type, callback) {
		var target = $('#button_post');
		if(new_post_type == 'text') {
			var text = $('#new_post_text');
			if(text.val().trim() == '') {
				text.focus();
				$rootScope.showConfirmDialog({
					type: 'ALERT',
					title: 'New post',
					description: 'Please type text to post.',
					onOk : function() {
					}
				});
			}
			else {
				target.attr('disabled', 'disabled');
				text.attr('disabled','disabled');
				CommonLib.show_waiting_circle(target, 1);
				$http({method: 'GET', url: $rootScope.get_api_url() + '&option=add_text&description='+text.val()+'&tags=' + '&credit=0'}).
		        success(function(data, status, headers, config) {
		        	if(data.status == true) {
		        		text.val('');
		        		callback();
		        	}
		        	else {
		        	}
		        	CommonLib.hide_waiting_circle();
		        	target.removeAttr('disabled');
		        	text.removeAttr('disabled');
		        }).
		        error(function(data, status, headers, config) {
		        	CommonLib.hide_waiting_circle();
		        	target.removeAttr('disabled');
		        	text.removeAttr('disabled');
		        });
			}
		}else {
			var text = $('#new_post_description');
			var file_input = $('#new_post_file');
			if($rootScope.new_post_files == null || $rootScope.new_post_files.length == 0) {
				$rootScope.showConfirmDialog({
					type: 'ALERT',
					title: 'New post',
					description: 'Please select ' + new_post_type + ' to post.',
					onOk : function() {
					}
				});
				return;
			}
			else if(typeof $rootScope.new_post_files == 'undefined' || text.val().trim() == '') {

				$rootScope.showConfirmDialog({
					type: 'ALERT',
					title: 'New post',
					description: 'Please type description to post.',
					onOk : function() {
					}
				});
				return;
			}
			else {
				if($rootScope.new_post_files.length == 0) {
					$rootScope.showConfirmDialog({
						type: 'ALERT',
						title: 'New post',
						description: 'Please select ' + new_post_type + ' to post.',
						onOk : function() {
						}
					});
					return;
				}
				else {
					var extension = $rootScope.new_post_files[0].name.split('.').pop().toLowerCase();
					if(new_post_type == 'photo' && extension != 'jpg' && extension != 'jpeg' && extension != 'bmp' && extension != 'gif' && extension != 'png') {
						$rootScope.showConfirmDialog({
							type: 'ALERT',
							title: 'New post',
							description: 'Please select only jpg, jpeg, bmp, gif or png file.',
							onOk : function() {
							}
						});
						return;
					}
					else if(new_post_type == 'video' && extension != 'mpg' && extension != 'mp4' && extension != 'flv') {
						$rootScope.showConfirmDialog({
							type: 'ALERT',
							title: 'New post',
							description: 'Please select only mpg, mp4 or flv file.',
							onOk : function() {
							}
						});
						return;
					}
				}

				target.attr('disabled', 'disabled');
				text.attr('disabled','disabled');
				CommonLib.show_waiting_circle(target, 1);
				var url = '';
				var fileFormDataName = '';
				if(new_post_type == 'photo') { url = $rootScope.get_api_url() + '&option=add_photo&description='+text.val()+'&tags=' + '&credit=0'; fileFormDataName = 'image'; }
				else { url = $rootScope.get_api_url() + '&option=add_video&description='+text.val()+'&tags=' + '&credit=0'; fileFormDataName = 'video'; }
				
				for (var i = 0; i < $rootScope.new_post_files.length; i++) {
					var file = $rootScope.new_post_files[i];
					$scope.upload = $fileUploader.upload({
						url: url, 
						file: file,
						fileFormDataName: fileFormDataName
					}).progress(function(evt) {
					}).success(function(data, status, headers, config) {
		        		text.val('');
		        		file_input.val('');
		        		$('#img_preview').attr('src', 'http://www.swol.co/static/img/noImageAvailable.JPG');
		        		
		        		
		        		callback();
		        		
			        	CommonLib.hide_waiting_circle();
			        	target.removeAttr('disabled');
			        	text.removeAttr('disabled');
					});
				}
			}
		}
	};
	$rootScope.shareOnFacebook = function(post) {
		if(!post.unlock) return;
		if($rootScope.current_user.enable_facebook == 0) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Share on Facebook',
				description: 'Please enable your Facebook sharing option.',
				onOk : function() {
				}
			});
			return;
		}
//		var body = {
//			link: $rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id,
//			name: "Star Club",
//			caption: "Build great social apps and get more installs.",
//			message: post.description,
//			picture: ''
//		}
//		if(post.post_type == 'text') {
//			body.picture = $rootScope.WEBSITE_URL + '/images/starclub-logo.jpg';
//		}
//		else {
//			body.picture = post.image_path;
//		}
//		FB.getLoginStatus(function(response) {
//			if (response.status === 'connected') {
//				FB.api(
//				    "/me/permissions",
//				    function (response) {
//				      if (response && !response.error && response.data[0].publish_actions != null && typeof response.data[0].publish_actions != 'undefined' && response.data[0].publish_actions == 1) {
//			    		  $rootScope.do_post_on_facebook(body);
//				      }
//				      else {
//				    	  FB.login(function(response) {
//							if (response.authResponse) {
//								$rootScope.do_post_on_facebook(body);
//							}
//							else {
//								
//							}
//						}, {scope: 'publish_actions'});
//				      }
//				    }
//				);
//			} else {
//				FB.login(function(response) {
//					if (response.authResponse) {
//						$rootScope.do_post_on_facebook(body);
//					}
//					else {
//						
//					}
//				}, {scope: 'publish_actions'});
//			}
//		});
//		window.open(
//				'http://www.facebook.com/sharer.php?s=100&p[title]=Star Club&p[summary]='+post.description+'&p[url]='+$rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id+'&p[images][0]='+(post.post_type == 'text' ? $rootScope.WEBSITE_URL + '/images/starclub-logo.jpg' : post.image_path),
//			      //'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id), 
//			      'facebook-share-dialog', 
//			      'width=626,height=436');
//		window.open('http://www.facebook.com/sharer.php?s=100&p[title]=Star Club&p[summary]='+post.description+'&p[url]='+$rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id+'&p[images][0]='+(post.post_type == 'text' ? $rootScope.WEBSITE_URL + '/images/starclub-logo.jpg' : post.image_path),'sharer','toolbar=0,status=0,width=548,height=325');
		
		var picture_link = '';
		if(post.post_type == 'text') {
			picture_link = '';
		}
		else if(post.post_type == 'photo')	{
			picture_link = post.fullimage_path;
		}
		else {
			picture_link = post.image_path.replace('&w=360','').replace('/360');
			if(post.post_type == 'video') {
				var string = post.image_path;

			    var new_string = string.replace(l_aArgs.SERVER_ASSETS_PATH, '');


	
				picture_link = l_aArgs.SERVER_ASSETS_PATH + '/watermark/' + new_string;
			}
		}
		var fb_options = {
		        method: 'feed',
		        name: l_aArgs.NAME,
		        link: post.deep_link_web_lp != '' ? post.deep_link_web_lp : ($rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id),
		        picture: $rootScope.getBitLyUrl(picture_link),
		        caption: "",
		        description: post.caption,
		        message: ''
		    };
		if($rootScope.current_user.facebook_page_id != '') {
			fb_options.from = $rootScope.current_user.facebook_page_id;
		}
		FB.ui(fb_options);
	};
	$rootScope.do_post_on_facebook = function(body) {
		body.link = body.picture;
		FB.api(
		    "/me/feed",
		    "post",
		    body,
		    function (response) {
		      if (response && !response.error) {
		    	  $rootScope.showConfirmDialog({
						type: 'ALERT',
						title: 'Facebook Post',
						description: 'Contents shared on facebook.',
						onOk : function() {
						}
					});
		      }
		    }
		);
	};
	$rootScope.shareOnTwitter = function(post) {
		if(!post.unlock) return;

		if($rootScope.current_user.enable_twitter == 0) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Share on Twitter',
				description: 'Please enable your Twitter sharing option.',
				onOk : function() {
				}
			});
			return;
		}
		var body = {
			message: post.caption,
			type: 'picture',
			picture: ''
		};
		var picture_link = '';
		if(post.post_type == 'text') {
			picture_link = '';
		}
		else if(post.post_type == 'photo')	{
			picture_link = post.fullimage_path;
		}
		else {
			picture_link = post.image_path.replace('&w=360','').replace('/360');
			if(post.post_type == 'video') {
				var string = post.image_path;

			    var new_string = string.replace(l_aArgs.SERVER_ASSETS_PATH, '');

				picture_link = l_aArgs.SERVER_ASSETS_PATH + '/watermark/' + new_string;
			}
		}
		body.picture = $rootScope.getBitLyUrl(picture_link);
		
		window.open('https://twitter.com/intent/tweet?text=' + body.message + '&url=' + body.picture,'_blank','toolbar=0,status=0,width=548,height=325');
	};
	$rootScope.putMetaTag = function(post, star) {
		/*$rootScope.meta_tag = {
			link: $rootScope.WEBSITE_URL + '/#homefeed/' + post.post_type + '/' + post.content_id,
			title: 'Star Club',
			description: post.description,
			image: post.post_type == 'text' ? $rootScope.WEBSITE_URL + '/images/starclub-logo.jpg' : post.image_path
		};*/
	};
	$rootScope.initialize = function() {
		$rootScope.loadCookie();
		
		if($rootScope.current_user == null) {
			$rootScope.onShowLoginButton_click();
		}
	};
	
	$rootScope.invitesFBFriends = function() {
		 FB.ui ({
		   method: 'apprequests',
		   message: 'Invite your Friends to Star club'
		  });
	};
	
	$rootScope.onBuyMoreCredits = function() {
		$rootScope.buyCreditsDialog = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'buy_credit_dialog',
		    resolve: {
		    },
			templateUrl: '/views/buy_credits.html',
			controller: 'buyCreditsController'
		});
	};

	$rootScope.onSelectFacebookPage = function() {
		if($rootScope.current_user.enable_facebook == 0) {
			$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Share on Facebook',
				description: 'Please enable your Facebook sharing option.',
				onOk : function() {
				}
			});
			return;
		}
		$rootScope.selectFacebookPageDialog = $modal.open({
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    windowClass: 'select_facebook_page_dialog',
		    resolve: {
		    },
			templateUrl: '/views/select_facebook_page.html',
			controller: 'selectFacebookPageController'
		});
	};
	
	$rootScope.initialize();
}]);

