enriqueApp.controller('pollsAndQuizzesController', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
    $scope.feeds = [];
    $scope.star = {};
    $scope.page = 1;
	
	$scope.is_loading = false;
    $scope.show_comment_box = true;
    
    
    $scope.loadData = function () {
    	if($rootScope.current_user == null) {
    		$rootScope.showConfirmDialog({
				type: 'ALERT',
				title: 'Polls and Quizzes',
				description: 'You need to login for these contents',
				onOk : function() {
					document.location = $rootScope.WEBSITE_URL;
				}
			});
    	}
    	else {
        	if($scope.is_loading) return;
        	
        	$scope.is_loading = true;
	    	CommonLib.show_loading_bubble();
	    	var url = $rootScope.get_api_url() + '&option=get_main_feed&post_type=poll_quiz&page='+$scope.page+'&comment_count=3';
	    	$http({method: 'GET', url: url}).
	        success(function(data, status, headers, config) {
	        	if(data.status == true) {
	        		$scope.star = data.star;
	        		$scope.feeds = $scope.feeds.concat( data.feeds );
	        		
	        		$scope.page ++;
	        	}
	        	CommonLib.hide_loading_bubble();
	        	
	        	$scope.is_loading = false;
	        }).
	        error(function(data, status, headers, config) {
	        	CommonLib.hide_loading_bubble();
	        	
	        	$scope.is_loading = false;
	        });
    	}
    };
}]);