<?php
class Hub_Master extends CI_Controller 

{
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');


		$this->load->helper('url');
	
		$this->load->helper('language');
		$this->load->model('hub_master_m');
        $this->load->model('main_m');
	
	
		$this->form_validation->set_error_delimiters(
				$this->config->item('error_start_delimiter'),
				$this->config->item('error_end_delimiter')
           
		);
		$this->chanel = $this->session->userdata('chanel');
		$this->data['chanel'] = $this->chanel;
		$this->data['show_errors'] = array();

	}
	
  function _remap($method) {
        $this->load->view('header_n');
        $this->load->view('sidebar_n');
        $this->{$method}();
        $this->load->view('footer_v');
       
    }

	   function index(){
          $this->data['hubmanager_list'] = $this->hub_master_m->get_hub_manager();
          
            $this->load->view('hubmanager_list', $this->data);

      }

     public function hubmanager_edit(){
        
        $id = $this->uri->segment(3);
        $this->data['hubmanager'] = $this->hub_master_m->get_hub_manager_id($id);
        
        $this->data['id'] = $this->data['hubmanager'][0]['id'];
        $this->data['name'] = $this->data['hubmanager'][0]['name'];
        $this->data['template_path'] = $this->data['hubmanager'][0]['template_path'];
        $this->data['api_path'] = $this->data['hubmanager'][0]['api_path'];
        $this->data['front_style_path'] = $this->data['hubmanager'][0]['front_style_path'];
        $this->data['back_style_path'] = $this->data['hubmanager'][0]['back_style_path'];
        $this->data['push_cert_path'] = $this->data['hubmanager'][0]['push_cert_path'];
        $this->data['logo_backend'] = $this->data['hubmanager'][0]['logo_backend'];
        
         if ($this->input->post('submit')){  
         $this->update_hub_manager_id($id);
          }

         $this->load->view('hubmanager_edit', $this->data);
   

    }


     public function update_hub_manager_id($id) {
     
   
      $name = $this->input->post('name');
      $template_path = $this->input->post('template_path');
      $api_path = $this->input->post('api_path');
      $front_style_path = $this->input->post('front_style_path');
      $back_style_path = $this->input->post('back_style_path');
      $push_cert_path = $this->input->post('push_cert_path');
      $logo_backend = $this->input->post('logo_backend');

      $data = array (
            'name' => $name, 
            'template_path' => $template_path,
            'api_path' => $api_path,
            'front_style_path' => $front_style_path,
            'back_style_path' => $back_style_path,
            'push_cert_path' => $push_cert_path,
            'logo_backend' => $logo_backend
            );


            if  ($this->hub_master_m->update_hub_manager($id, $data))
            {
                redirect(site_url('hub_master/index'));
            }
        
            return;
    
      
    }


     public function hubmanager_add($post){

      

          $this->form_validation->set_rules('name', 'name', 'required|max_length[512]|xss_clean');


           if ($this->form_validation->run()==false){}
            else
           {
           if ($this->form_validation->run() == true) 

          { 

        
             if($this->input->post('name',false) == true)

                {
                
             
                {
            $this->data['hub_manager'] = $this->hub_master_m->add_hub_manager();


             redirect(site_url('hub_master/index'));


                }
                }
           
              
                {
                  
                    if(empty($name)){
            show_error("Enter a name!");
              return;
                
                                     }
                }

                }
           
            
          
        }
                  

    
        
        $this->load->view('hubmanager_add', $this->data);
    }



      
  


     public function confirm_deletion(){
     
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete("hub_manager");
        redirect(site_url('hub_master/'));
     }

    
 

   


 

 
}
	