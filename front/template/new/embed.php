<?php

  include(__DIR__.'/../config/init.php');
  $l_sThisURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  
  $l_sCode = str_replace("/","",$_GET['p_sCode']);
  $l_sCode = Utils::decryptItemId($l_sCode);

  list($l_nChannelId,$l_sAssetType,$l_nAssetId) = explode(",",$l_sCode);
  $l_xChannelManager = new ChannelManager();
  $l_xChannel = $l_xChannelManager->getById($l_nChannelId);

  $l_xItemManager = new ItemManager();
  $l_xAsset = $l_xItemManager->getByIdWithTypeId($l_nAssetId,CONTENT_TYPE_ID_VIDEO);
  $l_aVideoParts = pathinfo($l_xAsset['destination']);
  $l_sVideoFile = $l_aVideoParts['dirname'].'/'.$l_aVideoParts['filename'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta property="og:title" content=<?= json_encode($l_xAsset['description']) ?> />
<meta property="og:description" content=<?= json_encode($l_xAsset['description']) ?>/>
<meta property="og:type" content="website" />
<meta property="og:url" content="http://<?= $l_xChannel->url ?>.on.starsite.com/content/video/<?= $l_xAsset['id'] ?>/" />
<meta property="og:image" content="<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/watermark/<?= str_replace("video","photo",$l_sVideoFile) ?>.jpg" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="1200" />
<style type="text/css">
body{ margin:0; padding:0; }
::selection { background: #B3D9FF; color:#000; }
::-moz-selection { background: #B3D9FF; color:#000; }
.flowplayer { width: 80%;  font-family:Arial; background:url(<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/<?= str_replace("video","photo",$l_sVideoFile) ?>.jpg?var=123) no-repeat; background-size:cover; }
   .flowplayer .fp-controls{background-color: rgba(0, 0, 0, 0.4); }
   .flowplayer .fp-timeline { background-color: rgba(0, 0, 0, 0.5)}
   .flowplayer .fp-progress { background-color: rgba(219, 0, 0, 1)}
   .flowplayer .fp-buffer { background-color: rgba(249, 249, 249, 1)}
   
   .flowplayer .fp-logo img {
    width: 75%;
    position: relative;
}
.flowplayer .fp-logo {
    bottom: 25px;
    right: 92%;
    z-index:2;
}
</style>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type='text/javascript' src='http://scads.starsite.com/www/delivery/spcjs.php?id=1'></script>
<script type="text/javascript">

    function checkIfFlowReady(){
        var len = $('.fp-embed-code textarea').length;
	       if( $('.fp-embed-code textarea').length == 1) {
		      $('.fp-embed-code textarea').val( '<iframe scrolling="no" frameborder="0" width="100%" height="auto" src="<?= $l_sThisURL; ?>"></iframe>');
	   }else{
	       setTimeout( checkIfFlowReady , 500);
	   }
    }
    
    $(document).ready(function(){
	   checkIfFlowReady();
    });
</script>
</head>
<body>

<div style="position: relative; padding-bottom: 56.25%;height: 0;">
<script src='<?= preg_replace("/https?:/","",CDN_FRONT) ?>/fp/html5/embed.min.js?var=<?= $l_nRandom; ?>'>
 <div style="width: 100%; height: 100%;position: absolute;top: 0;left: 0; background-image:<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/<?= str_replace("video","photo",$l_sVideoFile) ?>.jpg" data-logo="<?= CDN_FRONT ?>/assets/images/star.png" data-rtmp="rtmp://s2otr7ksbek4s2.cloudfront.net/cfx/st" data-key="$846027373127464" data-analytics="<?= $l_xChannel->google_analytics_key ?>" class="flowplayer">
 <video>
     <source src="<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/<?= $l_sVideoFile ?>/master.m3u8" type="application/x-mpegurl">
     <source src="<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/<?= $l_sVideoFile ?>-480.webm" type="video/webm">
     <source src="<?= CDN_FRONT ?>/<?= $l_xChannel->url ?>/assets/ufile/<?= $l_sVideoFile ?>-480.mp4" type="video/mp4">
     <source src="mp4:<?= $l_xChannel->url ?>/assets/ufile/<?= $l_sVideoFile ?>-480" type="video/flash">
 </video>
</div>
</script>
</div>
</body>
</html>