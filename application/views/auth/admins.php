      <!-- BEGIN PAGE -->  
      <div class="page-content">

         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <h3 class="page-title">
                     Admin Profile
                     <small><?php echo $user->first_name." ".$user->last_name; ?></small>
                  </h3>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
               <div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box">

                        <div id="form_head_back">Edit Admin Profile</div>
                     <div class="portlet-body form pb-feed">
                        <!-- BEGIN FORM-->
                        <div class="row-fluid">
                            <div class="span6">
                                <?php echo form_open_multipart(site_url('admins/admin0'),'');?>
                                    <h3>CMS ADMIN</h3>
                                    <?php echo $admin_message0;?>
                                    <input type="hidden" name="id" value="<?php echo $admin_data['id0']; ?>" />
                                    <div class="control-group">
                                        <label for="email" class="control-label">Email:</label>
                                        <div class="controls">
                                            <input name="email" value="<?php echo $admin_data['email0']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="email" class="control-label">Phone:</label>
                                        <div class="controls">
                                            <input name="phone" value="<?php echo $admin_data['phone0']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="password" class="control-label">Password:</label>
                                        <div class="controls">
                                            <input name="password" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="confirm_password" class="control-label">Confirm Password:</label>
                                        <div class="controls">
                                            <input name="password_confirm" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <input name="submit" value="Save User" class="btn blue" type="submit">
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="span6">
                                <?php echo form_open_multipart(site_url('admins/admin1'),'');?>
                                    <h3>POST & DELETE</h3>
                                    <?php echo $admin_message1;?>
                                    <input type="hidden" name="id" value="<?php echo $admin_data['id1']; ?>" />
                                    <div class="control-group">
                                        <label for="email" class="control-label">Email:</label>
                                        <div class="controls">
                                            <input name="email" value="<?php echo $admin_data['email1']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="email" class="control-label">Phone:</label>
                                        <div class="controls">
                                            <input name="phone" value="<?php echo $admin_data['phone1']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="password" class="control-label">Password:</label>
                                        <div class="controls">
                                            <input name="password" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="confirm_password" class="control-label">Confirm Password:</label>
                                        <div class="controls">
                                            <input name="password_confirm" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <input name="submit" value="Save User" class="btn blue" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">
                                <?php echo form_open_multipart(site_url('admins/admin2'),'');?>
                                    <h3>DELETE</h3>
                                    <?php echo $admin_message2;?>
                                    <input type="hidden" name="id" value="<?php echo $admin_data['id2']; ?>" />
                                    <div class="control-group">
                                        <label for="email" class="control-label">Email:</label>
                                        <div class="controls">
                                            <input name="email" value="<?php echo $admin_data['email2']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="email" class="control-label">Phone:</label>
                                        <div class="controls">
                                            <input name="phone" value="<?php echo $admin_data['phone2']; ?>" class="span6 m-wrap" type="text">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="password" class="control-label">Password:</label>
                                        <div class="controls">
                                            <input name="password" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="confirm_password" class="control-label">Confirm Password:</label>
                                        <div class="controls">
                                            <input name="password_confirm" value="" class="span6 m-wrap" type="password">
                                            <span class="help-inline"> (if changing password) </span>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <input name="submit" value="Save User" class="btn blue" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php echo form_close();?>       
                     </div>
                  </div> -->
                  <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT--> 
    
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE --> 

