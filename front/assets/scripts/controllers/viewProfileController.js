enriqueApp.controller('viewProfileController', ['$scope', '$rootScope', '$http', '$routeParams','$location', '$modal', function($scope, $rootScope, $http, $routeParams, $location, $modal) {
    $scope.feeds = [];
    $scope.user = null;
    
    $scope.new_post_type = 'text';
    $scope.follow_button_text = 'Follow';
    
    $scope.filter = 0;
    
    $scope.loadData = function () {
    	CommonLib.show_loading_bubble();
    	
    	var url = $rootScope.get_api_url() + '&option=get_user&user_id='+parseInt($routeParams.user_id) + ($rootScope.current_user == null ? '' : '&self_user_id=' + $rootScope.current_user.id + '&follow_param=1&cnt_of_page=1000');
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {

        	$rootScope.updateBrightCoveInfo(data);
        	if(data.status == true) {
        		$scope.feeds = data.feeds;
        		$scope.user = data.user;
        		$scope.user.followeds_count = data.user.followeds;
        		$scope.user.followings_count = data.user.followings;
        		$scope.user.did_follow = data.did_follow;
        		$scope.user.did_block = data.did_block;
        		if($scope.user.did_follow) $scope.follow_button_text = 'Unfollow';
        		else $scope.follow_button_text = 'Follow';
        		
        	}
        	CommonLib.hide_loading_bubble();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_loading_bubble();
        });
    };
    
	$scope.onSelectNewPostType = function(e) {
		var target= $(e.target);
		$scope.new_post_type = target.attr('data-post-type');
	};
	
	$scope.onChangeFilter_click = function(e) {
		var target = $(e.target);
		$scope.filter = target.attr('data-filter');
	}
	
	
	$scope.onButtonFollowUser_click = function(e) {
		var target = $(e.target);
		var user_id = target.attr('data-user-id');
		var follow_status = target.attr('data-follow-status');
		
		CommonLib.show_waiting_circle(target);
    	var url = $rootScope.get_api_url() + '&option=set_followuser&followed_user_id='+user_id+'&is_unfollow=' + (follow_status=='true' ? '1' : '0');
    	$http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
        	if(data.status == true) {
        		$scope.user.did_follow = !(follow_status == 'true');
        		$scope.user.followeds_count = data.numbers;

        		if($scope.user.did_follow) $scope.follow_button_text = 'Unfollow';
        		else $scope.follow_button_text = 'Follow';
        	}
        	CommonLib.hide_waiting_circle();
        }).
        error(function(data, status, headers, config) {
        	CommonLib.hide_waiting_circle();
        });
	}

	$scope.onButtonMessageUser_click = function(e) {
		var target = $(e.target);
		
		$rootScope.showSendMessageDialog($scope.user.id, $scope.user.name, $scope.user.img_url, null);
	};
	$scope.onButtonSettings_click = function() {
		$location.path('settings');
	}
	$scope.onButtonInbox_click = function() {
		$location.path('inbox');
	}
	$scope.onRefresh = function() {
		$scope.feeds = [];
		$scope.loadData();
	};
    $scope.loadData();
}]);